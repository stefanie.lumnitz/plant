#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import os
import sys
import plant
import itertools
import numpy as np


def get_parser():
    '''
    Command line parser.
    '''
    descr = ('Filter, multi-look or generate covariance/coherency'
             ' matrices of the input data.'
             ' Options are:'
             ' circular mean (--mean)'
             ' boxcar (--boxcar)'
             ' Gaussian (--gaussian)'
             ' median (--median)'
             ' min (--min)'
             ' max (--max)'
             ' standard deviation (--stddev).'
             ' Multi-look is provided through the options:'
             ' mean (--nlooks)'
             ' median (--nlooks-median)'
             ' min (--nlooks-min)'
             ' max (--nlooks-max)'
             ' To generate covariance (--covariance) or'
             ' Pauli-basis coherency (--coherency) matrices,'
             ' the output directory (--output-dir)'
             ' instead of output file (--output-file) needs to be set.')
    epilog = ('Boxcar filter (size 5x5):\n'
              '    plant_filter.py <INPUT> -b 5 -o <OUTPUT> \n'
              'Boxcar filter (size 3x5):\n'
              '    plant_filter.py <INPUT> -b 3 5 -o <OUTPUT> \n'
              'Gaussian filter (size 5x5):\n'
              '    plant_filter.py <INPUT> -g 5 -o <OUTPUT> \n'
              'Circular kernel filter (diameter 5x5):\n'
              '    plant_filter.py <INPUT> -m 5 -o <OUTPUT> \n'
              'Multilook 5x3:\n'
              '    plant_filter.py <INPUT> --nlooks-y 5 --nlooks-x 3'
              ' -o <OUTPUT> \n'
              '    plant_filter.py <INPUT> --nlooks-az 5 --nlooks-rg 3'
              ' -o <OUTPUT> \n'
              '    plant_filter.py <INPUT> -a 5 -r 3'
              ' -o <OUTPUT> \n'
              'Multiple files to multi-band file:\n'
              '    plant_filter.py <INPUT1> <INPUT2> -b 5 -o <OUTPUT> \n'
              'Multiple files to multiple output files (--separate):\n'
              '    plant_filter.py <INPUT1> <INPUT2> -b 5'
              ' --od <OUTPUT_DIR> --separate')
    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            input_images=2,
                            default_options=1,
                            multilook=1,
                            output_dir=1,
                            # mask=1,
                            separate=1,
                            output_file=1)

    parser.add_argument('--min-points',
                        '--min-ndata',
                        dest='min_points',  # type=float,
                        help='Minimun number of valid points'
                        ' inside window'
                        ' (default: %(default)s)')
    parser.add_argument('--ndata',
                        dest='ndata_window_size', type=float, nargs='+',
                        help='Number of valid points '
                        ' inside window (default: %(default)s)',
                        default=[1])
    parser.add_argument('-m',
                        '--mean',
                        '--circular-mean',
                        '--mean-filter-size',
                        dest='mean', type=float, nargs='+',
                        help='Mean filter size'
                        ' (default: %(default)s)', default=[1])
    parser.add_argument('-b',
                        '--boxcar',
                        '--boxcar-filter-size',
                        dest='boxcar', type=float, nargs='+',
                        help='Boxcar filter size'
                        ' (default: %(default)s)', default=[1])
    parser.add_argument('-g',
                        '--gaussian',
                        '--gaussian-filter-size',
                        nargs='+',
                        dest='gaussian', type=float,
                        help='Gaussian filter size'
                        ' (default: %(default)s)', default=[1])
    parser.add_argument('--median',
                        '--median-filter-size',
                        dest='median', type=float, nargs='+',
                        help='Median filter size'
                        ' (default: %(default)s)', default=[1])
    parser.add_argument('--min',
                        '--min-filter-size',
                        dest='min_filter_size', type=float, nargs='+',
                        help='Min filter size'
                        ' (default: %(default)s)', default=[1])
    parser.add_argument('--max',
                        '--max-filter-size',
                        dest='max_filter_size', type=float, nargs='+',
                        help='Max filter size'
                        ' (default: %(default)s)', default=[1])
    parser.add_argument('--stddev',
                        '--std',
                        '--stddev_window_size',
                        dest='stddev_window_size', type=float, nargs='+',
                        help='Stddev window size'
                        ' (default: %(default)s)', default=[1])
    
    parser.add_argument('--nlooks-zoom',
                        dest='nlooks_zoom',
                        type=float, nargs='+',
                        help='Number of looks (zoom)'
                        ' (default: %(default)s)', default=[1])

    parser.add_argument('--ml-med',
                        '--median-ml',
                        '--nlooks-median',
                        '--median-nlooks',
                        dest='nlooks_median',
                        type=float, nargs='+',
                        help='Number of looks (median value)'
                        ' (default: %(default)s)', default=[1])
    
    parser.add_argument('--ml-min',
                        '--minimum-ml',
                        '--nlooks-min',
                        '--min-nlooks',
                        '--nlooks-minimum',
                        '--minimum-nlooks',
                        dest='nlooks_min',
                        type=float, nargs='+',
                        help='Number of looks (minimum value)'
                        ' (default: %(default)s)', default=[1])
    
    parser.add_argument('--ml-max',
                        '--maximum-ml',
                        '--nlooks-max',
                        '--max-nlooks',
                        '--nlooks-maximum',
                        '--maximum-nlooks',
                        dest='nlooks_max',
                        type=float, nargs='+',
                        help='Number of looks (maximum value)'
                        ' (default: %(default)s)', default=[1])
    
    parser.add_argument('--ml-std',
                        '--stddev-ml',
                        '--nlooks-stddev',
                        '--stddev-nlooks',
                        dest='nlooks_stddev',
                        type=float, nargs='+',
                        help='Number of looks (stddev value)'
                        ' (default: %(default)s)', default=[1])
    
    parser.add_argument('--ml-ndata',
                        '--ndata-ml',
                        '--nlooks-ndata',
                        '--ndata-nlooks',
                        dest='nlooks_ndata',
                        type=float, nargs='+',
                        help='Number of looks (valid data count)'
                        ' (default: %(default)s)', default=[1])
    
    parser.add_argument('--nn', '--nearest-neighbor', dest='nearest_neighbor',
                        action='store_true',
                        help='Nearest neighbor interpolation.')
    
    parser.add_argument('--out-width',
                        '--new-width',
                        '--output-width',
                        dest='output_width',
                        type=int,
                        help='Output width')
    
    parser.add_argument('--out-length',
                        '--new-length',
                        '--output-length',
                        dest='output_length',
                        type=int,
                        help='Output length')

    parser.add_argument('--oversample',
                        '--oversampling',
                        dest='oversampling',
                        type=float, nargs='+',
                        help='Oversampling factor'
                        ' (default: %(default)s)')

    parser_matrix = parser.add_mutually_exclusive_group()
    parser_matrix.add_argument('--cov',
                               '--cov-matrix',
                               '--covariance-matrix',
                               dest='flag_covariance_matrix',
                               action='store_true',
                               help='Generate covariance matrix [C].')

    parser_matrix.add_argument('--coh',
                               '--coh-matrix',
                               '--coherency-matrix',
                               dest='flag_coherency_matrix',
                               action='store_true',
                               help='Generate coherency matrix [T].')
    
    # parser.add_argument('--replace-null',
    #                    '--replace-nan',
    #                    '--replace-nans',
    #                    dest='replace_null',
    #                    required=False, action='store_true',
    #                    help='Interpolate NULL values.',
    #                    default=False)
    return parser


class PlantFilter(plant.PlantScript):

    def __init__(self, parser, argv=None):
        '''
        class initialization
        '''
        self.replace_null = False
        super().__init__(parser, argv)

    def run(self):
        '''
        Run filter
        '''
        if self.separate and self.flag_covariance_matrix:
            self.print('ERROR separate and covariance matrix modes'
                       ' cannot be used together')
            return 
        if not self.output_ext:
            self.output_ext = '.bin'
        if self.separate:
            input_images = self.input_images
            ret_list = []
            for i, current_file in enumerate(input_images):
                self.input_images = [current_file]
                self.output_file = self.output_files[i]
                if (self.output_skip_if_existent and
                        plant.isfile(self.output_file)):
                    print('INFO output file %s already exist, '
                          'skipping execution..' % self.output_file)
                    continue
                ret = self.run_filter()
                ret_list.append(ret)
            if len(ret_list) == 1:
                return ret_list[0]
            return ret_list
        elif (self.flag_covariance_matrix or
              self.flag_coherency_matrix):
            if self.output_file:
                self.print('ERROR for this option, please'
                           ' use output dir (--od)'
                           ' instead of output file (-o)')
            if self.flag_covariance_matrix:
                return self.run_covariance_matrix()
            if len(self.input_images) < 3:
                self.print('ERROR at least 3 inputs are required'
                           ' to generate the coherency matrix')
                return
            return self.run_coherency_matrix()
        elif (not self.output_file and
              not self.output_dir and
              not self.output_ext):
            self.parser.print_usage()
            self.print('ERROR one the following argument is required: '
                       '--output_file, --output_dir, --output_ext')
            sys.exit(1)
        elif (not self.output_file and
              (self.output_dir or
               self.output_ext)):
            self.parser.print_usage()
            self.print('ERROR this script only accepts --output_dir or '
                       '--output_ext in --separate mode')
            sys.exit(1)
        return self.run_filter()

    def run_covariance_matrix(self):

        pol_list = []
        factor_list = []
        kwargs = {}
        kwargs['pol_list'] = pol_list
        kwargs['factor_list'] = factor_list
        self.append_pol(self.hh_file, default_input_index=0,
                        factor=1, **kwargs)
        self.append_pol(self.hv_file, default_input_index=1,
                        factor=np.sqrt(2.0), **kwargs)
        self.append_pol(self.vv_file, default_input_index=2,
                        factor=1, **kwargs)
        ret_list = self.generate_matrix(pol_list, factor_list)
        return ret_list

    def generate_matrix(self, pol_list, factor_list, flag_covariance=True):

        prefix = 'C' if flag_covariance else 'T'
        
        indexes_list = range(len(pol_list))
        ret_list = []

        for p1, p2 in itertools.combinations_with_replacement(indexes_list, 2):
            pol_1_obj = pol_list[p1]
            ret_obj = pol_1_obj.deep_copy()
            factor_1 = factor_list[p1]
            self.geotransform = pol_1_obj.geotransform
            if p1 != p2:
                pol_2_obj = pol_list[p2]
                factor_2 = factor_list[p2]
                if pol_1_obj.nbands != pol_2_obj.nbands:
                    self.print(f'ERROR inputs {pol_list[p1]} and'
                               f' {pol_list[p2]} have different number of'
                               f' bands: {pol_1_obj.nbands} and'
                               f' {pol_2_obj.nbands}')
                    return
            for b in range(pol_1_obj.nbands):
                if p1 == p2:
                    image = np.absolute(pol_1_obj.get_image(band=b))**2
                    image *= factor_1
                else:
                    image = (pol_1_obj.get_image(band=b) *
                             np.conj(pol_2_obj.get_image(band=b)))
                    image *= factor_1*factor_2
                ret_obj.set_image(image, band=b)
            filter_kwargs = self.get_filter_kwargs()
            ret_obj = plant.filter_data(ret_obj, **filter_kwargs)
            if (self.output_dtype is not None and
                    'float' in self.output_dtype.lower() and
                    'complex' in plant.get_dtype_name(ret_obj.dtype).lower()):
                output_file = os.path.join(self.output_dir,
                                           f'{prefix}{p1+1}{p2+1}_real' +
                                           self.output_ext)
                ret_real_obj = plant.real(ret_obj)
                # ret_real_obj = np.real(ret_obj.image)
                self.save_image(ret_real_obj, output_file)
                ret_list.append(ret_real_obj)
                output_file = os.path.join(self.output_dir,
                                           f'{prefix}{p1+1}{p2+1}_imag' +
                                           self.output_ext)
                ret_imag_obj = plant.imag(ret_obj)
                # ret_imag_obj = np.imag(ret_obj.image)
                self.save_image(ret_imag_obj, output_file)
                ret_list.append(ret_imag_obj)
            else:
                output_file = os.path.join(self.output_dir,
                                           f'{prefix}{p1+1}{p2+1}' +
                                           self.output_ext)
                self.save_image(ret_obj, output_file)
                ret_list.append(ret_obj)
        return ret_list

    def run_coherency_matrix(self):

        pol_list = []
        factor_list = []
        kwargs = {}
        kwargs['pol_list'] = pol_list
        kwargs['factor_list'] = factor_list
        self.append_pol(self.hh_file, default_input_index=0,
                        factor=1, **kwargs)
        self.append_pol(self.hv_file, default_input_index=1,
                        factor=2.0, **kwargs)
        self.append_pol(self.vv_file, default_input_index=2,
                        factor=1, **kwargs)
        # print('*** pol_list: ', pol_list)

        w_vector_list = [[0, 2, '+'],
                         [0, 2, '-'],
                         [1, None, None]]

        new_pol_list = []
        new_factor_list = [0.5] * len(w_vector_list)

        for p1, p2, operation in w_vector_list:
            print(f'p{p1} {operation} p{p2}')
            pol_1_obj = pol_list[p1]  # .copy()
            ret_obj = pol_1_obj.deep_copy()
            factor_1 = factor_list[p1]
            self.geotransform = pol_1_obj.geotransform
            if p2 is not None:
                pol_2_obj = pol_list[p2]  # .copy()
                factor_2 = factor_list[p2]
                if pol_1_obj.nbands != pol_2_obj.nbands:
                    self.print(f'ERROR inputs {pol_list[p1]} and'
                               f' {pol_list[p2]} have different number of'
                               f' bands: {pol_1_obj.nbands} and'
                               f' {pol_2_obj.nbands}')
                    return
            for b in range(pol_1_obj.nbands):
                if p2 is None:
                    image = np.absolute(pol_1_obj.get_image(band=b))**2
                    image *= factor_1
                else:
                    if operation == '-':
                        factor_2 *= -1.0
                    image = (factor_1*pol_1_obj.get_image(band=b) +
                             factor_2*pol_2_obj.get_image(band=b))
                ret_obj.set_image(image, band=b)
            new_pol_list.append(ret_obj)

        ret_list = self.generate_matrix(new_pol_list, new_factor_list,
                                        flag_covariance=False)
        return ret_list

    def append_pol(self, identified_file,
                   default_input_index=None,
                   factor=None, pol_list=[], factor_list=[]):
        current_file = None
        if identified_file:
            current_file = identified_file
        elif len(self.input_images) >= default_input_index+1:
            current_file = self.input_images[default_input_index]
        if current_file:
            image_obj = self.read_image(current_file)
            # print('*** current_file: ', current_file)
            # print('*** shape: ', image_obj.shape)
            pol_list.append(image_obj)
            factor_list.append(factor)

    def run_filter(self):
        self.print('input image(s): %s'
                   % str(self.input_images))
        image_obj = self.read_image(self.input_images[0])
        band_count = image_obj.nbands
        for current_input in range(1, len(self.input_images)):
            current_image_obj = self.read_image(
                self.input_images[current_input])
            for current_band in range(current_image_obj.nbands):
                image = current_image_obj.get_image(band=current_band)
                # image = np.copy(image)
                image_obj.setImage(image, band=band_count)
                band_count += 1
        
        self.geotransform = image_obj.geotransform

        kwargs = self.get_filter_kwargs()
        ret = plant.filter_data(image_obj, **kwargs)
        out_image_obj = ret
        # image_obj.image = out_data
        out_image_obj = plant.apply_null(out_image_obj,
                                         in_null=self.in_null,
                                         out_null=self.out_null)

        self.save_image(out_image_obj, self.output_file)
        return out_image_obj

    def get_filter_kwargs(self):

        kwargs = {}
        kwargs['overwrite_obj'] = True
        kwargs['min_points'] = self.min_points
        kwargs['mean'] = self.mean
        kwargs['boxcar'] = self.boxcar
        kwargs['gaussian'] = self.gaussian
        kwargs['median'] = self.median
        kwargs['min_filter_size'] = self.min_filter_size
        kwargs['max_filter_size'] = self.max_filter_size
        kwargs['stddev_window_size'] = self.stddev_window_size
        kwargs['ndata_window_size'] = self.ndata_window_size
        kwargs['nlooks_zoom'] = self.nlooks_zoom
        kwargs['nlooks'] = self.nlooks
        kwargs['nlooks_median'] = self.nlooks_median
        kwargs['nlooks_min'] = self.nlooks_min
        kwargs['nlooks_max'] = self.nlooks_max
        kwargs['nlooks_stddev'] = self.nlooks_stddev
        kwargs['nlooks_ndata'] = self.nlooks_ndata

        kwargs['output_width'] = self.output_width
        kwargs['output_length'] = self.output_length
        kwargs['oversampling'] = self.oversampling
        kwargs['resize_interpolation'] = None

        kwargs['nearest_neighbor'] = self.nearest_neighbor
        kwargs['replace_null'] = self.replace_null
        kwargs['verbose'] = self.verbose

        return kwargs


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantFilter(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
