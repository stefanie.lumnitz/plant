#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from os import path, makedirs, stat
import numpy as np
# import ast
import string
# import matplotlib.pyplot as plt
from PIL import Image, ImageDraw, ImageFont
from scipy.ndimage.interpolation import shift
# from copy import deepcopy
import plant

FLAG_SPLIT_SPECTRUM_NORMALIZE = False


def get_parser():
    '''
    Command line parser.
    '''
    descr = ('Provide multiple tools for manipulating data'
             ' including basic algebraic and mathematical'
             ' operations, rotation, shifting and normalization.')
    epilog = ''
    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            input_images=1,
                            # default_input_options=1,
                            topo_dir=1,
                            # geo_selection=1,
                            output_file=1,
                            output_dir=1,
                            pixel_size_x=1,
                            pixel_size_y=1,
                            default_options=1,
                            default_geo_input_options=1,
                            # default_geo_output_options=1,
                            separate=1)
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--rn',
                       '--rename',
                       '--mv',
                       '--move',
                       dest='flag_rename',
                       action='store_true',
                       help='Rename image and headers')
    group.add_argument('--del',
                       '--remove',
                       '--rm',
                       dest='flag_remove',
                       action='store_true',
                       help='Remove image and headers')
    group.add_argument('--cp',
                       '--copy',
                       dest='flag_copy',
                       action='store_true',
                       help='Copy image and headers')

    group.add_argument('--clip',
                       nargs=2,
                       type=float,
                       dest='clip',
                       help='Limit (clip) values inside a '
                       'min and max range. Please provide '
                       "two values (use 'nan' to reset limit)")

    group.add_argument('--assign-geo',
                       '--assign-georeferences',
                       action='store_true',
                       dest='assign_georeferences',
                       help='Assign lat/lon georeferences to an input file')

    group.add_argument('--to-rgb',
                       '--to-cmap',
                       dest='to_rgb',
                       help='Convert a single band input into '
                       'a tree band output using a color map',
                       action='store_true')

    group.add_argument('--hillshade',
                       '--hill-shade',
                       '--shaderelief',
                       '--shade-relief',
                       '--terrainrelief',
                       '--terrain-relief',
                       dest='hillshade',
                       help='Convert a DEM into a shaded relief '
                       'a tree band output using a color map',
                       action='store_true')

    group.add_argument('--slide-compare',
                       '--compare-slide',
                       dest='slide_compare',
                       help='Compare two inputs using HTML/JavaScript slide',
                       action='store_true')

    group.add_argument('--y-shift-padding',
                       '--shift-y-padding',
                       dest='shift_y_padding',
                       type=str,
                       help='Shift image in Y-direction (zero padding)')

    group.add_argument('--y-shift',
                       '--shift-y',
                       dest='shift_y',
                       type=str,
                       help='Shift image in Y-direction')

    group.add_argument('--x-shift',
                       '--shift-x',
                       dest='shift_x',
                       type=str,
                       help='Shift image in X-direction')

    group.add_argument('--shift',
                       '--xy-shift'
                       '--shift-xy',
                       dest='shift',
                       nargs=2,
                       type=str,
                       help='Shift image in X- and Y-direction')

    group.add_argument('-e',
                       '--equation',
                       '--eval',
                       '--evaluate',
                       type=str,
                       dest='equation',
                       help='Safely evaluate equation')
    group.add_argument('--spectral-norm',
                       action='store_true',
                       dest='spectral_norm',
                       help='Spectral normalization')

    group.add_argument('--spectral-norm-x',
                       action='store_true',
                       dest='spectral_norm_x',
                       help='Spectral normalization in X direction')

    group.add_argument('--spectral-norm-y',
                       action='store_true',
                       dest='spectral_norm_y',
                       help='Spectral normalization in Y direction')

    group.add_argument('--spectral-split-x',
                       '--split-spectrum-x',
                       '--spectral-split-rg',
                       '--split-spectrum-rg',
                       # action='store_true',
                       type=int,
                       dest='spectral_split_x',
                       help='Spectral split in X-dimension')

    group.add_argument('--spectral-split-y',
                       '--split-spectrum-y',
                       '--spectral-split-az',
                       '--split-spectrum-az',
                       type=int,
                       # action='store_true',
                       dest='spectral_split_y',
                       help='Spectral split in Y-dimension')

    group.add_argument('--fft-split-x',
                       '--split-fft-x',
                       '--fft-split-rg',
                       '--split-fft-rg',
                       # action='store_true',
                       type=int,
                       dest='fft_split_x',
                       help='FFT split in X-dimension')

    group.add_argument('--fft-split-y',
                       '--split-fft-y',
                       '--fft-split-az',
                       '--split-fft-az',
                       type=int,
                       # action='store_true',
                       dest='fft_split_y',
                       help='FFT split in Y-dimension')

    group.add_argument('--separate-real-imaginary',
                       '--out-real-imaginary',
                       '--separate-real-imag',
                       '--out-real-imag',
                       action='store_true',
                       dest='separate_real_imag',
                       help='Decompose complex data into  real and'
                       ' imaginary part (output is a 2-bands file)')

    group.add_argument('--separate-polar',
                       '--out-polar',
                       '--complex-to-polar',
                       '--complex-2-polar',
                       action='store_true',
                       dest='separate_polar',
                       help='Decompose complex data into magnitude '
                       ' and phase (output is a 2-bands file)')

    group.add_argument('--aggregate-real-imaginary',
                       '--in-real-imaginary',
                       '--aggregate-real-imag',
                       '--in-real-imag',
                       action='store_true',
                       dest='aggregate_real_imag',
                       help='Create complex data from real'
                       ' (1st input) and imaginary (2nd input) parts')

    group.add_argument('--aggregate-polar',
                       '--in-polar',
                       '--polar-to-complex',
                       '--polar-2-complex',
                       action='store_true',
                       dest='aggregate_polar',
                       help='Create complex data from magnitude '
                       ' (1st input) and phase (2nd input)')

    group.add_argument('--concatenate-x', '--x-concatenate',
                       '--join-x', '--x-join',
                       action='store_true',
                       dest='concatenate_x',
                       help='Concatenate inputs in X-direction')

    group.add_argument('--concatenate-y', '--y-concatenate',
                       '--join-y', '--y-join',
                       action='store_true',
                       dest='concatenate_y',
                       help='Concatenate inputs in Y-direction')

    group.add_argument('--concatenate-z', '--z-concatenate',
                       '--join-z', '--z-join',
                       action='store_true',
                       dest='concatenate_z',
                       help='Concatenate inputs in Z-direction')

    group.add_argument('--join', '--join-bands', '--join-inputs',
                       action='store_true',
                       dest='join',
                       help='Join inputs in a single file')

    # group.add_argument('--phase-wrap', '--wrap-phase', '--wrap-phase',
    #                   action='store_true',
    #                   dest='phase_wrap',
    #                   help='Wrap phase')

    group.add_argument('--rotate-90',
                       action='store_true',
                       dest='rotate_90',
                       help='Rotate input by 90 degrees')

    group.add_argument('--rotate-180',
                       action='store_true',
                       dest='rotate_180',
                       help='Rotate input by 180 degrees')

    group.add_argument('--rotate-270',
                       action='store_true',
                       dest='rotate_270',
                       help='Rotate input by 270 degrees')

    group.add_argument('--slope-x', '--x-slope',
                       '--derivative-x', '--x-derivative',
                       action='store_true',
                       dest='derivative_x',
                       help='X-axis derivative '
                       '(use --pixel-size-x parameter if not '
                       'unitary)')

    group.add_argument('--slope-y', '--y-slope',
                       '--derivative-y', '--y-derivative',
                       action='store_true',
                       dest='derivative_y',
                       help='Y-axis derivative '
                       '(use --pixel-size-y parameter if not '
                       'unitary)')

    group.add_argument('--phase-derivative-x',
                       '--derivative-phase-x',
                       '--x-derivative-phase',
                       action='store_true',
                       dest='derivative_phase_x',
                       help='X-axis derivative (phase) ')

    group.add_argument('--phase-derivative-y',
                       '--derivative-phase-y',
                       '--y-derivative-phase',
                       action='store_true',
                       dest='derivative_phase_y',
                       help='Y-axis derivative (phase)')

    group.add_argument('--integrate-x', '--x-integrate',
                       action='store_true',
                       dest='integrate_x',
                       help='X-axis integrate '
                       '(use --pixel-size-x parameter if not '
                       'unitary)')

    group.add_argument('--integrate-y', '--y-integrate',
                       action='store_true',
                       dest='integrate_y',
                       help='Y-axis integrate '
                       '(use --pixel-size-y parameter if not '
                       'unitary)')

    group.add_argument('--unwrap-x', '--x-unwrap',
                       action='store_true',
                       dest='unwrap_x',
                       help='X-axis unwrap '
                       '(use --pixel-size-x parameter if not '
                       'unitary)')

    '''
    group.add_argument('--unwrap-y', '--y-unwrap',
                       action='store_true',
                       dest='unwrap_y',
                       help='Y-axis unwrap '
                       '(use --pixel-size-y parameter if not '
                       'unitary)')
    '''

    group.add_argument('--slope',
                       action='store_true',
                       dest='slope',
                       help='Slope '
                       '(sqrt(derivative_x**2+derivative_y**2))'
                       ' in radians (use --pixel-size-x and --pixel-size-y '
                       'if these parameters are not unitary)')

    group.add_argument('--normalize-x',
                       '--x-normalize',
                       action='store_true',
                       dest='normalize_x',
                       help='X-axis normalization to [0, 1]'
                       '(v-min)/(max-min)')

    group.add_argument('--normalize-y',
                       '--y-normalize',
                       action='store_true',
                       dest='normalize_y',
                       help='Y-axis normalization to [0, 1]'
                       '(v-min)/(max-min)')

    group.add_argument('--normalize',
                       '--norm',
                       action='store_true',
                       dest='normalize',
                       help='Image normalization to [0, 1]'
                       '(v-min)/(max-min)')

    group.add_argument('--normalize-x-mean',
                       '--x-normalize-mean',
                       action='store_true',
                       dest='normalize_x_mean',
                       help='X-axis mean normalization '
                       '(v-mean)/(max-min)')

    group.add_argument('--normalize-y-mean',
                       '--y-normalize-mean',
                       action='store_true',
                       dest='normalize_y_mean',
                       help='Y-axis mean normalization '
                       '(v-mean)/(max-min)')

    group.add_argument('--normalize-mean',
                       action='store_true',
                       dest='normalize_mean',
                       help='Normalization based on image mean'
                       '(v-mean)/(max-min)')

    group.add_argument('--normalize-x-max',
                       '--x-normalize-max',
                       action='store_true',
                       dest='normalize_x_max',
                       help='X-axis max normalization '
                       '(divide each line by line max)')

    group.add_argument('--normalize-y-max',
                       '--y-normalize-max',
                       action='store_true',
                       dest='normalize_y_max',
                       help='Y-axis max normalization '
                       '(divide each column by column max)')

    group.add_argument('--normalize-max',
                       action='store_true',
                       dest='normalize_max',
                       help='Image max normalization'
                       '(divide image by its max)')

    group.add_argument('--normalize-x-sum',
                       '--x-normalize-sum',
                       action='store_true',
                       dest='normalize_x_sum',
                       help='X-axis sum normalization '
                       '(divide each line by line summation)')

    group.add_argument('--normalize-y-sum',
                       '--y-normalize-sum',
                       action='store_true',
                       dest='normalize_y_sum',
                       help='Y-axis sum normalization '
                       '(divide each column by column summation)')

    group.add_argument('--normalize-sum',
                       action='store_true',
                       dest='normalize_sum',
                       help='Image sum normalization'
                       '(divide image by its sum)')

    group.add_argument('--normalize-x-stddev',
                       '--x-normalize-stddev',
                       '--standardize-x',
                       '--x-standardize',
                       action='store_true',
                       dest='normalize_x_stddev',
                       help='X-axis stddev normalization '
                       ' (standardization): (v-mean)/stddev')

    group.add_argument('--normalize-y-stddev',
                       '--y-normalize-stddev',
                       '--standardize-y',
                       '--y-standardize',
                       action='store_true',
                       dest='normalize_y_stddev',
                       help='Y-axis stddev normalization '
                       ' (standardization): (v-mean)/stddev')

    group.add_argument('--normalize-stddev',
                       '--standardize',
                       action='store_true',
                       dest='normalize_stddev',
                       help='Normalization based on image stddev'
                       ' (standardization): (v-mean)/stddev')

    group.add_argument('--vect-to-image',
                       '--vector-to-image',
                       '--vect-image',
                       '--vector-image',
                       action='store_true',
                       dest='vector_to_image',
                       help='Generate a 2-D image using an input vector.'
                       ' First input is the data vector. The second input'
                       ' (optional) is the position vector of the output'
                       ' Y-axis.')

    group.add_argument('--vect-to-rgb',
                       '--vector-to-rgb',
                       '--vect-rgb',
                       '--vector-rgb',
                       action='store_true',
                       dest='vector_to_rgb',
                       help='Generate a 2-D RGB image using an input vector.'
                       ' First input is the data vector. The second input'
                       ' (optional) is the position vector of the output'
                       ' Y-axis.')

    group.add_argument('--diff',
                       '--sub',
                       '--subtract',
                       action='store_true',
                       dest='sub',
                       help='Difference between two input images')
    group.add_argument('--add',
                       '--sum',
                       '--plus',
                       action='store_true',
                       dest='add',
                       help='Add all input images (pixelwise)')
    group.add_argument('--cross-mult', '--cross-multi',
                       '--cross-multiply', '--cross-mul',
                       action='store_true',
                       dest='cross_mult',
                       help='Cross-multiply two complex images'
                       ' (s1.s2*)')
    group.add_argument('--mult', '--multi',
                       '--multiply', '--mul',
                       action='store_true',
                       dest='mult',
                       help='Multiply all input images (pixelwise)')
    group.add_argument('--div', '--divide',
                       action='store_true',
                       dest='div',
                       help='Divide first by the second input image')

    group.add_argument('--nan-diff',
                       '--nan-sub',
                       '--nan-subtract',
                       action='store_true',
                       dest='nan_sub',
                       help='Difference between two input images')
    group.add_argument('--nan-add',
                       '--nan-sum',
                       '--nan-plus',
                       action='store_true',
                       dest='nan_add',
                       help='Add all input images (pixelwise)')
    group.add_argument('--nan-mult', '--nan-multi',
                       '--nan-multiply', '--nan-mul',
                       action='store_true',
                       dest='nan_mult',
                       help='Multiply all input images (pixelwise)')
    group.add_argument('--nan-div', '--nan-divide',
                       action='store_true',
                       dest='nan_div',
                       help='Divide first by the second input image')

    group.add_argument('--min', '--minimum',
                       action='store_true',
                       dest='data_min',
                       help='Minimum value between all input '
                       'images (pixelwise)')

    group.add_argument('--max', '--maximum',
                       action='store_true',
                       dest='data_max',
                       help='Maximum value between all input '
                       'images (pixelwise)')

    group.add_argument('--nan-mean',
                       '--nan-average',
                       '--nan-avg',
                       action='store_true',
                       dest='data_nan_mean',
                       help='Mean value of non NULL (pixelwise)')
    group.add_argument('--avg', '--average', '--mean',
                       action='store_true',
                       dest='data_mean',
                       help='Mean value (pixelwise)')

    group.add_argument('--nan-median',
                       action='store_true',
                       dest='data_nan_median',
                       help='Median value of non NULL (pixelwise)')
    group.add_argument('--median',
                       action='store_true',
                       dest='data_median',
                       help='Median value (pixelwise)')

    '''
    group.add_argument('--avg', '--average',
                       action='store_true',
                       dest='data_avg',
                       help='Average value (pixelwise)')
    '''
    group.add_argument('--import-null',
                       '--import-nan',
                       '--import-nans',
                       '--copy-nan', '--copy-nans',
                       action='store_true',
                       dest='import_null',
                       help='Import NaNs from second image to the '
                       ' first, saving results in the output')

    group.add_argument('--import-geo',
                       '--import-geotransform',
                       '--import-geoinformation',
                       '--copy-geo',
                       '--copy-geotransform',
                       '--copy-geoinformation',
                       action='store_true',
                       dest='import_geo',
                       help='Import geo-information from second image to the '
                       ' first, saving results in the output')

    group.add_argument('--apply-header-to',
                       # '--copy-header',
                       type=str,
                       dest='apply_header_to',
                       help='Use header from first image to open the '
                       ' image referenced from this parameter. The'
                       ' results will be saved in the output')

    group.add_argument('--profilex', '--profile-x',
                       '--xprofile', '--x-profile',
                       dest='profilex',
                       action='store_true',
                       help='X-dimension averaged profile (for a single'
                       ' line profile, select the line using -hrow)')

    group.add_argument('--profiley', '--profile-y',
                       '--yprofile', '--y-profile',
                       dest='profiley',
                       action='store_true',
                       help='Y-dimension averaged profile (for a single'
                       ' column profile, select the column using -col)')

    group.add_argument('--profileband', '--profile-band',
                       '--bandprofile', '--band-profile',
                       dest='profile_band',
                       action='store_true',
                       help='Bands averaged profile')

    group.add_argument('--zeros',
                       '--zeroes',
                       action='store_true',
                       dest='create_zeros',
                       help='Create a image with zeros. The first input is'
                       ' the number of rows (lenght) and the second input is'
                       ' the number of cols (width).')

    group.add_argument('--cover-null',
                       '--cover-nan',
                       action='store_true',
                       dest='cover_null',
                       help='Cover NULL values of the first input '
                       'with data from the second input')

    group.add_argument('--crop-box-valid',
                       '--crop-valid-box',
                       '--box-valid',
                       action='store_true',
                       dest='crop_valid_box',
                       help='Find minimum box that contains all valid data.'
                       ' and crop inputs')

    group.add_argument('--set-bbox',
                       dest='set_bbox',
                       nargs=4,
                       type=float,
                       help='Set bbox')

    group.add_argument('--set-bbox-outer',
                       dest='set_bbox_outer',
                       nargs=4,
                       type=float,
                       help='Set bbox (outer edges convention)')

    group.add_argument('--georef-shift',
                       dest='georef_shift',
                       nargs=2,
                       type=float,
                       help='Shift geoinformation header')

    parser.add_argument('--linecolor', '--line-color',
                        dest='linecolor',
                        type=str,
                        help='Defines plot line color'
                        ' (for option --vect-to-rgb)')
    parser.add_argument('--linewidth', '--line-width',
                        dest='linewidth',
                        type=int,
                        help='Defines plot linewidth'
                        ' (for option --vect-to-rgb)')

    parser.add_argument('--shift-mode',
                        dest='shift_mode',
                        type=str,
                        help='Shift mode. Options:'
                        ' constant, nearest, reflect or wrap',
                        default='constant')

    parser.add_argument('--shift-order',
                        dest='shift_order',
                        type=float,
                        help='Shift spline interpolation order',
                        default=0)

    parser.add_argument('--argument',
                        '--operator',
                        dest='argument',
                        type=str,
                        help='Argument for selected operation for'
                        ' --separate mode')

    parser.add_argument('--text',
                        '--draw-text',
                        '--annotate',
                        dest='draw_text',
                        type=str,
                        help='Insert text (first element) '
                        ' at position Y, X (second'
                        ' and third elements), and fontsize'
                        ' (optional)')

    return parser


class PlantUtil(plant.PlantScript):

    def __init__(self, parser, argv=None):
        '''
        class initialization
        '''
        super().__init__(parser, argv)

    def run(self):
        '''
        Run main method
        '''
        self.flag_no_output_operation = any([self.flag_remove])
        self.flag_multiple_input_operation = any([self.join,

                                                  self.add,
                                                  self.sub,
                                                  self.mult,

                                                  self.nan_add,
                                                  self.nan_sub,
                                                  self.nan_mult,

                                                  self.profilex,
                                                  self.profiley,
                                                  self.profile_band,

                                                  self.shift,
                                                  self.shift_y,
                                                  self.shift_y_padding,
                                                  self.shift_x,
                                                  self.cover_null,
                                                  self.data_min,
                                                  self.data_max,
                                                  # self.data_avg,

                                                  self.concatenate_x,
                                                  self.concatenate_y,
                                                  self.concatenate_z,
                                                  self.data_nan_mean,
                                                  self.data_mean,
                                                  self.data_nan_median,
                                                  self.data_median,

                                                  self.flag_copy,
                                                  self.flag_remove])
        if self.separate:
            input_images = self.input_images
            ret_list = []
            for i, current_file in enumerate(input_images):
                self.input_images = [current_file]
                self.output_file = self.output_files[i]
                ret = self.generate_single_output()
                ret_list.append(ret)
            return ret_list
        elif (not self.output_file and
              not self.output_dir and
              not self.output_ext and
              not self.flag_no_output_operation):
            self.parser.print_usage()
            self.print('ERROR one the following argument is required: '
                       '--output_file, --output_dir, --output_ext')
            return
        elif (not self.output_file and
              (self.output_dir or
               self.output_ext)):
            self.parser.print_usage()
            self.print('ERROR this script only accepts --output_dir or '
                       '--output_ext in --separate mode')
            return
        ret = self.generate_single_output()
        return ret

    def _normalize_spectrum(self, image_fft):
        image_fft_abs = plant.filter_data(
            np.absolute(image_fft),
            mean=11)
        max_v = np.nanpercentile(image_fft_abs, 99)
        # min_v = np.nanpercentile(image_fft_abs, 5)
        # valid_mask = image_fft_abs > min_v
        # valid_mask = np.binary_closing(valid_mask, 11)
        # ind = np.where(valid_mask)
        ind = np.where(image_fft_abs != 0)
        image_fft[ind] = max_v*image_fft[ind]/image_fft_abs[ind]
        # image_fft = max_v*image_fft/image_fft_abs
        return image_fft

    def generate_single_output(self):
        if self.argument is not None:
            self.input_images.append(self.argument)
        self.print(f'input image(s): {self.input_images}')
        image_obj = None

        self.print(f'output image: {self.output_file}')
        if not path.isdir(plant.dirname(self.output_file)):
            makedirs(plant.dirname(self.output_file))

        if ((self.join or self.flag_copy) and
                self.output_format is not None and
                self.output_format.upper() == 'VRT'):
            image_obj = self.read_image(self.input_images[0],
                                        only_header=True)
            plant.save_image(image_obj,
                             self.output_file,
                             output_reference_file=self.input_images,
                             output_format='VRT')
            return image_obj
        '''
        elif (len(self.input_images) > 2 and
              not self.flag_multiple_input_operation):
            self.print(f'ERROR multiple inputs ({len(self.input_images)})'
                       ' are not permitted for selected operation.')
            return
        '''

        # operations that do not open any image:
        if self.flag_rename:
            self.rename_image(self.input_images[0], self.output_file)
            return self.output_file

        if self.flag_remove:
            for current_image in self.input_images:
                plant.append_temporary_file(current_image)
            return

        # otherwise open first image (it will be used as ref. to the output)
        if image_obj is None:
            image_obj = self.read_image(self.input_images[0])
            if image_obj is None:
                return

        if self.assign_georeferences:
            ret = plant.assign_georeferences(
                image_obj,
                self.topo_dir,
                output_file=self.output_file,
                plant_transform_obj=self.plant_transform_obj,
                flag_temporary=False,
                force=self.force,
                verbose=self.verbose)
            return ret

        if self.draw_text:
            text_lines = self.draw_text.split(plant.IMAGE_NAME_SEPARATOR)

            for current_band in range(image_obj.nbands):
                image_1 = image_obj.get_image(band=current_band)
                dtype = image_1.dtype
                # .ravel()
                # pil_image = Image.new('F', (image_1.shape[0],
                #                             image_2.shape[0]))
                pil_image = Image.fromarray(image_1)
            
            for text_line in text_lines:
                text_line_splitted = text_line.split(
                    plant.ELEMENT_SEPARATOR)
                kwargs = {}
                if len(text_line_splitted) > 3:
                    fontsize = text_line_splitted[3]
                    font_type = ImageFont.truetype('Times',
                                                   fontsize)
                    kwargs['font_type'] = font_type

                pil_draw = ImageDraw.Draw(pil_image)
                pil_draw.text(xy=(float(text_line_splitted[2]),
                                  float(text_line_splitted[1])),
                              text=text_line_splitted[0],
                              **kwargs)
                out_image = np.asarray(pil_image, dtype=dtype)
                image_obj.set_band(out_image,
                                   band=current_band)



            # image_obj = out_image_obj
            self.save_image(image_obj, self.output_file)
            return image_obj


        if self.vector_to_image or self.vector_to_rgb:
            image_2_obj = self.read_image(self.input_images[1])
            out_image_obj = None
            for current_band in range(image_obj.nbands):
                image_1 = image_obj.get_image(band=current_band).ravel()
                if image_2_obj is not None:
                    image_2 = image_2_obj.get_image(band=current_band).ravel()
                if image_2_obj is None or image_2 is None:
                    image_2 = np.arange(image_1.shape[0])
                '''
                out_image = np.zeros((image_2.shape[0],
                                      image_1.shape[0]))
                self.print('image shape: %s' % str(out_image.shape))
                '''
                # WHITE = (255, 255, 255)
                # BLUE = "#0000ff"
                # RED  = "#ff0000"
                if self.vector_to_image:
                    pil_image = Image.new('F', (image_1.shape[0],
                                                image_2.shape[0]))
                    output_dtype = np.float32
                elif self.vector_to_rgb:
                    pil_image = Image.new('RGB', (image_1.shape[0],
                                                  image_2.shape[0]),
                                          self.background_color)
                    output_dtype = np.uint8
                pil_draw = ImageDraw.Draw(pil_image)
                if self.linewidth is None:
                    self.linewidth = 1
                x_vect = np.arange(image_1.shape[0])
                y_vect = np.zeros((image_1.shape[0]))
                for i in range(0, image_1.shape[0]):
                    if plant.isnan(image_1[i]):
                        y_vect[i] = np.nan
                        continue
                    index = np.argmin(np.absolute(image_2-image_1[i]))
                    if (plant.isnan(index) or index < 0 or
                            index > image_2.shape[0]-1):
                        y_vect[i] = np.nan
                        continue
                    y_vect[i] = image_2.shape[0]-1-index
                xy_tuples = []
                for x, y in zip(x_vect, y_vect):
                    if plant.isnan(y):
                        if len(xy_tuples) == 0:
                            continue
                        pil_draw.line(xy_tuples,
                                      width=self.linewidth,
                                      fill=self.linecolor)
                        xy_tuples = []
                        continue
                    xy_tuples.append((x, y))
                pil_draw.line(xy_tuples,
                              width=self.linewidth,
                              fill=self.linecolor)

                # fill=BLUE

                '''

                for i in range(1, image_2.shape[0]):


                    denominator = float(image_2[i-1] - image_2[i])
                    lower_limit = float(image_2[i-1])
                    upper_limit = float(image_2[i])
                    ind = np.where(np.logical_and(image_1 >= lower_limit,
                                                  image_1 < upper_limit))

                    # lower limit
                    lower_line = np.absolute((image_1[ind[0]] -
                                              lower_limit)/denominator)
                    out_image[image_2.shape[0]-1-(i-1)-1, ind[0]] = lower_line

                    # upper_limit
                    upper_line = np.absolute((image_1[ind[0]] -
                                              upper_limit)/denominator)
                    out_image[image_2.shape[0]-1-(i-1), ind[0]] = upper_line
                    # out_image[min([image_2.shape[0]-1-(i-1)+1,
                    #               image_2.shape[0]-1]), ind[0]] = 1
                '''
                im_arr = np.fromstring(pil_image.tobytes(), dtype=output_dtype)
                if self.vector_to_image:
                    im_arr = im_arr.reshape((pil_image.size[1],
                                             pil_image.size[0]))
                    ncolors = 1
                elif self.vector_to_rgb:
                    im_arr = im_arr.reshape((pil_image.size[1],
                                             pil_image.size[0], 3))
                    ncolors = 3
                for b in range(ncolors):
                    if self.vector_to_image:
                        out_image = im_arr[:, :]
                    elif self.vector_to_rgb:
                        out_image = im_arr[:, :, b]
                    output_dtype = np.float32
                    out_image = (np.asarray(out_image,
                                            dtype=output_dtype)/255.0)
                    if self.cmap_max is None:
                        self.cmap_max = 1.0
                    if self.cmap_min is None:
                        self.cmap_min = 0.0
                    if out_image_obj is None:
                        out_image_obj = plant.PlantImage(out_image)
                    else:
                        out_image_obj.set_band(out_image,
                                               band=current_band*ncolors+b)
            image_obj = out_image_obj
            self.save_image(image_obj, self.output_file)
            return image_obj

        if self.profilex or self.profiley:
            out_image_obj = None
            output_nbands = 0
            if self.profilex:
                axis = 0
            else:
                axis = 1
            for i, current_image in enumerate(self.input_images):
                if i != 0:
                    image_obj = self.read_image(self.input_images[i])
                for current_band in range(image_obj.nbands):
                    image = image_obj.get_image(band=current_band)
                    out_image = np.nanmean(image, axis=axis)
                    if out_image_obj is None:
                        out_image_obj = plant.PlantImage(out_image)
                    else:
                        out_image_obj.set_band(out_image,
                                               band=output_nbands)
                    output_nbands += 1
            image_obj = out_image_obj
            self.save_image(image_obj, self.output_file)
            return image_obj

        if self.profile_band:
            out_image_obj = None
            output_nbands = 0
            for i, current_image in enumerate(self.input_images):
                if i != 0:
                    image_obj = self.read_image(self.input_images[i])
                image_list = []
                for current_band in range(image_obj.nbands):
                    image_list.append(
                        image_obj.get_image(band=current_band))
                    # out_image = np.nanmean(image, axis=axis)
                out_image = np.nanmean(
                    np.stack(image_list, axis=2), axis=2)
                if out_image_obj is None:
                    out_image_obj = plant.PlantImage(out_image)
                else:
                    out_image_obj.set_band(out_image,
                                           band=output_nbands)
                    output_nbands += 1
            image_obj = out_image_obj
            self.save_image(image_obj, self.output_file)
            return image_obj

        if self.shift_x or self.shift is not None:
            if self.shift_x:
                image_shift_obj = self.read_image(self.shift_x)
            else:
                # self.print('X-dimension shift: %s'
                #           % (self.shift[1]))
                image_shift_obj = self.read_image(self.shift[1])
            out_image_obj = None
            last_valid_image_shift = None
            out_image = None
            output_nbands = 0
            for i, current_image in enumerate(self.input_images):
                if i != 0:
                    image_obj = self.read_image(self.input_images[i])
                for current_band in range(image_obj.nbands):
                    if image_shift_obj.nbands > 1:
                        image_shift = image_shift_obj.get_image(
                            band=current_band)
                    else:
                        image_shift = image_shift_obj.image
                    '''
                    image_shift = image_shift_obj.get_image(
                        band=current_band)
                    '''
                    if image_shift is None and last_valid_image_shift is None:
                        self.print('ERROR invalid shift')
                        return
                    elif image_shift is None:
                        image_shift = last_valid_image_shift
                    else:
                        image_shift = image_shift.ravel()
                        last_valid_image_shift = image_shift
                    image_1 = image_obj.get_image(band=current_band)
                    if out_image is None:
                        out_image = np.zeros((image_1.shape))
                        if image_shift.shape[0] == 1:
                            self.print('X-shift: %f' % image_shift[0])
                        elif (image_shift.shape[0] != image_1.shape[0]):
                            self.print('X-shift vector length: %d'
                                       % (image_shift.shape[0]))
                            self.print('ERROR shift vector '
                                       'is not a constant or its dimension '
                                       'does not agree with X-dimension '
                                       'of: %s (%d)'
                                       % (self.input_images[0],
                                          image_1.shape[0]))
                            return
                    if image_shift.shape[0] == 1:
                        out_image = shift(image_1, [0, image_shift[0]],
                                          order=self.shift_order,
                                          mode=self.shift_mode,
                                          cval=np.nan)
                    else:
                        for i in range(1, image_shift.shape[0]):
                            out_image[i, :] = shift(image_1[i, :],
                                                    image_shift[i],
                                                    order=self.shift_order,
                                                    mode=self.shift_mode,
                                                    cval=np.nan)
                    if out_image_obj is None:
                        out_image_obj = plant.PlantImage(out_image)
                    else:
                        out_image_obj.set_band(out_image,
                                               band=output_nbands)
                    output_nbands += 1
            image_obj = out_image_obj
            self.save_image(image_obj, self.output_file)
            return image_obj

        if self.shift_y_padding:
            if self.shift_y_padding:
                image_shift_obj = plant.read_image(self.shift_y_padding)
            else:
                # self.print('Y-dimension shift: %s'
                #            % (self.shift[0]))
                image_shift_obj = self.read_image(self.shift[0])
            # plant.debug(self.out_null)
            out_image_obj = None
            last_valid_image_shift = None
            output_nbands = 0
            for i, current_image in enumerate(self.input_images):
                if i != 0:
                    image_obj = self.read_image(self.input_images[1])

                for current_band in range(image_obj.nbands):
                    # out_image = None
                    if image_shift_obj.nbands > 1:
                        image_shift = image_shift_obj.get_image(
                            band=current_band)
                    else:
                        image_shift = image_shift_obj.image
                    if image_shift is None and last_valid_image_shift is None:
                        self.print('ERROR invalid shift')
                        return
                    elif image_shift is None:
                        image_shift = last_valid_image_shift
                    else:
                        image_shift = image_shift.ravel()
                        last_valid_image_shift = image_shift
                    image_1 = image_obj.get_image(band=current_band)
                    if image_shift.shape[0] == 1:
                        self.print('Y-shift: %f' % image_shift[0])
                    elif (image_shift.shape[0] != image_1.shape[1]):
                        self.print('Y-shift vector length: %d'
                                   % (image_shift.shape[0]))
                        self.print('ERROR shift vector '
                                   'is not a constant or '
                                   'its dimension does not '
                                   'agree with Y-dimension '
                                   'of: %s (%d)'
                                   % (self.input_images[0],
                                      image_1.shape[1]))
                        return
                    min_shift = np.floor(np.nanmin(image_shift))
                    max_shift = np.ceil(np.nanmax(image_shift))
                    diff_shift = int(max_shift-min_shift)
                    out_image = np.zeros((image_1.shape[0]+diff_shift,
                                          image_1.shape[1]))
                    out_image[:] = np.nan
                    out_image[0: image_1.shape[0], :] = image_1
                    if image_shift.shape[0] == 1:
                        out_image = shift(out_image,
                                          [image_shift[0]-min_shift, 0],
                                          order=self.shift_order,
                                          mode=self.shift_mode,
                                          cval=np.nan)
                    else:
                        for j in range(1, image_shift.shape[0]):
                            out_image[:, j] = shift(out_image[:, j],
                                                    image_shift[j]-min_shift,
                                                    order=self.shift_order,
                                                    mode=self.shift_mode,
                                                    cval=np.nan)
                    if out_image_obj is None:
                        out_image_obj = plant.PlantImage(out_image)
                    else:
                        out_image_obj.set_band(out_image,
                                               band=output_nbands)
                    output_nbands += 1
            image_obj = out_image_obj
            self.save_image(image_obj, self.output_file)
            return image_obj

        if self.shift_y or self.shift is not None:
            if self.shift_y:
                image_shift_obj = self.read_image(self.shift_y)
            else:
                # self.print('Y-dimension shift: %s'
                #            % (self.shift[0]))
                image_shift_obj = self.read_image(self.shift[0])
            out_image_obj = None
            last_valid_image_shift = None
            out_image = None
            output_nbands = 0
            for i, current_image in enumerate(self.input_images):
                if i != 0:
                    image_obj = self.read_image(self.input_images[1])

                for current_band in range(image_obj.nbands):
                    '''
                    image_shift = image_shift_obj.get_image(
                        band=current_band)
                    '''
                    if image_shift_obj.nbands > 1:
                        image_shift = image_shift_obj.get_image(
                            band=current_band)
                    else:
                        image_shift = image_shift_obj.image

                    if image_shift is None and last_valid_image_shift is None:
                        self.print('ERROR invalid shift')
                        return
                    elif image_shift is None:
                        image_shift = last_valid_image_shift
                    else:
                        image_shift = image_shift.ravel()
                        last_valid_image_shift = image_shift
                    image_1 = image_obj.get_image(band=current_band)
                    if out_image is None:
                        out_image = np.zeros((image_1.shape))
                        if image_shift.shape[0] == 1:
                            self.print('Y-shift: %f' % image_shift[0])
                        elif (image_shift.shape[0] != image_1.shape[1]):
                            self.print('Y-shift vector length: %d'
                                       % (image_shift.shape[0]))
                            self.print('ERROR shift vector '
                                       'is not a constant or '
                                       'its dimension does not '
                                       'agree with Y-dimension '
                                       'of: %s (%d)'
                                       % (self.input_images[0],
                                          image_1.shape[1]))
                            return
                    if image_shift.shape[0] == 1:
                        out_image = shift(image_1,
                                          [image_shift[0], 0],
                                          order=self.shift_order,
                                          mode=self.shift_mode,
                                          cval=np.nan)
                    else:
                        for j in range(1, image_shift.shape[0]):
                            out_image[:, j] = shift(image_1[:, j],
                                                    image_shift[j],
                                                    order=self.shift_order,
                                                    mode=self.shift_mode,
                                                    cval=np.nan)
                    if out_image_obj is None:
                        out_image_obj = plant.PlantImage(out_image)
                    else:
                        out_image_obj.set_band(out_image,
                                               band=output_nbands)
                    output_nbands += 1
            image_obj = out_image_obj
            self.save_image(image_obj, self.output_file)
            return image_obj

        if self.crop_valid_box:
            # plant.display(image_obj, title='before')
            image_obj = plant.crop_valid_box_image_obj(image_obj,
                                                       null=self.in_null,
                                                       verbose=self.verbose)
            # plant.display(image_obj, title='after')
            self.save_image(image_obj, self.output_file)
            return image_obj

        if self.apply_header_to:
            file_size = stat(self.apply_header_to).st_size

            width = image_obj.width
            length = image_obj.length
            depth = image_obj.depth
            nbands = image_obj.nbands
            n_elements = width*length*depth*nbands
            print(f'*** file_size: {file_size}')
            print(f'*** n_elements: {n_elements}')
            dtype = image_obj.dtype
            dtype_size = plant.get_dtype_size(dtype)
            exp_dtype_size = file_size/n_elements
            if dtype_size != exp_dtype_size and exp_dtype_size == 1:
                dtype = np.byte
            if dtype_size != exp_dtype_size and exp_dtype_size == 2:
                dtype = np.int16
            elif (dtype_size != exp_dtype_size and
                  exp_dtype_size == 4 and
                  'int' in plant.get_dtype_name(dtype).lower()):
                dtype = np.int32
            elif dtype_size != exp_dtype_size and exp_dtype_size == 4:
                dtype = np.float32
            elif (dtype_size != exp_dtype_size and
                  exp_dtype_size == 8 and
                  'complex' not in plant.get_dtype_name(dtype).lower()):
                dtype = np.float64
            elif (dtype_size != exp_dtype_size and
                  exp_dtype_size == 8):
                dtype = np.complex64
            elif (dtype_size != exp_dtype_size and
                  exp_dtype_size == 16):
                dtype = np.complex128
            

            print(f'*** exp_dtype_size: {exp_dtype_size}')
            print(f'*** dtype_size: {dtype_size}')
            print(f'*** dtype: {dtype}')
            
            '''
                    image_2_obj = self.read_image(self.apply_header_to,
                                                  flag_exit_if_error=False,
                                                  verbose=False)
            
                    if image_2_obj is None:
            '''
            virtual_ref = ('BIN:%s:%d:%s'
                           % (self.apply_header_to,
                              width,
                              plant.get_dtype_name(dtype)))
            image_2_obj = self.read_image(virtual_ref,
                                          verbose=False)
            self.save_image(image_2_obj, self.output_file)
            return image_obj


        if (self.sub or
                self.add or
                self.mult or
                self.div or

                self.nan_sub or
                self.nan_add or
                self.nan_mult or
                self.nan_div or

                self.cover_null or
                self.data_min or
                self.data_max or
                # self.data_avg or
                self.import_null or
                self.import_geo or
                # self.apply_header_to or

                self.cross_mult or
                self.aggregate_real_imag or
                self.aggregate_polar):

            if (len(self.input_images) != 2 and
                    (self.import_geo or
                     # self.apply_header_to or
                     self.aggregate_real_imag or
                     self.aggregate_polar)):
                self.print('ERROR selected option accepts'
                           ' two inputs only')
                return
            if len(self.input_images) <= 1:
                self.print('ERROR selected option requires at least'
                           ' two inputs')
                return

            geotransform_1 = image_obj.geotransform
            '''
            if self.data_avg:
                ndata_list = []
            '''
            for i in range(1, len(self.input_images)):
                image_2_obj = self.read_image(self.input_images[i])
                geotransform_2 = image_2_obj.geotransform
                if (geotransform_1 is not None and
                        geotransform_2 is not None and
                        not self.import_geo and
                        not plant.compare_geotransforms(geotransform_1,
                                                        geotransform_2)):
                    self.print('WARNING inputs have different '
                               'geometries', 1)
                    self.print('transforming data to the geometry of '
                               'the first image..', 1)
                    image_2_obj = plant.coregister_geo(
                        self.input_images[1],
                        self.input_images[0],
                        in_null=self.in_null,
                        out_null=self.in_null,
                        verbose=self.verbose,
                        force=self.force)
                for current_band in range(max([image_obj.nbands,
                                               image_2_obj.nbands])):
                    image_1 = image_obj.get_image(
                        band=min([current_band, image_obj.nbands-1]))
                    image_2 = image_2_obj.get_image(
                        band=min([current_band, image_2_obj.nbands-1]))
                    # print(np.nansum(image_1))
                    # if i == 1:
                    #     image_1 = np.copy(image_1)
                    # if current_band > image_2_obj.nbands:
                    #     image_2 = image_2_obj.get_image(band=current_band)
                    # else:
                    #     image_2 = image_2_obj.get_image()
                    # if image_2 is None:
                    #     continue
                    if image_1.shape != (1, 1) and image_2.shape != (1, 1):
                        image_1, image_2 = plant.get_intersection(image_1,
                                                                  image_2)
                    flag_constant = False
                    if image_1.shape == (1, 1):
                        flag_constant = True
                        image_1 = float(image_1[0, 0])
                    if image_2.shape == (1, 1):
                        flag_constant = True
                        image_2 = float(image_2[0, 0])
                    if flag_constant and self.nan_sub:
                        print('WARNING not implemented NaN subtraction'
                              ' with a constant. Performing non-NaN'
                              ' operation instead')
                        self.nan_sub = False
                        self.sub = True
                    if flag_constant and self.nan_add:
                        print('WARNING not implemented NaN addition'
                              ' with a constant. Performing non-NaN'
                              ' operation instead')
                        self.nan_add = False
                        self.add = True
                    if flag_constant and self.nan_mult:
                        print('WARNING not implemented NaN multiplication'
                              ' with a constant. Performing non-NaN'
                              ' operation instead')
                        self.nan_mult = False
                        self.mult = True
                    if flag_constant and self.nan_div:
                        print('WARNING not implemented NaN division'
                              ' with a constant. Performing non-NaN'
                              ' operation instead')
                        self.nan_div = False
                        self.div = True
                    # if self.import_header:
                    #     result = np.copy(image_2)

                    if self.sub and flag_constant:
                        result = image_1 - image_2
                    elif self.sub:
                        result = np.sum([image_1, -image_2], axis=0)
                    elif self.add and flag_constant:
                        result = image_1 + image_2
                    elif self.add:
                        result = np.sum([image_1, image_2], axis=0)
                    elif self.mult and flag_constant:
                        result = image_1 * image_2
                    elif self.mult:
                        result = np.prod([image_1, image_2], axis=0)
                    elif self.cross_mult and flag_constant:
                        result = image_1 * np.conj(image_2)
                    elif self.cross_mult:
                        result = np.prod([image_1,
                                          np.conj(image_2)], axis=0)
                    elif self.div and flag_constant:
                        result = image_1 / image_2
                    elif self.div:
                        result = np.prod([image_1, 1.0/image_2], axis=0)
                    elif self.nan_sub and flag_constant:
                        raise NotImplementedError
                    elif self.nan_sub:
                        result = np.nansum([image_1, -image_2], axis=0)
                    elif self.nan_add and flag_constant:
                        raise NotImplementedError
                    elif self.nan_add:
                        result = np.nansum([image_1, image_2], axis=0)
                    elif self.nan_mult and flag_constant:
                        raise NotImplementedError
                    elif self.nan_mult:
                        result = np.nanprod([image_1, image_2], axis=0)
                    elif self.nan_div and flag_constant:
                        raise NotImplementedError
                    elif self.nan_div:
                        result = np.nanprod([image_1, 1.0/image_2], axis=0)
                    elif self.cover_null:
                        result = image_1.copy()
                        ind = np.where(plant.isnan(image_1, null=self.in_null))
                        result[ind] = image_2[ind]
                    elif self.data_min:
                        # result = np.nanmin([image_1, image_2], axis=0)
                        result = np.fmin(image_1, image_2)
                    elif self.data_max:
                        # result = np.nanmax([image_1, image_2], axis=0)
                        result = np.fmax(image_1, image_2)
                        '''
                    elif self.data_avg:
                        result = np.nansum([image_1, image_2], axis=0)
                        if len(ndata_list) <= current_band:
                            valid_image = np.asarray(plant.isvalid(
                                image_1,
                                null=self.in_null),
                                                     dtype=np.int32)
                            ndata_list.append(valid_image)
                        valid_image = plant.isvalid(image_2, null=self.in_null)
                        ind = np.where(valid_image)
                        ndata_list[current_band][ind] += 1
                        '''
                    elif self.import_null:
                        result = np.copy(image_1)
                    elif self.import_geo:
                        result = np.copy(image_1)
                        image_obj.set_geotransform(image_2_obj.geotransform)
                        image_obj.set_projection(image_2_obj.projection)
                    elif self.aggregate_real_imag:
                        result = image_1 + 1j * image_2
                    elif self.aggregate_polar:
                        result = image_1 * plant.expj(image_2)
                    else:
                        self.print('ERROR option not yet implemented')
                        return
                    # if (not self.data_min and
                    #        not self.data_max and
                    #        not self.cover_null):
                    if (self.nan_sub or self.nan_add or self.nan_mult or
                            self.nan_div):
                        ind = np.where((plant.isnan(image_1) &
                                        plant.isnan(image_2)))
                        result[ind] = np.nan

                    if (self.import_null or
                        self.import_geo or
                        # self.import_header or
                            self.aggregate_real_imag or
                            self.aggregate_polar):
                        result = plant.insert_nan(
                            result,
                            np.where(plant.isnan(image_2,
                                                 self.in_null)),
                            out_null=self.out_null)
                    # print(np.nansum(result))
                    image_obj.setImage(result,
                                       band=current_band)

                '''
                if self.data_avg:
                    for current_band in range(image_obj.nbands):
                        result = image_obj.get_image(band=current_band)
                        result /= ndata_list[current_band]
                        image_obj.setImage(result,
                                           band=current_band)
                '''

            # if self.import_header:
            #     self.save_image(image_2_obj, self.output_file)
            #     return image_2_obj
            self.save_image(image_obj, self.output_file)
            return image_obj
        if (self.concatenate_x or
                self.concatenate_y or
                self.concatenate_z or
                self.data_nan_mean or
                self.data_mean or
                self.data_nan_median or
                self.data_median):
            if self.data_nan_mean:
                z_function = np.nanmean
            elif self.data_mean:
                z_function = np.mean
            elif self.data_nan_median:
                z_function = np.nanmedian
            elif self.data_median:
                z_function = np.median
            else:
                z_function = None
            y_dim = 0
            x_dim = 0
            z_dim = 0
            nbands = 0
            for i in range(len(self.input_images)):
                current_image = self.read_image(self.input_images[i],
                                                verbose=False,
                                                only_header=True)
                if i == 0:
                    dtype = current_image.dtype
                    projection = current_image.projection
                    scheme = current_image.scheme
                    nbands = current_image.nbands
                else:
                    nbands = min([nbands, current_image.nbands])
                if self.concatenate_x:
                    x_dim += current_image.width
                    y_dim = max([current_image.length, y_dim])
                    z_dim = max([current_image.depth, z_dim])
                elif self.concatenate_y:
                    x_dim = max([current_image.width, x_dim])
                    y_dim += current_image.length
                    z_dim = max([current_image.depth, z_dim])
                elif (self.concatenate_z or
                      self.data_nan_mean or
                      z_function is not None):
                    x_dim = max([current_image.width, x_dim])
                    y_dim = max([current_image.length, y_dim])
                    z_dim += current_image.depth
            '''
            print('*** dtype: ', dtype)
            if z_dim <= 1:
                new_image = np.zeros((y_dim, x_dim),
                                     dtype=dtype)
            else:
                new_image = np.zeros((z_dim, y_dim, x_dim),
                                     dtype=dtype)
            '''
            new_image = None
            new_image_obj = plant.PlantImage(length=y_dim,
                                             width=x_dim,
                                             depth=z_dim,
                                             nbands=nbands,
                                             dtype=dtype,
                                             projection=projection,
                                             scheme=scheme)
            for current_band in range(nbands):
                last_index = 0
                if z_dim <= 1:
                    new_image = np.zeros((y_dim, x_dim),
                                         dtype=dtype)
                else:
                    new_image = np.zeros((z_dim, y_dim, x_dim),
                                         dtype=dtype)
                for i in range(len(self.input_images)):
                    current_image = self.read_image(self.input_images[i],
                                                    band=current_band)
                    if self.concatenate_x:
                        new_image[..., 0:current_image.length,
                                  last_index:last_index +
                                  current_image.width] = current_image.image
                        last_index += current_image.width
                    elif self.concatenate_y:
                        new_image[..., last_index:last_index +
                                  current_image.length,
                                  0:current_image.width] = current_image.image
                        last_index += current_image.length
                    elif (self.concatenate_z or
                          self.data_nan_mean or
                          z_function is not None):
                        new_image[last_index:last_index +
                                  current_image.length,
                                  0:current_image.length,
                                  0:current_image.width] = current_image.image
                        last_index += current_image.depth
                new_image_obj.set_image(new_image, band=current_band)
            if z_function is not None:
                new_image_obj.geotransform = image_obj.geotransform
                new_image_obj.projection = image_obj.projection
                new_image_obj.gcp_list = image_obj.gcp_list
                new_image_obj.gcp_projection = image_obj.gcp_projection
                for current_band in range(nbands):
                    image = new_image_obj.get_image(band=current_band)
                    if 'complex' in plant.get_dtype_name(image).lower():
                        image_re = z_function(np.real(image), axis=0)
                        image_imag = z_function(np.imag(image), axis=0)
                        image = image_re + 1j*image_imag
                    else:
                        image = z_function(image, axis=0)
                    new_image_obj.set_image(image, band=current_band)
            shape_str = ', '.join([str(x) for x in new_image_obj.image.shape])
            if nbands == 1:
                self.print(f'output shape: {shape_str}')
            else:
                self.print(f'output shape: {shape_str}'
                           f' ({nbands} bands)')
            self.save_image(new_image_obj, self.output_file)
            return new_image_obj

        if self.equation is not None:
            self.equation = self.equation.lower()
            self.equation = self.equation.replace(', ', ' ')
            self.equation = self.equation.replace(': ', ' ')
            # self.equation = self.equation.replace('x', 'a')
            alphabet_list = list(string.ascii_lowercase)
            ret = plant.get_function(self.equation)
            flag_invert_equation = ret is None and '_inv' in self.equation
            if flag_invert_equation:
                ret = plant.get_function(self.equation.replace('_inv', ''))
            if ret is not None:
                func, n_coefficients, bounds, initial_points = ret
                args = self.equation.split(' ')[1:]
                args_float = [float(x) for x in args if x != '']
            else:
                flag_invert_equation = None
                func = None
                print('equation: '+self.equation)
                if 'a' not in self.equation and 'x' in self.equation:
                    self.equation = self.equation.replace('x', 'a')

            for current_band in range(image_obj.nbands):
                args = []
                equation_dict = {}
                for i, current_image in enumerate(self.input_images):
                    if current_band != 0:
                        band_str = ' (band: '+str(current_band)+')'
                    else:
                        band_str = ''
                    if func is None:
                        print('variable '+str(alphabet_list[i])+': ' +
                              current_image+band_str)
                    equation_dict[str(alphabet_list[i])] = \
                        self.read_image(current_image,
                                        band=current_band).image
                    args += [equation_dict[str(alphabet_list[i])]]
                if func is None:
                    # image = eval(self.equation, equation_dict)
                    import numexpr as ne
                    # print('*** self.equation:', self.equation)
                    # print('*** dict: ', equation_dict)
                    image = ne.evaluate(self.equation,
                                        local_dict=equation_dict)
                elif flag_invert_equation:
                    from scipy import optimize
                    solver = optimize.fsolve
                    image = np.copy(args[0])
                    with plant.PrintProgress(
                            image_obj.length,
                            sub_total=image_obj.width) as pp:
                        for line in range(image_obj.length):
                            for column in range(image_obj.width):
                                pp.print_progress(line, column)
                                current_value = image[line, column]
                                if plant.isnan(current_value):
                                    continue
                                image[line, column] = \
                                    solver(lambda x:
                                           (func(x, *args_float) -
                                            current_value),
                                           0.0)
                else:
                    args = args + args_float
                    equation_str = plant.get_function_equation(
                        self.equation, args_float)
                    print('equation: '+equation_str)
                    image = func(*args)
                image_obj.setImage(image,
                                   band=current_band)
            self.save_image(image_obj, self.output_file)
            return image_obj

        if self.create_zeros:
            if len(self.input_images) != 2:
                self.print('ERROR this option requires exacly 2 inputs')
                return
            length = image_obj.image
            if length.shape != (1, 1) and length.shape != (1, 1, 1):
                self.print('ERROR first input should be a single number')
                return
            length = int(length[0, 0])
            image_2_obj = self.read_image(self.input_images[1])
            width = image_2_obj.image
            if width.shape != (1, 1) and width.shape != (1, 1, 1):
                self.print('ERROR second input should be a single number')
                return
            width = int(width[0, 0])
            image = np.zeros((length, width))
            image_obj.set_image(image)
            self.save_image(image_obj,
                            self.output_file)
            return image_obj

        if self.separate_real_imag:
            out_image_obj = image_obj.copy()
            for current_band in range(image_obj.nbands):
                if image_obj.nbands == 1:
                    bands_str = ''
                else:
                    bands_str = f' of band {current_band}'
                image = image_obj.get_image(band=current_band)
                out_image_obj.set_image(
                    np.real(image),
                    band=current_band*2,
                    name=f'Real part{bands_str}')
                out_image_obj.set_image(
                    np.imag(image),
                    band=current_band*2+1,
                    name=f'Imaginary part{bands_str}')
            self.save_image(out_image_obj, self.output_file)
            return out_image_obj

        if self.separate_polar:
            out_image_obj = image_obj.copy()
            for current_band in range(image_obj.nbands):
                if image_obj.nbands == 1:
                    bands_str = ''
                else:
                    bands_str = f' of band {current_band}'
                image = image_obj.get_image(band=current_band)
                out_image_obj.set_image(
                    np.absolute(image),
                    band=current_band*2,
                    name=f'Magnitude{bands_str}')
                out_image_obj.set_image(
                    np.angle(image),
                    band=current_band*2+1,
                    name=f'Phase (rad){bands_str}')
            self.save_image(out_image_obj, self.output_file)
            return out_image_obj
 
        if (self.spectral_norm or
                self.spectral_norm_x or
                self.spectral_norm_y):
            kwargs = {}
            if self.spectral_norm_y:
                kwargs['axis'] = 0
                fft_func = np.fft.fft
                ifft_func = np.fft.ifft
            elif self.spectral_norm_x:
                kwargs['axis'] = 1
                fft_func = np.fft.fft
                ifft_func = np.fft.ifft
            else:
                fft_func = np.fft.fft2
                ifft_func = np.fft.ifft2
            out_image_obj = image_obj.copy()
            for current_band in range(image_obj.nbands):
                image = image_obj.get_image(band=current_band)
                image_fft = fft_func(image, **kwargs)
                image_fft = self._normalize_spectrum(image_fft)
                image = ifft_func(image_fft, **kwargs)
                out_image_obj.set_image(
                    image,
                    band=current_band)
            self.save_image(out_image_obj, self.output_file)
            return out_image_obj

        if (self.spectral_split_x or
                self.spectral_split_y or
                self.fft_split_x or
                self.fft_split_y):
            if self.spectral_split_y:
                axis = 0
                n_channels = self.spectral_split_y
            elif self.fft_split_y:
                axis = 0
                n_channels = self.fft_split_y
            elif self.spectral_split_x:
                axis = 1
                n_channels = self.spectral_split_x
            else:
                axis = 1
                n_channels = self.fft_split_x
            out_image_obj = image_obj.copy()
            for current_band in range(image_obj.nbands):
                if image_obj.nbands == 1:
                    bands_str = ''
                else:
                    bands_str = f' of band {current_band}'
                image = image_obj.get_image(band=current_band)
                image_fft = np.fft.fft(image, axis=axis)
                image_fft = np.fft.fftshift(image_fft, axes=(axis))
                for c in range(n_channels):
                    if FLAG_SPLIT_SPECTRUM_NORMALIZE:
                        image_fft = self._normalize_spectrum(image_fft)

                    if self.spectral_split_y or self.fft_split_y:
                        image_n_fft = image_fft[
                            c*(image_fft.shape[0]//n_channels):
                            (c+1)*(image_fft.shape[0]//n_channels), :]
                    else:
                        image_n_fft = image_fft[
                            :, c*(image_fft.shape[1]//n_channels):
                            (c+1)*(image_fft.shape[1]//n_channels)]
                    if self.spectral_split_x or self.spectral_split_y:
                        image_n_fft = np.fft.ifftshift(image_n_fft, axes=(axis))
                        image_n = np.fft.ifft(image_n_fft, axis=axis)
                    else:
                        image_n = image_n_fft
                    out_image_obj.set_image(
                        image_n,
                        band=current_band*n_channels+c,
                        name=f'Spectral channel {c+1}{bands_str}')
            self.save_image(out_image_obj, self.output_file)
            return out_image_obj

        if self.clip:
            for current_band in range(image_obj.nbands):
                image = np.copy(image_obj.get_image(band=current_band))
                if self.clip:
                    if plant.isvalid(self.clip[0]):
                        ind = np.where(image < self.clip[0])
                        image[ind] = self.clip[0]
                    if plant.isvalid(self.clip[1]):
                        ind = np.where(image > self.clip[1])
                        image[ind] = self.clip[1]
                    image_obj.set_image(image,
                                        band=current_band)
            self.save_image(image_obj,
                            self.output_file,
                            flag_scale_data=not self.to_rgb)
            return image_obj

        if self.to_rgb and (len(self.input_images) > 1 or
                            image_obj.nbands > 1):
            band_count = image_obj.nbands
            for i, current_file in enumerate(self.input_images):
                if i == 0:
                    continue
                image_2_obj = self.read_image(current_file)
                for b in range(image_2_obj.nbands):
                    image_obj.set_image(image_2_obj.get_image(band=b),
                                        band=band_count)
                    band_count += 1
            if image_obj.nbands == 2:
                image_obj.set_image(image_obj.get_image(band=0),
                                    band=2)
            self.save_image(image_obj, self.output_file)
            return image_obj

        if self.slide_compare:
            if (len(self.input_images) != 2 and
                    len(self.input_images) != 3):
                self.print('ERROR this option requires exacly 2 or 3'
                           ' inputs')
                return
            args = [self, image_obj, self.input_images[1]]
            if len(self.input_images) == 3:
                args.append(self.input_images[2])
            plant.save_html_slide_compare(
                *args,
                output_file=self.output_file)
            return self.output_file

        if (self.hillshade or
                self.to_rgb):
            ret_dict = plant.image_obj_expand_rgb(
                image_obj,
                flag_to_rgb=self.to_rgb,
                flag_hillshade=self.hillshade,
                background_color=self.background_color,
                cmap=self.cmap,
                cmap_min=self.cmap_min,
                cmap_max=self.cmap_max,
                pixel_size_x=self.pixel_size_x,
                pixel_size_y=self.pixel_size_y,
                percentile=self.percentile,
                verbose=self.verbose)
            image_obj = ret_dict['image_obj']
            # ctable = ret_dict['ctable']
            cmap = ret_dict['cmap']
            if self.hillshade:
                cmap_min = ret_dict['cmap_min_orig']
                cmap_max = ret_dict['cmap_max_orig']
            else:
                cmap_min = ret_dict['cmap_min']
                cmap_max = ret_dict['cmap_max']
            self.save_image(image_obj,
                            self.output_file,
                            # ctable=ctable,
                            cmap=cmap,
                            cmap_min=cmap_min,
                            cmap_max=cmap_max,
                            flag_cmap_already_expanded=True,
                            flag_scale_data=(not self.to_rgb and
                                             not self.hillshade))
            return image_obj

        '''
        if self.phase_wrap:
            for current_band in range(image_obj.nbands):
                image = np.copy(image_obj.get_image(band=current_band))
                image = plant.wrap_phase(image)
                image_obj.setImage(image,
                                   band=current_band)
            self.save_image(image_obj, self.output_file,
                            out_null=self.out_null)
            return image_obj
        '''
        if (self.rotate_90 or
                self.rotate_180 or
                self.rotate_270):
            kwargs = {}
            if self.rotate_90:
                func = np.rot90
                kwargs['k'] = 1
            elif self.rotate_180:
                func = np.rot90
                kwargs['k'] = 2
            elif self.rotate_270:
                func = np.rot90
                kwargs['k'] = 3
            out_image_obj = None
            for current_band in range(image_obj.nbands):
                image = np.copy(image_obj.get_image(band=current_band))
                out_image = func(image, **kwargs)
                if out_image_obj is None:
                    out_image_obj = plant.PlantImage(out_image)
                else:
                    out_image_obj.set_band(out_image,
                                           band=current_band)
            self.save_image(out_image_obj, self.output_file)
            return out_image_obj

        # if (self.integrate_x or
        #         self.integrate_y):
        
        if (self.derivative_x or
                self.derivative_y or
                self.derivative_phase_x or
                self.derivative_phase_y or
                self.integrate_x or
                self.integrate_y or
                self.unwrap_x or
                # self.unwrap_y or
                self.normalize_x or
                self.normalize_y or
                self.normalize or
                self.normalize_x_mean or
                self.normalize_y_mean or
                self.normalize_mean or
                self.normalize_x_max or
                self.normalize_y_max or
                self.normalize_max or
                self.normalize_x_sum or
                self.normalize_y_sum or
                self.normalize_sum or
                self.slope):
            for current_band in range(image_obj.nbands):
                image = np.copy(image_obj.get_image(band=current_band))
                if (self.derivative_x or
                        self.derivative_phase_x or
                        self.unwrap_x or
                        self.integrate_x or
                        self.slope):
                    if self.pixel_size_x is None:
                        print('WARNING considering unitary pixel_size_x')
                        self.pixel_size_x = 1
                    if self.derivative_x or self.slope:
                        result_x = plant.get_image_derivative_x(
                            image, self.pixel_size_x)
                    elif self.derivative_phase_x or self.unwrap_x:
                        result_x = plant.get_image_derivative_phase_x(
                            image, self.pixel_size_x)
                        if self.unwrap_x:
                            # offset = np.nanmean(image[:, 0:3], axis=1)
                            # offset = np.nanmean(result_x, axis=1)
                            # offset = image[:, 0]
                            # invalid_ind = np.where(np.logical_or(
                            #     result_x > 1.5, result_x < -1.5))
                            # result_x[invalid_ind] = np.nan
                            result_x = plant.get_image_integration_x(
                                result_x, self.pixel_size_x)
                            #    offset=offset)
                            diff = np.angle(plant.expj(image) *
                                            np.conj(plant.expj(result_x)))
                            offset = np.nanmedian(diff, axis=1)
                            for i in range(offset.size):
                                result_x[i, :] = result_x[i, :] + offset[i]
                            jumps = plant.get_phase_jumps(image-result_x)
                            for i in range(result_x.shape[0]-1):
                                offset = np.nanmedian(jumps[i+1, :] -
                                                      jumps[i, :])
                                jumps[i+1, :] = jumps[i+1, :] - offset
                            jumps = plant.filter_data(jumps,
                                                      median=[9, 1])
                            for i in range(result_x.shape[0]-1):
                                offset = np.nanmedian(jumps[i+1, :] -
                                                      jumps[i, :])
                                jumps[i+1, :] = jumps[i+1, :] - offset

                            result_x = image - 2*np.pi*jumps
                            '''
                            diff = image - result_x
                            offset = np.nanmean(diff, axis=1)
                            for i in range(offset.size):
                                result_x[i, :] = result_x[i, :] + offset[i]
                            '''
                            # result_x = image+diff_2pi
                        # offset = 0
                    else:
                        '''
                        result_x = plant.get_image_derivative_x(
                            image, self.pixel_size_x)
                        '''
                        result_x = plant.get_image_integration_x(
                            image, self.pixel_size_x)
                if (self.derivative_y or
                        self.derivative_phase_y or
                        # self.unwrap_y or
                        self.integrate_y or
                        self.slope):
                    if self.pixel_size_y is None:
                        print('WARNING considering unitary pixel_size_y')
                        self.pixel_size_y = 1
                    if self.derivative_y or self.slope:
                        result_y = plant.get_image_derivative_y(
                            image, self.pixel_size_y)
                    elif self.derivative_phase_y:
                        result_y = plant.get_image_derivative_phase_y(
                            image, self.pixel_size_y)
                    else:
                        result_y = plant.get_image_integration_y(
                            image, self.pixel_size_y)
                vmin = 0
                vmean = None
                if (self.derivative_x or
                        self.derivative_phase_x or
                        self.integrate_x or
                        self.unwrap_x):
                    image = result_x
                elif (self.derivative_y or
                      self.derivative_phase_y or
                      self.integrate_y):
                    image = result_y
                elif self.slope:
                    image = np.sqrt(result_x**2+result_y**2)
                elif (self.normalize_x or
                      self.normalize_x_mean or
                      self.normalize_x_max or
                      self.normalize_x_sum):
                    for i in range(image.shape[0]):
                        if (self.normalize_x or
                                self.normalize_x_max or
                                self.normalize_x_mean):
                            vmax = np.nanmax(image[i, :])
                        elif self.normalize_x_sum:
                            vmax = np.nansum(image[i, :])
                        if (self.normalize_x or
                                self.normalize_x_mean):
                            vmin = np.nanmin(image[i, :])
                        if self.normalize_x_mean:
                            vmean = np.nanmean(image[i, :])
                        else:
                            vmean = vmin
                        image[i, :] = plant.scale_data(image[i, :],
                                                       vmin,
                                                       vmax,
                                                       vmean=vmean)
                elif (self.normalize_y or
                      self.normalize_y_mean or
                      self.normalize_y_max or
                      self.normalize_y_sum):
                    for j in range(image.shape[1]):
                        if (self.normalize_y or
                                self.normalize_y_max or
                                self.normalize_y_mean):
                            vmax = np.nanmax(image[:, j])
                        elif self.normalize_y_sum:
                            vmax = np.nansum(image[:, j])
                        if (self.normalize_y or
                                self.normalize_y_mean):
                            vmin = np.nanmin(image[:, j])
                        if self.normalize_y_mean:
                            vmean = np.nanmean(image[:, j])
                        else:
                            vmean = None
                            # vmean = vmin
                        image[:, j] = plant.scale_data(image[:, j],
                                                       vmin,
                                                       vmax,
                                                       vmean=vmean)
                elif (self.normalize or
                      self.normalize_mean or
                      self.normalize_max or
                      self.normalize_sum):
                    if (self.normalize or
                            self.normalize_max or
                            self.normalize_mean):
                        vmax = np.nanmax(image)
                    elif self.normalize_sum:
                        vmax = np.nansum(image)
                    if (self.normalize or
                            self.normalize_mean):
                        vmin = np.nanmin(image)
                    if self.normalize_mean:
                        vmean = np.nanmean(image)
                    else:
                        vmean = None
                        # vmean = vmin
                    image = plant.scale_data(image,
                                             vmin,
                                             vmax,
                                             vmean=vmean)
                image_obj.setImage(image,
                                   band=current_band)
            self.save_image(image_obj, self.output_file,
                            out_null=self.out_null)
            return image_obj

        if self.set_bbox or self.set_bbox_outer:
            # image_obj = self.read_image(self.input_images[0])
            geotransform = image_obj.geotransform
            self.print('input geotransform:')
            if geotransform is not None and self.verbose:
                plant.print_geoinformation(geotransform=geotransform)
            self.print('output geotransform:')

            if self.set_bbox_outer:
                step_lat = ((self.set_bbox_outer[1] -
                             self.set_bbox_outer[0]) /
                            image_obj.width)
                step_lon = ((self.set_bbox_outer[3] -
                             self.set_bbox_outer[2]) /
                            image_obj.length)
                self.set_bbox = [self.set_bbox_outer[0]-step_lat/2,
                                 self.set_bbox_outer[1]+step_lat/2,
                                 self.set_bbox_outer[2]-step_lon/2,
                                 self.set_bbox_outer[3]+step_lon/2]
            geotransform = plant.get_geotransform(bbox=self.set_bbox,
                                                  lat_size=image_obj.length,
                                                  lon_size=image_obj.width)
            if self.verbose:
                plant.print_geoinformation(geotransform=geotransform)
            image_obj._geotransform = geotransform
            self.save_image(image_obj, self.output_file)
            return image_obj

        if self.georef_shift:
            # image_obj = self.read_image(self.input_images[0])
            geotransform = plant.geotransform_shift(
                image_obj.geotransform,
                self.georef_shift[0],
                self.georef_shift[1],
                lat_size=image_obj.length(),
                lon_size=image_obj.width())
            image_obj.setGeotransform(geotransform)
            self.save_image(image_obj, self.output_file)
            return image_obj

        # otherwise (just copy):
        # if (len(self.input_images) > 1 and
        #       (self.join or self.flag_copy)):
        if image_obj.image_loaded:
            out_image_obj = image_obj
            band_count = image_obj.nbands
        else:
            out_image_obj = plant.new_image_obj_like(image_obj)
            band_count = 0
        flag_skip_first_image = image_obj.image_loaded
        for current_input in range(len(self.input_images)):
            if current_input == 0 and flag_skip_first_image:
                continue
            current_image_obj = \
                self.read_image(self.input_images[current_input])
            for current_band in range(current_image_obj.nbands):
                band = current_image_obj.getBand(band=current_band)
                out_image_obj.setBand(band, band=band_count)
                band_count += 1
        self.save_image(out_image_obj, self.output_file)
        return out_image_obj


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantUtil(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
