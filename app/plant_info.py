#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import numpy as np
import plant


def get_parser():
    '''
    Command line parser.
    '''
    descr = ('Print information about the input data, including image'
             ' dimensions, file format, data type, geo-coordinates and'
             ' projection (if available). The --stats mode prints'
             ' and returns statistics of the inputs. Specific statistics'
             ' can also be obtained by using the parameters: '
             ' --min, --mean, --max, --stddev.')

    epilog = ''
    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            input_images=2,
                            default_input_options=1,
                            default_flags=1,
                            null=1,
                            output_dir=1,
                            separate=1,
                            topo_dir=1,
                            output_file=1,
                            default_output_options=1,
                            n_points=1,
                            sampling_step=1)

    # group = parser.add_mutually_exclusive_group()
    parser.add_argument('--only-geo', '--geo'
                        '--only-geoinformation',
                        dest='only_geoinformation',
                        action='store_true',
                        help='Show only geoinformation')

    parser.add_argument('--all',
                        dest='show_all',
                        action='store_true',
                        help='Show all information (except stats)')

    parser.add_argument('--stats',
                        '--statistics',
                        dest='data_stats',
                        action='store_true',
                        help='Show/return data statistics')
    parser.add_argument('--mean', dest='data_mean',
                        action='store_true',
                        help='Show/return data mean')
    # parser.add_argument('--median', dest='data_median',
    #                    action='store_true',
    #                    help='Show/return data median')
    parser.add_argument('--stddev', dest='data_stddev',
                        action='store_true',
                        help='Show/return data stddev')
    parser.add_argument('--min', dest='data_min',
                        action='store_true',
                        help='Show/return data min')
    parser.add_argument('--max', dest='data_max',
                        action='store_true',
                        help='Show/return data max')

    parser_approx_stats = parser.add_mutually_exclusive_group()

    parser_approx_stats.add_argument('--approx',
                                     '--approx-stats',
                                     '--fast',
                                     dest='approx_stats',
                                     action='store_true',
                                     help='Values rounded '
                                     'for faster computation')
    parser_approx_stats.add_argument('--no-approx',
                                     '--no-approx-stats',
                                     '--no-fast',
                                     '--not-fast',
                                     '--exact',
                                     '--precise',
                                     dest='approx_stats',
                                     action='store_false',
                                     help='Avoid rounding values')

    parser.add_argument('--lat',
                        type=str,
                        dest='lat',
                        help='Latitude value')
    parser.add_argument('--lon',
                        type=str,
                        dest='lon',
                        help='Longitude value')
    parser.add_argument('-y', '--y-pos', '--pos-y',
                        type=str,
                        dest='y_pos',
                        help='Y-index')
    parser.add_argument('-x', '--x-pos', '--pos-x',
                        type=str,
                        dest='x_pos',
                        help='X-index')

    parser.add_argument('--footprint',
                        '--footprint_size',
                        dest='footprint_m',
                        type=float,
                        required=False,
                        help='Footprint size in meters')
    return parser


class PlantInfo(plant.PlantScript):

    def __init__(self, parser, argv=None):
        '''
        class initialization
        '''
        super().__init__(parser, argv)
        self.populate_parameters()

    def populate_parameters(self):
        """
        populate attributes
        """
        if (self.approx_stats is None and (self.data_stats or
                                           self.data_mean or
                                           # self.data_median or
                                           self.data_stddev or
                                           self.data_min or
                                           self.data_max)):
            self.approx_stats = False
        elif self.approx_stats is None:
            self.approx_stats = True

    def run(self):
        '''
        Run info retrieve method
        '''
        # data_vect = np.zeros((len(self.input_images))) 
        # output_file_vect = []
        flag_get_point = (self.lat is not None or
                          self.lon is not None or
                          self.y_pos is not None or
                          self.x_pos is not None)
        if flag_get_point:
            image = None
        else:
            data_vect = []
        for i, current_file in enumerate(self.input_images):
            # self.print('current file: %s' % current_file)
            if flag_get_point:
                image_obj = self.read_image(current_file)
                ret = plant.get_point(image_obj,
                                      lat=self.lat,
                                      lon=self.lon,
                                      x_pos=self.x_pos,
                                      y_pos=self.y_pos,
                                      topo_dir=self.topo_dir,
                                      footprint_m=self.footprint_m,
                                      verbose=self.verbose)
                if ret is not None:
                    # data, y_pos, x_pos = ret
                    # if data is None:
                    #     data = np.nan
                    data = ret['data']
                    # data = data.tolist()
                    if image is None:
                        image = data
                    else:
                        image = np.concatenate((image, data), axis=1)
                    # data_vect.append(data)
                    # output_file_vect.append(current_file)
            else:
                data = self.get_info(current_file)
                if data is None:
                    data = [np.nan]
                # elif not isinstance(data, list):
                #     data = [data]
                data_vect.extend(data)
                # output_file_vect.append(current_file)

        '''
        if self.output_file and len(data_vect) != 0:
            self.save_vector(data_vect_obj,
                             self.output_file,
                             id_vect=output_file_vect,
                             str_id='file')
        '''
        # if flag_get_point:
        #     image = np.transpose(image)
        if not flag_get_point:
            image = plant.shape_image(data_vect)
            # if self.data_stats:
            #    image = np.transpose(image)
        if not self.data_stats:
            data_vect_obj = plant.PlantImage(image)
        else:
            data_vect_obj = plant.PlantImage(image[:, 0])
            name_list = ['mean', 'stddev', 'minimum', 'maximum']
            for b in range(image.shape[1]):
                if b != 0:
                    data_vect_obj.set_image(image[:, b], band=b)
                data_vect_obj.get_band(band=b).name = name_list[b]
        if self.output_file:
            self.save_image(data_vect_obj, self.output_file)
        return data_vect_obj


def main(argv=None):
    ret = plant.run_script(PlantInfo, get_parser(), argv)
    return ret


if __name__ == '__main__':
    main()
