#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import sys
from os import remove, path, makedirs
import numpy as np
import copy
import time
import shutil
import plant

# from ctypes import cdll  # , c_char_p, c_float  # , c_int
# azslope: should be look_angle

DEFAULT_EXT = '.cal'
DEFAULT_SIM_EXT = '.sim'
# FLAG_FAST = True


def get_parser():
    '''
    Command line parser.
    '''

    descr = ''
    epilog = ''

    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            input_images=1,
                            # pickle_dir=1,
                            topo_dir=1,
                            output_file=1,
                            output_dir=1,
                            separate=1,
                            default_options=1,
                            pixel_size=1)

    parser.add_argument('-a', '--action',
                        dest='action',
                        type=str,
                        help='list of radiometric corrections'
                        ' to perform. Options: abs, terrain, polcal, fr, '
                        'azslope (default: "%(default)s")',
                        default='terrain')

    parser.add_argument('--terrain', '--terrain-type',
                        '--rtc',
                        dest='terrain_correction_type',
                        type=str,
                        help="type of radiometric terrain correction: "
                        "'beta_naught', "
                        "'sigma-naught'"
                        "'sigma-naught-norlim', "
                        "'sigma-naught-ahmed', "
                        "'sigma-naught-ulander', "
                        "'gamma-naught-norlim', "
                        "'gamma-naught', "
                        "'gamma-naught-ulander' "
                        "(default: %(default)s)",
                        default='gamma-naught-ulander')

    parser.add_argument('--inc',
                        dest='input_inc_file',
                        type=str,
                        help='_incidence angle (single-band'
                        ' file)')
    parser.add_argument('--lia',
                        dest='input_lia_file',
                        type=str,
                        help='Local incidence angle (single'
                        '-band file)',
                        )
    parser.add_argument('--psi',
                        dest='input_psi_file',
                        type=str,
                        help='Local incidence angle (single'
                        '-band file)')

    parser.add_argument('--dem',
                        dest='dem_file',
                        type=str,
                        help='Reference DEM (z.rdr) file')
    parser.add_argument('--mask',
                        dest='mask_file',
                        type=str,
                        help='Layover/shadow mask file'
                        ' (mask.rdr) file')

    # outputs
    parser.add_argument('--az-slope',
                        dest='in_azslope_angle',
                        type=str,
                        help='Input save az slope angle (only '
                        'for azslope mode)')
    parser.add_argument('--out-az-slope',
                        dest='out_azslope_angle',
                        type=str,
                        help='Save az slope angle (only for '
                        'azslope mode)')
    parser.add_argument('--out-lia',
                        dest='out_lia',
                        type=str,
                        help='Save estimated lia angle. ')
    parser.add_argument('--out-psi',
                        dest='out_psi',
                        type=str,
                        help='Save estimated psi angle. ')

    # parameters
    parser.add_argument('-c', '--calibration-factor-db',
                        dest='calibration_factor_db',
                        type=float,
                        help='Calibration factor')

    parser.add_argument('--faraday-rotation-deg', '--fr-deg', '--fr',
                        dest='fr_angle',
                        type=float,
                        help='Faraday rotation angle (in '
                        'degrees)',
                        default=np.nan)

    parser.add_argument('--method',
                        dest='method',
                        type=float,
                        help='Local inc. angle method. '
                        'Options: 1 - calculate from DEM Lee200; 2 - '
                        'calculate from DEM test; 3 - Read from file'
                        "(default: %(default)s)",
                        default=3)

    parser.add_argument('--input-radiometry',
                        dest='input_radiometry',
                        type=str,
                        help='Input data radiometry. Options:'
                        'beta, sigma-inc (or sigma-ellipsoid), sigma-lia')

    # flags
    parser.add_argument('--sim', '--simulate-backscatter', '--simamp',
                        '--sim-amp', '--simulate',
                        dest='simulate',
                        action='store_true',
                        help='Simulate backscatter '
                        '(no input backscatter image is needed).')

    parser.add_argument('--line-by-line',
                        dest='fast',
                        action='store_false',
                        help='Process line by line')

    '''
    parser.add_argument('--calculate-psi',
                        dest='calculate_psi',
                        action='store_true',
                        help='calculate_psi')
    '''

    parser.add_argument('-b', '--out-db', '--out-dB', dest='output_in_dB',
                        action='store_true',
                        help='Output in dB')

    # parser.add_argument('--out-intensity',
    #                    dest='out_intensity',
    #                    action='store_true',
    #                     help='Output image is intensity',
    #                    default=False)
    # parser.add_argument('--in-intensity', '--input-intensity',
    #                    dest='in_intensity',
    #                    action='store_true',
    #                     help='Input image are '
    # power/intensity '
    #                    'proportional',
    #                    )

    in_intensity_group = parser.add_mutually_exclusive_group(required=False)
    in_intensity_group.add_argument('--in-intensity', '--in-int',
                                    dest='in_intensity',
                                    action='store_true',
                                    help='Input image is intensity')
    in_intensity_group.add_argument('--in-amplitude', '--in-amp',
                                    dest='in_amplitude',
                                    action='store_true',
                                    help='Input image is amplitude')

    # out_intensity_group = parser.add_mutually_exclusive_group(required=False)
    # out_intensity_group.add_argument('--out-intensity', '--out-int',
    #                                 dest='out_intensity',
    #                                 action='store_true',
    #                                 help='Output image is intensity')
    # out_intensity_group.add_argument('--out-amplitude', '--out-amp',
    #                                 dest='out_amplitude',
    #                                 action='store_true',
    #                                 help='Output image is amplitude')

    parser.add_argument('--out-magnitude',
                        dest='out_magnitude',
                        action='store_true',
                        help='Output image saved as real')
    return parser


class PlantRadiometricCorrection(plant.PlantScript):

    def __init__(self, parser, argv=None):
        '''
        class initialization
        '''
        super().__init__(parser, argv)
        self.image_obj = None
        self.geotransform = None
        self.dt = None
        self.length = None
        self.width = None
        self.scheme = None
        self.phase = 0
        self.hh_file_orig = ''
        self.hv_file_orig = ''
        self.vh_file_orig = ''
        self.vv_file_orig = ''
        self.receive = None
        self.transmit = None
        self.convert_inc_to_rad = False
        self.convert_lia_to_rad = False
        self.convert_psi_to_rad = False
        self.step_abs = False
        self.step_terrain_correction = False
        self.step_polcal = False
        self.step_fr = False
        self.step_azslope = False
        self.step_area = False
        self.input_inc = None
        self.input_lia = None
        self.input_psi = None
        self.input_dem = None
        # self.input_mask = None
        self.mask_file_str = ''

    def run(self):

        if self.separate:

            self_dict = {}
            prevent_copy_list = ['parser']
            for key, value in self.__dict__.items():
                if key in prevent_copy_list:
                    continue
                self_dict[key] = copy.deepcopy(value)

            input_images = self.input_images
            print('input_images:', input_images)
            print('output_files:', self.output_files)

            ret_list = []
            for i, current_file in enumerate(input_images):
                if i != 0:
                    plant.plant_config.logger_obj.flush_temporary_files()
                    self.__dict__.update(self_dict)
                self.input_images = [current_file]
                self.output_file = self.output_files[i]
                self.populate_parameters()
                self.loop_non_polarimetric_channels()
                ret = self.realize_changes()
                ret_list.append(ret)
            # plant.plant_config.logger_obj.flush_temporary_files()
            # self.__dict__.update(self_dict)
            return ret_list

        self.populate_parameters()
        self.loop_polarimetric_channels()
        self.loop_non_polarimetric_channels()
        ret = self.realize_changes()
        return ret

    def populate_parameters(self):
        '''
        populate class parameters
        '''
        '''
        if self.output_in_dB:
            self.out_intensity = True
        elif not self.out_intensity and self.out_amplitude:
            self.out_intensity = False
        elif not self.out_intensity and not self.out_amplitude:
            self.out_intensity = None  # default
        '''

        if not self.in_intensity and self.in_amplitude:
            self.in_intensity = False
        elif not self.in_intensity and not self.in_amplitude:
            self.in_intensity = None  # default

        # if single-pol:
        # self.print('')
        # if len(self.input_images) == 1 and len(self.identified_inputs) == 0:
        #    self.hh_file = self.input_images[0]
        #    if not plant.isfile(self.hh_file):
        #        self.print('ERROR file not found: '+self.hh_file)
        #        sys.exit(1)
        #    self.print('    %s' % self.hh_file)
        #    self.identified_inputs = [self.hh_file]

        # dual or full-pol:
        # elif len(self.input_images) > 1:
        #     ret = search_pol(self.input_images, sym=False, vh_to_hv=False,
        #                      verbose=True, mode='')
        #     self.hh_file = ret['hh_file']
        #     self.hv_file = ret['hv_file']
        #     self.vh_file = ret['vh_file']
        #     if self.hv_file == self.vh_file:
        #         self.vh_file = None
        #     self.vv_file = ret['vv_file']
        #     identified_inputs = [self.hh_file, self.hv_file,
        #                          self.vh_file, self.vv_file]
        #     identified_inputs = [f for f in identified_inputs if f]

        if not self.output_file:
            self.print('ERROR invalid output filename')
        # no input backscatter:
        if not self.simulate and not self.master_file:
            self.parser.print_help()
            self.print('EXIT please provide an input image')
            return
            # sys.exit(1)

        # saving original file names
        if not self.simulate:
            self.hh_file_orig = self.hh_file
            self.hv_file_orig = self.hv_file
            self.vh_file_orig = self.vh_file
            self.vv_file_orig = self.vv_file
        # self.input_images_orig = self.input_images
        self.current_images = self.input_images[:]

        if not self.simulate:
            self.image_obj = self.read_image(self.master_file,
                                             only_header=1)
            # print('*** ', self.master_file)
            # print(self.image_obj.width)
            # print(self.image_obj.length)
            # plant.imshow(self.image_obj)

            self.width = self.image_obj.width
            self.length = self.image_obj.length
            #  self.geotransform = image_obj.geotransform
            # self.projetion = image_obj.projection
            # self.scheme = image_obj.scheme

            if not self.output_ext:
                if '.' in self.master_file:
                    self.output_ext = '.'+(self.master_file.split('.'))[-1]
                else:
                    self.output_ext = DEFAULT_EXT
        else:
            self.output_ext = DEFAULT_SIM_EXT
            self.identified_inputs = []

        # number of valid polarimetric channels:
        self.number_of_files = len(self.identified_inputs)
        self.print(f'number of input files (pol): {self.number_of_files}')
        self.print(f'number of input files (total):'
                   f' {len(self.current_images)}')
        self.print('')
        self.print('selected steps (action): '+self.action)
        self.print('')
        self.step_abs = 'abs' in self.action or 'area' in self.action
        self.step_terrain_correction = 'terrain' in self.action
        self.step_polcal = 'polcal' in self.action
        self.step_azslope = 'azslope' in self.action
        self.step_fr = 'fr' in self.action or plant.isvalid(self.fr_angle)

        # if self.calibration_factor_db is not None:
        #    self.calibration_factor_db = self.calibration_factor_db
        # else:
        #    self.calibration_factor_db = \
        #        plant.get_parameter_from_pickle('calibrationFactor_dB',
        #                                        self.pickle_dir)
        self.step_area = self.step_abs and self.calibration_factor_db is None

        if self.topo_dir is None and len(self.input_images) >= 1:
            self.topo_dir = plant.get_common_directory(self.input_images)
        elif self.topo_dir is None:
            self.topo_dir = '.'
        '''
        if not self.pickle_dir:
            self.pickle_dir = self.topo_dir
        else:
            if not path.isdir(self.pickle_dir):
                self.pickle_dir = path.dirname(self.pickle_dir)
        '''

        # absolute radiometric correction parameters
        if self.step_abs:
            self.print('verifying inputs for absolute radiometric '
                       'correction...')
            if self.step_area:
                if self.pixel_size_az is None or self.pixel_size_rg is None:
                    self.print('ERROR pixel sizes could not be '
                               'determined for radiometric area correction. ')
                    return
                else:
                    self.pixel_area = self.pixel_size_az*self.pixel_size_rg
                    self.print('image intensity will be divided by pixel-'
                               'area (%f[m^2])...' % (self.pixel_area))
            elif self.calibration_factor_db is None:
                self.print('ERROR The calibration factor is'
                           ' necessary for absolute radiometric'
                           ' correction.')
                return

        # terrain correction parameters
        if self.step_terrain_correction:
            self.print('verifying inputs for terrain correction (mode %s)...'
                       % (self.terrain_correction_type))
            self.terrain_correction_type = \
                self.terrain_correction_type.replace('_',
                                                     '-')
            if self.terrain_correction_type == 'gama_naught':
                self.terrain_correction_type = 'gamma-naught-ulander'
            if (self.terrain_correction_type not in
                ['sigma-naught',
                 'sigma-naught-norlim'
                 'sigma-naught-norlim',
                 'gamma-naught-norlim',
                 'sigma-naught-ahmed',
                 'sigma-naught-ulander',
                 'gamma-naught-ulander']):
                self.print('ERROR terrain_correction_type not '
                           'recognized: %s'
                           % self.terrain_correction_type)

            # input radiometry
            if self.input_radiometry is None:
                self.print('WARNING the parameter --input-radiometry'
                           ' is not set. Considering input as'
                           ' beta/beta-naught')
                self.input_radiometry = 'beta'
                # sys.exit(1)
            else:
                if 'beta' in self.input_radiometry.lower():
                    self.input_radiometry = 'beta'
                elif ('sigma' in self.input_radiometry.lower() and
                      ('inc' in self.input_radiometry.lower() or
                       'ell' in self.input_radiometry.lower())):
                    self.input_radiometry = 'sigma-inc'
                elif 'sigma' in self.input_radiometry.lower():
                    self.input_radiometry = 'sigma-lia'
                else:
                    self.print('ERROR not recognized input radiometry: '
                               f' {self.input_radiometry}')
                    return
                self.print('INFO input radiometry:'
                           f' {self.input_radiometry}')

            # verifying file names according to the arguments

            self._check_inc_dict = None

            # inc (ellipsoid inc. angle)
            if not self.input_inc_file:
                # print('*** self.input_inc_file (before):',
                #       self.input_inc_file)
                self.input_inc_file = path.join(self.topo_dir, 'los.rdr:0')
                if not plant.isfile(self.input_inc_file):
                    ret = self._check_inc_file(self.topo_dir)
                    if ret is not None and 'inc' in ret.keys():
                        self.input_inc_file = ret['inc']
                # print('*** self.input_inc_file (after):',
                #       self.input_inc_file)

            # lia (local-inc. angle)
            # if self.method >= 3 and
            if not self.input_lia_file:
                self.input_lia_file = path.join(self.topo_dir,
                                                'localInc.rdr')
                if not plant.isfile(self.input_lia_file):
                    self.input_lia_file = path.join(self.topo_dir,
                                                    'incLocal.rdr')
                if not plant.isfile(self.input_lia_file):
                    ret = self._check_inc_file(self.topo_dir)
                    if ret is not None and 'lia' in ret.keys():
                        self.input_lia_file = ret['lia']
                if not plant.isfile(self.input_lia_file):
                    self.input_lia_file = None

            # psi (projection angle)
            # print('*** self.input_psi_file (before):',
            #       self.input_psi_file)
            if not self.input_psi_file:
                self.input_psi_file = path.join(self.topo_dir,
                                                'localPsi.rdr')
                if not plant.isfile(self.input_psi_file):
                    ret = self._check_inc_file(self.topo_dir)
                    if ret is not None and 'psi' in ret.keys():
                        self.input_psi_file = ret['psi']
                if not plant.isfile(self.input_psi_file):
                    self.input_psi_file = None
            # print('*** self.input_psi_file (after):',
            #       self.input_psi_file)
            '''
            if (not self.input_psi_file and not self.calculate_psi and
                    plant.isfile(path.join(self.topo_dir, 'inc.rdr'))):
                self.input_psi_file = path.join(self.topo_dir, 'inc.rdr:0')
            elif (not self.input_psi_file and not self.calculate_psi and
                    plant.isfile(path.join(self.topo_dir, 'incLocal.rdr'))):
                self.input_psi_file = path.join(self.topo_dir, 'incLocal.rdr:0')
            '''

            if not self.mask_file:
                self.mask_file = path.join(self.topo_dir, 'mask.rdr')
            if not self.dem_file:
                self.dem_file = path.join(self.topo_dir, 'z.rdr')

            # verifying necessary files for abs correction
            if self.method < 3:
                flag_open_input_dem = (self.terrain_correction_type in
                                       ['sigma-naught-norlim',
                                        'gamma-naught-norlim',
                                        'sigma-naught-ahmed',
                                        'sigma-naught-ulander',
                                        'gamma-naught-ulander'])
                '''
                elif self.calculate_psi:
                flag_open_input_dem = (self.terrain_correction_type in
                                       ['sigma-naught-ahmed',
                                        'sigma-naught-ulander',
                                        'gamma-naught-ulander'])
                '''
            else:
                flag_open_input_dem = False
            flag_open_input_psi = (self.terrain_correction_type in
                                   ['sigma-naught-ahmed',
                                    'sigma-naught-ulander',
                                    'gamma-naught-ulander'])
            if ('sigma-inc' in self.input_radiometry or
                    'sigma-ellipsoid' in self.input_radiometry or
                    # self.calculate_psi or
                    self.method < 3):
                flag_open_input_inc = (self.terrain_correction_type in
                                       ['sigma-naught',
                                        'gamma-naught',
                                        'sigma-naught-norlim',
                                        'gamma-naught-norlim',
                                        'sigma-naught-ahmed',
                                        'sigma-naught-ulander',
                                        'gamma-naught-ulander'])
            else:
                flag_open_input_inc = (self.terrain_correction_type in
                                       ['sigma-naught',
                                        'gamma-naught'
                                        'sigma-naught-ahmed'])
            flag_open_input_lia = ('sigma-lia' in self.input_radiometry or
                                   self.terrain_correction_type in
                                   ['sigma-naught-norlim',
                                    'gamma-naught-norlim',
                                    'sigma-naught-ahmed',
                                    'sigma-naught-ulander',
                                    'gamma-naught-ulander'])

            self.print('Files needed for selected terrain correction (%s):'
                       % self.terrain_correction_type)
            with plant.PlantIndent():
                if flag_open_input_dem:
                    self.print(f'reference DEM: {self.dem_file}')
                if flag_open_input_inc:
                    self.print(f'incidence angle: {self.input_inc_file}')
                if flag_open_input_lia:
                    self.print(f'local-incidence angle: {self.input_lia_file}')
                if flag_open_input_psi:
                    self.print(f'projection angle: {self.input_psi_file}')

            # verifying files existence
            # input_inc_file_str = self.input_inc_file
            # input_lia_file_str = self.input_lia_file
            # if self.method >= 3:
            #     self.input_lia_file = self.input_inc_file
            if (not plant.isfile(self.input_inc_file) and
                    flag_open_input_inc):
                self.print(f'ERROR file not found: {self.input_inc_file}')
                return
                # sys.exit(1)
            dem_file_str = self.dem_file
            if (not plant.isfile(dem_file_str) and
                    flag_open_input_dem):
                self.print(f'ERROR file not found: {dem_file_str}')
                return
                # sys.exit(1)

            if (self.method >= 3 and
                not plant.isfile(self.input_lia_file) and
                    flag_open_input_lia):
                self.print(f'ERROR File not found: {self.input_lia_file}')
                return
                # sys.exit(1)

            self.mask_file_str = self.mask_file

            # opening necessary files
            if flag_open_input_inc:
                self.input_inc = self.read_file(self.input_inc_file)
                # if np.max(np.absolute(self.input_inc)) > 2*np.pi:
                if plant.is_deg(self.input_inc):
                    self.print('inc. angle will be converted to radians...')
                    self.convert_inc_to_rad = True
                    # self.input_inc = np.radians(self.input_inc)
                else:
                    self.print('inc. angle is given in radians...')
            if flag_open_input_dem:
                self.input_dem = self.read_file(dem_file_str)
            if flag_open_input_lia and self.input_lia_file:
                self.input_lia = self.read_file(self.input_lia_file)
                # if np.max(np.absolute(self.input_lia)) > 2*np.pi:
                if plant.is_deg(self.input_lia):
                    self.print('local-inc. angle will be converted to'
                               ' radians...')
                    self.convert_lia_to_rad = True
                    # self.print('converting lia angle to radians...')
                    # self.input_lia = np.radians(self.input_lia)
                else:
                    self.print('local-inc. angle psi is given in radians...')
            elif flag_open_input_lia:
                if self.pixel_size_az is None or self.pixel_size_rg is None:
                    self.populate_pixel_sizes(self.pixel_size_az,
                                              self.pixel_size_rg)
                self.input_lia = plant.get_local_inc_angle(
                    self.input_dem,
                    self.input_inc,
                    self.pixel_size_az,
                    self.pixel_size_rg,
                    method=self.method,
                    degrees=self.convert_inc_to_rad)
            if self.out_lia:
                self.save_image(self.convert_lia(self.input_lia), self.out_lia)

            if (flag_open_input_psi and self.input_psi_file and
                    plant.isfile(self.input_psi_file)):
                self.input_psi = self.read_file(self.input_psi_file)
                # try:
                #    self.input_psi = self.read_file(self.input_psi_file,
                #                                   band=1)
                # except:
                #    self.input_psi = self.read_file(self.input_psi_file,
                #                                   band=0)
                # if np.max(np.absolute(self.input_psi)) > 2*np.pi:
                # if (np.nanmax(self.input_psi) > 2*np.pi or
                #        np.nanmin(self.input_psi) < -2*np.pi):
                #    self.print('Converting _psi angle to radians...')
                # self.input_psi = np.radians(self.input_psi)
            elif flag_open_input_psi:
                self.print('ERROR the projection angle (psi) is'
                           ' required for the selected radiometric'
                           ' terrain correction (RTC).')
                return
            '''
            elif flag_open_input_psi:
                if self.pixel_size_az is None and self.pixel_size_rg is None:
                    self.populate_pixel_sizes(self.pixel_size_az,
                                              self.pixel_size_rg)
                if self.out__psi:
                    psi_file_temp = self.out_psi
                else:
                    psi_file_temp = 'psi.temp'+str(int(self.start_time))

                plant.get_psi_angle(self.input_dem,
                                    self.input_inc,
                                    self.input_lia,
                                    self.pixel_size_az,
                                    self.pixel_size_rg,
                                    degrees_inc=self.convert_inc_to_rad,
                                    degrees_lia=self.convert_lia_to_rad,
                                    out_file=psi_file_temp)
                self.input_psi = self.read_file(psi_file_temp)

                if not self.out_psi:
                    if plant.isfile(psi_file_temp):
                        remove(psi_file_temp)
                    if plant.isfile(psi_file_temp+'.vrt'):
                        remove(psi_file_temp+'.vrt')
                    if plant.isfile(psi_file_temp+'.xml'):
                        remove(psi_file_temp+'.xml')
            '''
            if flag_open_input_psi:
                # if np.max(np.absolute(self.input_psi)) > 2*np.pi:
                if plant.is_deg(self.input_psi):
                    self.print('Projection angle psi will be converted to'
                               ' radians...')
                    self.convert_psi_to_rad = True
                else:
                    self.print('Projection angle psi is given in radians...')

            if self.out_psi and flag_open_input_psi and self.input_psi_file:
                self.save_image(self.input_psi, self.out_psi)

            # self.input_mask = None
            # if plant.isfile(self.mask_file_str):
            #    self.input_mask = self.read_file(self.mask_file_str, band=0)

        # Verifying Faraday rotation correction inputs
        # self.fr_angle = 0
        if self.step_fr:
            # self.print('verifying inputs for Faraday Rotation correction...')
            if np.isnan(self.fr_angle):
                self.print('ERROR the Faraday Rotation angle is'
                           ' necessary for Faraday Rotation'
                           ' correction.')
                # self.step_fr = False
                return
            else:
                self.fr_angle = float(self.fr_angle)

        # Verifying polarimetric correction inputs
        if self.step_polcal:
            self.print('verifying inputs for polarimetric correction...')
        if self.step_polcal and self.number_of_files != 4:
            self.print('ERROR all polarimetric channels are necessary'
                       ' for polarimetric correction (polcal).')
            return
            # self.step_polcal = False
        elif self.step_polcal:
            '''
            ret = plant.get_polcal_transmit_and_receive_from_pickle(
                self.pickle_dir)
            if ret is None:
                self.transmit = None
                self.receive = None
            else:
            '''
            self.transmit = None
            self.receive = None
            if self.transmit is None or self.receive is None:
                # self.print('WARNING it was not possible to unpack: ' +
                #            path.join(self.pickle_dir, 'data'))
                self.print('ERROR The polarimetric constants are necessary'
                           ' for polarimetric correction (polcal).')
                # self.step_polcal = False
                return

        # Verifying azimuth slope correction inputs
        if self.step_azslope:
            self.print('verifying inputs for azimuth slope correction...')
        if self.step_azslope and self.number_of_files != 4:
            self.print('ERROR all polarimetric channels are'
                       ' necessary for azimuth slope correction (azslope).')
            return
        elif self.step_azslope:
            if self.pixel_size_az is None or self.pixel_size_rg is None:
                self.populate_pixel_sizes(self.pixel_size_az,
                                          self.pixel_size_rg)

            if self.input_inc is None:
                if not self.input_inc_file:
                    self.input_inc_file = path.join(self.topo_dir, 'los.rdr')
                self.input_inc_file = self.input_inc_file
                if not plant.isfile(self.input_inc_file):
                    self.print('ERROR File not found: ' +
                               self.input_inc_file)
                    # sys.exit(1)
                    return
                self.input_inc = self.read_file(self.input_inc_file)
                # if np.max(np.absolute(self.input_inc)) > 2*np.pi:
                if plant.is_deg(self.input_inc):
                    self.print('inc. angle will be converted to radians...')
                    self.convert_inc_to_rad = True
                    # self.input_inc = np.radians(self.input_inc)

            if self.input_dem is None:
                if not self.dem_file:
                    self.dem_file = path.join(self.topo_dir, 'z.rdr')
                dem_file_str = self.dem_file
                if not plant.isfile(dem_file_str):
                    self.print('ERROR File not found: ' +
                               dem_file_str)
                    # sys.exit(1)
                    return
                self.input_dem = self.read_file(dem_file_str)

            if self.pixel_size_az is None or self.pixel_size_rg is None:
                self.print('WARNING Pixel sizes could not be '
                           'determined. ')
            elif self.pixel_size_az is None:
                self.print('WARNING Pixel size in azimuth could not be '
                           'determined. ')
            elif self.pixel_size_rg is None:
                self.print('WARNING Pixel size in range could not be '
                           'determined. ')
            if self.pixel_size_az is None or self.pixel_size_rg is None:
                plant.prompt_continue('Pixel sizes are necessary for'
                                      ' azimuth slope correction.'
                                      ' Do you want to continue without'
                                      ' performing this correction?'
                                      ' ([y]es/[n]o) ', force=self.force)
                self.step_azslope = False

        # if self.out_intensity is None:
        #     self.out_intensity = self.in_intensity
        self.print('')
        self.print('radiometric correction steps:')
        if self.step_polcal:
            self.print('    - polarimetric correction.')
        if self.step_fr:
            self.print('    - Faraday Rotation correction.')
        if self.step_azslope:
            self.print('    - azimuth slope correction.')
        if self.step_abs:
            self.print('    - absolute radiometric correction.')
        if self.step_terrain_correction:
            self.print('    - terrain correction.')

    def _check_inc_file(self, topo_dir):
        if self._check_inc_dict is not None:
            return self._check_inc_dict
        self._check_inc_dict = {}
        ext_list = ['.rdr', '.bin']
        inc_name_list = ['inc', 'localInc', 'incLocal']
        for ext in ext_list:
            for inc_name in inc_name_list:
                inc_file = path.join(topo_dir, inc_name+ext)
                if not path.isfile(inc_file):
                    continue
                inc_obj = self.read_image(inc_file, verbose=False)
                # single-band
                if inc_name == 'inc' and inc_obj is not None and inc_obj.nbands == 1:
                    self._check_inc_dict['inc'] = inc_file
                elif inc_name == 'localInc' and inc_obj is not None and inc_obj.nbands == 1:
                    self._check_inc_dict['lia'] = inc_file
                elif inc_name == 'incLocal' and inc_obj is not None and inc_obj.nbands == 1:
                    self._check_inc_dict['lia'] = inc_file

                # 2 bands
                elif inc_obj is not None and inc_obj.nbands == 2:
                    self._check_inc_dict['psi'] = inc_file+':0'
                    self._check_inc_dict['lia'] = inc_file+':1'
                if ('inc' in self._check_inc_dict and
                    'lia' in self._check_inc_dict and
                        'psi' in self._check_inc_dict):
                    break
        return self._check_inc_dict

    def convert_inc(self, input_data):
        '''
        convert inc angle to radians
        '''
        if self.convert_inc_to_rad:
            return np.radians(input_data)
        return input_data

    def convert_lia(self, input_data):
        if self.convert_lia_to_rad:
            return np.radians(input_data)
        return input_data

    def convert_psi(self, input_data):
        '''
        convert psi angle to radians
        '''
        if (input_data.shape[0] != self.length or
                input_data.shape[1] != self.width):
            print('*** cropping angle...', input_data.shape)
            input_data = plant.copy_shape_from_tuple((self.length, self.width),
                                                     input_data)
            print('*** new shape: ', input_data.shape)
        if self.convert_psi_to_rad:
            return np.radians(input_data)
        return input_data

    def populate_pixel_sizes(self, args_pixel_size_az, args_pixel_size_rg):
        self.pixel_size_az = args_pixel_size_az
        self.pixel_size_rg = args_pixel_size_rg
        if self.pixel_size_az:
            self.print('azimuth pixel size: ' +
                       str(self.pixel_size_az))
        else:
            self.print('ERROR azimuth pixel size could not be determined.')
            return
        if self.pixel_size_rg:
            self.print('range pixel size: ' +
                       str(self.pixel_size_rg))
        else:
            self.print('ERROR range pixel size could not be determined.')
            return

    def read_file(self, input_images):
        '''
        reads an ISCE product and return it with the same shape as the main
        input file.
        '''
        image_obj = self.read_image(input_images,
                                    # band=band,
                                    verbose=self.verbose)
        # if image_obj is None:
        #    self.print('ERROR opening file: '+input_images)
        #    sys.exit(1)
        if self.image_obj is None:
            self.image_obj = image_obj
        if self.width is None:
            self.width = image_obj.width
        if self.length is None:
            self.length = image_obj.length
        if image_obj.width != self.width or image_obj.length != self.length:
            self.print('input data shape: %d x %d ' % (self.length,
                                                       self.width))
            self.print(input_images+' shape: %d x %d '
                       % (image_obj.length,
                          image_obj.width))
            if (abs(image_obj.width - self.width) > 1 or
                    abs(image_obj.length - self.length) > 1):
                self.print('ERROR input image(s) and '+input_images +
                           f' dimensions do not match ({image_obj.shape})'
                           f' ({self.length}, {self.width}).')
                return

            self.print('WARNING input image(s) and '+input_images+' dimensions'
                       ' do not match. Resizing input image...')
            image = plant.copy_shape_from_tuple((self.length, self.width),
                                                image_obj.image)
            image_obj.set_image(image)
        return image_obj.image

    def apply_abs_and_terrain_correction(self, current_file,
                                         output_file=None):
        '''
        main class that performs radiometric correction
        '''
        if not output_file:
            output_file = self.get_filename(current_file, temp=True)
        if not plant.isfile(current_file) and not self.simulate:
            self.print('WARNING File not found: ' + current_file)
            return -1

        self.print('')

        out_ret = plant.overwrite_file_check(output_file, force=self.force)
        if not out_ret:
            self.print('operation cancelled.')
            sys.exit(0)

        if self.step_terrain_correction and self.step_abs:
            self.print('applying radiometric terrain and absolute '
                       'corrections (mode: ' +
                       self.terrain_correction_type+').')
            self.print('calibration factor [dB]: ' +
                       str(self.calibration_factor_db))
        elif self.step_terrain_correction:
            self.print('applying radiometric terrain correction (mode: ' +
                       self.terrain_correction_type+').')
        elif self.step_abs:
            self.print('applying absolute radiometric correction...')
            self.print('calibration factor [dB]: ' +
                       str(self.calibration_factor_db))

        if not self.simulate:
            # read images and parameters
            image_obj = self.read_image(current_file, verbose=self.verbose)
            # if image_obj.nbands > 1:
            #     self.print('ERROR not implemented for multiband files')
            #     return
            if self.in_intensity is None:
                self.in_intensity = 'complex' in plant.get_dtype_name(
                    image_obj.dtype).lower()
                
            self.dt = image_obj.dtype
            # image_orig = image_obj.image
            if self.output_in_dB or self.out_magnitude:
                self.dt = np.float32
            band_range = image_obj.nbands
        else:
            self.dt = np.float32
            band_range = 1
            if self.in_intensity is None:
                self.in_intensity = True

        if not self.in_intensity:
            print('input image as amplitude')

        image = None

        # one block
        if self.fast is None or self.fast:
            try:
                if not self.simulate:
                    image_tc_obj = image_obj.soft_copy()
                for b in range(band_range):
                    if not self.simulate:
                        image = image_obj.get_image(band=b)
                        # print('*** mean:', np.nanmean(image))
                    image_tc = self.run_terrain_correction(image,
                                                           inc=self.input_inc,
                                                           lia=self.input_lia,
                                                           psi=self.input_psi)
                    if not self.simulate:
                        image_tc_obj.set_image(image_tc)
                if self.simulate:
                    self.save_image(image_tc, output_file)
                else:
                    self.save_image(image_tc_obj, output_file)
                return
            except MemoryError:
                image = None

        # line by line
        with open(output_file, 'w') as out_file:
            for b in range(band_range):
                print(f'*** band (2): {b}')
                if not self.simulate:
                    image_orig = image_obj.get_image(band=b)
                for i in range(self.length):
                    if not self.simulate:
                        image = np.copy(image_orig[i, :])
                    if self.input_mask is not None:
                        image = plant.insert_nan(
                            image,
                            np.where(self.input_mask[i, :] != 1),
                            out_null=np.nan)
                    if self.input_inc is not None:
                        inc = self.input_inc[i, :]
                    if self.input_lia is not None:
                        lia = self.input_lia[i, :]
                    if self.input_psi is not None:
                        psi = self.input_psi[i, :]
                    self.run_terrain_correction(image,
                                                out_file=out_file,
                                                inc=inc,
                                                lia=lia,
                                                psi=psi)
        if self.image_obj.nbands > 1:
            scheme = 'BSQ'
            self.image_obj.scheme = scheme
        else:
            scheme = None
        plant.create_isce_header(output_file,
                                 scheme=scheme,
                                 image_obj=self.image_obj)
        self.save_image(self.image_obj, output_file, save_header_only=True)

    def run_terrain_correction(self, image, out_file=None,
                               inc=None, lia=None, psi=None):

        if not self.step_terrain_correction:
            image_tc = self.save_to_file(image, out_file)
            return image_tc
        # remove inc. compensation
        if ('sigma-inc' in self.input_radiometry or
                'sigma-ellipsoid' in self.input_radiometry):
            factor = 1.0/np.sin(self.convert_inc(inc))
        elif 'sigma-lia' in self.input_radiometry:
            factor = 1.0/np.sin(self.convert_lia(lia))
        elif image is not None:
            factor = np.ones_like(image, dtype=np.float32)
        elif inc is not None:
            factor = np.ones_like(inc, dtype=np.float32)
        elif lia is not None:
            factor = np.ones_like(lia, dtype=np.float32)
        elif psi is not None:
            factor = np.ones_like(psi, dtype=np.float32)

        if (factor.shape[0] != self.length or
                factor.shape[1] != self.width):
            factor = plant.copy_shape_from_tuple((self.length, self.width),
                                                 factor)
        if self.terrain_correction_type == 'beta_naught':
            image_tc = self.save_to_file(image, out_file, factor=factor)
            return image_tc
        elif self.terrain_correction_type == 'sigma-naught':
            factor *= np.sin(self.convert_inc(inc))
            image_tc = self.save_to_file(image, out_file, factor=factor)
        elif self.terrain_correction_type == 'sigma-naught-norlim':
            factor *= np.sin(self.convert_lia(lia))
            image_tc = self.save_to_file(image, out_file, factor=factor)
        elif self.terrain_correction_type == 'sigma-naught-ahmed':
            factor *= np.cos(self.convert_inc(inc))
            factor *= np.absolute(np.cos(self.convert_psi(psi)))
            factor /= np.cos(self.convert_lia(lia))
            image_tc = self.save_to_file(image, out_file, factor=factor)
        elif self.terrain_correction_type == 'sigma-naught-ulander':
            factor *= np.absolute(np.cos(self.convert_psi(psi)))
            image_tc = self.save_to_file(image, out_file, factor=factor)
        elif self.terrain_correction_type == 'gamma-naught':
            factor *= np.tan(self.convert_inc(inc))
            # factor /= np.cos(self.convert_inc(inc))
            image_tc = self.save_to_file(image, out_file, factor=factor)
        elif self.terrain_correction_type == 'gamma-naught-norlim':
            factor *= np.tan(self.convert_lia(lia))
            image_tc = self.save_to_file(image, out_file, factor=factor)
        elif self.terrain_correction_type == 'gamma-naught-ulander':
            factor *= np.absolute(np.cos(self.convert_psi(psi)))
            factor /= np.cos(self.convert_lia(lia))
            image_tc = self.save_to_file(image, out_file, factor=factor)
        return image_tc

    def save_to_file(self, image, out_file, factor=None):
        '''
        save image following ISCE standards.
        if image was complex, restore phase values
        '''
        # if self.step_terrain_correction and not self.in_intensity:
        #     image = np.sqrt(image)

        # self.dt = image.dtype.name
        if factor is not None:
            if not self.in_intensity:
                # with np.errstate(invalid='ignore'):
                factor = np.sqrt(np.absolute(factor))
            if image is not None:
                image = image*factor
            else:
                image = 1.0/factor
        elif image is None:
            self.print('ERROR no input image or no terrain correction'
                       ' selected')
            # sys.exit(1)
            return

        # apply absolute correction
        if self.step_abs:
            if self.step_area:
                image = image/self.pixel_area
            if self.output_in_dB:
                indexes = np.where(image == 0)
                image = plant.insert_nan(image,
                                         indexes,
                                         out_null=np.nan)
                # image[indexes] = np.nan
                if self.in_intensity:
                    image = (10*np.log10(image)+self.calibration_factor_db)
                else:
                    image = (20*np.log10(image)+self.calibration_factor_db)
                # self.dt = np.float32
            elif self.in_intensity:
                image *= 10**(self.calibration_factor_db/10.0)
            else:
                image *= 10**(self.calibration_factor_db/20.0)
        elif self.output_in_dB:
            if self.in_intensity:
                image = 10*np.log10(image)
            else:
                image = 20*np.log10(image)

        if (not self.in_intensity and not self.output_in_dB):
            image = image**2
        '''
        elif (not self.out_intensity and self.in_intensity and
                not self.output_in_dB):
            image = np.sqrt(np.absolute(image))*np.angle(image)
        '''

        if self.out_magnitude:
            image = np.absolute(image)
            # self.dt = image.dtype.name

        image = np.asarray(image, dtype=self.dt)
        if out_file is None:
            return image
        image.tofile(out_file)

        # save_image(image, output_file, verbose=self.verbose)

    # def loopAzSlopeCorrection(self):
    #     """
    #     apply polarimetic azimuth orientation correction.
    #
    #     """
    #     self.print('')
    #     self.print('applying azimuth terrain orientation correction...')

    #     # get output names
    #     hhOut_file = self.get_filename(self.hh_file, temp=True)
    #     hvOut_file = self.get_filename(self.hv_file, temp=True)
    #     vhOut_file = self.get_filename(self.vh_file, temp=True)
    #     vvOut_file = self.get_filename(self.vv_file, temp=True)
    #     self.print('')
    #     self.print('temporary files:')
    #     self.print('%s' % hhOut_file)
    #     self.print('%s' % hvOut_file)
    #     self.print('%s' % vhOut_file)
    #     self.print('%s' % vvOut_file)

    #     # calculate azimuth orientation
    #     azslope_angle = self.in_azslope_angle
    #     if azslope_angle is '' or azslope_angle is None:
    #         azslope_angle = plant.get_azslope_angle(self.input_dem,
    #                                           self.convert_inc(self.input_inc),
    #                                           self.pixel_size_az,
    #                                           self.pixel_size_rg)

    #     if self.out_azslope_angle:
    #         save_image(azslope_angle, self.out_azslope_angle,
    #                    verbose=self.verbose, force=self.force)

    #     # read images
    #     self.print('')

    #     if self.method == 4:
    #         # should be wrong
    #         self.print('WARNING azslope angle is inverted.')
    #         Shh, Shv, Svh, Svv = rotate_matrix(self.hh_file,
    #                                            self.hv_file,
    #                                            self.vh_file,
    #                                            self.vv_file,
    #                                            azslope_angle,
    #                                            hhOut_file,
    #                                            hvOut_file,
    #                                            vhOut_file,
    #                                            vvOut_file,
    #                                            verbose=self.verbose)
    #     else:
    #         Shh, Shv, Svh, Svv = rotate_matrix(self.hh_file,
    #                                            self.hv_file,
    #                                            self.vh_file,
    #                                            self.vv_file,
    #                                            azslope_angle,
    #                                            hhOut_file,
    #                                            hvOut_file,
    #                                            vhOut_file,
    #                                            vvOut_file,
    #                                            verbose=self.verbose,
    #                                            inverse_rotation=True)

    #     # save results
    #     # self.print('')
    #     # save_image(Shh, hhOut_file, verbose=self.verbose)
    #     # save_image(Shv, hvOut_file, verbose=self.verbose)
    #     # save_image(Svh, vhOut_file, verbose=self.verbose)
    #     # save_image(Svv, vvOut_file, verbose=self.verbose)
    #     self.hh_file = hhOut_file
    #     self.hv_file = hvOut_file
    #     self.vh_file = vhOut_file
    #     self.vv_file = vvOut_file

    def apply_polarimetric_correction(self, transmit=None, receive=None):

        """
        apply polarimetic calibration.

        @param transmit (\a isceobj.Sensor.Polarimetry.Distortion)
        The transmission distortion parameters
        @param receive (\a isceobj.Sensor.Polarimetry.Distortion)
        The reception distortion parameters
        """
        from polcal import polcal_py

        # determine ISSI library (issi.so) location
        # lib = cdll.LoadLibrary(os.path.dirname(__file__) + '/issi.so')

        # lib = cdll.LoadLibrary(path.join(os.environ['ISCE_HOME'],
        #                                 'components', 'contrib',
        #                                 'ISSI', 'issi.so'))
        # lib = cdll.LoadLibrary(path.join('/home/shiroma/tasks/isce/bin/isce',
        #                                 'components', 'contrib',
        #                                 'ISSI', 'issi.so'))
        # lib = cdll.LoadLibrary(path.join('/home/shiroma/dev/plant',
        #                                  'radiometric_correction.so'))

        # get output names
        hhOut_file = self.get_filename(self.hh_file, temp=True)
        hvOut_file = self.get_filename(self.hv_file, temp=True)
        vhOut_file = self.get_filename(self.vh_file, temp=True)
        vvOut_file = self.get_filename(self.vv_file, temp=True)
        self.print('')
        if self.step_polcal:
            self.print('applying polarimetric correction...')
        if self.step_fr:
            self.print('applying Faraday rotation correction...')
        if self.step_azslope:
            self.print('applying azimuth terrain orientation correction...')
        self.print('')
        self.print('temporary files:')
        self.print('%s' % hhOut_file)
        self.print('%s' % hvOut_file)
        self.print('%s' % vhOut_file)
        self.print('%s' % vvOut_file)

        # prepare parameters
        # hh_file_c = path.abspath(self.hh_file).encode("utf-8")
        # hv_file_c = path.abspath(self.hv_file).encode("utf-8")
        # vh_file_c = path.abspath(self.vh_file).encode("utf-8")
        # vv_file_c = path.abspath(self.vv_file).encode("utf-8")
        # hhOut_file_c = path.abspath(hhOut_file).encode("utf-8")
        # hvOut_file_c = path.abspath(hvOut_file).encode("utf-8")
        # vhOut_file_c = path.abspath(vhOut_file).encode("utf-8")
        # vvOut_file_c = path.abspath(vvOut_file).encode("utf-8")
        hh_file_c = path.abspath(self.hh_file)
        hv_file_c = path.abspath(self.hv_file)
        vh_file_c = path.abspath(self.vh_file)
        vv_file_c = path.abspath(self.vv_file)
        hhOut_file_c = path.abspath(hhOut_file)
        hvOut_file_c = path.abspath(hvOut_file)
        vhOut_file_c = path.abspath(vhOut_file)
        vvOut_file_c = path.abspath(vvOut_file)

        if self.step_polcal:
            # Unpack the transmit and receive distortion matrices
            transmitCrossTalk1Real_c = transmit.getCrossTalk1().real
            transmitCrossTalk1Imag_c = transmit.getCrossTalk1().imag
            transmitCrossTalk2Real_c = transmit.getCrossTalk2().real
            transmitCrossTalk2Imag_c = transmit.getCrossTalk2().imag
            transmitChannelImbalanceReal_c = \
                transmit.getChannelImbalance().real
            transmitChannelImbalanceImag_c = \
                transmit.getChannelImbalance().imag
            receiveCrossTalk1Real_c = receive.getCrossTalk1().real
            receiveCrossTalk1Imag_c = receive.getCrossTalk1().imag
            receiveCrossTalk2Real_c = receive.getCrossTalk2().real
            receiveCrossTalk2Imag_c = receive.getCrossTalk2().imag
            receiveChannelImbalanceReal_c = \
                receive.getChannelImbalance().real
            receiveChannelImbalanceImag_c = \
                receive.getChannelImbalance().imag
        else:
            transmitCrossTalk1Real_c = 0
            transmitCrossTalk1Imag_c = 0
            transmitCrossTalk2Real_c = 0
            transmitCrossTalk2Imag_c = 0
            transmitChannelImbalanceReal_c = 1
            transmitChannelImbalanceImag_c = 0
            receiveCrossTalk1Real_c = 0
            receiveCrossTalk1Imag_c = 0
            receiveCrossTalk2Real_c = 0
            receiveCrossTalk2Imag_c = 0
            receiveChannelImbalanceReal_c = 1
            receiveChannelImbalanceImag_c = 0

        if self.step_azslope:
            azslope_file = self.in_azslope_angle
            if not azslope_file and not self.out_azslope_angle:
                azslope_file = 'azslope.temp'+str(int(self.start_time))
            elif not azslope_file:
                azslope_file = self.out_azslope_angle
            plant.get_azslope_angle(self.input_dem,
                                    self.convert_inc(
                                        self.input_inc),
                                    self.pixel_size_az,
                                    self.pixel_size_rg,
                                    azslope_file,
                                    force=self.force)
            azslope_file_c = path.abspath(azslope_file)
        else:
            azslope_file_c = ''

        # width_c = c_int(self.width)
        # length_c = c_int(self.length)
        image_obj = self.read_image(self.master_file, only_header=True)
        dataType = plant.get_isce_dtype(image_obj.dtype)
        flag_is_complex = np.asarray(('CFLOAT' in dataType.upper() or
                                      'COMPLEX' in dataType.upper()),
                                     dtype=np.byte)

        time_1 = time.time()

        # lib.polcal(hh_file_c, hv_file_c, vh_file_c, vv_file_c,
        #            hhOut_file_c, hvOut_file_c, vhOut_file_c, vvOut_file_c,
        #            transmitCrossTalk1Real_c, transmitCrossTalk2Real_c,
        #            transmitChannelImbalanceReal_c,
        #            transmitCrossTalk1Imag_c, transmitCrossTalk2Imag_c,
        #            transmitChannelImbalanceImag_c,
        #            receiveCrossTalk1Real_c, receiveCrossTalk2Real_c,
        #            receiveChannelImbalanceReal_c,
        #            receiveCrossTalk1Imag_c, receiveCrossTalk2Imag_c,
        #            receiveChannelImbalanceImag_c,
        #            self.width, self.length)

        flag_c_call = False
        if flag_c_call:
            command = '/home/shiroma/dev/plant/polcal.abi3.so '
            # swap = sys.byteorder == 'big'
            command_vect = [hh_file_c, hv_file_c, vh_file_c, vv_file_c,
                            hhOut_file_c, hvOut_file_c, vhOut_file_c,
                            vvOut_file_c,
                            str(self.width), str(self.length),
                            str(flag_is_complex),
                            str(transmitCrossTalk1Real_c),
                            str(transmitCrossTalk2Real_c),
                            str(transmitChannelImbalanceReal_c),
                            str(transmitCrossTalk1Imag_c),
                            str(transmitCrossTalk2Imag_c),
                            str(transmitChannelImbalanceImag_c),
                            str(receiveCrossTalk1Real_c),
                            str(receiveCrossTalk2Real_c),
                            str(receiveChannelImbalanceReal_c),
                            str(receiveCrossTalk1Imag_c),
                            str(receiveCrossTalk2Imag_c),
                            str(receiveChannelImbalanceImag_c),
                            str(azslope_file_c)]
            command += ' '.join(command_vect)
            plant.execute(command, verbose=True)
        else:
            from isceobj.Image import createImage
            hh_fileImage = createImage()
            hh_fileImage.load(hh_file_c+'.xml')
            hh_fileImage.createImage()
            hh_fileAccessor = hh_fileImage.getAccessor()

            hv_fileImage = createImage()
            hv_fileImage.load(hv_file_c+'.xml')
            hv_fileImage.createImage()
            hv_fileAccessor = hv_fileImage.getAccessor()

            vh_fileImage = createImage()
            vh_fileImage.load(vh_file_c+'.xml')
            vh_fileImage.createImage()
            vh_fileAccessor = vh_fileImage.getAccessor()

            vv_fileImage = createImage()
            vv_fileImage.load(vv_file_c+'.xml')
            vv_fileImage.createImage()
            vv_fileAccessor = vv_fileImage.getAccessor()

            hhOut_fileImage = createImage()
            hhOut_fileImage.initImage(hhOut_file_c, 'write', self.width)
            hhOut_fileImage.dataType = hh_fileImage.dataType
            hhOut_fileImage.createImage()
            hhOut_fileAccessor = hhOut_fileImage.getAccessor()

            hvOut_fileImage = createImage()
            hvOut_fileImage.initImage(hvOut_file_c, 'write', self.width)
            hvOut_fileImage.dataType = hv_fileImage.dataType
            hvOut_fileImage.createImage()
            hvOut_fileAccessor = hvOut_fileImage.getAccessor()

            vhOut_fileImage = createImage()
            vhOut_fileImage.initImage(vhOut_file_c, 'write', self.width)
            vhOut_fileImage.dataType = vh_fileImage.dataType
            vhOut_fileImage.createImage()
            vhOut_fileAccessor = vhOut_fileImage.getAccessor()

            vvOut_fileImage = createImage()
            vvOut_fileImage.initImage(vvOut_file_c, 'write', self.width)
            vvOut_fileImage.dataType = vv_fileImage.dataType
            vvOut_fileImage.createImage()
            vvOut_fileAccessor = vvOut_fileImage.getAccessor()

            if azslope_file_c:
                azslope_image = createImage()
                azslope_image.set_filename(azslope_file_c)
                azslope_image.load(azslope_file_c+'.xml')
                azslope_image.initImage(azslope_file_c, 'read', self.width)
                azslope_image.createImage()
                azslope_file_accessor = azslope_image.getAccessor()
            else:
                azslope_file_accessor = 0

            if plant.isvalid(self.fr_angle) and self.fr_angle != 0:
                self.print('Faraday rotation angle (deg): ' +
                           str(self.fr_angle))
            elif plant.isnan(self.fr_angle):
                self.fr_angle = 0

            polcal_py(hh_fileAccessor, hv_fileAccessor, vh_fileAccessor,
                      vv_fileAccessor, hhOut_fileAccessor, hvOut_fileAccessor,
                      vhOut_fileAccessor, vvOut_fileAccessor,
                      self.width, self.length, flag_is_complex,
                      transmitCrossTalk1Real_c, transmitCrossTalk2Real_c,
                      transmitChannelImbalanceReal_c, transmitCrossTalk1Imag_c,
                      transmitCrossTalk2Imag_c, transmitChannelImbalanceImag_c,
                      receiveCrossTalk1Real_c, receiveCrossTalk2Real_c,
                      receiveChannelImbalanceReal_c, receiveCrossTalk1Imag_c,
                      receiveCrossTalk2Imag_c, receiveChannelImbalanceImag_c,
                      azslope_file_accessor,
                      self.fr_angle)

            hh_fileImage.finalizeImage()
            hv_fileImage.finalizeImage()
            vh_fileImage.finalizeImage()
            vv_fileImage.finalizeImage()
            if self.step_azslope and not self.out_azslope_angle:
                if plant.isfile(azslope_file):
                    remove(azslope_file)
                if plant.isfile(azslope_file+'.vrt'):
                    remove(azslope_file+'.vrt')
                if plant.isfile(azslope_file+'.xml'):
                    remove(azslope_file+'.xml')

            descr = ''
            hhOut_fileImage.setImageType('bil')
            hhOut_fileImage.addDescription(descr)
            hhOut_fileImage.finalizeImage()
            hhOut_fileImage.renderHdr()
            hvOut_fileImage.setImageType('bil')
            hvOut_fileImage.addDescription(descr)
            hvOut_fileImage.finalizeImage()
            hvOut_fileImage.renderHdr()
            vhOut_fileImage.setImageType('bil')
            vhOut_fileImage.addDescription(descr)
            vhOut_fileImage.finalizeImage()
            vhOut_fileImage.renderHdr()
            vvOut_fileImage.setImageType('bil')
            vvOut_fileImage.addDescription(descr)
            vvOut_fileImage.finalizeImage()
            vvOut_fileImage.renderHdr()

        time_diff = (self.output_file +
                     ' --- ' +
                     'polcal time: '+plant.hms_string(time.time()-time_1))

        plant.execute('echo "'+time_diff+'" > ' +
                      self.output_file+'.txt')

        # copy headers
        shutil.copyfile(self.hh_file+'.xml', hhOut_file+'.xml')
        shutil.copyfile(self.hv_file+'.xml', hvOut_file+'.xml')
        shutil.copyfile(self.vh_file+'.xml', vhOut_file+'.xml')
        shutil.copyfile(self.vv_file+'.xml', vvOut_file+'.xml')

        # move change the reference files to the calibrated files
        self.hh_file = hhOut_file
        self.hv_file = hvOut_file
        self.vh_file = vhOut_file
        self.vv_file = vvOut_file

    def loop_non_polarimetric_channels(self):
        '''
        call absolute radiometric correction for each input file
        '''
        if (not self.step_abs and not self.step_terrain_correction
            and not ((self.out_intensity and not self.in_intensity) or
                     (self.output_in_dB))):
            return

        for i, current_file in enumerate(self.current_images):
            output_file = self.get_filename(current_file, temp=True)
            self.apply_abs_and_terrain_correction(current_file,
                                                  output_file)
            # image_obj = plant.read_image(output_file)
            # plant.imshow(image_obj, title=output_file, dark_theme=True)
            self.current_images[i] = output_file
        '''
        if self.hh_file:
            output_file = self.get_filename(self.hh_file, temp=True)
            self.apply_abs_and_terrain_correction(self.hh_file,
                                               output_file)
            self.hh_file = output_file
        if self.hv_file:
            output_file = self.get_filename(self.hv_file, temp=True)
            self.apply_abs_and_terrain_correction(self.hv_file,
                                               output_file)
            self.hv_file = output_file
        if self.vh_file:
            output_file = self.get_filename(self.vh_file, temp=True)
            self.apply_abs_and_terrain_correction(self.vh_file,
                                               output_file)
            self.vh_file = output_file
        if self.vv_file:
            output_file = self.get_filename(self.vv_file, temp=True)
            self.apply_abs_and_terrain_correction(self.vv_file,
                                               output_file)
            self.vv_file = output_file
        '''
        if self.simulate:
            output_file = self.get_filename(self.output_file,
                                            temp=True)
            self.apply_abs_and_terrain_correction(None,
                                                  output_file)
            self.current_images = [output_file]

    def loop_polarimetric_channels(self):
        '''
        call polarimetric-related corrections (polcal and azslope)
        '''
        if (self.number_of_files == 4 and self.step_polcal or
                (self.step_fr and self.fr_angle != 0) or self.step_azslope):
            self.apply_polarimetric_correction(self.transmit, self.receive)
        # if self.number_of_files == 4 and self.step_azslope:
        #     radiometric_correction_obj.loopAzSlopeCorrection()

    def get_filename(self, filename_orig, temp=False):
        '''
        get temporary or output filename (determined by temp=False/True)
        '''
        ret_dict = plant.parse_filename(filename_orig)
        filename = ret_dict['filename']
        timestamp = str(float(time.time()))
        # timestamp = str(int(self.start_time))
        if temp:
            str_ext = '_temp_'+timestamp
        else:
            str_ext = self.output_ext
        if str_ext not in filename or not temp:
            file_temp_splitted = filename.split('.')
            if len(file_temp_splitted) > 1:
                file_temp = '.'.join(file_temp_splitted[:-1])
            else:
                file_temp = file_temp_splitted[0]
            file_temp = file_temp + str_ext
        elif str_ext+'(2)' in filename:
            file_temp = filename.replace(str_ext+'(2)',
                                         str_ext+'(3)')
        else:
            file_temp = filename.replace(str_ext,
                                         str_ext+'(2)')
        if temp:
            plant.append_temporary_file(path.basename(file_temp))
        return path.basename(file_temp)

    def get_output_name(self, output_file, filename_orig, output_dir, pol):
        '''
        get output name based on the original file name
        '''

        if (path.isdir(output_file) or
                output_file.endswith('/')):
            output_dir = output_file
            output_file = ''
        if not path.isdir(output_dir):
            makedirs(output_dir)

        if output_dir and not(output_file):
            output_file = path.join(output_dir, path.basename(filename_orig))
        elif (not(output_dir) and output_file and self.number_of_files > 1 and
              pol):
            output_file = output_file+'_'+pol
        elif (not(output_dir) and output_file and self.number_of_files > 1):
            output_file = self.get_filename(output_file)
        # else:
        #     output_file = self.get_filename(filename_orig)
        return output_file

    def realize_changes(self):
        '''
        save changes
        '''
        self.print('')
        self.print('output files:')

        for i, current_file in enumerate(self.current_images):
            if self.simulate or len(self.current_images) == 1:
                output_file = self.output_file
            else:
                output_file = self.output_files[i]
                if current_file != self.input_images[i]:
                    plant.rename_image(current_file,
                                       output_file,
                                       verbose=self.verbose,
                                       force=self.force)
                    continue
            # plant.plant.copy_image(self.input_images[i],
            #                        output_file,
            #                        verbose=self.verbose,
            #                        force=self.force)
            plant.plant.copy_image(current_file,
                                   output_file,
                                   verbose=self.verbose,
                                   force=self.force)
            '''
            if current_file == self.hh_file:
                pol = 'hh'
            elif current_file == self.hv_file:
                pol = 'hv'
            elif current_file == self.vh_file:
                pol = 'vh'
            elif current_file == self.vv_file:
                pol = 'vv'
            else:
                pol = ''
            if not self.simulate:
                current_file_orig = self.input_images_orig[i]
            output_file = self.get_output_name(self.output_file,
                                               current_file_orig,
                                               self.output_dir,
                                               pol)
            # if self.hh_file != self.hh_file_orig:

        if self.hh_file:
            output_file = self.get_output_name(self.output_file,
                                               self.hh_file_orig,
                                               self.output_dir,
                                               'hh')
            if self.hh_file != self.hh_file_orig:
                plant.rename_image(self.hh_file, output_file,
                             verbose=self.verbose,
                             force=self.force)
            else:
                plant.copy_image(self.hh_file, output_file,
                           verbose=self.verbose,
                           force=self.forcer)
        if self.hv_file:
            output_file = self.get_output_name(self.output_file,
                                               self.hv_file_orig,
                                               self.output_dir,
                                               'hv')
            if self.hv_file != self.hv_file_orig:
                plant.rename_image(self.hv_file, output_file,
                             verbose=self.verbose,
                             force=self.force)
            else:
                plant.copy_image(self.hv_file, output_file,
                           verbose=self.verbose,
                           force=self.force)
        if self.vh_file:
            output_file = self.get_output_name(self.output_file,
                                               self.vh_file_orig,
                                               self.output_dir,
                                               'vh')
            if self.vh_file != self.vh_file_orig:
                plant.rename_image(self.vh_file, output_file,
                             verbose=self.verbose,
                             force=self.force)
            else:
                plant.copy_image(self.vh_file, output_file,
                           verbose=self.verbose,
                           force=self.force)
        if self.vv_file:
            output_file = self.get_output_name(self.output_file,
                                               self.vv_file_orig,
                                               self.output_dir,
                                               'vv')
            if self.vv_file != self.vv_file_orig:
                plant.rename_image(self.vv_file, output_file,
                             verbose=self.verbose,
                             force=self.force)
            else:
                plant.copy_image(self.vv_file, output_file,
                           verbose=self.verbose,
                           force=self.force)
            '''
    # def __del__(self):
    #     for current_file in plant.plant_config.temporary_files:
    #         if plant.isfile(current_file):
    #             self.print('removing: '+current_file)
    #             remove(current_file)
    #         if plant.isfile(current_file+'.xml'):
    #             remove(current_file+'.xml')
    #         if plant.isfile(current_file+'.vrt'):
    #             remove(current_file+'.vrt')


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantRadiometricCorrection(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
