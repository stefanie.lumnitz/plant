#!/usr/bin/env python
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import os
from distutils.core import setup

__version__ = version = VERSION = '0.1.39dev'
# __version__ = version = VERSION = open('VERSION').read().strip()

'''
>> python3 setup.py sdist  # generage <tar.gz>
>> pip install <tar.gz>  # install
pytest <= 3.6.4
'''

directory = os.path.abspath(os.path.dirname(__file__))

long_description = ''

package_data_dict = {}
package_data_dict[''] = [
    'VERSION',
    os.path.join('db', 'template_slide_compare.html')]

setup(
    name='PLAnT',
    # version=f'{plant.VERSION}',
    # version='0.0.30dev',
    version=version,
    description='Polarimetric-interferometric Lab and Analysis Tools',
    package_dir={'plant': '.'},
    packages=['plant',
              'plant.modules',
              'plant.tests'],  # 'app',
    package_data=package_data_dict,
    classifiers=['Programming Language :: Python', ],
    # py_modules
    # python_requires='>3.6.0',
    py_modules=['plant.app.plant_util',
                'plant.app.plant_agb_samples',
                'plant.app.plant_csv_reader',
                'plant.app.plant_display',
                'plant.app.plant_filter',
                'plant.app.plant_geocode',
                'plant.app.plant_info',
                'plant.app.plant_jaxafnf',
                'plant.app.plant_mosaic',
                'plant.app.plant_ls',
                # 'plant.app.plant_pspwrapper',
                'plant.app.plant_radiometric_correction',
                'plant.app.plant_slantrange',
                'plant.app.plant_template'],
    scripts=['app/plant_util.py',
             'app/plant_agb_samples.py',
             'app/plant_csv_reader.py',
             'app/plant_display.py',
             'app/plant_filter.py',
             'app/plant_geocode.py',
             'app/plant_info.py',
             'app/plant_mosaic.py',
             'app/plant_jaxafnf.py',
             'app/plant_ls.py',
             # 'app/plant_pspwrapper.py',
             'app/plant_radiometric_correction.py',
             'app/plant_slantrange.py',
             'app/plant_template.py'],
    # data_files=data_files,
    # package_data={'doc': path.join('doc', '*')},  # db
    # data_files=[('__init__.py', 'plant_config.txt')],
    # [('__init__.py'),
    # data_files=[('plant_config.txt')],
    install_requires=['numpy', 'argparse', 'matplotlib', 'scipy',
                      'scikit-image', 'astropy', 'pandas',
                      'pillow', 'h5py',
                      'folium', 'statsmodels', 'python-dateutil',
                      'numexpr', 'jinja2'],
    # gdal, libgdal, ncurses, pytest==3.2.1, pillow
    url='https://gitlab.com/plant/plant',
    author='Gustavo H. X. Shiroma, Marco Lavalle, Federico Croce',
    author_email=('gustavo.h.shiroma@jpl.nasa.gov,'
                  ' marco.lavalle@jpl.nasa.gov,'
                  ' federico.croce@jpl.nasa.gov'),
    license='Copyright by the California Institute of Technology.'
    ' ALL RIGHTS RESERVED.',
    # PLAnT has C/C++ extensions, so it's not zip safe.
    # zip_safe=False,
    long_description=long_description,
)
