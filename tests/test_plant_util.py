#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Federico Croce, Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


import numpy as np
import plant
import pytest


@pytest.mark.parametrize("var", [('0:3')])
def test_plant_util(var):
    with plant.PlantLogger():
        args = [float(x) for x in var.split(':')]
        var_2d = plant.shape_image(np.arange(*args))

        # function
        out = plant.util(var)
        assert np.array_equal(out.image, var_2d)

        # plant_execute
        temp_file = plant.get_temporary_file()
        plant.execute(f'plant_util.py {var} -f -o {temp_file} -q -f')
        plant.append_temporary_file(temp_file)
        assert plant.isfile(temp_file)
