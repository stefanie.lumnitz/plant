#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma adapted from gdalviewer (Piyush Agram)
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


from tkinter import Scrollbar, Canvas, Frame, Tk, YES, BOTH, SUNKEN, \
    VERTICAL, HORIZONTAL, RIGHT, Y, NO, BOTTOM, X, LEFT, NW, Button, \
    ALL, CURRENT
import numpy as np
from PIL import Image, ImageTk
from collections import deque
import matplotlib.pyplot as plt


class TKViewer(Tk):

    def __init__(self, data, vmin=None, vmax=None,
                 title="Tkinter Viewer"):
        '''
        Init
        '''
        self.data = data
        self.vmin = vmin
        self.vmax = vmax
        self.title = title

    def run(self):
        trans = Transformer(cmin=self.vmin, cmax=self.vmax)
        cont = Container(
            data=self.data, trans=trans, cmap=plt.get_cmap('jet'))
        root = Tk()
        root.title(self.title)
        frame_im = ScrolledCanvas(cont, root)
        frame = Frame(root, width=self.data.shape[1],
                      height=self.data.shape[0])
        frame.pack()
        zoom_p_button = Button(frame, text="+", command=tk_zoom_p)
        zoom_p_button.pack(side=LEFT)
        zoom_r_button = Button(frame, text=" ", command=tk_zoom_r)
        zoom_r_button.pack(side=LEFT)
        zoom_m_button = Button(frame, text="-", command=tk_zoom_m)
        zoom_m_button.pack(side=LEFT)
        #            frame.focus_set()
        frame_im.mainloop()


class myScrollbar(Scrollbar):
    '''
    My scroll bar.
    '''
    def __init__(self, master=None, tagg=None, cnf={}, **kw):
        '''
        Init
        '''
        self.tagg = tagg
        Scrollbar.__init__(self, master=master, cnf=cnf, **kw)

    def set(self, *args):
        Scrollbar.set(self, *args)
        # print(self.__dict__)
        # self.master.updateVisibleBox()


class myCanvas(Canvas):
    '''
    My canvas.
    '''
    def __init__(self, master=None, cnf={}, **kw):
        '''
        Init
        '''
        Canvas.__init__(self, master=master, cnf=cnf, **kw)

    def getVisibleRegion(self):
        '''
        Get the part of canvas that is visible to user.
        '''
        x1, y1 = self.canvasx(0), self.canvasy(0)
        w, h = self.winfo_width(), self.winfo_height()
        x2, y2 = self.canvasx(w), self.canvasy(h)
        return (x1, y1, x2, y2)


class ScrolledCanvas(Frame):
    def __init__(self, data=None, parent=None):
        self.__xmin = None
        self.__xmax = None
        self.__ymin = None
        self.__ymax = None

        self.__count = 0
        self.__list = deque([], 100)
        # Keep only last 100 raster updates in memory

        self.data = data
        self.width = data.data.shape[1]
        self.length = data.data.shape[0]
        Frame.__init__(self, parent)
        self.pack(expand=YES, fill=BOTH)
        self.canv = myCanvas(self, relief=SUNKEN)
        self.canv.config(width=min(400, self.width),
                         height=min(300, self.length))
        self.canv.config(xscrollincrement=20, yscrollincrement=20)
        self.canv.config(highlightthickness=0)
        self.canv.config(scrollregion=(0, 0, self.width, self.length))
        self.sbarV = myScrollbar(self, 'Y', orient=VERTICAL)
        self.sbarH = myScrollbar(self, 'X', orient=HORIZONTAL)
        self.sbarV.config(command=self.yview)
        self.sbarH.config(command=self.xview)
        self.sbarV.pack(side=RIGHT, fill=Y, expand=NO)
        self.sbarH.pack(side=BOTTOM, fill=X, expand=NO)
        self.canv.pack(side=LEFT, expand=YES, fill=BOTH)
        self.canv.config(yscrollcommand=self.sbarV.set)
        self.canv.config(xscrollcommand=self.sbarH.set)
        self.canv.bind('<Configure>', self.resize)
#        self.canv.bind("<Key>", self.key)
        self.canv.bind("<Button-1>", self.callbackMouseLeftButton)
        self.canv.bind("<B1-Motion>", self.callbackMouseLeftButton)
        self.canv.bind("<Button-2>", self.callbackMouseCenterButton)
        self.canv.bind("<B2-Motion>", self.callbackMouseCenterButton)
        self.canv.bind("<Button-3>", self.callbackMouseRightButton)

        self.current_polygon = []
        self.point_array = []
#        self.polygon_list = []

        self.canv.update_idletasks()
        self.updateVisibleBox()

#    def key(self,event):
#        print("pressed"+repr(event.char))

    def callbackMouseLeftButton(self, event):
        vis = self.canv.getVisibleRegion()
        xp = np.int(vis[0]+event.x)
        yp = np.int(vis[1]+event.y)
        data = self.data.data
        v = data[xp, yp]

        if self.current_polygon == []:
            current = self.canv.find_withtag(CURRENT)
            if self.canv.type(current) == 'image':
                self.current_polygon = [xp, yp]
                self.point_array.append(self.canv.create_oval(xp-2, yp-2, xp+2,
                                                              yp+2,
                                                              fill='red',
                                                              outline='red'))
            elif self.canv.type(current) == 'polygon':
                all_tags = self.canv.find_withtag(ALL)
                for current_tag in all_tags:
                    if (current_tag != current and
                            self.canv.type(current_tag) == 'polygon'):
                        self.canv.itemconfig(current_tag, fill='red',
                                             outline='red')
                self.canv.itemconfig(current, fill="blue", outline='blue')
        else:
            self.current_polygon = self.current_polygon + [xp, yp]
            self.canv.create_line(self.current_polygon[-2],
                                  self.current_polygon[-1], xp, yp,
                                  fill='red')
            self.point_array.append(self.canv.create_oval(xp-2, yp-2, xp+2,
                                                          yp+2, fill='red',
                                                          outline='red'))
        if np.sum(v.shape) == 0:
            print('Values at ('+str(xp)+','+str(yp)+'): '+str(v))
        else:
            print('Values at ('+str(xp)+','+str(yp)+'): ('+str(v[0])+', ' +
                  str(v[1])+', '+str(v[2])+')')

    def callbackMouseCenterButton(self, event):
        if self.current_polygon == []:
            current = self.canv.find_withtag(CURRENT)
            if self.canv.type(current) == 'polygon':
                self.canv.delete(current)
            else:
                all_tags = self.canv.find_withtag(ALL)
                for current_tag in all_tags:
                    if self.canv.type(current_tag) == 'polygon':
                        if self.canv.itemcget(current_tag, "fill") == 'blue':
                            self.canv.delete(current_tag)
                all_tags = self.canv.find_withtag(ALL)
                for current_tag in all_tags:
                    if self.canv.type(current_tag) == 'polygon':
                        last_polygon = current_tag
                self.canv.itemconfig(last_polygon, fill="blue", outline='blue')
        else:
            self.current_polygon = self.current_polygon[0:-2]
            current_point = self.point_array.pop()
            self.canv.delete(current_point)

    def callbackMouseRightButton(self, event):
        if self.current_polygon == []:
            all_tags = self.canv.find_withtag(ALL)
            for current_tag in all_tags:
                if self.canv.type(current_tag) == 'polygon':
                    print(current_tag)
                    print(self.canv.coords(current_tag))
        else:
            all_tags = self.canv.find_withtag(ALL)
            for current_tag in all_tags:
                if self.canv.type(current_tag) == 'polygon':
                    self.canv.itemconfig(current_tag, fill='red',
                                         outline='red')
            polygon = self.canv.create_polygon(self.current_polygon,
                                               fill='blue', outline='blue',
                                               width=2)
            print(polygon)
            self.current_polygon = []
            while 1:
                try:
                    current_point = self.point_array.pop()
                    self.canv.delete(current_point)
                except:
                    break
            self.point_array = []

            #        print('release')

    def updateVisibleBox(self):
        box = self.canv.getVisibleRegion()
        self.__xmin = int(box[0])
        self.__xmax = int(box[2])
        self.__ymin = int(box[1])
        self.__ymax = int(box[3])

        self.putImage([self.__xmin, self.__ymin, self.__xmax, self.__ymax])

    def xview(self, *args):
        self.canv.xview(*args)
        vis = self.canv.getVisibleRegion()
        new_xmin = vis[0]
        new_xmax = vis[2]
        if new_xmin > self.__xmin:   # Moved to right
            draw_xmin = max(new_xmin, self.__xmax)
            draw_xmax = max(new_xmax, self.__xmax)
        elif new_xmin < self.__xmin:  # Moved to left
            draw_xmin = min(new_xmin, self.__xmin)
            draw_xmax = min(new_xmax, self.__xmin)
        else:
            return
        self.__count += 1
        self.putImage([draw_xmin, self.__ymin, draw_xmax, self.__ymax])
        self.__xmin = new_xmin
        self.__xmax = new_xmax

    def yview(self, *args):
        self.canv.yview(*args)
        vis = self.canv.getVisibleRegion()
        new_ymin = vis[1]
        new_ymax = vis[3]
        if new_ymin > self.__ymin:   # Moved to right
            draw_ymin = max(new_ymin, self.__ymax)
            draw_ymax = max(new_ymax, self.__ymax)
        elif new_ymin < self.__ymin:  # Moved to left
            draw_ymin = min(new_ymin, self.__ymin)
            draw_ymax = min(new_ymax, self.__ymin)
        else:
            return
        self.__count += 1
        self.putImage([self.__xmin, draw_ymin, self.__xmax, draw_ymax])
        self.__ymin = new_ymin
        self.__ymax = new_ymax

    def resize(self, event):
        self.canv.config(width=event.width, height=event.height)
        self.updateVisibleBox()

    def putImage(self, box):
        img = self.data[box[0]:box[2], box[1]:box[3]]
        imgtk = ImageTk.PhotoImage(image=img)

        if len(self.__list) == self.__list.maxlen:
            obj = self.__list.pop()
            self.canv.delete(obj)

        self.__list.appendleft(imgtk)

        flag_found_image = 0
        all_tags = self.canv.find_withtag(ALL)
        for current_tag in all_tags:
            if self.canv.type(current_tag) == 'image':
                flag_found_image = 1
                self.canv.itemconfig(current_tag, image=imgtk)
        if not flag_found_image:
            self.canv.create_image(box[0], box[1], anchor=NW, image=imgtk)


class Transformer(object):
    '''
    Class transforms data to range 0.0 - 1.0 for mapping into colormap.
    Extend to Range, Wrapped, Exp, Piece-wise, Other useful
    transformations etc.
    '''
    def __init__(self, cmin=0., cmax=100.):
        self.top = 1.
        self.cmin = cmin
        self.cmax = cmax

    def __call__(self, x):
        #        res = (np.mod(x-self.cmin, self.cmax)*self.top/self.cmax)
        res = np.clip((x-self.cmin)/(self.cmax-self.cmin), 0., 1.)
        return res


class Container(object):
    '''
    Class holds the data and transformer used to display data.
    Also holds a cmap that is applied to data for display.
    Data can be numpy array, memmap, hdf5 array, gdal container etc.
    '''

    def __init__(self, data=None, cmap=None, trans=None):
        self.data = data
        self.trans = trans
        self.cmap = cmap

    def __getitem__(self, *args):
        nargs = (args[0][1], args[0][0])
        if len(self.data.shape) == 3:
            res = self.trans(self.data.__getitem__(nargs))
            res = res*255
            res = res.clip(0, 255).astype(np.uint8)
        else:
            res = self.trans(self.data.__getitem__(nargs))
            res = self.cmap(np.absolute(res))*255
            res = res.clip(0, 255).astype(np.uint8)
        return Image.fromarray(res[:, :, :3])


def tk_zoom_p():
    print('+')


def tk_zoom_m():
    print('-')


def tk_zoom_r():
    print('r')
