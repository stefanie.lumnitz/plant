#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import numpy as np
import gdal
import plant

# def get_dtype_name(data):
#    ret = get_dtype_name_(data)
#    print('/// ', ret.__class__)
#    return ret


def get_dtype_name(data):
    class_name = data.__class__.__name__
    if (class_name == 'list'):
        return get_dtype_name(data[0])
    elif (class_name == 'memmap'):
        return data.dtype.name
    elif (class_name == 'type'):
        return str(data.__name__)
    elif (class_name == 'ndarray' and
          data.dtype.name == 'object'):
        return get_dtype_name(data.item(0))
    elif class_name == 'ndarray':
        return data.dtype.name
    elif class_name == 'str':
        # np_class_name = get_np_dtype(class_name)
        # print('DEBUG', np_class_name)
        return data
    elif class_name == 'dtype':
        return str(data)
    # elif class_name == 'NoneType':
    #     return None
    else:
        return class_name


def get_gdal_dtype_from_np(dtype):
    # GDT_CInt16 = 8
    # GDT_CInt32 = 9
    # GDT_TypeCount = 12

    if isinstance(dtype, str):
        dtype = dtype.lower()
    else:
        dtype = np.dtype(dtype).name
    if 'byte' in dtype:
        return gdal.GDT_Byte
    elif 'float64' in dtype:
        return gdal.GDT_Float64
    elif 'float' in dtype:
        return gdal.GDT_Float32
    elif 'byte' in dtype or 'int8' in dtype:
        return gdal.GDT_Byte
    elif 'uint16' in dtype:
        return gdal.GDT_UInt16
    elif 'uint32' in dtype:
        return gdal.GDT_UInt32
    # elif 'uint64' in dtype:
    #    return gdal.GDT_UInt64
    elif 'int16' in dtype:
        return gdal.GDT_Int16
    elif 'int32' in dtype:
        return gdal.GDT_Int32
    # elif 'int64' in dtype:
    #    return gdal.GDT_Int64
    elif 'complex128' in dtype:
        return gdal.GDT_CFloat64
    elif 'complex' in dtype:
        return gdal.GDT_CFloat32
    # else:
    return gdal.GDT_Unknown


def apply_srf_dtype(image):
    dtype = get_dtype_name(image)
    if 'int' in dtype:
        new_dtype = np.byte
    elif 'complex' in dtype:
        new_dtype = np.complex64
    else:
        new_dtype = np.float32
    image = np.asarray(image, dtype=new_dtype)
    return image


def get_np_dtype_from_srf(dtype):
    for srf_dtype, np_dtype in plant.SRF_DTYPE:
        if dtype == srf_dtype:
            return np_dtype
    # print(f'ERROR SRF data type not recognized:'
    #       f' {dtype}')
    return


def get_srf_dtype(dtype):
    dtype = dtype.lower()
    if 'int' in dtype or 'byte' in dtype:
        return 1
    if 'complex' in dtype:
        return 30
    return 20


def get_np_dtype_from_gdal(dtype):
    # GDT_CInt16 = 8
    # GDT_CInt32 = 9
    # GDT_TypeCount = 12
    # if not isinstance(dtype, str):
    #     dtype = np.dtype(dtype).name
    if dtype == gdal.GDT_Byte:
        return np.byte
    elif dtype == gdal.GDT_Float64:
        return np.float64
    elif dtype == gdal.GDT_Float32:
        return np.float32
    elif dtype == gdal.GDT_Byte:
        return np.byte
    elif dtype == gdal.GDT_UInt16:
        return np.uint16
    elif dtype == gdal.GDT_UInt32:
        return np.uint32
    # elif dtype == gdal.GDT_UInt64:
    #     return np.uint64
    elif dtype == gdal.GDT_Int16:
        return np.int16
    elif dtype == gdal.GDT_Int32:
        return np.int32
    # elif dtype == gdal.GDT_Int64:
    #    return np.int64
    elif dtype == gdal.GDT_CFloat64:
        return np.complex128
    elif dtype == gdal.GDT_CFloat32:
        return np.complex64
    # elif dtype == gdal.GDT_CFloat128:
    #    return np.complex128
    else:
        return np.float32


def get_vrt_dtype(dtype):

    dtype_upper = get_isce_dtype(dtype).upper()

    if 'BYTE' in dtype_upper or 'CHAR' in dtype_upper:
        return 'Byte'
    elif 'UINT16' in dtype_upper:
        return 'UInt16'
    elif ('SHORT' in dtype_upper or
          'CIQBYTE' in dtype_upper or
          'INT16' in dtype_upper):
        return 'Int16'
    elif 'UINT64' in dtype_upper:
        return 'UInt64'
    elif 'UINT' in dtype_upper:
        return 'UInt32'
    elif 'INT64' in dtype_upper or 'LONG' in dtype_upper:
        return 'Int64'
    elif 'INT' in dtype_upper:
        return 'Int32'
    elif 'FLOAT' in dtype_upper:
        return 'Float32'
    elif 'DOUBLE' in dtype_upper:
        return 'Float64'
    elif 'CFLOAT' in dtype_upper:
        return 'CFloat32'
    elif 'CDOUBLE' in dtype_upper:
        return 'CFloat64'
    else:
        return


# def getGDALDataType(filename_orig, verbose=False):
#    filename = search_image(filename_orig)[0]
#    gdalinfo = plant_execute('gdalinfo '+filename, verbose=verbose)
#    for current_line in gdalinfo:
#        if 'Type' in current_line:
#            try:
#                line_temp = current_line.split('Type=')[1]
#                dtype = line_temp.split(',')[0]
#                return dtype
#            except:
#                return


def get_isce_dtype(dtype):
    '''
    return ISCE type name (input: numpy array or datatype name)
    '''
    if dtype is None:
        return
    if not isinstance(dtype, str):
        dtype = np.dtype(dtype).name
    dtype_lower = dtype.lower()
    if dtype_lower == 'float32':
        return 'FLOAT'
    elif dtype_lower == 'float64':
        return 'DOUBLE'
    elif dtype_lower == 'byte' or 'int8'in dtype_lower:
        return 'BYTE'
    elif 'int16' in dtype_lower:
        return 'SHORT'
    elif 'int64' in dtype_lower:
        return 'LONG'
    elif 'int' in dtype_lower:
        return 'INT'  # INTEGER?
    elif (dtype_lower == 'complex64' or
          dtype_lower == 'complex'):
        return 'CFLOAT'
    elif dtype_lower == 'complex128':
        return 'CDOUBLE'
    else:
        return dtype


def get_dtype_size(input_data_type):
    '''
    return ISCE type size in bytes
    '''
    if input_data_type is None:
        return

    if not isinstance(input_data_type, str):
        try:
            dtype_size = np.dtype(input_data_type).itemsize
            return dtype_size
        except:
            pass
    multiplier = 1
    if isinstance(input_data_type, list):
        dtype_upper = input_data_type[0].upper()
    else:
        if '[' in input_data_type and ']' in input_data_type:
            value = input_data_type.split('[')[1].split(']')[0]
            if plant.isnumeric(value):
                multiplier = int(value)
        dtype_upper = input_data_type.upper()
    if 'CFLOAT' in dtype_upper:
        return multiplier*8
    elif 'CDOUBLE' in dtype_upper:
        return multiplier*16
    elif 'FLOAT64' in dtype_upper:
        return multiplier*8
    elif 'FLOAT' in dtype_upper:
        return multiplier*4
    elif 'DOUBLE' in dtype_upper:
        return multiplier*8
    elif 'BYTE' in dtype_upper or 'CHAR' in dtype_upper:
        return multiplier*1
    elif 'SHORT' in dtype_upper:
        return multiplier*2
    # elif 'INTEGER' in dtype_upper:
    #    return multiplier*4
    elif 'LONG' in dtype_upper:
        return multiplier*8
    elif 'INT16' in dtype_upper:
        return multiplier*2
    elif 'INT32' in dtype_upper:
        return multiplier*4
    elif 'INT64' in dtype_upper:
        return multiplier*8
    elif 'INT' in dtype_upper:
        return multiplier*4
    else:
        dtype = get_isce_dtype(input_data_type)
        if dtype.upper() != dtype_upper:
            return multiplier*get_dtype_size(dtype)

        return


def get_mdx_dtype(dtype):
    '''
    return mdx type paramter command (input: numpy array or datatype name)
    '''
    if not isinstance(dtype, str):
        dtype = np.dtype(dtype).name
    dtypes = ['-b1', '-byte', '-b2', '-byte2', '-i1',
              '-integer*1', '-i2', '-integer*2', '-i4',
              '-integer*4', '-r4', '-real*4',
              '-r8', '-real*8',
              '-c2', '-complex*2', '-c8', '-complex*8',
              '-c8mag', '-cmag', '-c8pha', '-cpha', '-c2mag',
              '-c2pha', '-rmg', '-vfmt', '-val_frmt', '-c16']
    if dtype in dtypes:
        return dtype
    if 'byte' in dtype:
        return '-b1'
    elif 'float64' in dtype:
        return '-r8'
    elif 'float' in dtype:
        return '-r4'
    elif 'byte' in dtype or dtype in 'int8':
        return '-b1'
    elif 'int8' in dtype:
        return '-i1'
    elif 'int16' in dtype:
        return '-i2'
    elif 'int32' in dtype:
        return '-i4'
    elif 'int64' in dtype:
        return '-i8'
    elif 'complex128' in dtype:
        return '-c16'
    elif 'complex' in dtype:
        return '-c8'
    else:
        return ''


def get_np_dtype(isceType):
    '''
    return numpy type (input: isce data type)
    '''
    if isceType is None:
        return
    try:
        isceType_upper = isceType.upper()
    except AttributeError:
        isceType_upper = get_dtype_name(isceType).upper()

    if ('CFLOAT' in isceType_upper or
            'COMPLEX64' in isceType_upper):
        dtype = np.complex64
    elif ('CDOUBLE' in isceType_upper or
          'COMPLEX128' in isceType_upper):
        dtype = np.complex128
    elif 'UINT16' in isceType_upper:
        dtype = np.uint16
    elif 'UINT32' in isceType_upper:
        dtype = np.uint32
    elif 'UINT64' in isceType_upper:
        dtype = np.uint64
    elif 'FLOAT64' in isceType_upper:
        dtype = np.float64
    elif 'FLOAT' in isceType_upper:
        dtype = np.float32
    elif 'DOUBLE' in isceType_upper:
        dtype = np.float64
    elif ('BYTE' in isceType_upper or
          'INT8' in isceType_upper):
        dtype = np.byte
    elif 'SHORT' in isceType_upper or 'INT16' in isceType_upper:
        dtype = np.int16
    # elif 'INTEGER' in isceType_upper:
    #    dtype = np.int32
    elif 'LONG' in isceType_upper:
        dtype = np.int64
    elif 'INT64' in isceType_upper:
        dtype = np.int64
    elif 'INT' in isceType_upper:
        dtype = np.int32
    else:
        dtype = None
    return dtype
#        typeMap = { 'BYTE'   : 1,
#                    'SHORT'  : 2,
#                    'INT'    : 3,
#                    'LONG'   : 14,
#                    'FLOAT'  : 4,
#                    'DOUBLE' : 5,
#                    'CFLOAT' : 6,
#                    'CDOUBLE': 9 }

# 1 = Byte: 8-bit unsigned integer
# 2 = Integer: 16-bit signed integer
# 3 = Long: 32-bit signed integer
# 4 = Floating-point: 32-bit single-precision
# 5 = Double-precision: 64-bit double-precision floating-point
# 6 = Complex: Real-imaginary pair of single-precision floating-point
# 9 = Double-precision complex: Real-imaginary pair of double precision
#     floating-poin t
# 12 = Unsigned integer: 16-bit
# 13 = Unsigned long integer: 32-bit
# 14 = 64-bit long integer (signed)
# 15 = 64-bit unsigned long integer (unsigned)

