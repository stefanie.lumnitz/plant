#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# import sys
import numpy as np
import os
import gdal
from collections.abc import Sequence
# from copy import deepcopy
# from io import StringIO
import numbers
import plant

PLANT_IMAGE_IS_MUTABLE = False


_IMAGE_OBJ_ATTRS_TO_UPDATE_AFTER_LOADING_IMAGE = [
    '_geotransform',
    '_applied_transform_obj_crop_list',
    '_gcp_list',
    '_gcp_projection',
    '_width',
    '_length',
    '_depth',
    '_width_orig',
    '_length_orig',
    '_depth_orig',
    '_projection',
    '_metadata',
    '_null',
    '_offset_x',
    '_offset_y']

_BAND_OBJ_ATTRS_TO_KEEP_AFTER_LOADING_IMAGE = [
    '_name']


def _create_PlantBandOperation(image_ref, inputs,
                               inputs_kwargs, plant_transform_obj=None):
    max_nbands = None
    if image_ref is None:
        for arg in inputs:
            if (not isinstance(arg, plant.PlantImage) or
                (max_nbands is not None and
                 max_nbands >= arg.nbands)):
                continue
            max_nbands = arg.nbands
            image_ref = arg
        if image_ref is None:
            image_ref = PlantBand(inputs[0])
            max_nbands = image_ref.nbands
    else:
        max_nbands = image_ref.nbands
    out_image_obj = image_ref.soft_copy()
    if plant_transform_obj is not None:
        out_image_obj.plant_transform_obj = plant_transform_obj
    for b in range(max_nbands):
        band_args = []
        for input in inputs:
            if isinstance(input, plant.PlantImage):
                band_args.append(input.get_band(band=b))
            else:
                band_args.append(input)
        band_operation_obj = PlantBandOperation(*band_args,
                                                **inputs_kwargs)
        for band_obj in band_args:
            if not isinstance(band_obj, PlantBand):
                continue
            band_obj._notify_changes_list.append(
                band_operation_obj)
        out_image_obj.set_band(band_operation_obj, band=b,
                               realize_changes=False)
    plant.prepare_image(out_image_obj)
    return out_image_obj


def algebraic_decorator(operation):
    def decorated(f):
        def wrapper(*args, **kwargs):
            if isinstance(args[1], str) and operation=='__add__':
                return args[0].name+args[1]
            elif isinstance(args[1], str) and operation=='__radd__':
                return args[1]+args[0].name
            # print('WARNING algebraic operations with PlantImage'
            #       ' objects are still in development')
            kwargs = {}
            kwargs['operation'] = operation
            out_image_obj = _create_PlantBandOperation(
                None, args, kwargs)
            '''
            out_image_obj = image_ref.soft_copy()
            for b in range(max_nbands):
                band_args = []
                for arg in args:
                    if isinstance(arg, plant.PlantImage):
                        band_args.append(arg.get_band(band=b))
                    else:
                        band_args.append(arg)
                band_operation_obj = PlantBandOperation(*band_args,
                                                        **kwargs)
                for band_obj in band_args:
                    if not isinstance(band_obj, PlantBand):
                        continue
                    # print('*** adding band to notification list'
                    #      f' of id: {id(band_obj)}:', band_obj)
                    band_obj._notify_changes_list.append(
                        band_operation_obj)
                # print('*** operation obj created (2): ',
                #       id(band_operation_obj))
                # print('*** band_operation_obj.image: ',
                #       band_operation_obj.image)
                out_image_obj.set_band(band_operation_obj, band=b)
            # print('*** band_operation_obj: ', band_operation_obj)
            '''
            return out_image_obj
        return wrapper
    return decorated


'''
@algebraic_decorator('__sin__')
def sin(input_data, **kwargs):
    pass


@algebraic_decorator('__cos__')
def cos(input_data, **kwargs):
    pass
'''


class ExtendedNdarray(np.ndarray):

    # def __init__(self, parent, *args, **kwargs):
    #    print('*** parent: ', parent)
    #    ret = super(ExtendedNdarray, self).__init__(*args, **kwargs)
    #    return ret

    def __new__(cls, parent_obj, band, *args, **kwargs):
        cls.parent_obj = parent_obj
        cls.band = band
        # ret = super(ExtendedNdarray, self).__init__(*args, **kwargs)
        obj = np.ndarray.__new__(cls, *args, **kwargs)
        return obj

    def __getitem__(self, key):
        # print('*** getitem: ', key)
        # ret = super(ExtendedNdarray, self).__getitem__(key)
        if (isinstance(key, slice) and key.start is None and
                key.stop is None and key.step is None):
            image_obj = self.parent_obj
        else:
            image_obj = self.parent_obj.__getitem__(key)
        image = image_obj.get_array(band=self.band)
        new_ndarray = np.ndarray(
            buffer=image,
            shape=image.shape,
            dtype=image.dtype)
        return new_ndarray


class PlantBandBase():

    def initialize_parameters(self):
        self._applied_transform_obj_crop_header_list = []
        self._applied_transform_obj_crop_image_list = []
        self._applied_transform_obj_list = None

        self._notify_changes_list = []
        self._notify_changes_locker = False
        self._image = None
        # self._full_name = None
        self._name = None
        self._ctable = None
        self._dtype = None
        self._null = np.nan
        self._stats = None
        self._parent_list = None
        self._parent_orig_band_index = None
        self._parent_orig = None
        self._parent_orig_band_orig_index = None
        # self._locked = False

    def notify_changes(self):
        if self._notify_changes_locker:
            return
        # print(f'*** notifying bands (id: {id(self)}): ',
        #       len(self._notify_changes_list))
        self._notify_changes_locker = True
        for band_obj in self._notify_changes_list:
            # print('*** notifying band: ', band_obj)
            _ = band_obj.get_image()
        self._notify_changes_list = []
        self._notify_changes_locker = False

    '''
    def deepcopy(self):
        new = PlantBand()
        for key, value in self.__dict__.items():
            new.__dict__[key] = plant.get_immutable_value(value)
        return new
    '''

    def copy(self):
        new = PlantBand()
        for key, value in self.__dict__.items():
            new.__dict__[key] = plant.get_immutable_value(value)
        return new

    def soft_copy(self):
        new = PlantBand()
        for key, value in self.__dict__.items():
            if key == '_image':
                continue
            new.__dict__[key] = plant.get_immutable_value(value)
        return new

    # def copy_parameters(self):
    #    return deepcopy(self)

    def to_array(self):
        return self._image

    @property
    def image_loaded(self):
        flag_image_loaded = self._image is not None
        return flag_image_loaded

    @property
    def dataType(self):
        return self.dtype

    @property
    def dtype(self):
        if self.image_loaded:
            self._dtype = plant.get_dtype_name(self._dtype)
        return self._dtype

    @dtype.setter
    def dtype(self, val):
        self._dtype = val
        if self.image_loaded:
            self._image = np.asarray(self._image,
                                     dtype=val)

    @property
    def ctable(self):
        return self._ctable

    @ctable.setter
    def ctable(self, val):
        if isinstance(val, dict):
            ctable = gdal.ColorTable()
            for key, value in val.items():
                ctable.SetColorEntry(int(key), value)
            self._ctable = ctable
        elif val is None:
            self._ctable = None
        else:
            self._ctable = val.Clone()

    @property
    def name(self, default=None):
        if self._name:
            return self._name
        if default is not None:
            return default
        parent_orig_obj = self.parent_orig
        name = os.path.basename(parent_orig_obj.name)
        if parent_orig_obj.nbands != 1:
            b = self.parent_band_index
            name += f' (band {b})'
        # self._name = name
        return self._name

    '''
    @property
    def full_name(self):
        parent_obj = self.parent_list[-1]
        parent_orig_obj = self.parent_orig
        flag_all_same_image_obj = True
        ref_image_obj = None
        # check if all bands are originated from the same
        #     image_obj (file)
        # print('*** self.parent_list: ', len(self.parent_list))
        # print('*** parent_list:', [id(f) for f in self.parent_list])
        # print('*** parent_obj.nbands: ', parent_obj.nbands)
        for b in range(parent_obj.nbands):
            current_image_obj = parent_obj.get_band(
                band=b).parent_orig
            if ref_image_obj is None:
                ref_image_obj = current_image_obj
                continue
            if ref_image_obj is not current_image_obj:
                # print('*** ref_image_obj: ', id(ref_image_obj))
                # print('*** current_image_obj: ', id(current_image_obj))
                flag_all_same_image_obj = False
                break
        # print('*** flag_all_same_image_obj: ', flag_all_same_image_obj)
        if flag_all_same_image_obj:
            return self._name
        if not self.name and parent_orig_obj.nbands == 1:
            return parent_orig_obj.name
        if not self.name:
            b = self.parent_band_index
            return parent_orig_obj.name + f' (band {b})'
        return parent_orig_obj.name + f' ({self.name})'


    @property
    def name(self):
        return self._name
    '''

    @name.setter
    def name(self, val):
        self._name = val

    @property
    def null(self):
        return self._null

    @null.setter
    def null(self, val):
        self._null = val

    @property
    def stats(self):
        return self._stats

    @stats.setter
    def stats(self, val):
        self._stats = val

    @property
    def parent_list(self):
        return self._parent_list

    @parent_list.setter
    def parent_list(self, val):
        self._parent_list = val

    @property
    def parent_band_index(self):
        for parent_obj in reversed(self.parent_list):
            for b in range(parent_obj.nbands):
                if parent_obj.get_band(band=b) is self:
                    return b
        return None

    @property
    def parent_orig_band_index(self):
        return self._parent_orig_band_index

    @parent_orig_band_index.setter
    def parent_orig_band_index(self, val):
        self._parent_orig_band_index = val

    @property
    def parent_orig(self):
        return self._parent_orig

    @parent_orig.setter
    def parent_orig(self, val):
        self._parent_orig = val

    @property
    def parent_orig_band_orig_index(self):
        return self._parent_orig_band_orig_index

    @parent_orig_band_orig_index.setter
    def parent_orig_band_orig_index(self, val):
        self._parent_orig_band_orig_index = val

    def __getattr__(self, name):
        '''
        subprocess execution wrapper (API)
        '''
        if name in self.__dir__():
            return
        if not name.startswith('__'):
            wrapper_obj = plant.ModuleWrapper(name, self)
            if wrapper_obj._module_obj:
                return wrapper_obj
        try:
            ret = getattr(self.parent_orig, name)
            flag_attribute_error = False
        except AttributeError:
            flag_attribute_error = True
        if flag_attribute_error:
            raise AttributeError(
                f'{self.__class__.__name__}.{name} is invalid.')
        return ret

    # getPlantTransformObj = plant_transform_obj.fget
    getImageType = dtype.fget
    getCTable = ctable.fget
    getName = name.fget
    # getFullName = full_name.fget
    getNull = null.fget
    getStats = stats.fget

    # get_plant_transform_obj = plant_transform_obj.fget
    get_image_type = dtype.fget
    get_ctable = ctable.fget
    # get_full_name = full_name.fget
    get_name = name.fget
    get_null = null.fget
    get_stats = stats.fget

    # setPlantTransformObj = plant_transform_obj.fset
    setImageType = dtype.fset
    setCTable = ctable.fset
    setName = name.fset
    setNull = null.fset
    setStats = stats.fset

    # set_plant_transform_obj = plant_transform_obj.fset
    set_image_type = dtype.fset
    set_ctable = ctable.fset
    set_name = name.fset
    set_null = null.fset
    set_stats = stats.fset


class PlantImage():

    def __new__(cls, *args, **kwargs):
        input_data = args[0] if len(args) > 0 else None
        input_data_kw = kwargs.pop('input_data', None)
        ref_image_obj = kwargs.pop('ref_image_obj', None)

        if isinstance(ref_image_obj, PlantImage):
            ref_image_obj.init(*args, **kwargs)
            return ref_image_obj

        if isinstance(input_data, PlantImage):
            input_data.init(**kwargs)
            return input_data

        if input_data is None:
            input_data = input_data_kw
        if isinstance(input_data, PlantImage):
            input_data.init(**kwargs)
            return input_data
        new = super(PlantImage, cls).__new__(cls)
        # print('*** new PlantImage created: ', id(new))
        new.initialize_parameters()
        new.init(input_data, **kwargs)
        return new

    # @algebraic_decorator('__neg__')
    # def __neg__(self):
    #    pass
    # @algebraic_decorator('__pow__')
    # def __pow__(self, *args):
    #     pass
    def __len__(self):
        return self.nbands

    @algebraic_decorator('__add__')
    def __add__(self, image_2_obj):
        pass

    @algebraic_decorator('__sub__')
    def __sub__(self, image_2_obj):
        pass

    @algebraic_decorator('__mul__')
    def __mul__(self, image_2_obj):
        pass

    @algebraic_decorator('__div__')
    def __div__(self, image_2_obj):
        pass

    @algebraic_decorator('__pow__')
    def __pow__(self, *args):
        pass

    @algebraic_decorator('__sin__')
    def sin(input_data, **kwargs):
        pass

    @algebraic_decorator('__cos__')
    def cos(input_data, **kwargs):
        pass

    @algebraic_decorator('__radd__')
    def __radd__(self, image_2_obj):
        pass

    @algebraic_decorator('__rsub__')
    def __rsub__(self, image_2_obj):
        pass

    @algebraic_decorator('__rmul__')
    def __rmul__(self, image_2_obj):
        pass

    @algebraic_decorator('__rdiv__')
    def __rdiv__(self, image_2_obj):
        pass

    @algebraic_decorator('__rpow__')
    def __rpow__(self, *args):
        pass

    def initialize_parameters(self):
        self._plant_transform_obj = None
        self._applied_transform_obj_crop_list = []
        # self._realize_changes_locked = True
        self._realize_changes_locked = False

        self._filename_orig = None
        self._filename = None
        self._header_file = None
        self._band_list = []
        self._file_format = None
        self._geotransform = None
        self._gcp_list = None
        self._gcp_projection = None
        self._scheme = None
        self._nbands = None
        self._nbands_orig = None
        self._band_orig = None
        self._sampling_step_x = None
        self._sampling_step_y = None
        self._sampling_step_z = None
        self._input_key = None
        # self._ctable = None
        self._width = None
        self._length = None
        self._depth = None
        # self._dtype = None
        self._name = None
        self._projection = None
        self._metadata = None
        # self._description = None
        self._null = np.nan
        # self._stats = None
        self._offset_x = None
        self._offset_y = None
        self._width_orig = None
        self._length_orig = None
        self._depth_orig = None
        self._selected_band = 0

    def init(self, *args, **kwargs):

        # plant.debug('args: ', args)
        # plant.debug('kwargs: ', kwargs)
        input_data = args[0] if len(args) > 0 else None
        if input_data is None:
            input_data = kwargs.pop('input_data', None)
        filename_orig = kwargs.pop('filename_orig', None)
        filename = kwargs.get('filename', None)
        header_file = kwargs.get('header_file', None)
        # args = [filename]
        image = kwargs.get('image', None)
        # dtype = kwargs.get('dtype', None)
        name = kwargs.get('name', None)
        projection = kwargs.get('projection', None)
        file_format = kwargs.get('file_format', None)
        geotransform = kwargs.get('geotransform', None)
        gcp_list = kwargs.get('gcp_list', None)
        gcp_projection = kwargs.get('gcp_projection', None)

        scheme = kwargs.get('scheme', None)
        nbands = kwargs.get('nbands', None)
        nbands_orig = kwargs.get('nbands_orig', None)
        band_orig = kwargs.get('band_orig', None)
        sampling_step = kwargs.pop('sampling_step', None)
        sampling_step_x = kwargs.pop('sampling_step_x', None)
        sampling_step_y = kwargs.pop('sampling_step_y', None)
        sampling_step_z = kwargs.pop('sampling_step_z', None)
        input_key = kwargs.get('input_key', None)

        # ctable = kwargs.get('ctable', None)
        metadata = kwargs.get('metadata', None)
        # description = kwargs.get('description', None)
        null = kwargs.get('null', np.nan)
        # stats = kwargs.get('stats', None)
        width_orig = kwargs.get('width_orig', None)
        length_orig = kwargs.pop('length_orig', None)
        depth_orig = kwargs.pop('depth_orig', None)
        verbose = kwargs.get('verbose', False)
        # byteorder
        plant_transform_obj = kwargs.get('plant_transform_obj', None)
        realize_changes_locked = kwargs.get('realize_changes_locked', None)

        '''
        applied_transform_obj_crop_list = \
            kwargs.get('applied_transform_obj_crop_list', None)
        applied_transform_obj_list = \
            kwargs.get('applied_transform_obj_list', None)
        '''
        width = kwargs.get('width', None)
        length = kwargs.get('length', None)
        depth = kwargs.get('depth', None)
        offset_x = kwargs.get('offset_x', None)
        offset_y = kwargs.get('offset_y', None)
        # sampling_step = kwargs.pop('sampling_step_x', None)

        update = kwargs.get('update', False)

        if (plant_transform_obj is not None and
                isinstance(plant_transform_obj, plant.PlantTransform)):
            plant_transform_obj = plant.PlantTransform(
                plant_transform_obj=plant_transform_obj,
                verbose=verbose,
                width=width,
                length=length,
                depth=depth,
                offset_x=offset_x,
                offset_y=offset_y)
            self._plant_transform_obj = plant_transform_obj
        elif plant_transform_obj is not None:
            self._plant_transform_obj = plant_transform_obj

        '''
        if applied_transform_obj_crop_list:
            self._applied_transform_obj_crop_list = \
                applied_transform_obj_crop_list
        if applied_transform_obj_list:
            self._applied_transform_obj_list = \
                applied_transform_obj_list
        '''
        if isinstance(input_data, PlantImage):
            if update:
                input_data.realize_changes()
            self.__dict__.update(input_data.__dict__)
        elif isinstance(input_data, PlantBand):
            # self._band_list = [input_data]
            image = input_data.image
        elif isinstance(input_data, str):
            _ = self.read_image(
                input_data,
                method_kwargs_dict=plant.get_kwargs_dict_read_image(),
                ref_image_obj=self,
                **kwargs)
            # self.__dict__.update(this_obj.__dict__)
        elif input_data is not None:
            image = input_data

        if filename_orig is not None:
            self._filename_orig = filename_orig

        if filename is not None:
            self._filename = filename

        if header_file is not None:
            self._header_file = header_file

        if (self._filename is None and
                self._filename_orig is not None):
            self._filename = self._filename_orig
        if (self._filename_orig is None and
                self._filename is not None):
            self._filename_orig = self._filename
            # print('*** updated (2): ', self._filename_orig)

        if self._name is None:
            self._name = name

        if nbands is not None:
            self._nbands = nbands
        elif len(self._band_list) != 0 and self._nbands is None:
            self._nbands = len(self._band_list)

        if nbands_orig is not None:
            self._nbands_orig = nbands_orig
        elif len(self._band_list) != 0 and self._nbands_orig is None:
            self._nbands_orig = len(self._band_list)

        if self._nbands is None and self._nbands_orig is not None:
            self._nbands = self._nbands_orig
        elif self._nbands_orig is None and self._nbands is not None:
            self._nbands_orig = self._nbands

        if (self._band_orig is None and band_orig is not None and
            (isinstance(band_orig, Sequence) or
             isinstance(band_orig, np.ndarray))):
            self._band_orig = band_orig
        elif self._band_orig is None and band_orig is not None:
            self._band_orig = [band_orig]
        elif (self._band_orig is None and
              self._nbands_orig is not None and
              self._nbands_orig == 1):
            self._band_orig = [0]

        if self._sampling_step_x is None and sampling_step_x is None:
            self._sampling_step_x = sampling_step
        elif self._sampling_step_x is None:
            self._sampling_step_x = sampling_step_x
        if self._sampling_step_y is None and sampling_step_y is None:
            self._sampling_step_y = sampling_step
        elif self._sampling_step_y is None:
            self._sampling_step_y = sampling_step_y
        if self._sampling_step_z is None and sampling_step_z is None:
            self._sampling_step_z = sampling_step
        elif self._sampling_step_z is None:
            self._sampling_step_z = sampling_step_z

        if self._input_key is None:
            self._input_key = input_key

        # if self._null is None:
        #     self._null = null
        '''
        if plant.isvalid(float(null)):
            self._null = null
        elif (not plant.isvalid(float(self._null)) and
              (self.dtype is not None and
               'int' in plant.get_dtype_name(self.dtype))):
            self._null = plant.INT_NAN
        '''
        if plant.isvalid(null, null=np.nan):
            self._null = null
        elif (not plant.isvalid(self._null, null=np.nan) and
              (self.dtype is not None and
               'int' in plant.get_dtype_name(self.dtype))):
            self._null = plant.INT_NAN

        # if image is not None and (isinstance(image, list) or
        #                           isinstance(image, tuple)):
        # kwargs['parent'] = self
        # kwargs['parent_orig'] = self

        if (image is not None and hasattr(image, "__getitem__") and
                # not isinstance(image, np.ndarray) and
                isinstance(image, Sequence) and
                len(image) > 0 and
                ((self._nbands is not None and
                  self._nbands == len(image)) or
                 isinstance(image[0], np.ndarray) or
                 isinstance(image[0], plant.PlantImage) or
                 isinstance(image[0], plant.PlantBand) or
                 (len(image) > 1 and
                  plant.get_sequence_depth(image) > 2))):
            self._band_list = [PlantBand(band, **kwargs)
                               for band in image]
        elif (image is not None and hasattr(image, "__getitem__") and
              # not isinstance(image, np.ndarray) and
              isinstance(image, Sequence) and
              len(image) > 0):
            self._band_list = [PlantBand(image, **kwargs)]
        elif image is not None:
            self._band_list = [PlantBand(image, **kwargs)]

        if (len(self._band_list) == 0 and
                self._nbands is not None):
            self._band_list = []
            for current_band in range(self._nbands):
                self._band_list.append(PlantBand(**kwargs))
        elif image is not None and len(self._band_list) == 0:
            self._band_list = [PlantBand(**kwargs)]
        if file_format is not None:
            self._file_format = file_format
        elif (self._file_format is None and
              self._filename_orig is not None):
            self._file_format = plant.get_output_format(
                self._filename_orig, file_format)
        elif (self._file_format is None and
              self._filename is not None):
            self._file_format = plant.get_output_format(
                self._filename, file_format)
        if geotransform is not None:
            self._geotransform = geotransform
        if gcp_list is not None:
            self._gcp_list = gcp_list
        if gcp_projection is not None:
            self._gcp_projection = gcp_projection

        if scheme is not None:
            self._scheme = scheme
        if self._nbands is None:
            self._nbands = len(self._band_list)

        if self._nbands_orig is None:
            self._nbands_orig = len(self._band_list)
        if self._band_orig is None and self._nbands == self._nbands_orig:
            self._band_orig = np.arange(self._nbands_orig)

        '''
        if nbands is not None:
            self._nbands = nbands
        elif len(self._band_list) != 0:
            self._nbands = len(self._band_list)

        if nbands_orig is not None:
            self._nbands_orig = nbands_orig
        elif len(self._band_list) != 0:
            self._nbands_orig = len(self._band_list)
        '''

        # if ctable is not None:
        #    self._ctable = ctable
        if width is not None:
            self._width = int(width)
        else:
            for b in range(self._nbands):
                if (len(self._band_list) != 0 and
                        self._band_list[b].image_loaded and
                        len(self._band_list[b].image.shape) >= 1):
                    self._width = self._band_list[b].image.shape[-1]
                elif (len(self._band_list) != 0 and
                      self._band_list[b].image_loaded):
                    self._width = 1
                if self._width is not None:
                    break

        if length is not None:
            self._length = int(length)
        else:
            for b in range(self._nbands):
                if (len(self._band_list) != 0 and
                        self._band_list[b].image_loaded and
                        len(self._band_list[b].image.shape) >= 2):
                    self._length = self._band_list[b].image.shape[-2]
                elif (len(self._band_list) != 0 and
                      self._band_list[b].image_loaded):
                    self._length = 1
                if self._length is not None:
                    break

        if (width is not None and
                length is not None and
                depth is None):
            depth = 1

        if depth is not None:
            self._depth = int(depth)
        else:
            for b in range(self._nbands):
                if (len(self._band_list) != 0 and
                        self._band_list[b].image_loaded and
                        len(self._band_list[b].image.shape) >= 3):
                    self._depth = self._band_list[b].image.shape[-3]
                elif (len(self._band_list) != 0 and
                      self._band_list[b].image_loaded):
                    self._depth = 1
                if self._depth is not None:
                    break

        # if dtype is not None:
        #     self._dtype = dtype
        # elif len(self._image_list) != 0:
        #    self._dtype = plant.get_dtype_name(self._image_list[0])

        # else:
        #     self._dtype = None
        if projection:
            self._projection = projection
        if metadata:
            self._metadata = metadata
        # if description:
        #     self._description = description

        # if stats is not None:
        #    self._stats = stats  # min, max, offset, scale
        if offset_x is not None:
            self._offset_x = offset_x
        if self._offset_x is None:
            self._offset_x = 0
        if offset_y is not None:
            self._offset_y = offset_y
        if self._offset_y is None:
            self._offset_y = 0
        if width_orig is not None:
            self._width_orig = width_orig
        elif self._width_orig is None:
            self._width_orig = self._width

        if length_orig is not None:
            self._length_orig = length_orig
        elif self._length_orig is None:
            self._length_orig = self._length

        if depth_orig is not None:
            self._depth_orig = depth_orig
        elif self._depth_orig is None:
            self._depth_orig = self._depth

        if realize_changes_locked is not None:
            # print(f'*** {id(self)} locked:', realize_changes_locked)
            self._realize_changes_locked = realize_changes_locked

        # if len(self._band_list) >= 1:
        if self._nbands == 0 or len(self._band_list) == 0:
            return
        for i, band in enumerate(self._band_list):
            if i > self._nbands:
                break
            if (band.parent_orig_band_orig_index is None and
                    self._band_orig is None):
                band.parent_orig_band_orig_index = i
            elif band.parent_orig_band_orig_index is None:
                band.parent_orig_band_orig_index = self._band_orig[i]
            if band.parent_orig is None:
                band.parent_orig = self
            if band.parent_orig_band_index is None:
                band.parent_orig_band_index = i
            if band.parent_list is None:
                band.parent_list = [self]
            # print('*** new band parent_orig: ',
            #      id(band.parent_orig))
        # if plant.isvalid(null):
        #     self._null = null
        # elif (self._dtype is not None and
        #        'int' in str(self._dtype)):
        #    self._null = plant.INT_NAN

        # because of a bug in ISCE:
        # if ('int' in plant.get_dtype_name(self._dtype) and
        #        len(self._image_list) != 0 and
        #        self._nbands is not None):
        #    for b in range(self._nbands):
        #        if (self._image_list is not None and
        #                b <= len(self._image_list)-1):
        #            self._image_list[b] = np.asarray(self._image_list[b],
        #                                             dtype=np.int32)
        #        self._dtype = np.int32
        # if self._plant_transform_obj is None:
        # self._plant_transform_obj = plant_transform_obj
        # print(self._plant_transform_obj)
        # print(plant_transform_obj)

    def __repr__(self):
        '''
        id_str = plant.get_obj_id(self)
        old_stdout = sys.stdout
        mystdout = StringIO()
        print('PLAnT image:')
        print('    '+id_str)
        plant.get_info(self)
        sys.stdout = old_stdout
        # ret = id_str+'\n'
        ret = mystdout.getvalue()
        '''
        ret = f"PlantImage('{self.name}')"
        return ret

    def __str__(self):
        if self.filename_orig:
            return self.filename_orig
        # if self.filename:
        if self.filename:
            return self.filename
        # return self.name
        id_str = plant.get_obj_id(self)
        return f'MEM:{id_str}'
        # return 'PLAnT Image'

    def deepcopy(self):
        return self.deep_copy()

    def deep_copy(self, ref_image_obj=None):
        if ref_image_obj is None:
            ref_image_obj = PlantImage()
        for key, value in self.__dict__.items():
            ref_image_obj.__dict__[key] = \
                plant.get_immutable_value(value)
        ref_image_obj._band_list = [self.get_band(band=b).copy()
                                    for b in range(self.nbands)]
        return ref_image_obj

    def copy(self, ref_image_obj=None):
        if ref_image_obj is None:
            ref_image_obj = PlantImage()
        for key, value in self.__dict__.items():
            ref_image_obj.__dict__[key] = \
                plant.get_immutable_value(value)
        return ref_image_obj

    def soft_copy(self, ref_image_obj=None):
        if ref_image_obj is None:
            ref_image_obj = PlantImage()
        for key, value in self.__dict__.items():
            if key == '_band_list':
                continue
            ref_image_obj.__dict__[key] = \
                plant.get_immutable_value(value)
        ref_image_obj._band_list = \
            [PlantBand(None) for b in range(self.nbands)]
        # ref_image_obj.nbands = 0
        return ref_image_obj

    # def copy(self):
    #     # self.realize_changes()
    #     return deepcopy(self)

    def to_array(self):
        '''
        if all_bands:
            image_list = [self.get_image(band=b)
                          for b in range(self.nbands)]
            return np.asarray(image_list)
        '''
        args = []
        for b in range(self.nbands):
            args.append(self.get_image(band=b))
        stacked_image = np.dstack(args)
        return stacked_image

    def realize_changes(self, band=None):
        if self._realize_changes_locked:
            # print('*** rc is locked')
            return
        self.lock_realize_changes()
        # print('*** locking rc')
        if band is None:
            band_list = range(self.nbands)
        else:
            band_list = [band]
        for b in band_list:
            band = self.get_band(band=b)

            if (band.parent_orig is not None and
                    band.parent_orig is not self and
                    band.parent_orig._realize_changes_locked):
                # print('*** parent is locked')
                continue

            self.get_image(band=b)
        # print('*** unlocking rc')
        self.unlock_realize_changes()

    def lock_realize_changes(self):
        # print(f'*** {id(self)} locked')
        self._realize_changes_locked = True

    def unlock_realize_changes(self, unlock_children=False):
        # print(f'*** {id(self)} unlocked')
        self._realize_changes_locked = False
        if not unlock_children:
            return
        band_list = range(self.nbands)
        for b in band_list:
            band = self.get_band(band=b)
            if not band.parent_orig._realize_changes_locked:
                continue
            band.parent_orig.unlock_realize_changes()

    @property
    def plant_transform_obj(self):
        return self._plant_transform_obj

    @plant_transform_obj.setter
    def plant_transform_obj(self, val):
        # print('*** update plant_transform_obj')
        # self.realize_changes()
        self._plant_transform_obj = val

    @property
    def crop_window(self):
        # print('*** get crop_window called')
        default_crop_window = [0, 0, self.width, self.length]
        if self._plant_transform_obj is None:
            return default_crop_window
        crop_window = self._plant_transform_obj.crop_window
        if crop_window is None:
            return default_crop_window
        for i, value in enumerate(crop_window):
            if plant.isvalid(value):
                continue
            crop_window[i] = default_crop_window[i]
        return crop_window

    @property
    def name(self, band=None, default=None):
        if self._name is not None:
            return self._name
        if default is not None:
            return default
        if band is not None:
            band_obj = self.get_band(band=band)
            return band_obj.get_name(default='')
        if self.nbands == 1:
            band = self._selected_band
            band_obj = self.get_band(band=band)
            name = self._band_list[band].get_name(default='')
            if name:
                return name
        name = os.path.basename(self.filename)
        return name

    # @property
    # def name(self):
    #     return self._name

    @name.setter
    def name(self, val, band=None):
        if band is None:
            self._name = val
            if self.nbands > 1:
                return
            band = 0
        band = self.get_band(band=band)
        band.set_name(val)

    @property
    def filename_orig(self):
        if self._filename_orig:
            # print('*** _filename_orig: ', self._filename_orig)
            return self._filename_orig
        arg_id = str(id(self))
        # plant.plant_config.variables[arg_id] = value
        # variables_to_pop.append(arg_id)
        value_str = f'MEM:{arg_id}'
        return value_str

    @filename_orig.setter
    def filename_orig(self, val):
        # print('*** update filename_orig')
        self.realize_changes()
        # print('*** setting with : ', val)
        self._filename_orig = val

    @property
    def filename(self):
        if self._filename:
            return self._filename
        # if not self.filename_orig:
        #     return
        '''
        if plant.IMAGE_NAME_SEPARATOR not in self.filename_orig:
            self._filename = self.filename_orig
        file_splitted = self.filename_orig.split(plant.IMAGE_NAME_SEPARATOR)
        if len(file_splitted) > 2:
            self._filename = file_splitted[1]
        else:
            self._filename = file_splitted[0]
        '''
        ret_dict = plant.parse_filename(self.filename_orig)
        self._filename = ret_dict['filename']
        return self._filename

    @filename.setter
    def filename(self, val):
        # print('*** update filename')
        self.realize_changes()
        self._filename = val

    @property
    def header_file(self):
        if self._header_file is not None:
            return self._header_file
        filename = self.filename
        if not filename:
            return
        if (self.file_format is None or
                'ENVI' in self.file_format.upper()):
            header_file = plant.get_envi_header(filename)
            if plant.isfile(header_file):
                self._header_file = header_file
        if (self.file_format is None or
                ('ISCE' in self.file_format.upper() and
                 plant.isfile(filename+'.xml'))):
            self._header_file = filename+'.xml'
        if (self.file_format is None or
                ('ISCE' in self.file_format.upper() and
                 filename.endswith('.xml'))):
            self._header_file = filename
        if (self.file_format is None or
                ('ANN' in self.file_format.upper())):
            header_dict = plant.parse_uavsar_filename(filename)
            header_file = plant.get_annotation_file(
                header_dict, dirname=os.path.dirname(filename))
            if plant.isfile(header_file):
                self._header_file = header_file
        return self._header_file

    @header_file.setter
    def header_file(self, val):
        self.realize_changes()
        self._header_file = val

    def add_band(self, new_band, **kwargs):
        self.set_image(new_band, band=self.nbands, **kwargs)

    @property
    def band(self, band=None):
        if band is None:
            band = self._selected_band
        if band < 0:
            print(f'ERROR invalid band: {band}')
            return
        elif len(self._band_list) == 0:
            print(f'ERROR image object has {len(self._band_list)}'
                  ' band(s)')
            return
        elif band > self.nbands - 1:
            print(f'ERROR error reading band: {band}.'
                  f' Image object has only {self.nbands} band(s)')
            return
        band_obj = self._band_list[band]
        '''
        if band_obj is not None and not band_obj.image_loaded:
            _ = band_obj.image
        '''
        return band_obj

    @band.setter
    def band(self, new_band, band=None, realize_changes=None):
        '''
        print('*** asking for new_band name attr for id:', id(new_band))
        with plant.PlantIndent():
            name = new_band.name
        print('*** new_band name', name)
        print('*** new_band _name',
              new_band._name, new_band.image_loaded, id(new_band),
              new_band.__class__)
        '''

        if new_band is None:
            print('WARNING invalid argument for band creation: None')
            return
        if realize_changes is None:
            # if self is responsible to load band => realize changes
            realize_changes = False
            for b in range(self.nbands):
                if b > len(self._band_list)-1:
                    break
                current_band = self._band_list[b]
                if current_band.parent_orig is self:
                    realize_changes = True
                    break

        if not isinstance(new_band, PlantBand):
            self.set_image(new_band, band=band)
            return

        if band is None:
            band = self._selected_band

        if (band <= len(self._band_list)-1 and
                self._band_list[band].image_loaded):
            # print('*** band setter notify_changes')
            self._band_list[band].notify_changes()

        # print('*** band: ', band)
        # print('*** len(self._band_list): ', len(self._band_list))
        # if band < len(self._band_list):
        #    # if self._band_list[band]._locked:
        #    #    raise
        #    # self._band_list[band]._locked = True
        # realize_changes = True

        if realize_changes:
            self.realize_changes()

        if realize_changes and not new_band.image_loaded:
            new_band.get_image()
            # if not new_band.image_loaded:
            #     raise
        if new_band.parent_orig is None:
            new_band.parent_orig = self
        '''
                if (band.parent_orig_band_orig_index is None and
                        self._band_orig is None):
                    band.parent_orig_band_orig_index = i
                elif band.parent_orig_band_orig_index is None:
                    band.parent_orig_band_orig_index = self._band_orig[i]
        '''
        if (new_band.parent_orig_band_orig_index is None and
                self._band_orig is not None):
            # new_band.parent_orig_band_orig_index = band
            # elif new_band.parent_orig_band_orig_index is None:
            new_band.parent_orig_band_orig_index = \
                self._band_orig[band]
        # print('*** here 1')
        if new_band.parent_list is None:
            new_band.parent_list = [self]
        else:
            new_band.parent_list.append(self)
            # print('*** here (a)', [id(f) for f in new_band.parent_list])
        if new_band.parent_orig_band_index is None:
            new_band.parent_orig_band_index = band
        # band = 0 if band is None else band
        '''
        print('*** aaaaaaaaaaaaaaaa b3 (after)',
              self._band_list[0]._name, id(self))

        print('*** new_band before loading 4',
              new_band.name, new_band.image_loaded)
        print('*** new_band before loading 4 _name',
              new_band._name, new_band.image_loaded)
        '''

        if (band <= len(self._band_list)-1):
            self._band_list[band] = new_band
            # print('*** aaaaaaaaaaaaaaaa b4.1 (after)',
            #       self._band_list[0]._name, id(self), band)

        else:
            for i in range(len(self._band_list), band+1):
                self._band_list.append(PlantBand(None))
            self._band_list[band] = new_band
        
        if new_band.image_loaded:
            self.update_parameters_with_image(new_band.image)
        elif (new_band.parent_orig is not None and
              new_band.parent_orig is not self):
            self.update_parameters_with_image_obj(new_band.parent_orig)
        nbands_observed = max([0]+[b for b, current_band in
                                   enumerate(self._band_list)
                                   if current_band._image is not None])+1
        self._nbands = max([self._nbands, nbands_observed, band+1])
        # self._band_list[band]._locked = False

    def update_parameters_with_image(self, image):
        # shape = self._depth, self._length, self._width
        new_shape = plant.get_image_dimensions(image)
        self._depth, self._length, self._width = new_shape
        # if shape != new_shape and self.geotransform is not None:
        #     print(f'WARNING georeference of image obj "{self}"'
        #           ' invalid for new shape. Removing georeference...')
        #     self.geotransform = None
        #     self.projection = None
        if self._width_orig is None:
            self._width_orig = self._width
        if self._length_orig is None:
            self._length_orig = self._length
        if self._depth_orig is None:
            self._depth_orig = self._depth
        # if band == self._selected_band:
        #    self._dtype = plant.get_dtype_name(val)

    def update_parameters_with_image_obj(self, image_obj):
        if image_obj._width is not None:
            self._width = image_obj._width
        if image_obj._length is not None:
            self._length = image_obj._length
        if image_obj._depth is not None:
            self._depth = image_obj._depth
        if image_obj._width_orig is not None:
            self._width_orig = image_obj._width_orig
        if image_obj._length_orig is not None:
            self._length_orig = image_obj._length_orig
        if image_obj._depth_orig is not None:
            self._depth_orig = image_obj._depth_orig
        if image_obj.geotransform is not None:
            self._geotransform = image_obj.geotransform
        # if (image_obj._dtype is not None and
        #        band == self._selected_band):
        #    self._dtype = image_obj._dtype

    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs_orig):
        # args_str = ','.join([str(i) for i in inputs])
        kwargs = {}
        kwargs['ufunc_kwargs'] = kwargs_orig
        # kwargs['operation'] = f'{ufunc.__name__}({args_str})'
        kwargs['operation'] = f'{ufunc.__name__}'
        kwargs['ufunc'] = ufunc
        kwargs['method'] = method
        out_image_obj = _create_PlantBandOperation(
            self, inputs, kwargs)
        return out_image_obj

    def __getitem__(self, key):
        if (isinstance(key, slice) and key.start is None and
                key.stop is None and key.step is None):
            return self
        if (isinstance(key, slice) or
                isinstance(key, numbers.Number) or
                len(key) == 1):
            if isinstance(key, numbers.Number):
                band = [key]
            else:
                band = plant.expand_slice(key, default_stop=self.nbands)
            out_image_obj = self.soft_copy()
            out_image_obj.nbands = 0
            for i, b in enumerate(band):
                out_image_obj.set_band(self.get_band(band=b).copy(),
                                       band=i)
            return out_image_obj
        print('WARNING __getitem_ from PlantImage are not fully'
              ' implemented. Please use image_obj.image[key]'
              ' instead')
        print('*** key: ', key)
        key_y, key_x = key
        key_y_updated = plant.update_slice_cut(
            key_y, self.offset_y, size_cut=self.length)
        key_x_updated = plant.update_slice_cut(
            key_x, self.offset_x, size_cut=self.width)
        key = [key_y_updated, key_x_updated]
        print('*** key cut: ', key)
        flag_load_all_image = None
        if not self.image_loaded:
            flag_load_all_image = False
            n_elements = self.n_elements()
            n_elements_slice = self.n_elements(key=key)
            rate = float(n_elements_slice)/n_elements
            flag_load_all_image = rate > 0.5
        print('*** flag_load_all_image: ', flag_load_all_image)

        # if slice is small does not need to load parent image
        if (flag_load_all_image is False and
                not self.plant_transform_obj.flag_apply_crop()):
            out_image_obj = self.soft_copy()
            print('*** key: ', key)
            out_image_obj.plant_transform_obj.select_row = key[0]
            out_image_obj.plant_transform_obj.select_col = key[1]
            print('**** donneeeee',
                  id(out_image_obj.plant_transform_obj))
            out_image_obj.plant_transform_obj.update_crop_window(
                geotransform=out_image_obj.geotransform,
                projection=out_image_obj.projection,
                length_orig=out_image_obj.length_orig,
                width_orig=out_image_obj.width_orig)
            # if (id(out_image_obj.plant_transform_obj) in
            #        out_image_obj._applied_transform_obj_crop_list):
            #    out_image_obj._applied_transform_obj_crop_list.remove(
            #        id(out_image_obj.plant_transform_obj))
            plant.prepare_image(out_image_obj)
            return out_image_obj

        # otherwise load parent image and cut the slice
        kwargs = {}
        kwargs['operation'] = '__getitem__'
        kwargs['key'] = key
        transform_kwargs = {}
        if len(key) > 0:
            transform_kwargs['select_row'] = key[0]
        if len(key) > 1:
            transform_kwargs['select_col'] = key[1]
        plant_transform_obj = plant.PlantTransform(**transform_kwargs)
        out_image_obj = _create_PlantBandOperation(
            self, [self], kwargs, plant_transform_obj=plant_transform_obj)
        return out_image_obj

    def __setitem__(self, key, value):
        print('*** __setitem__ is slice: ',
              isinstance(key, slice))
        print('*** key: ', key)
        print('*** value: ', value)
        try:
            self.image.__setitem__(key, value)
        except ValueError:
            print('*** copying')
            self.image = self.image.copy()
            self.image.flags.writeable = True
            self.image.__setitem__(key, value)

    def __delitem__(self, key):
        raise NotImplementedError

    @property
    def image(self, *args, **kwargs):
        # def new_image(self, *args, **kwargs):
        # print('*** self.sampling_step:', self.sampling_step, id(self))
        # print('*** self.sampling_step x:', self.sampling_step_x, id(self))
        # print('*** self.sampling_step y:', self.sampling_step_y, id(self))
        image = self._get_image(*args, **kwargs)
        if image is not None and not PLANT_IMAGE_IS_MUTABLE:
            # return image.copy()
            image.flags.writeable = False
        # print('*** self.sampling_step (after):', self.sampling_step,
        #       id(self))
        # print('*** self.sampling_step x (after):', self.sampling_step_x,
        #       id(self))
        # print('*** self.sampling_step y (after):', self.sampling_step_y,
        #       id(self))
        return image

    @property
    def new_image(self, *args, **kwargs):
        # def image(self, *args, **kwargs):
        band = kwargs.pop('band', None)
        if band is None:
            band = self._selected_band
        if band < 0:
            print(f'ERROR invalid band: {band}')
            return
        elif len(self._band_list) == 0:
            print(f'ERROR image object has {len(self._band_list)}'
                  ' band(s)')
            return
        elif band > self.nbands - 1:
            print(f'ERROR error reading band: {band}.'
                  f' Image object has only {self.nbands} band(s)')
            return
        if (band < len(self._band_list) and
                self._band_list[band].image_loaded):
            image = self._band_list[band].array
            return image
        new_image = ExtendedNdarray(self,
                                    band,
                                    # buffer=image.data,
                                    shape=self.shape,
                                    dtype=self.dtype)
        return new_image

    @property
    def array(self, *args, **kwargs):
        return self.get_image(*args, **kwargs)
        # return self.new_image(*args, **kwargs)

    def _get_image(self, band=None):
        # input_band = band
        if band is None:
            band = self._selected_band
        if band < 0:
            print(f'ERROR invalid band: {band}')
            return
        elif len(self._band_list) == 0:
            print(f'ERROR image object has {len(self._band_list)}'
                  ' band(s)')
            return
        elif band > self.nbands - 1:
            print(f'ERROR error reading band: {band}.'
                  f' Image object has only {self.nbands} band(s)')
            return
        if (band < len(self._band_list) and
                self._band_list[band].image_loaded):
            image = self._band_list[band].array
            return image

        # different parent
        if (self._band_list[band].parent_orig is not None and
                self._band_list[band].parent_orig is not self):
            parent_orig = self._band_list[band].parent_orig
            band_orig = self._band_list[band].parent_orig_band_index

            image = parent_orig.get_array(band=band_orig)

            # the band obj may be different after the image is loaded
            new_band = parent_orig.get_band(band=band_orig)
            flag_resampling = (
                (self.sampling_step_x is not None and
                 self.sampling_step_x != 1) or
                (self.sampling_step_y is not None and
                 self.sampling_step_y != 1))
            if flag_resampling:
                print(f'*** resampling {parent_orig.input_key}!!!: ',
                      self.sampling_step_x,
                      self.sampling_step_y)
                parent_ref = parent_orig.soft_copy()
                parent_ref.set_band(new_band.deep_copy(),
                                       band=0)
                parent_ref.nbands = 1
                # print('*** before:', parent_ref.shape)
                parent_ref = plant.sample_image(
                    parent_ref,
                    # only_header=only_header,
                    # sampling_step=self.sampling_step,
                    sampling_step_x=self.sampling_step_x,
                    sampling_step_y=self.sampling_step_y)
                # print('*** after:', parent_ref.shape)
                # new_band.set_image(image)
                # new_band.set_width(image.shape[1])
                # new_band.set_length(image.shape[0])
                new_band = parent_ref.band
            else:
                parent_ref = parent_orig

            self._band_list[band] = new_band
            # print('*** band: ', band)
            # for b in range(len(self._band_list)):
            #     print('*** nnnnnnnnnnnnnn:', self._band_list[b].image.shape)

            # print(f'*** copying attrs from {id(parent_orig)} to {id(self)}')
            for key, value in parent_ref.__dict__.items():
                # if 'band' in key.lower() or key.startswith('__'):
                #     continue
                # if (value is not None and
                if key not in _IMAGE_OBJ_ATTRS_TO_UPDATE_AFTER_LOADING_IMAGE:
                    continue
                # if (flag_resampling and (key == 'width' or
                #                         key == 'length' or
                #                         key == 'depth')):
                #    continue
                self.__dict__[key] = value

            new_band.parent_list.append(self)
            if image is None:
                print(f'ERROR loading band {band} from'
                      f' {self._band_list[band].parent_orig.filename}')
            return image
        if self._band_orig is None or band > len(self._band_orig)-1:
            band_orig = band
        else:
            # try:
            band_orig = self._band_orig[band]
            # except:
            #     # fix this!!!!
            #     band_orig = band

        if self.filename_orig is None:
            return

        if (band_orig < len(self._band_list) and
            isinstance(self._band_list[band_orig],
                       plant.PlantBandOperation)):
            band_obj = self._band_list[band_orig]
            return band_obj.array

        # if self.plant_transform_obj is not None:
        #     band_obj._applied_transform_obj_crop_list = Non
        # with plant.PlantIndent():
        image_obj = self.read_image(
            self.filename_orig,
            band=band_orig,
            method_kwargs_dict=plant.get_kwargs_dict_read_image(),
            # ref_image_obj=self,
            flag_private_attributes=True,
            flag_exit_if_error=False,
            only_header=False)
        # print(f'***>>> band orig: ', band_orig)
        # print(f'***>>> nbands: ', image_obj.nbands)

        if image_obj is None:
            print(f'ERROR reading band {band_orig} from {self}')
            return

        # print(f'*** copying attrs from {id(image_obj)} to {id(self)}')
        for key, value in image_obj.__dict__.items():
            # if 'band' in key.lower() or key.startswith('__'):
            #     continue
            # if (value is None or
            if key not in _IMAGE_OBJ_ATTRS_TO_UPDATE_AFTER_LOADING_IMAGE:
                continue

            self.__dict__[key] = value
            # print(f'updating {key} to {value}: {self.__dict__[key]}')

        # self.__dict__.update(image_obj.__dict__)
        # print('*** set_band called (parent)', id(self))
        image_obj.band.parent_orig = self
        image_obj.band.parent_orig_band_orig_index = band_orig
        image_obj.band.parent_orig_band_index = band
        if (band < len(self._band_list)):
            band_obj = self._band_list[band]
            for key in _BAND_OBJ_ATTRS_TO_KEEP_AFTER_LOADING_IMAGE:
                if band_obj.__dict__[key] is None:
                    continue
                image_obj.band.__dict__[key] = band_obj.__dict__[key]
        self.set_band(image_obj.band, band=band, realize_changes=False)
        '''
        for b in range(image_obj.nbands):
            band_obj = image_obj._band_list[b]
            if self._band_list[b].image_loaded:
                continue
            self.set_band(band_obj.image,
                          band=band, realize_changes=False)
        '''

        # print('*** band: ', band)
        # print('*** len: ', len(self._band_list))
        image = self._band_list[band]._image
        # print('*** recently loaded')
        return image

        '''
        if band is None and len(self._band_list) == 0:
            return

        # band = 0 if band is None else band
        if (band > len(self._band_list)-1):
            print('WARNING requested band ('+str(band)+') not found')
            return
        image = self._band_list[band].image
        # if image is None:
        #    print('ERROR requested image is empty')
        #    return
        return image
        '''

    def read_image(self, *args, **kwargs):
        '''
        new_kwargs = {}
        print('********************************')
        print(kwargs)
        for key, value in kwargs.items():
            new_key = key[1:] if key.startswith('_') else key
            new_kwargs[new_key] = value
        '''
        method_to_execute = plant.read_image
        kwargs['method_to_execute'] = method_to_execute
        # print('*** kwargs before: ', kwargs)
        kwargs = plant.populate_kwargs(self, *args, **kwargs)
        # print('*** kwargs after: ', kwargs)
        ret = method_to_execute(*args, **kwargs)
        return ret

    @image.setter
    def image(self, val, band=None, name=None, realize_changes=None):

        # realize changes deprecated?
        if band is None:
            band = self._selected_band
        # if band < len(self._band_list):
        #    if self._band_list[band]._locked:
        #        print('WARNING image is locked')
        #        return
        #        # raise
        #    self._band_list[band]._locked = True
        if (band <= len(self._band_list)-1 and
                self._band_list[band].image_loaded):
            # print('*** image setter notify_changes')
            self._band_list[band].notify_changes()
        '''
        if realize_changes:
            self.realize_changes()
            # print('*** update image')
        '''
        # band = 0 if band is None else band
        if val is not None:
            if (isinstance(val, plant.PlantImage) or
                    isinstance(val, plant.PlantBand)):
                val = val.image
            elif not isinstance(val, np.ndarray):
                val = np.asarray(val)
            self.update_parameters_with_image(val)
            # self._depth, self._length, self._width = \
            #    plant.get_array_dimensions(val)
            '''
            if len(val.shape) >= 3:
                self._depth = val.shape[2]
            else:
                self._depth = 1
            if len(val.shape) > 1:
                self._width = val.shape[1]
                self._length = val.shape[0]
            else:
                self._length = 1
                self._width = val.shape[0]
                # val = val.reshape(self._width, 1)
            '''
            val = plant.shape_image(val)
            # self._dtype = val.dtype.name

        if (band <= len(self._band_list)-1):
            self._band_list[band].image = val
        else:
            for i in range(len(self._band_list), band+1):
                self._band_list.append(PlantBand(val))
        if self._band_list[band].parent_orig is None:
            self._band_list[band].parent_orig = self
        if (self._band_list[band].parent_orig_band_orig_index is None and
                self._band_orig is not None):
            # self._band_list[band].parent_orig_band_orig_index = band
            # elif self._band_list[band].parent_orig_band_orig_index is None:
            self._band_list[band].parent_orig_band_orig_index = \
                self._band_orig
        if self._band_list[band].parent_list is None:
            self._band_list[band].parent_list = [self]
        if self._band_list[band].parent_orig_band_index is None:
            self._band_list[band].parent_orig_band_index = band
        nbands_observed = max([0] + [b for b, current_band in
                                     enumerate(self._band_list)
                                     if current_band.image_loaded])+1
        self._nbands = max([self._nbands, nbands_observed, band+1])
        '''
        if realize_changes:
            self.get_image(band=band)
        '''
        if name is not None:
            self._band_list[band].name = name
        # self._band_list[band]._locked = False

    @image.deleter
    def image(self, band=None):
        if band is None:
            band = self._selected_band
        # band = 0 if band is None else band
        if (band <= self._nbands-1):
            self._band_list.pop(band)
        self._selected_band = 0

    @property
    def image_list(self):
        return [band.image for band in self._band_list]

    @property
    def image_loaded(self):
        # return any([band.image is not None for band in self._band_list])
        if self._selected_band > len(self._band_list)-1:
            return False
        flag_image_loaded = self._band_list[self._selected_band].image_loaded
        return flag_image_loaded

    def n_elements(self, flag_all=None, key=None):
        n_elements = 1
        if (isinstance(key, slice) and key.start is None and
                key.stop is None and key.step is None):
            key = None
        elif isinstance(key, slice):
            key = [key]

        if (self.length is not None and
                key is not None and len(key) >= 1 and
                key[0] is not None):
            expanded = plant.expand_slice(
                key[0], default_stop=self.length)
            n_elements *= len(expanded)
        elif self.length is not None:
            n_elements *= self.length

        if (self.width is not None and
                key is not None and len(key) >= 2 and
                key[1] is not None):
            n_elements *= len(plant.expand_slice(
                key[1], default_stop=self.width))
        elif self.width is not None:
            n_elements *= self.width

        if (self.depth is not None and
                key is not None and len(key) >= 3 and
                key[2] is not None):
            n_elements *= len(plant.expand_slice(
                key[2], default_stop=self.depth))
        elif self.depth is not None:
            n_elements *= self.depth

        if flag_all:
            n_elements *= self.nbands
        return n_elements

    @property
    def width(self):
        if self.image_loaded:
            return self.image.shape[-1]
        if self._width is not None:
            return self._width
        return self._width_orig

    @width.setter
    def width(self, val):
        # print('*** update width')
        for b in range(self.nbands):
            band = self.get_band(band=b)
            parent_orig = band.parent_orig
            if (parent_orig is not None and
                    parent_orig.width is not None and
                    val == parent_orig.width):
                continue
            self.realize_changes(band=b)
            if self.nbands == 1:
                print(f'WARNING changing image'
                      f' width from {parent_orig.width}'
                      f' to {val}')
            else:
                print(f'WARNING changing band {b}'
                      f' width from {parent_orig.width}'
                      f' to {val}')
            new_shape = list(band.image.shape)
            new_shape[-1] = val
            image = plant.cut_image_with_shape(
                band.image, new_shape)
            band.image = image

        # self.realize_changes()
        self._width = val
        if self._width_orig is None:
            self._width_orig = self._width

    '''
    @property
    def length(self):
        return self._height
        if self._length_orig is None:
            self._length_orig = self._length
    '''

    @property
    def length(self):
        if self.image_loaded:
            return self.image.shape[-2]
        if self._length is not None:
            return self._length
        return self._length_orig

    @length.setter
    def length(self, val):
        for b in range(self.nbands):
            band = self.get_band(band=b)
            parent_orig = band.parent_orig
            if (parent_orig.length is not None and
                    val == parent_orig.length):
                continue
            self.realize_changes(band=b)
            if self.nbands == 1:
                print(f'WARNING changing image'
                      f' length from {parent_orig.length}'
                      f' to {val}')
            else:
                print(f'WARNING changing band {b}'
                      f' length from {parent_orig.length}'
                      f' to {val}')
            new_shape = list(band.image.shape)
            new_shape[-2] = val
            image = plant.cut_image_with_shape(
                band.image, new_shape)
            band.image = image
        self._length = val
        if self._length_orig is None:
            self._length_orig = self._length

    @property
    def depth(self):
        if self.image_loaded and len(self.image.shape) > 2:
            return self.image.shape[-3]
        elif self.image_loaded:
            return 1
        if self._depth is not None:
            return self._depth
        if self._depth_orig is not None:
            return self._depth_orig
        return 1

    @depth.setter
    def depth(self, val):
        for b in range(self.nbands):
            band = self.get_band(band=b)
            parent_orig = self
            if (parent_orig.depth is None or
                    (parent_orig.depth is not None and
                     val == parent_orig.depth)):
                continue
            self.realize_changes(band=b)
            if self.nbands == 1:
                print(f'WARNING changing image'
                      f' depth from {parent_orig.depth}'
                      f' to {val}')
            else:
                print(f'WARNING changing band {b}'
                      f' depth from {parent_orig.depth}'
                      f' to {val}')
            new_shape = list(parent_orig.shape)
            if len(new_shape) >= 3:
                new_shape[-3] = val
            else:
                new_shape.insert(0, val)
            image = plant.cut_image_with_shape(
                band.image, new_shape)
            band.image = image
        self._depth = val
        if self._depth_orig is None:
            self._depth_orig = self._depth

    @property
    def shape(self):
        if self._depth is None or self._depth <= 1:
            return (self._length, self._width)
        else:
            return (self._depth, self._length, self._width)

    @shape.setter
    def shape(self, val):
        if (not isinstance(val, list) and
                not isinstance(val, tuple)):
            val = [val]
        for i in range(max([len(self.shape), len(val)])):
            if i == 0:
                self.width = val[-1]
            elif i == 1 and len(val) >= 2:
                self.length = val[-2]
            elif i == 1:
                self.length = 1
            elif i == 2 and len(val) >= 3:
                self.depth = val[-3]
            elif i == 2:
                self.depth = 1

    @property
    def dataType(self, band=None):
        if band is None:
            band = self._selected_band
        if band > len(self._band_list)-1:
            return
        return self._band_list[band].dtype

    @property
    def dtype(self, band=None):
        if band is None:
            band = self._selected_band
        if band >= len(self._band_list):
            return
        current_band = self._band_list[band]
        if current_band is None:
            return
        return current_band.dtype

    @dtype.setter
    def dtype(self, val, band=None, realize_changes=True):
        if realize_changes:
            self.realize_changes()
        if band is None:
            band = self._selected_band
        self._band_list[band].dtype = val

    @property
    def null(self, band=None):
        if band is None:
            return self._null
        current_band = self._band_list[band]
        if current_band is None:
            return self._null
        return current_band.null

    @null.setter
    def null(self, val, band=None, realize_changes=True):
        if band is None:
            # band = self._selected_band
            self._null = band
            return
        self.realize_changes()
        self._band_list[band].null = val

    @property
    def bands(self):
        return self._nbands

    @property
    def numBands(self):
        return self._nbands

    @property
    def nbands(self):
        return self._nbands

    @nbands.setter
    def nbands(self, val):
        if val < len(self._band_list):
            self._band_list = self._band_list[:val]
        # elif val > len(self._band_list)
        # add empty bands
        self._nbands = val
        for b in range(min([self.nbands, val])):
            self.realize_changes(band=b)
        # if len(self._band_list) > val:
        #     self._band_list = self._band_list[:val]

    @property
    def nbands_orig(self):
        return self._nbands_orig

    @nbands_orig.setter
    def nbands_orig(self, val, realize_changes=None):
        # print('*** update nbands_orig')
        if realize_changes is not None:
            self.realize_changes()
        self._nbands_orig = val
        if len(self._band_list) > val:
            self._band_list = self._band_list[:val]
        else:
            for i in range(len(self._band_list), val):
                self._band_list.append(PlantBand(None))

    @property
    def band_orig(self):
        return self._band_orig

    @band_orig.setter
    def band_orig(self, val, realize_changes=None):
        # print('*** update band_orig')
        if realize_changes is not None:
            self.realize_changes()
        if plant.is_sequence(val):
            self._band_orig = val
        else:
            self._band_orig = [val]

    @property
    def sampling_step(self):
        # return self._sampling_step_y, self._sampling_step_x
        # return self._sampling_step_x
        raise NotImplementedError

    @sampling_step.setter
    def sampling_step(self, val):
        self._sampling_step_x = val
        self._sampling_step_y = val

    @property
    def sampling_step_x(self):
        return self._sampling_step_x

    @sampling_step_x.setter
    def sampling_step_x(self, val, realize_changes=None):
        # print('*** update sampling_step_x')
        if realize_changes is not None:
            self.realize_changes()
        self._sampling_step_x = val

    @property
    def sampling_step_y(self):
        return self._sampling_step_y

    @sampling_step_y.setter
    def sampling_step_y(self, val, realize_changes=None):
        # print('*** update sampling_step_y')
        if realize_changes is not None:
            self.realize_changes()
        self._sampling_step_y = val

    @property
    def sampling_step_z(self):
        return self._sampling_step_z

    @sampling_step_z.setter
    def sampling_step_z(self, val, realize_changes=None):
        # print('*** update sampling_step_z')
        if realize_changes is not None:
            self.realize_changes()
        self._sampling_step_z = val

    @property
    def input_key(self):
        return self._input_key

    @input_key.setter
    def input_key(self, val, realize_changes=None):
        # print('*** update input_key')
        if realize_changes is not None:
            self.realize_changes()
        self._input_key = val

    @property
    def ctable(self, band=None):
        if band is None:
            band = self._selected_band
        if band > len(self._band_list)-1:
            return
        return self._band_list[band].ctable

    @ctable.setter
    def ctable(self, val, band=None, realize_changes=None):
        # print('*** update ctable')
        if realize_changes is not None:
            self.realize_changes()
        if band is None:
            band = self._selected_band
        if band > len(self._band_list)-1:
            return
        self._band_list[band].ctable = val

    # @dtype.setter
    # def dtype(self, val):
    #    self._band_list[self._selected_band]._dtype = val
    #    # self._image = np.asarray(self._image,
    #    #                         dtype=val)
    #    for i in range(0, len(self._image_list)):
    #        self._image_list[i] = np.asarray(self._image_list[i],
    #                                         dtype=val)

    @property
    def projection(self):
        return self._projection

    @projection.setter
    def projection(self, val, realize_changes=None):
        if realize_changes is not None:
            self.realize_changes()
        self._projection = val

    @property
    def metadata(self):
        return self._metadata

    @metadata.setter
    def metadata(self, val, realize_changes=None):
        if realize_changes is not None:
            self.realize_changes()
        self._metadata = val

    '''
    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, val):
        self._description = val
    '''

    '''
    @property
    def metadata(self, band=None):
        if band is None:
            band = self._selected_band
        return self._band_list[band].metadata

    @metadata.setter
    def metadata(self, val, band=None):
        if band is None:
            band = self._selected_band
        self._band_list[band].metadata = val
    '''

    '''
    @property
    def null(self):
        return self._null

    @null.setter
    def null(self, val):
        self._null = val
    '''

    @property
    def stats(self, band=None):
        if band is None:
            band = self._selected_band
        if band > len(self._band_list)-1:
            return
        return self._band_list[band].stats

    @stats.setter
    def stats(self, val, band=None):
        if band is None:
            band = self._selected_band
        if band > len(self._band_list)-1:
            return
        self._band_list[band].stats = val

    @property
    def offset_x(self):
        return self._offset_x

    @offset_x.setter
    def offset_x(self, val):
        self._offset_x = val

    @property
    def offset_y(self):
        return self._offset_y

    @offset_y.setter
    def offset_y(self, val):
        self._offset_y = val

    @property
    def width_orig(self):
        return self._width_orig

    @width_orig.setter
    def width_orig(self, val):
        self._width_orig = val

    @property
    def length_orig(self):
        return self._length_orig

    @length_orig.setter
    def length_orig(self, val):
        self._length_orig = val

    @property
    def depth_orig(self):
        if self._depth_orig is not None:
            return self._depth_orig
        return 1

    @depth_orig.setter
    def depth_orig(self, val):
        self._depth_orig = val

    # @property
    # def format(self):
    #    return self._format
    # @format.setter
    # def format(self, val):
    #    self._format = val

    @property
    def file_format(self):
        return self._file_format

    @file_format.setter
    def file_format(self, val):
        self._file_format = val

    @property
    def geotransform(self):
        return self._geotransform

    @geotransform.setter
    def geotransform(self, val, realize_changes=None):
        if realize_changes is not False:
            self.realize_changes()
        self._geotransform = val

    @property
    def gcp_list(self):
        return self._gcp_list

    @gcp_list.setter
    def gcp_list(self, val, realize_changes=None):
        if realize_changes is not False:
            self.realize_changes()
        self._gcp_list = val

    @property
    def gcp_projection(self):
        return self._gcp_projection

    @gcp_projection.setter
    def gcp_projection(self, val, realize_changes=None):
        if realize_changes is not False:
            self.realize_changes()
        self._gcp_projection = val

    @property
    def scheme(self):
        if self._scheme:
            return self._scheme
        if not self.header_file:
            return
        if (not self.file_format or
                'ENVI' in self.file_format.upper()):
            image_obj = plant.get_info_from_envi_header(
                self.header_file)
            if image_obj is not None:
                self._scheme = image_obj.scheme

        if (not self.file_format or
                'ISCE' in self.file_format.upper()):
            try:
                image_obj = plant.get_info_from_xml(
                    self.header_file)
                if image_obj is not None:
                    self._scheme = image_obj.scheme
            except KeyError:
                pass

        return self._scheme

    @scheme.setter
    def scheme(self, val, realize_changes=None):
        if realize_changes is not False:
            self.realize_changes()
        self._scheme = val

    # class ImageCoordinate(Configurable):
    # import ImageCoordinate from Image.py
    # coord1 = ImageCoordinate()
    # coord2 = ImageCoordinate()
    # def coord2.coordStart
    # geotransform_temp[0]
    # def coord2.coordEnd
    # geotransform_temp[0]+(lon_size-1)*geotransform_temp[1]
    # def coord2.coordDelta
    # geotransform_temp[1]

    # def coord1.coordStart
    # geotransform_temp[3]
    # def coord1.coordEnd
    # geotransform_temp[3]+(lat_size-1)*geotransform_temp[5]
    # def coord1.coordDelta
    # geotransform_temp[5]

    isImageLoaded = image_loaded.fget

    getPlantTransformObj = plant_transform_obj.fget
    getFilenameOrig = filename_orig.fget
    getFilename = filename.fget
    getName = name.fget
    getHeaderFile = header_file.fget
    getImage = image.fget
    getBand = band.fget
    getWidth = width.fget
    getLength = length.fget
    # getLength = length.fget
    getDepth = depth.fget
    getShape = shape.fget
    getImageType = dtype.fget
    getDType = dtype.fget
    getDataType = dtype.fget
    getFileFormat = file_format.fget
    getGeotransform = geotransform.fget
    getGcpList = gcp_list.fget
    getGcpProjection = gcp_projection.fget
    getProjection = projection.fget
    getScheme = scheme.fget
    getNumBands = nbands.fget
    getBands = nbands.fget
    getNBands = nbands.fget
    getNBandsOrig = nbands_orig.fget
    getBandOrig = band_orig.fget
    # getSamplingStep = sampling_step.fget
    getSamplingStepX = sampling_step_x.fget
    getSamplingStepY = sampling_step_y.fget
    getSamplingStepZ = sampling_step_z.fget
    getInputKey = input_key.fget
    getCTable = ctable.fget
    getMetadata = metadata.fget
    # getDescription = description.fget
    getNull = null.fget
    getStats = stats.fget
    getOffsetX = offset_x.fget
    getOffsetY = offset_y.fget
    getWidthOrig = width_orig.fget
    getLengthOrig = length_orig.fget
    getDepthOrig = depth_orig.fget

    is_image_loaded = image_loaded.fget
    get_plant_transform_obj = plant_transform_obj.fget
    get_filename_orig = filename_orig.fget
    get_filename = filename.fget
    get_name = name.fget
    get_header_file = header_file.fget
    get_array = array.fget
    get_new_image = new_image.fget
    get_image = image.fget
    get_band = band.fget
    get_width = width.fget
    get_length = length.fget
    # get_length = length.fget
    get_depth = depth.fget
    get_shape = shape.fget
    get_image_type = dtype.fget
    get_dtype = dtype.fget
    get_datatype = dtype.fget
    get_file_format = file_format.fget
    get_geotransform = geotransform.fget
    get_gcp_projection = gcp_projection.fget
    get_projection = projection.fget
    get_gcp_list = gcp_list.fget
    get_scheme = scheme.fget
    get_num_bands = nbands.fget
    get_bands = nbands.fget
    get_nbands = nbands.fget
    get_nbands_orig = nbands_orig.fget
    get_band_orig = band_orig.fget
    # get_sampling_step = sampling_step.fget
    get_sampling_step_x = sampling_step_x.fget
    get_sampling_step_y = sampling_step_y.fget
    get_sampling_step_z = sampling_step_z.fget
    get_input_key = input_key.fget
    get_ctable = ctable.fget
    get_metadata = metadata.fget
    # get_description = description.fget
    get_null = null.fget
    get_stats = stats.fget
    get_offset_x = offset_x.fget
    get_offset_y = offset_y.fget
    get_width_orig = width_orig.fget
    get_length_orig = length_orig.fget
    get_depth_orig = depth_orig.fget

    setPlantTransformObj = plant_transform_obj.fset
    setFilenameOrig = filename_orig.fset
    setFilename = filename.fset
    setName = name.fset
    setHeaderFile = header_file.fset
    setImage = image.fset
    setBand = band.fset
    setWidth = width.fset
    setLength = length.fset
    # setLength = length.fset
    setDepth = depth.fset
    setShape = shape.fset
    setImageType = dtype.fset
    setDType = dtype.fset
    setDataType = dtype.fset
    setFileFormat = file_format.fset
    setGeotransform = geotransform.fset
    setGcpProjection = gcp_projection.fset
    setGcpList = gcp_list.fset
    setProjection = projection.fset
    setScheme = scheme.fset
    setNumBands = nbands.fset
    setBands = nbands.fset
    setNBands = nbands.fset
    setNBandsOrig = nbands_orig.fset
    setBandOrig = band_orig.fset
    setSamplingStep = sampling_step.fset
    setSamplingStepX = sampling_step_x.fset
    setSamplingStepY = sampling_step_y.fset
    setSamplingStepZ = sampling_step_z.fset
    setInputKey = input_key.fset
    setCTable = ctable.fset
    setMetadata = metadata.fset
    # setDescription = description.fset
    setNull = null.fset
    setStats = stats.fset
    setOffsetX = offset_x.fset
    setOffsetY = offset_y.fset
    setWidthOrig = width_orig.fset
    setLengthOrig = length_orig.fset
    setDepthOrig = depth_orig.fset

    set_plant_transform_obj = plant_transform_obj.fset
    set_filename_orig = filename_orig.fset
    set_filename = filename.fset
    set_name = name.fset
    set_header_file = header_file.fset
    set_image = image.fset
    set_band = band.fset
    set_width = width.fset
    set_length = length.fset
    # set_length = length.fset
    set_depth = depth.fset
    set_shape = shape.fset
    set_image_type = dtype.fset
    set_dtype = dtype.fset
    set_datatype = dtype.fset
    set_file_format = file_format.fset
    set_geotransform = geotransform.fset
    set_gcp_list = gcp_list.fset
    set_gcp_projection = gcp_projection.fset
    set_projection = projection.fset
    set_scheme = scheme.fset
    set_num_bands = nbands.fset
    set_bands = nbands.fset
    set_nbands = nbands.fset
    set_nbands_orig = nbands_orig.fset
    set_band_orig = band_orig.fset
    set_sampling_step = sampling_step.fset
    set_sampling_step_x = sampling_step_x.fset
    set_sampling_step_y = sampling_step_y.fset
    set_sampling_step_z = sampling_step_z.fset
    set_input_key = input_key.fset
    set_ctable = ctable.fset
    set_metadata = metadata.fset
    # set_description = description.fset
    set_null = null.fset
    set_stats = stats.fset
    set_offset_x = offset_x.fset
    set_offset_y = offset_y.fset
    set_width_orig = width_orig.fset
    set_length_orig = length_orig.fset
    set_depth_orig = depth_orig.fset

    def __getattr__(self, name):
        '''
        subprocess execution wrapper (API)
        '''
        if name in self.__dir__():
            return
        if name.startswith('__'):
            raise AttributeError
        wrapper_obj = plant.ModuleWrapper(name, self)
        # print('*** name: ', name)
        # print('*** wrapper_obj: ', wrapper_obj)
        if not wrapper_obj._module_obj:
            raise AttributeError(
                f'{self.__class__.__name__}.{name} is invalid.')
        # f' (obj id: {id(self)}).')
        # raise AttributeError
        return wrapper_obj

    # def __setattr__(self, name, val):
    #     print('*** here', name, val)


class PlantBand(PlantBandBase):

    def __new__(cls, *args, **kwargs):
        input_data = args[0] if len(args) > 0 else None
        input_data_kw = kwargs.pop('input_data', None)
        if input_data is None:
            input_data = input_data_kw
        if isinstance(input_data, PlantBand):
            input_data.init(**kwargs)
            return input_data
        new = super(PlantBand, cls).__new__(cls)
        # print('*** new band: ', id(new))
        new.initialize_parameters()
        new.init(input_data, **kwargs)
        return new

    def init(self, *args, **kwargs):
        input_data = args[0] if len(args) > 0 else None
        image = kwargs.pop('image', None)
        name = kwargs.pop('name', None)
        dtype = kwargs.pop('dtype', None)
        ctable = kwargs.pop('ctable', None)
        null = kwargs.pop('null', np.nan)
        stats = kwargs.pop('stats', None)
        parent_orig = kwargs.pop('parent_orig', None)
        parent_orig_band_orig_index = kwargs.pop(
            'parent_orig_band_orig_index', None)
        parent_list = kwargs.pop('parent', None)
        parent_orig_band_index = kwargs.pop('parent_orig_band_index', None)

        # verbose = kwargs.get('verbose', False)
        '''
        plant_transform_obj = kwargs.get('plant_transform_obj', None)
        ret = plant.PlantTransform(plant_transform_obj=plant_transform_obj,
                             verbose=verbose,
                             width=width,
                             length=length,
                             depth=depth,
                             offset_x=offset_x,
                             offset_y=offset_y)
        self._plant_transform_obj = ret
        '''

        if isinstance(input_data, PlantBand):
            self.__dict__.update(input_data.__dict__)
        elif isinstance(input_data, PlantImage):
            self._image = input_data.to_array()
            '''
            if (len(self._image.shape) == 3 and
                    self._image.shape[0] == 1):
                self._image = self._image.reshape(self._image.shape[0:2])
            '''
            # self.__dict__.update(input_data.__dict__)
        elif input_data is not None:
            self._image = input_data

        if image is not None:
            self._image = image
        self._image = plant.shape_image(self._image)

        if name:
            self._name = name

        if ctable is not None:
            self._ctable = ctable.Clone()

        if dtype is not None:
            self._dtype = dtype
        elif self._image is not None:
            self._dtype = plant.get_dtype_name(self._image)

        if stats is not None:
            self._stats = stats  # min, max, offset, scale
        if plant.isvalid(null, null=np.nan):
            self._null = null
        elif (not plant.isvalid(self._null, null=np.nan) and
              (self.dtype is not None and
               'int' in plant.get_dtype_name(self.dtype))):
            self._null = plant.INT_NAN

        # because of a bug in ISCE:
        '''
        if ('int' in plant.get_dtype_name(self._dtype) and
                self._image is not None):
            self._image = np.asarray(self._image,
                                     dtype=np.int32)
            self._dtype = np.int32
        '''

        # if self._image is not None:
        #     print(self._image.shape)
        # if self._image is not None:
        #     print(self._image.shape)
        # if self._image is not None and len(self._image.shape) == 1:
        #    self._image = np.reshape(self._image.shape[0], 1)
        if parent_orig is not None:
            self._parent_orig = parent_orig
        if parent_orig_band_orig_index is not None:
            self._parent_orig_band_orig_index = parent_orig_band_orig_index
        if parent_list is not None and len(parent_list) > 1:
            self._parent_list = parent_list
        if parent_orig_band_index is not None:
            self._parent_orig_band_index = parent_orig_band_index

    '''
    @property
    def plant_transform_obj(self):
        return self._plant_transform_obj

    @plant_transform_obj.setter
    def plant_transform_obj(self, val):
        self._plant_transform_obj = val
    '''

    def __repr__(self):
        '''
        id_str = plant.get_obj_id(self)
        ret = ('PLAnT band:\n')
        ret += id_str+'\n'
        if self._name:
            ret += ('    name: %s\n'
                    % str(self.name))
        if self._image is not None:
            ret += ('    image shape: %s\n'
                    % str(self.image.shape))
        if self._dtype is not None:
            ret += ('    data type: %s\n'
                    % str(plant.get_dtype_name(self._dtype)))
        ret += ('    null: %s\n'
                % str(self.null))
        if self._stats is not None:
            ret += ('    stats: %s\n'
                    % (self.stats))
        if self._ctable is not None:
            ret += ('    has color table: %s'
                    % str(self.ctable is not None))
        else:
            ret += '    no ctable'
        '''
        ret = f"PlantBand('{self.name}')"
        return ret

    def __str__(self):
        if self.name:
            return self.name
        max_n_elements = 10
        if self.image_loaded:
            if (len(self.image.shape) <= 2 and
                    self.image.shape[0] == 1 and
                    self.image.shape[1] == 1):
                return f'{str(self.image[0, 0])}'
            if (len(self.image.shape) <= 2 and
                    self.image.shape[0] == 1 and
                    self.image.shape[1] < max_n_elements):
                return f'{str(self.image[0, :])}'
            n_elements = 1
            for dim in self.image.shape:
                n_elements = n_elements*dim
            if n_elements < max_n_elements:
                return f'{str(self.image)}'
        id_str = plant.get_obj_id(self)
        return f'MEM:{id_str}'

    @property
    def new_image(self):
        # def image(self):
        if self.image_loaded:
            return self._image
        new_image = ExtendedNdarray(self.parent_orig,
                                    self.parent_orig_band_index,
                                    # buffer=image.data,
                                    shape=self.shape,
                                    dtype=self.dtype)
        return new_image

    @property
    def image(self):
        # def new_image(self):
        if (not self.image_loaded and
                self.parent_orig is not None and
                self.parent_orig_band_index is not None):
            # print('*** id(self): ', id(self))
            # print('*** parent_orig(self): ', id(self.parent_orig))
            self._image = self.parent_orig.get_image(
                band=self.parent_orig_band_index)
        return self._image

    @property
    def array(self):
        return self.get_image()
        # return self.new_image()

    @image.setter
    def image(self, val):
        # print('*** image.setter called (2)')
        if self.image_loaded:
            self.notify_changes()
        if (isinstance(val, plant.PlantImage) or
                isinstance(val, plant.PlantBand)):
            val = val.image
        elif not isinstance(val, np.ndarray):
            val = np.asarray(val)
        self._image = plant.shape_image(val)
        self._dtype = plant.get_dtype_name(val)

    getImage = image.fget
    get_image = image.fget
    get_new_image = new_image.fget
    get_array = array.fget
    setImage = image.fset
    set_image = image.fset


class PlantBandOperation(PlantBandBase):

    '''
    def initialize_parameters(self):
        self.operation = None
        self.args = None
        self.kwargs = None
    '''

    def init(self, *args, **kwargs):
        # print('*** 2 args: ', args)
        # print('*** 2 kwargs: ', kwargs)
        self.operation = kwargs.pop('operation', None)
        self.args = args
        self.kwargs = kwargs
        # self._band_list = []
        # self._selected_band = 0
        PlantBand.initialize_parameters(self)

    def __new__(cls, *args, **kwargs):
        # print('*** 1 args: ', args)
        # print('*** 1 kwargs: ', kwargs)
        # cls.init(*args, **kwargs)
        # print('*** cls: ', cls)
        new = super(PlantBandOperation, cls).__new__(cls)
        # print('*** operation obj created (1): ', id(new))
        # new.initialize_parameters()
        new.init(*args, **kwargs)
        # print('*** new.operation: ', new.operation)
        # new = super(PlantBandOperation, cls)
        # .__new__(cls)
        # attributes_to_override = ['image']
        '''
        attributes_to_override = []
        import inspect
        attr_list = inspect.getmembers(PlantBand)
        for attribute, reference in attr_list:
            # if hasattribute(new ,method) and
            if (attribute.startswith('get') or
                    attribute.startswith('set') or
                    attribute.startswith('_') or
                    attribute in attributes_to_override):
                continue
            # if not callable(getattribute(new, attribute)):
            #     continue
            try:
                # plant_image_attr = getattr(PlantBand, attribute)
                setattr(new, attribute, reference)
                print('[OK] *** attribute: ', attribute)
            except AttributeError:
                print('[FAIL] *** attribute: ', attribute)
                pass
        new._band_list = []
        '''
        # for i in range(new.nbands):
        #     new._band_list.append(PlantBand(None))
        # new.operation = kwargs.pop('operation')
        # new.args = args
        # new.kwargs = kwargs
        # print('*** image_obj created: ', id(new))
        # new.initialize_parameters()
        # new.init(input_data, **kwargs)
        return new

    '''
    @classmethod
    def __instancecheck__(cls, instance):
        print('*** here', cls, instance)
        ret = isinstance(instance, plant.PlantBandOperation)
        ret |= isinstance(instance, plant.PlantBand)
        return ret
    '''

    @property
    def __class__(self):
        return plant.PlantBand

    @property
    def array(self):
        return self.get_image()

    @property
    def image(self):
        '''
        def image(self, band=None):
        if band is None:
            band = self._selected_band
        if (band < len(self._band_list) and
                self._band_list[band].image_loaded):
            print('*** image previously loaded')
            image = self._band_list[band].image
            return image
        '''
        # print(f'*** get_image from operation (id: {id(self)})'
        # ' image loaded: ', self.image_loaded)
        if self.image_loaded:
            # print(f'*** image already loaded (id: {id(self)})')
            return self._image

        print('*** (operation) kwargs:', self.kwargs)
        if self.kwargs and 'ufunc' in self.kwargs.keys():
            ufunc = self.kwargs.pop('ufunc')
            ufunc_kwargs = self.kwargs.pop('ufunc_kwargs')
            new_args = []
            for band_obj in self.args:
                new_args.append(band_obj.image)
                band_obj._notify_changes_list.remove(self)
            result = ufunc(*new_args, **ufunc_kwargs)
            self._image = result
            return result

        band_1_obj = self.args[0]
        if self.operation == '__getitem__':
            result = band_1_obj.image.__getitem__(self.kwargs['key'])
            self._image = result
            return result

        if len(self.args) > 1:
            band_2_obj = self.args[1]
        else:
            band_2_obj = None
        # band_1_obj = self
        # new_obj = band_1_obj.copy()
        if isinstance(band_2_obj, PlantBand):
            geotransform_1 = band_1_obj.geotransform
            geotransform_2 = band_2_obj.geotransform
            if (geotransform_1 is not None and
                    geotransform_2 is not None and
                    not plant.compare_geotransforms(geotransform_1,
                                                    geotransform_2)):
                print('WARNING inputs have different geometries', 1)
        elif band_2_obj is not None:
            band_2_obj = PlantBand(band_2_obj)
        # if band is None:
        #     band_list = range(max([band_1_obj.nbands, band_2_obj.nbands]))
        # else:
        #     band_list = [band]
        # for b in band_list:
        if self in band_1_obj._notify_changes_list:
            band_1_obj._notify_changes_list.remove(self)

        # self.parent_orig.get_image
        image_1 = band_1_obj.image  # get_image(band=band)
        if band_2_obj is not None:
            if self in band_2_obj._notify_changes_list:
                band_2_obj._notify_changes_list.remove(self)
            image_2 = band_2_obj.image  # get_image(band=band)
            if image_1.shape != (1, 1) and image_2.shape != (1, 1):
                image_1, image_2 = plant.get_intersection(image_1,
                                                          image_2)

        flag_constant = False
        if image_1.shape == (1, 1):
            flag_constant = True
            image_1 = float(image_1[0, 0])
        print(f'executing operation: {self.operation}')
        print(f'    argument 1: {band_1_obj} (id: {id(band_1_obj)})')
        if band_2_obj is not None:
            print(f'    argument 1: {band_2_obj} (id: {id(band_2_obj)})')
        if band_2_obj is not None and image_2.shape == (1, 1):
            flag_constant = True
            image_2 = float(image_2[0, 0])
        if self.operation == '__sin__':
            result = np.sin(image_1)
        elif self.operation == '__cos__':
            result = np.cos(image_1)
        elif ((self.operation == '__add__' or
              self.operation == '__radd__') and
              flag_constant):
            result = image_1 + image_2
        elif (self.operation == '__add__' or
              self.operation == '__radd__'):
            result = np.nansum([image_1, image_2], axis=0)
        elif self.operation == '__sub__' and flag_constant:
            result = image_1 - image_2
        elif self.operation == '__sub__':
            result = np.nansum([image_1, -image_2], axis=0)
        elif self.operation == '__rsub__' and flag_constant:
            result = image_2 - image_1
        elif self.operation == '__rsub__':
            result = np.nansum([image_2, - image_1], axis=0)
        elif ((self.operation == '__mul__' or
               self.operation == '__rmul__')
              and flag_constant):
            result = image_1 * image_2
        elif (self.operation == '__mul__' or
              self.operation == '__rmul__'):
            result = np.nanprod([image_1, image_2], axis=0)
        elif self.operation == '__div__' and flag_constant:
            result = image_1 / image_2
        elif self.operation == '__div__' or self.operation == 'true_divide':
            result = np.nanprod([image_1, 1.0/image_2], axis=0)
        elif self.operation == '__rdiv__' and flag_constant:
            result = image_2 / image_1
        elif self.operation == '__rdiv__':
            result = np.nanprod([image_2, 1.0/image_1], axis=0)
        elif self.operation == '__pow__':
            result = np.power(image_1, image_2)
        elif self.operation == '__rpow__':
            result = np.power(image_2, image_1)
        else:
            print(f'ERROR operation not implemented: {self.operation}')
            return
        # self.set_image(result)  # , band=band, realize_changes=False
        self._image = result
        # new_obj.filename = ''
        # new_obj.filename_orig = None
        # new_obj.nbands = max_band
        # new_obj.nbands_orig = max_band
        # self = new_obj
        # return new_obj
        return result

    @image.setter
    def image(self, val):
        # print('*** image.setter called (1)')
        if self.image_loaded:
            self.notify_changes()
        if (isinstance(val, plant.PlantImage) or
                isinstance(val, plant.PlantBand)):
            val = val.image
        elif not isinstance(val, np.ndarray):
            val = np.asarray(val)
        self._image = plant.shape_image(val)
        self._dtype = plant.get_dtype_name(val)

    # @image.setter
    # def image(self, *args, **kwargs):
    #     return PlantBand.set_image(self, *args, **kwargs)

    # def update_parameters_with_image(self, *args, **kwargs):
    #     return PlantBand.update_parameters_with_image(self, *args, **kwargs)

    '''
    def __getattr__(self, name, *args, **kwargs):
        print('*** name: ', name, *args, **kwargs)
        method_to_call = getattr(PlantBand, name)
        if callable(method_to_call):
            print('    is callable')
            print('    len(args): ', len(args))
            ret = method_to_call(self, *args, **kwargs)
            # ret = method_to_call(self)
            return ret
        print('    not callable')
        ret = method_to_call.fget(self)
        return ret
    '''

    '''
    @scheme.setter
    def __getattr__(self, name, *args, **kwargs):
        print('*** name: ', name, *args, **kwargs)
        method_to_call = getattr(PlantBand, name)
        if callable(method_to_call):
            print('    is callable')
            # ret = method_to_call(self, *args, **kwargs)
            ret = method_to_call(self)
        else:
            print('    not callable')
            ret = method_to_call.fget(self)
        return ret
    '''

    def __repr__(self):
        '''
        id_str = plant.get_obj_id(self)
        old_stdout = sys.stdout
        mystdout = StringIO()
        print('PLAnT band operation:')
        # print(f'{self}')
        print('    '+id_str)
        # print('')
        print(f'    operation: {self.operation}')
        for i, arg in enumerate(self.args):
            print(f'    arg {i+1}: {arg.__class__}')
        sys.stdout = old_stdout
        ret = mystdout.getvalue()
        '''
        args_str = ', '.join([arg.__repr__() for arg in self.args])
        if args_str:
            args_str = ', '+args_str
        ret = f"PlantBandOperation('{self.operation}'{args_str})"
        return ret

    def __str__(self):
        ret = f'PLAnT band operation: {self.operation}'
        return ret

    '''
    @property
    def _nbands(self):
        max_nbands = None
        for arg in self.args:
            if (not isinstance(arg, plant.PlantBand) or
                (max_nbands is not None and
                 max_nbands >= arg.nbands)):
                continue
            max_nbands = arg.nbands
        return max_nbands

    @_nbands.setter
    def _nbands(self, val):
        pass

    @property
    def nbands(self):
        return self._nbands

    @nbands.setter
    def nbands(self, val):
        pass
    '''

    set_image = image.fset
    get_image = image.fget
    # get_array = array.fget
    '''
    def __repr__(self):
    def __str__(self):
    def copy(self):
    def to_array(self):
    def realize_changes(self, band=None):
    def plant_transform_obj(self):
    def plant_transform_obj(self, val):
    def crop_window(self):
    def filename_orig(self):
    def filename_orig(self, val):
    def filename(self):
    def filename(self, val):
    def header_file(self):
    def header_file(self, val):
    def band(self, band=None):
    def band(self, val, band=None, realize_changes=None):
    def update_parameters_with_image(self, val):
    def update_parameters_with_image_obj(self, image_obj):
    def image(self, band=None):
    def read_image(self, *args, **kwargs):
    def image(self, val, band=None, realize_changes=True,
    def image(self, band=None):
    def image_list(self):
    def image_loaded(self):
    def width(self):
    def width(self, val):
    def length(self):
    def length(self, val):
    def depth(self):
    def depth(self, val):
    def shape(self):
    def shape(self, val):
    def dataType(self, band=None):
    def dtype(self, band=None):
    def dtype(self, val, band=None, realize_changes=True):
    def null(self, band=None):
    def null(self, val, band=None, realize_changes=True):
    def bands(self):
    def numBands(self):
    def nbands(self):
    def nbands(self, val):
    def nbands_orig(self):
    def nbands_orig(self, val, realize_changes=None):
    def band_orig(self):
    def band_orig(self, val):
    def sampling_step(self):
    def sampling_step(self, val):
    def sampling_step_x(self):
    def sampling_step_x(self, val):
    def sampling_step_y(self):
    def sampling_step_y(self, val):
    def sampling_step_z(self):
    def sampling_step_z(self, val):
    def input_key(self):
    def input_key(self, val):
    def ctable(self, band=None):
    def ctable(self, val, band=None):
    def projection(self):
    def projection(self, val):
    def metadata(self):
    def metadata(self, val):
    def stats(self, band=None):
    def stats(self, val, band=None):
    def offset_x(self):
    def offset_x(self, val):
    def offset_y(self):
    def offset_y(self, val):
    def width_orig(self):
    def width_orig(self, val):
    def length_orig(self):
    def length_orig(self, val):
    def depth_orig(self):
    def depth_orig(self, val):
    def file_format(self):
    def file_format(self, val):
    def geotransform(self):
    def geotransform(self, val):
    def gcp_list(self):
    def gcp_list(self, val):
    def gcp_projection(self):
    def gcp_projection(self, val):
    def scheme(self):
    def scheme(self, val):
    '''
