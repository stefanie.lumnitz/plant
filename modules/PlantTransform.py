#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import sys
import numpy as np
import numbers
import copy
import plant
from .polygon import Polygon

FLAG_CUT_TO_IMAGE_SIZE = True
MASK_DTYPE = np.byte

_PLANT_TRANSFORM_OBJ_COPY_ATTRS_TO_SKIP = [
    '_offset_x',
    '_offset_y',
    '_width',
    '_length',
    '_width_orig',
    '_length_orig',
    'crop_window']


class PlantTransform():

    def __new__(cls, *args, **kwargs):
        input_data = args[0] if len(args) > 0 else None
        input_data_kw = kwargs.pop('input_data', None)
        if input_data is None:
            input_data = input_data_kw
        if isinstance(input_data, PlantTransform):
            input_data.init(**kwargs)
            return input_data
        new = super(PlantTransform, cls).__new__(cls)
        new.initialize_parameters()
        new.init(input_data, **kwargs)
        return new

    def initialize_parameters(self):
        self.srcwin = None
        self._offset_x = None
        self._offset_y = None
        self._width = None
        self._length = None
        self._length_orig = None
        self._width_orig = None

        self.geotransform = None
        self.projection = None

        self.topo_dir = None

        self.mask_ref = None
        self.mask_equal = None
        self.mask_different = None
        self.mask_greater = None
        self.mask_less = None
        self.mask_greater_equal = None
        self.mask_less_equal = None
        self.mask_erode = None
        self.mask_border = None
        self._select_geo_x = None
        self._select_geo_y = None
        self._select_col = None
        self._select_row = None
        self.polygon = None
        self.geo_polygon = None
        self.out_mask = None
        self.mask_as_input = None
        self.function_dict = {}

        self.in_mask = None
        self.memory_mask = None
        self.null = None
        self.verbose = None

    def init(self, *args, **kwargs):
        # self.flag_transform_applied = False
        plant_transform_obj = kwargs.pop('plant_transform_obj', None)

        crop_window = kwargs.pop('crop_window', None)

        srcwin = kwargs.pop('srcwin', None)

        # relative to the image
        _offset_x = kwargs.pop('offset_x', None)
        _offset_y = kwargs.pop('offset_y', None)
        _width = kwargs.pop('width', None)
        _length = kwargs.pop('length', None)
        _length_orig = kwargs.pop('_length_orig', None)
        _width_orig = kwargs.pop('_width_orig', None)

        geotransform = kwargs.pop('geotransform', None)
        projection = kwargs.pop('projection', None)

        topo_dir = kwargs.pop('topo_dir', None)

        mask_ref = kwargs.pop('mask_ref', None)
        mask_equal = kwargs.pop('mask_equal', None)
        mask_different = kwargs.pop('mask_different', None)
        mask_greater = kwargs.pop('mask_greater', None)
        mask_less = kwargs.pop('mask_less', None)
        mask_greater_equal = kwargs.pop('mask_greater_equal', None)
        mask_less_equal = kwargs.pop('mask_less_equal', None)
        mask_erode = kwargs.pop('mask_erode', None)
        mask_border = kwargs.pop('mask_border', None)
        select_geo_x = kwargs.pop('select_geo_x', None)
        select_geo_y = kwargs.pop('select_geo_y', None)
        select_col = kwargs.pop('select_col', None)
        select_row = kwargs.pop('select_row', None)
        polygon = kwargs.pop('polygon', None)
        geo_polygon = kwargs.pop('geo_polygon', None)
        out_mask = kwargs.pop('out_mask', None)
        mask_as_input = kwargs.pop('mask_as_input', False)

        function_dict = kwargs.pop('function_dict', None)

        in_mask = kwargs.pop('in_mask', None)
        memory_mask = kwargs.pop('memory_mask', None)
        null = kwargs.pop('null', None)
        verbose = kwargs.pop('verbose', None)

        if srcwin is not None:
            self.srcwin = srcwin
        if _offset_x is not None:
            self._offset_x = _offset_x
        if _offset_y is not None:
            self._offset_y = _offset_y
        if _width is not None:
            self._width = _width
        if _length is not None:
            self._length = _length
        if _length_orig is not None:
            self._length_orig = _length_orig
        if _width_orig is not None:
            self._width_orig = _width_orig
        if geotransform is not None:
            self.geotransform = geotransform
        if projection is not None:
            self.projection = projection
        if topo_dir is not None:
            self.topo_dir = topo_dir

        if mask_ref is not None:
            self.mask_ref = mask_ref
        if mask_equal is not None:
            self.mask_equal = mask_equal
        if mask_different is not None:
            self.mask_different = mask_different
        if mask_greater is not None:
            self.mask_greater = mask_greater
        if mask_less is not None:
            self.mask_less = mask_less
        if mask_greater_equal is not None:
            self.mask_greater_equal = mask_greater_equal
        if mask_less_equal is not None:
            self.mask_less_equal = mask_less_equal
        if mask_erode is not None:
            self.mask_erode = mask_erode
        if mask_border is not None:
            self.mask_border = mask_border
        if select_geo_x is not None:
            self._select_geo_x = select_geo_x
        if select_geo_y is not None:
            self._select_geo_y = select_geo_y
        if select_col is not None:
            self._select_col = select_col
        if select_row is not None:
            self._select_row = select_row
        if polygon is not None:
            self.polygon = polygon
        if geo_polygon is not None:
            self.geo_polygon = geo_polygon
        if out_mask is not None:
            self.out_mask = out_mask
        if mask_as_input is not None:
            self.mask_as_input = mask_as_input
        if in_mask is not None:
            self.in_mask = in_mask
        if memory_mask is not None:
            self.memory_mask = memory_mask
        if null is not None:
            self.null = null
        if verbose is not None:
            self.verbose = verbose

        flag_update_crop_window = kwargs.pop('flag_update_crop_window',
                                             None)
        # self.is_transformation_applied = \
        #     kwargs.pop('is_transformation_applied', False)

        if function_dict is not None:
            self.function_dict = function_dict
        for key, value in kwargs.items():
            if (key.startswith('transform_') and
                    value is not None):
                self.function_dict[key] = value

        if plant_transform_obj is not None:
            for element in plant_transform_obj.__dict__:
                # print('*** ', element, self.__dict__[element],
                #       plant_transform_obj.__dict__[element])
                # if (element.startswith('_') or
                #         element == 'crop_window' or
                #         self.__dict__[element]):
                #     continue
                if (element in _PLANT_TRANSFORM_OBJ_COPY_ATTRS_TO_SKIP or
                        self.__dict__[element]):
                    continue
                self.__dict__[element] = \
                    plant_transform_obj.__dict__[element]
                # self.__dict__.update(

        if flag_update_crop_window is None:
            flag_update_crop_window = (self.flag_apply_crop() or
                                       (crop_window is None and
                                        self._offset_x is None and
                                        self._offset_y is None and
                                        self._width is None and
                                        self._length is None))
        if flag_update_crop_window:
            self.update_crop_window(crop_window)

    def update_crop_window(self,
                           crop_window=None,
                           geotransform=None,
                           projection=None,
                           length_orig=None,
                           width_orig=None):
        if (geotransform is not None and
                geotransform != self.geotransform):
            self.geotransform = geotransform
        if (projection is not None and
                projection != self.projection):
            self.projection = projection
        if length_orig is not None:
            self._length_orig = length_orig
        if width_orig is not None:
            self._width_orig = width_orig
        # else:
        #     return
        crop_window = get_crop_window(
            select_row=self.select_row,
            select_col=self.select_col,
            select_geo_y=self.select_geo_y,
            select_geo_x=self.select_geo_x,
            polygon=self.polygon,
            geo_polygon=self.geo_polygon,

            geotransform=self.geotransform,
            length_orig=self._length_orig,
            width_orig=self._width_orig,
            # offset_x=None,
            # offset_y=None,
            # width=None,
            # length=None,

            topo_dir=self.topo_dir,
            crop_window=crop_window,
            srcwin=self.srcwin,
            verbose=self.verbose)
        offset_x, offset_y, width, length = crop_window
        self._offset_x = offset_x
        self._offset_y = offset_y
        self._width = width
        self._length = length

    def __repr__(self):
        ret = f'PlantTransform({id(self)})'
        return ret

    def __str__(self):
        ret_string = 'PLAnT transform:\n'
        id_str = plant.get_obj_id(self)
        ret_string += id_str+'\n'
        ret_string += str(self.__dict__)
        return ret_string

    def flag_apply_function(self):
        if self.function_dict is None:
            return False
        flag_function = any([v for v in self.function_dict.values() if v])
        return flag_function

    def flag_apply_crop(self):
        flag_crop = (self.srcwin or
                     # self.polygon or
                     self.select_geo_x is not None or
                     self.select_geo_y is not None or
                     self.select_col is not None or
                     self.select_row is not None)
        return flag_crop

    def flag_apply_mask(self, image_obj=None):
        flag_mask = (self.mask_equal is not None or
                     self.mask_different is not None or
                     self.mask_greater is not None or
                     self.mask_less is not None or
                     self.mask_greater_equal is not None or
                     self.mask_less_equal is not None or
                     self.mask_erode is not None or
                     self.mask_border is not None or
                     self.out_mask is not None or
                     self.in_mask is not None or
                     self.mask_ref is not None or
                     self.memory_mask is not None or
                     bool(self.mask_as_input) or
                     bool(self.polygon) or
                     bool(self.geo_polygon) or
                     bool(self.select_geo_x) or
                     bool(self.select_geo_y))
        if flag_mask:
            return True
        if self.select_row is None and self.select_col is None:
            return False

        flag_rows = False
        # length_orig = None
        # width_orig = None
        # depth_orig = None
        if image_obj is not None:
            length_orig = image_obj.length_orig
            width_orig = image_obj.width_orig
            # depth_orig = image_obj.depth_orig
        else:
            length_orig = self._length_orig
            width_orig = self._width_orig
            # depth_orig = image_obj.depth_orig

        if self.select_row is not None:
            ret = plant.read_slice(
                self.select_row, default_stop=length_orig)
            if ret is not None:
                rows = np.asarray(ret)
                vect_size = int(np.max(rows)-np.min(rows))
                if vect_size > 1:
                    vect = np.ones((vect_size))
                    ind = np.asarray(rows-np.min(rows), dtype=np.int)
                    ind = np.clip(ind, 0, vect_size-1)
                    vect[ind] = 0
                    flag_rows = any(vect)
        flag_cols = False
        if self.select_col is not None:
            ret = plant.read_slice(
                self.select_col, default_stop=width_orig)
            if ret is not None:
                cols = np.asarray(ret)
                vect_size = int(np.max(cols)-np.min(cols))
                if vect_size > 1:
                    vect = np.ones((vect_size))
                    ind = np.asarray(cols-np.min(cols), dtype=np.int)
                    ind = np.clip(ind, 0, vect_size-1)
                    vect[ind] = 0
                    flag_cols = any(vect)
        flag_mask = bool(flag_rows) or bool(flag_cols)
        return flag_mask

    def flag_apply_transformation(self, image_obj=None):
        if (image_obj is not None and
                self.flag_transformation_applied(image_obj)):
            return False
        '''
        width_orig = None
        length_orig = None
        if image_obj is not None:
            width_orig = image_obj.width_orig
            length_orig = image_obj.length_orig
        '''
        flag_transformation = (plant.isvalid(self.null) or
                               self.flag_apply_mask(image_obj=image_obj) or
                               self.flag_apply_crop() or
                               self.mask_as_input or
                               self.flag_apply_function())
        return flag_transformation

    def flag_transformation_applied(self, image_obj):
        for b in range(image_obj.nbands):
            band_obj = image_obj.get_band(band=b)
            if (band_obj._applied_transform_obj_list is None or
                    id(self) not in band_obj._applied_transform_obj_list):
                return False
        return True

    def get_crop_dict(self):
        crop_dict = {}
        crop_dict['select_col'] = self._select_col
        crop_dict['select_row'] = self._select_row
        crop_dict['select_geo_y'] = self._select_geo_y
        crop_dict['select_geo_x'] = self._select_geo_x
        return crop_dict

    def is_crop_applied(self, input_data):
        if isinstance(input_data, plant.PlantImage):
            crop_list = input_data._applied_transform_obj_crop_list
        elif (isinstance(input_data, plant.PlantBand) and
                not input_data.image_loaded):
            crop_list = input_data._applied_transform_obj_crop_header_list
        elif isinstance(input_data, plant.PlantBand):
            crop_list = input_data._applied_transform_obj_crop_image_list
        else:
            crop_list = input_data
        if crop_list is None:
            return False
        for crop_dict in crop_list:
            if self.get_crop_dict() == crop_dict:
                return True
        return False

    def copy(self, ref_image_obj=None):
        if ref_image_obj is None:
            ref_image_obj = PlantTransform()
        for key, value in self.__dict__.items():
            ref_image_obj.__dict__[key] = \
                plant.get_immutable_value(value)
        return ref_image_obj

    @property
    def crop_window(self):
        return [self._offset_x,
                self._offset_y,
                self._width,
                self._length]

    @crop_window.setter
    def crop_window(self, val):
        offset_x, offset_y, width, length = val
        self._offset_x = offset_x
        self._offset_y = offset_y
        self._width = width
        self._length = length

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, val):
        self._width = val

    @property
    def length(self):
        return self._length

    @length.setter
    def length(self, val):
        self._length = val

    @property
    def offset_x(self):
        return self._offset_x

    @offset_x.setter
    def offset_x(self, val):
        self._offset_x = val
        # self.update_crop_window()

    @property
    def offset_y(self):
        return self._offset_y

    @offset_y.setter
    def offset_y(self, val):
        self._offset_y = val
        # self.update_crop_window()

    @property
    def select_col(self):
        return self._select_col

    @select_col.setter
    def select_col(self, val):
        self._select_col = val
        self._offset_x = None
        self._width = None
        # self.update_crop_window()

    @property
    def select_row(self):
        return self._select_row

    @select_row.setter
    def select_row(self, val):
        self._select_row = val
        self._offset_y = None
        self._length = None
        # self.update_crop_window()

    @property
    def select_geo_x(self):
        return self._select_geo_x

    @select_geo_x.setter
    def select_geo_x(self, val):
        self._select_geo_x = val
        self._offset_x = None
        self._width = None

    @property
    def select_geo_y(self):
        return self._select_geo_y

    @select_geo_y.setter
    def select_geo_y(self, val):
        self._select_geo_y = val
        self._offset_y = None
        self._length = None


def apply_mask(image_obj=None,
               output_format=None,
               vmax=None,
               vmin=None,
               out_null=np.nan,
               verbose=False,
               dtype=None,
               force=None,
               input_format=None,
               output_dtype=None,
               plant_transform_obj=None,

               length=None,
               width=None,

               mask_ref=None,
               crop_window=None,
               offset_x=None,
               offset_y=None,
               mask_equal=None,
               mask_different=None,
               mask_greater=None,
               mask_less=None,
               mask_greater_equal=None,
               mask_less_equal=None,
               mask_erode=None,
               mask_border=None,
               out_mask=None,
               mask_as_input=False,
               in_mask=None,
               memory_mask=None,
               polygon=None,
               geo_polygon=None,
               srcwin=None,
               select_col=None,
               select_row=None,
               select_geo_x=None,
               select_geo_y=None,

               function_dict=None,

               null=np.nan,
               overwrite_obj=True):

    if not overwrite_obj:
        image_obj = image_obj.copy()
    if plant_transform_obj is None:
        plant_transform_obj = \
            PlantTransform(plant_transform_obj=plant_transform_obj,
                           crop_window=crop_window,
                           offset_x=offset_x,
                           offset_y=offset_y,
                           width=width,
                           length=length,
                           # geotransform=geotransform,
                           mask_ref=mask_ref,
                           mask_equal=mask_equal,
                           mask_different=mask_different,
                           mask_greater=mask_greater,
                           mask_less=mask_less,
                           mask_greater_equal=mask_greater_equal,
                           mask_less_equal=mask_less_equal,
                           mask_erode=mask_erode,
                           mask_border=mask_border,
                           select_col=select_col,
                           select_row=select_row,
                           select_geo_x=select_geo_x,
                           select_geo_y=select_geo_y,
                           out_mask=out_mask,
                           mask_as_input=mask_as_input,
                           in_mask=in_mask,
                           memory_mask=memory_mask,
                           srcwin=srcwin,
                           polygon=polygon,
                           geo_polygon=geo_polygon,
                           function_dict=function_dict,
                           null=null)
    if plant_transform_obj.offset_x is None and image_obj is not None:
        plant_transform_obj.offset_x = image_obj.offset_x
    if plant_transform_obj.offset_y is None and image_obj is not None:
        plant_transform_obj.offset_y = image_obj.offset_y
    if not plant_transform_obj.flag_apply_transformation(image_obj=image_obj):
        return image_obj

    for b in range(image_obj.nbands):
        _apply_mask_single_band(image_obj,
                                band=b,
                                plant_transform_obj=plant_transform_obj,
                                output_format=None,
                                vmax=None,
                                vmin=None,
                                out_null=np.nan,
                                verbose=False,
                                dtype=None,
                                force=None,
                                input_format=None,
                                output_dtype=None)

    if not image_obj.image_loaded:
        return
    '''
    if image_obj._applied_transform_obj_list is None:
        image_obj._applied_transform_obj_list = [id(plant_transform_obj)]
    elif image_obj._applied_transform_obj_list is not None:
        image_obj._applied_transform_obj_list.append(
            id(plant_transform_obj))
    '''
    return image_obj


def get_plant_transform_obj(plant_transform_obj=None,

                            crop_window=None,
                            srcwin=None,

                            offset_x=None,
                            offset_y=None,
                            width=None,
                            length=None,

                            geotransform=None,
                            projection=None,
                            topo_dir=None,

                            mask_ref=None,
                            mask_equal=None,
                            mask_different=None,
                            mask_greater=None,
                            mask_less=None,
                            mask_greater_equal=None,
                            mask_less_equal=None,
                            mask_erode=None,
                            mask_border=None,

                            select_col=None,
                            select_row=None,
                            select_geo_x=None,
                            select_geo_y=None,
                            polygon=None,
                            geo_polygon=None,

                            out_mask=None,
                            mask_as_input=False,

                            function_dict=None,
                            transform_abs=None,
                            transform_real=None,
                            transform_imag=None,
                            transform_conj=None,

                            transform_fft_x=None,
                            transform_fft_y=None,
                            transform_fft=None,
                            transform_ifft_x=None,
                            transform_ifft_y=None,
                            transform_ifft=None,

                            transform_square=None,
                            transform_square_root=None,
                            transform_negative=None,
                            transform_inverse=None,
                            transform_reverse_x=None,
                            transform_reverse_y=None,
                            transform_angle=None,
                            transform_angle_deg=None,
                            transform_phase_wrap=None,
                            transform_sin=None,
                            transform_arcsin=None,
                            transform_cos=None,
                            transform_arccos=None,
                            transform_tan=None,
                            transform_arctan=None,
                            transform_deg2rad=None,
                            transform_rad2deg=None,
                            transform_db10=None,
                            transform_db20=None,
                            transform_inv_db10=None,
                            transform_inv_db20=None,

                            in_mask=None,
                            memory_mask=None,
                            null=None,
                            verbose=False):
    kwargs = locals()
    plant_transform_obj = PlantTransform(**kwargs)
    return plant_transform_obj


def flag_apply_function_key(key, args, kwargs, plant_transform_obj):
    if kwargs.pop(key, False) or key in args:
        return True
    if (plant_transform_obj is not None and
            plant_transform_obj.function_dict is not None):
        # print('*** ', plant_transform_obj.function_dict.keys())
        if plant_transform_obj.function_dict['transform_%s' % key]:
            return True
    return False


def apply_function(*args, **kwargs):
    image = args[0]
    args = args[1:]
    if len(args) == 1 and args[0] is None:
        return image
    tr_obj = kwargs.pop('plant_transform_obj', None)
    flag_return_float = kwargs.pop('flag_return_float', False)
    flag_inv_db10 = flag_apply_function_key('inv_db10', args, kwargs, tr_obj)
    flag_inv_db20 = flag_apply_function_key('inv_db20', args, kwargs, tr_obj)
    flag_reverse_x = flag_apply_function_key('reverse_x', args, kwargs, tr_obj)
    flag_reverse_y = flag_apply_function_key('reverse_y', args, kwargs, tr_obj)
    flag_negative = flag_apply_function_key('negative', args, kwargs, tr_obj)
    flag_inverse = flag_apply_function_key('inverse', args, kwargs, tr_obj)
    flag_angle = flag_apply_function_key('angle', args, kwargs, tr_obj)
    flag_angle_deg = flag_apply_function_key('angle_deg', args, kwargs, tr_obj)
    flag_phase_wrap = flag_apply_function_key('phase_wrap',
                                              args, kwargs, tr_obj)
    flag_sin = flag_apply_function_key('sin', args, kwargs, tr_obj)
    flag_arcsin = flag_apply_function_key('arcsin', args, kwargs, tr_obj)
    flag_cos = flag_apply_function_key('cos', args, kwargs, tr_obj)
    flag_arccos = flag_apply_function_key('arccos', args, kwargs, tr_obj)
    flag_tan = flag_apply_function_key('tan', args, kwargs, tr_obj)
    flag_arctan = flag_apply_function_key('arctan', args, kwargs, tr_obj)
    flag_deg2rad = flag_apply_function_key('deg2rad', args, kwargs, tr_obj)
    flag_rad2deg = flag_apply_function_key('rad2deg', args, kwargs, tr_obj)
    flag_fft_x = flag_apply_function_key('fft_x', args, kwargs, tr_obj)
    flag_fft_y = flag_apply_function_key('fft_y', args, kwargs, tr_obj)
    flag_fft = flag_apply_function_key('fft', args, kwargs, tr_obj)
    flag_ifft_x = flag_apply_function_key('ifft_x', args, kwargs, tr_obj)
    flag_ifft_y = flag_apply_function_key('ifft_y', args, kwargs, tr_obj)
    flag_ifft = flag_apply_function_key('ifft', args, kwargs, tr_obj)
    flag_square = flag_apply_function_key('square', args, kwargs, tr_obj)
    flag_square_root = flag_apply_function_key('square_root', args, kwargs,
                                               tr_obj)
    # flag_square_root = (flag_square_root or
    #                   flag_apply_function_key('sqrt', args, kwargs, tr_obj))
    flag_db10 = flag_apply_function_key('db10', args, kwargs, tr_obj)
    flag_db20 = flag_apply_function_key('db20', args, kwargs, tr_obj)
    flag_abs = flag_apply_function_key('abs', args, kwargs, tr_obj)
    flag_real = flag_apply_function_key('real', args, kwargs, tr_obj)
    flag_imag = flag_apply_function_key('imag', args, kwargs, tr_obj)
    flag_conj = flag_apply_function_key('conj', args, kwargs, tr_obj)
    flag_abs = (flag_abs or flag_square or flag_db10 or flag_db20 or
                flag_square_root)
    # flag_angle or flag_angle_deg or
    # flag_square_root or flag_real or flag_imag)
    flag_float = (flag_abs or flag_square or flag_db10 or flag_db20 or
                  flag_angle or flag_angle_deg or
                  flag_square_root or flag_real or flag_imag)

    if flag_return_float:
        return flag_float

    local_variables = locals()
    found_function = False
    for key, value in local_variables.items():
        if not key.startswith('flag'):
            continue
        found_function |= value
    if not found_function:
        if len(args) == 0:
            return image
        function_str = ', '.join(args)
        print(f'ERROR invalid function: {function_str}')

    if flag_fft_x:
        image = np.fft.fft(image, axis=-1)
        image = np.fft.fftshift(image, axes=(-1))
    if flag_fft_y:
        image = np.fft.fft(image, axis=-2)
        image = np.fft.fftshift(image, axes=(-2))
    if flag_fft:
        image = np.fft.fft2(image)
        image = np.fft.fftshift(image, axes=(-1, -2))
    if flag_ifft_x:
        image = np.fft.ifftshift(image, axes=(-1))
        image = np.fft.ifft(image, axis=-1)
    if flag_ifft_y:
        image = np.fft.ifftshift(image, axes=(-2))
        image = np.fft.ifft(image, axis=-2)
    if flag_ifft:
        image = np.fft.ifftshift(image, axes=(-1, -2))
        image = np.fft.ifft2(image)
    if flag_inv_db10:
        # print('*** inv_db10')
        # print('*** (before): ', image[30, 30], image[30, 1100])
        image = 10 ** (image/10)
        # print('*** (after): ', image[30, 30], image[30, 1100])
    if flag_inv_db20:
        image = 10 ** (image/20)
    if flag_reverse_x and not isinstance(image, numbers.Number):
        image = image[:, ::-1]
    if flag_reverse_y and not isinstance(image, numbers.Number):
        image = image[::-1, :]
    if flag_negative:
        image = -image
    if flag_inverse:
        image = 1.0/image
    if flag_deg2rad:
        image = np.radians(image)
    if flag_sin:
        image = np.sin(image)
    if flag_arcsin:
        image = np.arcsin(image)
    if flag_cos:
        image = np.cos(image)
    if flag_arccos:
        image = np.arccos(image)
    if flag_tan:
        image = np.tan(image)
    if flag_arctan:
        image = np.arctan(image)
    if flag_rad2deg:
        image = np.degrees(image)
    # elif flag_clip:
    #    if plant.isvalid(flag_clip[0]):
    #        image[np.where(image < flag_clip[0])]
    # = flag_clip[0]
    #        if plant.isvalid(flag_clip[1]):
    #     image[np.where(image > flag_clip[1])]
    # = flag_clip[1]
    if flag_angle:  # cast to float if complex
        image = np.angle(image)
    if flag_angle_deg:  # cast to float if complex
        image = np.angle(image, deg=True)
    if flag_phase_wrap:
        image = plant.wrap_phase(image)
    if flag_imag:
        image = np.imag(image)
    if flag_real:
        image = np.real(image)
    if flag_conj:
        image = np.conj(image)
    if flag_abs:  # cast to float if complex
        image = np.absolute(image)
        if flag_square:
            image *= image
        if flag_square_root:
            image = np.sqrt(image)
        if flag_db10 and isinstance(image, numbers.Number):
            image = 10 * np.log10(image)
        elif flag_db10:
            ind = np.where(image <= 0)
            image[ind] = np.nan
            ind = np.where(image > 0)
            image[ind] = 10 * np.log10(image[ind])
        if flag_db20 and isinstance(image, numbers.Number):
            image = 20 * np.log10(image)
        elif flag_db20:
            ind = np.where(image <= 0)
            image[ind] = np.nan
            ind = np.where(image > 0)
            image[ind] = 20 * np.log10(image[ind])
    # print('*** (after2): ', image[30, 30], image[30, 1100])
    return image


def _apply_mask_single_band(image_obj,
                            band=None,
                            output_format=None,
                            vmax=None,
                            vmin=None,

                            out_null=np.nan,
                            verbose=False,
                            dtype=None,
                            force=None,
                            input_format=None,
                            output_dtype=None,
                            plant_transform_obj=None,
                            null=np.nan):
    '''
    - create a mask using in_mask, polygon, col/row string
         1: valid data
         0: invalid data
    - vmin and vmax are applied on image
    '''
    # crop_window = get_crop_window(plant_transform_obj=plant_transform_obj)
    crop_window = plant_transform_obj.crop_window
    offset_x, offset_y, width, length = crop_window
    '''
    flag_crop = False
    if (length is not None and image_obj.length is not None and
            length < image_obj.length and offset_y is not None):
        flag_crop = True

    if (width is not None and image_obj.width is not None and
            width < image_obj.width and offset_x is not None):
        flag_crop = True

    if flag_crop:
    '''
    apply_crop(image_obj,
               offset_x=offset_x,
               offset_y=offset_y,
               width=width,
               length=length,
               plant_transform_obj=plant_transform_obj,
               verbose=False)

    band_obj = image_obj.get_band(band=band)
    if (band_obj._applied_transform_obj_list is not None and
            plant_transform_obj is not None and
            id(plant_transform_obj) in band_obj._applied_transform_obj_list):
        return

    image_loaded = band_obj.image_loaded

    '''
    if image_obj._applied_transform_obj_list is None:
        image_obj._applied_transform_obj_list = [id(plant_transform_obj)]
    elif image_obj._applied_transform_obj_list is not None:
        image_obj._applied_transform_obj_list.append(
            id(plant_transform_obj))
    '''

    dtype = image_obj.dtype

    if (not image_loaded and
            plant_transform_obj.mask_as_input):
        image_obj.set_dtype(MASK_DTYPE, realize_changes=False)
        return
    elif ((not image_loaded and
           'complex' not in plant.get_dtype_name(dtype)) or
            (not plant_transform_obj.flag_apply_mask(image_obj=image_obj) and
             not plant_transform_obj.flag_apply_function())):
        # image_obj.set_image(image, band=band) 
        return
    elif not image_loaded:
        flag_float = apply_function(None,
                                    # abs=True,
                                    flag_return_float=True,
                                    plant_transform_obj=plant_transform_obj)
        if flag_float and 'complex128' in plant.get_dtype_name(dtype):
            image_obj.set_dtype(np.float64, realize_changes=False)
        elif flag_float:
            image_obj.set_dtype(np.float32, realize_changes=False)
        return

    length = image_obj.length
    width = image_obj.width
    length_orig = image_obj.length_orig
    width_orig = image_obj.width_orig

    image = np.copy(image_obj.get_image(band=band))
    # if length != image_obj.image.shape[0]:
    #    raise

    # function
    # print('*** ', kwargs)
    if plant_transform_obj.flag_apply_function():
        image = apply_function(image,
                               # abs=True,
                               plant_transform_obj=plant_transform_obj)

    plant_transform_obj_mask = copy.deepcopy(plant_transform_obj)
    plant_transform_obj_mask.in_mask = None
    plant_transform_obj_mask.mask_ref = None

    if plant_transform_obj.memory_mask is not None:
        mask = plant_transform_obj.memory_mask
    else:
        mask = np.ones_like(image, dtype=np.bool)

    # in_mask
    in_mask = plant_transform_obj.in_mask
    if (not in_mask and plant_transform_obj.mask_ref and
            plant_transform_obj.mask_ref.endswith('.shp')):
        in_mask = plant_transform_obj.mask_ref
        plant_transform_obj.mask_ref = None

    if in_mask:
        # if plant.IMAGE_NAME_SEPARATOR in plant_transform_obj.in_mask:
        #    plant_transform_obj.in_mask = \
        #        plant_transform_obj.in_mask.split(
        #            plant.IMAGE_NAME_SEPARATOR)[0]
        mask_obj = plant.read_image(
            in_mask,
            input_width=width,
            input_length=length,
            geotransform=plant_transform_obj.geotransform,
            # flag_exit_if_error=False,
            # verbose=False,
            projection=plant_transform_obj.projection,
            plant_transform_obj=plant_transform_obj_mask)

        # if image_obj is None:
        #    image_obj = mask_obj
        if dtype is None:
            dtype = mask_obj.dtype
        '''
        flag_crop = False
        if length is not None and mask_obj.length is not None:
            if length < mask_obj.length and offset_y is not None:
                flag_crop = True
        if width is not None and mask_obj.width is not None:
            if width < mask_obj.width and offset_x is not None:
                flag_crop = True
        if flag_crop:
        apply_crop(mask_obj,
                   offset_x=offset_x,
                   offset_y=offset_y,
                   width=width,
                   length=length,
                   plant_transform_obj=plant_transform_obj,
                   verbose=False)
        '''
        mask = plant.copy_shape(image, mask_obj.image)

    if image is None and plant_transform_obj.mask_ref is None:
        print('ERROR input image is invalid')
        return

    if image is not None or plant_transform_obj.mask_ref is not None:
        if image is not None and plant.isvalid(vmin):
            ind_invalid = np.where(image < vmin)
            mask[ind_invalid] = 0
        if image is not None and plant.isvalid(vmax):
            ind_invalid = np.where(image > vmax)
            mask[ind_invalid] = 0
        if plant_transform_obj.mask_ref is None:
            mask_ref_image = image
        else:
            # print('*** opening mask_ref_obj')
            # with plant.PlantIndent():
            mask_ref_obj = plant.PlantImage(
                plant_transform_obj.mask_ref,
                plant_transform_obj=plant_transform_obj_mask)
            # print('*** opening mask_ref_obj.shape: ', mask_ref_obj.shape)
            # print('*** mask_ref.nbands: ', mask_ref_obj.nbands)
            # print('*** mask_ref id: ', id(mask_ref_obj))
            # if image_obj is None:
            #     image_obj = mask_ref_obj
            '''
            flag_crop = False
            if length is not None and mask_ref_obj.length is not None:
                if length < mask_ref_obj.length and offset_y is not None:
                    flag_crop = True
            if width is not None and mask_ref_obj.width is not None:
                if width < mask_ref_obj.width and offset_x is not None:
                    flag_crop = True
            if flag_crop:
            apply_crop(mask_ref_obj,
                       offset_x=offset_x,
                       offset_y=offset_y,
                       width=width,
                       length=length,
                       plant_transform_obj=plant_transform_obj,
                       verbose=False)
            '''
            if dtype is None:
                dtype = mask_ref_obj.dtype
            # mask_ref_image = mask_ref_obj.image
            # print('*** image: ', image)
            if image.shape != mask_ref_obj.shape:
                print(f'ERROR the shape of mask_ref {mask_ref_obj.shape}'
                      f' does not match the input shape: {image.shape}')
                return
            mask_ref_image = mask_ref_obj.image
            # mask_ref_image = plant.copy_shape(image, mask_ref_obj.image)
            # print('*** mask_ref_obj.image: ', mask_ref_obj.image)
            # print('*** mask_ref_image: ', mask_ref_image)
            '''
            if length is not None and mask_ref_obj.length is not None:
                if length != mask_ref_obj.length:
                    print('ERROR mask length (%d) different '
                          'from image length (%d)'
                          % (mask_ref_obj.length, length))
                    return
            else:
                length = mask_ref_obj.length
            if width is not None and mask_ref_obj.width is not None:
                if width != mask_ref_obj.width:
                    print('ERROR mask width (%d) different '
                          'from image width (%d)'
                          % (mask_ref_obj.width, width))
                    return
            else:
                width = mask_ref_obj.width
            '''

        if 'c' in plant.get_dtype_name(mask_ref_image):
            mask_ref_image = np.abs(mask_ref_image)

        try:
            mask[np.where(plant.isnan(mask_ref_image, null))] = 0
        except ValueError:
            mask = mask.copy()
            mask[np.where(plant.isnan(mask_ref_image, null))] = 0
        # with np.errstate(invalid='ignore', divide='ignore'):

        if plant_transform_obj.mask_greater is not None:
            mask[np.where(mask_ref_image >
                          plant_transform_obj.mask_greater)] = 0
        if plant_transform_obj.mask_greater_equal is not None:
            mask[np.where(mask_ref_image >=
                          plant_transform_obj.mask_greater_equal)] = 0
        if plant_transform_obj.mask_less is not None:
            mask[np.where(mask_ref_image <
                          plant_transform_obj.mask_less)] = 0
        if plant_transform_obj.mask_less_equal is not None:
            mask[np.where(mask_ref_image <=
                          plant_transform_obj.mask_less_equal)] = 0
        if plant_transform_obj.mask_equal is not None:
            mask[np.where(mask_ref_image ==
                          plant_transform_obj.mask_equal)] = 0
        if plant_transform_obj.mask_different is not None:
            mask[np.where(mask_ref_image !=
                          plant_transform_obj.mask_different)] = 0

    if (plant_transform_obj.topo_dir is not None and
            (plant_transform_obj.select_geo_y or
             plant_transform_obj.select_geo_x)):
        if plant_transform_obj.select_geo_y:
            lat_list = plant.read_image(
                plant_transform_obj.select_geo_y,
                plant_transform_obj=None,
                verbose=False).image.ravel().tolist()
            if len(lat_list) > 2:
                print(f'ERROR lat selection is implemented'
                      f' for up to 2 elements. Input array: {lat_list}')
            lat_beg = lat_list[0]
            lat_end = lat_list[-1]
        if plant_transform_obj.select_geo_x:
            lon_list = plant.read_image(
                plant_transform_obj.select_geo_x,
                plant_transform_obj=None,
                verbose=False).image.ravel().tolist()
            if len(lon_list) > 2:
                print(f'ERROR lon selection is implemented'
                      f' for up to 2 elements. Input array: {lon_list}')
            lon_beg = lon_list[0]
            lon_end = lon_list[-1]

        ret = plant.get_lat_lon_files_from_topo_dir(
            plant_transform_obj.topo_dir)
        file_lat, file_lon = ret
        lat_image = plant.read_matrix(file_lat,
                                      plant_transform_obj=None)
        lat_image = apply_crop(lat_image,
                               plant_transform_obj=plant_transform_obj)
        lon_image = plant.read_matrix(file_lon,
                                      plant_transform_obj=None)
        lon_image = apply_crop(lon_image,
                               plant_transform_obj=plant_transform_obj)
        if plant_transform_obj.select_geo_y:
            mask &= (lat_image >= lat_beg) & (lat_image < lat_end)
        if plant_transform_obj.select_geo_x:
            mask &= (lon_image >= lon_beg) & (lon_image < lon_end)

    flag_use_and = False
    if flag_use_and:
        mask_sel = np.ones_like(mask, dtype=np.bool)
        mask_operation = np.logical_and
    else:
        mask_sel = np.zeros_like(mask, dtype=np.bool)
        mask_operation = np.logical_or

    mask_temp = np.zeros_like(mask, dtype=np.bool)

    # Polygon
    if (plant_transform_obj.polygon or
            plant_transform_obj.geo_polygon):
        # print('*** polygon: ', plant_transform_obj.polygon)
        # print('*** geo_polygon: ', plant_transform_obj.geo_polygon)
        polygon_list = []
        if plant_transform_obj.polygon:
            polygon_list.append([plant_transform_obj.polygon, False])
        if plant_transform_obj.geo_polygon:
            polygon_list.append([plant_transform_obj.geo_polygon, True])
        for filename, flag_geo in polygon_list:
            ret = read_polygon_from_file(
                filename,
                width=width,
                length=length,
                geotransform=plant_transform_obj.geotransform,
                projection=plant_transform_obj.projection,
                flag_geo=flag_geo,
                verbose=verbose)
            if ret is None:
                continue
            rows, cols = ret
            if plant.get_sequence_depth(rows) == 1:
                rows = [rows]
            if plant.get_sequence_depth(cols) == 1:
                cols = [cols]
            for row, col in zip(rows, cols):
                if offset_x is not None:
                    col = [min([max([x-offset_x, 0]), width-1])
                           for x in col]
                if offset_y is not None:
                    row = [min([max([x-offset_y, 0]), length-1])
                           for x in row]
                if ((len(col) != len(row)) and
                        (len(col) > 2 or len(row) > 2)):
                    print('ERROR --col and --row have different '
                          'number of elements')
                    sys.exit(1)
                elif (len(col) == len(row) and
                      (len(col) > 2 or len(row) > 2)):
                    p = Polygon(col, row)
                    offset_col, offset_row, sel_width, \
                        sel_length = p.getBoundary()
                    outsides = p.outside()
                    sel_length = int(sel_length)
                    sel_width = int(sel_width)
                    outsides = np.asarray(outsides).reshape(
                        sel_length, sel_width)
                    sel_width = min([sel_width, mask.shape[1] - offset_col])
                    sel_length = min([sel_length, mask.shape[0] -
                                      offset_row])
                    sel_length = int(sel_length)
                    sel_width = int(sel_width)
                    outsides = outsides[:sel_length, :sel_width]
                    offset_row = int(offset_row)
                    offset_col = int(offset_col)
                    mask_temp[offset_row:offset_row + sel_length,
                              offset_col:offset_col + sel_width] = 1 - outsides
                    mask_sel = mask_operation(mask_sel, mask_temp)
                    mask_temp = np.zeros_like(mask, dtype=np.bool)
    
    rows = []
    cols = []

    if plant_transform_obj.srcwin is not None:
        rows.append(np.arange(plant_transform_obj.srcwin[1],
                              plant_transform_obj.srcwin[1] +
                              plant_transform_obj.srcwin[3]).tolist())
        cols.append(np.arange(plant_transform_obj.srcwin[0],
                              plant_transform_obj.srcwin[0] +
                              plant_transform_obj.srcwin[2]).tolist())

    if plant_transform_obj.select_row is not None:
        ret = plant.read_slice(
            plant_transform_obj.select_row,
            default_stop=length_orig)
        if ret is not None:
            rows += ret
    if plant_transform_obj.select_col is not None:
        ret = plant.read_slice(
            plant_transform_obj.select_col,
            default_stop=width_orig)
        if ret is not None:
            cols += ret

    for i in range(len(cols)):
        for current_col in cols[i]:
            if plant.isnan(current_col):
                continue
            col = int(current_col)
            if offset_x is not None:
                col = int(current_col-offset_x)
            # if offset_x is not None:
            #     col = min([max([col-offset_x, 0]), width-1])
            # single-col selection
            mask_temp[..., col] = 1
            mask_sel = mask_operation(mask_sel, mask_temp)
            mask_temp = np.zeros_like(mask, dtype=np.bool)
    for i in range(len(rows)):
        for current_row in rows[i]:
            if plant.isnan(current_row):
                continue
            row = int(current_row)
            if offset_y is not None:
                row = int(current_row)-offset_y
            if row < 0 or row > length-1:
                continue
            # if offset_y is not None:
            #    row = min([max([row-offset_y, 0]), length-1])
            # single-line selection
            mask_temp[..., row, :] = 1
            mask_sel = mask_operation(mask_sel, mask_temp)
            mask_temp = np.zeros_like(mask, dtype=np.bool)

    if flag_use_and or np.any(mask_sel):
        mask = np.logical_and(mask, mask_sel)

    if plant_transform_obj.mask_erode is not None:
        from scipy.ndimage import binary_erosion
        flag_reshape = mask.shape[0] == 1
        if flag_reshape:
            mask = mask.ravel()
        mask = binary_erosion(mask,
                              iterations=plant_transform_obj.mask_erode)
        if flag_reshape:
            mask = mask.reshape((1, mask.size))

    if plant_transform_obj.mask_border is not None:
        from scipy.ndimage import binary_erosion, binary_dilation
        flag_reshape = mask.shape[0] == 1
        if flag_reshape:
            mask = mask.ravel()
        mask_temp = binary_dilation(mask,
                                    iterations=plant_transform_obj.mask_border)
        mask_temp = binary_erosion(mask_temp,
                                   iterations=plant_transform_obj.mask_border)
        mask = np.logical_or(mask, mask_temp)
        mask = binary_erosion(mask,
                              iterations=plant_transform_obj.mask_border)
        if flag_reshape:
            mask = mask.reshape((1, mask.size))

    mask = np.asarray(mask, dtype=MASK_DTYPE)

    if plant_transform_obj.out_mask:
        # if image_obj is not None:
        image_temp_obj = plant.PlantImage(image_obj)
        image_temp_obj.set_image(mask)
        image_temp_obj.nbands = 1
        # else:
        #    image_temp_obj = mask
        plant.save_image(image_temp_obj,
                         plant_transform_obj.out_mask,
                         verbose=verbose,
                         force=force,
                         output_format=output_format,
                         output_dtype=np.byte)

    if band_obj._applied_transform_obj_list is None:
        band_obj._applied_transform_obj_list = [id(plant_transform_obj)]
    elif band_obj._applied_transform_obj_list is not None:
        band_obj._applied_transform_obj_list.append(
            id(plant_transform_obj))

    if plant_transform_obj.mask_as_input:
        image_obj.set_image(mask, band=band)
        return

    ind = np.where(mask == 0)
    flag_mask = plant.get_indexes_len(ind) != 0

    if flag_mask:
        # image = np.copy(image_obj.get_image(band=current_band))
        image = plant.insert_nan(image, ind, out_null=np.nan)
    # print('*** image: ', image)
    image_obj.set_image(image, band=band)
    # flag_changed = flag_mask or plant_transform_obj.flag_apply_function()
    # plant_transform_obj.is_transformation_applied = True
    # if flag_changed:
    # return image, flag_changed

def read_polygon_from_file(polygon,
                           flag_from_file=True,
                           width=None,
                           length=None,
                           geotransform=None,
                           projection=None,
                           flag_geo=False,
                           verbose=True,
                           **kwargs):
    if flag_geo and geotransform is None:
        # print('WARNING geotransform not found to'
        # f' process georeferenced polygon: {polygon}')
        return
    
    ret = parse_polygon_from_file(polygon,
                                  flag_from_file=flag_from_file,
                                  verbose=verbose)
    if ret is None or not flag_geo:
        return ret

    ret_geo = plant.get_point(geotransform=geotransform,
                              projection=projection,
                              lat=ret[0],
                              lon=ret[1],
                              verbose=verbose)

    if ret_geo is None:
        return ret_geo

    rows = ret_geo['y_pos']
    cols = ret_geo['x_pos']
    return rows, cols
    


def parse_polygon_from_file(polygon,
                            flag_from_file=True,
                            verbose=True):

    if flag_from_file:
        try:
            polygon_obj = plant.read_image(polygon,
                                           # input_width=width,
                                           # input_length=length,
                                           # geotransform=geotransform,
                                           # projection=projection
                                           flag_exit_if_error=False,
                                           verbose=False)
        except:
            polygon_obj = None
        if polygon_obj is not None:
            # return polygon_obj
            rows_array = None
            cols_array = None

            if polygon_obj.nbands == 1 and polygon_obj.length == 2:
                band = polygon_obj.get_band(band=0)
                for b in range(polygon_obj.length):
                    rows_array = band.image[0, :]
                    cols_array = band.image[1, :]
            elif polygon_obj.nbands == 1 and polygon_obj.width == 2:
                band = polygon_obj.get_band(band=0)
                for b in range(polygon_obj.length):
                    rows_array = band.image[:, 0]
                    cols_array = band.image[:, 1]
            else:
                for b in range(polygon_obj.nbands):
                    band = polygon_obj.get_band(band=b)
                    if band.name == plant.Y_BAND_NAME:
                        if verbose:
                            print('(polygon)'
                                  f' Y-values found at band: {b}')
                        rows_array = band.image
                    elif band.name == plant.X_BAND_NAME:
                        if verbose:
                            print('(polygon)'
                                  f' X-values found at band: {b}')
                        cols_array = band.image
                    elif polygon_obj.nbands == 2 and b == 0:
                        if verbose:
                            print('(polygon)'
                                  ' considering Y-values from:'
                                  f' polygon band: {b}')
                        rows_array = band.image
                    elif polygon_obj.nbands == 2 and b == 1:
                        if verbose:
                            print('(polygon)'
                                  ' considering X-values from:'
                                  f' polygon band: {b}')
                        cols_array = band.image
            if rows_array is not None and cols_array is None:
                print(f'ERROR reading polygon from: {polygon}')
                return
            if rows_array is not None and cols_array is not None:
                # rows = rows.ravel()
                # cols = cols.ravel()
                # rows_with_nan_list = [rows_array.ravel().tolist()]
                # cols_with_nan_list = [cols_array.ravel().tolist()]
                rows_with_nan_list = rows_array.tolist()
                cols_with_nan_list = cols_array.tolist()

                if (len(rows_with_nan_list) > 0 and 
                        plant.isnumeric(rows_with_nan_list[0])):
                    rows_with_nan_list = [rows_with_nan_list]
                if (len(cols_with_nan_list) > 0 and 
                        plant.isnumeric(cols_with_nan_list[0])):
                    cols_with_nan_list = [cols_with_nan_list]
                rows = []
                cols = []
                for row, col in zip(rows_with_nan_list,
                                    cols_with_nan_list):
                    rows.append([x for x in row if plant.isvalid(x)])
                    cols.append([x for x in col if plant.isvalid(x)])
                if verbose:
                    print('polygon:')
                    with plant.PlantIndent():
                        print(f'rows: {rows}')
                        print(f'cols: {cols}')
                return rows, cols

    cols = ['']
    rows = ['']
    col_index = None
    row_index = None
    # flag_from_file = plant.isfile(polygon)
    if flag_from_file:
        with open(polygon, 'r') as f:
            polygon = f.readlines()
    '''
    if 'COL' not in ''.join(polygon):
        row_index = None  # 5
        col_index = None  # 3
        if verbose:
            print('    polygon copied from bash')
    else:
    '''
    if 'COL' not in ''.join(polygon):
        row_index = 1
        col_index = 0
        if verbose:
            print('    polygon from mdx "select" option')

    for polygon_line in polygon:
        while polygon_line != polygon_line.replace('  ', ' '):
            polygon_line = polygon_line.replace('  ', ' ')
        polygon_line = polygon_line.strip().split(' ')
        if col_index is None:
            for j in range(3, len(polygon_line)):
                if 'COL' in polygon_line[j]:
                    col_index = j+1
                    row_index = j+3
                    break
        if col_index is None:
            continue
        try:
            col = polygon_line[col_index]
            row = polygon_line[row_index]
        except IndexError:
            continue
        if not plant.isnumeric(col) or not plant.isnumeric(row):
            continue
        cols.append(col)
        rows.append(row)
    if col_index is not None:
        cols = ':'.join(cols[1:])
        rows = ':'.join(rows[1:])
    # if col_temp is not '':
    #    cols = cols + plant.ELEMENT_SEPARATOR + col_temp
    # if row_temp is not '':
    #    rows = rows + plant.ELEMENT_SEPARATOR + row_temp
    return rows, cols


def get_crop_window(select_row=None,
                    select_col=None,
                    polygon=None,
                    geo_polygon=None,

                    srcwin=None,
                    crop_window=None,

                    offset_x=None,
                    offset_y=None,
                    width=None,
                    length=None,

                    geotransform=None,
                    projection=None,
                    length_orig=None,
                    width_orig=None,
                    # projection=None,
                    topo_dir=None,
                    select_geo_x=None,
                    select_geo_y=None,

                    verbose=True):
    # based on GDAL's srcwin
    if crop_window is not None:
        offset_x = offset_x if offset_x is None else crop_window[0]
        offset_y = offset_y if offset_y is None else crop_window[1]
        width = width if width is None else crop_window[2]
        length = length if length is None else crop_window[3]

    if (offset_x is not None and
            offset_y is not None and
            width is not None and
            length is not None):
        return [offset_x, offset_y, width, length]

    rows = []
    cols = []
    if select_geo_x or select_geo_y:
        # if geotransform is not None:
        #    print('ERROR not implemented. Please use plant_mosaic')
        if select_geo_y:
            lat_list = plant.read_image(
                select_geo_y,
                plant_transform_obj=None,
                verbose=False).image.ravel().tolist()
            if len(lat_list) > 2:
                print(f'ERROR lat selection is implemented'
                      f' for up to 2 elements. Input array: {lat_list}')
            lat_beg = lat_list[0]
            lat_end = lat_list[-1]
        if select_geo_x:
            lon_list = plant.read_image(
                select_geo_x,
                plant_transform_obj=None,
                verbose=False).image.ravel().tolist()
            if len(lon_list) > 2:
                print(f'ERROR lon selection is implemented'
                      f' for up to 2 elements. Input array: {lon_list}')
            lon_beg = lon_list[0]
            lon_end = lon_list[-1]

        if geotransform is not None and length_orig is not None:
            ret_dict = plant.get_coordinates(geotransform=geotransform,
                                             projection=projection)
            if ret_dict is None:
                print(f'error invalid geotransform: {geotransform}'
                      ' or projection: {projection}')
                return
            lat_arr = ret_dict['lat_arr']
            lon_arr = ret_dict['lon_arr']
            step_lat = ret_dict['step_lat']
            step_lon = ret_dict['step_lon']
            # lat_size = ret_dict['lat_size']
            # lon_size = ret_dict['lon_size']
            if select_geo_y:
                lat_orig_beg = lat_arr[0]
                y_beg = int(round((lat_beg-lat_orig_beg)/step_lat))
                y_beg = int(length_orig-1-y_beg)
                y_end = int(round((lat_end-lat_orig_beg)/step_lat))
                y_end = int(length_orig-1-y_end)
                rows.append([y_beg, y_end])

            if select_geo_x:
                lon_orig_beg = lon_arr[0]
                x_beg = int(round((lon_beg-lon_orig_beg)/step_lon))
                x_end = int(round((lon_end-lon_orig_beg)/step_lon))
                cols.append([x_beg, x_end])
        elif topo_dir is not None:
            ret = plant.get_lat_lon_files_from_topo_dir(topo_dir)
            if ret is None:
                print(f'ERROR reading topo files from {topo_dir}')
                return
            file_lat, file_lon = ret
            lat_image = plant.read_matrix(file_lat)
            lon_image = plant.read_matrix(file_lon)
            mask = None
            if select_geo_y:
                mask = np.ones_like(lat_image, dtype=np.bool)
                mask &= (lat_image >= lat_beg) & (lat_image < lat_end)
            if select_geo_x:
                if mask is None:
                    mask = np.ones_like(lon_image, dtype=np.bool)
                mask &= (lon_image >= lon_beg) & (lon_image < lon_end)

            ret_dict = plant.crop_valid_box_image_obj(mask,
                                                      null=False,
                                                      flag_all=True,
                                                      verbose=verbose)
            if ret_dict is None:
                print('ERROR not valid points after applying lat/lon'
                      ' selection')
            cols.append([ret_dict['min_x'], ret_dict['max_x']])
            rows.append([ret_dict['min_y'], ret_dict['max_y']])
        # else:
        #    print('ERROR no geotransform or topo_dir available'
        #          ' for lat/lon selection options')

    if srcwin is not None:
        rows.append(np.arange(srcwin[1], srcwin[1]+srcwin[3]).tolist())
        cols.append(np.arange(srcwin[0], srcwin[0]+srcwin[2]).tolist())

    if select_row is not None:
        ret = plant.read_slice(select_row, default_stop=length_orig)
        if ret is not None:
            rows += ret
    if select_col is not None:
        ret = plant.read_slice(select_col, default_stop=width_orig)
        if ret is not None:
            cols += ret

    min_col = None
    max_col = None
    for i in range(len(cols)):
        for current_col in cols[i]:
            if plant.isnan(current_col):
                continue
            col = int(current_col)
            if (min_col is None or col < min_col):
                min_col = col
            if (max_col is None or col > max_col):
                max_col = col
    min_row = None
    max_row = None
    for i in range(len(rows)):
        for current_row in rows[i]:
            if plant.isnan(current_row):
                continue
            row = int(current_row)
            if (min_row is None or row < min_row):
                min_row = row
            if (max_row is None or row > max_row):
                max_row = row

    if polygon or geo_polygon:
        polygon_list = []
        if polygon:
            polygon_list.append([polygon, False])
        if geo_polygon:
            polygon_list.append([geo_polygon, True])
        for filename, flag_geo in polygon_list:
            ret = read_polygon_from_file(
                filename,
                width=width,
                length=length,
                geotransform=geotransform,
                projection=projection,
                flag_geo=flag_geo,
                verbose=verbose)
            if ret is None:
                continue

            rows, cols = ret
            if plant.get_sequence_depth(rows) == 1:
                rows = [rows]
            if plant.get_sequence_depth(cols) == 1:
                cols = [cols]

            for row, col in zip(rows, cols):
                # row, col = ret
                if ((len(col) != len(row)) and
                        (len(col) > 2 or len(row) > 2)):
                    print('ERROR --col and --row have different '
                          'number of elements')
                    sys.exit(1)
                if (len(col) != len(row) or
                        not (len(col) > 2 or len(row) > 2)):
                    continue
                p = Polygon(col, row)
                offset_col, offset_row, sel_width, \
                    sel_length = p.getBoundary()
                if (min_col is None or
                        offset_col > min_col):
                    min_col = offset_col
                if (min_row is None or
                        offset_row > min_row):
                    min_row = offset_row
                if (max_col is None or
                        offset_col+sel_width < max_col):
                    max_col = offset_col+sel_width
                if (max_row is None or
                        offset_row+sel_length < max_row):
                    max_row = offset_row+sel_length
    
    '''
    if select_row:
        if rows:
            rows += plant.ELEMENT_SEPARATOR
        rows += select_row
    if len(rows) == 0:
        rows = []
    elif ':' not in rows:
        rows = [rows.replace(plant.ELEMENT_SEPARATOR, ':')]
    else:
        rows = rows.split(plant.ELEMENT_SEPARATOR)
    # if select_col is not None:
    if select_col:
        if cols:
            cols += plant.ELEMENT_SEPARATOR
        cols += select_col
    if len(cols) == 0:
        cols = []
    elif ':' not in cols:
        cols = [cols.replace(plant.ELEMENT_SEPARATOR, ':')]
    else:
        cols = cols.split(plant.ELEMENT_SEPARATOR)
    '''

    # print('*** rows: ', rows)
    # print('*** cols: ', cols)

    # row =
    # [int(float(x)) for x in row.split(':') if plant.isnumeric(x)]

    '''
        # Multi-lines selection
        if len(col) == 2:
            if (min_col is None or
                    col[0] > min_col):
                min_col = col[0]
            if (max_col is None or
                    col[1] < max_col):
                max_col = col[1]-1

        # Single-line selection
        elif len(col) == 1:
            min_col = col[0]
            max_col = col[0]

        # Multi-lines selection
        if len(row) == 2:
            if (min_row is None or
                    row[0] > min_row):
                min_row = row[0]
            if (max_row is None or
                    row[1] < max_row):
                max_row = row[1]-1

        # Single-line selection
        elif len(row) == 1:
            min_row = row[0]
            max_row = row[0]
    '''

    # print('offset_x: ', offset_x)
    # print('offset_y: ', offset_y)
    # print('min_col: ', min_col)
    # print('max_col: ', max_col)
    # print('min_row: ', min_row)
    # print('max_row: ', max_row)

    offset_x = offset_x if offset_x is not None else min_col
    offset_y = offset_y if offset_y is not None else min_row
    if offset_x is not None:
        offset_x = max([offset_x, 0])
    if offset_y is not None:
        offset_y = max([offset_y, 0])

    if (max_row is not None and
            length_orig is not None):
        max_row = min([max_row, length_orig-1])

    if (max_col is not None and
            width_orig is not None):
        max_col = min([max_col, width_orig-1])

    if (max_col is not None and
        min_col is not None and
        (width is None or
         width < max_col-min_col+1)):
        width = max_col-min_col+1

    if (max_row is not None and
        min_row is not None and
        (length is None or
         length < max_row-min_row+1)):
        length = max_row-min_row+1

    error_message = ('ERROR cropping input. Please check '
                     'cropping parameters.')
    if length is not None and length <= 0:
        print(error_message)
        sys.exit(1)
    if width is not None and width <= 0:
        print(error_message)
        sys.exit(1)
    return [offset_x, offset_y, width, length]


# @debug_decorator(debug_level=5)
def apply_crop(image,
               offset_x=None,
               offset_y=None,
               end_x=None,
               end_y=None,
               width=None,
               length=None,
               plant_transform_obj=None,
               overwrite_obj=True,
               verbose=True):
    # print(f'*** cropping {image}')
    # raise
    flag_input_is_obj = isinstance(image, plant.PlantImage)
    image_obj = plant.PlantImage(image)
    if flag_input_is_obj and not overwrite_obj:
        image_obj = image_obj.copy()
    if offset_x is None and plant_transform_obj is not None:
        offset_x = plant_transform_obj.offset_x
    if offset_y is None and plant_transform_obj is not None:
        offset_y = plant_transform_obj.offset_y
    if width is None and plant_transform_obj is not None:
        width = plant_transform_obj.width
    if length is None and plant_transform_obj is not None:
        length = plant_transform_obj.length
    if offset_x is None:
        offset_x = 0
    if offset_y is None:
        offset_y = 0
    if width is None and end_x is not None and offset_x is not None:
        width = end_x - offset_x + 1
    elif width is None:
        width = image_obj.width
    # if width is None:
    #     width = image_obj.width_orig
    if length is None and end_y is not None and offset_y is not None:
        length = end_y - offset_y + 1
    elif length is None:
        length = image_obj.length
    # if length is None:
    #     length = image_obj.length_orig

    if not (offset_x or
            offset_y or
            width != image_obj.width_orig or
            length != image_obj.length_orig):
        if flag_input_is_obj:
            return image_obj
        return image_obj.image
    if flag_input_is_obj:
        crop_indexes_str = ('cropping %s with indexes: [%d:%d, %d:%d]' %
                            (image, offset_y, offset_y+length,
                             offset_x, offset_x+width))
    else:
        crop_indexes_str = ('cropping input with indexes: [%d:%d, %d:%d]' %
                            (offset_y, offset_y+length,
                             offset_x, offset_x+width))
    # flag_skip = False
    # flag_update_image_obj = False

    if image_obj.image_loaded:
        if verbose:
            print(crop_indexes_str)
        flag_error = True
        for band in range(0, image_obj.nbands):
            band_obj = image_obj.get_band(band=band)

            # print('*** this: ', id(band_obj))
            # print('*** crop_list: ',
            #       band_obj._applied_transform_obj_crop_list)

            if (plant_transform_obj is not None and
                    plant_transform_obj.is_crop_applied(band_obj)):
                flag_error = False
                continue

            image = band_obj.image
            if (all([dim == 1 for dim in image.shape]) or
                    image_obj.file_format == 'ARRAY'):
                # input is a constant, skipping crop
                flag_error = False
                continue
            if FLAG_CUT_TO_IMAGE_SIZE:
                if isinstance(image, list):
                    image = np.asarray(image)
                # try:
                # print('*** shape 1: ', image.shape)
                new_image = image[..., offset_y:offset_y+length,
                                  offset_x:offset_x+width]
                # print('*** new shape 1: ', new_image.shape)
            else:
                new_image = plant.cut_image_with_shape(image,
                                                       [length, width])

                # print('*** new shape 2: ', new_image.shape)
            flag_empty_band = any([dim == 0 for dim in new_image.shape])
            if (flag_empty_band and image_obj.nbands != 1):
                print(f'WARNING the band {band} is empty after cropping.')
                raise
            elif not flag_empty_band:
                flag_error = False
                image_obj.set_image(new_image, band=band)
                # flag_update_image_obj = True

            if (plant_transform_obj is not None and
                    not band_obj.image_loaded):
                band_obj._applied_transform_obj_crop_header_list.append(
                    plant_transform_obj.get_crop_dict())
            elif plant_transform_obj is not None:
                band_obj._applied_transform_obj_crop_image_list.append(
                    plant_transform_obj.get_crop_dict())

        filename = image_obj.filename_orig
        if flag_error and image_obj.nbands == 1:
            print(f'ERROR empty image after cropping {filename}'
                  f' with indexes: {crop_indexes_str}.'
                  ' Please, check cropping parameters.')
            return
        elif flag_error:
            print(f'ERROR invalid bands after cropping {filename}'
                  f' with indexes: {crop_indexes_str}.'
                  ' Please, check cropping parameters.')
            return
    if (plant_transform_obj is not None and
            plant_transform_obj.is_crop_applied(image_obj)):
        return

    if not image_obj.image_loaded:
        # print('WARNING insert new code here')
        image_obj._width = width
        image_obj._length = length
        # raise

    image_obj.offset_x = offset_x
    image_obj.offset_y = offset_y

    if plant_transform_obj is not None:
        image_obj._applied_transform_obj_crop_list.append(
            plant_transform_obj.get_crop_dict())

    # if not flag_update_image_obj:
    #     return
    # image_obj.width_orig = image_obj.width
    # image_obj.length_orig = image_obj.length
    geotransform = image_obj.geotransform
    # projection = image_obj.projection
    # print('*** (2) ', image_obj.width_orig)
    # print('*** (2) ', image_obj.length_orig)

    if plant_transform_obj is not None:
        image_obj.plant_transform_obj = plant_transform_obj
        # print('*** (3) ', image_obj.width_orig)
        # print('*** (3) ', image_obj.length_orig)
    if geotransform is None and flag_input_is_obj:
        return image_obj
    elif geotransform is None:
        return image_obj.image
    # print('*** (4) ', image_obj.width_orig)
    # print('*** (4) ', image_obj.length_orig)
    geotransform = crop_geotransform(geotransform,
                                     plant_transform_obj=plant_transform_obj,
                                     # offset_x=offset_x,
                                     # offset_y=offset_y,
                                     # new_lon_size=width,
                                     # new_lat_size=length,
                                     lon_size=image_obj.width_orig,
                                     lat_size=image_obj.length_orig)
    # ret = get_coordinates(geotransform=geotransform,
    #                       lon_size=width_orig,
    #                       lat_size=length_orig)
    # if ret is None:
    #     return
    # lat_arr, lon_arr, step_lat, step_lon, lat_size, lon_size = ret
    # geotransform = get_geotransform(lat_arr=lat_arr,
    #                                 lon_arr=lon_arr,
    #                                 step_lat=step_lat,
    #                                 step_lon=step_lon,
    #                                 lat_size=lat_size,
    #                                 lon_size=lon_size)
    image_obj.geotransform = geotransform
    # print('*** new geotransform: ', image_obj.geotransform)
    # image_obj.projection = projection
    if flag_input_is_obj:
        return image_obj
    return image_obj.image


def apply_null(image_orig,
               in_null=np.nan,
               out_null=np.nan,
               overwrite_obj=True,
               force_update=False):
    if isinstance(image_orig, plant.PlantImage):
        if not overwrite_obj:
            image_obj = image_orig.copy()
        else:
            image_obj = image_orig
        if not image_obj.image_loaded and not force_update:
            return image_obj
        for current_band in range(image_obj.nbands):
            if (plant.isnan(in_null, null=np.nan) and
                plant.isvalid(image_obj.get_null(band=current_band),
                              null=np.nan)):
                null_band = image_obj.get_null(band=current_band)
            else:
                null_band = in_null
            if plant.isnan(null_band) and plant.isnan(out_null):
                return image_obj
            # image = np.copy(image_obj.getImage(band=current_band))
            image = image_obj.get_image(band=current_band)
            image = apply_null(image,
                               in_null=null_band,
                               out_null=out_null,
                               force_update=force_update)
            if image is not None:
                image_obj.set_image(image, band=current_band)
        return image_obj
    elif isinstance(image_orig, list):
        image = np.asarray([image_orig], dtype=np.float)
    else:
        image = image_orig
    if image is None:
        return
    dtype = plant.get_dtype_name(image)
    # print('*** dtype: ', dtype)
    if 'bytes' in dtype:
        return image
    ind = np.where(plant.isnan(image, null=in_null))
    # if 'int' in plant.get_dtype_name(image):
    #    out_null = plant.INT_NAN
    if plant.get_indexes_len(ind) != 0:
        if (plant.isnan(out_null) and
                'int' in plant.get_dtype_name(image).lower()):
            print('WARNING output is integer. NULL value '
                  'will be converted to %d' % (plant.INT_NAN))
            out_null = plant.INT_NAN
        try:
            image[ind] = out_null
        except ValueError:
            image = np.copy(image)
            image[ind] = out_null
    if isinstance(image_orig, list):
        return list(image)
    return image


def crop_geotransform(geotransform_orig,
                      offset_x=None,
                      offset_y=None,
                      end_x=None,
                      end_y=None,
                      new_lon_size=None,
                      new_lat_size=None,
                      lon_size=None,
                      lat_size=None,
                      plant_transform_obj=None):
    if offset_x is None and plant_transform_obj is not None:
        offset_x = plant_transform_obj.offset_x
    if offset_y is None and plant_transform_obj is not None:
        offset_y = plant_transform_obj.offset_y
    if new_lon_size is None and plant_transform_obj is not None:
        new_lon_size = plant_transform_obj.width
    if new_lat_size is None and plant_transform_obj is not None:
        new_lat_size = plant_transform_obj.length

    if plant.isnan(offset_x):
        offset_x = 0
    if plant.isnan(offset_y):
        offset_y = 0
    if plant.isnan(new_lon_size) and plant.isvalid(end_x):
        new_lon_size = end_x - offset_x + 1
    if plant.isnan(new_lat_size) and plant.isvalid(end_y):
        new_lat_size = end_y - offset_y + 1

    if (not(offset_x) and
            not(offset_y) and
            (new_lat_size is None or
             new_lat_size == lat_size) and
            (new_lat_size is None or
             new_lat_size == lat_size)):
        return geotransform_orig

    step_lat = geotransform_orig[5]
    step_lon = geotransform_orig[1]
    lat_beg = geotransform_orig[3]
    lon_beg = geotransform_orig[0]

    '''
    print('*** geotransform_orig: ', geotransform_orig)
    print('*** lat_beg: ', lat_beg)
    print('*** offset_y: ', offset_y)
    print('*** end_y: ', end_y)
    if plant_transform_obj is not None:
        print('*** offset_y: ', plant_transform_obj.offset_y)
        print('*** end_y: ', plant_transform_obj.end_y)
    '''

    '''
    allow_lat_invertion = False
    if allow_lat_invertion:
        lat_end = np.nan
        if len(geotransform_orig) > 6:
            lat_end = geotransform_orig[7]
        elif (lat_size is not None and
              step_lat is not None):
            lat_end = (geotransform_orig[3] +
                       (lat_size-1)*step_lat)
        if step_lat < 0 and plant.isvalid(lat_end):
            temp = lat_beg
            lat_beg = lat_end
            lat_end = temp
            step_lat = -step_lat
        elif step_lat < 0:
            print('ERROR (crop_geotransform) please provide lat_size')
            return
    '''

    if plant.isvalid(step_lon):
        lon_beg_cut = (lon_beg +
                       step_lon*offset_x)
    else:
        lon_beg_cut = np.nan

    if plant.isvalid(step_lat) and step_lat < 0:
        lat_beg_cut = (lat_beg +
                       step_lat*offset_y)
    elif plant.isvalid(step_lat):
        '''
        print('*** new code!!!')
        print('*** lat_size: ', lat_size)
        print('*** new_lat_size: ', new_lat_size)
        print('*** offset_y: ', offset_y)
        print('*** new offset: ', (lat_size-new_lat_size -
                                   offset_y))
        '''
        lat_beg_cut = (lat_beg +
                       step_lat*(lat_size -
                                 (new_lat_size+offset_y)))
        '''
        print('*** lat_beg_cut: ', lat_beg_cut)
        '''
    else:
        lat_beg_cut = np.nan

    if (plant.isvalid(step_lon) and
            plant.isvalid(new_lon_size)):
        lon_end_cut = (lon_beg +
                       step_lon *
                       (offset_x+new_lon_size-1))
    else:
        lon_end_cut = np.nan

    if (plant.isvalid(step_lat) and
            plant.isvalid(new_lat_size)):
        lat_end_cut = (lat_beg_cut +
                       step_lat *
                       (new_lat_size-1))
    else:
        lat_end_cut = np.nan
    '''
    print('    lat beg: ', lat_beg)
    print('    lat beg cut: ', lat_beg_cut)
    print('    lon beg: ', lon_beg)
    print('    lon beg cut: ', lon_beg_cut)
    print('')
    # print('    lat end: ', lat_end)
    # print('    lon end: ', lon_end)
    print('    lat end cut: ', lat_end_cut)
    # print('    lon end: ', lon_end)
    print('    lon end cut: ', lon_end_cut)
    print('')
    print('    step_lat: ', step_lat)
    print('    step_lon: ', step_lon)
    print('    offset_x: ', offset_x)
    print('    offset_y: ', offset_y)
    print('')
    print('    new_lat_size: ', new_lat_size)
    print('    new_lon_size: ', new_lon_size)
    '''
    geotransform = [lon_beg_cut,
                    step_lon,
                    0,
                    lat_beg_cut,
                    0,
                    step_lat,
                    lon_end_cut,
                    lat_end_cut,
                    new_lon_size,
                    new_lat_size]
    # print('*** output geotransform: ', geotransform)
    if step_lat < 0:
        lat_beg_cut, lat_end_cut = lat_end_cut, lat_beg_cut
        step_lat = abs(step_lat)

    # if (plant.isvalid(lat_end_cut) and
    #        plant.isvalid(lon_end_cut) and
    #        plant.isvalid(new_lat_size) and
    #        plant.isvalid(new_lon_size)):
    geotransform = [lon_beg_cut,
                    step_lon,
                    0,
                    lat_beg_cut,
                    0,
                    step_lat,
                    lon_end_cut,
                    lat_end_cut,
                    new_lon_size,
                    new_lat_size]
    # print('*** output geotransform: ', geotransform)
    return geotransform
    # return geotransform_orig


def list_string_div(data_str, divisor):
    if data_str is None:
        return
    data_str_splitted = data_str.split(':')
    new_data_str_list = []
    for element in data_str_splitted:
        new_element_list = []
        for subelement in element.split(plant.ELEMENT_SEPARATOR):
            if not subelement:
                continue
            new_element = str(int(np.ceil(float(subelement)/divisor)))
            new_element_list.append(new_element)
        new_data_str_list.append(
            plant.ELEMENT_SEPARATOR.join(new_element_list))
    new_data_str = ':'.join(new_data_str_list)
    return new_data_str


def list_string_mult(data_str, multiplier):
    if data_str is None:
        return
    data_str_splitted = data_str.split(':')
    new_data_str_list = []
    for element in data_str_splitted:
        new_element_list = []
        for subelement in element.split(plant.ELEMENT_SEPARATOR):
            if not subelement:
                continue
            new_element = str(int(subelement)*multiplier)
            new_element_list.append(new_element)
        new_data_str_list.append(
            plant.ELEMENT_SEPARATOR.join(new_element_list))
    new_data_str = ':'.join(new_data_str_list)
    return new_data_str
