#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Frederico Croce, Marco Lavalle
# Copyright 2015, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


import sys
from os import path
import random
import operator
# import datetime
import numpy as np
import matplotlib as mpl
# import matplotlib
from collections.abc import Sequence
from matplotlib.patches import Rectangle
from matplotlib import style
from matplotlib.colors import ListedColormap
import matplotlib.animation as animation
from scipy.optimize import curve_fit
import plant
from itertools import groupby
from plant.modules.PlantScript import PlantScript
from matplotlib.ticker import FormatStrFormatter
from matplotlib.font_manager import FontProperties
# from plant.modules.plant_constants import ELEMENT_SEPARATOR

# SERIF_FAMILY_NAME = 'DejaVu Serif'
# SERIF_FAMILY_NAME = 'serif'
FLAG_MULTIPLOT_USE_NAME = True
FLAG_PLOT_BARPLOT_ERROR_VALUES = False
DUAL_POL_AVG = False
FLAG_FIX_AXIS_DECIMAL_PLACES = False
# DUAL_POL_AVG = False

# APP_NAME_MDX = 'plant_display.py'
# APP_NAME_DISPLAY = 'plant_display_lib.py'

HISTOGRAM = 'Histogram'
DENSITY_PLOT = 'Density plot'

GENERIC_PLOT = 'Plot'
# GENERIC_PLOT_MAX_NPOINTS = 100
GENERIC_PLOT_MAX_NPOINTS = 2**64

# TRENDPLOT_3D = 'Trendplot 3D'
PROFILE = 'Profile'
IMAGEVIEWER = 'Image viewer'
MULTIPLOT = 'Multiplot'
PRINT = 'Print data'
GEOLOCATION = 'Geo-location'

ANNOT_SIZE = 0.05
MAX_NAME_SIZE = 64
UPDATE_NAMES_LARGER_THAN = 17
MAX_LABEL_SIZE = 128
MAX_TITLE_SIZE = 128
DEFAULT_NBINS = 512
DEFAULT_NBINS_GENERIC_PLOT = 10
# DEFAULT_NBINS_TRENDPLOT = 10
MAX_NBINS = 2048
# FLAG_DEFAULT_TITLE_INPUT_NAMES = False

LINESTYLE_DEFAULT = 'solid'
LINESTYLE_STATS_DEFAULT = 'solid'
LINEWIDTH_STATS_DEFAULT = 2
LINECOLOR_STATS_DEFAULT_DARK = 'white'
LINECOLOR_STATS_DEFAULT = 'black'
COLOR_STATS_TEXT_DEFAULT = 'black'
# from plant.modules.constant import
# INPUT_SEPARATOR = ','

PREDEFINED_STYLES = ['0']


def get_parser():
    #                 get_single_args=False, get_all_args=False,
    #                 parse_known_args=None):
    '''
    Command line parser.
    '''
    app_name = 'plant_display.py'
    descr = ('Provides multiple tools for data visualization'
             ' and analysis:'
             ' image show (--imshow),'
             ' histogram (--hist),'
             ' X/Y profile (--profile-x/--profile-y)'
             ' 2-D histogram (--hist2d)'
             ' bar plot (--barplot),'
             ' scatterplot (--scatterplot)'
             ' trendplot (--trendplot)'
             ' multi plot (--multiplot)'
             ' text print (--print). The resulting plot'
             ' can be saved to any figure format using'
             ' "-o/--output-file". In the API mode,'
             ' the Matplotlib Pyplot and Figure classes'
             ' are available'
             ' through the class attributes plot and figure,'
             ' respectively and'
             ' the Axes or Subplot Axes object are'
             ' available through "ax" or "axes".')
    epilog = ('Image viewer:\n'
              '    '+app_name+' <INPUT> --imshow\n'
              'Image viewer (with jet colormap):\n'
              '    '+app_name+' <INPUT> --imshow --cmap jet\n'
              'Image viewer (RGB):\n'
              '    '+app_name+' <INPUT1> <INPUT2> <INPUT3> --imshow\n'
              'Image viewer (no RGB):\n'
              '    '+app_name+' <INPUT1> <INPUT2> <INPUT3> --no-rgb\n'
              'Histogram:\n'
              '    '+app_name+' <INPUT> --hist\n'
              'Histogram:\n'
              '    '+app_name+' <INPUT> --hist\n'
              'Histogram (dark theme):\n'
              '    '+app_name+' <INPUT> --hist --dark\n'
              '2D histogram (X, Y):\n'
              '    '+app_name+' <INPUT1> <INPUT2> --hist2d\n'
              '2D histogram (X, Y1 and Y2):\n'
              '    '+app_name+' <INPUT1> <INPUT2> <INPUT_3> --hist2d\n'
              'X-profile:\n'
              '    '+app_name+' <INPUT> --profilex\n'
              'Y-profile (without grid):\n'
              '    '+app_name+' <INPUT> --profiley --no-grid\n'
              'Multiplot:\n'
              '    '+app_name+' <INPUT1> <INPUT2> <INPUT3> --multiplot\n'
              'Barplot:\n'
              '    '+app_name+' <INPUT1> <INPUT2> <INPUT3> --barplot\n'
              'Barplot with multiple bar sets:\n'
              '    '+app_name+' *HH* --end-hy *HV* --end-hy *VV*'
              ' --barplot\n'
              'Barplot with multiple labelled bar sets:\n'
              '    '+app_name+' *HH* --end-hy-name HH *HV*'
              ' --end-hy-name HV *VV* --end-hy-name VV --barplot\n'
              'Plot (Y):\n'
              '    '+app_name+' <INPUT> --plot\n'
              'Plot (X and Y):\n'
              '    '+app_name+' <INPUT1> <INPUT2> --plot\n'
              'Trendplot (X and Y):\n'
              '    '+app_name+' <INPUT1> <INPUT2> --trendplot\n'
              'Trendplot (gray scale):\n'
              '    '+app_name+' <INPUT1> <INPUT2> --bw --trendplot\n'
              'Trendplot with color from last input:\n'
              '    '+app_name+' <INPUT1> <INPUT2> <INPUT_3> '
              ' --color-mode 1 --trendplot\n'
              'Scatterplot:\n'
              '    '+app_name+' <INPUT1> <INPUT2> --scatterplot\n'
              '3D scatterplot:\n'
              '    '+app_name+' <INPUT1> <INPUT2> <INPUT_3> --3d'
              ' --scatterplot\n'
              'Other examples:\n'
              '    '+app_name+' <INPUT> --min 0 --max --hist1 --nbins 50\n'
              '    '+app_name+' <INPUT> --absolute --hist\n'
              # '      '+app_name+' --hist --col 0:0:50:50,2429:2372:2360 '
              # '--row 0:50:50:0,1470:1506:1542 '
              # '--out-mask out_byte_mask.bin\n'
              '    '+app_name+' --hist <INPUT> --phase '
              '--color "teal" --mode --title "TITLE" --labelx '
              '"LABELX" --labely "LABELY"\n'
              '    '+app_name+' --hist --in-mask mask_byte.bin <INPUT1> '
              '<INPUT2> -o <OUTPUT> --title "Title"\n'
              '    '+app_name+' --hist2d <INPUT1> -o '
              '<OUTPUT> --absolute <INPUT2> --drawline 1\n'
              '    '+app_name+' --in-mask <MASK> <INPUT1> '
              '--phase <INPUT2> --hist2d ')

    # 'OPTION note(s):\n'
    # '\n')
    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            input_images=1,
                            default_input_options=1,
                            default_flags=1,
                            flag_use_ctable=1,
                            in_null=1,
                            null=1,
                            # db10=1,
                            # db20=1,
                            # inv_db10=1,
                            # inv_db20=1,
                            default_output_options=1)

    # modes
    group = parser.add_mutually_exclusive_group(required=False)

    group.add_argument('--hist',
                       '--histogram',
                       dest='hist',
                       action='store_true',
                       help='Histogram plot')

    group.add_argument('--hist-2d',
                       '--histogram-2d',
                       '--hist2d',
                       '--histogram2d',
                       '--density',
                       '--density-plot',
                       action='store_true',
                       help='Histogram 2D '
                       '(two inputs)',
                       dest='hist2d')

    group.add_argument('--scatterplot', '--scatter', '--scatter-plot',
                       action='store_true',
                       help='Scatter-plot (first input is X-axis)',
                       dest='scatterplot')
    group.add_argument('--plot',
                       dest='generic_plot',
                       action='store_true',
                       help='Plot between images')
    group.add_argument('--trendplot', '--trend', '--trend-plot',
                       dest='trendplot',
                       action='store_true',
                       help='Trend plot')
    group.add_argument('--profilex', '--profile-x',
                       '--xprofile', '--x-profile',
                       dest='profilex',
                       action='store_true',
                       help='X-dimension averaged profile (for a single'
                       ' line profile, select the line using -hrow)')

    group.add_argument('--profiley', '--profile-y',
                       '--yprofile', '--y-profile',
                       dest='profiley',
                       action='store_true',
                       help='Y-dimension averaged profile (for a single'
                       ' column profile, select the column using -col)')

    group.add_argument('--profiley-horizontal',
                       '--profile-y-horizontal',
                       '--yprofile-horizontal',
                       '--y-profile-horizontal',
                       '--profiley-h',
                       '--profileyh',
                       '--profile-y-h',
                       '--yprofile-h',
                       '--y-profile-h',
                       dest='profiley_horizontal',
                       action='store_true',
                       help='Y-dimension averaged profile (for a single'
                       ' column profile, select the column using -col)')

    group.add_argument('--print',
                       dest='print_text',
                       action='store_true',
                       help='Print data values')

    group.add_argument('--multiplot',
                       '--multi-plot',
                       dest='multiplot',
                       action='store_true',
                       help='Time-series/multi-data plot')

    group.add_argument('--barplot',
                       '--bar-plot',
                       dest='barplot',
                       action='store_true',
                       help='Time-series/multi-data barplot'
                       ' (multiplot with bar style)')

    group.add_argument('--table',
                       dest='table',
                       action='store_true',
                       help='Show table')

    group.add_argument('--imshow',
                       # '--im',
                       # '-im',
                       # '--image',
                       dest='im',
                       action='store_true',
                       help='Show image using matplotlib')

    group.add_argument('--mdx',
                       dest='mdx',
                       action='store_true',
                       help='Show image using mdx')

    group.add_argument('--im-geo',
                       '-im-geo',
                       '--geo-im'
                       '--image-geo',
                       '--geo-image',
                       dest='im_geo',
                       action='store_true',
                       help='Show geo-referenced image using matplotlib')

    '''
    group.add_argument('--tk', '-tk', dest='tk',
                       action='store_true',
                       help='Show data using tkinter',
                       default=False)
    '''
    group.add_argument('--geolocation', '--geo-location',
                       '--geo',
                       dest='geolocation',
                       action='store_true',
                       help='Plot geo-location of the input datasets')

    # outputs
    parser.add_argument('-o', '--output',
                        dest='output_file',
                        type=str,
                        required=False,
                        help='Save plxot/image as JPEG, PDF formats '
                        '(defined by files extension)',
                        default='')

    parser.add_argument('--out-text',
                        dest='out_text',
                        type=str,
                        help='Save output in a text file')

    parser.add_argument('--crop-output',
                        dest='crop_output',
                        action='store_true',
                        help='Crop output plot removing white spaces')

    # data options
    parser.add_argument('--fig',
                        '--fig-number',
                        '--number-fig',
                        dest='figure_number',
                        type=int,
                        help='Figure number')

    parser.add_argument('--ct', '--complex-type',
                        dest='complex_type',
                        type=str,
                        help='Display type (for complex data). Options:'
                        'real, imag, mag, phase, or mag_square '
                        '(default: "%(default)s")')

    # parser.add_argument('--filter-size', '--filtersize',
    #                    dest='filter_size', type=float,
    #                    help='Boxcar filter size',
    #                    default=1)
    parser.add_argument('--min',
                        dest='default_min',
                        type=str,
                        help='Minimum data value (single values applies to '
                        'all inputs or multiple values '
                        'separated by commas)')

    parser.add_argument('--max',
                        dest='default_max',
                        type=str,
                        help='Maximum data value (single values applies to '
                        'all inputs or multiple values '
                        'separated by commas)')

    parser.add_argument('--nbins',
                        dest='default_nbins',
                        type=str,
                        help='Number of bins to discretize data values '
                        '(single values applies to '
                        'all inputs or multiple values '
                        'separated by commas)')

    parser.add_argument('-c', '--color',
                        type=str,
                        dest='color',
                        help='Color')

    parser.add_argument('--pol-color',
                        '--pol-colors',
                        '--color-pol',
                        '--colors-pol',
                        '--rgb-color',
                        '--rgb-colors',
                        '--color-rgb',
                        '--colors-rgb',
                        dest='flag_pol_color',
                        default=None,
                        help='Select colors using usual RGB/polarimetric'
                        ' color order',
                        action='store_true')

    parser.add_argument('--color-stats',
                        '--stats-color',
                        type=str,
                        dest='color_stats',
                        help='Stats color')

    parser.add_argument('--color-text-stats',
                        '--stats-color-text',
                        '--fontcolor-stats',
                        '--stats-fontcolor',
                        type=str,
                        dest='color_stats_text',
                        help='Stats text color')

    parser.add_argument('--stats-linestyle',
                        '--fit-linestyle',
                        '--linestyle-stats',
                        '--linestyle-fit',
                        dest='linestyle_stats',
                        type=str,
                        help='Defines stats and fit '
                        ' linestyle following pyplot standards. Options: '
                        'solid, dashed, dashdot and dotted')
    parser.add_argument('--stats-linecolor',
                        '--fit-linecolor',
                        '--linecolor-stats',
                        '--linecolor-fit',
                        dest='linecolor_stats',
                        type=str,
                        help='Defines stats and fit '
                        ' linecolor following pyplot standards. Options: '
                        'solid, dashed, dashdot and dotted')
    parser.add_argument('--linestyle',
                        dest='linestyle',
                        type=str,
                        help='Defines'
                        ' linestyle following pyplot standards. Options: '
                        'solid, dashed, dashdot and dotted')
    parser.add_argument('--edgecolor',
                        '--edge-color',
                        dest='edgecolor',
                        type=str,
                        help='Defines plot edge color')
    parser.add_argument('--hatch',
                        # '--edge-color',
                        dest='hatch',
                        type=str,
                        help='Defines plot hatch')
    parser.add_argument('--linecolor',
                        dest='linecolor',
                        type=str,
                        help='Defines plot line color')
    parser.add_argument('--linewidth',
                        dest='linewidth',
                        type=int,
                        help='Defines plot linewidth',
                        default=2)
    parser.add_argument('--stats-linewidth',
                        '--linewidth-stats',
                        '--fit-linewidth',
                        '--linewidth-fit',
                        dest='linewidth_stats',
                        default=LINEWIDTH_STATS_DEFAULT,
                        type=int,
                        help='Defines stats/fits linewidth')
    parser.add_argument('--markersize', '--marker-size',
                        dest='markersize', type=float,
                        help='Defines plot line size',
                        default=None)
    parser.add_argument('--marker',
                        dest='marker', type=str,
                        help='Defines plot marker')
    parser.add_argument('--alpha', dest='alpha', type=float,
                        help='Defines plot alpha')

    # plot options
    parser.add_argument('--max-name-length',
                        '--max-name-size',
                        dest='max_name_size',
                        type=int,
                        help='Maximum name/label length '
                        '(default: "%(default)s")',
                        default=None)

    parser.add_argument('--invert-axis',
                        '--inv-axis',
                        '--switch-axis',
                        dest='inv_axis',
                        default=None,
                        help='Switch plot axis (x- and y-axis).',
                        action='store_true')

    change_marker_group = parser.add_mutually_exclusive_group(
        required=False)
    change_marker_group.add_argument('--change-marker',
                                     dest='change_marker',
                                     help='Change marker style.',
                                     action='store_true')
    change_marker_group.add_argument('--no-change-marker',
                                     '--not-change-marker',
                                     dest='change_marker',
                                     default=True,
                                     help='Prevent rotating marker style.',
                                     action='store_false')

    change_linestyle_group = parser.add_mutually_exclusive_group(
        required=False)
    change_linestyle_group.add_argument('--change-linestyle',
                                        dest='change_linestyle',
                                        help='Change linestyle style.',
                                        action='store_true')
    change_linestyle_group.add_argument('--no-change-linestyle',
                                        '--not-change-linestyle',
                                        dest='change_linestyle',
                                        help='Prevent rotating linestyle'
                                        ' style.',
                                        action='store_false')

    parser.add_argument('--legend-location',
                        '--legend-position',
                        '--legend-pos',
                        dest='legend_location',
                        help='Defines legend location. Options: '
                        "'upper right', 'upper left', 'lower left', "
                        "'lower right', 'right', 'center left', "
                        "'center right', 'lower center’, 'upper center', "
                        "'center'",
                        type=str)

    parser.add_argument('--legend-bbox',
                        dest='legend_bbox',
                        help='Defines matplotlib bbox to ancher',
                        nargs=2,
                        type=float)

    parser.add_argument('--legend-fontsize',
                        dest='legend_fontsize',
                        type=float,
                        default=None,
                        help='Legend font size')

    parser.add_argument('--dpi',
                        dest='dpi',
                        type=int,
                        help='The resolution in dots per inch')

    '''
    parser.add_argument('--bg', '--bg-color',
                        '--background-color',
                        type=str,
                        # default='white',
                        dest='background_color',
                        help='Plot background color.')
    '''

    parser.add_argument('--facecolor',
                        type=str,
                        # default='white',
                        dest='facecolor',
                        help='Set facecolor color.')

    parser.add_argument('--x-min'
                        '--xmin',
                        '--set-x-min'
                        '--set-xmin',
                        '--profile-start',
                        '--profile-begin',
                        '--min-abscissa',
                        '--abscissa-min',
                        type=float,
                        # default=None,
                        dest='xmin',
                        help='Set the minimum limit of X axes.')

    parser.add_argument('--x-max'
                        '--xmax',
                        '--set-x-max'
                        '--set-xmax',
                        '--profile-end',
                        '--max-abscissa',
                        '--abscissa-max',
                        type=float,
                        # default=None,
                        dest='xmax',
                        help='Set the maximum limit of X axes.')

    parser.add_argument('--y-min'
                        '--ymin',
                        '--set-y-min'
                        '--set-ymin',
                        type=float,
                        # default=None,
                        dest='ymin',
                        help='Set the minimum limit of Y axes.')

    parser.add_argument('--y-max'
                        '--ymax',
                        '--set-y-max'
                        '--set-ymax',
                        type=float,
                        # default=None,
                        dest='ymax',
                        help='Set the maximum limit of Y axes.')

    parser.add_argument('--plot-width'
                        '--size-x',
                        '--sizex'
                        '--xsize',
                        '--plot-size-x',
                        type=int,
                        dest='plot_size_x',
                        help='Plot width')

    parser.add_argument('--plot-height'
                        '--size-y',
                        '--sizey'
                        '--ysize',
                        '--plot-size-y',
                        type=int,
                        dest='plot_size_y',
                        help='Plot height')

    parser.add_argument('--style',
                        type=str,
                        dest='plot_style',
                        help='Plot style: default, ggplot, grayscale, etc..'
                        ' (default: "%(default)s")')
    parser.add_argument('--stddev-scale', dest='stddev_scale',
                        type=float,
                        help='Scale the standard deviation plot by a '
                        'factor')

    parser.add_argument('--fontsize',
                        '--font-size',
                        dest='fontsize',
                        type=float,
                        help='Plot font size')

    parser.add_argument('--fontcolor',
                        '--font-color',
                        dest='fontcolor',
                        type=str,
                        help='Plot font color')

    parser.add_argument('--fontweight',
                        '--font-weight',
                        dest='fontweight',
                        type=str,
                        # action='store_true',
                        help="Set the font weight. May be either a"
                        " numeric value in the range 0-1000 or one of"
                        " 'ultralight', 'light', 'normal', 'regular',"
                        " 'book', 'medium', 'roman', 'semibold',"
                        " 'demibold', 'demi', 'bold', 'heavy',"
                        " 'extra bold', 'black'")

    parser.add_argument('--fontcolor-axis',
                        '--font-color-axis',
                        dest='fontcolor_axis',
                        type=str,
                        help='Plot font color (axis)')

    parser.add_argument('--fontfile',
                        '--font-file',
                        # '--fontname',
                        # '--font-name',
                        dest='fontfile',
                        type=str,
                        help='Plot font file (.ttf, .afm, etc.)')

    parser.add_argument('--fontfamily',
                        '--font-family',
                        '--fontname',
                        '--font-name',
                        dest='fontfamily',
                        type=str,
                        help='Plot fontfamily. Options:'
                        ' serif, sans-serif, cursive, fantasy'
                        ' and monospace')

    '''
    parser.add_argument('--fontname',
                        '--font-name',
                        dest='fontname',
                        type=str,
                        help='Plot fontname. Options:'
                        ' serif, sans-serif, cursive, fantasy'
                        ' and monospace')
    '''

    parser.add_argument('--name',
                        '--label',
                        '--legend-label',
                        dest='name',
                        type=str,
                        help='Name of the input data')

    parser.add_argument('--labelx',
                        '--xlabel',
                        '--label-x',
                        '--x-label',
                        dest='label_x',
                        type=str,
                        help='X-axis label')

    parser.add_argument('--labely',
                        '--ylabel',
                        '--label-y',
                        '--y-label',
                        dest='label_y',
                        type=str,
                        help='Y-axis label')

    parser.add_argument('--labelz',
                        '--zlabel',
                        '--label-z',
                        '--z-label',
                        dest='label_z',
                        type=str,
                        help='Z-axis label (3D)')

    parser.add_argument('--label-colorbar',
                        '--colorbar-label',
                        dest='colorbar_label',
                        type=str,
                        help='Colorbar label')

    parser.add_argument('--db-colorbar',
                        '--colorbar-db',
                        dest='colorbar_db',
                        action='store_true',
                        help='Colorbar label')

    parser.add_argument('--orientation-colorbar',
                        '--colorbar-orientation',
                        dest='colorbar_orientation',
                        type=str,
                        help='Colorbar orientation')

    parser.add_argument('--title',
                        dest='title',
                        type=str,
                        help='Plot title')

    parser.add_argument('--invert-x-axis',
                        '--invert-xaxis',
                        '--invert-x',
                        '--revert-x-axis',
                        '--revert-xaxis',
                        '--revert-x',
                        dest='invert_x_axis',
                        action='store_true',
                        help='Invert X-axis')

    parser.add_argument('--invert-y-axis',
                        '--invert-yaxis',
                        '--invert-y',
                        '--revert-y-axis',
                        '--revert-yaxis',
                        '--revert-y',
                        dest='invert_y_axis',
                        action='store_true',
                        help='Invert Y-axis')

    # ancillary curves and function fits
    parser.add_argument('--text',
                        '--draw-text',
                        '--annotate',
                        dest='draw_text',
                        # nargs=3,
                        type=str,
                        help='Insert text (first element) '
                        ' at position Y, X (second'
                        ' and third elements)')

    parser.add_argument('--stats-fontsize',
                        '--fontsize-stats',
                        dest='fontsize_stats',
                        type=float,
                        default=None,
                        help='Legend font size')

    parser.add_argument('--hline',
                        '--draw-hline',
                        dest='draw_hline',
                        type=str,
                        help='Plot a horizontal line with a sequence'
                        ' of Y positions')

    parser.add_argument('--vline',
                        '--draw-vline',
                        dest='draw_vline',
                        type=str,
                        help='Plot a vertical line with a sequence'
                        ' of Y positions')

    parser.add_argument('--draw-lines',
                        '--draw-lines-plot',
                        dest='draw_line_with_points',
                        type=str,
                        help='Plot lines with a sequence'
                        ' of Y followed by X positions')

    parser.add_argument('--point',
                        '--points',
                        '--draw-points',
                        '--draw-point',
                        dest='draw_point',
                        # nargs=3,
                        type=str,
                        help='Insert point'
                        ' at position Y, X (first'
                        ' and second elements) with'
                        ' optional label (third element)')

    parser.add_argument('--hspan',
                        '--draw-hspan',
                        dest='draw_hspan',
                        type=str,
                        help='Plot a horizontal span defined by two'
                        ' Y positions')

    parser.add_argument('--vspan',
                        '--draw-vspan',
                        dest='draw_vspan',
                        type=str,
                        help='Plot a vertical span defined by two'
                        ' X positions')

    parser.add_argument('--rect',
                        '--draw-rect',
                        '--rectangle',
                        '--draw-rectangle',
                        dest='draw_rectangle',
                        # nargs=2,
                        type=str,
                        help='Plot a rectangle defined by two'
                        ' X positions')

    parser.add_argument('--draw-line', '--drawline',
                        dest='draw_line',
                        type=str,
                        help='Plot an ancillary line (ax+b.'
                        ' "a" and "b" '
                        'separated by ":" (ex: -drawline 1:0.1 for a=1 and '
                        'b=0.1. ')

    parser.add_argument('--draw-curve', '--drawcurve',
                        '--draw-function', '--drawfunction',
                        dest='draw_function',
                        type=str,
                        help='Plot an ancillary curve function. '
                        'Ex.: x*sin(x), x**2')

    parser.add_argument('--line-fit',
                        '--linefit',
                        '--robust-line-fit',
                        '--robust-linefit',
                        dest='linefit',
                        action='store_true',
                        help='Performs a line fite (robust algorithm)')

    parser.add_argument('--fit', '--polyfit', '--poly-fit',
                        dest='polyfit', type=int,
                        help='Set the number of degrees for polynomial fit')

    parser.add_argument('--f-fit',
                        '--function-fit',
                        '--curve-fit',
                        '--curvefit',
                        '--ffit',
                        dest='function_fit',
                        type=str,
                        help='Function fit. Options: '
                        'wcm, wcm_db, wcm_h, wcm_db_h, '
                        'wcm_vgratio, wcm_vgratio_db, '
                        'wcm_agb_vgratio, wcm_agb_vgratio_db, '
                        'wcm_gvratio, wcm_gvratio_db, '
                        'wcm_agb_gvratio, wcm_agb_gvratio_db, '
                        'agb_h, h_agb, dba_1, dba_1_db, '
                        'dba_hv and dba_hv_db.')

    parser.add_argument('--out-f-fit',
                        '--out-function-fit',
                        '--out-curve-fit',
                        '--out-curvefit',
                        '--out-ffit',
                        dest='out_function_fit',
                        type=str,
                        help='Save coefficients of function fit.')

    parser.add_argument('--f-saturation', '--ffit-saturation',
                        dest='function_fit_saturation',
                        type=float,
                        help='Function-fit saturation threshold'
                        '(default: "%(default)s")')

    parser.add_argument('--decimal',
                        '--decimal-places',
                        dest='decimal_places',
                        type=int,
                        help='Defines the number of decimal places')

    parser.add_argument('--nsig-figs',
                        '--n-significant-figures',
                        '--sig-figs',
                        '--significant-figures',
                        dest='n_significant_figures',
                        type=int,
                        help='Defines the number of significant figures')

    # flags
    parser.add_argument('--bw',
                        '--black-and-white',
                        '--gray',
                        '--grey',
                        dest='gray_scale',
                        action='store_true',
                        help='Use only gray scale')

    hatch_group = parser.add_mutually_exclusive_group(required=False)
    hatch_group.add_argument('--hatch-on',
                             '--show-hatch',
                             dest='flag_hatch',
                             action='store_true',
                             default=None,
                             help='Use plot hatch')

    hatch_group.add_argument('--no-hatch',
                             '--hatch_off',
                             '--no-show-hatch',
                             dest='flag_hatch',
                             default=None,
                             action='store_false',
                             help='Prevent using plot hatch')

    grid_group = parser.add_mutually_exclusive_group(required=False)
    grid_group.add_argument('--grid', dest='grid',
                            action='store_true',
                            default=None,
                            help='Show grid')

    grid_group.add_argument('--no-grid', dest='grid',
                            default=None,
                            action='store_false',
                            help='Hide grid')

    theme_group = parser.add_mutually_exclusive_group(required=False)
    theme_group.add_argument('--dark', dest='dark_theme',
                             action='store_true',
                             default=None,
                             help='Show grid')

    theme_group.add_argument('--light',
                             dest='dark',
                             default=None,
                             action='store_false',
                             help='Use light theme')

    colorbar_group = parser.add_mutually_exclusive_group(required=False)
    colorbar_group.add_argument('--colorbar', dest='colorbar',
                                action='store_true',
                                default=None,
                                help='Show colorbar')

    colorbar_group.add_argument('--no-colorbar', dest='colorbar',
                                action='store_false',
                                default=None,
                                help='Hide colorbar')

    colorbar_in_line_group = parser.add_mutually_exclusive_group(
        required=False)
    colorbar_in_line_group.add_argument('--colorbar-inline',
                                        '--colorbar-in-line',
                                        dest='flag_colorbar_in_line',
                                        default=None,
                                        help='Aggregate colorbar in a single'
                                        ' line/column',
                                        action='store_true')
    colorbar_in_line_group.add_argument('--no-colorbar-inline',
                                        '--no-colorbar-in-line',
                                        dest='flag_colorbar_in_line',
                                        help='Prevent aggregating colorbar'
                                        'in a single line/column',
                                        action='store_false')

    parser.add_argument('--labelx-vertical',
                        '--xlabel-vertical',
                        '--label-x-vertical',
                        '--x-label-vertical',
                        dest='xlabel_vertical',
                        action='store_true',
                        help='Rotate x-labels (xticks) from '
                        'horizontal to vertical')

    parser.add_argument('--labelx-rotation',
                        '--xlabel-rotation',
                        '--label-x-rotation',
                        '--x-label-rotation',
                        dest='xlabel_rotation',
                        type=str,
                        help='Indicate x-labels (xticks) rotation'
                        ' in degrees')

    parser.add_argument('--hide-annotations', '--hide-text',
                        dest='show_annotations',
                        action='store_false',
                        default=True,
                        help='Hide plot statistics (mean, median, '
                        'etc.) annotations')

    legend_group = parser.add_mutually_exclusive_group(required=False)
    legend_group.add_argument('--legend',
                              dest='legend',
                              default=None,
                              action='store_true',
                              help='Show legend')

    legend_group.add_argument('--no-legend',
                              dest='legend',
                              default=None,
                              action='store_false',
                              help='Hide legend')

    # flags (for each input)
    parser.add_argument('--mean', dest='mean',
                        action='store_true',
                        help='Calculate and plot data mean')
    parser.add_argument('--median', dest='median',
                        action='store_true',
                        help='Calculate and plot data median')
    parser.add_argument('--mode', dest='mode',
                        action='store_true',
                        help='Calculate and plot data mode')
    parser.add_argument('--stddev', dest='stddev',
                        action='store_true',
                        help='Calculate and plot data standard deviation')

    parser.add_argument('--normed',
                        '--normalized',
                        '--normalize',
                        dest='sum_normed',
                        action='store_true',
                        help='Normalize data sum (hist,'
                        'hist2d, profile or trendplot colormode 5)')

    parser.add_argument('--max-normed',
                        '--normalize-max',
                        dest='max_normed',
                        action='store_true',
                        help='Normalize data max '
                        '(profile or trendplot colormode 5)')

    parser.add_argument('--normed-to',
                        '--normalized-to',
                        '--normalize-to',
                        dest='sum_normed_to_value',
                        # action='store_true',
                        type=float,
                        help='Normalize data sum to the'
                        ' selected value'
                        ' (profile or trendplot colormode 5)')

    parser.add_argument('--max-normed-to',
                        '--normalize-max-to',
                        dest='max_normed_to_value',
                        # action='store_true',
                        type=float,
                        help='Normalize data max to the selected value'
                        '(profile or trendplot colormode 5)')

    parser.add_argument('--log', dest='hist_log',
                        action='store_true',
                        help='Logarithm scale (hist, hist2d)')

    parser.add_argument('--tex', dest='tex',
                        action='store_true',
                        help='Use TeX')

    parser.add_argument('--no-show', dest='no_show',
                        action='store_true',
                        help='Do not show plot/image dialog')

    parser.add_argument('--no-title', dest='no_title',
                        action='store_true',
                        help='Do not show plot/image title')

    group_separator = plant.PARSER_GROUP_SEPARATOR

    # generic plot
    generic_plot = parser.add_argument_group(group_separator +
                                             'plot (--plot), '
                                             'scatterplot (--scatterplot), '
                                             'trendplot (--trendplot)'
                                             ' arguments')
    generic_plot.add_argument('--first-input-as-x',
                              '--first-image-as-x',
                              action='store_true',
                              dest='first_image_as_x')
    generic_plot.add_argument('--np', dest='trendplot_np', type=int,
                              help='Number of plot points per bin',
                              default=10)
    generic_plot.add_argument('--n', '-n', dest='trendplot_n', type=int,
                              help='Number of pixels for each plot point',
                              default=100)
    generic_plot.add_argument('--min-n', dest='trendplot_min_n', type=int,
                              help='Number of pixels for each plot point',
                              default=1)
    '''
    generic_plot.add_argument('--points-size',
                              '--point-size',
                              '--pointssize',
                              '--pointsize',
                              type=float,
                              dest='points_size',
                              help='Size of point plots')
    '''
    generic_plot.add_argument('--color-mode', '--colormode',
                              dest='plot_color_mode',
                              help='Trend-plot color mode. Options: '
                              ' 0: a single color for each plot;'
                              ' 1: use last image;'
                              ' 2: use x-axis;'
                              ' 3: use y-axis;'
                              ' 4: use z-axis (3D mode);'
                              ' 5: use number of averaged points.',
                              type=int,
                              default=0)

    generic_plot.add_argument('--all-points', '--all',
                              dest='generic_plot_all',
                              action='store_true',
                              help='Prevent selecting a smaller '
                              'subset in case of too many points')

    generic_plot.add_argument('--periodic',
                              dest='flag_periodic_trendplot',
                              action='store_true',
                              help='Periodic trendplot')

    generic_plot.add_argument('--3d',
                              dest='generic_plot_3d',
                              action='store_true',
                              help='Plot in 3D (for --plot/'
                              '--scatterplot/--trendplot)')

    # histogram
    parser_hist = parser.add_argument_group(group_separator +
                                            'histogram (--hist) '
                                            'arguments')

    parser_hist.add_argument('--histtype', dest='histtype',
                             type=str,
                             help='Type of histogram plot')

    # hist2d
    parser_hist2d = parser.add_argument_group(group_separator +
                                              'histogram 2D (--hist2d)'
                                              ' arguments')

    parser_hist2d.add_argument('--normalize-x-max',
                               '--x-normalize-max',
                               action='store_true',
                               dest='hist2d_normalize_x_max',
                               help='X-axis max normalization '
                               '(divide each line by line max value)')

    parser_hist2d.add_argument('--normalize-y-max',
                               '--y-normalize-max',
                               action='store_true',
                               dest='hist2d_normalize_y_max',
                               help='Y-axis max normalization '
                               '(divide each line by line max value)')

    parser_hist2d.add_argument('--cmin',
                               dest='cmin',
                               type=float,
                               help='Histogram 2D minimum count')

    parser_hist2d.add_argument('--cmax',
                               dest='cmax',
                               type=float,
                               help='Histogram 2D maximum count')

    parser_hist2d.add_argument('--c-percentile',
                               dest='cpercentile',
                               type=float,
                               help='Percentile to determine'
                               ' histogram 2D minimum'
                               ' and maximum count')

    # profiles
    parser_profiles = parser.add_argument_group(group_separator +
                                                'profile-x/-y (--profilex/'
                                                '--profiley) arguments')

    '''
    parser_profiles.add_argument('--profile-start',
                                 '--profile-begin',
                                 '--min-abscissa',
                                 '--abscissa-min',
                                 dest='min_abscissa_point',
                                 type=int,
                                 help='Profile abscissa start')
    parser_profiles.add_argument('--profile-end',
                                 '--max-abscissa',
                                 '--abscissa-max',
                                 dest='xmax',
                                 type=int,
                                 help='Profile abscissa end')
    '''

    parser_profiles.add_argument('--profile-abscissa',
                                 '--abscissa-profile',
                                 dest='profile_abscissa',
                                 type=str,
                                 help='File or vector containing abscissa '
                                 'values. Vector length should agree with '
                                 'profile length.')
    parser_profiles.add_argument('--profile-coregister',
                                 '--prof-coregister',
                                 '--coregister-profile',
                                 '--coregister-prof',
                                 dest='flag_coregister_profiles',
                                 action='store_true',
                                 help='Co-register profiles to the first'
                                 ' profile')
    parser_profiles.add_argument('--semilogy',
                                 dest='profiles_semilogy',
                                 action='store_true',
                                 help='Plot Y-axis in log scale')
    parser_profiles.add_argument('--label-db',
                                 dest='profiles_db',
                                 action='store_true',
                                 help='Plot Y-axis in dB scale')
    parser_profiles.add_argument('--use-func',
                                 '--use-function',
                                 dest='profiles_use_func',
                                 help='Use function to calculate the profile')

    # multiplot
    parser_mp = parser.add_argument_group(group_separator +
                                          'multiplot (--multiplot)'
                                          ' arguments')

    parser_mp.add_argument('--hy',
                           '--mp-y',
                           '--mp-line',
                           '--multiplot-y',
                           '--multiplot-line',
                           type=str,
                           # nargs=nargs,
                           dest='multiplot_line',
                           help='Multiplot plot number/name '
                           '(e.g. polarization)')

    parser_mp.add_argument('--end-hy-name',
                           '--end-mp-y-name',
                           '--end-mp-line-name',
                           '--end-multiplot-y-name',
                           '--end-multiplot-line-name',
                           type=str,
                           # nargs=nargs,
                           dest='multiplot_line_end_name',
                           help='Indicates the end of a multiplot line'
                           ' also defining the number/name of the'
                           ' ended line (e.g. polarization)')

    parser_mp.add_argument('--end-hy',
                           '--end-mp-y',
                           '--end-mp-line',
                           '--end-multiplot-y',
                           '--end-multiplot-line',
                           action='store_true',
                           # nargs=nargs,
                           dest='multiplot_line_end',
                           help='Indicates the end of a multiplot line')

    parser_mp.add_argument('--hx',
                           '--column',
                           '--mp-x',
                           '--mp-column',
                           '--multiplot-x',
                           '--multiplot-column',
                           type=str,
                           # nargs=nargs,
                           dest='multiplot_column',
                           help='Multiplot X (absciss) position number/name '
                           '(e.g. temporal position)')

    parser_mp.add_argument('--end-hx-name',
                           '--end-mp-x-name',
                           '--end-mp-column-name',
                           '--end-multiplot-x-name',
                           '--end-multiplot-column-name',
                           type=str,
                           # nargs=nargs,
                           dest='multiplot_column_end_name',
                           help='Indicates the end of a multiplot column'
                           ' also defining the number/name of the'
                           ' ended column (only for option: --line-order)')

    parser_mp.add_argument('--end-hx',
                           '--end-mp-x',
                           '--end-mp-column',
                           '--end-multiplot-x',
                           '--end-multiplot-column',
                           action='store_true',
                           # nargs=nargs,
                           dest='multiplot_column_end',
                           help='Indicates the end of a multiplot column')

    parser_mp.add_argument('--multiplot-use-filenames',
                           '--mp-use-filenames',
                           '--use-filenames',
                           action='store_true',
                           # nargs=nargs,
                           dest='flag_multiplot_use_name',
                           help='Use filenames instead of file indexes in'
                           ' multiplot')

    parser_mp.add_argument('--line-order',
                           '--inputs-ordered-by-line',
                           dest='multiplot_line_order',
                           action='store_true',
                           help='Multiplot: Inputs are ordered by '
                           'lines (instead of columns)')

    parser_mp.add_argument('--multiplot-post-function',
                           '--mp-post-function',
                           dest='multiplot_post_function',
                           type=str,
                           help='Multiplot function to be applied after'
                           ' multiplot averaging. Options: inv_db10,'
                           'inv_db20, negative, inverse, angle, square,'
                           'square_root, db10, db20, abs')

    '''
    parser_mp.add_argument('--multiplottype',
                           '--multiplot-type',
                           '--multiplotstyle',
                           '--multiplot-style',
                           dest='multiplot_type', type=str,
                           help='Type of multiplot. Options: line, bar.')
    '''

    parser_mp.add_argument('--nlines',
                           dest='multiplot_nlines',
                           type=int,
                           help='Multiplot maximum number of lines')

    parser_mp.add_argument('--ncolumns',
                           dest='multiplot_ncolumns',
                           type=int,
                           help='Multiplot maximum number of columns')

    parser.add_argument('--barplot-width',
                        dest='barplot_width',
                        type=float,
                        # action='store_true',
                        help='Set barplot width')

    parser.add_argument('--barplot-show-values',
                        dest='barplot_show_values',
                        action='store_true',
                        help='Show barplot values')

    parser.add_argument('--barplot-rmse',
                        dest='barplot_rmse',
                        action='store_true',
                        help='Show barplot RMSE')
    # parser_mp.add_argument('--hide-values', dest='multiplot_hide_values',
    #                       action='store_true',
    #                       help='Multiplot: Hide value labels',
    #                       required=False)

    # geo
    parser_geo = parser.add_argument_group(group_separator +
                                           'geo (--geo) '
                                           'arguments')
    parser_geo.add_argument('--label-position',
                            '--label-pos',
                            '--name-position',
                            '--name-pos',
                            dest='name_position',
                            help='Position for label/name. Options: '
                            "'upper right', 'upper left', 'lower left', "
                            "'lower right', 'right', 'center left', "
                            "'center right', 'lower center’, 'upper center', "
                            "'center'",
                            type=str,
                            default='center')

    # imageviewer
    parser_imageviewer = parser.add_argument_group(group_separator +
                                                   'image viewer'
                                                   ' (--imshow)'
                                                   ' arguments')

    parser_imageviewer.add_argument('--hillshade',
                                    '--hill-shade',
                                    '--shaderelief',
                                    '--shade-relief',
                                    '--terrainrelief',
                                    '--terrain-relief',
                                    # type=float,
                                    dest='hillshade',
                                    help='Convert a DEM into a shaded relief '
                                    'a tree band output using a color map',
                                    # required=False,
                                    action='store_true',
                                    default=False)

    separate_group = parser_imageviewer.add_mutually_exclusive_group(
        required=False)
    separate_group.add_argument('--separate',
                                dest='imshow_separate',
                                action='store_true',
                                default=None,
                                help='Plot images separately')

    separate_group.add_argument('--together',
                                '--not-separate',
                                '--no-separate',
                                dest='imshow_separate',
                                default=None,
                                action='store_false',
                                help='Plot images together (in a single plot)')


    geo_axis_group = parser_imageviewer.add_mutually_exclusive_group(
        required=False)
    geo_axis_group.add_argument('--geo-axis',
                                '--geo-coordinates',
                                dest='geo_axis',
                                action='store_true',
                                default=None,
                                help='Plot geo-coordinates')

    geo_axis_group.add_argument('--not-geo-axis',
                                '--no-geo-axis',
                                '--not-geo-coordinates',
                                '--no-geo-coordinates',
                                dest='geo_axis',
                                default=None,
                                action='store_false',
                                help='Prevent plotting geo-coordinates')
    # geo_axis_factor = 1e-3

    geo_axis_group.add_argument('--geo-axis-factor',
                                dest='geo_axis_factor',
                                type=float,
                                help='Prevent plotting geo-coordinates')


    # portrait_group = parser.add_mutually_exclusive_group(required=False)
    parser_imageviewer.add_argument('--collage-col',
                                    '--collage-column',
                                    '--portrait',
                                    dest='flag_collage_col',
                                    action='store_true',
                                    help='Show image in portrait mode')

    parser_imageviewer.add_argument('--collage-row',
                                    '--collage-line',
                                    '--landscape',
                                    dest='flag_collage_row',
                                    action='store_true',
                                    help='Show image in landscape mode')

    parser_imageviewer.add_argument('--extent',
                                    type=float,
                                    nargs=4,
                                    dest='extent',
                                    help='The location, in data-coordinates,'
                                    ' of the lower-left and upper-right'
                                    ' corners: left, right, bottom, top')

    parser_imageviewer.add_argument('--top',
                                    type=str,
                                    dest='tick_params_top',
                                    help='Matplotlib tick_params top.'
                                    'Options: True, False')

    parser_imageviewer.add_argument('--label-top',
                                    '--labeltop',
                                    type=str,
                                    dest='tick_params_labeltop',
                                    help='Matplotlib tick_params labeltop.'
                                    'Options: True, False')

    parser_imageviewer.add_argument('--bottom',
                                    type=str,
                                    dest='tick_params_bottom',
                                    help='Matplotlib tick_params bottom.'
                                    'Options: True, False')

    parser_imageviewer.add_argument('--label-bottom',
                                    '--labelbottom',
                                    type=str,
                                    dest='tick_params_labelbottom',
                                    help='Matplotlib tick_params labelbottom.'
                                    'Options: True, False')

    parser_imageviewer.add_argument('--left',
                                    type=str,
                                    dest='tick_params_left',
                                    help='Matplotlib tick_params left.'
                                    'Options: True, False')

    parser_imageviewer.add_argument('--labelleft',
                                    '--label-left',
                                    type=str,
                                    dest='tick_params_labelleft',
                                    help='Matplotlib tick_params labelleft.'
                                    'Options: True, False')

    parser_imageviewer.add_argument('--right',
                                    type=str,
                                    dest='tick_params_right',
                                    help='Matplotlib tick_params right.'
                                    'Options: True, False')

    parser_imageviewer.add_argument('--labelright',
                                    '--label-right',
                                    type=str,
                                    dest='tick_params_labelright',
                                    help='Matplotlib tick_params labelright.'
                                    'Options: True, False')

    parser_imageviewer.add_argument('--origin',
                                    type=str,
                                    dest='origin',
                                    help='Matplotlib imshow origin. Options: '
                                    'upper, lower')

    parser_imageviewer.add_argument('--aspect',
                                    type=str,
                                    dest='aspect',
                                    help='Plot aspect. Options: auto, equal,'
                                    ' scalar')

    '''
    parser_imageviewer.add_argument('--zoom', '-z',
                                    dest='zoom',
                                    type=float,
                                    help='Image zoom')
    '''
    parser_imageviewer.add_argument('--interp', '--interpolation',
                                    '--resample-algorithm',
                                    '--resample-alg',
                                    type=str,
                                    dest='interpolation',
                                    help='Interpolation method (e.g.'
                                    ' none, bilinear, bicubic, etc.)')

    parser_imageviewer.add_argument('--animate',
                                    '--animated',
                                    dest='flag_animate',
                                    help='Animate images',
                                    action='store_true')

    parser_tight = parser_imageviewer.add_mutually_exclusive_group()
    parser_tight.add_argument('--tight',
                              '--tight-layout',
                              dest='tight_layout',
                              default=None,
                              help='Animate images',
                              action='store_true')
    parser_tight.add_argument('--no-tight',
                              '--no-tight-layout',
                              dest='tight_layout',
                              default=None,
                              help='Animate images',
                              action='store_false')

    parser_imageviewer.add_argument('--image-bg',
                                    '--image-as-bg',
                                    '--image-as-background',
                                    '--first-image-as-background',
                                    dest='flag_first_image_as_background',
                                    help='Animate images',
                                    action='store_true')

    parser_imageviewer.add_argument('--animation-interval',
                                    dest='animation_interval',
                                    type=float,
                                    help='Animatino interval')

    parser_imageviewer.add_argument('--animation-repeat-delay',
                                    dest='animation_repeat_delay',
                                    type=float,
                                    help='Animatino repeat_delay')

    parser_imrgb = parser_imageviewer.add_mutually_exclusive_group()

    parser_imrgb.add_argument('--im-rgb',
                              '--image-rgb',
                              '-im-rgb',
                              '--rgb',
                              dest='im_rgb',
                              default=None,
                              help='Plot images in RGB colors',
                              action='store_true')

    parser_imrgb.add_argument('--no-im-rgb',
                              '-no-im-rgb',
                              '--no-rgb',
                              dest='im_rgb',
                              default=None,
                              help='Prevent plotting images in RGB colors',
                              action='store_false')
    cartopy_group = parser_imageviewer.add_mutually_exclusive_group(
        required=False)
    cartopy_group.add_argument('--cartopy', dest='flag_cartopy',
                               action='store_true',
                               # default=None,
                               default=False,
                               help='Use cartopy')
    cartopy_group.add_argument('--no-cartopy', dest='flag_cartopy',
                               default=None,
                               action='store_false',
                               help='Prevent using cartopy')
    parser_imageviewer.add_argument('--html',
                                    '--web',
                                    '--webbrowser',
                                    '--browser',
                                    dest='flag_html',
                                    action='store_true',
                                    help='Open image on a webbroser'
                                    ' (using folium)')
    parser_imageviewer.add_argument('--folium',
                                    '--inline-html',
                                    '--in-line-html',
                                    dest='flag_folium',
                                    action='store_true',
                                    help='Use folium')

    draw_ocean_group = parser_imageviewer.add_mutually_exclusive_group(
        required=False)
    draw_ocean_group.add_argument('--ocean',
                                  dest='draw_ocean',
                                  default=None,
                                  help='Cartopy: draw oceans',
                                  action='store_true')
    draw_ocean_group.add_argument('--no-ocean',
                                  dest='draw_ocean',
                                  help='Cartopy: prevent drawing'
                                  ' oceans',
                                  action='store_false')
    draw_land_group = parser_imageviewer.add_mutually_exclusive_group(
        required=False)
    draw_land_group.add_argument('--land',
                                 dest='draw_land',
                                 default=None,
                                 help='Cartopy: draw land',
                                 action='store_true')
    draw_land_group.add_argument('--no-land',
                                 dest='draw_land',
                                 help='Cartopy: prevent drawing land',
                                 action='store_false')
    draw_lakes_group = parser_imageviewer.add_mutually_exclusive_group(
        required=False)
    draw_lakes_group.add_argument('--lakes',
                                  dest='draw_lakes',
                                  default=None,
                                  help='Cartopy: draw lakes',
                                  action='store_true')
    draw_lakes_group.add_argument('--no-lakes',
                                  dest='draw_lakes',
                                  help='Cartopy: prevent drawing lakes',
                                  action='store_false')
    draw_borders_group = parser_imageviewer.add_mutually_exclusive_group(
        required=False)
    draw_borders_group.add_argument('--borders',
                                    dest='draw_borders',
                                    default=None,
                                    help='Cartopy: draw borders',
                                    action='store_true')
    draw_borders_group.add_argument('--no-borders',
                                    dest='draw_borders',
                                    help='Cartopy: prevent drawing borders',
                                    action='store_false')
    draw_coastline_group = parser_imageviewer.add_mutually_exclusive_group(
        required=False)
    draw_coastline_group.add_argument('--coastline',
                                      dest='draw_coastline',
                                      default=None,
                                      help='Cartopy: draw coastlines',
                                      action='store_true')
    draw_coastline_group.add_argument('--no-coastline',
                                      dest='draw_coastline',
                                      help='Cartopy: prevent drawing'
                                      ' coastlines',
                                      action='store_false')
    draw_rivers_group = parser_imageviewer.add_mutually_exclusive_group(
        required=False)
    draw_rivers_group.add_argument('--rivers',
                                   dest='draw_rivers',
                                   help='Cartopy: draw rivers',
                                   action='store_true')
    draw_rivers_group.add_argument('--no-rivers',
                                   dest='draw_rivers',
                                   help='Cartopy: prevent drawing rivers',
                                   action='store_false')
    return parser


class DataDisplay:

    def __init__(self):
        self.name = None
        self.user_defined_name = None
        self.filename = None
        self.image_name = None
        self.band = None
        self.nbands = None
        self.number = None

        self.file_format = None
        self.data = None
        self.ndata = None
        self.geotransform = None
        self.projection = None
        self.width = None
        self.length = None
        self.depth = None
        self.complex_type = None
        self.hatch = None
        self.alpha = None
        self.cmap = None
        self.cmin = None
        self.cmax = None
        self.cpercentile = None
        self.vmax = None
        self.vmin = None
        self.nbins = None
        self.color = None
        self.edgecolor = None
        # self.edgecolor_stats = None
        self.color_stats = None
        self.color_stats_text = None
        self.linecolor = None
        self.linecolor_stats = None
        self.linestyle = None
        self.linestyle_stats = None
        self.linewidth = None
        self.linewidth_stats = None
        self.fontsize_stats = None

        self.markersize = None
        self.marker = None

        self.ctable = None
        # self.line = 0
        self.xmin = None
        self.xmax = None
        self.flag_vmin_set_by_user = None
        self.flag_vmax_set_by_user = None

        self.mean = None
        self.mode = None
        self.median = None
        self.stddev = None

        self.sum_normed = None
        self.max_normed = None
        self.sum_normed_to_value = None
        self.max_normed_to_value = None

        # self.db10=False
        # self.db20=False
        # self.inv_db10=False
        # self.inv_db20=False

        # self.color_mean = None
        # self.color_mode = None
        # self.color_median = None
        # self.color_stddev = None
        # self.color_mean_text = None
        # self.color_mode_text = None
        # self.color_median_text = None
        # self.color_stddev_text = None


class PlantDisplayLib(PlantScript):

    def __init__(self, parser=None, argv=None, mdx_mode=False):
        '''
        class initialization
        '''
        # self.replace_null = False
        # super().__init__(parser, argv)

        # def __init__(self, args=None
        #    '''
        #    variables initialization.
        #    '''
        self.parameters_populated = False
        self.input_number = 0
        self.sampling_step = None
        # self.red_x = None
        # self.red_y = None
        self.vmin = None
        self.vmax = None
        self.multiplot_line_end_previous = False
        self.multiplot_column_end_previous = False
        # self.multiplot_line = None
        # self.multiplot_column = None
        self.plot_list = []
        self.ax_list = []
        self.folium_map = None
        self.fig = None
        self.plt = None
        self.axes = None
        self.ax = None
        self.tex = None
        self.fontproperties = None
        if mdx_mode:
            # self.app_name = APP_NAME_MDX
            if parser is None:
                parser = get_parser()
            # argv = plant.pre_parser(argv)
            # flag_error = False
            # try:
            #    args = parser.parse_args(argv)
            # except:
            #    flag_error = True#

            # if flag_error:
            #    parser.print_help()
            #    sys.exit(0)
            hhelp_1 = self.check_if_present(argv, '-h')
            hhelp_2 = self.check_if_present(argv, '--help')
            if hhelp_1 or hhelp_2:
                # parser.print_help()
                try:
                    args = parser.parse_args(argv)
                except:
                    args = None
                    # raised_exception = sys.exc_info()[0]
                if (args is not None and
                        'cli_mode' in args.__dict__.keys()):
                    self.cli_mode = args.__dict__['cli_mode']
                else:
                    self.cli_mode = True
                plant.argparse_print_help(parser,
                                          cli_mode=self.cli_mode)
                return
            '''
            flag_verbose = True
            if 'verbose' not in self.__dict__.keys():
                self.verbose = flag_verbose
            else:
                flag_verbose = self.verbose
            '''
            args = self.get_mdx_arguments(argv,
                                          all_images_setup=True)
            # print('*** 111 parsed args: ', args)
            super().__init__(parser,  parsed_args=args)
            # print(args)
            # self.print('PLAnT plant_display.py ' +
            #           get_command_line_from_argv(args))
            # if all_images_setup:
            #    self.print('arguments: '+' '.join(args))
            # self.verbose = flag_verbose
        else:
            # self.app_name = APP_NAME_DISPLAY
            if parser is None:
                parser = get_parser()
            # print('*** 222: ', argv)
            super().__init__(parser, argv)

        if self.default_min is not None:
            self.default_min = plant.parse_arg_str(self.default_min,
                                                   dtype=float)
        if self.default_max is not None:
            self.default_max = plant.parse_arg_str(self.default_max,
                                                   dtype=float)
        if self.default_nbins is not None:
            self.default_nbins = plant.parse_arg_str(self.default_nbins,
                                                     dtype=int)
            

        if self.color is not None:
            self.color = plant.parse_arg_str(self.color)
        if self.cmap is not None:
            self.cmap = plant.parse_arg_str(self.cmap)
        if self.multiplot_line is not None:
            self.multiplot_line = plant.parse_arg_str(self.multiplot_line)
        if self.multiplot_column is not None:
            self.multiplot_column = plant.parse_arg_str(self.multiplot_column)

        if self.name is not None:
            self.name = plant.parse_arg_str(self.name)

        # self.out_mask = temp.out_mask
        # self.out_maskover = temp.out_maskover
        # self.filter_size = temp.filter_size
        # self.in_null = temp.in_null
        # self.linestyle = temp.linestyle
        # self.linecolor = temp.linecolor
        # self.linewidth = temp.linewidth
        # self.alpha = temp.alpha
        # self.percentile = temp.percentile
        # self.complex_type = temp.complex_type
        # self.median = temp.median
        # self.mean = temp.mean
        # self.mode = temp.mode
        # self.stddev = temp.stddev
        # self.db10 = temp.db10
        # self.db20 = temp.db20
        # self.inv_db10 = temp.inv_db10
        # self.inv_db20 = temp.inv_db20

        # ret = search_pol(self.input_images, sym=False, vh_to_hv=False,
        #                 verbose=False, mode='lex')
        # ret = search_pol(self.input_images,
        #                 sym=False,
        #                 exact_str_vv_search=False,
        #                 verbose=False)

        # if ret is not None:
        #    self.hh_file = ret['hh_file']
        #    self.hv_file = ret['hv_file']
        #    self.vh_file = ret['vh_file']
        #    self.vv_file = ret['vv_file']
        #    self.identified_inputs = ret['identified_inputs']
        #    self.master_file = self.identified_inputs[0]
        # self.populate_parameters()

    def populate_parameters(self, image_obj=None):
        '''
        populate class parameters
        '''
        # np.seterr(divide='ignore', invalid='ignore', over='ignore')

        '''
        if len(self.input_images) == 0:
            parser = get_parser()
            parser.print_usage()
            return
            # sys.exit(0)
        '''

        flag_option_selected = (self.hist or
                                self.hist2d or
                                self.scatterplot or
                                self.generic_plot or
                                self.trendplot or
                                self.profilex or
                                self.profiley or
                                self.profiley_horizontal or
                                self.print_text or
                                self.multiplot or
                                self.barplot or
                                self.table or
                                self.im or
                                self.im_geo or
                                self.geolocation)

        if not flag_option_selected:
            if image_obj is None:
                for current_image in self.input_images:
                    try:
                        image_obj = self.read_image(current_image,
                                                    verbose=False,
                                                    only_header=True)
                        break
                    except:
                        continue
            if image_obj is None:
                # raise error?
                self.im = True
            elif image_obj.length == 1:
                # self.type = GENERIC_PLOT
                self.generic_plot = True
            elif image_obj.width == 1 and image_obj.length != 1:
                # self.type = GENERIC_PLOT
                self.profiley = True
            else:
                # self.type = IMAGEVIEWER
                # selected_type.append(self.type)
                self.im = True
                self.mode = 'pyplot'
        self.type = ''
        # selected_type = []
        self.text = None
        self.multiplot_type = None
        if self.hist:
            self.type = HISTOGRAM
            # selected_type.append(self.type)
        if self.hist2d:
            self.type = DENSITY_PLOT
            # selected_type.append(self.type)

        if self.trendplot or self.generic_plot or self.scatterplot:
            self.type = GENERIC_PLOT
            # selected_type.append(self.type)
            if self.generic_plot:
                self.text = 'Plot'
            elif self.scatterplot:
                self.text = 'Scatterplot'
            elif self.trendplot:
                self.text = 'Trendplot'
            else:
                self.text = ''

        if self.profilex:
            self.type = PROFILE
            # selected_type.append(self.type)
            self.profile_direction = 'X'
            self.profile_axis = 0
            self.text = 'X profile'

        if self.profiley or self.profiley_horizontal:
            self.type = PROFILE
            # selected_type.append(self.type)
            self.profile_direction = 'Y'
            self.profile_axis = 1
            self.text = 'Y profile'
            if (self.profiley_horizontal and
                    self.inv_axis is None):
                self.inv_axis = True
                self.invert_y_axis = True
            # elif self.profiley:
            #     if self.orientation is None:
            #         self.orientation = 'vertical'
            #         # self.invert_yaxis = False
            #     elif self.orientation.lower().startswith('h'):
            #         # self.invert_yaxis = True
            #     else:
            #         self.invert_yaxis = True
        if self.im or self.im_geo or self.flag_animate:
            self.type = IMAGEVIEWER
            # selected_type.append(self.type)
            self.mode = 'pyplot'

        '''
        if self.tk:
            self.type = IMAGEVIEWER
            selected_type.append(self.type)
            self.mode = 'tk'
        '''
        if self.multiplot:
            # self.mean = (self.mean or not (self.median or
            #                               self.mode or
            #                               self.stddev))

            # if self.app_name==APP_NAME_MDX:
            #    self.print('ERROR not implemented MULTIPLOT for %s'
            #               ' (use %s)'
            #               %(APP_NAME_MDX, APP_NAME_DISPLAY))
            #    sys.exit(1)
            self.type = MULTIPLOT
            self.text = 'Multiplot'
            self.multiplot_type = 'line'
            # selected_type.append(self.type)
        if self.barplot:
            self.type = MULTIPLOT
            self.text = 'Barplot'
            self.multiplot_type = 'bar'
            # selected_type.append(self.type)
        if self.table:
            self.type = MULTIPLOT
            self.text = 'Table'
            self.multiplot_type = 'table'
            # selected_type.append(self.type)
        if self.print_text:
            self.type = PRINT
            # selected_type.append(self.type)
        if self.geolocation:
            self.type = GEOLOCATION
            # selected_type.append(self.type)

        if self.no_show:
            mpl.use('Agg')
        import matplotlib.pyplot as plt
        self.plt = plt
        # if len(selected_type) > 1:
        #    print('ERROR conflicting options: '+', '.join(selected_type))
        #    sys.exit(1)

        if self.text is None:
            self.text = self.type

        if self.grid is None:
            self.grid = not self.type == IMAGEVIEWER
            '''
            (self.multiplot_type is not None and
             self.multiplot_type == 'bar'))
            '''

        self.linestyle_orig = self.linestyle

        self.plot_names_list = []
        self.plot_handles_list = []

        '''
        if self.percentile is None and self.type == GENERIC_PLOT:
            self.percentile = 110
        # elif self.percentile is None and self.type == DENSITY_PLOT:
        #     self.percentile = 80
        elif self.percentile is None:
            self.percentile = 99
        if self.cpercentile is None:
            self.cpercentile = 90
        print('*** self.cpercentile 111 : ', self.cpercentile)
        '''
        ret = self.get_min_max_nbins(self.default_min,
                                     self.default_max,
                                     self.default_nbins)
        self.default_min, self.default_max, self.default_nbins = ret
        # if self.edgecolor is None:
        #     self.edgecolor='black'

        if (self.legend_location is not None and
                self.legend is not False):
            self.legend = True
        self.mappable = None
        self.colorbar_range = [np.nan, np.nan]

        # if (self.type != IMAGEVIEWER and
        # self.type == IMAGEVIEWER and not self.im_rgb) and
        if self.plt.get_fignums():
            self.plt.cla()
            self.plt.clf()
            self.plt.close('all')
            # window(s) open
        # else:
        # no windows
        if self.type == IMAGEVIEWER and self.flag_use_ctable is None:
            self.flag_use_ctable = True
        if self.linewidth_stats is None:
            self.linewidth_stats = self.linewidth
        if self.fontsize is None:
            self.fontsize = 16
        if self.legend_fontsize is None:
            self.legend_fontsize = max(self.fontsize - 2, 2)
        if self.fontsize_stats is None:
            self.fontsize_stats = self.legend_fontsize
        if self.color_stats_text is None:
            self.color_stats_text = COLOR_STATS_TEXT_DEFAULT

        if (self.colorbar is None and
                (self.colorbar_db or
                 self.colorbar_orientation is not None or
                 self.colorbar_label is not None or
                 self.flag_colorbar_in_line is not None)):
            self.colorbar = True
        # if self.colorbar is None:
        #    self.colorbar = False
        self.geotransform = None
        self.projection = None
        self.input_number = 0
        self.mask = None

        # MULTIPLOT options
        self.plot_x_list = []
        self.plot_y_list = []
        # self.plot_y_color_list = []
        self.plot_xy_list = []
        self.plot_last_line = None
        self.plot_last_column = None

        if (self.out_text is not None and
                (self.type == DENSITY_PLOT or
                 self.type == IMAGEVIEWER or
                 self.type == PROFILE)):
            self.print('WARNING out-text is not available for '
                       'this option')

        # self.type == MULTIPLOT

        # if self.type == MULTIPLOT and not self.median and not self.stddev:
        #    self.mean = True
        # self.type == GENERIC_PLOT or
        self.flag_use_first_image_mask = self.type == DENSITY_PLOT
        #    self.type == PRINT)
        # nlines is only set when the number of lines is first determined
        self.annotation_count = 1
        self.data = {}
        if (self.plot_color_mode is not None and
                self.plot_color_mode > 5):
            self.print('ERROR invalid trendplot color mode: ' +
                       str(self.plot_color_mode))
            return
        self.parameters_populated = True
        # if (self.out_text is not None and not self.type == HISTOGRAM
        #        and not self.type == PRINT):
        #    print('WARNING text mode only implemented for histogram and '
        #          'print.')
        # if self.mode and not self.type == HISTOGRAM:
        #     print('WARNING --mode only implemented for histogram')

    def _load_bands_separately(self, nbands, band=None):
        if nbands == 1 or band is not None:
            return False
        flag_load_together = (self.type == IMAGEVIEWER and
                              self.im_rgb is not False and
                              not (self.im_rgb is None and
                                   nbands > 4))
        return not flag_load_together

    def run(self):
        flag_multiplot_default = \
            (self.type == MULTIPLOT and
             self.band is None and
             self.multiplot_line is None and
             self.multiplot_line_end is not True and
             self.multiplot_line_end_name is None and
             self.multiplot_column is None and
             self.multiplot_column_end is not True and
             self.multiplot_column_end_name is None)
        for image in self.input_images:
            image_obj = self.read_image(image,
                                        only_header=True,
                                        verbose=False)
            if image_obj.nbands == 1:
                self.insert_data(image)
                continue
            if self._load_bands_separately(image_obj.nbands):
                print(f'## image: {image_obj.filename}')
                for current_band in range(image_obj.nbands):
                    with plant.PlantIndent():
                        if (current_band==image_obj.nbands-1 and
                                flag_multiplot_default):
                            self.multiplot_line_end_name = image.name
                            # args_orig.extend([
                            #     '--end-multiplot-line-name',
                            #     image_obj.name])
                        self.insert_data(image,
                                         band=current_band)
                continue
            self.insert_data(image)
        ret = self.show()
        return ret

    def insert_data_with_arg(self,
                             image,
                             args_orig,
                             band=None):

        args = args_orig[:]
        if not self.parameters_populated:
            self.populate_parameters(image_obj=image)
        self.populate_parameters_from_args_current_data(args)
        flag_multiplot_default = \
            (self.type == MULTIPLOT and
             band is None and
             self.multiplot_line is None and
             self.multiplot_line_end is not True and
             self.multiplot_line_end_name is None and
             self.multiplot_column is None and
             self.multiplot_column_end is not True and
             self.multiplot_column_end_name is None)
    
        if band is None:
            band = self.band
        if (isinstance(image, plant.PlantImage) and
                self._load_bands_separately(image.nbands,
                                            band=band)):
            image_obj = image
            print(f'## image: {image_obj.filename}')
            for b in range(image_obj.nbands):
                with plant.PlantIndent():
                    if (b==image_obj.nbands-1 and
                            flag_multiplot_default):
                        self.multiplot_line_end_name = image.name
                        args_orig.extend(['--end-multiplot-line-name',
                                          image_obj.name])
                    self.insert_data_with_arg(image_obj,
                                              args_orig, b)
            return
        self.insert_data(image,
                         vmin=self.vmin,
                         vmax=self.vmax,
                         nbins=self.nbins,
                         args=args,
                         band=band)
        '''
        image_obj = self.read_image(image,
                                    only_header=True,
                                    band=band,
                                    verbose=False)

        self.populate_parameters_from_args_current_data(args)
        if image_obj.nbands == 1:
            self.insert_data(image,
                             vmin=self.vmin,
                             vmax=self.vmax,
                             nbins=self.nbins,
                             args=args,
                             band=band)
            return

        for current_band in range(image_obj.nbands):
            self.insert_data(image,
                             vmin=self.vmin,
                             vmax=self.vmax,
                             nbins=self.nbins,
                             args=args,
                             band=current_band)
        '''

    def populate_parameters_from_args_current_data(self, argv):
        # self.mask = None
        # print('*** argv: ', argv)
        args = self.get_mdx_arguments(argv)
        verbose_orig = args.verbose
        args.verbose = False
        # print('*** args: ', args)
        temp = PlantScript(get_parser(), parsed_args=args)
        self.band = temp.band

        self.verbose &= verbose_orig
        self.flag_debug |= temp.flag_debug
        self.force != temp.force
        if temp.debug_level is not None:
            self.debug_level = temp.debug_level
        # if temp.dark_theme is not None:
        #     self.dark_theme = temp.dark_theme
        # if temp.plot_style:
        #     self.plot_style = temp.plot_style

        # copy only single-data specific parameters
        # self.title = temp.title
        # self.verbose = temp.verbose
        if temp.default_min is None:
            self.vmin = None
        else:
            self.vmin = float(temp.default_min)
        if temp.default_max is None:
            self.vmax = None
        else:
            self.vmax = float(temp.default_max)
        if temp.default_nbins is None:
            self.nbins = None
        else:
            self.nbins = int(temp.default_nbins)
        self.color = temp.color
        if self.type == DENSITY_PLOT or self.type == IMAGEVIEWER:
            self.cmap = temp.cmap
        self.multiplot_line = temp.multiplot_line
        self.multiplot_line_end = temp.multiplot_line_end
        self.multiplot_line_end_name = temp.multiplot_line_end_name
        self.multiplot_column_end = temp.multiplot_column_end
        self.multiplot_column_end_name = temp.multiplot_column_end_name
        self.multiplot_column = temp.multiplot_column

        if self.type == PROFILE:
            self.sum_normed = temp.sum_normed
            self.max_normed = temp.max_normed
            self.sum_normed_to_value = temp.sum_normed_to_value
            self.max_normed_to_value = temp.max_normed_to_value

        parsed_name = plant.parse_arg_str(temp.name)
        if ((parsed_name is not None and
             (isinstance(parsed_name, str) or
              (not isinstance(parsed_name, str)) and
              len(parsed_name) == 1)) or
                not (isinstance(self.name, list) and len(self.name) > 1)):
            self.name = None if temp.name is None else parsed_name
        # self.out_text = temp.out_text
        # self.out_pdf = temp.out_pdf

        # These parameters should be fixed for command-line plant_display.py
        self.plant_transform_obj = temp.plant_transform_obj

        # self.out_mask = temp.out_mask
        # self.mask_as_input = temp.mask_as_input

        # self.filter_size = temp.filter_size
        self.in_null = temp.in_null
        self.linestyle = temp.linestyle
        self.linestyle_stats = temp.linestyle_stats
        self.linecolor = temp.linecolor
        self.linecolor_stats = temp.linecolor_stats
        self.color_stats = temp.color_stats
        self.color_stats_text = temp.color_stats_text
        self.linewidth = temp.linewidth
        self.linewidth_stats = temp.linewidth_stats
        if self.linewidth_stats is None:
            self.linewidth_stats = self.linewidth
        # if self.legend_fontsize is None:
        #    self.legend_fontsize = self.fontsize
        # if self.linewidth_stats is None:
        #     self.linewidth_stats = self.linewidth
        if (self.plot_style is not None or
                self.plot_style not in PREDEFINED_STYLES):
            self.edgecolor = temp.edgecolor
            self.hatch = temp.hatch
            self.markersize = temp.markersize
            self.marker = temp.marker
            self.alpha = temp.alpha

        self.percentile = temp.percentile

        if self.percentile is None and self.trendplot:
            self.percentile = 90
        elif self.percentile is None and self.type == GENERIC_PLOT:
            self.percentile = 100
        elif self.percentile is None and (self.type == DENSITY_PLOT or
                                          self.type == HISTOGRAM):
            self.percentile = 99.8
        elif self.percentile is None:
            self.percentile = 99
        '''
        if self.linecolor is None and self.type == DENSITY_PLOT:
            self.linecolor = 'white'
        elif self.linecolor is None:
            self.linecolor = 'black'
        '''

        # if self.edgecolor is None:
        #     self.edgecolor = 'black'

        self.complex_type = temp.complex_type
        # print('DEBUG multiplot? ', self.multiplot)
        # #print(self.input_number)
        # print(self.median)
        # print(self.mean)
        # print(self.mode)
        # print(self.stddev)
        if self.type != MULTIPLOT:
            self.median = temp.median
            self.mean = temp.mean
            self.mode = temp.mode
            self.stddev = temp.stddev
        self.stddev_scale = temp.stddev_scale
        self.cmax = temp.cmax
        self.cmin = temp.cmin
        self.cpercentile = temp.percentile

        # self.db10 = temp.db10
        # self.db20 = temp.db20
        # self.inv_db10 = temp.inv_db10
        # self.inv_db20 = temp.inv_db20
        # self.sum_normed = temp.sum_normed

        # These parameters should be fixed for mdx.py:
        # self.row = temp.row
        # self.col = temp.col
        # self.polygon = temp.polygon
        # self.in_mask = temp.in_mask

        '''
        not_copy_list = ['cmap', 'draw_line', 'label_y', 'label_x',
                         'start_time', 'logger_filename', 'logger']
        for key in temp.__dict__.keys():
            if key in not_copy_list:
                print('not copied: ', key)
                continue
            if key is 'default_min':
                self.__dict__['vmin'] = temp.__dict__[key]
                print('vmin')
                continue
            if key is 'default_max':
                self.__dict__['vmax'] = temp.__dict__[key]
                print('vmax')
                continue
            if key is 'default_nbins':
                self.__dict__['nbins'] = temp.__dict__[key]
                print('vmax')
                continue
            self.__dict__[key] = temp.__dict__[key]
            print('copied: '+key)
            '''
        # if temp.__dict__[i] != self.__dict__[i]:
        #    print('--------------------')
        #    print(i, temp.__dict__[i], self.__dict__[i])

        # self.cmin = temp.cmin
        # self.cmax = temp.cmax
        # self.xmin = temp.xmin
        # self.xmax = temp.xmax
        # self.fontsize = temp.fontsize

    def get_mdx_arguments(self, args, all_images_setup=False):
        # if all_images_setup:
        #    self.print('arguments: '+' '.join(args))
        args_orig = list(args)
        rgb = self.check_if_present(args, '-rgb')
        if rgb:
            self.im_rgb = True
        self.get_if_present(args, '-s')
        self.get_if_present(args, '-mix')
        hset = self.get_if_present(args, '-set')
        hcmap = self.get_if_present(args, '-cmap')

        if all_images_setup:
            while('-a' in args):
                self.get_if_present(args, '-a')
            while('-m' in args):
                self.get_if_present(args, '-m')
            ha = None
            hm = None
        else:
            ha = self.get_if_present(args, '-a')
            hm = self.get_if_present(args, '-m')

        parser = get_parser()
        args = plant.pre_parser(
            args, parser=parser,
            flag_remove_duplicates=not all_images_setup)
        ret = parser.parse_known_args(args)
        # print('*** ret: ', ret)
        ignored_args = ret[1]
        temp = ret[0]
        # print('*** args: ', args)
        # print('*** temp.verbose: ', temp.verbose)
        if hset is not None and temp.title is None:
            temp.title = hset
        if hcmap is not None and temp.cmap is None:
            temp.cmap = hcmap

        # "shared" --min/--max/--nbins by default
        if all_images_setup and not(temp.hist
                                    or temp.profilex
                                    or temp.profiley
                                    or temp.profiley_horizontal):
            temp.default_min = None
            temp.default_max = None
            temp.default_nbins = None

        ret = self.get_min_max_nbins(temp.default_min,
                                     temp.default_max,
                                     temp.default_nbins)
        temp.default_min, temp.default_max, temp.default_nbins = ret

        # mdx: min, max, nbins are floats (not lists of floats)
        if temp.default_min is not None:
            temp.default_min = temp.default_min[0]
        if temp.default_max is not None:
            temp.default_max = temp.default_max[0]
        if temp.default_nbins is not None:
            temp.default_nbins = temp.default_nbins[0]

        if ha is not None and temp.default_min is None:
            temp.default_min = str(ha)
        if (hm is not None and temp.default_max is None and
                temp.default_min is not None):
            temp.default_max = str(float(temp.default_min) + float(hm))

        if temp.complex_type is None:
            if (self.check_if_present(args, '-c8mag') or
                    self.check_if_present(args, '-c2mag') or
                    self.check_if_present(args, '-cmag')):
                temp.complex_type = 'mag'
            elif (self.check_if_present(args, '-c8pha') or
                  self.check_if_present(args, '-c2pha') or
                  self.check_if_present(args, '-cpha')):
                temp.complex_type = 'phase'
        for arg in ignored_args:
            if arg == plant.get_mdx_dtype(arg):
                self.check_if_present(ignored_args, arg)
        ignored_args = [arg for arg in ignored_args if
                        (not plant.isnumeric(arg) and
                         arg.startswith('-'))]

        # remove duplicated args from ignored:
        invalid_list = []
        ret = plant.get_images_from_list(ignored_args,
                                         flag_exit_if_error=False,
                                         invalid_list=invalid_list,
                                         flag_no_messages=True,
                                         verbose=False)
        duplicated_args = set([arg for arg in invalid_list
                               if args_orig.count(arg) >= 1])
        invalid_list = set([arg for arg in invalid_list
                            if args_orig.count(arg) == 1])
        if len(duplicated_args) > 0:
            self.print('WARNING duplicated arguments: ' +
                       ' '.join(duplicated_args))
        if len(invalid_list) > 0:
            self.print('ERROR not recognized arguments: ' +
                       ' '.join(invalid_list))
            return

        # if all images => remove PlantTransform keywords
        if all_images_setup:
            method_args = plant.get_plant_transform_obj.__code__.co_varnames
            method_nargs = plant.get_plant_transform_obj.__code__.co_argcount
            method_defaults = plant.get_plant_transform_obj.__defaults__
            method_args = method_args[:method_nargs]
            args_dict = temp.__dict__
            for i, arg in enumerate(method_args):
                if (arg in args_dict.keys() and
                        arg != 'verbose' and
                        arg != 'width' and
                        arg != 'length' and
                        arg != 'null'):
                    # print(f'{arg}: {args_dict[arg]} substituted by'
                    #       f' {method_defaults[i]}')
                    args_dict[arg] = method_defaults[i]

        return temp

    def insert_data(self,
                    image_obj,
                    vmin=None,
                    vmax=None,
                    nbins=None,
                    args=None,
                    band=None):
        band_orig = band
        image_obj = plant.PlantImage(image_obj,
                                     verbose=False,
                                     sampling_step=self.sampling_step,
                                     band=band)
        if not self.parameters_populated:
            self.populate_parameters(image_obj=image_obj)

        if image_obj.filename_orig:
            filename = image_obj.filename_orig
        elif image_obj.filename:
            filename = image_obj.filename
        else:
            filename = f'input {self.input_number+1}'

        if (self.type == IMAGEVIEWER and self.im_rgb and
            ((self.input_number >= 3 and image_obj.nbands == 1) or
             self.input_number >= 4)):
            self.print('WARNING RGB mode of image viewer accepts at '
                       'most four images (RGB and alpha)', 1)
            self.print('ignoring: '+filename, 1)
            return

        '''
        if (self.type == IMAGEVIEWER and
                (self.sampling_step is None or
                 self.red_x is None or
                 self.red_y is None)):
            ''
            if image_obj is None:
                image_obj = self.read_image(image,
                                            verbose=False,
                                            only_header=True,
                                            force=self.force)
            ''

            if (self.zoom is not None and
                    self.sampling_step is None and
                    self.zoom < 0):
                self.sampling_step = -self.zoom
                self.red_x = 1
                self.red_y = 1
            elif (self.zoom is not None and
                  self.sampling_step is None):
                self.sampling_step = 1
                self.red_x = 1
                self.red_y = 1
            elif (self.zoom is None and
                  self.sampling_step is not None):
                self.red_y = max([int(image_obj.length /
                                      (self.sampling_step *
                                       2*plant.DEFAULT_PLOT_SIZE_Y)), 1])
                self.red_x = max([int(image_obj.width /
                                      (self.sampling_step *
                                       2*plant.DEFAULT_PLOT_SIZE_X)),
                                  self.red_y])
                self.red_x = self.red_y
            elif (self.zoom is None):
                red_y = max([int(image_obj.length /
                                 (2*plant.DEFAULT_PLOT_SIZE_Y)), 1])
                self.sampling_step = max([int(image_obj.width /
                                              (2*plant.DEFAULT_PLOT_SIZE_X)),
                                          red_y])
                self.red_x = 1
                self.red_y = 1
                if self.sampling_step != 1:
                    self.print('WARNING reducing image size with sampling'
                               f' factor of {self.sampling_step} for faster'
                               ' visualization. Use -z 1 to disable this'
                               ' option.')
            elif self.zoom < 0:
                # self.red_x = max(int(-self.zoom/
                #                     self.sampling_step), 1)
                # self.red_y = self.red_x
                self.red_x = -self.zoom
                self.red_y = -self.zoom
            # image_obj.sampling_step = self.sampling_step
            if self.sampling_step > 1:
                image_obj = plant.sample_image(image_obj,
                                               sampling_step=self.sampling_step)
            '''
        # image_obj = self.read_image(image,
        #                            only_header=True,
        #                            verbose=False)
        # import time
        # print('start: ', time.time()-self.start_time)
        # start_time_2 = time.time()
        # print('*** image: ', image)

        # print('*******************')
        # print('*** flag_mute: ', self.flag_mute)
        # print('**** flag_ipython: ', plant.plant_config.flag_ipython)
        # print('*** main script obj: ',
        #       str(plant.plant_config.main_script.__class__.__name__))
        # print('*** script obj: ',
        #       str(plant.plant_config.current_script.__class__.__name__))
        # print('*******************')

        # print('*** band: ', band)
        if (band is not None and image_obj.nbands > 1 and
                hasattr(band, "__getitem__") and len(band) > 1):
            band_obj = image_obj.get_band(band=band[0])
        else:
            band_obj = image_obj.get_band(band=band)

        # print(image_obj.nbands, band)
        # if self.flag_use_ctable:
        #    self.im_rgb = True

        # print('diff: ', time.time()-start_time_2)
        if self.type == GEOLOCATION and image_obj.geotransform is None:
            print(f'WARNING invalid geolocation in {filename}. '
                  f'Ignoring input..')
            return

        dtype = plant.get_dtype_name(band_obj.dtype)
        dtype = 'str' if 'str' in dtype else dtype
        data_obj = DataDisplay()
        self.data[self.input_number] = data_obj
        data_obj.number = self.input_number
        data_obj.width = image_obj.width
        data_obj.length = image_obj.length
        data_obj.depth = image_obj.depth
        data_obj.geotransform = image_obj.geotransform
        data_obj.projection = image_obj.projection
        # nbands_orig = image_obj.nbands_orig
        if self.flag_use_ctable:
            data_obj.ctable = band_obj.ctable
        file_format = image_obj.file_format

        if band is None:  # or nbands_orig <= 1:
            self.print('## input %d: %s'
                       % (data_obj.number+1, image_obj), 1)
        else:
            if (image_obj.band_orig is None or
                    hasattr(band, '__getitem__') or
                    band >= len(image_obj.band_orig)):
                band_orig = band
            else:
                band_orig = image_obj.band_orig[band]
            self.print('## input %d: %s (band: %s)'
                       % (data_obj.number+1, image_obj,
                          band_orig), 1)

        with plant.PlantIndent():
            if args is not None:
                args_string = ' '.join(args)
                if args_string:
                    self.print(f'arguments: {args_string}')

            # name
            # print('*** self.name: ', self.name)
            # print('*** band_obj.full_name: ', band_obj.full_name)
            '''
            if self.name is None and band_obj.full_name:
                # data_obj.name = band_obj.name
                # data_obj.name = (path.basename(filename) +
                #                  f' ({band_obj.name})')
                data_obj.name = band_obj.full_name
            el
            '''
            parameters_dict = {}
            # print('*** 0')
            if (self.name is None and band_obj.name and
                    not band_obj.name.startswith('MEM')):
                data_obj.name = band_obj.name
                # print('*** 1')
            elif (self.name is None and
                  plant.test_other_drivers(
                      path.basename(filename),
                      parameters_dict=parameters_dict) and
                  'key' in parameters_dict.keys() and
                  parameters_dict['key'] is not None):
                data_obj.name = parameters_dict['key']
                # print('*** 2')
            elif (self.name is None and
                  image_obj.nbands > 1 and
                  not filename.startswith('MEM')):
                data_obj.name = (path.basename(filename) +
                                 f' (band: {band})')
                # print('*** 3')
            elif self.name is None:
                data_obj.name = None
                data_obj.user_defined_name = None
            elif isinstance(self.name, str):
                data_obj.name = self.name
                data_obj.user_defined_name = data_obj.name
                # print('*** 4')
            elif len(self.name) == 1:
                data_obj.name = self.name[0]
                data_obj.user_defined_name = data_obj.name
                # print('*** 5')
            elif (len(self.name) != 1 and
                    len(self.name) < self.input_number+1):
                self.print('ERROR name/label/legend-label '
                           'arameter vector has less elements '
                           'than number of input files.')
                return
            else:
                data_obj.name = self.name[self.input_number]
                data_obj.user_defined_name = data_obj.name
                # print('*** 6')
            if not image_obj.name.startswith('MEM'):
                data_obj.image_name = image_obj.name
            if data_obj.image_name is None:
                data_obj.image_name = f'input {self.input_number}'

            data_obj.band = band
            data_obj.nbands = image_obj.nbands
            data_obj.file_format = image_obj.file_format
            # print('*** 7')
            # else:
            #     data_obj.image_name = ''

            if data_obj.name is None:
                data_obj.name = data_obj.image_name

            if data_obj.name is not None:
                data_obj.name = data_obj.name.strip()
            if data_obj.user_defined_name is not None:
                data_obj.user_defined_name = data_obj.user_defined_name.strip()
            if data_obj.image_name is not None:
                data_obj.image_name = data_obj.image_name.strip()

            # print('*** name : ', data_obj.name,
            #       data_obj.name.__class__)
            # print('*** image name : ', data_obj.image_name,
            #       data_obj.image_name.__class__)

            # filename
            if image_obj.filename_orig:
                data_obj.filename = image_obj.filename_orig
            elif image_obj.filename:
                data_obj.filename = image_obj.filename
            elif (band_orig is None or
                  plant.IMAGE_NAME_SEPARATOR in filename or
                  image_obj.nbands_orig == 1):
                data_obj.filename = filename
            else:
                data_obj.filename = '%s:%d' % (filename, band_orig)

            '''
            substituted by plant.get_image_names
            if len(data_obj.name) > MAX_NAME_SIZE:
                data_obj.name = '...'+data_obj.name[-MAX_NAME_SIZE-3:]
            '''

            if data_obj.name is not None and filename != data_obj.name:
                self.print(f'name: {data_obj.name}')
            self.print(f'format: {file_format}')
            self.print(f'dtype: {dtype}')
            if (data_obj.geotransform is not None and
                    self.type == GEOLOCATION):
                self.print('geotransform: %s'
                           % (data_obj.geotransform))
                # self.print('projection: %s'
                #           % (data_obj.projection))
            # sys.exit(1)
            # if '-c8mag' in args:
            #    self.complex_type = 'mag'
            # elif '-c8pha' in args:
            #    self.complex_type = 'phase'
            # else:
            # self.print('opening: ' + image)

            if self.type == GEOLOCATION:
                # data_obj.data = image_obj.geotransform
                self.input_number += 1
                return

            if self.type == IMAGEVIEWER and not self.im:
                self.geotransform = image_obj.geotransform
                self.projection = image_obj.projection
                if self.geotransform:
                    self.print('geotransform: %s'
                               % data_obj.geotransform)
                    #  self.print('projection: %s'
                    #             % self.projection)
                    # no mode (im or im_geo) selected
                    # selecting im_geo
                    '''
                    if not self.im_geo:
                        try:
                            from mpl_toolkits.basemap import Cartopy
                            self.im_geo = True
                        except ImportError:
                            self.geotransform = None
                            self.projection = None
                            self.im = True
                    '''

                elif self.im_geo:
                    self.print('WARNING geolocation not found for: ' +
                               data_obj.filename, 1)
                    self.im_geo = False
                    self.im = True

            if self.color_stats is None:
                self.color_stats = self.color
            data_obj.cmap = self.cmap
            data_obj.edgecolor = self.edgecolor
            data_obj.hatch = self.hatch
            data_obj.color = self.color
            data_obj.color_stats = self.color_stats
            data_obj.linestyle = self.linestyle
            data_obj.linestyle_stats = self.linestyle_stats
            data_obj.linecolor = self.linecolor
            data_obj.linecolor_stats = self.linecolor_stats
            data_obj.linewidth = self.linewidth
            data_obj.linewidth_stats = self.linewidth_stats
            data_obj.color_stats_text = self.color_stats_text
            data_obj.fontsize_stats = self.fontsize_stats
            data_obj.alpha = self.alpha
            data_obj.sum_normed = self.sum_normed
            data_obj.max_normed = self.max_normed
            data_obj.sum_normed_to_value = self.sum_normed_to_value
            data_obj.max_normed_to_value = self.max_normed_to_value
            data_obj.width = image_obj.width
            data_obj.length = image_obj.length
            data_obj.depth = image_obj.depth
            # if 'int' not in dtype:
            # print('*** image_obj.nbands: ', image_obj.nbands)

            if (self.sampling_step is not None and
                    self.sampling_step != 1):
                self.print(f're-sampling step: {self.sampling_step}')
                sampling_step_suffix = '(after re-sampling)'
            else:
                sampling_step_suffix = ''
            self.print('width: %d %s'
                       % (data_obj.width, sampling_step_suffix))
            self.print('length: %d %s'
                       % (data_obj.length, sampling_step_suffix))
            if band is None and image_obj.nbands > 1:
                self.print(f'bands: {image_obj.nbands}')
                data_stack = []
                for b in range(image_obj.nbands):
                    data_stack.append(image_obj.get_image(band=b))
                data = np.dstack(data_stack)
                data_obj.depth = image_obj.nbands
            elif hasattr(band, "__getitem__") and len(band) > 1:
                self.print(f'bands: {band}')
                data_stack = []
                for b in range(len(band)):
                    data_stack.append(image_obj.get_image(band=b))
                data = np.dstack(data_stack)
                data_obj.depth = len(band)
            else:
                if data_obj.depth > 1:
                    self.print('depth: %d %s'
                               % (data_obj.depth,
                                  sampling_step_suffix))
                if band is None:
                    data = image_obj.get_image()
                else:
                    data = image_obj.get_image(band=band)

            # else:
            #    data = np.asarray(np.copy(image_obj.get_image()),
            #                       dtype=np.float32)

            flag_cast_to_real = not self.print_text
            if (flag_cast_to_real and
                ('complex' in dtype.lower() or
                 'cfloat' in dtype.lower() or
                 'cdouble' in dtype.lower())):
                if self.complex_type is None:
                    self.print('WARNING Data is complex.'
                               ' Using absolute value.')
                    self.complex_type = 'mag'
                data_obj.complex_type = self.complex_type
                if 'real' in self.complex_type:
                    self.print('selecting real part..')
                    data = np.real(data)
                elif 'imag' in self.complex_type:
                    self.print('selecting the imaginary part..')
                    data = np.imag(data)
                elif ('power' in self.complex_type or
                      'int' in self.complex_type or
                      'mag_sq' in self.complex_type):
                    self.print('selecting magnitude square '
                               '(power/intensity) ..')
                    data = np.absolute(data)**2
                elif ('mag' in self.complex_type or
                      'abs' in self.complex_type):
                    self.print('selecting magnitude..')
                    data = np.absolute(data)
                elif 'phase' in self.complex_type:
                    self.print('selecting phase..')
                    data = np.angle(data)
                else:
                    self.print('ERROR complex type not recognized: ' +
                               self.complex_type)
                    return

            # print('*** dtype:', dtype)
            dtype = plant.get_dtype_name(data)
            # print('*** dtype:', dtype)
            '''
            if ('complex' in dtype.lower() or
                    'cfloat' in dtype.lower() or
                    'cdouble' in dtype.lower()):
                data = np.absolute(data)
            '''

            if self.in_null is not None:
                if 'nan' in str(self.in_null).lower():
                    self.print('null value: numpy.nan')
                else:
                    self.print('null value: ' + str(self.in_null))
            else:
                self.in_null = np.nan
            self.in_null = float(self.in_null)

            if 'str' in dtype:
                self.input_number += 1
                data_obj.data = data
                return

            data = plant.apply_null(data, in_null=self.in_null)

            data_obj.flag_vmin_set_by_user = vmin is not None
            data_obj.flag_vmax_set_by_user = vmax is not None

            if (image_obj.file_format in plant.FIG_DRIVERS and
                    vmin is None):
                vmin = 0
            if (image_obj.file_format in plant.FIG_DRIVERS and
                    vmax is None):
                vmax = 255

            if (vmin is None and self.default_min is not None):
                if (len(self.default_min) != 1 and
                        len(self.default_min) < self.input_number+1):
                    self.print('ERROR min parameter vector has less elements '
                               'than number of input files.')
                    sys.exit(1)
                elif len(self.default_min) != 1:
                    vmin = self.default_min[self.input_number]
                else:
                    vmin = self.default_min[0]

            if (vmax is None and self.default_max is not None):
                if (len(self.default_max) != 1 and
                        len(self.default_max) < self.input_number+1):
                    self.print('ERROR max parameter vector has less elements '
                               'than number of input files.')
                    sys.exit(1)
                elif len(self.default_max) != 1:
                    vmax = self.default_max[self.input_number]
                else:
                    vmax = self.default_max[0]

            if self.flag_use_first_image_mask and self.mask is None:
                self.mask = plant.isnan(data)
            elif self.flag_use_first_image_mask:
                if data.shape != self.mask.shape:
                    mask = plant.copy_shape(data, self.mask)
                else:
                    mask = self.mask
                data = plant.insert_nan(data,
                                        mask,
                                        # in_null=self.in_null,
                                        out_null=np.nan)
            '''
            elif self.flag_use_first_image_mask:
                shape_1 = self.mask.shape
                shape_2 = mask.shape
                if shape_1 == shape_2:
                    self.mask = np.logical_and(self.mask, mask)
                else:
                    min_shape = [np.min([shape_1[i], shape_2[i]])
                                 for i in range(len(shape_1))]
                    if len(min_shape) == 1:
                        self.mask = np.logical_and(
                            self.mask[0:min_shape[0]],
                            mask[0:min_shape[0]])
                    if len(min_shape) == 2:
                        self.mask = np.logical_and(
                            self.mask[0:min_shape[0], 0:min_shape[1]],
                            mask[0:min_shape[0], 0:min_shape[1]])
                    if len(min_shape) == 3:
                        self.mask = np.logical_and(
                            self.mask[0:min_shape[0], 0:min_shape[1],
                                      0:min_shape[2]],
                            mask[0:min_shape[0], 0:min_shape[1],
            0:min_shape[2]])
            '''

            '''
            elif not self.flag_use_first_image_mask and mask is not None:
                data = plant.insert_nan(data,
                                        np.where(np.logical_not(mask)),
                                        # in_null=self.in_null,
                                        out_null=np.nan)
                # data[np.where(np.logical_not(mask))] = np.nan
            '''
            # mask user-defined null:
            # if str(self.in_null).lower() != 'nan':
            #    mask[np.where(data == self.in_null)] = 0
            # n_valid_elements = int(np.sum(
            #    np.logical_and(mask, plant.isvalid(data)),
            #    dtype=np.float64))
            # if not(self.flag_use_first_image_mask):

            n_valid_elements = None
            if self.type == HISTOGRAM:
                mask = plant.isvalid(data)
                n_valid_elements = np.sum(mask)
                data = data[np.where(mask)]
                self.print('number of selected elements: ' +
                           str(n_valid_elements))
            '''
            if n_valid_elements == 0:
                self.print(f'WARNING insufficient number of selected'
                           f' elements ({n_valid_elements}).'
                           f' Skipping image...')
                return
            '''

            # if n_valid_elements == 0:
            #    self.print('WARNING no valid values found in %s'
            #               % image)
            #    return
            #    # sys.exit(1)

            # if (self.type == HISTOGRAM or
            #        self.type == MULTIPLOT):
            #    data = data[np.where(mask)]

            # if (self.type != MULTIPLOT):
            #    data = self.apply_db_transformation(data)

            
            data_obj.data = data
            data_obj.cmin = self.cmin
            data_obj.cmax = self.cmax
            if self.cpercentile is None:
                self.cpercentile = 99
            # print('*** self.cpercentile 333: ', self.cpercentile)
            # print('*** self.cpercentile 2: ', self.cpercentile)
            data_obj.cpercentile = self.percentile
            # print(f'*** cmin: {self.cmin}')
            # print(f'*** cmax: {self.cmax}')

            if (self.stddev_scale is None and
                    self.stddev is not None):
                self.stddev_scale = 1

            if self.type == PROFILE:
                self._prepare_profile(data_obj)
            elif self.type == MULTIPLOT:
                self._prepare_multiplot(data_obj)

            shape = data_obj.data.shape

            if len(shape) < 3:
                ret_dict = self._set_min_max_nbins(
                    vmin, vmax, nbins, data_obj.data,
                    n_elements=n_valid_elements)
                if ret_dict is not None:
                    data_obj.vmin = ret_dict['vmin']
                    data_obj.vmax = ret_dict['vmax']
                    data_obj.nbins = ret_dict['nbins']
            else:
                data_obj.vmin = []
                data_obj.vmax = []
                data_obj.nbins = []
                for d in range(shape[2]):
                    print(f'channel: {d}')
                    with plant.PlantIndent():
                        ret_dict = self._set_min_max_nbins(
                            vmin, vmax, nbins, data_obj.data[:, :, d],
                            n_elements=n_valid_elements)
                    if ret_dict is not None:
                        data_obj.vmin.append(ret_dict['vmin'])
                        data_obj.vmax.append(ret_dict['vmax'])
                        data_obj.nbins.append(ret_dict['nbins'])

            # if self.type == IMAGEVIEWER:
            #     data_obj.data[np.where(mask == 0)] = vmin
            if (self.type == DENSITY_PLOT or
                    self.type == IMAGEVIEWER or
                    self.type == GENERIC_PLOT):
                if (self.type == IMAGEVIEWER and self.input_number > 3 and
                        self.im_rgb):
                    self.print('ERROR Number of input files exceeded.')
                    sys.exit(1)
                self.input_number += 1
                return
            self.mean = (self.mean or (self.type == MULTIPLOT and
                                       (not (self.median or
                                             self.mode or
                                             self.stddev))))
            self.calculate_statistics(data_obj)
            if self.type == MULTIPLOT:
                if self.mean:
                    data_obj.data = data_obj.mean
                elif self.median:
                    data_obj.data = data_obj.median
                elif self.mode:
                    data_obj.data = data_obj.mode
                elif self.stddev:
                    data_obj.data = data_obj.stddev  # already in dB
                    data_obj.stddev = None
                # if self.mean or self.median or self.mode:
                #    data_obj.data =
                # self.apply_db_transformation(data_obj.data)

                # self.print('multiplot value: %f'
                #            %(data_obj.data))
                # if data_obj.stddev is not None:
                #    self.print('multiplot error (stddev) value: %f'
                #               %(data_obj.stddev))
            self.input_number += 1

    def _set_min_max_nbins(self, vmin, vmax, nbins, data,
                           n_elements=None, flag_calculate_nbins=None):
        if self.type == PRINT:
            return
        if flag_calculate_nbins is None:
            flag_calculate_nbins = (self.type == HISTOGRAM or
                                    self.type == DENSITY_PLOT or
                                    self.type == GENERIC_PLOT)

        vmin_orig_string = ''  # 'user defined'
        vmax_orig_string = ''  # 'user defined'
        # plant.get_dtype_name(data) != 'datetime')
        if vmin is None and self.type != MULTIPLOT:
            if 'str' in plant.get_dtype_name(data):
                vmin = None
            else:
                vmin = plant.nanpercentile(
                    data, max([100 - self.percentile, 0]))
            if self.percentile < 100:
                vmin_orig_string = f' ({self.percentile}%)'
            if plant.isnan(vmin):
                self.print('WARNING invalid result after applying'
                           ' percentile %f.' % max([100 - self.percentile, 0]))
                # return
            # vmin = np.nan

        # plant.get_dtype_name(data) != 'datetime'
        if vmax is None and self.type != MULTIPLOT:
            # print('*** here 3')
            if 'str' in plant.get_dtype_name(data):
                vmax = None
            else:
                vmax = plant.nanpercentile(data,
                                           min([self.percentile, 100]))
            # print('*** here 4')
            # if isinstance(vmax, datetime.datetime):
            #      vmin = None
            if self.percentile < 100:
                vmax_orig_string = f' ({self.percentile}%)'
            # if plant.isnan(vmax):
            #    self.print('WARNING invalid result after applying'
            #               ' percentile %f. Changing percentile to 1000...'
            #               % max([100 - self.percentile, 0]))
            #    vmax = np.nanmax(data)
            #    vmax_orig_string = ' (0 percentile)'
            if plant.isnan(vmax):
                self.print('WARNING invalid result after applying'
                           ' percentile %f.' % max([self.percentile, 0]))
                # return

        if plant.isvalid(vmin) and plant.isvalid(vmax) and vmin > vmax:
            self.print('ERROR --max must be larger than --min.')
            return

        if self.percentile > 100:
            vmin_orig_string = (' (extrapolated %.1f%%)'
                                % (self.percentile - 100))
            vmax_orig_string = (' (extrapolated %.1f%%)'
                                % (self.percentile - 100))
            extrapolation = ((vmax-vmin) *
                             (float(self.percentile-100)/100.0))
            vmax = vmax + extrapolation
            vmin = vmin - extrapolation

        '''
        if vmin is not None and vmax is not None:
            vdiff = vmax - vmin
            if isinstance(vdiff, datetime.timedelta):
                print('*** here')
                vdiff = vdiff.seconds
        '''

        if flag_calculate_nbins:
            if (self.default_nbins is not None and
                    nbins is None and len(self.default_nbins) != 1):
                nbins = self.default_nbins[self.input_number]
            if (self.default_nbins is not None and
                    nbins is None):
                nbins = self.default_nbins[0]

            if (nbins is None and self.type == GENERIC_PLOT):
                nbins = DEFAULT_NBINS_GENERIC_PLOT
            elif nbins is None:
                nbins = DEFAULT_NBINS
            '''
            elif (nbins is None and
                  vmin is not None and
                  vmax is not None and
                  plant.get_dtype_name(data) != 'datetime' and
                  vmax - vmin < MAX_NBINS and
                  vmax - vmin > 50 and
                  np.all(plant.isinteger(data[
                      np.where(plant.isvalid(data))]))):
                # nbins = (vmax - vmin - 1)//1
                nbins = DEFAULT_NBINS
            elif nbins is None:
                if n_elements is None:
                    n_elements = plant.get_n_elements_from_shape(
                        data.shape)
                nbins = min([n_elements*10, DEFAULT_NBINS])
                # nbins = DEFAULT_NBINS
                if self.type == DENSITY_PLOT:
                    nbins = max([nbins, np.sqrt(n_elements)//1024])
                    nbins = min([nbins, MAX_NBINS])
                else:
                    nbins = max([nbins, n_elements//1024])
                    nbins = min([nbins, MAX_NBINS])
            '''

        if flag_calculate_nbins:
            self.print('nbins = ' + str(int(nbins)))
        if (self.type == HISTOGRAM or
                self.type == DENSITY_PLOT or
                self.type == PROFILE or
                self.type == GENERIC_PLOT):
            self.print('min'+str(vmin_orig_string)+' = ' + str(vmin))
            self.print('max'+str(vmax_orig_string)+' = ' + str(vmax))

        elif self.type == IMAGEVIEWER:
            self.print('min (color scale) = ' + str(vmin))
            self.print('max (color scale) = ' + str(vmax))

        ret_dict = {}
        ret_dict['vmin'] = float(vmin)
        ret_dict['vmax'] = float(vmax)
        ret_dict['nbins'] = nbins
        
        return ret_dict

    def calculate_statistics(self, data_obj):
        if 'str' in plant.get_dtype_name(data_obj.data):
            return
        if self.median:
            data_obj.median = np.median(data_obj.data)
            self.print(f'median: {data_obj.median}')
            data_obj.color_median = data_obj.color
            data_obj.color_median_text = self.linecolor
        if self.mean:
            if data_obj.mean is None:
                data_obj.mean = np.nanmean(data_obj.data, dtype=np.float32)
            self.print(f'mean: {data_obj.mean}')
            # data_obj.color_mean = data_obj.color
            # data_obj.color_mean_text = 'black'
        if self.mode and not self.type == HISTOGRAM:
            if data_obj.mode is None:
                # if self.type == GENERIC_PLOT:
                # data_obj.mode = mode(data_obj.data)
                data_obj.mode = np.nanmax(data_obj.data)
                self.print(f'mode: {data_obj.mode}')
                '''
                else:
                    nbins = data_obj.nbins
                    data_obj.mode = np.nanmax(data_obj.data,
                                              nbins=nbins)
                    self.print(f'mode: {data_obj.mode}'
                               f' ({nbins} bins)')
                '''
            # data_obj.color_mode = data_obj.color
            # data_obj.color_mode_text = 'black'
        if self.stddev:
            if data_obj.mean is None and self.type != MULTIPLOT:
                data_obj.mean = np.nanmean(data_obj.data)
                self.print(f'mean: {data_obj.mean}')
            if data_obj.stddev is None:
                data_obj.stddev = np.nanstd(data_obj.data)
                data_obj.stddev_text = 'black'
                self.print(f'stddev: {data_obj.stddev}')
            # data_obj.color_stddev = data_obj.color
            # data_obj.color_stddev_text = self.linecolor

    def _prepare_profile(self, data_obj):
        mask = plant.isvalid(data_obj.data)
        isfinite_matrix = np.sum(mask,
                                 axis=self.profile_axis, dtype=np.byte)
        if np.sum(isfinite_matrix) == 0:
            # data_obj.data = np.nan
            data_obj.data = isfinite_matrix * np.nan
            return

        '''
        data_obj.data[np.where(np.logical_not(mask))] = 0  # np.nan
        data_obj.data = np.sum(data_obj.data,
                               axis=self.profile_axis,
                               dtype=np.float32)
        # mask = np.asarray(mask, dtype=float)
        n_elements = np.nansum(mask, axis=self.profile_axis,
                               dtype=np.float64)
        for i in range(len(data_obj.data)):
            if n_elements[i] != 0:
                data_obj.data[i] = data_obj.data[i] / float(n_elements[i])
            else:
                data_obj.data[i] = np.nan
        data_obj.data = np.asarray(data_obj.data, dtype=np.float)
        '''

        ind = np.where(np.logical_not(mask))
        # data_obj.data[ind] = np.nan
        data_obj.data = plant.insert_nan(data_obj.data, indexes=ind,
                                         out_null=np.nan)
        data_obj.dat = plant.shape_image(data_obj.data)
        if (self.profiles_use_func is None or
                self.profiles_use_func == 'mean' or
                self.profiles_use_func == 'nanmean'):
            use_func = np.nanmean
            self.print('profile function: nanmean')
        elif (self.profiles_use_func == 'std' or
                self.profiles_use_func == 'nanstd' or
                self.profiles_use_func == 'stddev' or
                self.profiles_use_func == 'nanstddev'):
            self.print('profile function: nanstd')
            use_func = np.nanstd
        else:
            self.print(f'ERROR function not recognized: '
                       f' {self.profiles_use_func}')
        data_obj.data = use_func(data_obj.data, axis=self.profile_axis)
        # print('*** data: ', data_obj.data)
        if data_obj.sum_normed:
            data_obj.data /= np.nansum(data_obj.data)
        elif data_obj.max_normed:
            data_obj.data /= np.nanmax(data_obj.data)
        elif data_obj.sum_normed_to_value:
            data_obj.data *= (data_obj.sum_normed_to_value /
                              np.nansum(data_obj.data))
        elif data_obj.max_normed_to_value:
            data_obj.data *= (data_obj.max_normed_to_value /
                              np.nanmax(data_obj.data))
        data_obj.data[np.where(plant.isnan(data_obj.data))] = np.nan

        if self.flag_coregister_profiles and self.input_number > 0:
            # self.print('************************')
            # self.print('*** coregistering profile:')
            # self.print('*** 1: ', data_obj.data)
            # self.print('*** 2: ', self.data[0].data)
            data_obj.data = plant.coregister_1d(data_obj.data,
                                                self.data[0].data,
                                                verbose=True)

        self.print('number of elements after averaging: ' +
                   str(len(data_obj.data)))
        min_temp = \
            np.asarray(np.where(isfinite_matrix)).item(0)

        profile_min, _ = self.get_plot_orientation(self.xmin,
                                                   self.ymin)
        profile_max, _ = self.get_plot_orientation(self.xmax,
                                                   self.ymax)

        if profile_min is None:
            data_obj.xmin = min_temp
        else:
            data_obj.xmin = profile_min
        self.print('min profile index: %d'
                   % data_obj.xmin)
        max_temp = \
            np.asarray(np.where(isfinite_matrix)).item(-1)
        if profile_max is None:
            data_obj.xmax = max_temp
        else:
            data_obj.xmax = profile_max
        self.print('max profile index: %d'
                   % data_obj.xmax)

    def _prepare_multiplot(self, data_obj):

        if data_obj.stddev is None and self.stddev:
            data = data_obj.data
            data_obj.stddev = np.nanstd(data)
            self.print('stddev: %f'
                       % data_obj.stddev)

        if (self.multiplot_line_end_name is not None and
                self.plot_last_line is not None and
                self.multiplot_line_end_name !=
                self.plot_y_list[self.plot_last_line]):
            this_multiplot_line = self.plot_y_list[self.plot_last_line]
            # plant.debug('updating line %s to %s'
            #            % (this_multiplot_line, self.multiplot_line_end_name))
            self.plot_y_list[self.plot_last_line] = \
                self.multiplot_line_end_name
            new_pair = []
            for pair in self.plot_xy_list:
                if pair[0] == self.multiplot_line_end_name:
                    new_pair.append((self.multiplot_line_end_name, pair[1]))
                else:
                    new_pair.append(pair)
            self.multiplot_line_end = True
            '''
            for im in range(self.input_number):
                if self.data[im].name is None:
            '''

        if (self.multiplot_column_end_name is not None and
                self.plot_last_column is not None and
                self.multiplot_column_end_name !=
                self.plot_y_list[self.plot_last_column]):
            this_multiplot_column = self.plot_y_list[self.plot_last_column]
            # plant.debug('updating column %s to %s'
            #            % (this_multiplot_column,
            #               self.multiplot_column_end_name))
            self.plot_y_list[self.plot_last_column] = \
                self.multiplot_column_end_name
            new_pair = []
            for pair in self.plot_xy_list:
                if pair[0] == self.multiplot_column_end_name:
                    new_pair.append((self.multiplot_column_end_name, pair[1]))
                else:
                    new_pair.append(pair)
            self.multiplot_column_end = True
            # self.multiplot_line_end_previous = False
            # self.multiplot_column_end_previous = False

        if (self.plot_last_column is not None and
                self.plot_last_line is not None and
                (self.multiplot_column is not None or
                 self.multiplot_line is not None)):
            if self.multiplot_column is not None:
                this_multiplot_column = self.multiplot_column
            else:
                this_multiplot_column = \
                    self.plot_x_list[self.plot_last_column]
            if self.multiplot_line is not None:
                # plant.debug('111')
                this_multiplot_line = self.multiplot_line
            elif self.multiplot_line_end_previous:
                # plant.debug('222')
                this_multiplot_line = None
            else:
                # plant.debug('333')
                this_multiplot_line = \
                    self.plot_y_list[self.plot_last_line]
            pair = (this_multiplot_line,
                    this_multiplot_column)
            if pair not in self.plot_xy_list:
                if self.multiplot_column is None:
                    self.multiplot_column = this_multiplot_column
                if self.multiplot_line is None:
                    self.multiplot_line = this_multiplot_line

        # plant.debug(self.multiplot_column, self.multiplot_line)

        # set multiplot column
        flag_add_column = False
        # column set by user
        if self.multiplot_column is not None:
            this_multiplot_column = self.multiplot_column
            # plant.debug('here column 1')
        # first execution (use name)
        elif (len(self.plot_x_list) == 0 and
              not self.flag_multiplot_use_name):
            flag_add_column = True
            # this_multiplot_column = 0
        # first execution
        elif len(self.plot_x_list) == 0:
            this_multiplot_column = data_obj.filename
            # plant.debug('here column 2')
        # line order and reached line limit or multiplot column end
        elif (self.multiplot_line_order and
              (self.multiplot_nlines is not None and
               self.plot_last_line + 1 >= self.multiplot_nlines) or
              self.multiplot_column_end_previous):
            flag_add_column = True
            # plant.debug('here column 3')
        # keep column
        elif self.multiplot_line_order:
            this_multiplot_column = None
        # n columns already reached max n_columns or line end
        elif ((self.multiplot_ncolumns is not None and
               self.plot_last_column + 1 >= self.multiplot_ncolumns) or
              self.multiplot_line_end_previous):
            this_multiplot_column = self.plot_x_list[0]
            # plant.debug('here column 4')

        elif self.plot_last_column + 1 >= len(self.plot_x_list):
            flag_add_column = True
            # plant.debug('here column 5')

        else:
            this_multiplot_column = \
                self.plot_x_list[self.plot_last_column + 1]
            # plant.debug('here column 6')

        if flag_add_column:
            if self.plot_last_column is None:
                last_multiplot_column = -1
            else:
                last_multiplot_column = \
                    self.plot_x_list[self.plot_last_column]
            if self.flag_multiplot_use_name:
                this_multiplot_column = data_obj.filename
            elif (plant.isnumeric(last_multiplot_column) and
                    float(last_multiplot_column)//1 ==
                    float(last_multiplot_column)):
                # elif plant.isnumeric(last_multiplot_column):
                this_multiplot_column = int(last_multiplot_column) + 1
            else:
                this_multiplot_column = 'C%d' % (len(self.plot_x_list)+1)

        # set multiplot line
        flag_add_line = False
        # line set by user
        if self.multiplot_line is not None:
            # plant.debug('here line 1')
            # plant.debug(self.multiplot_line)
            this_multiplot_line = self.multiplot_line
        # first execution
        elif len(self.plot_y_list) == 0:
            flag_add_line = True
            # plant.debug('here line 2')
            # this_multiplot_line = ' 0'
        # not line order and reached column limit
        elif (not self.multiplot_line_order and
              (self.multiplot_ncolumns is not None and
               self.plot_last_column+1 >= self.multiplot_ncolumns) or
              self.multiplot_line_end_previous):
            # plant.debug('here line 3')
            flag_add_line = True
        # keep line
        elif not self.multiplot_line_order:
            # plant.debug('here line 4')
            this_multiplot_line = None
        # n lines already reached max n_lines
        elif ((self.multiplot_nlines is not None and
              self.plot_last_line+1 >= self.multiplot_nlines) or
              self.multiplot_column_end_previous):
            # plant.debug('here line 5')
            this_multiplot_line = self.plot_y_list[0]
        elif self.plot_last_line + 1 >= len(self.plot_y_list):
            # plant.debug('here line 6')
            flag_add_line = True
        else:
            # plant.debug('here line 7')
            this_multiplot_line = \
                self.plot_y_list[self.plot_last_line + 1]
        if flag_add_line:
            # last_multiplot_line = \
            #     self.plot_y_list[self.plot_last_line]
            # if (plant.isnumeric(last_multiplot_line) and
            #         float(last_multiplot_line)//1 ==
            #         float(last_multiplot_line)):
            #     this_multiplot_line = int(last_multiplot_line) + 1
            # # elif plant.isnumeric(last_multiplot_line):
            # #    this_multiplot_line = float(last_multiplot_line) + 1
            # else:
            this_multiplot_line = 'Line %d' % (len(self.plot_y_list)+1)

        # plant.debug(this_multiplot_column, this_multiplot_line)
        # add column
        if this_multiplot_column is not None:
            # check if it already exists
            if this_multiplot_column not in self.plot_x_list:
                # plant.debug('column doest not exist in ')
                # plant.debug(self.plot_x_list)
                self.plot_x_list.append(this_multiplot_column)
            # else:
            #    # plant.debug('column %s already exists'
            # % this_multiplot_column)
            # points to the element list
            self.plot_current_column = \
                self.plot_x_list.index(this_multiplot_column)
        else:
            # keep last column
            # plant.debug('keep last column')
            self.plot_current_column = self.plot_last_column

        self.plot_last_column = self.plot_current_column
        data_obj.column = self.plot_current_column
        self.print('multiplot column: %s'
                   % str(data_obj.column))

        # add line
        if this_multiplot_line is not None:
            # check if it already exists
            if this_multiplot_line not in self.plot_y_list:
                self.plot_y_list.append(this_multiplot_line)
                # self.plot_y_color_list.append(None)
            # points to the element list
            self.plot_current_line = \
                self.plot_y_list.index(this_multiplot_line)
        else:
            # keep last line
            self.plot_current_line = self.plot_last_line

        # if (self.color is not None and
        #         self.plot_y_color_list[self.plot_current_line] is None):
        #     self.plot_y_color_list[self.plot_current_line] = self.color

        self.plot_last_line = self.plot_current_line
        data_obj.line = self.plot_current_line
        self.print('multiplot line: %d' % data_obj.line)

        pair = (this_multiplot_line,
                this_multiplot_column)
        if pair not in self.plot_xy_list:
            self.plot_xy_list.append(pair)

        self.multiplot_line_end_previous = self.multiplot_line_end
        self.multiplot_column_end_previous = self.multiplot_column_end

    def _update_names(self, image_list):
        # name(s)
        if self.max_name_size is not None:
            max_name_size = self.max_name_size
        elif self.type == GENERIC_PLOT or self.type == DENSITY_PLOT:
            max_name_size = None
        else:
            max_name_size = MAX_NAME_SIZE

        input_names = [self.data[im].name
                       for im in image_list]
        # add here: if .names are the same, check .input_names
        # names_freq = [len(list(group))
        # print('*** input_names: ', input_names)
        # for key, group in groupby(input_names):
        #     # print('*** key:', key, 'group:', group,
        #     #       'len:', len(list(group)))
        input_names_list = [key for key, group in groupby(input_names)
                            if len(list(group)) > 1]
        # print('*** input_names_list: ', input_names_list)
        if len(input_names_list) >= 1:
            # print('*** repeated names')
            image_names = [self.data[im].image_name
                           for im in image_list]
            image_names_list = [key for key, group in groupby(image_names)
                                if len(list(group)) > 1]
            # print('*** image_names_list: ', image_names_list)
            if len(image_names_list) >= 1:
                # print('*** repeated names 2')

                image_input_names = []
                for im in image_list:
                    if self.data[im].name is not None:
                        image_input_names.append(
                            self.data[im].image_name+'(' +
                            self.data[im].name+')')
                    else:
                        image_input_names.append(
                            self.data[im].image_name+'(band: ' +
                            str(self.data[im].band)+')')
                image_input_names_list = \
                    [key for key, group in groupby(image_input_names)
                     if len(list(group)) > 1]
            for im in image_list:
                if (self.data[im].user_defined_name is not None and
                    (self.data[im].name ==
                     self.data[im].user_defined_name)):
                    # print('*** 1')
                    continue
                if self.data[im].name not in input_names_list:
                    # print('*** 2')
                    continue
                if self.data[im].image_name not in image_names_list:
                    self.data[im].name = self.data[im].image_name
                    # print('*** 3')
                    continue
                name_test = (self.data[im].image_name+'(' +
                             self.data[im].name+')')
                if name_test in image_input_names_list:
                    self.data[im].name = name_test
                    # print('*** 4')
                    continue
                # print('*** 5')
                self.data[im].name = self.data[im].filename
        #    print('*** key: ', key, ' group:', group,
        #          'len:', len(list(group)))
        #    if len(list(group)) > 1:
        #        continue
        #    print('*** ok')
        # for im in range(self.input_number):
        #     if self.data[im].name is None

        input_names_none = [path.basename(self.data[im].filename)
                            for im in image_list
                            if self.data[im].name is None]

        if len(input_names_none) != 0:
            new_input_names = plant.get_image_names(
                input_names_none,
                max_name_size=max_name_size,
                update_names_larger_than=UPDATE_NAMES_LARGER_THAN)
            im_none_count = 0
            for im in image_list:
                if self.data[im].name is not None:
                    continue
                self.data[im].name = new_input_names[im_none_count]
                im_none_count += 1
        # print('*** new_input_names: ', new_input_names)

    def _update_min_max_nbins(self, image_list):
        flag_extrapolate_min = False
        if self.default_min is None and self.type != MULTIPLOT:
            if self.type == PROFILE:
                flag_extrapolate_min = True



            self.default_min = []
            for i in image_list:
                if (self.data[i].vmin is not None and
                        hasattr(self.data[i].vmin, '__getitem__')):
                    self.default_min.extend(self.data[i].vmin)
                elif self.data[i].vmin is not None:
                    self.default_min.append(self.data[i].vmin)
                else:
                    self.default_min.append(np.nan)

            if any(plant.isvalid(self.default_min)):
                self.default_min = [plant.nanmin(self.default_min)]
            else:
                self.default_min = np.nan

        flag_extrapolate_max = False
        if self.default_max is None and self.type != MULTIPLOT:
            if self.type == PROFILE:
                flag_extrapolate_max = True
            # self.default_max = [self.data[i].vmax
            #                    if self.data[i].vmax is not None else np.nan
            #                    for i in image_list]







            self.default_max = []
            for i in image_list:
                if (self.data[i].vmax is not None and
                        hasattr(self.data[i].vmax, '__getitem__')):
                    self.default_max.extend(self.data[i].vmax)
                elif self.data[i].vmax is not None:
                    self.default_max.append(self.data[i].vmax)
                else:
                    self.default_max.append(np.nan)




            if any(plant.isvalid(self.default_max)):
                self.default_max = [plant.nanmax(self.default_max)]
            else:
                self.default_max = np.nan

        if flag_extrapolate_min or flag_extrapolate_max:
            data_range = (np.nanmax(self.default_max) -
                          np.nanmin(self.default_min))
            if flag_extrapolate_min:
                self.default_min = [np.nanmin(self.default_min) -
                                    0.25 * data_range]
            if flag_extrapolate_max:
                self.default_max = [np.nanmax(self.default_max) +
                                    0.25 * data_range]

        if self.default_nbins is None:
            
            self.default_nbins = []
            for i in image_list:
                if (self.data[i].nbins is not None and
                        hasattr(self.data[i].nbins, '__getitem__')):
                    self.default_nbins.extend(self.data[i].nbins)
                elif self.data[i].nbins is not None:
                    self.default_nbins.append(self.data[i].nbins)
                else:
                    self.default_nbins.append(np.nan)

            # self.default_nbins = [self.data[i].nbins
            #                      if self.data[i].nbins is not None else np.nan
            #                      for i in image_list]
            if any(plant.isvalid(self.default_nbins)):
                self.default_nbins = [np.nanmax(self.default_nbins)]
            else:
                self.default_nbins = np.nan

    def show(self):
        if self.input_number == 0:
            # get_parser(app_name=APP_NAME_MDX).print_help()
            return

        self.n_plots = self.input_number
        self.flag_folium |= self.flag_html

        # first image as x:
        if (self.type == MULTIPLOT and
            (self.input_number == 1 or
             (self.input_number == 2 and
              self.plot_color_mode == 1))):
            self.first_image_as_x = True
        elif self.type == DENSITY_PLOT or self.scatterplot or self.trendplot:
            self.first_image_as_x = True

        # update names
        image_list = range(int(self.first_image_as_x), self.input_number)
        self._update_names(image_list)

        # update min, max, and nbins
        self._update_min_max_nbins(image_list)

        # set colorbar orientation
        if self.colorbar_orientation is None:
            self.colorbar_orientation = 'vertical'

        # assert that inputs have the same shape
        if self.type == GENERIC_PLOT or self.type == DENSITY_PLOT:
            self.check_data_shapes()

        self._populate_decoration_parameters()
        self.print(f'## {self.text.lower()} options:')

        # plot
        with plant.PlantIndent():
            if self.type == HISTOGRAM:
                self.plot_hist_loop()
            elif self.type == PROFILE:
                self.plot_profile_loop()
            elif self.type == PRINT:
                self.func_print_text()
                return self
            elif self.type == IMAGEVIEWER:
                self.plot_imshow()
                if not self.no_show:
                    self.plt.show()
                return self
            elif self.type == MULTIPLOT:
                self.plot_multiplot()
            elif self.type == DENSITY_PLOT:
                self.plot_hist2d_loop()
            elif self.type == GEOLOCATION:
                self.plot_geolocation()
            elif self.type == GENERIC_PLOT:
                self.plot_generic()

        # label_y
        if self.label_y is None:
            if self.type == HISTOGRAM:
                self.label_y = 'Histogram count'
            elif self.type == PROFILE:
                self.label_y = self.profile_direction + ' profile'
            elif (self.type == DENSITY_PLOT):
                self.label_y = self.data[1].name
        if (self.label_y is not None and
                len(self.label_y) > MAX_LABEL_SIZE):
            self.label_y = self.label_y[0:MAX_LABEL_SIZE]

        # label_x
        if (self.label_x is None and
                (self.type == DENSITY_PLOT or
                 self.type == GENERIC_PLOT)):
            self.label_x = self.data[0].name
        elif (self.label_x is None and
              (self.type == PROFILE)):
            self.label_x = (self.profile_direction +
                            ' - position index')
        if (self.label_x is not None and
                len(self.label_x) > MAX_LABEL_SIZE):
            self.label_x = self.label_x[0:MAX_LABEL_SIZE]

        xlabel, ylabel = self.get_plot_orientation(self.plt.xlabel,
                                                   self.plt.ylabel)

        if self.label_x is not None:
            self.print(f'label-X: {self.label_x}')
            xlabel(self.label_x, fontsize=self.fontsize)
        if self.label_y is not None:
            self.print(f'label-Y: {self.label_y}')
            ylabel(self.label_y, fontsize=self.fontsize)

        # label_z
        if self.generic_plot_3d:
            if self.label_z is not None:
                if len(self.label_z) > MAX_LABEL_SIZE:
                    self.label_z = self.label_z[0:MAX_LABEL_SIZE]
                self.print(f'label-Z: {self.label_z}')
                self.ax.set_zlabel(self.label_z,
                                   fontsize=self.fontsize)

        # title
        if (self.title is None and
                self.type == DENSITY_PLOT):
            self.title = self.type
        elif (self.title is None and
              self.type == MULTIPLOT and
              self.median):
            self.title = 'median'
        elif (self.title is None and
              self.type == MULTIPLOT and
              self.mode):
            self.title = 'mode'
        elif (self.title is None and
              self.type == MULTIPLOT and
              self.mean):
            self.title = 'mean'
        elif (self.title is None and
              self.type == MULTIPLOT and
              self.stddev):
            self.title = 'stddev'
        elif self.title is None:
            self.title = self.type
        if len(self.title) > MAX_TITLE_SIZE:
            self.title = self.title[0:MAX_TITLE_SIZE]
        if not self.no_title:
            self.print(f'title: {self.title}')
            self.plt.title(self.title, fontsize=self.fontsize)

        # grid
        self.plt.grid(self.grid)

        # label_x orientation
        if self.xlabel_vertical:
            self.plt.xticks(rotation='vertical')
        elif self.xlabel_rotation is not None:
            if (self.xlabel_rotation == 'horizontal' or
                (plant.isnumeric(self.xlabel_rotation) and
                 float(self.xlabel_rotation) < 20)):
                ha = 'center'
            else:
                ha = 'right'
            self.plt.xticks(rotation=self.xlabel_rotation, ha=ha)
        else:
            xticks_rotation = 'horizontal'
            ha = 'center'
            xticklabel_list = \
                [x.get_text() for x in self.ax.get_xticklabels()]
            if len(xticklabel_list) > 0:
                max_len = max([len(str(x_str))
                               for x_str in xticklabel_list])
                if max_len > 5:
                    xticks_rotation = '45'
                    ha = 'right'
            self.plt.xticks(rotation=xticks_rotation, ha=ha)

        # support lines
        self.draw_support_lines()

        # colorbar
        if self.colorbar:
            if self.mappable is not None:
                colorbar_min, colorbar_max = self.mappable.get_clim()
                if plant.isnan(self.colorbar_range[0]):
                    self.colorbar_range[0] = colorbar_min
                if plant.isnan(self.colorbar_range[1]):
                    self.colorbar_range[1] = colorbar_max
                self.mappable.set_clim(self.colorbar_range)
                cbar = self.plt.colorbar(
                    self.mappable,
                    orientation=self.colorbar_orientation)
            else:
                cbar = self.plt.colorbar(
                    orientation=self.colorbar_orientation)

            if self.colorbar_label is not None:
                cbar.ax.set_ylabel(self.colorbar_label,
                                   fontsize=self.fontsize)
            temp = cbar.ax.get_yticklabels()
            cbar.ax.set_yticklabels(temp,
                                    fontsize=self.fontsize)

        # legend
        if (self.legend is None and
                (((self.type == HISTOGRAM or
                   self.type == PROFILE) and
                  self.n_plots > 1) or
                 (self.type == MULTIPLOT and
                  self.multiplot_nlines > 1))):
            self.legend = True

        if self.legend:
            if len(self.plot_names_list) == 0:
                self.plot_names_list = []
                for i in range(self.n_plots):
                    self.plot_names_list.append(self.data[i].name)
            if self.legend_location is None:
                self.legend_location = 'best'

            if len(self.plot_handles_list) == 0:
                self.plt.legend(self.plot_names_list,
                                loc=self.legend_location,
                                bbox_to_anchor=self.legend_bbox,
                                fontsize=self.legend_fontsize)
            else:
                self.plt.legend(handles=self.plot_handles_list,
                                labels=self.plot_names_list,
                                loc=self.legend_location,
                                bbox_to_anchor=self.legend_bbox,
                                fontsize=self.legend_fontsize)

        # fix axis decimal places
        if (FLAG_FIX_AXIS_DECIMAL_PLACES and
            (self.decimal_places is not None or
                self.n_significant_figures is not None)):
            for c, functions in enumerate([[self.ax.xaxis,
                                            self.ax.get_xticklabels,
                                            self.ax.set_xticklabels],
                                           [self.ax.yaxis,
                                            self.ax.get_yticklabels,
                                            self.ax.set_yticklabels]]):
                axis, g_ax_l, s_ax_l = functions
                labels = [x.get_text() for x in g_ax_l()]
                flag_all_empty = all([len(x) == 0 for x in labels])
                flag_all_numeric = all([plant.isnumeric(x)
                                        for x in labels])
                if flag_all_empty or flag_all_numeric:
                    if self.decimal_places is not None:
                        axis.set_major_formatter(
                            FormatStrFormatter(f'%.{self.decimal_places}f'))
                    continue
                for i, label in enumerate(labels):
                    if not plant.isnumeric(label):
                        continue
                    labels[i] = plant.format_number(
                        label,
                        decimal_places=self.decimal_places,
                        sigfigs=self.n_significant_figures)
                if c == 0 and self.xlabel_rotation is not None:
                    if (self.xlabel_rotation == 'horizontal' or
                        (plant.isnumeric(self.xlabel_rotation) and
                         float(self.xlabel_rotation) < 20)):
                        ha = 'center'
                    else:
                        ha = 'right'
                    s_ax_l(labels, rotation=self.xlabel_rotation, ha=ha)
                else:
                    s_ax_l(labels)

        # update axis limits
        x_min, x_max = self.ax.get_xlim()
        flag_invert_x = x_max < x_min
        if flag_invert_x:
            self.xmin, self.xmax = self.xmax, self.xmin
        if plant.isvalid(self.xmin):
            x_min = self.xmin
        if plant.isvalid(self.xmax):
            x_max = self.xmax
        if plant.isvalid(self.xmin) or plant.isvalid(self.xmax):
            self.ax.set_xlim([x_min, x_max])

        y_min, y_max = self.ax.get_ylim()
        flag_invert_y = y_max < y_min
        if flag_invert_y:
            self.ymin, self.ymax = self.ymax, self.ymin
        if plant.isvalid(self.ymin):
            y_min = self.ymin
        if plant.isvalid(self.ymax):
            y_max = self.ymax
        if plant.isvalid(self.ymin) or plant.isvalid(self.ymax):
            self.ax.set_ylim([y_min, y_max])
        if self.invert_x_axis:
            self.plt.gca().invert_xaxis()
            print('inverted X-axis')
        if self.invert_y_axis:
            self.plt.gca().invert_yaxis()
            print('inverted Y-axis')

        # set fontproperties
        for item in ([self.ax.title,
                      self.ax.xaxis.label,
                      self.ax.yaxis.label] +
                     self.ax.get_xticklabels() +
                     self.ax.get_yticklabels()):
            if self.fontproperties is not None:
                item.set_fontproperties(self.fontproperties)

        # folim adjust
        if not self.flag_folium:
            self.plt.subplots_adjust()

        # update window size
        if (self.plot_size_x is not None or
                self.plot_size_y is not None):
            try:
                mngr = self.plt.get_current_fig_manager()
                mngr.window.setGeometry(0, 0,
                                        self.plot_size_x,
                                        self.plot_size_y)
                print(f'window size set to (x, y): {self.plot_size_x}, '
                      f'{self.plot_size_y}')
            except:
                print('ERROR setting window size to (x, y):'
                      f' {self.plot_size_x}, {self.plot_size_y}')
                return

        # apply tight layout
        if self.tight_layout is not False:
            print(f'applying tight-layout..')
            try:
                self.plt.tight_layout()
                flag_error = False
            except:
                flag_error = True
            if flag_error:
                try:
                    import matplotlib.gridspec as gridspec
                    gs1 = gridspec.GridSpec(1, 1)
                    gs1.tight_layout(self.fig)
                except:
                    error_message = plant.get_error_message()
                    print(f'WARNING there was a problem applying'
                          f' tight_layout: {error_message}')

        # save output (figure)
        if self.output_file:
            plant.save_fig(self.output_file,
                           plot=self.plt,
                           verbose=self.verbose,
                           crop_output=self.crop_output,
                           force=self.force)

        # show
        if not self.no_show:
            self.plt.show()

        return self

    def _set_matplotlib_parameters(self):
        # if self.fontfamily is None:
        #    self.fontfamily = 'Times New Roman'
        # use_tex = 1
        if self.tex:
            print('INFO TEX mode')
            mpl.rc('text', usetex=True)
            # mpl.rc('font', family=SERIF_FAMILY_NAME)
            mpl.rcParams["pgf.texsystem"] = 'pdflatex'
            mpl.rcParams['text.usetex'] = True
            mpl.rcParams['text.latex.unicode'] = True
            '''
            if (self.fontfamily is not None and
                    'times' in self.fontfamily.lower()):
                mpl.rc('font', family=SERIF_FAMILY_NAME,
                       serif=self.fontfamily)
            elif self.fontfamily is not None:
                mpl.rc('font', family=self.fontfamily)
            '''
            mpl.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

        # mpl.rcParams.update({'font.size': self.fontsize})
        # mpl.rcParams.update({'legend.fontsize': self.fontsize})
        # mpl.rc('legend', fontsize=self.fontsize)
        mpl.rc('xtick', labelsize=self.fontsize)
        mpl.rc('ytick', labelsize=self.fontsize)

        # mpl.rcParams['axes.facecolor'] = self.background_color
        mpl.rcParams['savefig.facecolor'] = self.facecolor
        if self.dpi is not None:
            mpl.rcParams['figure.dpi'] = self.dpi
            mpl.rcParams['savefig.dpi'] = self.dpi
        self.fontproperties = FontProperties(fname=self.fontfile)
        # self.fontproperties.fontext = 'afm'
        '''
        if (self.fontfamily is not None and
                False and
                'times' in self.fontfamily.lower()):
            # print('*** fontfamily: serif')
            self.fontproperties.set_family(SERIF_FAMILY_NAME)
            # self.fontproperties.set_name(self.fontfamily)
            mpl.rcParams['font.family'] = SERIF_FAMILY_NAME
            mpl.rcParams['font.serif'] = self.fontfamily
            # print('*** keys: ', mpl.rcParams.keys())
            # mpl.rcParams['axes.font.family'] = SERIF_FAMILY_NAME
            # mpl.rcParams['axes.font.serif'] = self.fontfamily
            mpl.rc('font', family=SERIF_FAMILY_NAME,
                   serif=self.fontfamily)
        if
            # print(f'*** fontfamily: {self.fontfamily}')
            # self.fontproperties.findfont(fontext='afm')
            # mpl.rcParams['font.family'] = SERIF_FAMILY_NAME
            # mpl.rcParams['axes.labelfamily'] = self.fontfamily
            # mpl.rcParams['axes.font.family'] = self.fontfamily
            # mpl.rc('axes.font.family'] = self.fontfamily
        # self.print(f'font name: {self.fontproperties.get_name()}')
        # self.print(f'font file: {self.fontproperties.get_name()}')
        # self.fontproperties = None
        #     mpl.rc('font', family=SERIF_FAMILY_NAME, serif='Times')
        # elif self.fontfamily is not None:
        #     mpl.rc('font', family=self.fontfamily)
        # if self.fontfamily is not None:
        # mpl.rcParams['font.family'] = self.fontfamily
        # if self.fontserif is not None:
        #     # mpl.rcParams['axes.font.family'] = self.fontfamily
        '''

        if self.fontfamily is not None:
            self.fontproperties.set_family(self.fontfamily)
            mpl.rcParams['font.family'] = self.fontfamily
            mpl.rc('font', family=self.fontfamily)
        if self.fontsize:
            mpl.rcParams['font.size'] = self.fontsize
            mpl.rcParams['axes.labelsize'] = self.fontsize
            mpl.rcParams['xtick.labelsize'] = self.fontsize
            mpl.rcParams['ytick.labelsize'] = self.fontsize
            if self.fontproperties is not None:
                self.fontproperties.set_size(self.fontsize)

        if self.fontcolor:
            mpl.rcParams['text.color'] = self.fontcolor
            # self.fontproperties.set_fontcolor(self.fontcolor)

        if self.fontcolor_axis:
            mpl.rcParams['axes.labelcolor'] = self.fontcolor_axis

        if self.fontweight is not None:
            if self.fontproperties is not None:
                self.fontproperties.set_weight(self.fontweight)
            mpl.rcParams["font.weight"] = self.fontweight
            mpl.rcParams['axes.titleweight'] = self.fontweight
            mpl.rcParams['axes.labelweight'] = self.fontweight

        '''
        mpl.rcParams['savefig.dpi'] = 300
        mpl.rcParams['font.family'] = 'sans-serif'
        mpl.rcParams['font.weight'] = 'light'
        '''

    def _populate_decoration_parameters(self):

        self.fig_kwargs = {}
        if (self.plot_size_x is not None or
                self.plot_size_y is not None):
            if self.plot_size_x is None:
                self.plot_size_x = (self.plot_size_y *
                                    plant.DEFAULT_PLOT_SIZE_X /
                                    plant.DEFAULT_PLOT_SIZE_Y)
            if self.plot_size_y is None:
                self.plot_size_y = (self.plot_size_x *
                                    plant.DEFAULT_PLOT_SIZE_Y /
                                    plant.DEFAULT_PLOT_SIZE_X)
            figsize = (self.plot_size_x, self.plot_size_y)
        else:
            figsize = (8.0, 6.0)
            # mpl.rcParams['figure.figsize'] = [8.0, 6.0]
        # print('*** figsize:', figsize)
        self.fig_kwargs['figsize'] = figsize
        mpl.rcParams['figure.figsize'] = figsize
        self.fig_kwargs['facecolor'] = self.facecolor
        # self.fig_kwargs['figure.facecolor'] = self.facecolor
        self.fig_kwargs['dpi'] = self.dpi
        if self.figure_number is not None:
            self.fig_kwargs['num'] = self.figure_number
        else:
            self.fig_kwargs['num'] = self.text
        # self.fig_kwargs['title'] = self.text

        if (self.type != IMAGEVIEWER and
                self.type != PRINT):
            # facecolor=self.facecolor,
            self.fig = self.plt.figure(**self.fig_kwargs)

            # self.fig.set_size_inches(38.5, 30.5)
            self.ax = self.plt.gca()
            # ######## self.ax.images = []
            # self.ax.ticklabel_format(useOffset=False)

        if (self.dark_theme and
                self.plot_style is None and
                self.type != DENSITY_PLOT):
            self.plot_style = 'dark_background'

        if (self.background_color is None and
                self.type == DENSITY_PLOT):
            self.background_color = None
        elif (self.background_color is None and
                self.type != IMAGEVIEWER and
                self.plot_style != 'dark_background'):
            self.background_color = plant.PLOT_BACKGROUND_COLOR
        elif self.background_color is None and self.type != IMAGEVIEWER:
            self.background_color = plant.PLOT_BACKGROUND_COLOR_DARK
        elif (self.background_color is None and
              self.plot_style != 'dark_background'):
            self.background_color = plant.IMSHOW_BACKGROUND_COLOR
        elif self.background_color is None:
            self.background_color = plant.IMSHOW_BACKGROUND_COLOR_DARK

        if (self.facecolor is None and
                self.plot_style != 'dark_background'):
            self.facecolor = plant.PLOT_BACKGROUND_COLOR
        elif self.facecolor is None:
            self.facecolor = plant.PLOT_BACKGROUND_COLOR_DARK

        if self.type != PRINT:
            self._set_matplotlib_parameters()
            # self.plt.style.use(['dark_background', 'presentation'])
        for i in range(0, self.input_number):
            # print('*** populate_decoration', i)
            data_obj = self.data[i]

            if (data_obj.linecolor is None and
                    (self.plot_style == 'dark_background' or
                     self.dark_theme)):
                data_obj.linecolor = 'white'
                '''
        if (self.linecolor is None and
                (self.plot_style == 'dark_background' or
                 self.dark_theme)):
            self.linecolor = 'white'
        elif (self.linecolor is None and
              self.type != DENSITY_PLOT):
            self.linecolor = 'black'
                '''
            # elif (self.linecolor is None and
            #       self.type != DENSITY_PLOT):
            #     self.linecolor = 'black'

            if (self.plot_style is not None and
                    self.plot_style == PREDEFINED_STYLES[0]):
                if data_obj.alpha is None:
                    data_obj.alpha = 0.4
                # data_obj.edgecolor = None
                if data_obj.markersize is None:
                    data_obj.markersize = 30
                if data_obj.fontsize is None:
                    data_obj.fontsize = 10
            elif self.plot_style is not None:
                style.use(self.plot_style)

            # cmap
            if (data_obj.cmap is not None and
                    data_obj.cmap == 'red'):
                data_obj.cmap = 'Reds'
            elif (data_obj.cmap is not None and
                  data_obj.cmap == 'green'):
                data_obj.cmap = 'Greens'
            elif (data_obj.cmap is not None and
                  data_obj.cmap == 'blue'):
                data_obj.cmap = 'Blues'
                # data_obj.cmap = self.cmap
                '''
                elif (data_obj.cmap is not None and
                    len(self.cmap) > self.input_number):
                data_obj.cmap = self.cmap[self.input_number]
                elif (self.cmap is not None and
                    len(self.cmap) == 1):
                data_obj.cmap = self.cmap[0]
                '''
            elif ((data_obj.cmap is None and
                   self.type == IMAGEVIEWER and
                   not self.hillshade) or
                  self.gray_scale):
                data_obj.cmap = 'gray'
                # self.cmap = data_obj.cmap
            elif (data_obj.cmap is None and
                  self.type == IMAGEVIEWER):
                data_obj.cmap = 'terrain'
                # self.cmap = data_obj.cmap
            elif (data_obj.cmap is None and
                  self.type == DENSITY_PLOT and i > 1):
                data_obj.cmap = plant.get_cmap_display(
                    max(0, i-1),
                    dark_theme=self.dark_theme,
                    cmap_vect=self.cmap)
            # else:
            #     data_obj.cmap = None

            # color
            # if data_obj.color is None and self.type == MULTIPLOT:
            #    data_obj.color = 'red'

            # print('*** data_obj.color: ', data_obj.color)

            # color_list_number = int(self.type == GEOLOCATION)

            color_orig = data_obj.color

            if (data_obj.color is None and
                    self.type == GENERIC_PLOT and
                    self.plot_color_mode != 0):
                data_obj.color = 'gray'
            elif (data_obj.color is None and
                  self.type != MULTIPLOT):
                data_obj.color = plant.get_color_display(
                    i-int(self.first_image_as_x),
                    flag_pol_color=self.flag_pol_color,
                    # total=(self.input_number -
                    #       int(self.first_image_as_x)),
                    color_vect=data_obj.color,
                    cmap=data_obj.cmap,
                    # color_list_number=color_list_number)
                    flag_line_or_point=(self.type != GEOLOCATION and
                                        self.generic_plot))

            # edgecolor
            if data_obj.edgecolor is None:
                color = data_obj.color
                data_obj.edgecolor = plant.get_edgecolor(color)

            # alpha
            if data_obj.alpha is None:
                data_obj.alpha = 0.7 if data_obj.cmap is None else 0.9

            # if self.cmap is not None:
            #     self.print('cmap: ' + str(self.cmap))
            if (data_obj.hatch is None and
                    self.type != MULTIPLOT and
                (self.flag_hatch or
                 (self.gray_scale and self.flag_hatch is not False))):
                data_obj.hatch = plant.HATCH_LIST[
                    i % len(plant.HATCH_LIST)]

            # linestyle
            # data_obj.linestyle = LINESTYLE_LIST
            if (data_obj.linestyle is None and
                    self.gray_scale and
                    self.change_linestyle is not False):
                data_obj.linestyle = plant.LINESTYLE_LIST[
                    i % len(plant.LINESTYLE_LIST)]
            elif (data_obj.linestyle is None and
                  self.gray_scale):
                data_obj.linestyle = 'solid'
            elif data_obj.linestyle is None:
                data_obj.linestyle = LINESTYLE_DEFAULT
            elif data_obj.linestyle_stats is None:
                data_obj.linestyle_stats = LINESTYLE_STATS_DEFAULT
            if data_obj.linecolor_stats is None and self.dark_theme:
                data_obj.linecolor_stats = LINECOLOR_STATS_DEFAULT_DARK
            elif data_obj.linecolor_stats is None:
                data_obj.linecolor_stats = LINECOLOR_STATS_DEFAULT

            # print('*** data_obj.color: ', data_obj.color)
            # print('*** data_obj.linecolor: ', data_obj.linecolor)

            # linecolor
            if (data_obj.linecolor is None and
                color_orig is not None and
                    (self.generic_plot or
                     self.multiplot or
                     self.type == PROFILE)):
                data_obj.linecolor = data_obj.color
            elif (data_obj.linecolor is None and
                    self.gray_scale and
                    self.change_linestyle is False and
                  (self.generic_plot or
                   self.multiplot or
                   self.type == PROFILE)):
                data_obj.linecolor = str([0.0, 0.875, 0.375, 0.625, 0.5, 0.75,
                                          0.25, 1.0, 0.125][np.mod(i, 9)])
            elif (data_obj.linecolor is None and
                    self.gray_scale and
                  (self.generic_plot or
                   self.multiplot or
                   self.type == PROFILE)):
                data_obj.linecolor = str(0.6*(
                    np.mod(i//len(plant.LINESTYLE_LIST), 2)))
            elif (data_obj.linecolor is None and
                  data_obj.color is not None and
                    (self.generic_plot or
                     self.multiplot or
                     self.type == PROFILE)):
                data_obj.linecolor = data_obj.color
            elif data_obj.linecolor is None:
                data_obj.linecolor = 'black'

            # print('*** data_obj.linecolor (final): ', data_obj.linecolor)

            if (self.type != DENSITY_PLOT and
                    self.type != IMAGEVIEWER):
                self.print(f'## input {i+1}:')
                with plant.PlantIndent():
                    if data_obj.color is not None:
                        self.print(f'color: {data_obj.color}')
                    if data_obj.cmap is not None:
                        self.print(f'cmap: {data_obj.cmap}')
                    if data_obj.linecolor is not None:
                        self.print(f'linecolor: {data_obj.linecolor}')
                    if data_obj.linestyle is not None:
                        self.print(f'linestyle: {data_obj.linestyle}')
                    if data_obj.linewidth is not None:
                        self.print(f'linewidth: {data_obj.linewidth}')
                    if data_obj.linecolor_stats is not None:
                        self.print(f'linecolor (stats):'
                                   f' {data_obj.linecolor_stats}')
                    if data_obj.linestyle_stats is not None:
                        self.print(f'linestyle (stats):'
                                   f' {data_obj.linestyle_stats}')
                    if data_obj.linewidth_stats is not None:
                        self.print(f'linewidth (stats):'
                                   f' {data_obj.linewidth_stats}')

            # if self.linestyle is None and self.type == GENERIC_PLOT:
            #    self.linestyle = '--'
            # elif self.linestyle_orig is None and self.type == PROFILE:
            #    ind = np.mod(self.input_number, 4)
            #    self.linestyle =( ['-', '--', ':', '-.'])[ind]
            # elif self.linestyle is None and self.type != MULTIPLOT:
            #     self.linestyle = 'solid'
            # elif self.linestyle is None:
            #     self.linestyle = '--'
            # self.print('linestyle: '+self.linestyle)
            # linestyle

    def draw_support_lines(self, flag_no_data=False,
                           plot=None, ax=None,
                           image_list=None):
        kwargs = locals()
        kwargs.pop('self')
        if plot is None:
            plot = self.plt
        if ax is None:
            ax = self.plt.gca()

        x_min, x_max = ax.get_xlim()
        y_min, y_max = ax.get_ylim()
        self._draw_support_lines(**kwargs)
        ax.set_xlim(x_min, x_max)
        ax.set_ylim(y_min, y_max)
        self._set_matplotlib_parameters()

    def _get_default_results_dict(self):
        ret_dict = {'r2': [],
                    'rmse': [],
                    'coeffs': []}
        return ret_dict

    def _draw_support_lines(self, flag_no_data=False,
                            plot=None, ax=None,
                            image_list=None):

        # TODO include:
        # self.color_stats
        # self.color_stats_text
        # self.fontsize_stats
        # self.alpha
        # self.fontfamily
        if plot is None:
            plot = self.plt
        if ax is None:
            ax = self.plt.gca()
        x_min, x_max = ax.get_xlim()
        y_min, y_max = min(ax.get_ylim()), max(ax.get_ylim())

        color_stats = self.color_stats
        # fontsize_stats = self.fontsize_stats
        alpha = self.alpha
        kwargs_font = {}
        kwargs_font['fontproperties'] = self.fontproperties
        kwargs_font['fontsize'] = self.fontsize_stats
        kwargs_font['fontfamily'] = self.fontfamily
        # fontfamily = self.fontfamily

        if image_list is None:
            image_list = range(int(self.first_image_as_x),
                               int(self.first_image_as_x)+self.n_plots)
        if self.dark_theme:
            linecolor_stats_default = LINECOLOR_STATS_DEFAULT_DARK
        else:
            linecolor_stats_default = LINECOLOR_STATS_DEFAULT

        linecolor_stats = linecolor_stats_default
        linestyle_stats = LINESTYLE_STATS_DEFAULT
        linewidth_stats = LINEWIDTH_STATS_DEFAULT
        color_stats_text = COLOR_STATS_TEXT_DEFAULT

        for im in image_list:
            if (linecolor_stats is None or
                    linecolor_stats == linecolor_stats_default):
                linecolor_stats = self.data[im].linecolor_stats
            if (linestyle_stats is None or
                    linestyle_stats == LINESTYLE_STATS_DEFAULT):
                linestyle_stats = self.data[im].linestyle_stats
            if (linewidth_stats is None or
                    linewidth_stats == LINEWIDTH_STATS_DEFAULT):
                linewidth_stats = self.data[im].linewidth_stats
            if (color_stats_text is None or
                    color_stats_text == COLOR_STATS_TEXT_DEFAULT):
                color_stats_text = self.data[im].color_stats_text

        '''
        self.print(f'Support line parameters: {image_list}')
        with plant.PlantIndent():
            self.print(f'color_stats: {color_stats}')
            self.print(f'fontsize_stats: {fontsize_stats}')
            self.print(f'alpha: {alpha}')
            self.print(f'fontfamily: {fontfamily}')
            self.print(f'linecolor_stats: {linecolor_stats}')
            self.print(f'linestyle_stats: {linestyle_stats}')
            self.print(f'linewidth_stats: {linewidth_stats}')
            self.print(f'color_stats_text: {color_stats_text}')
        '''
        # if ax is None:
        #     ax = plot.gca()

        if self.draw_hline:
            draw_hline_image = plant.read_image(self.draw_hline).image
            for y in draw_hline_image.ravel():
                plot.axhline(y=y,
                             color=linecolor_stats,
                             linewidth=linewidth_stats,
                             # linewidth=self.data[im+1].linewidth,
                             linestyle=linestyle_stats)

        if self.draw_vline:
            draw_vline_image = plant.read_image(self.draw_vline).image
            for x in draw_vline_image.ravel():
                # print('*** vline: ', x)
                plot.axvline(x=x,
                             color=linecolor_stats,
                             linewidth=linewidth_stats,
                             # linewidth=self.data[im+1].linewidth,
                             linestyle=linestyle_stats)

        if self.draw_line_with_points:
            draw_line_image = plant.read_image(
                self.draw_line_with_points).image
            for i in range(int(draw_line_image.shape[1]/2)):
                plot.plot(draw_line_image[i*2:i*2+2, 1],
                          draw_line_image[i*2:i*2+2, 0],
                          color=linecolor_stats,
                          linewidth=linewidth_stats,
                          linestyle=linestyle_stats)

        if self.draw_point:
            points = self.draw_point.split(plant.IMAGE_NAME_SEPARATOR)
            for point in points:
                point_splitted = point.split(
                    plant.ELEMENT_SEPARATOR)
                plot.plot(float(point_splitted[1]),
                          float(point_splitted[0]),
                          '.',
                          color=color_stats)
                if len(point_splitted) > 2:
                    if len(point_splitted) > 3:
                        text_offset_y = float(point_splitted[3])
                    else:
                        text_offset_y = 10
                    if len(point_splitted) > 4:
                        text_offset_x = float(point_splitted[4])
                    else:
                        text_offset_x = 10
                    kwargs_annotation = {}
                    if len(point_splitted) > 5:
                        kwargs_annotation['bbox'] = dict(
                            boxstyle="round",
                            alpha=alpha,
                            # pad=0.2,
                            edgecolor='none',
                            facecolor=point_splitted[5])
                    if len(point_splitted) > 6:
                        kwargs_annotation['arrowprops'] = dict(
                            arrowstyle=point_splitted[6],
                            # alpha=0.7,
                            # edgecolor='none',
                            edgecolor=linecolor_stats,
                            facecolor=color_stats)
                        # patchA=None,
                        # patchB=None,
                        # relpos=(0.2, 0.8),
                        # connectionstyle="arc3,rad=-0.1")
                    plot.annotate(point_splitted[2],
                                  xy=(float(point_splitted[1]),
                                      float(point_splitted[0])),
                                  xytext=(text_offset_x,
                                          text_offset_y),
                                  textcoords='offset pixels',
                                  # fontfamily=fontfamily,
                                  fontweight='bold',
                                  # fontsize='large',
                                  # linewidth=linewidth_stats,
                                  color=color_stats_text,
                                  # color=linecolor_stats,
                                  # fontsize=fontsize_stats,
                                  **kwargs_font,
                                  **kwargs_annotation)
                # linewidth=linewidth_stats,
                # linewidth=self.data[im+1].linewidth,
                # linestyle=linestyle_stats)

        if self.draw_text:
            text_lines = self.draw_text.split(plant.IMAGE_NAME_SEPARATOR)
            for text_line in text_lines:
                text_line_splitted = text_line.split(
                    plant.ELEMENT_SEPARATOR)
                kwargs_annotation = {}
                if len(text_line_splitted) > 3:
                    kwargs_annotation['bbox'] = dict(
                        boxstyle="round",
                        # pad=0.2,
                        alpha=alpha,
                        edgecolor='none',
                        facecolor=text_line_splitted[3])
                plot.annotate(text_line_splitted[0],
                              xy=(float(text_line_splitted[2]),
                                  float(text_line_splitted[1])),
                              # linewidth=linewidth_stats,
                              # fontfamily=fontfamily,
                              # fontname='',
                              # fontweight='bold',
                              # fontsize='large',
                              color=color_stats_text,
                              # fontsize=fontsize_stats,
                              **kwargs_font,
                              **kwargs_annotation)

        if self.draw_hspan:
            draw_hspan_obj = plant.read_image(self.draw_hspan)
            for i in range(draw_hspan_obj.length):
                plot.axhspan(draw_hspan_obj.image[i, 0],
                             draw_hspan_obj.image[i, 1],
                             linewidth=linewidth_stats,
                             color=color_stats,
                             alpha=alpha)

        if self.draw_vspan:
            draw_vspan_obj = plant.read_image(self.draw_vspan)
            for i in range(draw_vspan_obj.length):
                plot.axvspan(draw_vspan_obj.image[i, 0],
                             draw_vspan_obj.image[i, 1],
                             linewidth=linewidth_stats,
                             color=color_stats,
                             alpha=alpha)

        if self.draw_rectangle:
            draw_rectangle_obj = plant.read_image(self.draw_rectangle)
            # draw_rectangle_2_obj = plant.read_image(self.draw_rectangle[1])
            for i in range(0, draw_rectangle_obj.length):
                start_y = draw_rectangle_obj.image[i, 0]
                end_y = draw_rectangle_obj.image[i, 1] - 1 - start_y
                start_x = draw_rectangle_obj.image[i, 2]
                end_x = draw_rectangle_obj.image[i, 3] - 1 - start_x
                if plant.isnan(start_y):
                    start_y = y_min
                if plant.isnan(start_x):
                    start_x = x_min
                if plant.isnan(end_y):
                    end_y = y_max-start_y  # -linewidth/2
                if plant.isnan(end_x):
                    end_x = x_max-start_x  # -linewidth/2
                self.print(f'rectangle {i+1}: [{start_y}, {start_y+end_y},'
                           f' {start_x}, {start_x+end_x}]')
                color = 'r' if color_stats is None else color_stats
                rect = Rectangle((start_x, start_y),
                                 end_x, end_y,
                                 # linewidth=2,
                                 edgecolor=color,
                                 facecolor='none',
                                 linewidth=linewidth_stats,
                                 linestyle=linestyle_stats)
                ax.add_patch(rect)

        if (self.draw_line is None and
                self.draw_function is None and
                self.polyfit is None and
                not self.linefit and
                self.function_fit is None):
            return
        # step = (np.min([(self.data[0].vmax-self.data[0].vmin)/10000.0,
        #                (self.data[0].vmax-self.data[0].vmin) /
        #                self.data[0].nbins]))
        # x_vect_orig = np.linspace(self.data[0].vmin,
        #                          self.data[0].vmax,
        #                          10000)
        x_vect_orig = np.linspace(x_min,
                                  x_max,
                                  10000)
        # print('*** ', np.nanmax(x_vect_orig))
        # print('*** ', x_vect_orig)
        x_data = None
        if plant.get_dtype_name(self.data[0].data) == 'datetime':
            # if False:
            flag_matplotlib = False
            if not flag_no_data:
                x_data = plant.datetime_to_array(
                    self.data[0].data,
                    flag_matplotlib=flag_matplotlib)
            x_vect = plant.datetime_to_array(
                x_vect_orig,
                flag_matplotlib=flag_matplotlib)
            # step = plant.datetime_to_array(
            #    step,
            #    flag_matplotlib=flag_matplotlib)
        else:
            if not flag_no_data:
                x_data = self.data[0].data
            x_vect = x_vect_orig

        # print('>>> x_vect: ', x_vect)
        # print('>>> x_vect: ', x_vect_orig)

        if not flag_no_data:
            x_data = np.asarray(x_data, dtype=np.float32)
        x_vect = np.asarray(x_vect, dtype=np.float32)

        # x_min, x_max = self.ax.get_xlim()
        # y_min, y_max = self.ax.get_ylim()

        if (self.draw_line is not None and
                not ((self.type == GENERIC_PLOT and
                      not self.generic_plot_3d) or
                     self.type == IMAGEVIEWER or
                     self.type == PROFILE or
                     self.type == DENSITY_PLOT)):
            self.print('WARNING draw line not available for this option')
        elif (self.draw_line is not None):
            self.draw_line_dict = self._get_default_results_dict()
            # draw_line = # self.draw_line.split(':')
            draw_line = plant.read_image(self.draw_line).image[0]
            b = float(draw_line[1]) if len(draw_line) == 2 else 0
            a = float(draw_line[0])
            if not flag_no_data:
                for im_count, im in enumerate(image_list):
                    valid_indexes = np.where(plant.isvalid(
                        self.data[im].data))
                    y_vect_fit = a*x_data[valid_indexes]+b
                    self.print('draw line "a*x+b ('+self.data[im].name+')')
                    self.print('equation: %f*x+%f' % (a, b))
                    ret = plant.calculate_r2_rmse(
                        self.data[im].data[valid_indexes], y_vect_fit)
                    plant.list_insert(self.draw_line_dict['coeffs'],
                                      im_count, draw_line)
                    ret = plant.demux_input(ret, 2)
                    plant.list_insert(self.draw_line_dict['r2'],
                                      im_count, ret[0])
                    plant.list_insert(self.draw_line_dict['rmse'],
                                      im_count, ret[1])
            y_vect_fit = a*x_vect+b
            plot_x_arr, plot_y_arr = self.get_plot_orientation(x_vect_orig,
                                                               y_vect_fit)

            plot.plot(plot_x_arr,
                      plot_y_arr,
                      color=linecolor_stats,
                      linestyle=linestyle_stats,
                      linewidth=linewidth_stats)

            # y_from_x_min, y_from_x_max = (a*np.array([x_min, x_max],
            #                                         dtype=np.float)+b)
            # plot.plot([x_min, x_max],
            #         [y_from_x_min, y_from_x_max],
            #         color=self.data[im].linecolor,
            #         linestyle=self.data[im].linestyle,
            #         linewidth=self.data[im].linewidth)

        if (self.draw_function is not None and
                not ((self.type == GENERIC_PLOT and
                      not self.generic_plot_3d) or
                     self.type == PROFILE or
                     self.type == DENSITY_PLOT)):
            self.print('WARNING draw function not available for this option')
        elif self.draw_function is not None:
            # x_min, x_max = self.ax.get_xlim()
            # y_min, y_max = self.ax.get_ylim()
            self.draw_function_dict = self._get_default_results_dict()

            self.draw_function = self.draw_function.lower()
            self.draw_function = self.draw_function.replace(', ', ' ')
            self.draw_function = self.draw_function.replace(': ', ' ')
            ret = plant.get_function(self.draw_function)
            flag_invert_equation = ret is None and '_inv' in self.draw_function
            if flag_invert_equation:
                ret = plant.get_function(self.draw_function.replace('_inv',
                                                                    ''))
            if ret is not None:
                func, n_coefficients, bounds, initial_points = ret
                args = self.draw_function.split(' ')[1:]
                args_float = [float(x) for x in args if x != '']
            else:
                flag_invert_equation = None
                func = None
            if not flag_no_data:
                for im_count, im in enumerate(image_list):
                    args = []
                    valid_indexes = np.where(plant.isvalid(self.data[im].data))
                    if func is None:
                        self.print('ERROR not implemented')
                        # y_vect_fit = eval(self.draw_function,
                        #                   {'x': x_data[valid_indexes]})
                    elif flag_invert_equation:
                        from scipy import optimize
                        solver = optimize.fsolve
                        y_vect_fit = x_data[valid_indexes]
                        for i in range(y_vect_fit.shape[0]):
                            current_value = y_vect_fit[i]
                            if plant.isnan(current_value):
                                continue
                            y_vect_fit[i] = solver(
                                lambda x: (func(x, *args_float)-current_value),
                                1.0)
                    else:
                        args = [x_data[valid_indexes]] + args_float
                        y_vect_fit = func(*args)
                    self.print('draw function ('+self.data[im].name+')')
                    if func is None:
                        self.print('equation: %s' % (self.draw_function))
                    else:
                        self.print('%s: %s'
                                   % (self.draw_function.split(' ')[0],
                                      ', '.join(self.draw_function.split(
                                          ' ')[1:])))
                        equation = plant.get_function_equation(
                            self.draw_function.lower(),
                            args_float)
                        self.print('equation: %s' % (equation))
                    ret = plant.calculate_r2_rmse(
                        self.data[im].data[valid_indexes],
                        y_vect_fit)
                    plant.list_insert(self.draw_line_dict['coeffs'],
                                      im_count, y_vect_fit)
                    plant.list_insert(self.draw_function_dict['r2'],
                                      im_count, ret[0])
                    plant.list_insert(self.draw_function_dict['rmse'],
                                      im_count, ret[1])

            if func is None:
                self.print('ERROR not implemented')
                # _vect_fit = eval(self.draw_function, {'x': x_vect})
            else:
                args = [x_vect_orig] + args_float
                y_vect_fit = func(*args)
            plot_x_arr, plot_y_arr = self.get_plot_orientation(x_vect_orig,
                                                               y_vect_fit)
            plot.plot(plot_x_arr,
                      plot_y_arr,
                      color=linecolor_stats,
                      linestyle=linestyle_stats,
                      linewidth=linewidth_stats)

        # data fitting (only if "data" is provided)
        if flag_no_data:
            return

        if (self.linefit and
                not ((self.type == GENERIC_PLOT and
                      not self.generic_plot_3d) or
                     self.type == PROFILE or
                     self.type == DENSITY_PLOT)):
            self.print('WARNING linefit not available for this option')
        elif self.linefit:
            # import statsmodels.api.OLS as fit
            import statsmodels.api as sm
            self.linefit_dict = self._get_default_results_dict()
            # import statsmodels.graphics as smgraphics

            for im_count, im in enumerate(image_list):
                # data_y_array = np.asarray(data_y[im])
                valid_indexes = np.where(plant.isvalid(self.data[im].data))
                x = x_data[valid_indexes]
                X = sm.add_constant(x)
                y = self.data[im].data[valid_indexes]
                try:
                    linefit_y = sm.RLM(y, X).fit()
                    # linefit_y = sm.OLS(y, x).fit()
                except:
                    print('ERROR performing line fitting'
                          ' (statsmodels.api.RLM)')
                    return
                print(linefit_y.summary())
                coeffs = linefit_y.params
                y_vect_fit = linefit_y.predict(X)
                ret = plant.calculate_r2_rmse(
                    self.data[im].data[valid_indexes], y_vect_fit)
                plant.list_insert(self.linefit_dict['coeffs'], im_count,
                                  coeffs)
                plant.list_insert(self.linefit_dict['r2'], im_count, ret[0])
                plant.list_insert(self.linefit_dict['rmse'], im_count, ret[1])

                X_vect = sm.add_constant(x_vect)
                y_vect_fit = linefit_y.predict(X_vect)
                # y_vect_fit = f_y(x_vect)
                plot_x_arr, plot_y_arr = self.get_plot_orientation(x_vect_orig,
                                                                   y_vect_fit)
                plot.plot(plot_x_arr,
                          plot_y_arr,
                          color=self.data[im].linecolor_stats,
                          linestyle=self.data[im].linestyle_stats,
                          linewidth=self.data[im].linewidth_stats)

        if (self.polyfit is not None and
                not ((self.type == GENERIC_PLOT and
                      not self.generic_plot_3d) or
                     self.type == PROFILE or
                     self.type == DENSITY_PLOT)):
            self.print('WARNING polyfit not available for '
                       'this option')
        elif self.polyfit is not None:
            self.polyfit_dict = self._get_default_results_dict()
            for im_count, im in enumerate(image_list):
                # data_y_array = np.asarray(data_y[im])
                valid_indexes = np.where(plant.isvalid(self.data[im].data))
                try:
                    polyfit_y = np.polyfit(
                        x_data[valid_indexes],
                        (self.data[im].data)[valid_indexes],
                        self.polyfit)
                except:
                    print('ERROR fitting data with %d degree polynom'
                          % (self.polyfit))
                    sys.exit(1)
                polyfit_y = plant.format_number(
                    polyfit_y,
                    decimal_places=self.decimal_places,
                    sigfigs=self.n_significant_figures)
                # polyfit_y = [plant.round_to_sig_figs(
                #    x, self.n_significant_figures)
                #             for x in polyfit_y]
                self.print('polyfit of order ' +
                           str(self.polyfit)+' (' +
                           self.data[im].name+')')
                f_y = np.poly1d(polyfit_y)
                f_y_str = str(f_y)
                f_y_str = ' + '.join(['('+str(x)+'*x**' +
                                      str(len(polyfit_y)-i-1)+')'
                                      for i, x in enumerate(polyfit_y)])
                self.print('equation: %s' % (f_y_str))
                y_vect_fit = f_y(x_data[valid_indexes])
                ret = plant.calculate_r2_rmse(
                    self.data[im].data[valid_indexes], y_vect_fit)
                plant.list_insert(self.polyfit_dict['coeffs'],
                                  im_count, polyfit_y)
                plant.list_insert(self.polyfit_dict['r2'], im_count, ret[0])
                plant.list_insert(self.polyfit_dict['rmse'], im_count, ret[1])
                y_vect_fit = f_y(x_vect)
                plot_x_arr, plot_y_arr = self.get_plot_orientation(x_vect_orig,
                                                                   y_vect_fit)
                plot.plot(plot_x_arr,
                          plot_y_arr,
                          color=self.data[im].linecolor_stats,
                          linestyle=self.data[im].linestyle_stats,
                          linewidth=self.data[im].linewidth_stats)

        if (self.function_fit is not None and
                not ((self.type == GENERIC_PLOT and
                      not self.generic_plot_3d) or
                     self.type == PROFILE or
                     self.type == DENSITY_PLOT)):
            self.print('WARNING function_fit not available for this option')
        elif self.function_fit is not None:
            np.seterr(divide='ignore', invalid='ignore', over='ignore')
            self.function_fit_dict = self._get_default_results_dict()
            # coeffs_list = [None for x in range(len(image_list))]
            coeffs_list = []
            for im_count, im in enumerate(image_list):
                self.print('function fit ('+self.data[im].name+')')
                valid_indexes = np.where(plant.isvalid(self.data[im].data))
                y_data_valid = np.asarray(self.data[im].data[valid_indexes],
                                          dtype=np.float32)
                x_data_valid = x_data[valid_indexes]

                ret = plant.get_function(
                    self.function_fit.lower(),
                    x_data=x_data_valid,
                    y_data=y_data_valid)
                if ret is None:
                    self.print('ERROR function not found: ' +
                               str(self.function_fit))
                    sys.exit(1)
                # print(ret)
                func, n_coefficients, bounds, initial_points = ret
                function_fit_y = None
                # plant.debug(x_data_valid)
                # plant.debug(y_data_valid)
                # if initial_points is None and function_fit_y is None:
                # print('xxx: ', x_data_valid)
                # print('yyy: ', y_data_valid)
                self.print(f'bounds: {bounds}')
                # best_result_rmse = np.inf
                # if initial_points is not None:
                self.print(f'initial solution: {initial_points}')

                # method 1
                try:
                    best_function_fit_y, pcov = curve_fit(
                        func,
                        x_data_valid,
                        y_data_valid,
                        initial_points,
                        method='lm')
                    best_result_rmse = self.get_curvefit_results(
                        x_data_valid, y_data_valid,
                        func, best_function_fit_y)
                    # best_result_rmse = np.nanmean(np.sqrt(np.diag(pcov)))
                    # if plant.isnan(best_result_rmse)
                    # or best_result_rmse == 0:
                    #     best_result_rmse = 9999
                except:
                    best_result_rmse = np.inf
                    best_function_fit_y = None
                    # pass
                # print('*** x: ', x_data_valid)
                # print('*** y: ', y_data_valid)
                # print('*** method 1: ', best_function_fit_y,
                # best_result_rmse)

                # method 2
                try:
                    function_fit_y, pcov = curve_fit(
                        func,
                        x_data_valid,
                        y_data_valid)
                    rmse = self.get_curvefit_results(
                        x_data_valid, y_data_valid,
                        func, function_fit_y)
                    # print('*** method 2: ', function_fit_y, rmse)
                    # rmse = np.nanmean(np.sqrt(np.diag(pcov)))
                    # if plant.isnan(rmse) or rmse == 0:
                    #    rmse = 9999
                except:
                    function_fit_y = None
                    rmse = np.inf
                if (function_fit_y is not None and
                        plant.isvalid(rmse) and
                        rmse < best_result_rmse):
                    best_function_fit_y = function_fit_y
                    best_result_rmse = rmse

                # method 2
                if initial_points is None:
                    initial_points = np.zeros((n_coefficients))
                try:
                    function_fit_y, pcov = curve_fit(
                        func,
                        x_data_valid,
                        y_data_valid,
                        initial_points,
                        bounds=bounds)
                    rmse = self.get_curvefit_results(
                        x_data_valid, y_data_valid,
                        func, function_fit_y)
                    # print('*** method 3: ', function_fit_y, rmse)
                    # rmse = np.nanmean(np.sqrt(np.diag(pcov)))
                    # if plant.isnan(rmse) or rmse == 0:
                    #    rmse = 9999
                except:
                    function_fit_y = None
                    rmse = np.inf
                if (function_fit_y is not None and
                        plant.isvalid(rmse) and
                        rmse < best_result_rmse):
                    best_function_fit_y = function_fit_y
                    best_result_rmse = rmse

                # method 3
                initial_points = [random.random()
                                  for x in range(n_coefficients)]
                try:
                    function_fit_y, pcov = curve_fit(
                        func,
                        x_data_valid,
                        y_data_valid,
                        initial_points,
                        bounds=bounds)
                    rmse = self.get_curvefit_results(
                        x_data_valid, y_data_valid,
                        func, function_fit_y)
                    # print('*** method 4: ', function_fit_y, rmse)
                    # rmse = np.nanmean(np.sqrt(np.diag(pcov)))
                    # if plant.isnan(rmse) or rmse == 0:
                    #    rmse = 9999
                except:
                    function_fit_y = None
                    rmse = np.inf

                if (function_fit_y is not None and
                        plant.isvalid(rmse) and
                        rmse < best_result_rmse):
                    best_function_fit_y = function_fit_y
                    best_result_rmse = rmse

                if best_result_rmse == np.inf:
                    print(f'ERROR fitting function: {self.function_fit}')
                    return

                # pick best method
                function_fit_y = best_function_fit_y
                # print(function_fit_y, x_data_valid, y_data_valid)
                # plant.debug(function_fit_y)
                # print('*** before: ', function_fit_y)
                function_fit_y_str_list = [plant.format_number(
                    function_fit_y[i],
                    decimal_places=self.decimal_places,
                    sigfigs=self.n_significant_figures)
                                           for i in range(len(function_fit_y))]
                # function_fit_y = \
                #    [plant.round_to_sig_figs(x, self.n_significant_figures)
                #     for x in function_fit_y]
                # print('*** after: ', function_fit_y)
                function_fit_y_str = ', '.join(function_fit_y_str_list)
                # function_fit_y_str = str(function_fit_y_str).replace('[', '')
                # function_fit_y_str = function_fit_y_str.replace(']', '')
                self.print('%s: %s' % (self.function_fit.lower(),
                                       function_fit_y_str))
                equation = plant.get_function_equation(
                    self.function_fit.lower(),
                    function_fit_y_str_list)

                self.print('equation: %s' % equation)
                # coeffs_list[im] = [function_fit_y[i]
                # for i in range(n_coefficients)]
                coeffs = [i for i in function_fit_y]
                # for i in function_fit_y:
                #     print('*** i: ', i)
                #     print('*** i.__class__: ', i.__class__)
                args = [x_data_valid]
                args += coeffs

                # print('*** args: ', args)
                y_vect_fit = func(*args)
                ret = plant.calculate_r2_rmse(y_data_valid, y_vect_fit)
                plant.list_insert(coeffs_list, im_count, coeffs)
                plant.list_insert(self.function_fit_dict['coeffs'],
                                  im_count, coeffs)
                plant.list_insert(self.function_fit_dict['r2'],
                                  im_count, ret[0])
                plant.list_insert(self.function_fit_dict['rmse'],
                                  im_count, ret[1])
                # print('*** x, y_fit', args[0], y_vect_fit)

                # self.inv_axis
                # print('x_vect: ', x_vect)
                args = [x_vect]
                args += coeffs
                # print(args)
                # print([plant.get_dtype_name(d) for d in args])
                # args + [[function_fit_y[i]] for i in range(n_coefficients)]
                y_vect_fit = func(*args)
                # print('*** ', args[0], y_vect_fit)
                if self.inv_axis:
                    y_lookup = y_data_valid
                    x_lookup = np.zeros_like(y_lookup)
                    for i in range(x_lookup.size):
                        x_lookup[i] = x_vect[
                            np.nanargmin(np.absolute(y_vect_fit-y_lookup[i]))]
                        # print(x_lookup[i], x_min, x_max)
                        if x_lookup[i] < x_min or x_lookup[i] > x_max:
                            x_lookup[i] = np.nan

                        # REMOVE THIS!!!!!!
                        if x_lookup[i] < 0 or x_lookup[i] > 90:
                            x_lookup[i] = np.nan

                        # self.print('*** %.1f, %.1f'
                        #           % (x_lookup[i], y_lookup[i]))
                    ind = plant.isvalid(x_lookup)
                    # print(x_lookup[ind],
                    #      x_data_valid[ind])
                    # coeffs = [i for i in function_fit_y]
                    self.print('inverted axis:')
                    ret = plant.calculate_r2_rmse(x_lookup[ind],
                                                  x_data_valid[ind])
                    plant.list_insert(self.function_fit_dict['coeffs'],
                                      im_count, coeffs)
                    plant.list_insert(self.function_fit_dict['r2'],
                                      im_count, ret[0])
                    plant.list_insert(self.function_fit_dict['rmse'],
                                      im_count, ret[1])

                # if ('_db' in self.function_fit.lower() and
                #        self.function_fit_saturation is None):
                #    self.function_fit_saturation = 0.367879441171
                # elif (self.function_fit_saturation is None):
                #    self.function_fit_saturation = 0.001
                plot_x_arr, plot_y_arr = self.get_plot_orientation(x_vect_orig,
                                                                   y_vect_fit)
                plot.plot(plot_x_arr,
                          plot_y_arr,
                          color=self.data[im].linecolor_stats,
                          linestyle=self.data[im].linestyle_stats,
                          linewidth=self.data[im].linewidth_stats)

                if self.function_fit_saturation is not None:
                    data_is_saturated = (((y_vect_fit[1:] - y_vect_fit[0:-1]) /
                                          (x_vect[1:] - x_vect[0:-1])) <
                                         self.function_fit_saturation)
                    if (np.sum(data_is_saturated) != 0):
                        # or self.function_fit_saturation!=0):
                        saturation_index = min(np.where(data_is_saturated)[0])
                        self.print('saturation value (delta_y/delta_x): ' +
                                   str(self.function_fit_saturation))
                        self.print('saturation index: ' +
                                   str(saturation_index))
                        self.print('saturation point (x, y): ' +
                                   str(x_vect[saturation_index]) +
                                   ', ' +
                                   str(y_vect_fit[saturation_index]))
                        plot.axvline(x=x_vect[saturation_index],
                                     color=self.data[im].linecolor_stats,
                                     # linewidth=self.data[im].linewidth,
                                     linestyle='dashed')
                        plot.axhline(y=y_vect_fit[saturation_index],
                                     color=self.data[im].linecolor_stats,
                                     # linewidth=self.data[im].linewidth,
                                     linestyle='dashed')
                        plot.scatter(x_vect[saturation_index],
                                     y_vect_fit[saturation_index],
                                     s=50,
                                     c=self.data[im].linecolor_stats)
            if self.out_function_fit is not None:
                # coeffs_list = np.asarray(coeffs_list)
                # print(coeffs_list)
                plant.save_image(coeffs_list, self.out_function_fit,
                                 force=self.force)
                # linestyle=self.data[im].linestyle)
                # self.plot_names_list.insert(0,
                # 'function-fit ('+self.data[im].name+')')
            '''
            if self.out_r2 is not None:
                # coeffs_list = np.asarray(coeffs_list)
                # print(coeffs_list)
                plant.save_image(coeffs_list, self.out_function_fit,
                                 force=self.force)
                # linestyle=self.data[im].linestyle)
                # self.plot_names_list.insert(0,
                # 'function-fit ('+self.data[im].name+')')
            '''

        self.ax.set_xlim(x_min, x_max)
        self.ax.set_ylim(y_min, y_max)

    def get_curvefit_results(self, x_data_valid, y_data_valid,
                             func, function_fit_y):
        coeffs = [i for i in function_fit_y]
        args = [x_data_valid] + coeffs
        y_vect_fit = func(*args)
        # print('**** args: ', args)
        # print('*** x_data_valid: ', x_data_valid)
        # print('*** y_vect_fit: ', y_vect_fit)
        result = plant.calculate_r2_rmse(y_data_valid, y_vect_fit,
                                         verbose=False,
                                         flag_calculate_r2=False)
        # print('*** result: ', result)
        return result

    def plot_hist_loop(self):
        if self.histtype is None and self.default_nbins[0] > 100:
            self.histtype = 'stepfilled'
        elif self.histtype is None or self.histtype == 'None':
            self.histtype = 'bar'
        print('nbins: '+str(self.default_nbins[0]))
        if isinstance(self.default_min, Sequence):
            print(f'min (hist. range): {max(self.default_min)}')
        else:
            print(f'min (hist. range): {self.default_min}')
        if isinstance(self.default_max, Sequence):
            print(f'max (hist. range): {min(self.default_max)}')
        else:
            print(f'max (hist. range): {self.default_min}')
        print('normed: '+str(self.sum_normed))
        print('log: '+str(self.hist_log))
        # print('alpha: '+str(self.alpha))
        print('histtype: '+str(self.histtype))
        # print('edgecolor: '+str(self.edgecolor))
        self.orientation = 'horizontal' if self.inv_axis else 'vertical'
        if self.orientation is not None:
            print('orientation: '+str(self.orientation))
        print('linewidth: '+str(self.linewidth))
        for i in range(self.input_number):
            self.plot_hist(self.data[i])

    def plot_hist(self, data_obj):
        plot_range = [min(self.default_min), max(self.default_max)]
        # if 'float64' in plant.get_dtype_name(data_obj.data).lower():
        #     data_obj.data = np.asarray(data_obj.data,
        #                                dtype=np.float32)

        n, bins, patches = self.plt.hist(
            data_obj.data,
            int(self.default_nbins[0]),
            range=plot_range,
            density=self.sum_normed,
            log=self.hist_log,
            facecolor=data_obj.color,
            edgecolor=data_obj.edgecolor,
            hatch=data_obj.hatch,
            orientation=self.orientation,
            alpha=data_obj.alpha,
            histtype=self.histtype,
            linewidth=int(self.linewidth))

        self.plot_names_list.append(data_obj.name)
        self.plot_handles_list.append(patches[0])

        # self.ax = self.plt.gca()
        # handles, labels =
        # print(self.ax.__dict__)
        # print(patches.axes.get_legend_handler_map())
        # self.plot_handles_list.append(patches)

        # bins_center = (bins[0:-1] + bins[1:]) / 2.0
        alpha = 1
        max_n = np.nanmax(n)
        if self.stddev and data_obj.stddev is not None:
            self.plt.axvspan(data_obj.mean - data_obj.stddev,
                             data_obj.mean + data_obj.stddev,
                             color=data_obj.color_stats,
                             alpha=alpha*0.1)

            if (data_obj.mean + data_obj.stddev < max(self.default_max)):
                self.plt.axvline(x=data_obj.mean + data_obj.stddev,
                                 color=data_obj.color_stats, alpha=alpha,
                                 linewidth=self.linewidth_stats,
                                 linestyle=self.linestyle_stats)
                # self.plot_names_list.insert(0,
                # '(mean+std) ('+data_obj.name+')')
                if self.show_annotations:
                    self.plt.annotate("  $\mu+\sigma$ = %.2f" %
                                      (data_obj.mean + data_obj.stddev),
                                      xy=(data_obj.mean + data_obj.stddev,
                                          max_n *
                                          (1+self.annotation_count *
                                           ANNOT_SIZE)),
                                      color=data_obj.color_stats_text,
                                      fontsize=data_obj.fontsize_stats)
                    self.annotation_count += 1
            if (data_obj.mean - data_obj.stddev > min(self.default_min)):
                self.plt.axvline(x=data_obj.mean + -data_obj.stddev,
                                 color=data_obj.color_stats, alpha=alpha,
                                 linewidth=self.linewidth_stats,
                                 linestyle=self.linestyle_stats)
                # self.plot_names_list.insert(0,
                # '(mean-std) ('+data_obj.name+')')
                if self.show_annotations:
                    self.plt.annotate("  $\mu-\sigma$ = %.2f" %
                                      (data_obj.mean - data_obj.stddev),
                                      xy=(data_obj.mean - data_obj.stddev,
                                          max_n *
                                          (1+self.annotation_count *
                                           ANNOT_SIZE)),
                                      color=data_obj.color_stats_text,
                                      fontsize=data_obj.fontsize_stats)
                    self.annotation_count += 1

        if self.mode:
            max_n_index, max_n_value = max(
                enumerate(n), key=operator.itemgetter(1))
            data_obj.mode = bins[max_n_index]
            self.print('## input %d: %s'
                       % (data_obj.number+1, data_obj.filename))
            with plant.PlantIndent():
                self.print('mode: ' + str(data_obj.mode))
                data_obj.color_mode = data_obj.color
                # self.plt.vlines(x=data_obj.mode
                # , color=data_obj.color_mode, alpha=1,
                #            linewidth=self.linewidth, ymin=0,
                # ymax=n[max_n_index])
                self.plt.axvline(x=data_obj.mode, color=data_obj.color_stats,
                                 alpha=alpha,
                                 linewidth=self.linewidth_stats,
                                 linestyle=self.linestyle_stats)
                if self.show_annotations:
                    self.plt.annotate("  mode = %.2f" % data_obj.mode,
                                      xy=(data_obj.mode,
                                          max_n * (1 + self.annotation_count *
                                                   ANNOT_SIZE)),
                                      color=data_obj.color_stats_text,
                                      fontsize=data_obj.fontsize_stats)
                    self.annotation_count += 1

        '''
        if self.mode and data_obj.mode is not None:
            # ret = min(enumerate(np.absolute(data_obj.mode -
            #                                 bins_center)),
            #           key=operator.itemgetter(1))
            # min_n_index, min_n_value = ret
            # self.plt.vlines(x=data_obj.mode, color=data_obj.color_mode,
            #  alpha=1, linewidth=self.linewidth, ymin=0, ymax=n[min_n_index])
            self.plt.axvline(x=data_obj.mode, color=data_obj.color_mode,
                        alpha=alpha, linewidth=self.linewidth,
                        linestyle=self.linestyle)
            self.plt.annotate("  mode = %.2f" % data_obj.mode,
                         xy=(data_obj.mode,
                             max_n * (1 + self.annotation_count*ANNOT_SIZE)),
                         color=data_obj.color_mode_text)
            self.annotation_count += 1
        '''

        if self.median and data_obj.median is not None:
            # ret = min(enumerate(np.absolute(data_obj.median -
            #                                 bins_center)),
            #           key=operator.itemgetter(1))
            # min_n_index, min_n_value = ret
            # self.plt.vlines(x=data_obj.median, color=data_obj.color_median,
            #  alpha=1, linewidth=self.linewidth, ymin=0, ymax=n[min_n_index])
            self.plt.axvline(x=data_obj.median, color=data_obj.color_stats,
                             alpha=alpha,
                             linewidth=self.linewidth_stats,
                             linestyle=self.linestyle_stats)
            if self.show_annotations:
                self.plt.annotate("  median = %.2f" % data_obj.median,
                                  xy=(data_obj.median,
                                      max_n * (1 + self.annotation_count *
                                               ANNOT_SIZE)),
                                  color=data_obj.color_stats_text,
                                  fontsize=data_obj.fontsize_stats)
                self.annotation_count += 1
        if self.mean and data_obj.mean is not None:
            # min_n_index, min_n_value = min(enumerate(np.absolute(
            #     data_obj.mean - bins_center)), key=operator.itemgetter(1))
            # self.plt.vlines(x=data_obj.mean
            # , color=data_obj.color_mean, alpha=1,
            #            linewidth=self.linewidth, ymin=0, ymax=n[min_n_index])
            self.plt.axvline(x=data_obj.mean, color=data_obj.color_stats,
                             alpha=alpha,
                             linewidth=self.linewidth_stats,
                             linestyle=self.linestyle_stats)
            if self.show_annotations:
                self.plt.annotate("  $\mu$ = %.2f" % data_obj.mean,
                                  xy=(data_obj.mean,
                                      max_n * (1 + self.annotation_count *
                                               ANNOT_SIZE)),
                                  color=data_obj.color_stats_text,
                                  fontsize=data_obj.fontsize_stats)
                self.annotation_count += 1
        hist_ylim_max = max_n * (1 + self.annotation_count*ANNOT_SIZE)
        # ax = self.plt.gca()
        current_ylim = self.ax.get_ylim()
        if hist_ylim_max > current_ylim[1]:
            self.ax.set_ylim([current_ylim[0], hist_ylim_max])
        self.ax.set_xlim(plot_range)

        if self.out_text is not None:
            flag_update_file = plant.overwrite_file_check(self.out_text,
                                                          force=self.force)
            if flag_update_file:
                with open(self.out_text, 'w+') as f:
                    for x in range(len(n)):
                        f.write(str((bins[x + 1] + bins[x]) /
                                    2) + ', ' + str(n[x]) + '\n')
                if plant.isfile(self.out_text):
                    self.print('output text: ' + self.out_text)
            else:
                self.print('WARNING file not updated: ' +
                           self.out_text)

    def plot_profile_loop(self):
        profile_abscissa = None
        for i in range(self.input_number):
            if self.data[i].sum_normed:
                self.print(f'file {i} normed:'
                           f' {self.data[i].sum_normed} (sum)')
            elif self.data[i].max_normed:
                self.print(f'file {i} normed:'
                           f' {self.data[i].max_normed} (max)')
            elif self.data[i].sum_normed_to_value:
                self.print(f'file {i} normed to:'
                           f' {self.data[i].sum_normed_to_value} (sum)')
            elif self.data[i].max_normed_to_value:
                self.print(f'file {i} normed to:'
                           f' {self.data[i].max_normed_to_value} (max)')
        if self.profile_abscissa is not None:
            self.print('profile abscissa: %s'
                       % self.profile_abscissa)
            profile_abscissa = plant.read_matrix(self.profile_abscissa).ravel()
            if len(profile_abscissa) != 2:
                for i in range(self.input_number):
                    if profile_abscissa.shape[0] != self.data[i].data.shape[0]:
                        self.print('WARNING profile abscissa vector'
                                   ' length (%d) does not match profile'
                                   ' length (%d).'
                                   % (profile_abscissa.shape[0],
                                      self.data[i].data.shape[0]))

        profile_min, _ = self.get_plot_orientation(self.xmin, self.ymin)
        profile_max, _ = self.get_plot_orientation(self.xmax, self.ymax)

        if profile_min is None:
            xmin = [
                self.data[i].xmin
                for i in range(self.input_number)
                if self.data[i].xmin is not None]
            min_index = np.nanmin(xmin)
            if profile_abscissa is None:
                profile_min = min_index
                # if plant.isvalid(min_index):
                #     profile_min = min_index
            elif len(profile_abscissa) == 2:
                profile_min = profile_abscissa[0]
            else:
                profile_min = profile_abscissa[min_index]
                # profile_min = profile_abscissa[0]
        if profile_max is None:
            xmax = [
                self.data[i].xmax
                for i in range(self.input_number)
                if self.data[i].xmax is not None]
            max_index = np.nanmax(xmax)
            # max_index = np.nanmax([self.data[i].xmax
            #                        for i in range(self.input_number)])
            if profile_abscissa is None:
                profile_max = max_index
                # if plant.isvalid(max_index):
                #     profile_max = max_index
            elif len(profile_abscissa) == 2:
                profile_max = profile_abscissa[1]
            else:
                profile_max = profile_abscissa[max_index]
                # profile_max = profile_abscissa[-1]
        self.print('profile start (abscissa): %d'
                   % (profile_min))
        self.print('profile end (abscissa): %d'
                   % (profile_max))
        # self.print('profile length: %d'
        #           % (profile_max - profile_min))
        # x_min, x_max = self.ax.get_xlim()
        # y_min, y_max = self.ax.get_ylim()
        # print('*** x_min, x_max: ', x_min, x_max)
        # print('*** y_min, y_may: ', y_min, y_max)
        # print('*** self.x_min, self.x_max: ', self.xmin, self.xmax)
        # print('*** self.y_min, self.y_may: ', self.ymin, self.ymax)
        self.print('plot min (ordinate): %d'
                   % (min(self.default_min)))
        self.print('plot max (ordinate): %d'
                   % (max(self.default_max)))
        if not self.inv_axis:
            if self.xmin is None:
                self.xmin = profile_min
            if self.xmax is None:
                self.xmax = profile_max
        else:
            if self.ymin is None:
                self.ymin = profile_min
            if self.ymax is None:
                self.ymax = profile_max

        for i in range(self.input_number):
            self.plot_profile(self.data[i], profile_abscissa=profile_abscissa)

        for i in range(0, self.input_number):
            self.data[self.input_number-i] = \
                self.data[self.input_number-i-1]

            # self.input_number += 1
        self.data[0] = DataDisplay()
        if profile_abscissa is None or len(profile_abscissa) == 2:
            max_len = np.nanmax([len(self.data[i].data)
                                 for i in range(1, self.input_number+1)])
            if profile_abscissa is None:
                self.data[0].data = np.arange(max_len)
            else:
                self.data[0].data = (((profile_abscissa[1] -
                                       profile_abscissa[0]) *
                                      np.arange(max_len) /
                                      float(max_len)) +
                                     profile_abscissa[0])
        else:
            self.data[0].data = profile_abscissa
        self.data[0].name = ''
        self.data[0].vmax = np.max(self.data[0].data)
        self.data[0].vmin = np.min(self.data[0].data)
        self.first_image_as_x = True
        # self.data[0].nbins = len(self.data[0].data)
        '''
        xlim, ylim = self.get_plot_orientation(self.ax.get_xlim(),
                                               self.ax.get_ylim())
        x_min, x_max = xlim
        y_min, y_max = ylim
        print('***  x_min: ', x_min)
        print('***  x_max: ', x_max)
        f_ax_x, f_ax_y = self.get_plot_orientation(self.ax.set_xlim,
                                                   self.ax.set_ylim)
        f_ax_x(x_min, x_max)
        '''

    def plot_profile(self, data_obj, profile_abscissa=None):
        plot_range = [min(self.default_min), max(self.default_max)]
        if profile_abscissa is None:
            profile_abscissa = np.arange(len(data_obj.data))
        elif len(profile_abscissa) == 2:
            profile_abscissa = (((profile_abscissa[1] -
                                  profile_abscissa[0]) *
                                 np.arange(len(data_obj.data)) /
                                 float(len(data_obj.data))) +
                                profile_abscissa[0])
        profile_abscissa, data = plant.get_intersection(profile_abscissa,
                                                        data_obj.data)
        plot_x_arr, plot_y_arr = self.get_plot_orientation(profile_abscissa,
                                                           data)
        handles, = self.plt.plot(plot_x_arr,
                                 plot_y_arr,
                                 color=data_obj.linecolor,
                                 alpha=data_obj.alpha,
                                 linestyle=data_obj.linestyle,
                                 linewidth=data_obj.linewidth)

        self.plot_names_list.append(data_obj.name)
        self.plot_handles_list.append(handles)

        if self.profiles_semilogy:
            f_ax_x, f_ax_y = self.get_plot_orientation(self.ax.set_xscale,
                                                       self.ax.set_yscale)
            f_ax_y('log')
        else:
            f_ax_x, f_ax_y = self.get_plot_orientation(self.ax.set_xlim,
                                                       self.ax.set_ylim)
            f_ax_y(plot_range)
            f_ax_x([self.xmin, self.xmax])

        '''
        if self.profiles_semilogy and not self.inv_axis:
            self.ax.set_yscale('log', basey=10)
        elif self.profiles_semilogy:
            self.ax.set_xscale('log', basex=10)
        elif not self.inv_axis:
            self.ax.set_ylim(plot_range)
        else:
            self.ax.set_xlim(plot_range)
        '''

        if self.profiles_db:
            decimal_places = self.decimal_places
            if (decimal_places is None and
                    self.n_significant_figures is None):
                decimal_places = 2
            g_ax_x, g_ax_y = self.get_plot_orientation(
                self.ax.get_xticks, self.ax.get_yticks)
            s_ax_x, s_ax_y = self.get_plot_orientation(
                self.ax.set_xticklabels, self.ax.set_yticklabels)
            labels = g_ax_y().tolist()
            labels = [plant.get_db(float(label))
                      for label in labels]
            labels = plant.format_number(
                labels,
                decimal_places=decimal_places,
                sigfigs=self.n_significant_figures)
            labels = [label if 'nan' not in label.lower() else ''
                      for label in labels]
            flag_plot_inf = True
            if not flag_plot_inf:
                labels = [label if 'inf' not in label.lower() else ''
                          for label in labels]
            s_ax_y(labels, rotation='horizontal')
        '''
        locs, labels = self.plt.xticks()
        for label in labels:
            print('x', label)
        locs, labels = self.plt.yticks()
        for label in labels:
            print('y', label)
        # self.plt.yticks(locs, [labels])
        '''
        self.plot_statistics(data_obj)

    def plot_statistics(self, data_obj, alpha=1, text_x_pos=0):
        # print(text_x_pos, ANNOT_SIZE)
        # print(self.fontsize)
        # print(data_obj.color_mean_text)
        if self.median and data_obj.median is not None:
            self.plt.axhline(y=data_obj.median, color=data_obj.color_stats,
                             alpha=alpha,
                             linewidth=data_obj.linewidth_stats,
                             linestyle=data_obj.linestyle_stats)
            if self.show_annotations:
                self.plt.annotate("median = %.2f" % data_obj.median,
                                  xy=(text_x_pos, ANNOT_SIZE +
                                      data_obj.median),
                                  color=data_obj.color_stats_text,
                                  fontsize=data_obj.fontsize)
        if self.mode and data_obj.mode is not None:
            self.plt.axhline(y=data_obj.mode, color=data_obj.color_stats,
                             alpha=alpha,
                             linewidth=data_obj.linewidth_stats,
                             linestyle=data_obj.linestyle_stats)
            if self.show_annotations:
                self.plt.annotate("mode = %.2f" % data_obj.mode,
                                  xy=(text_x_pos, ANNOT_SIZE + data_obj.mode),
                                  color=data_obj.color_stats_text,
                                  fontsize=data_obj.fontsize)
        if self.mean and data_obj.mean is not None:
            self.plt.axhline(y=data_obj.mean, color=data_obj.color_stats,
                             alpha=alpha,
                             linewidth=data_obj.linewidth_stats,
                             linestyle=data_obj.linestyle_stats)
            if self.show_annotations:
                self.plt.annotate("$\mu$ = %.2f" % data_obj.mean,
                                  xy=(text_x_pos, ANNOT_SIZE + data_obj.mean),
                                  color=data_obj.color_stats_text,
                                  fontsize=data_obj.fontsize)
        if self.stddev and data_obj.stddev is not None:
            self.plt.axhspan(data_obj.mean - data_obj.stddev,
                             data_obj.mean + data_obj.stddev,
                             color=data_obj.color_stats,
                             alpha=alpha*0.1)
            if (data_obj.mean + data_obj.stddev < max(self.default_max)):
                self.plt.axhline(y=data_obj.mean + data_obj.stddev,
                                 color=data_obj.color_stats, alpha=alpha,
                                 linewidth=data_obj.linewidth_stats,
                                 linestyle=data_obj.linestyle_stats)
                if self.show_annotations:
                    self.plt.annotate("$\mu+\sigma$ (%.2f)" % data_obj.stddev,
                                      xy=(text_x_pos, ANNOT_SIZE +
                                          data_obj.mean+data_obj.stddev),
                                      color=data_obj.color_stats_text,
                                      fontsize=data_obj.fontsize)
            if (data_obj.mean - data_obj.stddev > min(self.default_min)):
                self.plt.axhline(y=data_obj.mean - data_obj.stddev,
                                 color=data_obj.color_stats, alpha=alpha,
                                 linewidth=data_obj.linewidth_stats,
                                 linestyle=data_obj.linestyle_stats)
                if self.show_annotations:
                    self.plt.annotate("$\mu-\sigma$ (%.2f)" % data_obj.stddev,
                                      xy=(text_x_pos, ANNOT_SIZE +
                                          data_obj.mean-data_obj.stddev),
                                      color=data_obj.color_stats_text,
                                      fontsize=data_obj.fontsize)

    def plot_multiplot(self):
        # if self.multiplot_ncolumns is None:

        self.multiplot_ncolumns = self.data[0].column + 1
        for i in range(1, self.input_number):
            self.multiplot_ncolumns = max(self.multiplot_ncolumns,
                                          self.data[i].column + 1)
        # if self.multiplot_nlines is None:
        self.multiplot_nlines = self.data[0].line + 1
        for i in range(1, self.input_number):
            self.multiplot_nlines = max(self.multiplot_nlines,
                                        self.data[i].line + 1)

        self.linestyle = ['' for t in range(self.multiplot_nlines)]
        if self.multiplot_ncolumns == 1:
            x = ['' for t in range(self.multiplot_nlines)]
        else:
            x = [['' for t in range(self.multiplot_ncolumns)]
                 for t2 in range(self.multiplot_nlines)]

        y = np.zeros((self.multiplot_nlines, self.multiplot_ncolumns))
        yerr = np.zeros((self.multiplot_nlines, self.multiplot_ncolumns))
        y_name = ['' for t in range(self.multiplot_nlines)]
        # y_color = ['' for t in range(self.multiplot_nlines)]
        y[:] = np.nan
        flag_yerr = False
        yerr[:] = np.nan
        y_color_list = [None for t in range(self.multiplot_nlines)]
        hatch_list = [None for t in range(self.multiplot_nlines)]
        y_linestyle_list = [None for t in range(self.multiplot_nlines)]
        # x_vect = ['']*self.input_number
        # yerr = np.zeros((self.input_number))
        # print('*** ml ncolumns: ', self.multiplot_ncolumns)
        # print('*** ml nlines: ', self.multiplot_nlines)
        # if self.multiplot_post_function is not None:
        if self.multiplot_post_function:
            print(f'post-function: {self.multiplot_post_function}')
        for im in range(self.input_number):
            data_obj = self.data[im]
            i = data_obj.line
            j = data_obj.column
            y_value = data_obj.data
            # print(f'{im} before {self.multiplot_post_function}: {y_value}')
            y_value = plant.apply_function(y_value,
                                           self.multiplot_post_function,
                                           plant_transform_obj=None)
            # print(f'{im} after: {y_value}')
            y[i, j] = plant.format_number(
                y_value,
                decimal_places=self.decimal_places,
                sigfigs=self.n_significant_figures)
            # y[i, j] = plant.round_to_sig_figs(y_value,
            #                                  self.n_significant_figures)

            # if y_color_list[i] is None and data_obj.color is not None:
            #     y_color_list[i] = data_obj.color
            if y_color_list[i] is None:
                y_color_list[i] = plant.get_color_display(
                    i,
                    flag_pol_color=self.flag_pol_color,
                    # total=self.multiplot_nlines,
                    color_vect=data_obj.color,
                    cmap=data_obj.cmap,
                    # color_list_number=0)
                    flag_line_or_point=self.generic_plot)

            if hatch_list[i] is None:
                hatch_list[i] = data_obj.hatch

            if y_linestyle_list[i] is None:
                y_linestyle_list[i] = data_obj.linestyle
            # if j == 0:
            #     self.linestyle[i] = data_obj.linestyle
            if (self.multiplot_ncolumns != 1 and
                    self.plot_x_list[j] != data_obj.filename and
                    self.multiplot_nlines != 1 and
                    self.multiplot):
                # plant.debug('selecting %d from %s: %s'
                #      % (j, str(self.plot_x_list),
                #         self.plot_x_list[j]))
                x[i][j] = self.plot_x_list[j]
            elif (self.multiplot_ncolumns != 1 and
                  self.plot_x_list[j] != data_obj.filename):
                x[i][j] = data_obj.name
            # elif (self.multiplot_ncolumns != 1 and not x_is_numeric and
            #       not x[j]):
            #    x[j] = self.plot_x_list[j]
            # elif self.multiplot_ncolumns == 1:
            elif (self.multiplot_ncolumns != 1):
                x[i][j] = data_obj.name
            elif not x[i]:
                # self.xlabel_vertical = True
                x[i] = data_obj.name
            # print('*** name: ', data_obj.name)
            # if j == 0:
            if not y_name[i]:
                # y_name[i] = self.plot_y_list[i]
                y_name[i] = self.plot_y_list[i]
            # if not y_color[i]:
            #     y_color[i] = data_obj.color
            # x_vect[im] = data_obj.name
            if (data_obj.stddev is not None):
                yerr[i, j] = self.data[im].stddev
                flag_yerr = True
            else:
                yerr[i, j] = 0

        # ind = np.arange(self.multiplot_ncolumns)
        # ax = self.plt.gca()
        '''
        if self.multiplot_ncolumns == 1 and flag_yerr:
            ind = np.arange(self.multiplot_nlines) + 0.25
            handles = self.plt.bar(ind, y[:, 0], yerr=yerr[:, 0],
                              ecolor=self.linecolor)
            self.plot_handles_list.append(handles)
            for i in range(self.input_number):
                handles[i].set_color(self.data[i].color)
            plot_name = ', '.join([self.data[x].name
                                   for x in range(self.input_number)])
            self.plot_names_list.append(plot_name)
                # if not(self.multiplot_hide_values):
                #    length = (y[i, 0]*100//1)/100
                #    if length > 0:
                #        ypos = 1.05*length
                #    else:
                #        ypos = 1.05*length - 1.5
                #    self.plt.text(barlist[i].get_x()
                #  + barlist[i].get_width()/2.,
                #             ypos, '%f' % length, ha='center', va='bottom',
                #             fontsize=self.fontsize)
            # self.plt.errorbar(ind, y[:, 0], yerr[:, 0],
            # marker='o', markersize=8,
            #           linestyle=self.linestyle[0])
            # ax.set_xticks(ind)
            self.ax.set_xticks(ind + 0.4)
            self.ax.set_xticklabels(x, fontsize=self.fontsize)
            self.ax.set_xlim(0, self.multiplot_nlines + 0.3)
            '''

        '''
        if isinstance(self.linestyle, list):
            for i in range(len(self.linestyle)):
                if self.linestyle[i] != LINESTYLE_DEFAULT:
                    self.linestyle = self.linestyle[i]
            if isinstance(self.linestyle, list):
                self.linestyle = LINESTYLE_DEFAULT
        '''

        if self.multiplot_ncolumns == 1:
            self.multiplot_ncolumns = self.multiplot_nlines
            self.multiplot_nlines = 1
            # y = [y[:, 0].tolist()]
            y = y.reshape(1, y.shape[0])
            yerr = yerr.reshape(1, yerr.shape[0])
            x = [x]
            '''
            ind = np.arange(self.multiplot_nlines)
            handles = self.plt.errorbar(ind, y[:, 0],
                                   yerr=yerr[:, 0],
                                   marker = 'o',
                                   markersize=4,
                                   alpha=self.alpha,
                                   ecolor=self.linecolor,
                                   linestyle=self.linestyle,
                                   linewidth=self.linewidth)
            self.plot_handles_list.append(handles)
            plot_name = ', '.join([self.data[x].name
                                   for x in range(self.input_number)])
            self.plot_names_list.append(plot_name)
            self.ax.set_xticks(ind)
            self.ax.set_xticklabels(x, fontsize=self.fontsize)
            self.ax.set_ylim(vmin, vmax)
            self.ax.set_xlim(0, self.multiplot_nlines-1)
        else:
            '''
            # ind = np.arange(self.multiplot_ncolumns)
        x_is_numeric = all([plant.isnumeric(value)
                            for sublist in x
                            for value in sublist])
        flag_keep_original_x_axis = False
        if x_is_numeric:
            x = np.asarray(x, dtype=np.float).tolist()
            flag_keep_original_x_axis = np.array_equal(
                np.asarray(x).ravel(), np.arange(np.asarray(x).size))

        self.print('number of columns: %d'
                   % self.multiplot_ncolumns)
        self.print('number of lines: %d'
                   % self.multiplot_nlines)
        self.print('multiplot type: %s'
                   % self.multiplot_type)
        if self.decimal_places is not None:
            self.print('decimal places: %d'
                       % self.decimal_places)
        if self.n_significant_figures is not None:
            self.print('number of significant figures: %d'
                       % self.n_significant_figures)
        if self.decimal_places is not None:
            decimal_format = '%.'+str(self.decimal_places)+'f'
        space = 0.3
        width = (1.0-space)/self.multiplot_nlines
        xticks_list = []
        xticklabels_list = []
        # plant.debug(self.plot_y_color_list)
        if self.multiplot_type == 'table':
            y_text_list = []

        for i in range(self.multiplot_nlines):
            # data_obj = self.data[i+1]
            if not flag_yerr:
                current_yerr = None
            else:
                current_yerr = float(self.stddev_scale)*yerr[i, :]
            if x_is_numeric and self.multiplot:
                x_values = x[i]
            else:
                x_values = np.arange(self.multiplot_ncolumns)
            # plant.debug(x_values)
            self.print('line %d: %s labels: %s'
                       % (i+1, y_name[i], str(x[i])))
            if self.decimal_places is not None:
                y_text = [decimal_format % y[i, j]
                          for j in range(self.multiplot_ncolumns)]
            else:
                y_text = [str(y[i, j])
                          for j in range(self.multiplot_ncolumns)]

            self.print('line %d: %s values: %s'
                       % (i+1, y_name[i], y_text))
            if current_yerr is not None:
                self.print('line %d: %s stddev: %s'
                           % (i+1, y_name[i], str(current_yerr)))

            if self.cmap is None and self.gray_scale:
                self.cmap = 'gray'

            linestyle = y_linestyle_list[i]
            if (linestyle is None and
                    self.cmap is not None and
                    self.cmap == 'gray' and
                    self.change_linestyle is False and
                    (self.multiplot or self.generic_plot)):
                linestyle = 'solid'
            elif (linestyle is None and
                    self.cmap is not None and
                    self.cmap == 'gray' and
                    (self.multiplot or self.generic_plot)):
                linestyle = plant.LINESTYLE_LIST[
                    i % len(plant.LINESTYLE_LIST)]

            color = y_color_list[i]
            edgecolor = plant.get_edgecolor(color)
            hatch = hatch_list[i]
            if (hatch is None and
                (self.flag_hatch or
                 (self.gray_scale and self.flag_hatch is not False))):
                hatch = plant.HATCH_LIST[
                    i % len(plant.HATCH_LIST)]

            # plant.debug(color)
            if self.multiplot_type == 'bar':
                '''
                barplot_x = x_values+width*i
                barplot_y = y[i, :]
                valid_ind = np.where(plant.isvalid(barplot_x) &
                                     plant.isvalid(barplot_y))
                barplot_x = barplot_x[valid_ind]
                barplot_y = barplot_y[valid_ind]
                if current_yerr is None:
                    barplot_yerr = None
                else:
                    barplot_yerr = current_yerr[valid_ind]
                '''
                # ind = np.where(plant.isvalid(y[i, :]))
                # if current_yerr is not None:
                #    current_yerr = current_yerr[ind]
                kwargs_yerr = {}
                if self.barplot_width is None:
                    barplot_width = width
                else:
                    barplot_width = self.barplot_width * width
                    # 0.8 width
                if self.barplot_rmse:
                    handles = self.plt.bar((x_values+width*i),
                                           np.sqrt(y[i, :]**2 +
                                                   current_yerr**2),
                                           # yerr=current_yerr,
                                           # hatch=hatch,
                                           color='w',
                                           # alpha=0.2,
                                           # color=color,
                                           edgecolor=edgecolor,
                                           # linestyle='dashed',
                                           # capsize=5,
                                           # ecolor=self.linecolor,
                                           # align='edge',
                                           width=barplot_width)
                    handles = self.plt.bar((x_values+width*i),
                                           np.sqrt(y[i, :]**2 +
                                                   current_yerr**2),
                                           # yerr=current_yerr,
                                           hatch=hatch,
                                           # color='w',
                                           alpha=0.35,
                                           color=color,
                                           edgecolor=edgecolor,
                                           linestyle='dashed',
                                           # capsize=5,
                                           # ecolor=self.linecolor,
                                           # align='edge',
                                           width=barplot_width)
                    '''
                    handles = self.plt.bar((x_values+width*i),
                                           current_yerr,
                                           # yerr=current_yerr,
                                           hatch=hatch,
                                           # color='w',
                                           # alpha=0.2,
                                           # color=color,
                                           edgecolor='k',
                                           # linestyle='dashed',
                                           # capsize=5,
                                           # ecolor=self.linecolor,
                                           # align='edge',
                                           width=0.01)
                    '''
                else:
                    kwargs_yerr['yerr'] = current_yerr
                handles = self.plt.bar((x_values+width*i),
                                       y[i, :],
                                       # yerr=current_yerr,
                                       hatch=hatch,
                                       color=color,
                                       edgecolor=edgecolor,
                                       capsize=5,
                                       # align='edge',
                                       # ecolor=self.linecolor,
                                       width=barplot_width,
                                       **kwargs_yerr)
                # if current_yerr is not None:
                #     continue
                if self.barplot_rmse and self.barplot_show_values:
                    for j in range(self.multiplot_ncolumns):
                        '''
                        if (plant.isnan(y[i, j]) or
                                plant.isnan(current_yerr[j])):
                            continue
                        value = current_yerr[j]
                        value_str = plant.format_number(
                            value,
                            # current_yerr[j],
                            decimal_places=self.decimal_places,
                            sigfigs=self.n_significant_figures)
                        self.plt.text((x_values[j]+width*i),  # +0.4
                                      value,
                                      value_str,
                                      fontsize=self.legend_fontsize,
                                      ha='center',
                                      va='top')
                        '''
                        value = np.sqrt(y[i, j]**2 +
                                        current_yerr[j]**2)
                        value_str = plant.format_number(
                            value,
                            # current_yerr[j],
                            decimal_places=self.decimal_places,
                            sigfigs=self.n_significant_figures)
                        self.plt.text((x_values[j]+width*i),  # +0.4
                                      value,
                                      value_str,
                                      fontsize=self.legend_fontsize,
                                      ha='center',
                                      va='bottom')
                if self.barplot_show_values:
                    for j in range(self.multiplot_ncolumns):
                        if plant.isnan(y[i, j]):
                            continue
                        va = 'bottom' if y[i, j] > 0 else 'top'
                        self.plt.text((x_values[j]+width*i),  # +0.4
                                      y[i, j],
                                      y_text[j],
                                      fontsize=self.legend_fontsize,
                                      ha='center',
                                      va=va)
                    '''
                    the_table = self.plt.table(cellText=cell_text,
                      rowLabels=rows,
                      rowColours=colors,
                      colLabels=columns,
                      loc='bottom')
                    '''
                if current_yerr is not None and FLAG_PLOT_BARPLOT_ERROR_VALUES:
                    for j in range(self.multiplot_ncolumns):
                        if plant.isnan(y[i, j]):
                            continue
                        err_value = (y[i, j]+y[i, j]/abs(y[i, j]) *
                                     current_yerr[j])
                        err_str = plant.format_number(
                            err_value,
                            # current_yerr[j],
                            decimal_places=self.decimal_places,
                            sigfigs=self.n_significant_figures)
                        ha = 'left'
                        va = 'bottom' if err_value > 0 else 'top'
                        # if err_value < 0 else 'center'
                        self.plt.text((x_values[j]+width*i),  # +0.4
                                      err_value,
                                      ' '+err_str,
                                      fontsize=self.legend_fontsize,
                                      ha=ha,
                                      va=va)
                        err_value = (y[i, j]-y[i, j]/abs(y[i, j]) *
                                     current_yerr[j])
                        if err_value * y[i, j] > 0:
                            # same signal
                            continue
                        va = 'bottom' if err_value > 0 else 'top'
                        err_str = plant.format_number(
                            err_value,
                            # current_yerr[j],
                            decimal_places=self.decimal_places,
                            sigfigs=self.n_significant_figures)
                        # ha = 'left' if err_value < 0 else 'center'
                        self.plt.text((x_values[j]+width*i),  # +0.4
                                      err_value,
                                      ' '+err_str,
                                      fontsize=self.legend_fontsize,
                                      ha=ha,
                                      va=va)
                #     continue
                # rowColours=color,
                #  yerr=current_yerr,
                # rowColours=color
                #  edgecolor=edgecolor,
                #  ecolor=self.linecolor,
                #  width=width)
                '''
                if current_yerr is not None:
                    continue
                for j in range(self.multiplot_ncolumns):
                    self.plt.text(x_values[j]+width*i,  # +0.4
                             y[i, j],
                             y_text[j],
                             ha='center',
                            va='bottom')
                '''
            elif not self.multiplot_type == 'table':
                # marker = self.marker if self.marker is not None else 'o'
                if self.marker is not None:
                    marker = self.marker
                elif len(x_values) <= 32:
                    marker = 'o'
                else:
                    marker = ''
                handles = self.plt.errorbar(x_values,
                                            y[i, :],
                                            # x_err=1,
                                            yerr=current_yerr,
                                            marker=marker,
                                            hatch=hatch,
                                            # edgecolor=edgecolor,
                                            color=color,
                                            # linecolor=color,
                                            # ecolor=data_obj.linecolor,
                                            markersize=self.markersize,
                                            markeredgecolor=self.edgecolor,
                                            linestyle=linestyle,
                                            linewidth=self.linewidth)

            if not self.multiplot_type == 'table':
                self.plot_handles_list.append(handles)
            else:
                y_text_list.append(y_text)
            # plot_name = ', '.join([self.data[x].name
            #                       for x in range(self.input_number)])
            plot_name = y_name[i]

            self.plot_names_list.append(plot_name)

            if flag_keep_original_x_axis:
                continue
            if self.multiplot:
                ind = [c for c, elem in enumerate(x_values)
                       if elem not in xticks_list]
                if len(ind) > 0:
                    xticks_element = [x_values[c] for c in ind]
                    xticklabels_element = [x[i][c] for c in ind]
                else:
                    xticks_element = []
                    xticklabels_element = []
            else:
                # xticks_pos = [0.65*patch.get_width() +
                # patch.get_xy()[0] for patch in h]
                # xticks_element = [0.5*patch.get_width()+patch.get_xy()[0]
                #                   for patch in handles]
                # print(xticks_element)
                xticks_element = (x_values+width*i).tolist()
                # print(xticks_element_2)
                # xticklabels_element = x[i]
                xticklabels_element = x[i]
            xticks_list += xticks_element
            xticklabels_list += xticklabels_element

        if self.multiplot_type == 'table':
            self.ax.axis('tight')
            self.ax.axis('off')
            if len(x) == 1:
                col_labels = x[0]
                row_labels = None
                col_widths = [0.25]*(len(y[0])+1)
            else:
                col_labels = self.plot_x_list
                row_labels = y_name
                col_widths = [0.25]*(len(y[0])+1)
            handles = self.plt.table(cellText=y_text_list,
                                     rowLabels=row_labels,
                                     colLabels=col_labels,
                                     colWidths=col_widths,
                                     # rowColours=self.plot_y_color_list,
                                     # cellLoc='center',
                                     # rowLoc='center',
                                     # colLoc='center',
                                     loc='center')

        if not flag_keep_original_x_axis:
            '''
            if not x_is_numeric and not self.multiplot_type == 'table':
                max_len = max([len(str(x_str)) for x_str in xticklabels_list])
                # print('*** max_len: ', max_len)
                if max_len > 5:
                    self.fig.autofmt_xdate()
                    # xticks_rotation = '45'  # 'vertical'
                else:
                    xticks_rotation = 'horizontal'
                    ''
                    if (xticks_rotation == 'horizontal' or
                        (plant.isnumeric(xticks_rotation) and
                         float(xticks_rotation) < 30)):
                    else:
                    ha = 'right'
                    ''
                    ha = 'center'
                    self.plt.xticks(rotation=xticks_rotation, ha=ha)
                    '''
            # else:
            # if self.multiplot_type == 'bar':
            self.ax.set_xticks(xticks_list)  # 0.4
            self.ax.set_xticklabels(xticklabels_list, fontsize=self.fontsize)

        x_min, x_max = self.ax.get_xlim()
        y_min, y_max = self.ax.get_ylim()

        vmin = [self.data[i].vmin
                if self.data[i].vmin is not None else np.nan
                for i in range(self.input_number)]
        vmax = [self.data[i].vmax
                if self.data[i].vmax is not None else np.nan
                for i in range(self.input_number)]
        if all(plant.isnan(vmin)):
            vmin = y_min
        else:
            vmin = np.nanmin(vmin)
        if all(plant.isnan(vmax)):
            vmax = y_max
        else:
            vmax = np.nanmax(vmax)
        y_range = y_max - y_min
        # if flag_keep_original_x_axis:
        extrapolation_factor = 0
        # else:
        #     extrapolation_factor = 0.005
        # print(vmin-y_range*extrapolation_factor,
        #       vmax+y_range*extrapolation_factor)
        self.ax.set_ylim(vmin-y_range*extrapolation_factor,
                         vmax+y_range*extrapolation_factor)
        x_range = x_max - x_min
        self.ax.set_xlim(x_min-x_range*extrapolation_factor,
                         x_max+x_range*extrapolation_factor)

        if self.out_text is not None:
            x = [item for sublist in x for item in sublist]
            y = [item for sublist in y for item in sublist]
            if self.multiplot_nlines == 1:
                plant.save_text(x, y, 'x', 'y', self.out_text,
                                force=self.force)
            else:
                lines = [int(line) for line in range(self.multiplot_nlines)
                         for column in range(self.multiplot_ncolumns)]
                name_list = [y_name[line] for line in lines]
                # plant.debug(lines)
                # lines = [item for sublist in lines
                #         for item in sublist]
                # plant.debug(lines)
                # lines = [i for t in range(self.multiplot_nlines *
                #                          self.multiplot_ncolumns)]
                # plant.debug(lines)
                plant.save_text(lines, name_list, x, y,
                                'line', 'name', 'x', 'y',
                                self.out_text,
                                force=self.force)
            # self.out_text = False
            # self.ax.set_xticks(ind+0.25)
            # self.ax.set_xticklabels((ind))
            # barlist = self.plt.bar(ind+0.4, y)
            # for i in range(self.input_number):
            #    barlist[i].set_color(self.data[i].color)
            # self.ax.set_xticks(ind + 0.8)
            # self.plt.bar(ind+width, y, yerr=yerr, ecolor='b', color='w')
            # self.ax.errorbar(ecolor='b', capthick=2)

    def plot_hist2d_loop(self):
        if self.input_number < 2:
            self.print('ERROR option "%s" requires at least two inputs'
                       % self.type)
            return
        if (self.linecolor is None and
                self.input_number == 2):
            self.linecolor = 'gray'
        '''
        input_names_none = [path.basename(self.data[im].filename)
                            for im in range(1, len(self.data))
                            if self.data[im].name is None]
        if len(input_names_none) != 0:
            new_input_names = plant.get_image_names(
                input_names_none,
                max_name_size=self.max_name_size,
                update_names_larger_than=UPDATE_NAMES_LARGER_THAN)
            im_none_count = 0
            for im in range(1, self.input_number):
                if self.data[im].name is not None:
                    continue
                self.data[im].name = new_input_names[im_none_count]
                im_none_count += 1
        '''
        # self.data[0].data = self.data[0].data[np.where(self.mask)]
        self.n_plots = 0
        vmin = self.data[1].vmin
        vmax = self.data[1].vmax
        nbins_x = self.data[0].nbins
        nbins_y = self.data[1].nbins
        for i in range(1, self.input_number):
            # self.data[i].data = self.data[i].data[np.where(self.mask)]
            if i == 1:
                continue
            vmin = np.nanmin([vmin, self.data[i].vmin])
            vmax = np.nanmax([vmax, self.data[i].vmax])
            nbins_y = np.nanmin([nbins_y, self.data[i].nbins])
        plot_range_x = [self.data[0].vmin, self.data[0].vmax]
        plot_range_y = [vmin, vmax]
        nbins_x, nbins_y = self.get_plot_orientation(nbins_x, nbins_y)
        plot_range_x, plot_range_y = self.get_plot_orientation(plot_range_x,
                                                               plot_range_y)
        # self.print('## hist2d options:')
        self.print('X range: [%f, %f]' % (plot_range_x[0],
                                          plot_range_x[1]))
        self.print('X nbins: %d' % nbins_x)
        self.print('Y range: [%f, %f]' % (plot_range_y[0],
                                          plot_range_y[1]))
        self.print('Y nbins: %d' % nbins_y)
        self.print('normed: '+str(self.sum_normed))
        self.print('log: '+str(self.hist_log))

        '''
        if self.cmin is not None:
            self.print('cmin: '+str(self.cmin))
        if self.cmax is not None:
            self.print('cmax: '+str(self.cmax))
        '''

        for i in range(1, self.input_number):
            if self.data[i].cmap is not None:
                cmap = self.data[i].cmap
            elif self.input_number == 2:
                # cmap = 'jet'
                cmap = 'viridis'
            else:
                cmap = plant.get_cmap_display(
                    0, dark_theme=self.dark_theme)
            cmin = self.data[i].cmin
            cmax = self.data[i].cmax
            cpercentile = self.data[i].cpercentile
            if self.colorbar_label is not None:
                colorbar_label = self.colorbar_label
            elif self.input_number > 2:
                colorbar_label = self.data[i].name
            else:
                colorbar_label = None
            ind = np.where(plant.isvalid(self.data[i].data))
            # data_x = data_x[ind]
            # data_y = data_y[ind]
            args = self.get_plot_orientation(self.data[0].data[ind],
                                             self.data[i].data[ind])
            self._plot_hist2d(*args,
                              nbins_x, nbins_y, plot_range_x,
                              plot_range_y, cmap, cmin, cmax,
                              cpercentile,
                              self.data[i].name,
                              self.data[i].filename, colorbar_label,
                              y_data_counter=i+1,
                              flag_transparency=not(self.input_number <= 2))
        self.colorbar = False

    def _plot_hist2d(self, data_x, data_y, nbins_x, nbins_y,
                     plot_range_x, plot_range_y, cmap, cmin, cmax,
                     cpercentile, name, filename, colorbar_label,
                     y_data_counter=None, flag_transparency=True):
        kwargs = {}
        # plot_range_y = [p if plant.isvalid(p) else None
        #                 for p in plot_range_y]
        if (all(plant.isvalid(plot_range_x)) and
               all(plant.isvalid(plot_range_y))):
            kwargs['range'] = [plot_range_x, plot_range_y]
        ret = np.histogram2d(data_x, data_y,
                             bins=(nbins_x, nbins_y),
                             normed=self.sum_normed,
                             **kwargs)
        counts, xedges, yedges = ret

        if self.hist2d_normalize_x_max:
            for j in range(counts.shape[1]):
                vmin = np.nanmin(counts[:, j])
                vmax = np.nanmax(counts[:, j])
                vmean = vmin
                counts[:, j] = plant.scale_data(counts[:, j],
                                                vmin,
                                                vmax,
                                                vmean=vmean)
        elif self.hist2d_normalize_y_max:
            for i in range(counts.shape[0]):
                vmin = np.nanmin(counts[i, :])
                vmax = np.nanmax(counts[i, :])
                vmean = vmin
                counts[i, :] = plant.scale_data(counts[i, :],
                                                vmin,
                                                vmax,
                                                vmean=vmean)
        # hist2d_normalize_sum
        # cmin, cmax = self.cmin, self.cmax
        cmin_str = ''
        if cmin is None and not self.hist_log:
            cmin = 0
        elif cmin is None:
            ind = np.where(counts <= 0)
            counts[ind] = np.nan
            cmin = np.nanpercentile(counts, 100-self.cpercentile)
            counts[ind] = cmin
            counts[np.where(counts <= cmin)] = cmin
            cmin_str = f' (--c-percentile: {self.cpercentile}%)'
        cmax_str = ''
        if cmax is None and not self.sum_normed:
            cmax = np.max([1, np.nanpercentile(counts, self.cpercentile)])
            cmax_str = f' (--c-percentile: {self.cpercentile}%)'
        elif cmax is None:
            cmax = np.nanpercentile(counts, self.cpercentile)
            cmax_str = f' (--c-percentile: {self.cpercentile}%)'
        if ((plant.isnan(cmin) or plant.isnan(cmax)) and
                np.all(plant.isnan(counts))):
            self.print('WARNING no valid element found for {filename}.'
                       ' Skipping image...')
            return

        if plant.isnan(cmin):
            cmin = 0
        counts = counts.T
        if plant.isnan(cmax) or cmin == cmax:
            cmax = cmin + 1
        cmap_obj = self.plt.get_cmap(cmap)
        alpha_function = np.linspace(0, 1, cmap_obj.N)
        if flag_transparency:
            cmap_array = cmap_obj(np.arange(cmap_obj.N))
            background = cmap_array[0, :]
            cmap_array[:, -1] = alpha_function
            cmap_new = ListedColormap(cmap_array)
            cmap_array = cmap_obj(np.arange(cmap_obj.N))
            for i in range(3):
                new_array = cmap_array[:, i]
                new_array = (alpha_function*new_array +
                             (1-alpha_function)*background[i])
                cmap_array[:, i] = new_array
            cmap_array = np.clip(cmap_array, 0, 1)
            cmap_colorbar = ListedColormap(cmap_array)
        else:
            cmap_new = cmap
            cmap_colorbar = cmap
        if not self.hist_log:
            norm = mpl.colors.Normalize(vmin=cmin, vmax=cmax)
        else:
            norm = mpl.colors.LogNorm(vmin=cmin, vmax=cmax)
        xcenters = (xedges[:-1]+xedges[1:])/2
        ycenters = (yedges[:-1]+yedges[1:])/2
        if y_data_counter is not None:
            self.print('## input %d: %s' % (y_data_counter, filename))
        else:
            self.print('## input: %s' % (filename))
        with plant.PlantIndent():
            self.print('cmap: %s' % str(cmap))
            self.print(f'cmin: {cmin}{cmin_str}')
            self.print(f'cmax: {cmax}{cmax_str}')
            self.n_plots += 1

            if self.background_color is not None:
                self.print(f'background color: {self.background_color}')
                cmap_new = self.plt.get_cmap(cmap_new)
                cmap_new.set_bad(self.background_color, 0)
                cmap_obj.set_bad(self.background_color, 0)
                counts[np.where(counts == 0)] = np.nan
            counts = plant.filter_data(counts, mean=3)

            if (flag_transparency and
                    y_data_counter == 2 and
                    cmap not in plant.CMAP_LIGHT_LIST):
                im = mpl.image.NonUniformImage(
                    self.ax,
                    interpolation=self.interpolation,
                    cmap=cmap_obj)
                im.set_data(xcenters, ycenters, counts*0+cmin)
                self.ax.images.append(im)
            im = mpl.image.NonUniformImage(self.ax, cmap=cmap_new,
                                           norm=norm)
            im.set_data(xcenters, ycenters, counts)
            plot_range_x_orig, plot_range_y_orig = \
                self.get_plot_orientation(plot_range_x, plot_range_y)
            if all(plant.isvalid(plot_range_x_orig)):
                self.ax.set_xlim(plot_range_x_orig)
            if all(plant.isvalid(plot_range_y_orig)):
                self.ax.set_ylim(plot_range_y_orig)
            self.ax.images.append(im)

            if self.colorbar is None or self.colorbar:
                # self.colorbar = True
                cbar = plant.insert_colorbar(
                    self.plt, cmap_colorbar, cmin, cmax,
                    norm=norm,
                    # name=name,
                    decimal_places=self.decimal_places,
                    n_significant_figures=self.n_significant_figures,
                    flag_in_line=self.flag_colorbar_in_line,
                    flag_db=self.colorbar_db,
                    fontsize=self.fontsize)
                if colorbar_label is not None:
                    cbar.ax.set_ylabel(colorbar_label,
                                       fontsize=self.fontsize)

    def plot_geolocation(self):
        position = self.name_position.strip()
        if (position == 'left' or
                position == 'right' or
                position == 'center'):
            position = 'center '+position
        position = position.split(' ')
        if len(position) != 2:
            self.print('ERROR invalid name position: %s'
                       % (self.name_position))
        vertical_pos, horizontal_pos = position
        flag_projected = None
        for im in range(0, self.input_number):
            ret_dict = plant.get_coordinates(
                geotransform=self.data[im].geotransform,
                projection=self.data[im].projection,
                lat_size=self.data[im].length,
                lon_size=self.data[im].width)
            if ret_dict is None:
                continue
            print('self.data[im].projection: ',
                  self.data[im].projection)
            if (flag_projected is None and
                    self.data[im].projection):
                flag_projected = plant.is_projected(
                    self.data[im].projection)
            elif self.data[im].projection:
                flag_projected_temp = plant.is_projected(
                    self.data[im].projection)
                if flag_projected != flag_projected_temp:
                    self.print('ERROR input projections do not'
                               ' match')
                    return
            
            # if ret is not None:
            lat_arr = ret_dict['lat_arr']
            lon_arr = ret_dict['lon_arr']
            # step_lat = ret_dict['step_lat']
            # step_lon = ret_dict['step_lon']
            # lat_size = ret_dict['lat_size']
            # lon_size = ret_dict['lon_size']
            if im == 0:
                min_lat, max_lat = lat_arr
                min_lon, max_lon = lon_arr
            if lat_arr[0] < min_lat:
                min_lat = lat_arr[0]
            if lat_arr[1] > max_lat:
                max_lat = lat_arr[1]
            if lon_arr[0] < min_lon:
                min_lon = lon_arr[0]
            if lon_arr[1] > max_lon:
                max_lon = lon_arr[1]
            # self.data[im].data
            # self.plt.axhline(y=data_obj.mean - data_obj.stddev,
            #            color=data_obj.color_stddev, alpha=alpha,
            #            linewidth=self.linewidth,
            #            linestyle=self.linestyle)
            # for x, y, xe, ye in zip(x_data, ydata, xerror.T, yerror.T):
            
            rect = Rectangle((lon_arr[0], lat_arr[0]),
                             lon_arr[1]-lon_arr[0],
                             lat_arr[1]-lat_arr[0],
                             # facecolor=self.data[im].color,
                             color=self.data[im].color,
                             # label=self.data[im].name,
                             linewidth=self.linewidth,
                             # edgecolor='k',
                             alpha=self.alpha)
            if 'center' in horizontal_pos:
                x_label_pos = (lon_arr[1]+lon_arr[0])/2
                horizontal_alignment = 'center'
            elif 'left' in horizontal_pos:
                x_label_pos = lon_arr[0]
                horizontal_alignment = 'left'
            else:
                x_label_pos = lon_arr[1]
                horizontal_alignment = 'right'

            if 'center' in vertical_pos:
                y_label_pos = (lat_arr[1]+lat_arr[0])/2
                vertical_alignment = 'center'
            elif 'lower' in vertical_pos:
                y_label_pos = lat_arr[0]
                vertical_alignment = 'bottom'
            else:
                y_label_pos = lat_arr[1]
                vertical_alignment = 'top'

            self.ax.text(x_label_pos,
                         y_label_pos,
                         self.data[im].name,
                         horizontalalignment=horizontal_alignment,
                         verticalalignment=vertical_alignment,
                         fontsize=int(self.fontsize*2/3),
                         alpha=self.alpha,
                         color='k')
            self.ax.add_patch(rect)

            # self.plt.text(xy[0],
            #         y,
            #         text,
            #         ha="center",
            #         family='sans-serif',
            #         size=14)
            # patches.append(rect)
            # pc = PatchCollection([rect],
            #                     facecolor=self.data[im].color,
            #                     alpha=alpha,
            #                     edgecolor='k')
            # self.ax.add_collection(pc)

        # edgecolor='b'
        # Create patch collection with specified colour/alpha

        extra_factor = 0.1
        extra_lon = (max_lon - min_lon)*extra_factor
        extra_lat = (max_lat - min_lat)*extra_factor

        min_lon = min_lon-extra_lon
        max_lon = max_lon+extra_lon
        min_lat = min_lat-extra_lat
        max_lat = max_lat+extra_lat
        self.ax.set_ylim(min_lat, max_lat)
        self.ax.set_xlim(min_lon, max_lon)
        # flag_projected = plant.is_projected(projection)

        if flag_projected is not True and self.label_x is None:
            self.label_x = 'Longitude [deg]'
        elif self.label_x is None:
            self.label_x = 'Easting ´[m]'
        if flag_projected is not True and self.label_y is None:
            self.label_y = 'Latitude [deg]'
        elif self.label_y is None:
            self.label_y = 'Northing [m]'
        # Add collection to axes
        # self.plt.show()

    def plot_generic(self):
        if self.input_number < 2 and self.first_image_as_x:
            self.print('ERROR this option requires at least two inputs')
            return

        dtype = plant.get_dtype_name(self.data[0].data)
        xticklabels = None
        if 'str' in dtype:
            xticklabels = self.data[0].data.ravel()
            length = self.data[1].data.ravel().shape[0]
            self.data[0].data = \
                np.arange(length).reshape((self.data[1].data.shape))
            self.data[0].name = ''
            self.data[0].vmax = length-1+0.1*length
            self.data[0].vmin = 0-0.1*length
            self.data[0].nbins = length

        if not self.first_image_as_x:
            for i in range(0, self.input_number):
                self.data[self.input_number-i] = \
                    self.data[self.input_number-i-1]
            self.input_number += 1
            self.data[0] = DataDisplay()
            length = self.data[1].data.ravel().shape[0]
            if length > GENERIC_PLOT_MAX_NPOINTS:
                self.print('ERROR too many points for seleted option.'
                           ' Please, verify inputs.')
                return
            self.data[0].data = \
                np.arange(length).reshape((self.data[1].data.shape))
            self.data[0].name = ''
            self.data[0].vmax = length-1+0.1*length
            self.data[0].vmin = 0-0.1*length
            self.data[0].nbins = length
            self.first_image_as_x = True
        '''
        input_names_none = [path.basename(self.data[im].filename)
                            for im in range(1, len(self.data))
                            if self.data[im].name is None]
        if len(input_names_none) != 0:
            new_input_names = plant.get_image_names(
                input_names_none,
                max_name_size=self.max_name_size,
                update_names_larger_than=UPDATE_NAMES_LARGER_THAN)
            im_none_count = 0
            for im in range(1, self.input_number):
                if self.data[im].name is not None:
                    continue
                self.data[im].name = new_input_names[im_none_count]
                im_none_count += 1
        '''

        ind = np.where(plant.isvalid(self.data[0].data))
        for im in range(self.input_number):
            # for im in range(len(self.data)):
            # print('*** before ', self.data[im].data.shape)
            self.data[im].data = self.data[im].data[ind]
            # print('*** after ', self.data[im].data.shape)

        shape = self.data[0].data.shape
        n_elements = plant.get_n_elements_from_shape(shape)

        if (n_elements > GENERIC_PLOT_MAX_NPOINTS and
                not self.generic_plot_all and
                not self.trendplot):
            self.print('WARNING too many points: %d. Selecting '
                       'a random subset of %d points. To disable this '
                       'selection please use: --all-points.'
                       % (n_elements, GENERIC_PLOT_MAX_NPOINTS))
            ind = np.random.choice(n_elements,
                                   GENERIC_PLOT_MAX_NPOINTS)
            for im in range(self.input_number):
                self.data[im].data = (self.data[im].data.ravel())[ind]

        # if (n_elements > GENERIC_PLOT_MAX_NPOINTS and
        #        not plant_config.flag_all and
        #        not self.foce):
        #    while 1:
        #        res = input('WARNING Too many points (%d). Would '
        #                    'you like to select a smaller subset
        #                    Files ' + ', '.join(filelist) +
        #                    ' already exist. Do you want to overwrite'
        #                    ' them? ([y]es/[n]o)/[A]ll/[N]one ')
        #        if res.startswith('n'):
        #            return False
        #        elif res.startswith('N'):
        #            flag_never[0] = True
        #            return
        #        elif res.startswith('y'):
        #            return True
        #        elif res.startswith('A'):
        #            flag_all[0] = True
        #            return True

        for im in range(self.input_number):
            # print('***: ', plant.get_dtype_name(self.data[im].data))
            if plant.get_dtype_name(self.data[im].data) != 'datetime':
                if (self.median or self.mean or
                    (self.mode and not self.type == HISTOGRAM) or
                        self.stddev):
                    self.print(f'stats of {self.data[im].name}:')
                self.calculate_statistics(self.data[im])
            # self.print('')
        # self.print('## '+self.text+' options:')
        if (self.colorbar_label is None and
                self.plot_color_mode == 5 and
                self.flag_periodic_trendplot):
            if self.sum_normed:
                self.print(f'normed: {self.sum_normed} (min)')
            elif self.max_normed:
                self.print(f'normed: {self.max_normed} (max)')

        # nbins = self.data[0].nbins
        # np.seterr(all='ignore')

        # GENERIC_PLOT options
        # if (self.plot_color_mode is None and
        #        self.color is not None):
        #    self.plot_color_mode=0

        if (self.plot_color_mode is None and
                self.input_number == 3 and
                not self.generic_plot_3d):
            self.plot_color_mode = 1
        elif self.plot_color_mode is None:
            self.plot_color_mode = 0

        # elif (self.plot_color_mode is None):
        #     self.plot_color_mode=1
        # if self.plot_color_mode==3:
        #    n_plots = self.input_number-2
        # else:

        self.n_plots = (self.input_number-1 -
                        int(self.plot_color_mode == 1) -
                        int(self.generic_plot_3d))

        '''
        if (self.n_plots < 1 and
                not self.generic_plot_3d):
            self.print('ERROR insufficient input files for '
                       'color mode = ' +
                       str(self.plot_color_mode))
            sys.exit(1)
        elif self.n_plots < 1:
            self.print('ERROR insufficient input files for '
                       'color mode = ' +
                       str(self.plot_color_mode))
            sys.exit(1)
        '''

        if (self.plot_color_mode == 4 and
                not self.generic_plot_3d):
            self.print('ERROR trendplot color mode ' +
                       str(self.plot_color_mode) +
                       ' only available for 3D mode')
            return

        plot_names_list = [self.data[im].name
                           for im in range(1+int(self.generic_plot_3d),
                                           1+int(self.generic_plot_3d) +
                                           self.n_plots)]

        # for legends (may include other plots, e.g. polyfit):
        self.plot_names_list = plot_names_list[:]

        if (self.label_x is None):
            self.label_x = self.data[0].name

        # if self.data[0].filename is not None:
        #    label_y = ''
        # el
        if not self.generic_plot_3d:
            label_y = ' '.join(plot_names_list)
            if self.label_y is None:
                self.label_y = label_y
        else:
            if self.label_y is None:
                self.label_y = self.data[1].name
            label_z = ' '.join(plot_names_list)
            if self.label_z is None:
                self.label_z = label_z

        if (self.colorbar_label is None and
                self.plot_color_mode == 2):
            self.colorbar_label = self.data[0].name
        elif (self.colorbar_label is None and
              self.plot_color_mode == 1):
            self.colorbar_label = self.data[self.input_number-1].name

        elif (self.colorbar_label is None and
              self.plot_color_mode == 5 and
              self.flag_periodic_trendplot):
            self.colorbar_label = 'number of points'
            if self.sum_normed:
                self.colorbar_label += ' (sum normalized)'
            elif self.max_normed:
                self.colorbar_label += ' (max normalized)'
        elif (self.colorbar_label is None and
              self.plot_color_mode == 5):
            self.colorbar_label = '# averaged points'
            if self.sum_normed:
                self.colorbar_label += ' (normalized)'

        elif (self.colorbar_label is None and
              not self.generic_plot_3d):
            self.colorbar_label = label_y
        elif self.colorbar_label is None:
            self.colorbar_label = label_z

        min_x_range = self.data[0].vmin
        max_x_range = self.data[0].vmax
        vmin = self.data[0].vmin
        vmax = self.data[0].vmax

        if plant.isnan(vmin) or plant.isnan(vmax):
            if plant.isnan(vmin):
                vmin = np.nanmin(self.data[0].data)
            if plant.isnan(vmax):
                vmax = np.nanmax(self.data[0].data)
        # self.mask = 0
        n_selected_elements = self.data[0].data.shape[0]

        # self.print('alpha = '+str(self.alpha))
        self.print('number of selected elements: ' +
                   str(n_selected_elements))

        if self.median:
            self.print('mode: median')
        elif self.mode:
            self.print('mode: mode')
        elif self.stddev and not self.mean:
            self.print('mode: stddev')
        else:
            self.print('mode: mean')

        flag_show_yerr = ((self.median or self.mode or self.mean) and
                          self.stddev)

        self.print('# plots: '+str(self.n_plots))
        self.print('color mode: '+str(self.plot_color_mode))

        if self.trendplot:
            nbins = self.data[0].nbins
            # if nbins is None:
            #     nbins = DEFAULT_NBINS_TRENDPLOT
            step = (vmax - vmin)/nbins
            ind_mask = np.arange(n_selected_elements)
            pivots = np.arange(vmin, vmax+step, step)
            n_pivots = pivots.shape[0]
            self.print(f'nbins (--nbins): {nbins}')
            if not self.flag_periodic_trendplot:
                self.print('# plot points per bin (--np): ' +
                           str(self.trendplot_np))
                self.print('# averaged pixels per plot point (-n): ' +
                           str(self.trendplot_n))
                self.print('# plot points for each plot: ' +
                           str(n_pivots * self.trendplot_np))
                self.print('# plot points (all plots): ' +
                           str(n_pivots * self.trendplot_np * self.n_plots))

        # if (ind_mask.shape[0]==0):
        #    self.print('ERROR no valid elements after --min/--max selection')
        #    sys.exit(1)

        # data_0 = self.data[0].data # [ind_mask]
        # self.data[0].data = 0
        min_y_range = None
        max_y_range = None

        # first look for range set by user
        for im in range(int(self.first_image_as_x)+int(self.generic_plot_3d),
                        self.n_plots+int(self.first_image_as_x)):
            if (self.data[im].flag_vmin_set_by_user and
                    plant.isnan(min_y_range)):
                min_y_range = self.data[im].vmin
            elif self.data[im].flag_vmin_set_by_user:
                min_y_range = min(min_y_range,
                                  self.data[im].vmin)
            if (self.data[im].flag_vmax_set_by_user and
                    plant.isnan(max_y_range)):
                max_y_range = self.data[im].vmax
            elif self.data[im].flag_vmax_set_by_user:
                max_y_range = max(max_y_range,
                                  self.data[im].vmax)

        # if range is still None, read range from data obj.
        if min_y_range is None:
            for im in range(int(self.first_image_as_x) +
                            int(self.generic_plot_3d),
                            self.n_plots+int(self.first_image_as_x)):
                if plant.isnan(min_y_range):
                    min_y_range = self.data[im].vmin
                else:
                    min_y_range = min(min_y_range,
                                      self.data[im].vmin)
        if max_y_range is None:
            for im in range(int(self.first_image_as_x) +
                            int(self.generic_plot_3d),
                            self.n_plots+int(self.first_image_as_x)):
                if plant.isnan(max_y_range):
                    max_y_range = self.data[im].vmax
                else:
                    max_y_range = max(max_y_range,
                                      self.data[im].vmax)

        if self.generic_plot or self.scatterplot:
            data_x = self.data[0].data.tolist()
            data_n = 1
            data_y = [self.data[im+int(self.first_image_as_x)].data.tolist()
                      for im in range(self.input_number-1)]
            data_yerr = None
        else:
            data_x = []
            data_n = []  # if self.points_size is None else self.points_size
            data_y = [[] for x in range(self.input_number-1)]
            ind_list = []

            if flag_show_yerr:
                data_yerr = [[] for x in range(self.input_number-1)]
            else:
                data_yerr = None
            for i, p in enumerate(pivots):
                p_data_ind = np.where(np.logical_and(
                    (self.data[0].data) >= p,
                    (self.data[0].data) < p+step))
                if self.flag_periodic_trendplot:
                    ndata = plant.get_indexes_len(p_data_ind)
                    if ndata == 0:
                        continue
                    data_n.append(ndata)
                    data_x.append(p+step/2)
                    ind_list.append(p_data_ind)
                    continue
                p_data_0 = (self.data[0].data)[p_data_ind]
                ndata = plant.get_indexes_len(p_data_0)
                if (ndata < 1):
                    continue
                if ndata == 1:
                    np_ind = np.asarray([0])
                else:
                    np_ind = np.random.randint(
                        0, ndata-1,
                        size=min(self.trendplot_np, ndata))
                for j in range(plant.get_indexes_len(np_ind)):
                    c = np.random.uniform(step/2)
                    np_data_x = p_data_0[np_ind[j]]
                    np_data_ind = np.where(np.logical_and(
                        (self.data[0].data) >= np_data_x-c,
                        (self.data[0].data) < np_data_x+c))
                    np_data_0 = (self.data[0].data)[np_data_ind]
                    ndata = plant.get_indexes_len(np_data_0)
                    if ndata < self.trendplot_min_n:
                        continue
                    np_ind_mask = ind_mask[np_data_ind]
                    if ndata == 1:
                        n_ind = np.asarray([0])
                    else:
                        n_ind = np.random.randint(
                            0, ndata-1,
                            size=min(self.trendplot_n, ndata))
                    # remove n_ind from np_data_0?
                    # valid=np.where(plant.isvalid(np_data_0[n_ind]))
                    ndata = plant.get_indexes_len(n_ind)
                    if (ndata == 0):
                        continue
                    if self.trendplot_np == 1:
                        point_value = p+step/2
                    elif self.median:
                        point_value = np.nanmedian(np_data_0[n_ind])
                    else:
                        point_value = np.nanmean(np_data_0[n_ind])
                    ind_list.append(np_ind_mask[n_ind])
                    data_x.append(point_value)
                    data_n.append(ndata)

            for i in range(len(data_x)):
                for im in range(1, self.input_number):
                    if self.stddev or flag_show_yerr:
                        stddev_value = \
                            np.nanstd(self.data[im].data[ind_list[i]])
                        if flag_show_yerr:
                            data_yerr[im-1].append(stddev_value)
                    if self.median:
                        data_y[im-1].append(
                            np.nanmedian(self.data[im].data[ind_list[i]]))
                    elif self.mode:
                        data_y[im-1].append(
                            np.nanmax(self.data[im].data[ind_list[i]]))
                    elif self.stddev and not self.mean:
                        data_y[im-1].append(stddev_value)
                    else:
                        data_y[im-1].append(
                            np.nanmean(self.data[im].data[ind_list[i]]))
        self.sort = self.generic_plot
        if self.sort:
            ind = np.argsort(data_x)
            data_x_orig = data_x[:]
            for i in range(len(ind)):
                data_x[i] = data_x_orig[ind[i]]
            for im in range(0, self.input_number-1):
                data_y_orig = list(data_y[im])
                for i in range(len(ind)):
                    data_y[im][i] = data_y_orig[ind[i]]

                if flag_show_yerr:
                    data_yerr_orig = list(data_yerr[im])
                    for i in range(len(ind)):
                        data_yerr[im][i] = data_yerr_orig[ind[i]]

        if self.generic_plot_3d:
            # This import registers the 3D projection.
            # Please, do not remove it
            from mpl_toolkits.mplot3d import Axes3D
            self.ax = self.fig.add_subplot(111, projection='3d')
            data_z = data_y[1:]
            data_y = data_y[0]
        if (self.plot_color_mode == 1 and
                self.generic_plot_3d):
            data_c = data_z.pop()
        elif self.plot_color_mode == 1:
            data_c = data_y.pop()
        elif self.plot_color_mode == 2:
            data_c = data_x
        elif (self.plot_color_mode == 3 and
              self.generic_plot_3d):
            data_c = data_y
        elif (self.plot_color_mode == 5):
            data_c = data_n
            if self.sum_normed:
                data_c /= np.nansum(data_n)
            elif self.max_normed:
                data_c /= np.nansum(data_n)
            elif self.sum_normed_to_value:
                data_c *= self.sum_normed_to_value/np.nansum(data_n)
            elif self.max_normed_to_value:
                data_c *= self.max_normed_to_value/np.nansum(data_n)
        else:
            data_c = []

        if self.markersize is None:
            data_n_factor = 100.0/np.nanmax(data_n)
        else:
            data_n_factor = 1

        # DEFAULT_POINT_SIZE = 30

        if self.markersize is None:
            data_ps = np.asarray(data_n)*data_n_factor
        else:
            data_ps = self.markersize

        for im in range(1, self.n_plots+1):
            # print('*** im', im)
            if (self.plot_color_mode == 0 and
                    self.out_text is not None):
                data_c.append([self.data[im].color
                               for t in range(len(data_x))])
            plot_x_arr, plot_y_arr = self.get_plot_orientation(data_x,
                                                               data_y[im-1])
            if self.marker is not None:
                marker = self.marker
            elif ((plant.get_indexes_len(ind) > 32 and
                   self.generic_plot and
                   not self.generic_plot_3d and
                   self.plot_color_mode == 0) or
                  self.data[im].linewidth == 0):
                marker = ''
            elif self.change_marker:
                marker = plant.MARKER_LIST[(im-1)
                                           % len(plant.MARKER_LIST)]
            else:
                marker = 'o'
            kwargs = {}
            kwargs['alpha'] = self.data[im].alpha
            kwargs['linestyle'] = self.data[im].linestyle

            kwargs_plot = dict(kwargs)
            kwargs_plot['color'] = self.data[im].linecolor
            kwargs_plot['markeredgecolor'] = self.data[im].edgecolor
            kwargs_plot['linewidth'] = self.data[im].linewidth
            # kwargs_plot['edgecolor'] = self.data[im].edgecolor

            kwargs_scatter = dict(kwargs)
            # kwargs_scatter['edgewidth'] = 0
            # kwargs_scatter['color'] = self.data[im].color
            kwargs_scatter['s'] = np.asarray(data_ps, dtype=np.int)
            kwargs_scatter['marker'] = marker
            kwargs_scatter['hatch'] = self.data[im].hatch
            kwargs_scatter['edgecolors'] = self.data[im].edgecolor
            kwargs_scatter['linewidths'] = 1
            # self.data[im].linewidth
            kwargs_scatter['cmap'] = self.data[im].cmap

            if (self.generic_plot and
                    not self.generic_plot_3d and self.plot_color_mode == 0):
                ind = np.where(np.logical_and(plant.isvalid(plot_x_arr),
                                              plant.isvalid(plot_y_arr)))
                self.mappable, = self.plt.plot(
                    np.asarray(plot_x_arr)[ind],
                    np.asarray(plot_y_arr)[ind],
                    marker,
                    **kwargs_plot)
                '''
                    alpha=self.data[im].alpha,
                    markersize=self.data[im].markersize,
                    markeredgecolor=self.data[im].edgecolor,
                    linewidth=self.data[im].linewidth,
                    # linecolor=self.linecolor,
                    linestyle=self.data[im].linestyle,
                    # edgecolor=self.edgecolor,
                    color=self.data[im].linecolor)
                '''

            elif (self.plot_color_mode == 0 and
                    not self.generic_plot_3d):
                self.mappable = self.plt.scatter(
                    plot_x_arr,
                    plot_y_arr,
                    color=self.data[im].color,
                    **kwargs_scatter)
            elif (self.plot_color_mode == 3 and
                  not self.generic_plot_3d):
                self.mappable = self.plt.scatter(
                    plot_x_arr,
                    plot_y_arr,
                    c=plot_y_arr,
                    **kwargs_scatter)
                '''
                marker=marker,
                    alpha=self.data[im].alpha,
                    s=np.asarray(data_ps,
                                 dtype=np.int),
                    edgecolors=self.data[im].edgecolor)
                '''
            elif not self.generic_plot_3d:
                self.mappable = self.plt.scatter(
                    plot_x_arr,
                    plot_y_arr,
                    c=data_c,
                    **kwargs_scatter)
                '''
                    marker=marker,
                    alpha=self.data[im].alpha,
                    s=np.asarray(data_ps, dtype=np.int),
                    edgecolors=self.data[im].edgecolor)
                '''
            elif self.plot_color_mode == 0:
                self.mappable = self.ax.scatter(
                    data_x,
                    data_y,
                    zs=data_z[im-1],
                    color=self.data[im].color,
                    zdir='z',
                    depthshade=False,
                    **kwargs_scatter)
                '''
                    alpha=self.data[im].alpha,
                    s=np.asarray(data_ps,
                                 dtype=np.int),
                    edgecolors=self.data[im].edgecolor)
                '''
            elif self.plot_color_mode == 4:
                self.mappable = self.ax.scatter(
                    data_x,
                    data_y,
                    zs=data_z[im-1],
                    c=data_z[im-1],
                    zdir='z',
                    depthshade=False,
                    **kwargs_scatter)
                '''
                    alpha=self.data[im].alpha,
                    s=np.asarray(data_ps,
                                 dtype=np.int),
                    edgecolors=self.data[im].edgecolor)
                    '''
            else:
                self.mappable = self.ax.scatter(
                    data_x,
                    data_y,
                    zs=data_z[im-1],
                    c=data_c,
                    zdir='z',
                    depthshade=False,
                    **kwargs_scatter)
                '''
                    alpha=self.data[im].alpha,
                    s=np.asarray(data_ps,
                                 dtype=np.int),
                    edgecolors=self.data[im].edgecolor)
                    '''
            # self.plot_names_list.append(data_obj.name)
            if (self.generic_plot and not self.generic_plot_3d and
                    self.plot_color_mode != 0):
                ind = np.where(np.logical_and(plant.isvalid(data_x),
                                              plant.isvalid(plot_y_arr)))
                self.plt.plot(np.asarray(data_x)[ind],
                              np.asarray(plot_y_arr)[ind],
                              linewidth=self.data[im].linewidth,
                              # marker=marker,
                              markersize=self.data[im].markersize,
                              markeredgecolor=self.data[im].edgecolor,
                              # linecolor=self.linecolor,
                              linestyle=self.data[im].linestyle,
                              # color='gray')
                              # edgecolor=self.data[im].edgecolor,
                              color=self.data[im].color)
            if flag_show_yerr and not self.generic_plot_3d:
                self.plt.errorbar(data_x,
                                  plot_y_arr,
                                  # marker='.',
                                  # color=self.data[im].color,
                                  color='k',
                                  linestyle='None',
                                  yerr=data_yerr[im-1],
                                  alpha=self.data[im].alpha,
                                  # ecolor=self.data[im].linecolor,
                                  # linestyle=self.data[im].linestyle,
                                  linewidth=self.data[im].linewidth)

            self.plot_handles_list.append(self.mappable)
            # print(self.plot_handles_list)
        if xticklabels is not None:
            # print(xticklabels.ravel())
            self.ax.set_xticks(np.arange(len(xticklabels)))
            self.ax.set_xticklabels(xticklabels,
                                    fontsize=self.fontsize)
            # fontsize=self.data[im].fontsize)
        '''
        if self.generic_plot_3d:
            min_z_range = min_y_range
            max_z_range = max_y_range
            min_y_range = self.data[1].vmin
            max_y_range = self.data[1].vmax
        '''
        # scatter_data.append(tmp)
        x_min, x_max = self.ax.get_xlim()
        y_min, y_max = self.ax.get_ylim()

        if (not(plant.isvalid(min_x_range))):
            min_x_range = x_min
        if (not(plant.isvalid(max_x_range))):
            max_x_range = x_max
        if (not(plant.isvalid(min_y_range))):
            min_y_range = y_min
        if (not(plant.isvalid(max_y_range))):
            max_y_range = y_max

        # if self.generic_plot_3d:
        #    z_min, z_max = self.ax.get_zlim()
        #    if (not(plant.isvalid(min_z_range))):
        #        min_z_range = z_min
        #    if (not(plant.isvalid(max_z_range))):
        #        max_z_range = z_max
        #    self.ax.set_zlim(min_z_range, max_z_range)

        self.colorbar = self.plot_color_mode > 0
        if (self.colorbar and self.plot_color_mode == 2):
            self.colorbar_range = self.ax.get_xlim()
        elif (self.colorbar and self.plot_color_mode == 3):
            self.colorbar_range = self.ax.get_ylim()
        elif (self.colorbar and self.plot_color_mode == 4):
            self.colorbar_range = self.ax.get_zlim()
        elif (self.colorbar and self.plot_color_mode == 1):
            self.colorbar_range = [self.data[self.input_number-1].vmin,
                                   self.data[self.input_number-1].vmax]

        # if self.colorbar_range is not None:
        #    print('colorbar range: ', self.colorbar_range)

        # ind_lower_n = 0
        # ind_upper_n = 0
        # , 'lower right')
        # self.ax.legend(['A simple line'])
        # self.plt.figlegend( (tmp),
        #               (self.data[1].name),
        #               'upper right' )

        # print('xlim: ', [min_x_range, max_x_range])
        # if not self.stddev:
        self.plt.xlim([min_x_range, max_x_range])
        # print('ylim: ', [min_y_range, max_y_range])
        self.plt.ylim([min_y_range, max_y_range])

        '''
        x_min, x_max = self.ax.get_xlim()
        for im in range(1, self.n_plots+1):
            self.plot_statistics(self.data[im],
                                 text_x_pos=x_min)
        '''

        if (self.legend or
                (self.legend is None and
                 len(self.plot_names_list) > 1 and
                 self.plot_color_mode == 0)):
            '''
            if self.legend_location is None and not self.generic_plot_3d:
                data_x_array = np.array(data_x)
                ind = np.where(np.logical_and(
                    data_x_array >= ((1/2)*min_x_range +
                    (1/2)*max_x_range),
                    data_x_array <= max_x_range))
                data_x_array = 0
                try:
                    for im in range(0, self.n_plots):
                        data = np.array(data_y[im-1])[ind]
                        ind_lower = np.where(np.logical_and(
                            data>=min_y_range,
                            data<=((3/4)*min_y_range +
                            (1/4)*max_y_range)))
                        ind_lower_n += ind_lower[0].shape[0]
                        ind_upper = np.where(np.logical_and(
                            data>((1/4)*min_y_range +
                            (3/4)*max_y_range),
                            data<=max_y_range))
                        ind_upper_n += ind_upper[0].shape[0]
                except:
                    self.legend_location = 'lower right'
                if ind_lower_n < ind_upper_n:
                    self.legend_location = 'lower right'
            if self.legend_location is None:
                self.legend_location = 'upper right'
            '''

            if self.legend_location is None:
                self.legend_location = 'best'
            # font_legend = FontProperties(family=self.fontfamily)
            # print('*** fontfamily: ', self.fontfamily)
            self.plt.legend(handles=self.plot_handles_list,
                            labels=self.plot_names_list,
                            # fontfamily=self.fontfamily,
                            # prop=font_legend,
                            scatterpoints=1,
                            loc=self.legend_location,
                            bbox_to_anchor=self.legend_bbox,
                            fontsize=self.legend_fontsize)
            # bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.
            self.legend = False
            self.legend_location = False

        if self.out_text is not None:
            data_x = data_x * self.n_plots
            if not self.generic_plot_3d:
                data_y = [item for sublist in data_y for item in sublist]
            else:
                data_y = data_y * self.n_plots
                data_z = [item for sublist in data_z for item in sublist]

            # data_n = data_n * self.n_plots
            if self.plot_color_mode == 2:
                data_c = data_y
            else:
                try:
                    data_c = [item for sublist in data_c for item in sublist]
                except:
                    pass
                data_c *= self.n_plots
            # print(data_x, data_y, data_c, data_n)
            if not self.generic_plot_3d:
                plant.save_text(data_x, data_y, data_c,  # data_n,
                                'x', 'y', 'c',  # 'n',
                                self.out_text,
                                force=self.force)
            else:
                plant.save_text(data_x, data_y, data_z, data_c,  # data_n,
                                'x', 'y', 'z', 'c',  # 'n',
                                self.out_text,
                                force=self.force)
            self.out_text = False

        '''
        if self.out_text is not None:
            if self.plot_color_mode  == 2:
                data_c = data_y
            else:
                try:
                    data_c = [item for sublist in data_c for item in sublist]
                except:
                    pass

            if not self.generic_plot_3d and (self.plot or self.scatterplot):
                args = ([data_x] + data_y + ['x'] +
                        self.plot_names_list)
            elif self.generic_plot_3d and (self.plot or self.scatterplot):
                args = ([data_x, data_y] + data_z + ['x', 'y'] +
                        self.plot_names_list)
            elif not self.generic_plot_3d:
                args = ([data_x] + data_y + [data_n,
                                             'x'] +
                        self.plot_names_list + ['n'])
            else:
                args = ([data_x, data_y] + data_z + [data_n,
                        'x', 'y'] + self.plot_names_list + ['n'])

            plant.save_text(*args,
                            self.out_text,
                            force=self.force)
        '''

        # self.input_number = self.n_plots
        self.data[0].data = np.asarray(data_x)
        if not (self.generic_plot or self.scatterplot):
            self.data[0].ndata = np.asarray(data_n)
        for im in range(int(self.first_image_as_x),
                        int(self.first_image_as_x)+self.n_plots):
            self.data[im].data = np.asarray(
                data_y[im-int(self.first_image_as_x)])
            if not (self.generic_plot or self.scatterplot):
                self.data[im].ndata = np.asarray(data_n)

    '''
    def _blit_draw(self, artists, bg_cache):
        # Handles blitted drawing, which renders only the artists given instead
        # of the entire figure.
        updated_ax = []
        for a in artists:
            # If we haven't cached the background for this axes object, do
            # so now. This might not always be reliable, but it's an attempt
            # to automate the process.
            if a.axes not in bg_cache:
                # bg_cache[a.axes] = a.figure.canvas.copy_from_bbox(
    a.axes.bbox)
                # change here
                bg_cache[a.axes] = a.figure.canvas.copy_from_bbox(
    a.axes.figure.bbox)
            a.axes.draw_artist(a)
            updated_ax.append(a.axes)

        # After rendering all the needed artists, blit each axes individually.
        for ax in set(updated_ax):
            # and here
            # ax.figure.canvas.blit(ax.bbox)
            ax.figure.canvas.blit(ax.figure.bbox)
    '''
    def _set_animation_title(self, title, ax):
        title_artist = ax.text(
            0.5, 1.05,
            title,
            size=mpl.rcParams["axes.titlesize"],
            ha="center",
            bbox={'facecolor': 'w', 'alpha': 1,
                  'pad': 0, 'edgecolor': 'w'},
            transform=ax.transAxes, )
        return title_artist

    def plot_imshow(self):
        # if self.colorbar is None:
        #     self.colorbar = True
        # print('*** colorbar: ', self.colorbar)
        # if ((self.red_x is not None and self.red_x > 1)
        #    or (self.red_y is not None and self.red_y > 1)):
        '''
        if self.red_x is not None and self.red_x > 1:
            self.print('plot reduction in width: '+str(self.red_x))
        if self.red_y is not None and self.red_y > 1:
            self.print('plot reduction in length: '+str(self.red_y))
        if self.red_x is not None and self.red_x > 1:
            self.print('total reduction in width '
                       '(including sampling): ' +
                       str(self.red_x*self.sampling_step))
        if self.red_y is not None and self.red_y > 1:
            self.print('total reduction in length '
                       '(including sampling): ' +
                       str(self.red_y*self.sampling_step))
        '''
        if self.background_color:
            self.print('background color: %s'
                       % self.background_color)
        if self.facecolor:
            self.print('facecolor: %s' % self.facecolor)

        flag_different_geotransforms = False
        geotransform = self.data[0].geotransform
        for im in range(self.input_number):
            if ((geotransform is not None and
                 self.data[im].geotransform is not None and
                 not plant.compare_geotransforms(
                     geotransform,
                     self.data[im].geotransform)) or
                (geotransform is None and
                 self.data[im].geotransform is not None)):
                flag_different_geotransforms = True

        flag_different_shapes = False
        shape = self.data[0].data.shape
        for im in range(self.input_number):
            if shape != self.data[im].data.shape:
                flag_different_shapes = True
                break

        if (self.im_rgb is None and
            (self.gray_scale or
             flag_different_geotransforms or
             self.input_number == 1 or
             self.input_number > 4 or
             (self.input_number == 4 and self.data[0].nbands == 1))):
            self.im_rgb = False
        elif self.im_rgb is None:
            self.im_rgb =  not flag_different_shapes

        if (self.imshow_separate is None and
                (geotransform is not None and
                 flag_different_geotransforms)):
            self.imshow_separate = False
        elif self.imshow_separate is None:
            self.imshow_separate = (not self.im_rgb or
                                    flag_different_shapes or
                                    self.input_number == 1)

        if self.flag_cartopy or (self.flag_cartopy is None and
                                 self.geotransform is not None):
            try:
                import cartopy.crs as ccrs
                self.flag_cartopy = True
            except ModuleNotFoundError:
                if self.flag_cartopy:
                    print('WARNING error importing cartopy. '
                          'Using matplotlib instead..')
                    self.flag_cartopy = False
        # self.fig.patch.set_facecolor(self.background_color)
        # self.plt.rcParams['axes.facecolor'] = self.background_color
        # self.plt.rcParams['figure.facecolor'] = self.background_color
        # self.ax.set_facecolor(self.background_color)

        # print('*** self.percentile: ', self.percentile)
        kwargs = {}
        # kwargs['title'] = ''
        kwargs['hillshade'] = self.hillshade
        kwargs['interpolation'] = self.interpolation
        kwargs['draw_ocean'] = self.draw_ocean
        kwargs['draw_land'] = self.draw_land
        kwargs['draw_lakes'] = self.draw_lakes
        kwargs['draw_borders'] = self.draw_borders
        kwargs['draw_coastline'] = self.draw_coastline
        kwargs['draw_rivers'] = self.draw_rivers
        '''
        kwargs['draw_countries'] = self.draw_countries
        kwargs['draw_states'] = self.draw_states
        kwargs['draw_counties'] = self.draw_counties
        kwargs['etopo'] = self.etopo
        kwargs['blue_marble'] = self.blue_marble
        '''
        # kwargs['resolution'] = self.resolution
        # kwargs['map_projection'] = self.map_projection
        kwargs['background_color'] = self.background_color
        kwargs['facecolor'] = self.facecolor
        kwargs['fontsize'] = self.fontsize
        # kwargs['legend_fontsize'] = self.legend_fontsize
        kwargs['extent'] = self.extent
        kwargs['origin'] = self.origin
        kwargs['invert_x_axis'] = self.invert_x_axis
        kwargs['invert_y_axis'] = self.invert_y_axis
        kwargs['aspect'] = self.aspect
        kwargs['xlim'] = [self.xmin, self.xmax]
        kwargs['ylim'] = [self.ymin, self.ymax]
        kwargs['force'] = self.force
        kwargs['label_x'] = self.label_x
        kwargs['label_y'] = self.label_y
        # kwargs['red_x'] = 1
        # kwargs['red_y'] = 1
        kwargs['plot_size_x'] = self.plot_size_x
        kwargs['plot_size_y'] = self.plot_size_y
        kwargs['dpi'] = self.dpi
        # kwargs['geotransform'] = self.geotransform
        kwargs['flag_folium'] = self.flag_folium
        kwargs['flag_cartopy'] = self.flag_cartopy
        kwargs['percentile'] = self.percentile
        kwargs['grid'] = self.grid
        kwargs['tick_params_top'] = plant.get_bool_string(
            self.tick_params_top)
        kwargs['tick_params_labeltop'] = plant.get_bool_string(
            self.tick_params_labeltop)
        kwargs['tick_params_bottom'] = plant.get_bool_string(
            self.tick_params_bottom)
        kwargs['tick_params_labelbottom'] = plant.get_bool_string(
            self.tick_params_labelbottom)
        kwargs['tick_params_left'] = plant.get_bool_string(
            self.tick_params_left)
        kwargs['tick_params_labelleft'] = plant.get_bool_string(
            self.tick_params_labelleft)
        kwargs['tick_params_right'] = plant.get_bool_string(
            self.tick_params_right)
        kwargs['tick_params_labelright'] = plant.get_bool_string(
            self.tick_params_labelright)
        kwargs['plot_style'] = self.plot_style
        kwargs['dark_theme'] = self.dark_theme
        kwargs['flag_colorbar_in_line'] = self.flag_colorbar_in_line
        # if FLAG_FIX_AXIS_DECIMAL_PLACES:
        kwargs['decimal_places'] = self.decimal_places
        kwargs['n_significant_figures'] = self.n_significant_figures

        # if not self.im_rgb and self.flag_animate:
        if self.flag_animate:
            self._animate(kwargs)
            return

        if self.flag_collage_col is not None:
            flag_portrait = self.flag_collage_col
        else:
            flag_portrait = (self.data[0].data.shape[0] >
                             self.data[0].data.shape[1])

        # print('*** portrait: ', flag_portrait)
        # shape_0 = self.data[0].data.shape
        # flag_sharey = all([self.data[im].data.shape[0] == shape_0[0]
        #                    for im in range(self.input_number)])
        # flag_sharex = all([self.data[im].data.shape[1] == shape_0[1]
        #                    for im in range(self.input_number)])
        dict_subplot_kw = {'facecolor': self.background_color}
        sqrt_size = int(np.ceil(np.sqrt(float(self.input_number))))
        if not self.imshow_separate:
            nrows = 1
            ncols = 1
        elif self.flag_collage_col:
            nrows = self.input_number
            ncols = 1
        elif self.flag_collage_row:
            nrows = 1
            ncols = self.input_number
        elif flag_portrait:
            nrows = sqrt_size
            ncols = int(np.ceil(float(self.input_number) / sqrt_size))
        else:
            ncols = sqrt_size
            nrows = int(np.ceil(float(self.input_number) / sqrt_size))

        if self.flag_cartopy:
            dict_subplot_kw['projection'] = ccrs.PlateCarree()
        if not self.flag_folium:
            self.fig, self.axes = self.plt.subplots(
                nrows=nrows,
                ncols=ncols,
                # facecolor=self.background_color,
                squeeze=False,
                subplot_kw=dict_subplot_kw,
                # axisbg='red',
                **self.fig_kwargs)

        # fig.patch.set_facecolor('red')
        if self.flag_collage_row and not self.flag_folium:
            self.fig.subplots_adjust(hspace=0.5)

        # self.n_plots = self.input_number
        '''
        if self.flag_folium and self.im_rgb:
            range_list = [None]
        elif self.flag_folium:
            range_list = [None] * self.input_number
        else:
            range_list = self.axes.flat
        '''

        lat_min = None
        lon_min = None
        lat_max = None
        lon_max = None

        max_range = max([nrows * ncols, self.input_number])
        for im in range(max_range):

            if self.flag_folium:
                ax = None
            elif self.imshow_separate:
                ax = self.axes.flat[im]
            else:
                ax = self.axes.flat[0]

            if im > self.input_number -1:
                self.fig.delaxes(ax)
                continue

            # for im, ax in enumerate(range_list):

            if self.imshow_separate:
                lat_min = None
                lon_min = None
                lat_max = None
                lon_max = None

            self.ax_list.append(ax)
            # ax.set_facecolor(self.background_color)
            if self.ax is None:
                self.ax = ax
            if (self.flag_folium and not self.no_title and
                    self.title is not None):
                name = self.title
            else:
                name = None
            if len(self.data[im].data.shape) == 3:
                if (self.data[im].file_format.upper() in
                        plant.FIG_DRIVERS):
                    max_nchannels = 4
                else:
                    max_nchannels = 3

                vmin = []
                vmax = []
                args = []
                for d in range(min([self.data[im].data.shape[2],
                                    max_nchannels])):
                    args.append(plant.shape_image(
                        self.data[im].data[:, :, d]))
                    vmin.append(self.data[im].vmin[d])
                    vmax.append(self.data[im].vmax[d])


                cmap = ctable = None
                flag_colorbar = self.colorbar is True
                colorbar_label = self.data[im].name
                if name is None:
                    name = self.data[im].image_name

            elif not self.im_rgb or self.input_number == 1 or self.imshow_separate:
                # image_list = [im]
                cmap = self.data[im].cmap
                ctable = self.data[im].ctable
                flag_colorbar = self.colorbar or self.colorbar is None
                args = [self.data[im].data]
                vmin = self.data[im].vmin
                vmax = self.data[im].vmax
                colorbar_label = self.colorbar_label
                if name is None:
                    name = self.data[im].name
                # self.data[im].colorbar_label
            else:
                # image_list = range(self.input_number)
                cmap = None
                ctable = None
                # force self.colorbar == None to be False:
                flag_colorbar = self.colorbar is True
                data_0 = self.data[0].data
                data_1 = self.data[1].data
                if self.input_number >= 3:
                    data_2 = self.data[2].data
                self.print('red: file 1 %s'
                           % (self.data[0].filename))
                self.print('green: file 2 %s'
                           % (self.data[1].filename))
                if name is None:
                    name = ' '.join([self.data[im].name
                                     for im in range(self.input_number)])
                if self.input_number == 2 and not DUAL_POL_AVG:
                    self.print('blue: file 1 %s'
                               % (self.data[0].filename))
                    vmin = [self.data[0].vmin, self.data[1].vmin,
                            self.data[0].vmin]
                    vmax = [self.data[0].vmax, self.data[1].vmax,
                            self.data[0].vmax]
                    args = [data_0, data_1, data_0]
                    # if self.colorbar_label is None:
                    colorbar_label = [self.data[0].name,
                                      self.data[1].name,
                                      self.data[0].name]
                elif self.input_number == 2:
                    data_0, data_1 = plant.get_intersection(data_0,
                                                            data_1)
                    data_2 = (data_0+data_1)/2.0
                    self.print('blue: (file 1 + file 2)/2')
                    vmin = [self.data[0].vmin, self.data[1].vmin,
                            (self.data[0].vmin+self.data[1].vmin)/2.0]
                    vmax = [self.data[0].vmax, self.data[1].vmax,
                            (self.data[0].vmax+self.data[1].vmax)/2.0]
                    args = [data_0, data_1, data_2]
                    # if self.colorbar_label is None:
                    colorbar_label = [self.data[0].name,
                                      self.data[1].name,
                                      self.data[0].name+'+' +
                                      self.data[1].name]
                else:
                    data_2 = self.data[2].data
                    vmin = [self.data[0].vmin,
                            self.data[1].vmin,
                            self.data[2].vmin]
                    vmax = [self.data[0].vmax,
                            self.data[1].vmax,
                            self.data[2].vmax]
                    self.print('blue: file 3 %s'
                               % (self.data[2].filename))
                    if self.input_number >= 4:
                        data_3 = self.data[3].data
                        args = [data_0, data_1, data_2, data_3]
                    else:
                        args = [data_0, data_1, data_2]
                    # if self.colorbar_label is None:
                    colorbar_label = [self.data[0].name,
                                      self.data[1].name,
                                      self.data[2].name]

            if self.colorbar_label is not None:
                colorbar_label = self.colorbar_label

            if (self.flag_folium or
                self.geo_axis or
                (self.geo_axis is None and
                 self.data[im].geotransform is not None)):

                # self.folium_map = plot
                geotransform_edges = plant.geotransform_centers_to_edges(
                    self.data[im].geotransform,
                    lat_size=self.data[im].data.shape[0],
                    lon_size=self.data[im].data.shape[1])[0:6]
                ret_dict = plant.get_coordinates(
                    geotransform=geotransform_edges,
                    lat_size=self.data[im].data.shape[0],
                    lon_size=self.data[im].data.shape[1])
                current_lat_min, current_lat_max = ret_dict['lat_arr']
                current_lon_min, current_lon_max = ret_dict['lon_arr']
                if lat_min is None or current_lat_min < lat_min:
                    lat_min = current_lat_min
                if lon_min is None or current_lon_min < lon_min:
                    lon_min = current_lon_min
                if lat_max is None or current_lat_max > lat_max:
                    lat_max = current_lat_max
                if lon_max is None or current_lon_max > lon_max:
                    lon_max = current_lon_max

                if self.geo_axis_factor:
                    geo_axis_factor = float(self.geo_axis_factor)
                else:
                    geo_axis_factor = 1
                kwargs['extent'] = [current_lon_min * geo_axis_factor,
                                    current_lon_max * geo_axis_factor,
                                    current_lat_min * geo_axis_factor,
                                    current_lat_max * geo_axis_factor]

                flag_projected = plant.is_projected(
                    self.data[im].projection)

                if self.label_x is None:
                    if flag_projected is not True:
                        label_x = 'Longitude'
                        if geo_axis_factor == 1:
                            label_x += ' [deg]'
                    else:
                        label_x = 'Easting'
                        if geo_axis_factor == 1:
                            label_x += ' [m]'
                        elif geo_axis_factor == 1e-3:
                            label_x += ' [km]'
                    kwargs['label_x'] = label_x

                if self.label_y is None:
                    if flag_projected is not True:
                        label_y = 'Latitude'
                        if geo_axis_factor == 1:
                            label_y += ' [deg]'
                    else:
                        label_y = 'Northing'
                        if geo_axis_factor == 1:
                            label_y += ' [m]'
                        elif geo_axis_factor == 1e-3:
                            label_y += ' [km]'
                    kwargs['label_y'] = label_y

                '''
                print('*** self.xmin: ', self.xmin)
                print('*** self.xmax: ', self.xmax)
                print('*** self.ymin: ', self.ymin)
                print('*** self.ymax: ', self.ymax)
                print('*** lat_min: ', lat_min)
                print('*** lat_max: ', lat_max)
                print('*** xmin: ', kwargs['xlim'])
                print('*** ymin: ', kwargs['ylim'])
                '''
                if self.xmin is None:
                    kwargs['xlim'][0] = lon_min * geo_axis_factor
                if self.xmax is None:
                    kwargs['xlim'][1] = lon_max * geo_axis_factor
                if self.ymin is None:
                    kwargs['ylim'][0] = lat_min * geo_axis_factor
                if self.ymax is None:
                    kwargs['ylim'][1] = lat_max * geo_axis_factor
                '''
                print('*** xmin: ', kwargs['xlim'])
                print('*** ymin: ', kwargs['ylim'])
                '''

            self.plot = plant.imshow(
                *args,
                vmin=vmin,
                vmax=vmax,
                geotransform=self.data[im].geotransform,
                verbose=self.verbose,
                no_show=True,
                cmap=cmap,
                ctable=ctable,
                colorbar_db=self.colorbar_db,
                colorbar_label=colorbar_label,
                name=name,
                colorbar_orientation=self.colorbar_orientation,
                flag_colorbar=flag_colorbar,
                plot=ax,
                **kwargs)
            if self.flag_folium:
                if self.flag_folium:
                    kwargs['folium_map'] = self.plot
                continue

            self.plot_list.append(self.plot)
            self.draw_support_lines(plot=ax, ax=ax,
                                    flag_no_data=True,
                                    image_list=None)
            if not self.no_title and self.title is not None and max_range == 1:
                ax.set_title(self.title, fontsize=self.fontsize)
            elif not self.no_title:
                # if not self.im_rgb or self.input_number == 1:
                if name:
                    title = name
                else:
                    title = ''
                    '''
                    title = [self.data[im].name
                             for im in range(self.input_number)]
                    title = ' '.join(title)
                    '''
                print('name: ', name, self.fontsize)
                ax.set_title(title, fontsize=self.fontsize)
            if self.title is not None:
                self.fig.canvas.set_window_title(self.title)




        if not self.no_title and self.title is not None and max_range > 1:
            self.fig.suptitle(self.title, fontsize=self.fontsize)

        # self.fig.patch.set_facecolor(self.background_color)
        # self.plt.rcParams['axes.facecolor'] = self.background_color
        # self.plt.rcParams['figure.facecolor'] = self.background_color

        # self.ax.set_facecolor(self.background_color)
        if self.flag_folium:
            # self.folium_map = plot
            self.folium_map = plant.folium_add_layer(self.plot,
                                                     flag_google_maps=True)
            '''
            lat_min = max([lat_min, -89])
            lat_max = min([lat_max, 89])
            lon_min = max([lon_min, -179])
            lon_max = min([lon_max, 179])
            '''
            self.folium_map.fit_bounds([[lat_min, lon_min],
                                        [lat_max, lon_max]])
            self.plot_list.append(self.plot)
            # self.ax = self.plt.gca()
            # self.ax.set_facecolor(self.background_color)
            if ((self.output_file and
                 (self.output_file.endswith('.html') or
                  self.output_file.endswith('.htm'))) or
                    self.flag_html):
                if self.output_file:
                    output_file = self.output_file
                else:
                    output_file = plant.get_temporary_file(
                        append=True,
                        ext='.html')
                plant.save_folium(self.folium_map,
                                  output_file,
                                  verbose=self.verbose,
                                  force=self.force)
                if not self.no_show and self.flag_html:
                    import webbrowser
                    url = 'file://'
                    url += path.abspath(output_file)
                    print(f'opening URL: {url}')
                    webbrowser.open(url, new=1, autoraise=True)
                    import time
                    time.sleep(10)

            return
        if self.tight_layout is not False:
            try:
                self.plt.tight_layout()
            except:
                error_message = plant.get_error_message()
                print(f'WARNING there was a problem applying'
                      f' tight_layout: {error_message}')

        if self.output_file:
            plant.save_fig(self.output_file,
                           plot=self.plt,
                           crop_output=self.crop_output,
                           verbose=self.verbose,
                           force=self.force)
        self.plt.subplots_adjust()
        # if not self.no_show:
        #     self.plt.show()
        return

    def _animate(self, kwargs):
        if not self.flag_folium:
            self.fig = self.plt.figure(**self.fig_kwargs)
            self.ax = self.plt.gca()
        flag_update_file = plant.overwrite_file_check(
            self.output_file,
            force=self.force)
        if self.output_file and not flag_update_file:
            self.output_file = None
        if self.output_file:
            Writer = animation.writers['ffmpeg']
            writer = Writer(fps=15, metadata=dict(artist='Me'),
                            bitrate=1800)
            selected_sink = writer.saving(self.fig,
                                          self.output_file, 100)
        else:
            selected_sink = plant.DummySink()
        if self.colorbar is None:
            self.colorbar = False
        container_list = []
        with selected_sink:
            for im in range(self.input_number):
                if len(self.data[im].data.shape) == 3:
                    data = [plant.shape_image(self.data[im].data[:, :, d])
                            for d in range(self.data[im].data.shape[2])]
                    vmin = vmax = cmap = ctable = None
                else:
                    data = self.data[im].data
                    vmin = self.data[im].vmin
                    vmax = self.data[im].vmax
                    cmap = self.data[im].cmap
                    ctable = self.data[im].ctable
                if im == 0:
                    data_background = data
                    if self.flag_first_image_as_background:
                        continue
                self.plot = self._imshow_animate(
                    data,
                    data_background,
                    vmin=vmin,
                    vmax=vmax,
                    cmap=cmap,
                    ctable=ctable,
                    vmin_0=self.data[0].vmin,
                    vmax_0=self.data[0].vmax,
                    cmap_0=self.data[0].cmap,
                    ctable_0=self.data[0].ctable,
                    **kwargs)
                container = [self.plot]
                if (not self.no_title and self.title is not None):
                    name = self.title
                else:
                    name = self.data[im].name
                if name:
                    title_artist = self._set_animation_title(
                        name, self.ax)
                    container.append(title_artist)
                if self.output_file:
                    writer.grab_frame()
                self.plot_list.append([self.plot])
                container_list.append(container)
                # container_list.append([self.plot])
                # name=self.data[im].name
        '''
        if self.title is None:
        ax.set_title(self.data[im].name, fontsize=self.fontsize)
        else:
        ax.set_title(self.title, fontsize=self.fontsize)
        if self.title is not None:
        fig.canvas.set_window_title(self.title)
        '''
        if self.animation_interval is None:
            self.animation_interval = \
                1000/(self.input_number -
                      self.flag_first_image_as_background)
        if self.animation_repeat_delay is None:
            self.animation_repeat_delay = 0

        self.print(f'Animation interval:'
                   f' {self.animation_interval}')
        self.print(f'Animation repeat delay:'
                   f' {self.animation_repeat_delay}')
        # self.print('*** self.flag_first_image_as_background: '+
        #            str(self.flag_first_image_as_background))
        # self.draw_support_lines(flag_no_data=True)
        # if not self.no_show:
        #     self.plt.show()
        # mpl.animation.Animation._blit_draw = self._blit_draw
        _ = animation.ArtistAnimation(
            self.fig,
            # self.plot_list,
            container_list,
            interval=self.animation_interval,
            # blit=True,
            # init_func=None,
            repeat_delay=self.animation_repeat_delay)
        if self.flag_first_image_as_background:
            self.plot = plant.imshow(self.data[0].data,
                                     zorder=0,
                                     vmin=self.data[0].vmin,
                                     vmax=self.data[0].vmax,
                                     no_show=True,
                                     flag_colorbar=False,
                                     cmap=self.data[0].cmap,
                                     ctable=self.data[0].ctable,
                                     animated=False,
                                     plot=self.plt,
                                     **kwargs)
            '''
        if self.output_file:  # and self.output_file.endswith('mp4'):
            my_anim.save(self.output_file,
                         writer=writer)
            if plant.isfile(self.output_file):
                self.print(f'## file saved:'
                           f' {self.output_file} (animation)')
            '''
        '''
        elif self.output_file:
            plant.save_fig(self.output_file, plot=self.plt,
                           verbose=self.verbose,
                           force=self.force)
        '''
        # self.draw_support_lines()
        # if not self.no_show:
        #     self.plt.show()

    def _imshow_animate(self, image, image_0,
                        vmin=None, vmax=None, cmap=None, ctable=None,
                        vmin_0=None, vmax_0=None, cmap_0=None, ctable_0=None,
                        **kwargs):
        if (self.flag_first_image_as_background and
                self.output_file):
            self.plot = plant.imshow(image_0,
                                     zorder=0,
                                     vmin=vmin_0,
                                     vmax=vmax_0,
                                     cmap=cmap_0,
                                     ctable=ctable_0,
                                     no_show=True,
                                     flag_colorbar=False,
                                     animated=False,
                                     plot=self.plt,
                                     **kwargs)
            # self.draw_support_lines(plot=self.plt)
        self.plot = plant.imshow(image,
                                 vmin=vmin,
                                 vmax=vmax,
                                 cmap=cmap,
                                 ctable=ctable,
                                 animated=True,
                                 plot=self.plt,
                                 no_show=True,
                                 flag_colorbar=self.colorbar,
                                 **kwargs)
        '''
        if self.colorbar:
            cbar = plant.insert_colorbar(
                cmap=self.data[im].cmap,
                cmin=self.data[im].vmin,
                cmax=self.data[im].vmax,
                ctable=self.data[im].ctable,
                n_significant_figures=self.n_significant_figures,
                flag_db=self.colorbar_db,
                data=self.data[im].data,
                fontsize=self.fontsize)
        '''
        # output_file=self.output_file,
        # flag_colorbar=self.colorbar)
        '''
        if self.colorbar:
            self.fig.delaxes(self.fig.axes[1])
            self.fig.subplots_adjust(right=0.90)
        '''
        return self.plot

    def check_data_shapes(self):
        # forcing images to have same dimensions
        shape = self.data[0].data.shape
        min_shape = shape
        for im in range(1, self.input_number):
            current_shape = self.data[im].data.shape
            if len(shape) != len(current_shape):
                self.print('ERROR dimensions of %s and %s do not '
                           'match: %s %s.'
                           % (self.data[0].filename,
                              self.data[im].filename,
                              shape, current_shape))
                return
            if shape != current_shape:
                self.print('WARNING dimensions of %s and %s do not '
                           'match: %s %s. Considering data intersection only.'
                           % (self.data[0].filename,
                              self.data[im].filename,
                              shape, current_shape))
                min_shape = [np.min([min_shape[i], current_shape[i]])
                             for i in range(len(min_shape))]
        if min_shape != shape:
            for im in range(self.input_number):
                if len(min_shape) == 1:
                    self.data[im].data = self.data[im].data[0:min_shape[0]]
                if len(min_shape) == 2:
                    self.data[im].data = self.data[im].data[0:min_shape[0],
                                                            0:min_shape[1]]
                if len(min_shape) == 3:
                    self.data[im].data = self.data[im].data[0:min_shape[0],
                                                            0:min_shape[1],
                                                            0:min_shape[2]]

    def check_if_present(self, opList, option):
        if (option in opList):
            indx = opList.index(option)
            opList.pop(indx)
            return True
        # if ('--'+option in opList):
        #    opList.pop('-'+option)
        #    return True
        return False

    def get_if_present(self, opList, option, default=None):
        # check if option is present in the opList
        # and return the value. Return None if not present
        ret = None
        try:
            indx = opList.index(option)
            ret = opList[indx + 1]
            opList.pop(indx)
            opList.pop(indx)
        except:
            pass
        if ret is None:
            return default
        return ret

    def dump(self, line_str, f):
        if f is not None:
            line_str += '\n'
            f.write(line_str)
        else:
            self.print(line_str)

    def func_print_text(self, separator='\t', test_out_text=True):
        flag_out_text = self.out_text is not None and test_out_text
        if flag_out_text:
            self.func_print_text(separator=separator,
                                 test_out_text=False)
            flag_update_file = plant.overwrite_file_check(
                self.out_text,
                force=self.force)
            if not flag_update_file:
                self.print('WARNING file not updated: ' +
                           self.out_text)
                return
            f = open(self.out_text, 'w')
        else:
            f = None
        # ret = np.where(self.data[0].data)
        # print('*** len: ', self.data[0].data.shape)
        # if len(self.data[0].data.shape) > 2:
        #     ret = np.indices(self.data[0].data.shape)[0]
        # else:
        '''
        ret = np.where(self.data[0].data.shape)
        print('*** ret: ', ret)
        print('*** ret[0]: ', ret[0])
        print('*** ret[1]: ', ret[1])
        if len(ret) == 2:
            ind_y, ind_x = ret
        else:
            ind_x = ret
            ind_y = ind_x*0
        '''
        ret = np.indices(self.data[0].data.shape)
        ind_y = ret[0].ravel()
        ind_x = ret[1].ravel()

        line_list = []
        column_list = []
        column_list = ['point #', 'line', 'column']
        for i in range(self.input_number):
            column_list.append(self.data[i].name)
        line_list.append(column_list)

        for p in range(len(ind_y)):
            column_list = []
            column_list.append(str(p+1))
            column_list.append(str(ind_y[p]+1))
            column_list.append(str(ind_x[p]+1))
            for i in range(self.input_number):
                try:
                    data_value = (self.data[i].data[ind_y[p], ind_x[p]])
                except IndexError:
                    column_list.append('-')
                    continue
                data_str = str(plant.format_number(
                    data_value,
                    decimal_places=self.decimal_places,
                    sigfigs=self.n_significant_figures))
                column_list.append(data_str)
            line_list.append(column_list)

        max_len = np.zeros((3 + self.input_number), dtype=np.int)
        for column_list in line_list:
            for i, column in enumerate(column_list):
                max_len[i] = max([max_len[i], len(column)])

        line_str_list = []
        for i, column_list in enumerate(line_list):
            for j, column in enumerate(column_list):
                column_list[j] = column.ljust(max_len[j])
            line_str = separator.join(column_list)
            if i == 0:
                line_str = '## '+line_str
            line_str_list.append(line_str)

        for line_str in line_str_list:
            self.dump(line_str, f)

        if flag_out_text:
            self.print('output text: ' + self.out_text)

    def get_min_max_nbins(self,
                          default_min,
                          default_max,
                          default_nbins):

        nbins = None
        vmin = None
        vmax = None
        if (not isinstance(default_min, list) and
                default_min is not None):
            default_min = [default_min]
        if (not isinstance(default_max, list) and
                default_max is not None):
            default_max = [default_max]
        if (not isinstance(default_nbins, list) and
                default_nbins is not None):
            default_nbins = [default_nbins]

        if default_min is None and vmin is not None:
            default_min = vmin
        if default_max is None and vmax is not None:
            default_max = vmax

        if default_nbins is None and nbins is not None:
            default_nbins = nbins
        elif default_nbins is None:
            default_nbins = None  # DEFAULT_NBINS
        return default_min, default_max, default_nbins

    def get_plot_orientation(self, x_arr, y_arr):
        if not self.inv_axis:
            return x_arr, y_arr
        else:
            return y_arr, x_arr


def main(argv=None):
    with plant.PlantLogger():
        self_obj = PlantDisplayLib(argv=argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
