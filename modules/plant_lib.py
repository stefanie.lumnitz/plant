#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import sys
import time
from os import path, makedirs
import inspect
import subprocess
import importlib
from xml.etree.ElementTree import ElementTree
from collections.abc import Sequence
import numpy as np
import gdal
import plant


class PlantIndent(object):
    def __init__(self, prefix=None):
        self.close_logger = False
        if prefix is None:
            prefix = ' '*4
        if (plant.plant_config is None or
                plant.plant_config.logger_obj is None):
            # print('*** (PlantIndent) PlantLogger.__init__')
            plant.plant_config.logger_obj = plant.PlantLogger()
            plant.plant_config.logger_obj.__enter__()
            self.close_logger = True
        self.prefix_orig = plant.plant_config.logger_obj.prefix
        plant.plant_config.logger_obj.prefix = self.prefix_orig + prefix

    def __enter__(self):
        return self

    def __exit__(self, *x):
        if (plant.plant_config is not None and
                plant.plant_config.logger_obj is not None):
            plant.plant_config.logger_obj.prefix = self.prefix_orig
            if self.close_logger:
                # print('*** (PlantIndent) PlantLogger.close')
                plant.plant_config.logger_obj.close()


class DummySink(object):
    def write(self, data):
        pass

    def __enter__(self):
        return self

    def __exit__(*x):
        pass


'''
def reload():
    import importlib
    importlib.reload(plant)
'''


def cache(func):
    saved = {}

    # @wraps(func)
    def wrapper(*args):
        if args in saved:
            return wrapper(*args)
        result = func(*args)
        saved[args] = result
        return result
    return wrapper


# def gdal_callback(complete, message, unknown):
#    print(f'progress: {complete}, message: "{message}", unknown {unknown}')

gdal_callback = gdal.TermProgress


def gdal_binding_decorator(f):
    def wrapper(output_file, input_files, verbose=True,
                flag_return=False, **kwargs):
        method = f.__name__
        plant.debug(f'executing GDAL {method} with arguments:')
        plant.debug(f'    input_file(s): {input_files}')
        plant.debug(f'    output_file: {output_file}')
        plant.debug(f'    args: {kwargs}')
        if not flag_return:
            getattr(gdal, f'{method}')(output_file,
                                       input_files,
                                       callback=gdal_callback,
                                       **kwargs)
            image_obj = None
        else:
            ret = getattr(gdal, f'{method}')(output_file,
                                             input_files,
                                             callback=gdal_callback,
                                             **kwargs)
            image_obj = plant.read_image_from_gdal_dataset(
                ret, filename=output_file, verbose=False)
        if verbose and plant.isfile(output_file):
            print(f'## file saved: {output_file} (gdal.{method})')
        elif verbose and not flag_return:
            print(f'WARNING file not saved: {output_file} (gdal.{method})')
        return image_obj
    return wrapper
    # return decorated


@gdal_binding_decorator
def Grid(*args, **kwargs):
    pass


@gdal_binding_decorator
def Warp(*args, **kwargs):
    pass


@gdal_binding_decorator
def Translate(*args, **kwargs):
    pass


@gdal_binding_decorator
def BuildVRT(*args, **kwargs):
    pass


def get_stats_gdal(input_data, band=None, verbose=True, format='json',
                   plant_transform_obj=None, approx_stats=True):
    kwargs = locals()
    kwargs.pop('input_data')
    from json import JSONDecodeError
    image_obj = plant.read_image(input_data,
                                 band=band,
                                 verbose=verbose,
                                 read_only=True,
                                 plant_transform_obj=plant_transform_obj)
    if image_obj is None:
        return
    if image_obj.plant_transform_obj is not None:
        projWin = image_obj.plant_transform_obj.crop_window
    else:
        projWin = None
    flag_create_vrt = (plant_transform_obj is not None or
                       image_obj.nbands != 1 or
                       image_obj.nbands_orig != 1 or
                       (projWin is not None and
                        any(projWin[i] != x
                            for i, x in enumerate(
                                    [0, 0, image_obj.width,
                                     image_obj.length]))))
    if not flag_create_vrt:
        try:
            plant.debug(f'executing GDAL Info with arguments (1):')
            # plant.debug(f'    input_file(s): {ref_file}')
            ret_info = gdal.Info(image_obj.filename, format=format,
                                 stats=True, approxStats=approx_stats)
            ret = _get_stats_gdal_parse_ret(ret_info)
            return ret
        except JSONDecodeError:
            kwargs['format'] = None
            return get_stats_gdal(input_data, **kwargs)
        except:
            pass
    temp_file = plant.get_temporary_file(ext='.vrt')
    plant.save_image(image_obj, temp_file, verbose=verbose,
                     output_format='VRT', flag_temporary=True)
    # ref_file = temp_file
    plant.debug(image_obj.nbands)

    plant.debug(f'executing GDAL bind Info with arguments (2):')
    plant.debug(f'    input_file(s): {temp_file}')
    try:
        ret_info = gdal.Info(temp_file, stats=True, format=format,
                             approxStats=approx_stats)
    except JSONDecodeError:
        kwargs['format'] = None
        return get_stats_gdal(input_data, **kwargs)
    except RuntimeError:
        error_message = plant.get_error_message()
        print(f'WARNING {error_message}')
        ret_dict = {}
        ret_dict['minimum'] = np.nan
        ret_dict['maximum'] = np.nan
        ret_dict['mean'] = np.nan
        ret_dict['stdDev'] = np.nan
        return ret_dict
    ret = _get_stats_gdal_parse_ret(ret_info)
    plant.append_temporary_file(temp_file)
    return ret


def _get_stats_gdal_parse_ret(ret, band=None):
    # plant.debug(f'    ret: {ret}')
    if ret is None:
        return
    if isinstance(ret, dict) and len(ret['bands']) == 1:
        ret_dict = ret['bands'][0]
        # _fix_stats_dict(ret_dict)
        return ret_dict
    elif isinstance(ret, dict):
        # _fix_stats_dict(ret)
        return ret
    ret = ret.split('\n')
    ret_dict = {}
    for line in ret:
        if 'Minimum' not in line:
            continue
        line_splitted = line.split(',')
        for sub_line in line_splitted:
            variable, value = sub_line.split('=')
            variable = variable.strip().lower()
            value = value.strip()
            ret_dict[variable] = float(value)
    return ret_dict


'''
def _fix_stats_dict(ret_dict):
    print('*** before: ', ret_dict)
    for key, values in dict(ret_dict).items():
        print('*** key: ', key)
        if 'metadata' not in key:
            continue
        if 'STATISTICS_' not in key:
            continue
        print('*** key 2: ', key)
        ret_dict[key.replace('STATISTICS_', '').lower()] = values
    print('*** after: ', ret_dict)
'''


def get_min_max_gdal(input_file, plant_transform_obj=None, approx_stats=True):
    kwargs = locals()
    kwargs.pop('input_file')
    ret_dict = get_stats_gdal(input_file, **kwargs)
    if ret_dict is None:
        image_obj = plant.read_image(
            input_file,
            plant_transform_obj=plant_transform_obj)
        data = image_obj.image
        min_value = np.min(data)
        max_value = np.max(data)
        if plant.isnan(min_value) or plant.isnan(max_value):
            min_value = np.nanmin(data)
            max_value = np.nanmax(data)
        return min_value, max_value
    return ret_dict['minimum'], ret_dict['maximum']


def get_obj_id(obj):
    id_str = ('<%s.%s object at %s>'
              % (obj.__class__.__module__,
                 obj.__class__.__name__,
                 hex(id(obj))))
    return id_str


def handle_exception(message,
                     flag_exit_if_error,
                     flag_no_messages=False,
                     error_description=None,
                     verbose=True):
    if flag_no_messages:
        return
    if error_description is not None:
        print(error_description)
    if flag_exit_if_error:
        prefix = 'ERROR '
    elif message.startswith('opening ') or message.startswith('reading '):
        prefix = 'WARNING error '
    else:
        prefix = 'WARNING '
    # if verbose:
    print(prefix+message)


def is_sequence(arg):
    flag_sequence = (not hasattr(arg, "strip") and
                     (hasattr(arg, "__getitem__") or
                      hasattr(arg, "__iter__")))
    return flag_sequence


def test_download_dir(download_dir,
                      download_dir_default,
                      force=None):
    if not download_dir:
        download_dir_root = plant.plant_config.download_dir
        if download_dir_root:
            download_dir = path.join(download_dir_root,
                                     download_dir_default)
        else:
            download_dir = download_dir_default

    if download_dir and not path.isdir(download_dir):
        if plant.plant_config.flag_never:
            return
        # print('WARNING creating directory: ' +
        #      download_dir)
        makedirs(download_dir)
    return download_dir


def indent_XML(elem, type_size=None, last=None):
    if type_size is None:
        type_size = [0]
    if last is None:
        last = False
    tab = ' ' * 4
    if(len(elem)):
        type_size[0] += 1
        elem.text = '\n' + (type_size[0]) * tab
        lenEl = len(elem)
        lastCp = False
        for i in range(lenEl):
            if(i == lenEl - 1):
                lastCp = True
            indent_XML(elem[i], type_size, lastCp)
        if(not last):
            elem.tail = '\n' + (type_size[0]) * tab
        else:
            type_size[0] -= 1
            elem.tail = '\n' + (type_size[0]) * tab
    else:
        if(not last):
            elem.tail = '\n' + (type_size[0]) * tab
        else:
            type_size[0] -= 1
            elem.tail = '\n' + (type_size[0]) * tab
    return


def hms_string(sec_elapsed, method=1, separator=' '):
    '''
    convert seconds (integer) to hours, min., sec., ms. (str)
    '''
    h = int(sec_elapsed / (60 * 60))
    m = int((sec_elapsed % (60 * 60)) / 60)
    if method == 1:
        s = int(sec_elapsed % 60.)
        ms = int((sec_elapsed % 1)*1000)
        ret_string = ''
        if h > 0:
            ret_string += str(h)+'h'
        if m > 0:
            ret_string += str(m)+'m'
        if s > 0:
            ret_string += str(s)+'s'
        if h > 0 or m > 0 or s > 0:
            ret_string += separator
        if ms > 0:
            ret_string += str(ms)+'ms'
        if not ret_string:
            return '<0ms'
        return ret_string
    else:
        s = sec_elapsed % 60.
        return "{}:{:>02}:{:>05.2f}".format(h, m, s)


'''
def plant_print_imported_modules(search_string=None):
    ''
    print imported modules (isce, gdal and python).
    ''
    if not search_string:
        search_string = '*py'
    file_list = search_image(search_string)
    isce_module_list = []
    gdal_module_list = []
    python_module_list = []
    # comment_list = []
    for current_file in file_list:
        # print(current_file)
        flag_comment = False
        with open(current_file) as f:
            line = f.read()
            lines = line.split('\n')
            for i, current_line in enumerate(lines):
                if "''" in current_line:
                    flag_comment = not flag_comment
                if flag_comment:
                    continue
                current_line = (current_line.split('#'))[0]
                current_line = (current_line.split("'"))[0]
                current_line = current_line.strip()
                module_name = ''
                if current_line.startswith('from '):
                    current_line_temp = current_line.split(' ')
                    for w, current_word in enumerate(current_line_temp):
                        if 'from' in current_word:
                            try:
                                module_name = current_line_temp[w+1]
                            except:
                                pass
                            break
                elif 'import ' in current_line:
                    module_name = current_line.replace('import', '')
                if not module_name:
                    continue
                if 'plant' in module_name.lower():
                    continue
                module_name = module_name.strip()
                module_name = (module_name.split(' as '))[0]
                module_name = module_name.split(',')
                for current_module in module_name:
                    current_module = current_module.strip()
                    if ('isce' in current_module or
                            'zerodop' in current_module or
                            'stdproc' in current_module):
                        isce_module_list.append(current_module)
                    elif 'gdal' in current_module or 'gm' in current_module:
                        gdal_module_list.append(current_module)
                    else:
                        python_module_list.append(current_module)
    isce_module_list = set(isce_module_list)
    gdal_module_list = set(gdal_module_list)
    python_module_list = set(python_module_list)
    print('-------------------------------------')
    print('%d gdal modules:' % (len(gdal_module_list)))
    print(set(gdal_module_list))
    print('-------------------------------------')
    print('%d isce modules:' % (len(isce_module_list)))
    print(set(isce_module_list))
    print('-------------------------------------')
    print('%d python modules:' % (len(python_module_list)))
    print(set(python_module_list))
'''


def debug(*args):
    # curframe = inspect.currentframe()
    # calframe = inspect.getouterframes(curframe, 2)
    # curframe = inspect.currentframe()
    # calframe = inspect.getouterframes(curframe, 2)
    message = [str(x) for x in args]
    message = ' '.join(message)
    try:
        calframe = inspect.stack()
    except:
        print(f'DEBUG {message}')
        return
    try:
        with open(calframe[1][1], 'r', encoding="ISO-8859-1") as f:
            lines = f.readlines()
            line = lines[calframe[1][2]-1].replace('plant.debug(', '')
            if ')' in line:
                line = ')'.join(line.split(')')[:-1])
            line = line.strip()
            if line.replace("'", '').replace('"', '') != message:
                line = plant.bcolors.Cyan + line + plant.bcolors.ColorOff
                message = line + ' = ' + message
        # cal_info = ('('+path.basename(calframe[1][1])+'.' +
        #             calframe[1][3]+':'+str(calframe[1][2])+')')
        # cal_info = plant.bcolors.Cyan + cal_info + plant.bcolors.ColorOff
    except FileNotFoundError:
        pass
    print(f'DEBUG {message}')


'''
from pprint import pprint

class MethodDebug(object):
    """Debug a normal method.

    This decorator is used for debugging a normal method,
    with normal arguments, i.e, not printing out the data
    of the class it's contained in.

    Keyword arguments:
    debug -- Whether or not you want to debug the method.
    """
    def __init__(self, debug):
        self.flag_debug = debug

    def __call__(self, function):
        def wrapper(*args, **kwargs):
            if self.flag_debug:
                pprint(args)
                pprint(kwargs)
            return function(*args, **kwargs)
        return wrapper


class ClassMethodDebug(object):
    """Debug a class method.

    This decorator is used for debugging a class method,
    with normal arguments, and self. When using this
    decorator, the method will print out it's arguments
    and the attributes of the class it's contained in.

    Keyword arguments:
    debug -- Whether or not you want to debug the method.
    """
    def __init__(self, debug):
        self.flag_debug = debug

    def __call__(self, function):
        def wrapper(function_self, *args, **kwargs):
            if self.flag_debug:
                pprint(function_self.__dict__)
                pprint(args)
                pprint(kwargs)
            return function(function_self, *args, **kwargs)
        return wrapper
'''


class ModuleWrapper(object):
    def __init__(self, module_name, *args, ref=None, **kwargs):
        # print('*** init***')
        self._module_name = module_name
        self._module_obj = None
        self._ref = ref
        self._args = args
        self._kwargs = kwargs
        self._command = None
        self._set_module_obj(self._module_name)

    def _set_module_obj(self, name):
        self._module_name = self._module_name.replace('.py', '')
        if not self._module_name.startswith('plant_'):
            self._module_name = 'plant_' + self._module_name
        # print('*** module_name: ', self._module_name)
        try:
            self._module_obj = importlib.import_module('plant.app.' +
                                                       self._module_name)
        except ModuleNotFoundError:
            pass

    def __call__(self, *args, **kwargs):
        # print('*** __call__', args, kwargs)
        args = list(self._args) + list(args)
        kwargs = dict(self._kwargs, **kwargs)
        if self._module_obj is None:
            print(f'ERROR module not found: {self._module_name}')
            return

        self._set_command(*args, **kwargs)
        if self._command is None:
            return
        flag_mute = kwargs.get('flag_mute', None)
        verbose = kwargs.get('verbose', None) and not (flag_mute is True)
        if self._ref is not None:
            ret = self._ref.execute(self._command, verbose=verbose)
        else:
            ret = plant.execute(self._command, verbose=verbose)
        # except plant.PlantReturn as e:
        #     ret = e.args
        # except:
        #    print('*** exception: ')
        #    ret = None
        if not self._flag_output:
            return ret
        output_ret = _get_output_ret_from_plant_config()
        if output_ret is not None:
            ret = output_ret
        return ret

    def _set_command(self, *args, **kwargs):
        # variables_to_pop = []
        args_str = self._update_args_str(
            args, args_str='')
        if self._module_name == 'plant_display':
            args_str = ' ' + args_str
        else:
            args_str = ' -i ' + args_str

        argparse_method = getattr(self._module_obj, 'get_parser')
        parser = argparse_method()

        '''
        ret = plant.get_args_from_argparser(parser,
                                            store_true_action=False,
                                            store_false_action=False,
                                            store_action=True,
                                            help_action=False,
                                            dest='output_file')
        output_file_keys = [x.replace('-', '_').strip('_')
                            for x in ret]
        output_file_keys.append('output_file')

        ret = plant.get_args_from_argparser(parser,
                                            store_true_action=False,
                                            store_false_action=False,
                                            store_action=True,
                                            help_action=False,
                                            dest='output_dir')
        output_dir_keys = [x.replace('-', '_').strip('_')
                           for x in ret]
        output_dir_keys.append('output_dir')
        '''

        output_dict = _get_output_dict_from_parser(
            parser, kwargs, self._module_name)
        output_file_keys = output_dict['output_file_keys']
        # output_dir_keys = output_dict['output_dir_keys']
        output_str = output_dict['output_str']
        self._flag_output = output_dict['flag_output']

        kwargs_str = ''
        for key, value in kwargs.items():
            if key in output_file_keys:
                continue
            if isinstance(value, list):
                value_str = ''
                for v in value:
                    if value_str:
                        value_str += ' '
                    if isinstance(v, str) and "'" not in v:
                        value_str += "'"+str(v)+"'"
                    elif isinstance(v, str):
                        value_str += '"'+str(v)+'"'
                    else:
                        value_str += str(v)
            elif (isinstance(value, plant.PlantImage) or
                  isinstance(value, np.ndarray)):
                # new_image_obj = value.copy()
                # print('*** creating MEM tag 000000!!!!')
                arg_id = str(id(value))
                # print('*** value.filename_orig: ', value.filename_orig)
                # print('*** value.filename: ', value.filename)
                plant.plant_config.variables[arg_id] = value
                # variables_to_pop.append(arg_id)
                value_str = f' MEM:{arg_id}'
            elif not isinstance(value, str) or '"' not in value:
                value_str = f'"{value}"'
            else:
                value_str = f"'{value}'"

            kwargs_dest = {}
            if key.startswith('-'):
                kwargs_arg = {'arg': key}
            else:
                key_with_dashes = key.replace('_', '-')
                kwargs_dest['dest'] = key
                if len(key) == 1:
                    kwargs_arg = {'arg': '-'+key_with_dashes}
                else:
                    kwargs_arg = {'arg': '--'+key_with_dashes}
            flag_valid_argument = False
            for kwargs_argparser in [kwargs_dest, kwargs_arg]:
                if flag_valid_argument:
                    continue
                ret = plant.get_args_from_argparser(parser,
                                                    store_true_action=False,
                                                    store_false_action=False,
                                                    store_action=True,
                                                    help_action=False,
                                                    **kwargs_argparser)
                if ret:
                    kwargs_str += f' {ret[0]} {value_str}'
                    flag_valid_argument = True
                    continue

                # store True
                ret_store_true = plant.get_args_from_argparser(
                    parser,
                    store_true_action=True,
                    store_false_action=False,
                    store_action=False,
                    help_action=False,
                    **kwargs_argparser)
                # flag_valid_argument = False
                # add_if_flag_store_false = False
                if ret_store_true and bool(value):
                    kwargs_str += f' {ret_store_true[0]}'
                elif ret_store_true:
                    dest_store_true = plant.get_args_from_argparser(
                        parser,
                        store_true_action=True,
                        store_false_action=False,
                        store_action=False,
                        help_action=False,
                        get_dest=True,
                        **kwargs_argparser)
                    arg_store_false = plant.get_args_from_argparser(
                        parser,
                        store_true_action=False,
                        store_false_action=True,
                        store_action=False,
                        help_action=False,
                        dest=dest_store_true[0])
                    if arg_store_false:
                        kwargs_str += f' {arg_store_false[0]}'
                    # add_if_flag_store_false = True
                if ret_store_true:
                    flag_valid_argument = True
                    continue
                # elif ret_store_true:
                #      flag_valid_argument = True
                #     continue

                # store False
                ret_store_false = plant.get_args_from_argparser(
                    parser,
                    store_true_action=False,
                    store_false_action=True,
                    store_action=False,
                    help_action=False,
                    **kwargs_argparser)
                # add_if_flag_store_true = False
                if ret_store_false and bool(value):
                    kwargs_str += f' {ret_store_false[0]}'
                    # continue
                elif ret_store_false:
                    dest_store_false = plant.get_args_from_argparser(
                        parser,
                        store_true_action=False,
                        store_false_action=True,
                        store_action=False,
                        help_action=False,
                        get_dest=True,
                        **kwargs_argparser)
                    arg_store_true = plant.get_args_from_argparser(
                        parser,
                        store_true_action=True,
                        store_false_action=False,
                        store_action=False,
                        help_action=False,
                        dest=dest_store_false[0])
                    if arg_store_true:
                        kwargs_str += f' {arg_store_true[0]}'

                #    add_if_flag_store_true = True
                if ret_store_false:
                    flag_valid_argument = True

                '''
                print('*** add_if_flag_store_false: ',
                      add_if_flag_store_false)
                print('*** add_if_flag_store_true: ',
                      add_if_flag_store_true)
                print('*** ret_store_false: ', ret_store_false)
                print('*** ret_store_true: ', ret_store_true)
                if add_if_flag_store_false and ret_store_false:
                    kwargs_str += f' {ret_store_false[0]}'
                    flag_valid_argument = True
                elif add_if_flag_store_true and ret_store_true:
                    kwargs_str += f' {ret_store_true[0]}'
                    flag_valid_argument = True
                '''

                if flag_valid_argument:
                    continue
            if not flag_valid_argument:
                print(f'ERROR invalid argument: "{key}"')
                return

        # self._flag_output = False
        '''
        output_str = ''
        ret = plant.get_args_from_argparser(parser,
                                            dest='output_file')
        self._flag_output = bool(ret)
        output_key = None
        for key in output_file_keys:
            if key not in kwargs.keys():
                continue
            output_key = key
            break
        if self._flag_output and output_key:
            value_str = kwargs[output_key]
            output_str = f' {ret[0]} {value_str}'
        elif (self._flag_output and
              not any([key in kwargs.keys() for key in output_dir_keys]) and
              self._module_name != 'plant_display'):
            output_str = f' {ret[0]} MEM:output.bin'
        '''
        if self._module_name == 'plant_display':
            self._command = (f'{self._module_name}.py {kwargs_str} {args_str}'
                             f' {output_str}')
        else:
            self._command = (f'{self._module_name}.py {args_str} {kwargs_str}'
                             f' {output_str}')

    def _update_args_str(self, args, args_str=''):
        for arg in args:
            if isinstance(arg, str):
                args_str += f' {arg}'
                continue
            if (isinstance(arg, Sequence) and
                    all([isinstance(x, str) for x in arg])):
                args_str += self._update_args_str(arg)
                continue

            # print('*** creating MEM tag!!!!')
            if not isinstance(arg, plant.PlantImage):
                # # print('*** arg: ', arg)
                arg = plant.PlantImage(arg)
                # plant.imshow(arg)
                # print('*** creating MEM tag 11111!!!!')
                # # print('*** arg nbands: ', arg.nbands)
            # else:
            #     new_image_obj = arg.copy()
            #     print('*** creating MEM tag 2222!!!!')

            arg_id = str(id(arg))
            plant.plant_config.variables[arg_id] = arg
            # variables_to_pop.append(arg_id)
            args_str += f' MEM:{arg_id}'
        return args_str


def get_immutable_value(value):
    if (isinstance(value, list) or
            isinstance(value, dict) or
            isinstance(value, set)):
        return value.copy()
    if isinstance(value, plant.PlantTransform):
        return value.copy()
    # if (isinstance(value, plant.PlantImage) or
    #         isinstance(value, plant.PlantBand)):
    #     return value.deepcopy()
    # PlantTransform
    return value


def clear():
    if (plant.plant_config is not None and
            plant.plant_config.logger_obj is not None):
        plant.plant_config.logger_obj.close()


def _get_output_dict_from_parser(parser, args, module_name):
    orig_index = []
    if isinstance(args, dict):
        args_keys = args.keys()
        kwargs = args
    else:
        args_keys = []
        for i, arg in enumerate(args):
            if arg.startswith('--'):
                args_keys.append(arg[2:])
                orig_index.append(i)
            elif arg.startswith('-') and not plant.isnumeric(arg[1:]):
                args_keys.append(arg[1:])
                orig_index.append(i)
        args_keys = [x.replace('-', '_').strip('_')
                     for x in args_keys]
        kwargs = None
    # print('*** args_keys: ', args_keys)
    ret = plant.get_args_from_argparser(parser,
                                        store_true_action=False,
                                        store_false_action=False,
                                        store_action=True,
                                        help_action=False,
                                        dest='output_file')
    output_file_keys = [x.replace('-', '_').strip('_')
                        for x in ret]
    output_file_keys.append('output_file')

    ret = plant.get_args_from_argparser(parser,
                                        store_true_action=False,
                                        store_false_action=False,
                                        store_action=True,
                                        help_action=False,
                                        dest='output_dir')
    output_dir_keys = [x.replace('-', '_').strip('_')
                       for x in ret]
    output_dir_keys.append('output_dir')

    ret = plant.get_args_from_argparser(parser,
                                        dest='output_file')
    flag_output = bool(ret)
    output_key = None

    for key in output_file_keys:
        if key not in args_keys:
            continue
        output_key = key
        if isinstance(args, dict):
            break
        output_key_index = orig_index[args_keys.index(output_key)]
        break

    output_str = ''
    output_args = []
    flag_new_mem_output = False

    if flag_output and output_key:
        if kwargs is not None:
            value_str = kwargs[output_key]
        else:
            value_str = args[output_key_index+1]
        # print('*** here 1')
        output_str = f' {ret[0]} {value_str}'
        output_args.append(ret[0])
        output_args.append(value_str)
        # print('*** here 2')
    elif (flag_output and
          not any([key in args_keys
                   for key in output_dir_keys]) and
          module_name != 'plant_display'):
        # mem_output_str = 'MEM:output.bin'
        # temp_file = plant.get_temporary_file(ext='.vrt')
        mem_output_str = 'MEM:'+plant.get_temporary_file()
        output_str = f' {ret[0]} {mem_output_str}'
        output_args.append(ret[0])
        output_args.append(mem_output_str)
        flag_new_mem_output = True

    output_dict = {}
    output_dict['output_str'] = output_str
    output_dict['output_args'] = output_args
    output_dict['output_file_keys'] = output_file_keys
    output_dict['output_dir_keys'] = output_dir_keys
    output_dict['output_file_keys'] = output_file_keys
    output_dict['flag_output'] = flag_output
    output_dict['flag_new_mem_output'] = flag_new_mem_output
    # print('*** output_dict', output_dict)

    return output_dict


def _get_output_ret_from_plant_config():
    output_dict = {}
    output_ret = None
    for key in plant.plant_config.variables.keys():
        if key.upper().startswith('OUTPUT:'):
            output_dict[key.replace('OUTPUT:', '')] = \
                plant.plant_config.variables[key]
    if len(output_dict) == 1:
        output_ret = output_dict.popitem()[1]
    if len(output_dict) > 1:
        for key in output_dict.keys():
            plant.plant_config.variables.pop(f'OUTPUT:{key}')
        output_ret = output_dict
    # output_ret = ret
    return output_ret


def execute(command,
            verbose=True,
            level=1,
            return_time=False,
            ignore_exception=False):
    '''
    subprocess execution wrapper
    '''
    # hide output in std output
    # if not verbose:
    #     FILE_NULL = open(devnull, 'w')
    # else:
    #     FILE_NULL = None
    if not isinstance(command, list):
        command_vector = get_command_vector(command)
    else:
        command_vector = command
    if len(command_vector) == 0 and verbose:
        print('WARNING command not identified: '+command)
        return ['']

    '''
    if (plant.plant_config is not None and
            plant.plant_config.logger_obj is not None):
        selected_sink = plant.DummySink()
    else:
        selected_sink = plant.PlantLogger()
    '''

    # command_vector = command.split(' ')
    # command_vector = [x for x in command_vector if x]
    # print(command_vector)
    start_time = None
    module_name = command_vector[0]
    argv = command_vector[1:]
    if module_name.endswith('.py'):
        module_name = module_name.replace('.py', '')
        flag_error = False
        try:
            module_obj = importlib.import_module('plant.app.'+module_name)
        except ModuleNotFoundError:
            flag_error = True
        if not flag_error:









            current_script = plant.plant_config.current_script






            method_to_execute = getattr(module_obj, 'main')

            if plant.plant_config.logger_obj is None:
                sink = plant.PlantLogger()
            else:
                sink = plant.PlantIndent()

            with sink:
                if verbose:
                    #     print('PLAnT module: '+module_name)
                    #     print(' '.join(command_vector))
                    arguments = plant.get_command_line_from_argv(argv)
                    command_line = (f'{module_name}.py {arguments}')
                    print(f'PLAnT {plant.VERSION} (API) -'
                          f' {command_line}')

                '''
                if (not verbose and
                    ('-q' not in argv and
                     '--quiet' not in argv and
                     '-v' not in argv and
                     '--verbose' not in argv)):
                    argv += ['-q']
                '''
                parser_ref = plant.argparse()
                ret = plant.get_args_from_argparser(parser_ref,
                                                    store_true_action=True,
                                                    store_false_action=True,
                                                    store_action=False,
                                                    help_action=False,
                                                    dest='cli_mode')
                has_bash_flag = any([element in argv for element in ret])
                if not has_bash_flag:
                    argv.append('--no-bash')

                argparse_method = getattr(module_obj, 'get_parser')
                parser = argparse_method()

                # print('*** argv: ', argv)

                output_dict = _get_output_dict_from_parser(
                    parser, argv, module_name)

                # print('*** output_dict: ', output_dict)
                # print('*** parser_ref: ', parser_ref)
                # print('*** parser: ', parser)
                # print('*** argv: ', argv)
                # print('*** module_name: ', module_name)

                flag_output = output_dict['flag_new_mem_output']

                if output_dict['flag_new_mem_output']:
                    argv.extend(output_dict['output_args'])
                    argv.extend(['-u', '--ul', '10'])

                # print('*** argv (after): ', argv)
                # print('*** argv.__class__: ', argv.__class__)

                original_sys_argv = sys.argv
                sys.argv = [module_name+'.py'] + argv
                # with selected_sink:
                # with plant.PlantIndent(prefix='--->'):
                # ret = method_to_execute(argv)
                flag_error = False
                ret = None
                # print('*** before executing...')
                if return_time:
                    start_time = time.time()
                try:
                    ret = method_to_execute(argv)
                except SystemExit as e:
                    # if SystemExit(0) then remove exception:
                    if len(e.args) == 0 or e.args[0] != 0:
                        flag_error = True
                        error_message = plant.get_error_message()
                # except:
                #    flag_error = True
                #    error_message = plant.get_error_message()
                #    # import traceback
                #    # traceback.print_exc()
                finally:
                    sys.argv = original_sys_argv
                # print('*** after executing...')
                if (flag_error and not ignore_exception and
                        error_message and 'ERROR' in error_message):
                    print(error_message)
                elif flag_error and not ignore_exception:
                    print('ERROR executing PLAnT module %s: %s.'
                          % (module_name, error_message))
                # if flag_error:
                #     raise(plant.PlantExceptionError(error_message))
                if return_time:
                    ret = time.time() - start_time
                elif (module_name == 'plant_display' and
                        isinstance(ret, plant.PlantDisplayLib) and
                        ret.folium_map is not None):
                    ret = ret.folium_map
                elif flag_output:
                    output_ret = _get_output_ret_from_plant_config()
                    if output_ret is not None:
                        ret = output_ret

                if ret is not None:
                    ret_str = (f'. Returning object class:'
                               f' {ret.__class__.__name__}')
                else:
                    ret_str = ''
                if verbose:
                    print(f'PLAnT (API) - {module_name}.py {arguments}'
                          f' (completed){ret_str}')
                '''
                if isinstance(ret, plant.PlantImage):
                    print('*** realizing changes...')
                    ret.realize_changes()
                    print('*** ok')
                    print('*** mean: ', np.nanmean(ret.image))
                '''
                return ret

            plant.plant_config.logger_obj.flush_temporary_files()
            plant.plant_config.current_script = current_script
            '''
                return ret
            # sys.argv = original_sys_argv
            return ret
            '''
    '''
        if verbose:
            print(' '.join(command_vector))
            print('executing external routine (' +
                  str(module_name)+')..')
    '''
    # filter(None, lst)

    ret = None
    flag_rerun = False
    if return_time:
        start_time = time.time()
    try:
        ret = execute_external(command, verbose,
                               module_name=module_name,
                               ignore_exception=ignore_exception)
    except KeyboardInterrupt:
        raise plant.PlantExceptionKeyboardInterrupt
    except plant.PlantExceptionError:
        error_message = plant.get_error_message()
        if 'command not found' not in error_message:
            raise(plant.PlantExceptionError(error_message))
            # print(error_message)
            return
        flag_rerun = True
    if return_time:
        ret = time.time() - start_time
    if not flag_rerun:
        return ret

    try:
        ret_which = execute_external('which '+module_name,
                                     verbose=False,
                                     ignore_exception=True)
    except:
        ret_which = None
    # print('*** result from which:', ret_which)
    if ret_which:
        if return_time:
            start_time = time.time()
        command = command.replace(module_name, ret_which[0])
        ret = execute_external(command, verbose,
                               module_name=module_name,
                               ignore_exception=ignore_exception)
        if return_time:
            ret = time.time() - start_time
    return ret



def execute_external(command,
                     verbose=True,
                     module_name='',
                     ignore_exception=False):
    
    if verbose and module_name != 'plant_display':
        print("executing: %s" % command)
    # from shlex import split
        # with selected_sink:
        # with plant.PlantIndent():

    p = subprocess.Popen(command,
                         # ['/bin/bash', '-i', '-c', command],
                         shell=True,
                         # executable='/bin/bash',
                         # encoding='utf8',
                         # universal_newlines=True,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE,
                         bufsize=0)
    #  stderr=subprocess.PIPE)

    # lines_iterator = iter(p.stdout.readline, b"")
    out = []
    # level_prefix = ''.join('    '*level)
    # if level >= 1:
    #     level_prefix += ' '
    # for line in lines_iterator:
    # for line in p.stdout.read():
    with plant.PlantIndent(prefix='>>  '):
        line = ''
        while True:
            # line = p.stdout.readline()
            element = p.stdout.read(1)
            if len(element) == 0 and p.poll() is not None:
                break
            elif len(element) == 0:
                continue
            elif element != b'\r':
                try:
                    line += element.decode('utf-8')
                except UnicodeDecodeError:
                    pass
                if '\n' not in line:
                    continue
            if verbose:
                print(line, end='')
            out += [line.replace('\n', '')]
            line = ''
            if element == b'\r':
                line += '\r'
        # err +=

    if module_name == 'mdx' or module_name == 'plant_display':
        rows, cols = plant.read_polygon_from_file(out,
                                                  flag_from_file=False)
        if rows and cols:
            print('parsed mdx results:')
            # print(f'    selected rows: {rows}')
            # print(f'    selected cols: {cols}')
            print(f'    --rows {rows} --cols {cols}')
            return rows, cols
        # print('..done')

    out_err = []
    lines_iterator = iter(p.stderr.readline, b"")
    with plant.PlantIndent(prefix='>>  '):
        for line in lines_iterator:
            if verbose:
                print('[StdErr] '+line.decode('utf-8'), end='')
            out_err += [line.decode('utf-8')]

    p.communicate()

    '''
    print('******************')
    print('*** out *** ')
    print('******************')
    for line in out:
        print(line)

    print('******************')
    print('*** out err *** ')
    print('******************')
    for line in out_err:
        print(line)
    '''

    
    flag_error = p.returncode != 0
    if flag_error and not ignore_exception:
        command = command.strip()
        if len(out_err) == 1:
            error_message = out_err[0].replace('\n', '')
            print(f'ERROR executing command "{command}":'
                  f' {error_message}')
        else:
            print(f'ERROR executing command: {command}')
        return out_err
    # elif flag_error and not ignore_exception:
    #    print('ERROR executing %s'
    #          % command.strip().split(' ')[0])
    #    return out_err
    if len(out_err) > len(out):
        return out_err
    return out


def get_command_vector(command):
    '''
    return a command vector based on a command string
    '''
    command_vector = []
    current_command = ''
    flag_string = False
    # print('*** input command: ', command)
    for letter in command:
        if letter == '"' or letter == "'":
            flag_string = not(flag_string)
            current_command += letter
        elif flag_string or letter != ' ':
            current_command += letter
        else:
            if (('"' in current_command or
                    "'" in current_command) or
                    '*' not in current_command and
                    '?' not in current_command):
                command_vector.append(current_command)
            else:
                command_temp = plant.glob(current_command)
                if len(command_temp) > 0:
                    command_vector += command_temp
                else:
                    command_vector.append(current_command)
            current_command = ''
    if (('"' in current_command or
            "'" in current_command) or
            '*' not in current_command and
            '?' not in current_command):
        command_vector.append(current_command)
    else:
        command_vector += plant.glob(current_command)
    command_vector = [x.replace('"', '').replace("'", '')
                      for x in command_vector if x]
    if isinstance(command_vector, str):
        command_vector = [command_vector]
    # print('*** output command: ', command_vector)
    return command_vector


def prompt_continue(message, force=None):
    if force:
        return
    while 1:
        res = plant.get_keys(message)
        if res.startswith('n'):
            print('operation cancelled.')
            sys.exit(0)
        elif res.startswith('y'):
            break


# XML handler from Sentinel1A.py
def open_xml(xml):
    try:
        fp = open(xml, 'r')
    except IOError as strerr:
        print("IOError: %s" % strerr)
        return
    return ElementTree(file=fp).getroot()


def print_dict(input_dict, indent=0):
    for key, value in input_dict.items():
        print('\t' * indent + str(key)+':')
        if isinstance(value, dict):
            print_dict(value, indent+1)
        else:
            print('\t' * (indent+1) + str(value))


def grab_from_xml(xml_file, path, verbose=True):
    flag_error = False
    try:
        res = xml_file.find(path).text
    except:
        flag_error = True
    if flag_error:
        if verbose:
            print('WARNING Tag="%s" not found in %s' % (path, xml_file))
        return
    return res


def get_dict_glcf():
    dict_glcf = {}
    dict_glcf['0'] = 'Water'

    dict_glcf['1'] = 'Evergreen Needleleaf forest'
    dict_glcf['2'] = 'Evergreen Broadleaf forest'
    dict_glcf['3'] = 'Deciduous Needleleaf forest'
    dict_glcf['4'] = 'Deciduous Broadleaf forest'
    dict_glcf['5'] = 'Mixed forest'
    dict_glcf['6'] = 'Closed shrublands'
    dict_glcf['7'] = 'Open shrublands'
    dict_glcf['8'] = 'Woody savannas'
    dict_glcf['9'] = 'Savannas'
    dict_glcf['10'] = 'Grasslands'
    dict_glcf['11'] = 'Permanent wetlands'
    dict_glcf['12'] = 'Croplands'
    dict_glcf['13'] = 'Urban and built-up'
    dict_glcf['14'] = 'Cropland/Natural vegetation mosaic'
    dict_glcf['15'] = 'Snow and ice'
    dict_glcf['16'] = 'Barren or sparsely vegetated'
    dict_glcf['254'] = 'Unclassified'
    dict_glcf['255'] = 'Fill Value'
    return dict_glcf


def get_dict_globcover():

    dict_globcover = {}
    dict_globcover['0'] = 'Unclassified'
    dict_globcover['11'] = 'Post-flooding or irrigated croplands'
    dict_globcover['14'] = 'Rainfed croplands'
    dict_globcover['20'] = ('Mosaic Cropland (50-70%) / Vegetation '
                            '(grassland, shrubland, forest) (20-50%)')
    dict_globcover['30'] = ('Mosaic Vegetation (grassland, shrubland,'
                            'forest) (50-70%) / Cropland (20-50%)')
    dict_globcover['40'] = ('Closed to open (>15%) broadleaved evergreen '
                            'and/or semi-deciduous forest (>5m)')
    dict_globcover['50'] = ('Closed (>40%) broadleaved deciduous forest '
                            '(>5m)')
    dict_globcover['60'] = ('Open (15-40%) broadleaved deciduous forest '
                            '(>5m)')
    dict_globcover['70'] = ('Closed (>40%) needleleaved evergreen forest '
                            '(>5m)')
    dict_globcover['90'] = ('Open (15-40%) needleleaved deciduous or '
                            'evergreen forest (>5m)')
    dict_globcover['100'] = ('Closed to open (>15%) mixed broadleaved '
                             ' and needleleaved forest (>5m)')
    dict_globcover['110'] = ('Mosaic Forest/Shrubland (50-70%) / Grassland'
                             ' (20-50%)')
    dict_globcover['120'] = ('Mosaic Grassland (50-70%) / Forest/Shrubland'
                             ' (20-50%)')
    dict_globcover['130'] = 'Closed to open (>15%) shrubland (<5m)'
    dict_globcover['140'] = 'Closed to open (>15%) grassland'
    dict_globcover['150'] = ('Sparse (>15%) vegetation (woody vegetation, '
                             'shrubs, grassland)')
    dict_globcover['160'] = ('Closed (>40%) broadleaved forest regularly '
                             'flooded - Fresh water')
    dict_globcover['170'] = ('Closed (>40%) broadleaved semi-deciduous '
                             'and/or evergreen forest regularly flooded - '
                             'Saline water')
    dict_globcover['180'] = ('Closed to open (>15%) vegetation (grassland,'
                             ' shrubland, woody vegetation) on regularly '
                             'flooded or waterlogged '
                             'soil - Fresh, brackish or saline water')

    dict_globcover['190'] = 'Artificial surfaces and associated areas '
    '(urban areas >50%)'
    dict_globcover['200'] = 'Bare areas'
    dict_globcover['210'] = 'Water bodies'
    dict_globcover['220'] = 'Permanent snow and ice'
    dict_globcover['230'] = 'Unclassified'
    return dict_globcover


def get_proj_win_str(projWin):
    if isinstance(projWin, str):
        proj_win_str = projWin
    else:
        proj_win_str = ' '.join([str(x) for x in projWin])
    return proj_win_str



# *********************************************************************
#
# extract_values.py
# ---------------------------------------------------------
# Python script for extracting values of image according to
# the point shapefile.
# More: http://gis-lab.info/qa/extract-values-rasters.html
#
# Copyright (C) 2010 Alexander Bruy (alexander.bruy@gmail.com)
#               2015 Maxim Dubinin (sim@gis-lab.info)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
# to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
# MA 02111-1307, USA.
#
# *********************************************************************
#
# try:
#     from osgeo import ogr, osr
# except ImportError:
#     import ogr, osr
#
# import numpy as np
#
# outFormat = 'ESRI Shapefile'
#
#
# def mapToPixel(mX, mY, geoTransform):
#     '''Convert map coordinates to pixel coordinates.
#
#     @param mX              Input map X coordinate (double)
#     @param mY              Input map Y coordinate (double)
#     @param geoTransform    Input geotransform (six doubles)
#     @return pX, pY         Output coordinates (two doubles)
#     '''
#     if geoTransform[2] + geoTransform[4] == 0:
#         pX = (mX - geoTransform[0]) / geoTransform[1]
#         pY = (mY - geoTransform[3]) / geoTransform[5]
#     else:
#         pX, pY = applyGeoTransform(mX, mY, invertGeoTransform(geoTransform))
#     # return int(pX + 0.5), int(pY + 0.5)
#     return int(math.floor(pX)), int(math.floor(pY))
#
#
# def pixelToMap(pX, pY, geoTransform):
#     '''Convert pixel coordinates to map coordinates.
#
#     @param pX              Input pixel X coordinate (double)
#     @param pY              Input pixel Y coordinate (double)
#     @param geoTransform    Input geotransform (six doubles)
#     @return mX, mY         Output coordinates (two doubles)
#     '''
#     mX, mY = applyGeoTransform(pX, pY, geoTransform)
#     return mX, mY
#
#
# def applyGeoTransform(inX, inY, geoTransform):
#     '''Apply a geotransform to coordinates.
#
#     @param inX             Input coordinate (double)
#     @param inY             Input coordinate (double)
#     @param geoTransform    Input geotransform (six doubles)
#     @return outX, outY     Output coordinates (two doubles)
#     '''
#     outX = geoTransform[0] + inX * geoTransform[1] + inY * geoTransform[2]
#     outY = geoTransform[3] + inX * geoTransform[4] + inY * geoTransform[5]
#     return outX, outY
#
#
# def invertGeoTransform(geoTransform):
#     '''Invert standard 3x2 set of geotransform coefficients.
#
#     @param geoTransform        Input GeoTransform (six doubles - unaltered)
#     @return outGeoTransform    Output GeoTransform (six doubles - updated)
#                              on success, None if the equation is uninvertable
#     '''
#     # we assume a 3rd row that is [1 0 0]
#     # compute determinate
#     det = geoTransform[1] * geoTransform[5] - geoTransform[2]
# * geoTransform[4]
#
#     if abs(det) < 0.000000000000001:
#         return
#
#     invDet = 1.0 / det
#
#     # compute adjoint and divide by determinate
#     outGeoTransform = [0, 0, 0, 0, 0, 0]
#     outGeoTransform[1] = geoTransform[5] * invDet
#     outGeoTransform[4] = -geoTransform[4] * invDet
#
#     outGeoTransform[2] = -geoTransform[2] * invDet
#     outGeoTransform[5] = geoTransform[1] * invDet
#
#     outGeoTransform[0] = (geoTransform[2] * geoTransform[3] -
#                           geoTransform[0] * geoTransform[5]) * invDet
#     outGeoTransform[3] = (-geoTransform[1] * geoTransform[3] +
#                           geoTransform[0] * geoTransform[4]) * invDet
#
#     return outGeoTransform
#
