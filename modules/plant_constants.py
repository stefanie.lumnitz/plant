#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import os
import numpy as np
import sys


def get_version():
    pwd = os.path.dirname(__file__)
    version_filename = os.path.join(pwd, '..', 'VERSION')
    if os.path.isfile(version_filename):
        with open(version_filename, 'r') as f:
            version = f.read().strip()
        return version
    for d in sys.path:
        if 'site-packages' not in d:
            continue
        dir_check = os.path.join(d, 'plant')
        if not os.path.isdir(dir_check):
            continue
        version_filename = os.path.join(dir_check, 'VERSION')
        if not os.path.isfile(version_filename):
            continue
        with open(version_filename, 'r') as f:
            version = f.read().strip()
        return version
    return '0.0.??dev'

# version = '0.0.43dev'
# version = get_version()
# __version__ = version = VERSION = '0.0.43dev'

__version__ = version = VERSION = get_version()

MATPLOTLIB_BACKEND = None
# options: 'GTK3Agg', 'GTK3Cairo', 'MacOSX', 'nbAgg',
# 'Qt4Agg', 'Qt4Cairo', 'Qt5Agg', 'Qt5Cairo', 'TkAgg',
# 'TkCairo', 'WebAgg', 'WX', 'WXAgg', 'WXCairo', 'agg',
# 'cairo', 'pdf', 'pgf', 'ps', 'svg', 'template']

PLANT_CONFIG_FILE = 'plant_config.txt'
DEFAULT_OUTPUT_FORMAT = 'ISCE'
FLAG_OUTPUT_DIR_OVERWRITE = True

LINE_SEPARATOR = '#'
IMAGE_NAME_SEPARATOR = ':'
ELEMENT_SEPARATOR = ','
LINE_MULTIPLIER = '**'
ELEMENT_MULTIPLIER = '*'
SEPARATOR_LIST = ['_', '-', ',', ':']
PARSER_GROUP_SEPARATOR = "\n\nOPTION "
USAGE_EXAMPLES_STRING = 'usage examples: \n'
DESCRIPTION_STRING = 'description: \n'

Y_BAND_NAME = 'Geolocation: Y'
X_BAND_NAME = 'Geolocation: X'

DEFAULT_MIN_MAX_PERCENTILE = 95
# LINE_SEPARATOR = ','

INT_NAN = -32768
# DOUBLE_NAN = -1.7e308
# DOUBLE_NAN = 1.7976931348623155e+308
DOUBLE_NAN = 1.7976931348623157e+308
MAX_VALUE = 1.0e30

EARTH_RADIUS = 6378137
# DOWNLOAD_DIR = 'downloads'
DOWNLOAD_DIR = os.path.join('dat', 'plant')
DOWNLOAD_DIR_ICESAT_GLAS = 'ICESat_GLAS'
ICESAT_GLAS_DB_FILE = os.path.join('db', 'glas.db')

MAX_WINDOW_WIDTH = 1024
MAX_WINDOW_HEIGHT = 768
DEFAULT_PLOT_SIZE_X = 640
DEFAULT_PLOT_SIZE_Y = 545

IMSHOW_BACKGROUND_COLOR = 'BLUE'
PLOT_BACKGROUND_COLOR = 'WHITE'
IMSHOW_BACKGROUND_COLOR_DARK = 'BLACK'
PLOT_BACKGROUND_COLOR_DARK = 'BLACK'

N_SIGNIFICANT_FIGURES = 5

KML_TILE_SIZE = 8192
KMZ_TILE_SIZE = 2048
# KMZ_TILE_SIZE = 512
DEFAULT_CMAP = 'gray'  # jet

HEADER_EXTENSIONS = ['.xml', '.hdr', '.ann']
EXT_READ_FROM_ANN_FILE = ['mlc', 'slc', 'grd', 'dat', 'hgt', 'slope',
                          'inc', 'sch', 'kz', 'llh', 'dop', 'lkv',
                          'baseline', 'ann', 'amp1', 'amp2', 'cor', 'int',
                          'unw', 'rtc', 'rtcgrd']
EXT_READ_FROM_ANN_FILE_GEO = ['grd', 'hgt', 'inc', 'rtc', 'rtcgrd']

        
# eps, jpeg, jpg, pdf, pgf, png, ps, raw, rgba, svg, svgz, tif, tiff.

FIG_DRIVERS = ['EPS', 'JPEG', 'JPG', 'PDF',
               'PGF', 'PNG', 'PS', 'RAW', 'RGBA',
               'SVG', 'SVGZ']

MDX_NOT_SUPPORTED_FORMATS = ['TIF', 'TIFF', 'GTIFF', 'VRT'] + FIG_DRIVERS

DATA_DRIVERS = ['MEM', 'ISCE', 'SRF', 'BIN', 'NUMPY', 'SENTINEL', 'HDF5',
                'NETCDF', 'NISAR', 'ISCE', 'ANN']
TEXT_DRIVERS = ['TXT', 'TEXT', 'CSV']
AVAILABLE_DRIVERS = DATA_DRIVERS + TEXT_DRIVERS

SRF_MAGIC_NUMBER = 1504078485

# if sys.version_info[0] >= 3:
INVALID_DASH_LIST = ['—', '–']
# else:
# INVALID_DASH_LIST = ['-']

SRF_DTYPE = [(1, np.byte), (20, np.float32), (30, np.complex64),
             (21, np.complex64)]


DEFAULT_COLOR_LIST = ['rosybrown',
                      'teal',
                      'steelblue',
                      'darkred',
                      'darkgreen',
                      'darkblue',
                      'red',
                      'green',
                      'blue',
                      'maroon',
                      'grey',
                      'black',
                      'darkgreen',
                      'darkcyan',
                      'sienna']

DEFAULT_COLOR_LIST_1 = ['steelblue',
                        'rosybrown',
                        'teal',
                        'blue',
                        'red',
                        'green',
                        'darkblue',
                        'darkred',
                        'darkgreen',
                        'grey',
                        'black',
                        'maroon',
                        'sienna',
                        'darkcyan',
                        'darkgreen']

# colors source: http://ksrowell.com/blog-visualizing
# -data/2012/02/02/optimal-colors-for-graphs/
LINE_AND_POINTS_COLOR_LIST = [['blue', '#396AB1'],
                              ['orange', '#DA7C30'],
                              ['green', '#3E9651'],
                              ['red', '#CC2529'],
                              ['gray', '#535154'],
                              ['purple', '#6B4C9A'],
                              ['darkred', '#922428'],
                              ['beige', '#948B3D']]

BAR_COLOR_LIST = [['blue', (114, 147, 203)],
                  ['orange', (225, 151, 76)],
                  ['green', (132, 186, 91)],
                  ['red', (211, 94, 96)],
                  ['gray', (128, 133, 133)],
                  ['purple', (144, 103, 167)],
                  ['darkred', (171, 104, 87)],
                  ['beige', (204, 194, 16)]]

POL_COLOR_LIST = [3, 2, 0, 1]

# LINESTYLE_LIST = ['-',  '--', '-.', ':']
# LINESTYLE_LIST = ['solid', 'dashed', 'dashdot', 'dotted']
NISAR_PRODUCT_LIST = [os.path.join('SLC', 'swaths'),
                      os.path.join('GCOV', 'grids')]


LINESTYLE_LIST = ['solid',
                  # 'loosely dotted',
                  'dotted',
                  # 'densely dotted',
                  # 'loosely dashed',
                  'dashed',
                  # 'densely dashed',
                  # 'loosely dashdotted',
                  # 'dashdotted',
                  # 'densely dashdotted',
                  # 'loosely dashdotdotted',
                  # 'dashdotdotted',
                  # 'densely dashdotdotted']
                  'dashdot']

DEFAULT_GDAL_FORMAT = 'GTiff'
DEFAULT_GDAL_EXTENSION = '.tif'

OUTPUT_FORMAT_MAP = {}
OUTPUT_FORMAT_MAP['BIN'] = 'ENVI'
PROJECTION_REF_WKT = ('GEOGCS["GCS_WGS_1984",DATUM["WGS_1984",'
                      'SPHEROID["WGS_84",6378137,298.257223563]],'
                      'PRIMEM["Greenwich",0],'
                      'UNIT["Degree",0.017453292519943295]]')
PROJECTION_REF = ('+proj=longlat +ellps=WGS84 '
                  '+datum=WGS84 +no_defs')  # t_srs


# MARKER_LIST = ['o', '.', ',', 'x', '+', 'v', '^', '<', '>', 's', 'd']
MARKER_LIST = ['o', '^', 's', 'd', 'v', '<', '>', '.', ',', 'x', '+']
# MARKER_LIST = ['o', 's', '^', 'd', 'v', '<', '>', '.', ',', 'x', '+']

# HATCH_LIST = ['-', '+', 'x', '\\', '*', 'o', 'O', '.', '/']
HATCH_LIST = ['', '////', '//', '/////', '///', '/']
# , '\\', '\\\\', '\\\\\\'
# , '\\\', '\', '\\', '.'

CMAP_LIGHT_LIST = ['Reds', 'Greens', 'Blues', 'Greys',
                   'Oranges', 'Purples']

CMAP_DARK_LIST = ['bone', 'gist_heat', 'gray', 'copper',
                  'pink', 'hot']

RADAR_DECOMP_DICT = []
RADAR_DECOMP_DICT.append(['LEX_SQUARE', ['HHHH', 'VVVV', 'HVHV', 'VHVH']])
RADAR_DECOMP_DICT.append(['LEX', ['HH', 'VV', 'HV', 'VH']])
RADAR_DECOMP_DICT.append(['PAULI', ['HHmVV', 'HHpVV',  'Hv', 'Vh']])
RADAR_DECOMP_DICT.append(['PAULI_2', ['MM', 'PP',  'HV', 'VH']])
RADAR_DECOMP_DICT.append(['S2', ['s11', 's22', 's12', 's21']])
RADAR_DECOMP_DICT.append(['C4', ['C11', 'C44', 'C22', 'C33']])
# T11 single-bounce, T22 double-bounce
RADAR_DECOMP_DICT.append(['T4', ['T22', 'T11', 'T33', 'T44']])
RADAR_DECOMP_DICT.append(['C3', ['C11', 'C33', 'C22', 'C22']])
# T11 single-bounce, T22 double-bounce
RADAR_DECOMP_DICT.append(['T3', ['T22', 'T11', 'T33', 'T33']])
RADAR_DECOMP_DICT.append(['ELEM', ['Dbl', 'Odd', 'Vol', 'Vol']])
RADAR_DECOMP_DICT.append(['RVOG', ['_md', '_ms', '_mv', '_mv']])
RADAR_DECOMP_DICT.append(['KROGAGER', ['_Ks', '_Kd', '_Kh', '_Kh']])
RADAR_DECOMP_DICT.append(['OPT', ['Opt1', 'Opt2', 'Opt3', 'Opt3']])
RADAR_DECOMP_DICT.append(['COLOR', ['Red', 'Blue', 'Green', 'Green']])

WIKI_API = "https://en.wikipedia.org/w/api.php"


class bcolors:
    # HEADER = '\033[95m' # pink
    # OKBLUE = '\033[94m'
    # OKGREEN = '\033[92m'
    # WARNING = '\033[93m'
    # FAIL = '\033[91m'
    # GRAY = '\033[0;37m'
    # DARK_GRAY = '\033[2m'
    # BLACK = '\033[30m'
    # ENDC = '\033[0m' # no color
    BOLD = '\033[1m'
    # UNDERLINE = '\033[4m'

    # BLACK = '\033[0;30m'
    # RED = '\033[0;31m'
    # GREEN = '\033[0;32m'
    # ORANGE = '\033[0;33m'
    # BLUE = '\033[0;34m'
    # PURPLE = '\033[0;35m'
    # CYAN = '\033[0;36m'
    # 
    # BG_BLACK = '\033[0;40m'
    # BG_RED = '\033[0;41m'
    # BG_GREEN = '\033[0;42m'
    # BG_ORANGE = '\033[0;43m'
    # BG_BLUE = '\033[0;44m'
    # BG_PURPLE = '\033[0;45m'
    # BG_CYAN = '\033[0;46m'
    # BG_LIGHT_GRAY = '\033[0;47m'
    # 
    # LIGHT_BLACK = '\033[0;90m'
    # LIGHT_RED = '\033[0;91m'
    # LIGHT_GREEN = '\033[0;92m'
    # LIGHT_ORANGE = '\033[0;93m'
    # LIGHT_BLUE = '\033[0;94m'
    # LIGHT_PURPLE = '\033[0;95m'
    # LIGHT_CYAN = '\033[0;96m'

    # source: https://github.com/ryanoasis/
    # public-bash-scripts/blob/master/unix-color-codes.sh

    # Reset
    ColorOff = "\033[0m"       # Text Reset

    # Regular Colors
    Black = "\033[0;30m"        # Black
    Red = "\033[0;31m"          # Red
    Green = "\033[0;32m"        # Green
    Yellow = "\033[0;33m"       # Yellow
    Blue = "\033[0;34m"         # Blue
    Purple = "\033[0;35m"       # Purple
    Cyan = "\033[0;36m"         # Cyan
    White = "\033[0;37m"        # White

    # Bold
    BBlack = "\033[1;30m"       # Black
    BRed = "\033[1;31m"         # Red
    BGreen = "\033[1;32m"       # Green
    BYellow = "\033[1;33m"      # Yellow
    BBlue = "\033[1;34m"        # Blue
    BPurple = "\033[1;35m"      # Purple
    BCyan = "\033[1;36m"        # Cyan
    BWhite = "\033[1;37m"       # White

    # Underline
    UBlack = "\033[4;30m"       # Black
    URed = "\033[4;31m"         # Red
    UGreen = "\033[4;32m"       # Green
    UYellow = "\033[4;33m"      # Yellow
    UBlue = "\033[4;34m"        # Blue
    UPurple = "\033[4;35m"      # Purple
    UCyan = "\033[4;36m"        # Cyan
    UWhite = "\033[4;37m"       # White

    # Background
    On_Black = "\033[40m"       # Black
    On_Red = "\033[41m"         # Red
    On_Green = "\033[42m"       # Green
    On_Yellow = "\033[43m"      # Yellow
    On_Blue = "\033[44m"        # Blue
    On_Purple = "\033[45m"      # Purple
    On_Cyan = "\033[46m"        # Cyan
    On_White = "\033[47m"       # White

    # High Intensty
    IBlack = "\033[0;90m"       # Black
    IRed = "\033[0;91m"         # Red
    IGreen = "\033[0;92m"       # Green
    IYellow = "\033[0;93m"      # Yellow
    IBlue = "\033[0;94m"        # Blue
    IPurple = "\033[0;95m"      # Purple
    ICyan = "\033[0;96m"        # Cyan
    IWhite = "\033[0;97m"       # White

    # Bold High Intensty
    BIBlack = "\033[1;90m"      # Black
    BIRed = "\033[1;91m"        # Red
    BIGreen = "\033[1;92m"      # Green
    BIYellow = "\033[1;93m"     # Yellow
    BIBlue = "\033[1;94m"       # Blue
    BIPurple = "\033[1;95m"     # Purple
    BICyan = "\033[1;96m"       # Cyan
    BIWhite = "\033[1;97m"      # White

    # High Intensty backgrounds
    On_IBlack = "\033[0;100m"   # Black
    On_IRed = "\033[0;101m"     # Red
    On_IGreen = "\033[0;102m"   # Green
    On_IYellow = "\033[0;103m"  # Yellow
    On_IBlue = "\033[0;104m"    # Blue
    On_IPurple = "\033[10;95m"  # Purple
    On_ICyan = "\033[0;106m"    # Cyan
    On_IWhite = "\033[0;107m"   # White

    BLACK = "\e[00;30m"
    DARY_GRAY = "\e[01;30m"
    RED = "\e[00;31m"
    BRIGHT_RED = "\e[01;31m"
    GREEN = "\e[00;32m"
    BRIGHT_GREEN = "\e[01;32m"
    BROWN = "\e[00;33m"
    YELLOW = "\e[01;33m"
    BLUE = "\e[00;34m"
    BRIGHT_BLUE = "\e[01;34m"
    PURPLE = "\e[00;35m"

# Black        0;30     Dark Gray     1;30
# Red          0;31     Light Red     1;31
# Green        0;32     Light Green   1;32
# Yellow 0;33           Yellow        1;33
# Blue         0;34     Light Blue    1;34
# Purple       0;35     Light Purple  1;35
# Cyan         0;36     Light Cyan    1;36
# ## Light Gray   0;37     White         1;37

# BG_RED 0;41
