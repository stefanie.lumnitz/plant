#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from os import path
import datetime
import sys
import shelve
import numbers
import inspect
import numpy as np
import math
from scipy.interpolate import interp1d
import plant


def get_slope_alpha(image, pixel_size_x, inc=None,
                    psi=None):
    delta_h = plant.get_image_derivative_x(image)
    if psi is not None:
        denominator = pixel_size_x / np.cos(psi)
        denominator -= delta_h * np.tan(psi)
    else:
        denominator = pixel_size_x / np.sin(inc)
        denominator -= delta_h / np.tan(inc)
    alpha = np.arctan2(delta_h, denominator)
    # alpha[ind] = 0
    return alpha
    

def get_band_name(frequency):
    '''
    convert frequency to the band name (letter)
    '''
    # HF    3 to 30 MHz
    # VHF  30 to 300 MHz
    # UHF 300 to 1000 MHz
    # if 250e6 <= frequency and frequency < 500e6:
    #     band_name = 'P'
    if frequency < 1e9:
        band_name = 'P'
    elif 1e9 <= frequency and frequency < 2e9:
        band_name = 'L'
    elif 2e9 <= frequency and frequency < 4e9:
        band_name = 'S'
    elif 4e9 <= frequency and frequency < 8e9:
        band_name = 'C'
    elif 8e9 <= frequency and frequency < 12e9:
        band_name = 'X'
    elif 12e9 <= frequency and frequency < 18e9:
        band_name = 'Ku'
    elif 18e9 <= frequency and frequency < 27e9:
        band_name = 'K'
    elif 27e9 <= frequency and frequency < 40e9:
        band_name = 'Ka'
    elif 40e9 <= frequency and frequency < 75e9:
        band_name = 'V'
    elif 75e9 <= frequency and frequency < 110e9:
        band_name = 'W'
    elif 110e9 <= frequency and frequency < 300e9:
        band_name = 'mm'
    else:
        band_name = None
    return band_name


def get_frequency(band_name):
    '''
    convert a band name (letter) to the band center frequency
    '''
    # HF    3 to 30 MHz
    # VHF  30 to 300 MHz
    # UHF 300 to 1000 MHz
    # if 250e6 <= frequency and frequency < 500e6:
    #     band_name = 'P'
    if 'P' in band_name:
        # frequency = 400e6
        frequency = (440.0+280.0)*1e6/2.0  # AIRMOSS
    elif 'L' in band_name:
        # frequency = (1e9 + 2e9)/2
        frequency = (1297.5+1217.5)*1e6/2.0  # UAVSAR
    elif 'S' in band_name:
        frequency = (2e9 + 4e9)/2
    elif 'C' in band_name:
        frequency = (4e9 + 8e9)/2
    elif 'X' in band_name:
        frequency = (8e9 + 12e9)/2
    elif 'Ku' in band_name:
        frequency = (12e9 + 18e9)/2
    elif 'K' in band_name:
        frequency = (18e9 + 27e9)/2
    elif 'Ka' in band_name:
        frequency = (27e9 + 40e9)/2
    elif 'V' in band_name:
        frequency = (40e9 + 75e9)/2
    elif 'W' in band_name:
        frequency = (75e9 + 110e9)/2
    elif 'mm' in band_name:
        frequency = (110e9 + 300e9)/2
    else:
        frequency = None
    return frequency


def rotate_matrix(hhFile, hvFile, vhFile, vvFile, thetaFile,
                  hhOutFile, hvOutFile, vhOutFile, vvOutFile, verbose=False,
                  inverse_rotation=False):
    '''
    apply rotation over a 2x2 matrix
    counterclockwise rotation
    '''
    if isinstance(hhFile, str):
        image_obj = plant.read_image(hhFile, only_header=True)
        length = image_obj.length
        width = image_obj.width
        dt = image_obj.dtype
        # scheme = image_obj.scheme
        geotransform = image_obj.geotransform
        S11 = plant.read_matrix(hhFile, verbose=verbose, copy=False)
    else:
        S11 = hhFile
        length = S11.shape[0]
        width = S11.shape[1]
        dt = S11.dtype.name
        # scheme = image_obj.scheme
        geotransform = None
    if isinstance(hvFile, str):
        S12 = plant.read_matrix(hvFile, verbose=verbose, copy=False)
    else:
        S12 = hvFile
    if isinstance(vhFile, str):
        S21 = plant.read_matrix(vhFile, verbose=verbose, copy=False)
    else:
        S21 = vhFile
    if isinstance(vvFile, str):
        S22 = plant.read_matrix(vvFile, verbose=verbose, copy=False)
    else:
        S22 = vvFile
    if isinstance(thetaFile, str):
        theta = plant.read_matrix(thetaFile, verbose=verbose, copy=False)
    else:
        theta = thetaFile

    f11 = open(hhOutFile, 'w')
    f12 = open(hvOutFile, 'w')
    f21 = open(vhOutFile, 'w')
    f22 = open(vvOutFile, 'w')

    # S11 = np.copy(S11)
    # S12 = np.copy(S12)
    # S21 = np.copy(S21)
    # S22 = np.copy(S22)
    if isinstance(theta, np.ndarray):
        for i in range(S11.shape[0]):
            cos_theta = np.asarray(np.cos(theta[i, :]), dtype=np.float32)
            if inverse_rotation:
                sin_theta = np.asarray(np.sin(theta[i, :]), dtype=np.float32)
            else:
                sin_theta = np.asarray(-np.sin(theta[i, :]), dtype=np.float32)
            # right rotation
            S11_2 = S11[i, :]*cos_theta + S12[i, :]*sin_theta
            S12_2 = -S11[i, :]*sin_theta + S12[i, :]*cos_theta
            S21_2 = S21[i, :]*cos_theta + S22[i, :]*sin_theta
            S22_2 = -S21[i, :]*sin_theta + S22[i, :]*cos_theta
            # left rotation
            # S11[i, :] = cos_theta*S11_2 + sin_theta*S21_2
            # S12[i, :] = cos_theta*S12_2 + sin_theta*S22_2
            # S21[i, :] = -sin_theta*S11_2 + cos_theta*S21_2
            # S22[i, :] = -sin_theta*S12_2 + cos_theta*S22_2
            data_line = cos_theta*S11_2 + sin_theta*S21_2
            data_line.tofile(f11)
            data_line = cos_theta*S12_2 + sin_theta*S22_2
            data_line.tofile(f12)
            data_line = -sin_theta*S11_2 + cos_theta*S21_2
            data_line.tofile(f21)
            data_line = -sin_theta*S12_2 + cos_theta*S22_2
            data_line.tofile(f22)
    else:
        theta = np.asarray(theta, dtype=np.float32)
        for i in range(S11.shape[0]):
            cos_theta = np.cos(theta)
            if inverse_rotation:
                # clockwise rotation
                sin_theta = np.sin(theta)
            else:
                # counterclockwise rotation
                sin_theta = -np.sin(theta)
            # right rotation
            S11_2 = S11[i, :]*cos_theta + S12[i, :]*sin_theta
            S12_2 = -S11[i, :]*sin_theta + S12[i, :]*cos_theta
            S21_2 = S21[i, :]*cos_theta + S22[i, :]*sin_theta
            S22_2 = -S21[i, :]*sin_theta + S22[i, :]*cos_theta
            # left rotation
            data_line = cos_theta*S11_2 + sin_theta*S21_2
            data_line.tofile(f11)
            data_line = cos_theta*S12_2 + sin_theta*S22_2
            data_line.tofile(f12)
            data_line = -sin_theta*S11_2 + cos_theta*S21_2
            data_line.tofile(f21)
            data_line = -sin_theta*S12_2 + cos_theta*S22_2
            data_line.tofile(f22)

    plant.create_isce_header(hhOutFile,
                             nbands=1,
                             length=length,
                             width=width,
                             dtype=dt,
                             descr='',
                             geotransform=geotransform)
    plant.create_isce_header(hvOutFile,
                             nbands=1,
                             length=length,
                             width=width,
                             dtype=dt,
                             descr='',
                             geotransform=geotransform)
    plant.create_isce_header(vhOutFile,
                             nbands=1,
                             length=length,
                             width=width,
                             dtype=dt,
                             descr='',
                             geotransform=geotransform)
    plant.create_isce_header(vvOutFile,
                             nbands=1,
                             length=length,
                             width=width,
                             dtype=dt,
                             descr='',
                             geotransform=geotransform)
    return S11, S12, S21, S22


def getroot(filename, sensor=''):
    filename = path.basename(filename)
    if 'ALOS' in sensor:
        # print('filename: ', filename)
        file_array = path.basename(filename).split('-')
        if len(file_array) > 2:
            # print('root: ', file_array[2])
            return file_array[2]
    if 'FSAR' in sensor:
        file_array = path.basename(filename).split('_')
        if len(file_array) > 1:
            return file_array[1]
    if 'UAVSAR' in sensor:
        file_array = path.basename(filename).split('_')
        return file_array[0]
    return sensor


def get_polarimetric_data_type(input_filelist):
    '''
    return polar_type (S2, C3, C4, etc..)
    '''
    filelist = plant.get_file_list(input_filelist)
    if not filelist:
        return
    filelist = [path.basename(f) for f in filelist]
    # filelist.sort(key=len)
    max_S_dim = 0
    max_C_dim = 0
    max_T_dim = 0

    # testing for upper triangular matrix
    for i in range(1, 7):
        flag_S_all_found = True
        flag_C_all_found = True
        flag_T_all_found = True
        for j in range(1, i+1):
            for k in range(j, i+1):
                if not any('s'+str(j)+str(i) in f for f in filelist):
                    flag_S_all_found = False
                if not any('C'+str(j)+str(i) in f for f in filelist):
                    flag_C_all_found = False
                if not any('T'+str(j)+str(i) in f for f in filelist):
                    flag_T_all_found = False
        if flag_S_all_found and max_S_dim == i-1:
            max_S_dim = i
        if flag_C_all_found and max_C_dim == i-1:
            max_C_dim = i
        if flag_T_all_found and max_T_dim == i-1:
            max_T_dim = i
        if max_S_dim != i and max_C_dim != i and max_T_dim != i:
            break

    # in the case of S2, test also for s21 (not only upper triang. matr.):
    if max_S_dim == 2 and not any('s21' in f for f in filelist):
        max_S_dim = 1

    if max_S_dim <= 1 and max_C_dim == 0 and max_T_dim == 0:
        if (any('s11' in f for f in filelist) and
                any('s21' in f for f in filelist)):
            return 'pp1'
        elif (any('s12' in f for f in filelist) and
                any('s22' in f for f in filelist)):
            return 'pp2'
        elif (any('s11' in f for f in filelist) and
                any('s22' in f for f in filelist)):
            return 'pp3'
        elif ((any('I11' in f for f in filelist) and
                any('I12' in f for f in filelist))
              and any('I22' in f for f in filelist)):
            return 'pp5'
        elif (any('I11' in f for f in filelist) and
                any('I21' in f for f in filelist)):
            return 'pp5'
        elif (any('I12' in f for f in filelist) and
                any('I22' in f for f in filelist)):
            return 'pp6'
        elif (any('I11' in f for f in filelist) and
                any('I22' in f for f in filelist)):
            return 'pp7'
        elif max_S_dim == 0:
            ret = plant.search_pol(input_filelist, verbose=False, sym=False)
            if ret is None:
                return
            hh_file = ret['hh_file']
            hv_file = ret['hv_file']
            vh_file = ret['vh_file']
            vv_file = ret['vv_file']
            identified_inputs = [hh_file, hv_file, vh_file, vv_file]
            identified_inputs = [f for f in identified_inputs if f]
            if len(identified_inputs) == 1:
                return 'single'
            elif len(identified_inputs) == 2:
                return 'dual'
            elif len(identified_inputs) > 3:
                return 'full'
            else:
                return
    if max_S_dim >= max_C_dim and max_S_dim >= max_T_dim:
        return 'S'+str(max_S_dim)
    if max_C_dim >= max_T_dim:
        return 'C'+str(max_C_dim)
    return 'T'+str(max_T_dim)


def get_psi_angle(z,
                  inc,
                  LIA,
                  pixel_size_az,
                  pixel_size_rg,
                  degrees_inc=False,
                  degrees_LIA=False,
                  out_file=''):
    '''
    calculate psi angle (ulander1996, shimada2000)
    '''
    print('calculating projection angle (psi)..')
    if (z.shape[0] != inc.shape[0] or z.shape[1] !=
            inc.shape[1]):
        print('ERROR DEM and inc. angle dimensions do not match.')
        return

    if (z.shape[0] != LIA.shape[0] or z.shape[1] !=
            LIA.shape[1]):
        print('ERROR DEM and local inc. angle dimensions do not match.')
        return
    if out_file:
        f_out = open(out_file, 'w')
    else:
        psi = np.zeros((z.shape[0], z.shape[1]), dtype=np.float32)
    rg_slope_line = np.zeros((z.shape[1]), dtype=np.float32)
    for i in range(1, z.shape[0]-1):
        az_slope_line = ((z[i+1, :] - z[i-1, :])
                         / (2.0 * float(pixel_size_az)))
        rg_slope_line[1:-1] = (z[i, 2:] - z[i, :-2])/(2.0 *
                                                      float(pixel_size_rg))
        n_norm = np.sqrt(az_slope_line**2 + rg_slope_line**2 + 1)
        if degrees_inc:
            inc_rad = np.radians(inc[i, :])
        else:
            inc_rad = inc[i, :]
        # if degrees_LIA:
        #     LIA_rad = np.radians(LIA[i, :])
        # else:
        #     LIA_rad = LIA[i, :]
        # cos_psi = (1.0/np.absolute(np.tan(inc_rad)))*(-np.cos(LIA_rad) +
        #                                              1.0/(np.cos(inc_rad) *
        #                                                   n_norm))

        # Ulander:
        first_term = (np.sin(inc_rad)-np.cos(inc_rad)*rg_slope_line)/n_norm
        arccos_psi = np.arccos(np.clip(first_term, -1, 1))

        # GS
        # first_term = -np.cos(LIA_rad)/np.tan(inc_rad)
        # second_term = 1/(np.sin(inc_rad)*n_norm)
        # arccos_psi = np.arccos(np.clip(first_term + second_term, -1, 1))

        if out_file:
            if i == 1:
                arccos_psi.tofile(f_out)
            arccos_psi.tofile(f_out)
        else:
            psi[i, :] = arccos_psi
    if out_file:
        arccos_psi.tofile(f_out)
        plant.create_isce_header(out_file,
                                 nbands=1,
                                 length=z.shape[0],
                                 width=z.shape[1],
                                 dtype=arccos_psi.dtype,
                                 descr='')
        return
    else:
        return psi


def get_azslope_angle(z,
                      look_angle,
                      pixel_size_az,
                      pixel_size_rg,
                      output_file,
                      fs=1,
                      force=None):
    '''
    calculate azimuth orientation
    '''

    if ((z.shape[0] != look_angle.shape[0] or z.shape[1] !=
            look_angle.shape[1])):
        print('ERROR DEM and look_angle. angle dimensions do not match.')
        return
    # print('pixel_size_az: '+str(pixel_size_az))
    theta = np.zeros((z.shape[0], z.shape[1]), dtype=np.float32)
    # print(z.shape)
    # az_slope = np.zeros((z.shape[0], z.shape[1]))
    # print(z[0, :])
    # az_slope[1:-1, :] = (z[2:, :] - z[:-2, :]) / (2.0 *
    #                                              float(pixel_size_az))
    rg_slope_line = np.zeros((z.shape[1]), dtype=np.float32)
    for i in range(1, z.shape[0]-1):
        az_slope_line = (z[i+1, :]-z[i-1, :])/(2.0*float(pixel_size_az))
        rg_slope_line[1:-1] = (z[i, 2:] - z[i, :-2])/(2.0*float(pixel_size_rg))
        theta[i, :] = np.arctan2(az_slope_line, (np.sin(look_angle[i, :]) -
                                                 rg_slope_line *
                                                 np.cos(look_angle[i, :])))
    if fs > 1:
        theta = plant.filter_data(theta, boxcar=fs)

    plant.save_image(theta,
                     output_file,
                     force=force)


def get_boundaries_from_workreport(current_file):
    '''
    get lat/lon boundaries from ALOS workreport (before unpacking)
    '''
    if not plant.isfile(current_file):
        return
    ret = plant.read_parameters_text_file(current_file)
    lat1 = np.float(ret['Brs_FrameSceneLeftTopLatitude'])
    lat2 = np.float(ret['Brs_FrameSceneRightTopLatitude'])
    lat3 = np.float(ret['Brs_FrameSceneLeftBottomLatitude'])
    lat4 = np.float(ret['Brs_FrameSceneRightBottomLatitude'])
    lat_arr_temp = [lat1, lat2, lat3, lat4]
    lon1 = np.float(ret['Brs_FrameSceneLeftTopLongitude'])
    lon2 = np.float(ret['Brs_FrameSceneRightTopLongitude'])
    lon3 = np.float(ret['Brs_FrameSceneLeftBottomLongitude'])
    lon4 = np.float(ret['Brs_FrameSceneRightBottomLongitude'])
    lon_arr_temp = [lon1, lon2, lon3, lon4]

    ret = plant.read_parameters_text_file(current_file)
    lat1 = np.float(ret['Brs_ImageSceneLeftTopLatitude'])
    lat2 = np.float(ret['Brs_ImageSceneRightTopLatitude'])
    lat3 = np.float(ret['Brs_ImageSceneLeftBottomLatitude'])
    lat4 = np.float(ret['Brs_ImageSceneRightBottomLatitude'])
    lat_arr_temp += [lat1, lat2, lat3, lat4]
    lat_arr = [np.min([lat_arr_temp]),
               np.max([lat_arr_temp])]
    lon1 = np.float(ret['Brs_ImageSceneLeftTopLongitude'])
    lon2 = np.float(ret['Brs_ImageSceneRightTopLongitude'])
    lon3 = np.float(ret['Brs_ImageSceneLeftBottomLongitude'])
    lon4 = np.float(ret['Brs_ImageSceneRightBottomLongitude'])
    lon_arr_temp += [lon1, lon2, lon3, lon4]
    lon_arr = [np.min([lon_arr_temp]),
               np.max([lon_arr_temp])]
    return lat_arr, lon_arr


# def get_deriv(image, a

def get_local_inc_angle(z, inc, pixel_size_az,
                        pixel_size_rg, method=1, fs=1, degrees=False):
    '''
    calculate local incidence angle (method 1: Lee2000)
    '''
    if (z.shape[0] != inc.shape[0] or z.shape[1] !=
            inc.shape[1]):
        print('ERROR DEM and inc. angle dimensions do not match.')
        return
    # rg_slope = np.zeros((z.shape[0], z.shape[1]))
    # rg_slope[:, 1:-1] = (z[:, 2:] - z[:, :-2])/(2.0 *
    #                                            float(pixel_size_rg))
    local_inc_angle = np.zeros((z.shape[0], z.shape[1]))
    if method == 1:
        # az_slope = np.zeros((z.shape[0], z.shape[1]))
        # az_slope[1:-1, :] = ((z[2:, :] - z[:-2, :])
        #                      / (2.0 * float(pixel_size_az)))
        if degrees:
            rg_slope_line = np.zeros((z.shape[1]))
            for i in range(1, z.shape[0]-1):
                az_slope_line = ((z[i+1, :] - z[i-1, :])
                                 / (2.0 * float(pixel_size_az)))
                rg_slope_line[1:-1] = ((z[i, 2:] - z[i, :-2]) /
                                       (2.0*float(pixel_size_rg)))
                local_inc_angle[i, :] = np.arccos(
                    (rg_slope_line*np.sin(np.radians(inc[i, :])) +
                     np.cos(np.radians(inc[i, :]))) /
                    (np.sqrt(1+rg_slope_line**2+az_slope_line**2)))
        else:
            for i in range(1, z.shape[0]-1):
                az_slope_line = ((z[i+1, :] - z[i-1, :])
                                 / (2.0 * float(pixel_size_az)))
                rg_slope_line = (z[i, 2:] - z[i, :-2])/(2.0 *
                                                        float(pixel_size_rg))
                local_inc_angle[i, :] = np.arccos((rg_slope_line *
                                                   np.sin(inc[i, :]) +
                                                   np.cos(inc[i, :])) /
                                                  (np.sqrt(1+rg_slope_line **
                                                           2 +
                                                           az_slope_line**2)))
    if method == 2:
        rg_slope_line = np.zeros((z.shape[1]), dtype=np.float32)
        # local_inc_angle = np.zeros((z.shape[0], z.shape[1]))
        for i in range(1, z.shape[0]-1):
            rg_slope_line[1:-1] = (z[i, 2:] - z[i, :-2])/(2.0 *
                                                          float(pixel_size_rg))
            # rg_slope = np.zeros((z.shape[0], z.shape[1]))
            # rg_slope[:, 1:-1] = (z[:, 2:] - z[:, :-2])/(2.0 *
            #                                             float(pixel_size_rg))
            local_inc_angle_line = np.arctan(rg_slope_line)
            if degrees:
                local_inc_angle[i, :] = np.absolute(
                    np.radians(inc[i, :]) - local_inc_angle_line)
            else:
                local_inc_angle[i, :] = np.absolute(inc[i, :] -
                                                    local_inc_angle_line)

    if fs > 1:
        local_inc_angle = plant.filter_data(local_inc_angle,
                                            boxcar=fs)
        return local_inc_angle
    else:
        return local_inc_angle


# *********************************************************************
# *********************************************************************
# *********************************************************************
# ISCE RELATED METHODS
# *********************************************************************
# *********************************************************************
# *********************************************************************


def get_frame_from_pickle(pickle_dir=''):
    '''
    get frame from pickle file
    '''
    return get_parameter_from_pickle('frame', pickle_dir=pickle_dir)


def get_polcal_transmit_and_receive_from_pickle(pickle_dir=''):
    '''
    get ISCE transmit and receive classes from pickle file
    '''
    transmit = get_parameter_from_pickle('transmit', pickle_dir=pickle_dir)
    receive = get_parameter_from_pickle('receive', pickle_dir=pickle_dir)
    return transmit, receive


def get_image_shape_from_frame(frame='', verbose=True):
    '''
    get image shape from frame (without multilooking)
    '''
    if not frame or isinstance(frame, str):
        frame = get_frame_from_pickle(frame)
        if not frame:
            return
    width = frame.getNumberOfSamples()
    if width and verbose:
        print('width (from frame): '+str(round(width)))
    length = frame.getNumberOfLines()
    if length and verbose:
        print('length (from frame): '+str(round(length)))
    return length, width


def get_pixel_sizes_from_frame(frame='', verbose=False, ground=False,
                               default_pixel_size_az=None,
                               default_pixel_size_rg=None,
                               nlooks_az=None,
                               nlooks_rg=None,
                               data=None, data_width=None,
                               data_length=None):
    '''
    get azimuth and range pixels size from ISCE frame
    '''

    if not frame or isinstance(frame, str):
        frame = get_frame_from_pickle(frame)
        if not frame:
            return default_pixel_size_az, default_pixel_size_rg
    if ((nlooks_az is None or nlooks_rg is None) and
            (data or
             (data_length and data_width))):
        ret = get_image_shape_from_frame(frame, verbose=verbose)
        if ret:
            frame_length, frame_width = ret
            if data:
                data_length, data_width = data.shape
            nlooks_az = frame_length / data_length
            if verbose:
                print('nlooks_az (est.): '+str(round(nlooks_az)))
            nlooks_rg = frame_width / data_width
            if verbose:
                print('nlooks_rg (est.): '+str(round(nlooks_rg)))
    if nlooks_az is None:
        nlooks_az = 1
    if nlooks_rg is None:
        nlooks_rg = 1
    instrument = frame.getInstrument()
    pixel_size_rg = instrument.getRangePixelSize()
    if pixel_size_rg is None:
        if verbose:
            print('range pixel size: None')
        pixel_size_rg = default_pixel_size_rg
    else:
        # considering multilooking, presuming, etc..
        if nlooks_rg != 1 and verbose:
            print('range pixel size (ml): %.16f' % (pixel_size_rg))
        pixel_size_rg *= nlooks_rg
        if verbose:
            print('range pixel size: %.16f' % (pixel_size_rg))
        if ground and pixel_size_rg is not None:
            inc_angle = instrument.getIncidenceAngle()
            if not inc_angle:
                print('WARNING considering mean incidence angle 45 deg.')
                inc_angle = 45
            if verbose:
                print('average incidence angle: %.16f' % (inc_angle))
            pixel_size_rg /= math.sin(math.radians(inc_angle))
            if verbose:
                print('range pixel size  (ground): %.16f' % (pixel_size_rg))
    pixel_size_az = instrument.getAzimuthPixelSize()
    if pixel_size_az is not None:
        if nlooks_az != 1 and verbose:
            print('azimuth pixel size (ml): %.16f' % (pixel_size_az))
        # considering multilooking, presuming, etc..
        pixel_size_az *= nlooks_az
        if verbose:
            print('azimuth pixel size: %.16f' % (pixel_size_az))
        return pixel_size_az, pixel_size_rg
    elif verbose:
        print('instrument.getAzimuthPixelSize() returned None')
    velocity = frame.getSchVelocity()
    if velocity is None:
        if verbose:
            print('velocity: None')
        return default_pixel_size_az, pixel_size_rg
    if verbose:
        print('velocity: '+str(velocity))
    if frame.PRF is None:
        if verbose:
            print('prf: None')
        return default_pixel_size_az, pixel_size_rg
    if verbose:
        print('prf [Hz]: '+str(frame.PRF))
        print('prf delay [s]: '+str(1.0/frame.PRF))
    pixel_size_az = velocity * (1.0/frame.PRF)
    if pixel_size_az is None:
        return default_pixel_size_az, pixel_size_rg
    if verbose:
        print('azimuth pixel size: %.16f' % (pixel_size_az))
    return pixel_size_az, pixel_size_rg

# *********************************************************************
# *********************************************************************
# *********************************************************************
# ISCE DEPENDENT METHODS
# *********************************************************************
# Note: ISCE modules are imported only when necessary
# *********************************************************************
# *********************************************************************
# *********************************************************************


def get_files_from_uavsar_ann(ann_file, verbose=True):
    if not plant.isfile(ann_file):
        return
    metadata = plant.read_parameters_annotation_file(ann_file)
    slcHH = metadata['slcHH']
    slcHV = metadata['slcHV']
    slcVH = metadata['slcVH']
    slcVV = metadata['slcVV']
    return slcHH, slcHV, slcVH, slcVV


def get_boundaries_from_uavsar_ann(ann_file, verbose=True):
    '''
    get lat/lon boundaries from UAVSAR annotation file
    '''
    if not plant.isfile(ann_file):
        return
    # from iscesys.Parsers import rdf
    metadata = plant.read_parameters_annotation_file(ann_file)
    if not metadata:
        return
    lat1 = np.degrees(metadata['Approximate Upper Left Latitude'])
    lat2 = np.degrees(metadata['Approximate Upper Right Latitude'])
    lat3 = np.degrees(metadata['Approximate Lower Left Latitude'])
    lat4 = np.degrees(metadata['Approximate Lower Right Latitude'])
    lon1 = np.degrees(metadata['Approximate Upper Left Longitude'])
    lon2 = np.degrees(metadata['Approximate Upper Right Longitude'])
    lon3 = np.degrees(metadata['Approximate Lower Left Longitude'])
    lon4 = np.degrees(metadata['Approximate Lower Right Longitude'])
    lat_arr_temp = [lat1, lat2, lat3, lat4]
    lon_arr_temp = [lon1, lon2, lon3, lon4]
    lat_arr = [np.min([lat_arr_temp]),
               np.max([lat_arr_temp])]
    lon_arr = [np.min([lon_arr_temp]),
               np.max([lon_arr_temp])]
    if verbose:
        print('lat: %.16f, %.16f' % (lat_arr[0], lat_arr[1]))
        print('lon: %.16f, %.16f ' % (lon_arr[0], lon_arr[1]))
    return lat_arr, lon_arr


def get_boundaries_from_frame(frame='', length=0, verbose=True):
    '''
    get lat/lon boundaries from pickle-file frame
    '''
    # from datetime import timedelta
    # d = timedelta(seconds=1)
    if not frame or isinstance(frame, str):
        frame = get_frame_from_pickle(frame)
        if not frame:
            return None

    dr = frame.getInstrument().getRangePixelSize()
    r0 = frame.getStartingRange()
    startTime = frame.getSensingStart()  # +d
    endTime = frame.getSensingStop()  # -d
    # total_time = (endTime-startTime).total_seconds()
    width = frame.getNumberOfSamples()
    rf = r0+(width-1)*dr
    # orbit = db['orbit']
    orbit = frame.getOrbit()
    # length = stateVector.calculateHeight(planet.ellipsoid)
    # print('    length:                  %.16f' % length)
    if verbose:
        print('range: ')
        print('    near range:              %.16f' % r0)
        print('    far range:               %.16f' % rf)
        print('orbit:')
        print('    start time: %s' % (orbit.minTime))
        print('    end time: %s' % (orbit.maxTime))
        print('    total time [s]: %s'
              % (orbit.maxTime-orbit.minTime).total_seconds())
        print('frame:')
        print('    start time: %s' % (startTime))
        print('    end time: %s' % (endTime))
        print('    total time [s]: %s' % (endTime-startTime).total_seconds())
    if startTime < orbit.minTime or startTime > orbit.maxTime:
        print('WARNING frame start time seems to be wrong. ')
        return None, None
    if endTime > orbit.maxTime or endTime < orbit.minTime:
        print('WARNING frame end time seems to be wrong. ')
        return None, None
    lat1, lon1, h1 = orbit.pointOnGround(startTime, r0)
    lat2, lon2, h2 = orbit.pointOnGround(endTime, r0)
    lat3, lon3, h3 = orbit.pointOnGround(startTime, rf)
    lat4, lon4, h4 = orbit.pointOnGround(endTime, rf)
    if (plant.isnan(lat1) or plant.isnan(lon1) or
            plant.isnan(lat2) or plant.isnan(lon2) or
            plant.isnan(lat3) or plant.isnan(lon3) or
            plant.isnan(lat4) or plant.isnan(lon4)):
        if verbose:
            print('length: %d' % (length))
            print('plant.isnan(lat1): %d ' % (plant.isnan(lat1)))
            print('plant.isnan(lat2): %d ' % (plant.isnan(lat2)))
            print('plant.isnan(lat3): %d ' % (plant.isnan(lat3)))
            print('plant.isnan(lat4): %d ' % (plant.isnan(lat4)))
            print('plant.isnan(lon1): %d ' % (plant.isnan(lon1)))
            print('plant.isnan(lon2): %d ' % (plant.isnan(lon2)))
            print('plant.isnan(lon3): %d ' % (plant.isnan(lon3)))
            print('plant.isnan(lon4): %d ' % (plant.isnan(lon4)))
        from isceobj.Planet.Planet import Planet
        planet = Planet(pname='Earth')
        stateVector = orbit._stateVectors[0]
        new_length = stateVector.calculateHeight(planet.ellipsoid)
        if length == new_length:
            return None, None
        return get_boundaries_from_frame(frame, length=new_length,
                                         verbose=verbose)
    lat_arr_temp = [lat1, lat2, lat3, lat4]
    lon_arr_temp = [lon1, lon2, lon3, lon4]
    lat_arr = [np.min([lat_arr_temp]),
               np.max([lat_arr_temp])]
    lon_arr = [np.min([lon_arr_temp]),
               np.max([lon_arr_temp])]
    if verbose:
        print('lat: %.16f, %.16f' % (lat_arr[0], lat_arr[1]))
        print('lon: %.16f, %.16f ' % (lon_arr[0], lon_arr[1]))
    return lat_arr, lon_arr


def get_sensor_name(image_name='', directory='', verbose=False):
    '''
    try identify sensor and return its parameters
    '''
    if not image_name and not directory:
        if verbose:
            print('ERROR please provide a valid input to '
                  'get_sensor_name')
        return
    if image_name and not plant.isfile(image_name):
        if verbose:
            print('ERROR file not found: '+image_name)
        return
    if directory and not path.isdir(directory):
        if verbose:
            print('ERROR file not found: '+image_name)
        return
    if not directory and image_name:
        directory = path.dirname(image_name)
    search_leader_file_name = directory + '/LED*'
    leader_file_list = plant.glob(search_leader_file_name)
    leader_file_name = leader_file_list[0] if leader_file_list else ''
    sensor_name = ''
    # ALOS 1 can be ALOS or ALOS_SLC:
    if leader_file_name:
        ret = get_filename_from_leaderfile(leader_file_name, 'alos',
                                           verbose=verbose)
        if ret is None:
            ret = get_filename_from_leaderfile(leader_file_name, 'alos_slc',
                                               verbose=verbose)
        if ret is None:
            ret = get_filename_from_leaderfile(leader_file_name, 'alos2_slc',
                                               verbose=verbose)
        if ret is not None:
            sensor_name, productLevel = ret
            if productLevel is None:
                productLevel == 1.5
            if sensor_name.startswith('AL1') and productLevel == 1:
                sensor_name = 'ALOS'
            elif sensor_name.startswith('AL1'):
                sensor_name = 'ALOS_SLC'
            elif sensor_name.startswith('AL2'):
                sensor_name = 'ALOS2'
            else:
                if verbose:
                    print('WARNING sensor not identified: '+sensor_name)
                return
        return sensor_name, leader_file_name
    elif 'IMAGERY' in image_name:
        search_leader_file_name = image_name.replace('IMAGERY', 'SARLEADER')
        leader_file_list = plant.glob(search_leader_file_name)
        leader_file_name = leader_file_list[0] if leader_file_list else ''
        if leader_file_name:
            sensor_name = 'ERS'
            return sensor_name, leader_file_name

        # ALOS 1 can be ALOS or ALOS_SLC:

    alos_prefix = 'IMG-'
    fsar_suffix = '-JF.xml'
    uavsar_suffix = 'ann'
    if not image_name:
        search_string = path.join(directory, alos_prefix + '*')
        filelist = plant.glob(search_string)
        if not filelist:
            search_string = path.join(directory, '*' + fsar_suffix)
            filelist = plant.glob(search_string)
            if filelist:
                return 'FSAR', ''
        if not filelist:
            search_string = path.join(directory, '*' + uavsar_suffix)
            filelist = plant.glob(search_string)
        if filelist:
            image_name = filelist[0]
        else:
            return

    if not sensor_name:
        if alos_prefix in image_name:
            # ALOS 1 and 2:
            #     Scene ID = AAAAABBBBBCCCC-YYMMDD
            #     AAAAA : Satellite/Sensor name (ALOS2)
            #     BBBBB : Orbit accumulation number of a scene center
            #     CCCC  : Scene frame number of a scene center
            #          -: separator
            #     YYMMDD: Observation date of scene center (YY: lower 2
            #     figures of a year, MM: month, DD:day)
            image_name_splitted = image_name.split('-')
            if len(image_name_splitted) >= 3:
                Scene_ID = image_name_splitted[2]
            else:
                Scene_ID = ''
            default_sensor = 'ALOS_SLC'
            if Scene_ID.startswith('ALPSR'):
                sensor_name = 'ALOS_SLC'
            elif Scene_ID.startswith('ALOS2'):
                sensor_name = 'ALOS2'
            elif 'ALPSR' in image_name:
                sensor_name = 'ALOS_SLC'
            elif 'ALOS2' in image_name:
                sensor_name = 'ALOS2'
            else:
                if verbose:
                    print('WARNING considering sensor: '+default_sensor)
                sensor_name = default_sensor
        elif image_name.endswith(fsar_suffix):
            sensor_name = 'FSAR'
            # startswith('pp')?
            # endswith('-JF')?
        elif image_name.endswith(uavsar_suffix):
            # from iscesys.Parsers import rdf
            metadata = plant.read_parameters_annotation_file(image_name)
            processing_mode = ''
            try:
                processing_mode = metadata['Processing Mode']
                if verbose:
                    print('processing mode: '+processing_mode)
            except:
                pass
            if not processing_mode:
                try:
                    processing_mode = metadata['Acquisition Mode']
                    if verbose:
                        print('Acquisition mode: '+processing_mode)
                except:
                    pass
            if not processing_mode:
                try:
                    tStart = metadata['Start Time of Acquisition '
                                      'for Pass 1']
                    if tStart:
                        processing_mode = 'RPI'
                        if verbose:
                            print('processing mode 2: '+processing_mode)
                except:
                    pass
            if not processing_mode:
                try:
                    tStart = metadata['Start Time of Acquisition']
                    if tStart:
                        processing_mode = 'Polsar'
                        if verbose:
                            print('processing mode: '+processing_mode)
                except:
                    pass
            if not processing_mode:
                if verbose:
                    print('ERROR it was not possible to read the '
                          '"Processing Mode" (RTI/Polsar).')
                return None
            if processing_mode == 'RPI':
                sensor_name = 'UAVSAR_RPI'
            elif processing_mode.upper() == 'POLSAR':
                sensor_name = 'UAVSAR_Polsar'
            else:
                sensor_name = 'UAVSAR_STACK'
        else:
            if verbose:
                print('ERROR sensor not identified for: '+image_name)
            return
    if verbose:
        if verbose:
            print('sensor name: '+sensor_name)
            if leader_file_name:
                print('leader file: '+leader_file_name)
    return sensor_name, leader_file_name


def get_filename_from_leaderfile(leader_file_name, sensor, verbose=False):
    '''
    get filename and productLevel from leader file_array
    '''
    try:
        from isceobj.Sensor import xmlPrefix
    except ModuleNotFoundError:
        return None, None
    with open(leader_file_name, 'rb') as fp:
        import isceobj.Sensor.CEOS as CEOS
        xml_file = path.join(xmlPrefix, sensor+'/leader_file.xml')
        leaderFDR = CEOS.CEOSDB(xml=xml_file, dataFile=fp)
        leaderFDR.parse()
        fp.seek(leaderFDR.getEndOfRecordPosition())
        filename = leaderFDR.metadata['File Name']
        if verbose:
            print('filename = '+filename)
        try:
            xml_file = path.join(xmlPrefix,
                                 sensor+'/scene_record.xml')
            sceneHeaderRecord = CEOS.CEOSDB(xml=xml_file, dataFile=fp)
            sceneHeaderRecord.parse()
            productLevel = (sceneHeaderRecord.metadata['Product level code'])
            if verbose:
                print('productLevel = '+productLevel)
            productLevel = float(productLevel)
            return filename, productLevel
        except:
            return filename, None
    return None, None


def get_parameter_from_pickle(parameter, pickle_dir='', filename='', level=1):
    '''
    get parameter (e.g. frame, calibration factor, ..) from pickle
    file
    '''
    if not pickle_dir:
        pickle_dir = '.'
    parameter_value = None
    file_pickle = path.join(pickle_dir, 'data') if not filename else filename
    if plant.isfile(file_pickle+'.dat') or plant.isfile(file_pickle):
        try:
            with shelve.open(file_pickle, flag='r', writeback=False) as db:
                parameter_value = db[parameter]
                return parameter_value
        except PermissionError:
            return parameter_value
    return


    '''
    from isceobj.Image import createImage
    latImage = createImage()
    latImage.load(lat_file + '.xml')
    latImage.filename = lat_file
    lonImage = createImage()
    lonImage.load(lon_file + '.xml')
    lonImage.filename = lon_file
    azlooks = round(latImage.getLength() / length_data)
    rglooks = round(latImage.getWidth() / width_data)
    if azlooks > 1 or rglooks > 1:
        from mroipac.looks.Looks import Looks
        if verbose:
            print('    detected multi-looking on input data '
                       '(a, r): (%d, %d)'
                   % (azlooks, rglooks))
        # image is multilooked, need to multilook lat and lon
        lat_file += '.mlk'
        lon_file += '.mlk'
        latImage_mlk = plant.read_image(lat_file,
                                  only_header=True)
        if latImage_mlk is None:
            print('ERROR file not found: %s'
                  %(lat_file))
            sys.exit(1)
        azlooks_mlk = round(latImage_mlk.length / length_data)
        rglooks_mlk = round(latImage_mlk.width / width_data)
        if azlooks_mlk != 1 or rglooks_mlk != 1:
            lkObj = Looks()
            lkObj.setDownLooks(azlooks)
            lkObj.setAcrossLooks(rglooks)
            lkObj.setInputImage(latImage)
            lkObj.setOutputFilename(lat_file)
            lkObj.looks()
            lkObj.setInputImage(lonImage)
            lkObj.setOutputFilename(lon_file)
            lkObj.looks()
        elif verbose:
            print('    found multi-looked georeference files: ' +
                       lat_file + ' and ' + lon_file)
    elif azlooks == 0 or rglooks == 0:
        from isceobj.Image import createImage
        azlooks = round(length_data / latImage.getLength())
        rglooks = round(width_data / latImage.getWidth())
        if verbose:
            print('    detected multi-looking on lat.rdr/'
                  'lon.rdr (a, r): (%d, %d)'
                  % (azlooks, rglooks))
        lkObj = Looks()
        lkObj.setDownLooks(azlooks)
        lkObj.setAcrossLooks(rglooks)
        current_fileImage = createImage()
        current_fileImage.load(current_file + '.xml')
        current_fileImage.filename = current_file
        lkObj.setInputImage(current_fileImage)
        current_file += '.mlk'
        lkObj.setOutputFilename(current_file)
        lkObj.looks()
        '''


def func_gaussian(x, a, offset, sigma):
    return(a*np.exp(-(x-offset)**2/(2*sigma**2)))


def func_gaussians(*args):
    x = args[0]
    n_comp = (len(args)-1) // 3
    output = 0.0
    for i in range(n_comp):
        output += func_gaussian(x,
                                args[i+1],
                                args[i+1+n_comp],
                                args[i+1+2*n_comp])
    return output


def func_poly1(x, p0, p1):
    return(p1*x+p0)


def func_poly2(x, p0, p1, p2):
    return(p2*x**2+p1*x+p0)


def func_poly3(x, p0, p1, p2, p3):
    return(p3*x**3+p2*x**2+p1*x+p0)


def func_poly4(x, p0, p1, p2, p3, p4):
    return(p4*x**4+p3*x**3+p2*x**2+p1*x+p0)


def func_poly5(x, p0, p1, p2, p3, p4, p5):
    return(p5*x**5+p4*x**4+p3*x**3+p2*x**2+p1*x+p0)


def func_exp(x, p0, p1):
    return(p0*np.exp(p1*x))


'''
def func_sigmoid_red(x, x0, u, d):
    k = 1500.
    y = d + (u-d) / (1 + np.exp(k*(x-x0)))
    return y
'''


# u: upper asymptote
# d: lower asymptote
def func_sigmoid(x, x0, u, d, k):
    return d+(u-d)/(1+np.exp(k*(x-x0)))


def func_gen_sigmoid(x, x0, k, a, b, a1):
    return a*x+a1+b/(1+np.exp(-k*(x-x0)))


def func_exp2(x, p0, p1, p2, p3):
    return(p0*np.exp(p1*x)+p2*np.exp(p3*x))


def func_exp2_poly0(x, p0, p1, p2, p3, p4):
    return(p4+p0*np.exp(p1*x)+p2*np.exp(p3*x))


def func_exp2_poly1(x, p0, p1, p2, p3, p4, p5):
    return(p5*x+p4+p0*np.exp(p1*x)+p2*np.exp(p3*x))


def func_exp2_poly2(x, p0, p1, p2, p3, p4, p5, p6):
    return(p6*x**2+p5*x+p4+p0*np.exp(p1*x)+p2*np.exp(p3*x))


def func_log10(x, p0, p1):
    return(p0*np.log10(p1*x))


def func_log10_2(x, p0, p1, p2, p3):
    return(p0*np.log10(p1*x)+p2*np.log10(p3*x))


def func_ln(x, p0, p1):
    return(p0*np.log(p1*x))


def func_ln_2(x, p0, p1, p2, p3):
    return(p0*np.log(p1*x)+p2*np.log(p3*x))


def func_general(x, p0, p1, p2, p3, p4, p5, p6, p7, p8):
    return(p7*np.log(p8*x)+p6*x**2+p5*x+p4+p0*np.exp(p1*x)+p2*np.exp(p3*x))


def func_ln_2_poly2(x, p0, p1, p2, p3, p4):
    return(p3*np.log(p4*x)+p2*x**2+p1*x+p0)


# DBA model backscatter
def func_dba_1(x, fveg, A, B, C, ALPHA, SIGMA,
               string=False):
    if string:
        return('fveg*(A*(1.0-np.exp(-B*x))+'
               'C*(x**ALPHA)*np.exp(-B*x))+'
               '(1-fveg)*SIGMA')
    return(fveg*(A*(1.0-np.exp(-B*x)) +
                 C*(x**ALPHA)*np.exp(-B*x)) +
           (1-fveg)*SIGMA)


def func_dba_1_db(x, fveg, A, B, C, ALPHA, SIGMA,
                  string=False):
    if string:
        return('10*np.log10(fveg*(A*(1.0-np.exp(-B*x))+'
               'C*(x**ALPHA)*np.exp(-B*x))+'
               '(1-fveg)*SIGMA))')
    return(10*np.log10(fveg*(A*(1.0-np.exp(-B*x)) +
                             C*(x**ALPHA)*np.exp(-B*x)) +
                       (1-fveg)*SIGMA))


def func_dba_hv(x, A, B, C, D, ALPHA, GAMMA,
                string=False):
    if string:
        return('A*x**ALPHA*(1.0-np.exp(-B*x))+'
               '(C*x**GAMMA+D)*np.exp(-B*x))')
    return(A*x**ALPHA*(1.0-np.exp(-B*x)) +
           (C*x**GAMMA+D)*np.exp(-B*x))


def func_dba_hv_db(x, A, B, C, D, ALPHA, GAMMA,
                   string=False):
    if string:
        return('10*np.log10(A*B**ALPHA*(1.0-np.exp(-B*x))+'
               '(C*x**GAMMA+D)*np.exp(-B*x)))')
    return(10*np.log10(A*B**ALPHA*(1.0-np.exp(-B*x)) +
           (C*x**GAMMA+D)*np.exp(-B*x)))


# Pulliainen
def func_pulliainen(x, A, B, C, D, ALPHA,
                    string=False):
    if string:
        return('A*(1.0-np.exp(-B*x)) +'
               'C*(x**ALPHA)*np.exp(-B*x)+'
               'D*np.exp(-B*x))')
    return(A*(1.0-np.exp(-B*x)) +
           C*(x**ALPHA)*np.exp(-B*x) +
           D*np.exp(-B*x))


def func_pulliainen_db(x, A, B, C, D, ALPHA,
                       string=False):
    if string:
        return('10*np.log10(A*(1.0-np.exp(-B*x))+'
               'C*(x**ALPHA)*np.exp(-B*x)+'
               'D*np.exp(-B*x)))')
    return(10*np.log10(A*(1.0-np.exp(-B*x)) +
                       C*(x**ALPHA)*np.exp(-B*x) +
                       D*np.exp(-B*x)))


# Pulliainen (VG-ratio)
def func_pulliainen_vg(x, A, B, C, D, ALPHA,
                       string=False):
    if string:
        return('A*(1.0-np.exp(-B*x))/'
               '(C*(x**ALPHA)*np.exp(-B*x)+'
               'D*np.exp(-B*x)))')
    return(A*(1.0-np.exp(-B*x)) /
           (C*(x**ALPHA)*np.exp(-B*x) +
            D*np.exp(-B*x)))


def func_pulliainen_vg_db(x, A, B, C, D, ALPHA,
                          string=False):
    if string:
        return('10*np.log10(A*(1.0-np.exp(-B*x))/'
               '(C*(x**ALPHA)*np.exp(-B*x)+'
               'D*np.exp(-B*x))))')
    return(10*np.log10(A*(1.0-np.exp(-B*x)) /
                       (C*(x**ALPHA)*np.exp(-B*x) +
                        D*np.exp(-B*x))))


# AGB (Mg) allometry
def func_agb_h(x, ALPHA, BETA):
    return(ALPHA*x**BETA)  # x=height


def func_agb_inv(x, ALPHA, BETA):
    return((x/ALPHA)**(1.0/BETA))  # x=AGB


# WCM backscatter
def func_wcm_db(x, a, b, c):
    return(10.0*np.log10(a+b*(1.0-np.exp(-c*x))))


def func_wcm(x, a, b, c):
    return(a+b*(1-np.exp(-c*x)))


def func_wcm_inv(x, a, b, c):
    return(-(1.0/c)*np.log(1.0-(x-a)/b))


def func_wcm_db_inv(x, a, b, c):
    return(-(1.0/c)*np.log(1.0-((10.0**(x/10.0))-a)/b))


# WCM backscatter vgratio
def func_wcm_vgratio_inv(x, a, b, c):
    return(-(1.0/c)*np.log(b*x-a))


def func_wcm_vgratio_db_inv(x, a, b, c):
    return(-(1.0/c)*np.log(b*(10.0**(x/10.0))-a))


def func_wcm_vgratio(x, a, b, c):
    return((np.exp(a*x)+c)/b)


def func_wcm_vgratio_db(x, a, b, c):
    return(10*np.log10((np.exp(a*x)+c)/b))


# WCM backscatter gvratio
def func_wcm_gvratio_inv(x, a, b, c):
    return(-(1.0/c)*np.log(b/x-a))


def func_wcm_gvratio_db_inv(x, a, b, c):
    return(-(1.0/c)*np.log(b*(10.0**(-x/10.0))-a))


def func_wcm_gvratio(x, a, b, c):
    return(b/(np.exp(a*x)+c))


def func_wcm_gvratio_db(x, a, b, c):
    return(10*np.log10(b/(np.exp(a*x)+c)))


# WCM backscatter allometry
def func_wcm_db_h(x, a, b, c, BETA):
    return(10.0*np.log10(a+b*(1.0-np.exp(-c*(x**BETA)))))


def func_wcm_h(x, a, b, c, BETA):
    return(a+b*(1.0-np.exp(-c*(x**BETA))))


def get_function(function_orig, x_data=None, y_data=None):
    function_name = function_orig.split(' ')[0]
    initial_points = None
    current_module = sys.modules[__name__]
    all_functions = inspect.getmembers(current_module,
                                       inspect.isfunction)
    func = None
    bounds = None
    for x in all_functions:
        if x[0].replace('func_', '') == function_name.lower():
            func = x[1]
    if func is None:
        return
    if 'sigmoid' == function_name:
        # bounds = ([-np.inf, -np.inf, -np.inf],
        #          [np,inf, np.inf, np.inf])
        initial_points = [0., 1., 0., 1.]
        if x_data is not None:
            initial_points[0] = np.nanmean(x_data)
            initial_points[3] = 100.0/(np.nanpercentile(x_data, 90) -
                                       np.nanpercentile(x_data, 10))
        if y_data is not None:
            initial_points[1] = np.nanpercentile(y_data, 90)
            initial_points[2] = np.nanpercentile(y_data, 10)
    elif 'dba_hv_db' == function_name:
        # x, A, B, C, D, ALPHA, GAMMA
        initial_points = [0.638, 0.0326, 1.02e-16, 9.73e-17, 0.721, 0.473]
        bounds = ([0., 0., 0., 0., 0., 0.],
                  [np.inf, np.inf, np.inf, np.inf, 1., 1.])
    elif 'dba_hv' == function_name:
        initial_points = [0.638, 0.0326, 1.02e-16, 9.73e-17, 0.721, 0.473]
        bounds = ([0., 0., 0., 0., 0., 0.],
                  [np.inf, np.inf, np.inf, np.inf, 1., 1.])
    elif 'dba_1_db' == function_name:
        bounds = ([0.0, 0., 0., 0., 0, 0],
                  [1.0, np.inf, np.inf, np.inf, 1.0, np.inf])
    elif 'dba_1' == function_name:
        bounds = ([0.0, 0., 0., 0., 0, 0],
                  [1.0, np.inf, np.inf, np.inf, 1.0, np.inf])
    elif 'pulliainen_vg_db' == function_name:
        bounds = ([-np.inf, -np.inf, -np.inf, -np.inf, 0.0],
                  [np.inf, np.inf, np.inf, np.inf, 2.0])
    elif 'pulliainen_vg' == function_name:
        bounds = ([-np.inf, -np.inf, -np.inf, -np.inf, 0.0],
                  [np.inf, np.inf, np.inf, np.inf, 2.0])
    elif 'pulliainen_db' == function_name:
        bounds = ([-np.inf, -np.inf, -np.inf, -np.inf, 0.0],
                  [np.inf, np.inf, np.inf, np.inf, 2.0])
    elif 'pulliainen' == function_name:
        bounds = ([-np.inf, -np.inf, -np.inf, -np.inf, 0.0],
                  [np.inf, np.inf, np.inf, np.inf, 2.0])
    elif 'wcm_db_inv' == function_name:
        bounds = ([-np.inf, -np.inf, 0],
                  [np.inf, np.inf, np.inf])
    elif 'wcm_db' == function_name:
        initial_points = [0.0, 0.27, 0.08]
        # initial_points = [0.00542, 0.0795, 0.0164]
        # initial_points = [-16.59, 4.63, 0.014]
        bounds = ([-np.inf, -np.inf, 0],
                  [np.inf, np.inf, np.inf])
    elif 'wcm' == function_name:
        initial_points = [0.00542, 0.795, 0.0164]
        # initial_points = [0.00542, 0.0795, 0.0164]
        # initial_points = [-16.59, 4.63, 0.014]
        bounds = ([-np.inf, -np.inf, 0],
                  [np.inf, np.inf, np.inf])
    else:
        # bounds = None
        bounds = (-np.inf, np.inf)
    var_names = func.__code__.co_varnames
    if 'string' in var_names:
        n_coefficients = func.__code__.co_argcount - 2
    else:
        n_coefficients = func.__code__.co_argcount - 1
    return func, n_coefficients, bounds, initial_points


def get_function_equation(function_orig, function_fit_y):
    flag_invert_equation = False
    if isinstance(function_orig, str):
        ret = get_function(function_orig)
        if ret is None and '_inv' in function_orig:
            ret = get_function(function_orig.replace('_inv', ''))
            flag_invert_equation = True
        if ret is None:
            return
        func, *_ = ret
    else:
        func = function_orig
    var_names = func.__code__.co_varnames

    if 'string' in var_names:
        args = var_names[:-1]
        equation_str = func(*args, string=True)
    else:
        args = var_names
        current_module_file = func.__code__.co_filename
        with open(current_module_file, 'r', encoding="ISO-8859-1") as f:
            lines = f.readlines()
        equation_str = lines[func.__code__.co_firstlineno]
        if 'return' not in equation_str:
            print('ERROR please use string mode to read equation from %s'
                  % function_orig)
            return ''
        equation_str = equation_str.replace('return', '')
        while equation_str != equation_str.replace(' ', ''):
            equation_str = equation_str.replace(' ', '')
        equation_str = equation_str.replace('\n', '')
        if (equation_str.startswith('(') and
                equation_str.endswith(')')):
            equation_str = equation_str[1:-1]

    equation_str = equation_str.replace('np.', '')
    args_in = args[1:]
    args_sorted = sorted(args_in, key=len, reverse=True)
    for arg in args_sorted:
        equation_str = equation_str.replace(
            arg,
            str(function_fit_y[args_in.index(arg)]))
    equation_str = equation_str.replace('--', '+')
    equation_str = equation_str.replace('++', '+')
    equation_str = equation_str.replace('+-', '-')
    equation_str = equation_str.replace('(+', '(')
    if flag_invert_equation:
        equation_str = 'inverse of '+equation_str
    return equation_str


def generate_interferogram(master_image,
                           slave_image,
                           nlooks,
                           phase_ref=None,
                           verbose=True,
                           flag_complex=False,
                           flag_complex_coherence=False,
                           **kwargs):
    if flag_complex_coherence:
        denominator = (np.absolute(master_image) *
                       np.absolute(slave_image))
        if verbose:
            print('filtering denominator..')
            denominator = plant.filter_data(denominator,
                                            nlooks=nlooks,
                                            verbose=verbose,
                                            **kwargs)
    interferogram = master_image*np.conjugate(slave_image)
    if phase_ref is not None:
        interferogram *= np.exp(1j*(phase_ref))
    # interferogram = np.conjugate(interferogram)
    if flag_complex_coherence and verbose:
        print('filtering numerator..')
    interferogram = plant.filter_data(interferogram,
                                      nlooks=nlooks,
                                      verbose=verbose,
                                      **kwargs)
    if flag_complex_coherence and verbose:
        print('..done')
    if flag_complex_coherence:
        interferogram /= denominator
        return interferogram
    if not flag_complex:
        return np.angle(interferogram)
    return interferogram


# gamma_1 closer to the unitary circle intersection (ground),
# gamma_2 closer to volume
def find_unitary_circle_intersection(gamma_1,
                                     gamma_2,
                                     v_gt_g=False,
                                     g_gt_v=False):
    if v_gt_g and g_gt_v:
        print('ERROR please select only one option: v_gt_g or g_gt_v')
        return
    if v_gt_g or g_gt_v:
        output_image = np.zeros((gamma_1.shape), dtype=gamma_1.dtype)
        if v_gt_g:
            condition = plant.wrap_phase(np.angle(gamma_2) -
                                         np.angle(gamma_1)) >= 0
        else:
            condition = plant.wrap_phase(np.angle(gamma_2) -
                                         np.angle(gamma_1)) <= 0
        ind = np.where(condition)
        output_image[ind] = find_unitary_circle_intersection(gamma_1[ind],
                                                             gamma_2[ind])
        ind = np.where(not(condition))
        output_image[ind] = find_unitary_circle_intersection(gamma_2[ind],
                                                             gamma_1[ind])
        return output_image
    A = np.absolute(gamma_2)**2-1
    B = 2*np.real((gamma_1-gamma_2)*np.conj(gamma_2))
    C = np.absolute(gamma_1-gamma_2)**2
    F = (-B-np.sqrt(B**2-4*A*C))/(2*A)
    output_image = np.angle(gamma_1 - gamma_2*(1-F))
    return output_image


def coh_rmog(kz, theta, wavelength, phi, hv, m_dB, ke_dB, sg, sv, alpha,
             HR=None):
    '''
    Author       : Marco Lavalle (marco.lavalle@jpl.nasa.gov)
    (converted to Python by Gustavo H. X. Shiroma)
   
    Description  : This routine calculates the complex
                   temporal-volume coherence observed by a
                   repeat-pass polarimetric SAR interferometer
                   according to the random-motion-over-ground (RMoG) model

    References   : M. Lavalle, "Full and Compact Polarimetric Radar
                   Interferometry for Vegetation Remote Sensing",
                   PhD Dissertation, University of Rome and
                   University of Rennes, Dec. 2009.
                   M. Lavalle, M. Simard and S. Hensley, "A temporal
                   decorrelation model for polarimetric radar
                   interferometers," IEEE TGRS, 2011.

    Usage        : coh_rmog(<model parameters>)
                   where <model parameters> is

                   - kz, vertical wavenumber [m^-1]
                   - theta, look angle [rad]
                   - wavelength, wavelength [m]
                   - phi, ground-topography phase [rad]
                   - hv, canopy height [m]
                   - m_dB, ground-to-volume ratio [dB]
                   - ke_dB, mean extinction coefficient [dB] ~= 0
                   - sg, motion st. dev. of ground [m]
                   - sv, motion st. dev. of canopy [m]
                   - alpha, ground range tilt [rad]
    '''
    if HR is None:
        HR = 15  # arbitrary reference height [m]

    # Sanity check
    if (sum(wavelength < 0) > 0 or sum(hv < 0) > 0 or
            sum(sg < 0) > 0 or sum(sv < 0) > 0):
        print('ERROR please check input parameters.')
        return

    # Conversion from dB
    ke = (ke_dB)/4.34  # extinction coefficient [neper/m]
    m = 10**((m_dB)/10)  # ground-to-volume ratio [-]

    # Temporary variables
    k2 = (4*np.pi / wavelength) ** 2
    la = theta - alpha  # angle of incidence [rad]
    ds2 = sv**2 - sg**2  # differential motion variance [m^2]
    p1 = 2*ke / np.cos(la)
    p2 = p1 + 1j*kz
    p3 = -ds2 / (2*HR) * k2
    p4 = np.exp(1j*phi)

    # Temporal decorrelation definition
    coh_g = np.exp(-0.5 * k2 * sg ** 2)
    p_ratio = (p1 * (np.exp((p2+p3)*hv) - 1)) / ((p2+p3)*(np.exp(p1*hv) - 1))

    if not isinstance(hv, numbers.Number):
        p_ratio[np.where(hv < 1e-6)] = 1.0
    elif hv < 1e-6:
        p_ratio = 1.0

    coh_v = p4 * coh_g * p_ratio
    coh = p4 * (m * coh_g + coh_v * np.conj(p4)) / (m + 1)

    # Check coherence magnitude
    if np.sum(abs(coh) > 1) > 0:
        coh = None
    return coh


def get_topo_files(topo_dir):
    if not topo_dir:
        return
    # print('*** topo_dir:', topo_dir)
    ret_dict = {}
    topo_dict = {}
    if isinstance(topo_dir, plant.PlantImage):
        for b in range(topo_dir.nbands):
            band = topo_dir.get_band(band=b)
            if band.name == plant.Y_BAND_NAME:
                ret_dict['lat_file'] = band
            elif band.name == plant.X_BAND_NAME:
                ret_dict['lon_file'] = band
        if len(ret_dict.keys()) == 0:
            ret_dict['lon_file'] = topo_dir.get_band(band=0)
            ret_dict['lat_file'] = topo_dir.get_band(band=1)
            if topo_dir.nbands > 2:
                ret_dict['z_file'] = topo_dir.get_band(band=2)
    elif plant.isfile(topo_dir) and (topo_dir.endswith('.llh')):
        # UAVSAR
        ret_dict['lat_file'] = topo_dir + ':0'
        ret_dict['lon_file'] = topo_dir + ':1'
        ret_dict['z_file'] = topo_dir + ':2'
    elif path.isdir(topo_dir):
        if _is_file_rdr_or_vrt(path.join(topo_dir, 'lat.rdr')):
            topo_dict['lat_file'] = 'lat.rdr'
        elif _is_file_rdr_or_vrt(path.join(topo_dir, 'y.rdr')):
            topo_dict['lat_file'] = 'y.rdr'

        if _is_file_rdr_or_vrt(path.join(topo_dir, 'lon.rdr')):
            topo_dict['lon_file'] = 'lon.rdr'
        elif _is_file_rdr_or_vrt(path.join(topo_dir, 'x.rdr')):
            topo_dict['lon_file'] = 'x.rdr'

        if _is_file_rdr_or_vrt(path.join(topo_dir, 'localInc.rdr')):
            # topo_dict['lia_file'] = 'localInc.rdr'
            topo_dict['inc_file'] = 'localInc.rdr'
        elif _is_file_rdr_or_vrt(path.join(topo_dir, 'inc.rdr')):
            # topo_dict['lia_file'] = 'inc.rdr:1'
            topo_dict['inc_file'] = 'inc.rdr:1'

        # if _is_file_rdr_or_vrt(path.join(topo_dir, 'inc.rdr')):
        #     topo_dict['inc_file'] = 'inc.rdr:1'

        if _is_file_rdr_or_vrt(path.join(topo_dir, 'localPsi.rdr')):
            topo_dict['psi_file'] = 'localPsi.rdr'
        elif _is_file_rdr_or_vrt(path.join(topo_dir, 'inc.rdr')):
            topo_dict['psi_file'] = 'inc.rdr:0'
        topo_dict['simamp_file'] = 'simamp.rdr'

        if _is_file_rdr_or_vrt(path.join(topo_dir, 'los.rdr')):
            topo_dict['los_file'] = 'los.rdr:0'
        topo_dict['z_file'] = 'z.rdr'
        for topo_var, topo_file in topo_dict.items():
            _get_topo_files_append_file(ret_dict, topo_dir, topo_var,
                                        topo_file)
    elif topo_dir.endswith('.vrt'):
        # topo.vrt
        ret_dict['lon_file'] = topo_dir + ':0'
        ret_dict['lat_file'] = topo_dir + ':1'
        ret_dict['z_file'] = topo_dir + ':2'
    elif plant.isfile(topo_dir):
        image_obj = plant.read_image(topo_dir)
        return get_topo_files(image_obj)
    else:
        print('ERROR topo_dir/geolocation arrays not found in:'
              f' {topo_dir}')
    # print('*** ret_dict: ', ret_dict)
    return ret_dict


def shift_image_3d_inc(image_3d, inc, pixel_size_x,
                       height_min, height_max, height_step,
                       inverse=False):
    height_array = np.arange(height_min, height_max, height_step)
    out_image_3d = np.zeros_like(image_3d)
    x_pos, _ = np.meshgrid(np.arange(0, image_3d.shape[2]),
                           np.arange(0, image_3d.shape[0]))
    for i in range(height_array.size):
        height = height_array[i]
        out_image_3d[:, i, :] = shift_profile_inc(
            image_3d[:, i, :], inc, pixel_size_x, height,
            x_pos=x_pos, inverse=inverse)
    return out_image_3d


def shift_dem_inc(*args, **kwargs):
    if len(args) == 0:
        return
    kwargs['height'] = args[0]
    ret = shift_profile_inc(*args, **kwargs)
    return ret


def shift_profile_inc(image, inc, pixel_size_x, height,
                      x_pos=None, inverse=False):
    if x_pos is None:
        x_pos, _ = np.meshgrid(np.arange(0, image.shape[1]),
                               np.arange(0, image.shape[0]))
    direction = 1 if not inverse else -1
    inc_shift = x_pos+direction*(height*np.cos(inc)/pixel_size_x)
    out_image = np.zeros_like(image, dtype=np.float32)
    for line in range(image.shape[0]):
        f = interp1d(inc_shift[line, :], image[line, :],
                     bounds_error=False, fill_value=0)
        out_image[line, :] = f(np.arange(0, image.shape[1]))
    ind = np.where(plant.isnan(image))
    out_image[ind] = np.nan
    return out_image


def _is_file_rdr_or_vrt(filename):
    flag_is_file = (plant.isfile(filename) or
                    plant.isfile(filename.replace('.rdr', '.vrt')) or
                    plant.isfile(filename+'.vrt'))
    return flag_is_file


def _get_topo_files_append_file(ret_dict, topo_dir, topo_var, topo_file):
    current_file = path.join(topo_dir, topo_file)
    if plant.isfile(current_file):
        ret_dict[topo_var] = current_file
    elif plant.isfile(current_file.replace('.rdr', '.vrt')):
        ret_dict[topo_var] = current_file.replace('.rdr', '.vrt')
    elif plant.isfile(current_file+'.vrt'):
        ret_dict[topo_var] = current_file+'.vrt'
    else:
        ret_dict[topo_var] = None


# UAVSAR
def parse_uavsar_filename(input_filename, verbose=False):
    try:
        ret = _parse_uavsar_filename(input_filename, verbose=verbose)
    except:
        return
    return ret


def _parse_uavsar_filename(input_filename, verbose=False):
    ret_dict = {}
    filename_splitted = path.basename(input_filename).split('.')
    if len(filename_splitted) > 1:
        ret_dict['ext'] = filename_splitted[1]
    filename = filename_splitted[0]
    ret_dict['baseline_corrected'] = 'BC' in filename
    filename = filename.replace('_BC', '')
    ret_dict['baseline_uncorrected'] = 'BU' in filename
    filename = filename.replace('_BU', '')
    filename_splitted = filename.split('_')
    # print('*** filename splitted: ', filename_splitted)
    if (len(filename_splitted) < 4 or
            (len(filename_splitted) == 4 and
             len(filename_splitted[2]) == 5)):
        return
    ret_dict['site_name'] = filename_splitted[0]
    ret_dict['line_id'] = filename_splitted[1]
    # print('*** filename_splitted[2]: ', filename_splitted[2])
    if (len(filename_splitted[2]) == 5 or
            len(filename_splitted[2]) == 9):
        flight_id = filename_splitted[2]
        ret_dict['flight_id'] = flight_id
        ret_dict['year'] = flight_id[0:2]
        ret_dict['flight_number'] = flight_id[2:]
        stack_offset = 3
        if len(filename_splitted[2]) == 9:
            ret_dict['flight_line'] = flight_id[-3:]

        if len(filename_splitted[stack_offset]) == 9:
            # InSAR
            ret_dict['slave_flight_id'] = filename_splitted[stack_offset]
            ret_dict['number_of_days'] = filename_splitted[stack_offset+1][:-1]
            ret_dict['insar_id'] = filename_splitted[stack_offset+2]
            stack_offset += 3

        else:
            ret_dict['data_take_number'] = filename_splitted[stack_offset]
            stack_offset += 1
            if len(filename_splitted) > stack_offset:
                ret_dict['acquisition_date'] = filename_splitted[stack_offset]
                stack_offset += 1
        if len(filename_splitted) > stack_offset:
            ret_dict['band'] = filename_splitted[stack_offset][0]
            steering_offset = 1
            if ret_dict['band'] == 'P':
                stack_offset += 1
                steering_offset = 0
            steering = filename_splitted[stack_offset][steering_offset]
            polarization = ''
            while (not plant.isnumeric(steering) and
                   len(steering) != 0):
                polarization = steering[-1]+polarization
                steering = steering[0:-1]
            ret_dict['steering'] = steering
            ret_dict['polarization'] = polarization
            stack_offset += 1
    else:
        stack_offset = 2
    if len(filename_splitted) <= stack_offset:
        return
    ret_dict['stack_number'] = filename_splitted[stack_offset]
    if len(filename_splitted) > stack_offset+1:
        ret_dict['segment_number'] = filename_splitted[stack_offset+1]
    if len(filename_splitted) > stack_offset+2:
        ret_dict['downsample_factor'] = filename_splitted[stack_offset+2]
    # print('*** ', ret_dict)
    if verbose:
        plant.print_dict(ret_dict)
        # pprint(ret_dict)
    return ret_dict


def get_uavsar_inc(metadata, lkv_obj, llh_obj, input_key='lia'):
    peg_lat = np.radians(metadata['Peg Latitude'])
    peg_lon = np.radians(metadata['Peg Longitude'])
    peg_hdg = np.radians(metadata['Peg Heading'])
    lkv = np.dstack((lkv_obj.get_image(band=0),
                     lkv_obj.get_image(band=1),
                     lkv_obj.get_image(band=2)))
    lkv = -plant.normalize_vector(lkv)
    ret_dict = {}
    # pixel_size_x = metadata['slc_1_2x8_mag.col_mult']
    # pixel_size_y = metadata['slc_1_2x8_mag.row_mult']
    # print('pixel size x: ', pixel_size_x)
    # print('pixel size y: ', pixel_size_y)
    if 'los' in input_key:
        los = np.arccos(np.absolute(lkv[:, :, 2]))
        ret_dict['los'] = los
    if ('inc' in input_key or
            'lia' in input_key or
            'psi' in input_key):
        height = llh_obj.get_image(band=2)
    if 'inc' in input_key or 'psi' in input_key:
        height_const_h = np.full_like(height, np.nanmean(height))
        llh_const_h = plant.llh2enu(
            np.radians(llh_obj.get_image(band=1)),
            np.radians(llh_obj.get_image(band=0)),
            height_const_h,
            peg_lat, peg_lon, peg_hdg)
    if 'inc' in input_key:
        normal_versor = plant.get_normal_versor(llh_const_h)
        cos_dot = plant.inner(lkv, normal_versor)
        inc = np.arccos(cos_dot)
        ret_dict['inc'] = inc
    if 'lia' in input_key or 'psi' in input_key:
        llh = plant.llh2enu(np.radians(llh_obj.get_image(band=1)),
                            np.radians(llh_obj.get_image(band=0)),
                            height,
                            peg_lat, peg_lon, peg_hdg)
        normal_versor = plant.get_normal_versor(llh)
        if 'psi' in input_key:
            # print('ERROR not implemented yet')
            # velocity = np.zeros_like(llh_const_h)
            velocity = np.gradient(llh_const_h, axis=0)
            # velocity[..., 0] = np.gradient(llh_const_h[..., 0], axis=0)
            # velocity[..., 1] = np.gradient(-llh_const_h[..., 1], axis=0)
            # velocity[..., 2] = np.gradient(llh_const_h[..., 2], axis=0)
            # velocity[..., 2] = 0.0
            print('velocity: ')
            plant.print_mean_vector(velocity)
            # plant.imshow(velocity[..., 2])
            image_normal = np.cross(lkv, velocity)
            print('lkv: ')
            plant.print_mean_vector(lkv)
            image_normal = plant.normalize_vector(image_normal)
            # plant.imshow(lkv[..., 2])
            print('image normal: ')
            plant.print_mean_vector(image_normal)
            print('normal versor: ')
            plant.print_mean_vector(normal_versor)
            cos_dot = plant.inner(image_normal, normal_versor)
            # plant.imshow(image_normal[..., 0])
            # plant.imshow(image_normal[..., 1])
            # plant.imshow(image_normal[..., 2])
            psi = np.arccos(cos_dot)
            ret_dict['psi'] = psi
        if 'lia' in input_key:
            cos_dot = plant.inner(lkv, normal_versor)
            # plant.imshow(cos_dot)
            # ret_dict['cos_dot'] = normal_versor[:, :, 1]
            inc = np.arccos(cos_dot)
            ret_dict['lia'] = inc
        '''
        cos_dot = ((ref[:, :, 0]*alpha+ref[:, :, 1]*beta-ref[:, :, 2]) /
                   (ref_norm*np.sqrt(1.0+alpha**2+beta**2)))
        ang_ref = np.arccos(cos_dot)
        '''
    return ret_dict

    '''
    # target vector pointing to the aircraft in ENU
    # llh_m_lkv = llh - lkv

    # velocity
    velocity = np.gradient(llh - lkv, axis=0)
    velocity_norm = np.linalg.norm(velocity, axis=2)
    velocity_norm = velocity_norm[:, :, np.newaxis]
    velocity_versor = velocity / velocity_norm

    # projected lkv in velocity direction
    lkv_v = (np.sum(lkv*velocity_versor, axis=2)[:, :, np.newaxis] *
             velocity_versor)

    # cross-track incident vector
    incident_lkv = lkv - lkv_v
    inc = np.arccos(np.abs(incident_lkv[:, :, 2]) /
                    np.linalg.norm(incident_lkv, axis=2))
    return inc
    '''


'''
def get_annotation_file_old(filename):
    filename_splitted = filename.split('_')
    if 'ML3X3' in filename:
        last_root_ind = -4
        ann_extension = 'ML3X3.ann'
    elif 'ML5X5' in filename:
        last_root_ind = -4
        ann_extension = 'ML5X5.ann'
    else:
        last_root_ind = -3
        ann_extension = '.ann'
    if len(filename_splitted) > 3:
        header_file_search_string = \
            ('_'.join(filename_splitted[:last_root_ind]) +
             '*'+ann_extension)
    else:
        header_file_search_string = filename.replace(extension,
                                                     ann_extension)
    header_file_list = plant.glob(header_file_search_string)
    return header_file_list
'''


# Sentinel
def sentinel_get_dates(filename):
    ret = []
    for offset in [14, 30]:
        try:
            year = int(filename[14:18])
            month = int(filename[18:20])
            day = int(filename[20:22])
            hour = int(filename[23:25])
            minute = int(filename[25:27])
            second = int(filename[27:29])
        except ValueError:
            print('error parsing: %s' % filename)
            ret.append(None)
            continue
        date_elem = datetime.datetime(year, month, day,
                                      hour, minute, second)
        ret.append(date_elem)
    return ret


def sentinel_get_calibration_vectors(calibration_xml,
                                     flag_beta=False,
                                     flag_sigma=False,
                                     flag_gamma=False):
    '''
    code from ISCE (isce/components/isceobj/Sensor/GRD/Sentinel1.py)
    '''
    import os
    from scipy.interpolate import RectBivariateSpline
    import xml.etree.ElementTree as ElementTree
    flag_all = not (flag_beta or flag_sigma or flag_gamma)

    if calibration_xml is None:
        print('ERROR no calibration file provided')
        return
    if calibration_xml.startswith('/vsizip'):
        import zipfile
        parts = calibration_xml.split(os.path.sep)
        zipname = os.path.join(*(parts[2:-4]))
        fname = os.path.join(*(parts[-4:]))
        try:
            with zipfile.ZipFile(zipname, 'r') as zf:
                xmlstr = zf.read(fname)
        except:
            print('ERROR could not read calibration file:'
                  ' %s' % calibration_xml)
    else:
        try:
            with open(calibration_xml, 'r') as fid:
                xmlstr = fid.read()
        except:
            print('ERROR could not read calibration file: %s'
                  % calibration_xml)
    _xml_root = ElementTree.fromstring(xmlstr)
    node = _xml_root.find('calibrationVectorList')
    length = int(node.attrib['count'])
    lines = []
    pixels = []
    beta_nought = None
    sigma_nought = None
    gamma_nought = None
    for i, child in enumerate(node.getchildren()):
        pixnode = child.find('pixel')
        width = int(pixnode.attrib['count'])
        if i == 0:
            beta_nought = np.zeros((length, width))
            sigma_nought = np.zeros((length, width))
            gamma_nought = np.zeros((length, width))
            pixels = [float(x) for x in pixnode.text.split()]
        lines.append(int(child.find('line').text))
        if flag_all or flag_beta:
            signode = child.find('betaNought')
            beta_nought[i, :] = [float(x) for x in signode.text.split()]
        if flag_all or flag_sigma:
            signode = child.find('sigmaNought')
            sigma_nought[i, :] = [float(x) for x in signode.text.split()]
        if flag_all or flag_gamma:
            signode = child.find('gamma')
            gamma_nought[i, :] = [float(x) for x in signode.text.split()]
    lines = np.array(lines)
    pixels = np.array(pixels)
    ret = []
    if flag_all or flag_beta:
        beta_lut = RectBivariateSpline(lines, pixels, beta_nought,
                                       kx=1, ky=1)
        ret.append(beta_lut)
    if flag_all or flag_sigma:
        sigma_lut = RectBivariateSpline(lines, pixels, sigma_nought,
                                        kx=1, ky=1)
        ret.append(sigma_lut)
    if flag_all or flag_gamma:
        gamma_lut = RectBivariateSpline(lines, pixels, gamma_nought,
                                        kx=1, ky=1)
        ret.append(gamma_lut)
    if len(ret) == 0:
        return None
    if len(ret) == 1:
        return ret[0]
    return ret


def get_backward_map_fast(y1map_orig, x1map_orig, y2dim, x2dim, y2step, x2step,
                          y2min, x2min, data1=None, data2=None):
    '''
    get backward projection indexes map for data. The drawback of this
    function is that the holes of the backward map are not covered which
    can be a problem when working with high resolution backward mapping.
    '''
    y1map = np.asarray(((y1map_orig - y2min)/y2step), dtype=np.uint32)
    x1map = np.asarray(((x1map_orig - x2min)/x2step), dtype=np.uint32)
    indexes = np.where(x1map > (x2dim-1))
    x1map[indexes] = x2dim-1
    indexes = np.where(y1map > (y2dim-1))
    y1map[indexes] = y2dim-1
    y1dim = y1map.shape[0]
    x1dim = y1map.shape[1]
    x1indexes = np.tile(np.arange(0, x1dim, 1), (y1dim, 1))
    y1indexes = np.rot90(np.tile(np.arange(0, y1dim, 1),
                                 (x1dim, 1)), 1)

    # ref 1 to ref 2
    if data1 is not None:
        # data1_temp = np.copy(data1)
        n_looks_x = 1.0
        n_looks_y = 1.0
        length_data2_temp = np.asarray(y1dim//n_looks_y,
                                       dtype=np.int)
        width_data2_temp = np.asarray(x1dim//n_looks_x,
                                      dtype=np.int)
        data2 = np.zeros((length_data2_temp,
                          width_data2_temp), dtype=np.float32)
        ndata2 = np.zeros((length_data2_temp,
                           width_data2_temp), dtype=np.float32)
        y1indexes = y1indexes // n_looks_y
        x1indexes = x1indexes // n_looks_x
        x1indexes = np.asarray(x1indexes, dtype=np.uint32)
        y1indexes = np.asarray(y1indexes, dtype=np.uint32)
        indexes = np.where(y1indexes >= (length_data2_temp-1))
        y1indexes[indexes] = length_data2_temp-1
        indexes = np.where(x1indexes >= (width_data2_temp-1))
        x1indexes[indexes] = width_data2_temp-1
        data2[y1indexes, x1indexes] += data1[y1map, x1map]
        ndata2[y1indexes, x1indexes] += 1
        indexes = np.where(ndata2 > 0)
        data2[indexes] /= ndata2[indexes]
        indexes = np.where(ndata2 == 0)
        data2 = plant.insert_nan(data2,
                                 indexes,
                                 out_null=np.nan)
        # data2[indexes] = np.nan
        return data2

    # ref 2 to ref 1
    if data2 is not None:
        length_data2_temp, width_data2_temp = data2.shape
        n_looks_x = x1dim // width_data2_temp
        n_looks_y = y1dim // length_data2_temp
        data1 = np.zeros((y2dim, x2dim), dtype=np.float32)
        ndata1 = np.zeros((y2dim, x2dim), dtype=np.float32)
        y1indexes = y1indexes // n_looks_y
        x1indexes = x1indexes // n_looks_x
        indexes = np.where(y1indexes >= (length_data2_temp-1))
        y1indexes[indexes] = length_data2_temp-1
        indexes = np.where(x1indexes >= (width_data2_temp-1))
        x1indexes[indexes] = width_data2_temp-1
        data1[y1map, x1map] += data2[y1indexes, x1indexes]
        ndata1[y1map, x1map] += 1
        indexes = np.where(ndata1 > 0)
        data1[indexes] /= ndata1[indexes]
        indexes = np.where(ndata1 == 0)
        data1 = plant.insert_nan(data1,
                                 indexes,
                                 out_null=np.nan)
        # data1[indexes] = np.nan
        return data1


def get_backward_map_high_res(y1map_orig, x1map_orig, y2dim, x2dim,
                              y2step, x2step, y2min, x2min,
                              data1=None, data2=None):
    '''
    get backward projection indexes map for data. This function is intended
    to cover holes by using interpolation of the geolocation indexes
    (x1_2 and y1_2).
    '''

    y1map = np.asarray(((y1map_orig - y2min)/y2step), dtype=np.uint32)
    x1map = np.asarray(((x1map_orig - x2min)/x2step), dtype=np.uint32)
    indexes = np.where(x1map > (x2dim-1))
    x1map[indexes] = x2dim-1
    indexes = np.where(y1map > (y2dim-1))
    y1map[indexes] = y2dim-1
    x1_2 = np.zeros((y2dim, x2dim), dtype=np.uint32)
    y1_2 = np.zeros((y2dim, x2dim), dtype=np.uint32)
    y1dim = y1map.shape[0]
    x1dim = y1map.shape[1]
    x1indexes = np.tile(np.arange(0, x1dim, 1), (y1dim, 1))
    y1indexes = np.rot90(np.tile(np.arange(0, y1dim, 1),
                                 (x1dim, 1)), 1)
    x1_2[y1map, x1map] = x1indexes
    y1_2[y1map, x1map] = y1indexes

    # x1_2 = filter_map(x1_2, boxcar_filter_size=15)
    # y1_2 = filter_map(y1_2, boxcar_filter_size=15)

    # ref 1 to ref 2
    if data1 is not None:
        y_to_restore = y1map[0, 0]
        y1map = 0
        x_to_restore = x1map[0, 0]
        x1map = 0
        data1_temp = np.copy(data1)
        data1_temp_0_0 = data1_temp[0, 0]
        data1_temp = plant.insert_nan(data1_temp,
                                      indexes=[0, 0],
                                      out_null=np.nan)

        data2 = np.zeros((y1dim, x1dim))

        data2 = plant.insert_nan(data2,
                                 out_null=np.nan)
        data2[y1_2, x1_2] = data1_temp
        data2[y_to_restore, x_to_restore] = data1_temp_0_0
        print('..done')
        return data1_temp

    # ref 2 to ref 1
    if data2 is not None:
        data2_temp = np.copy(data2)
        data2_temp_0_0 = data2_temp[0, 0]
        data2_temp = plant.insert_nan(data2_temp,
                                      [0, 0],
                                      out_null=np.nan)
        length_data2_temp, width_data2_temp = data2_temp.shape
        n_looks_x = x1dim // width_data2_temp
        n_looks_y = y1dim // length_data2_temp
        y_to_restore = y1map[n_looks_y//2, n_looks_x//2]
        y1map = 0
        x_to_restore = x1map[n_looks_y//2, n_looks_x//2]
        x1map = 0
        x1_2 = x1_2 // n_looks_x
        y1_2 = y1_2 // n_looks_y
        indexes = np.where(x1_2 > (width_data2_temp-1))
        x1_2[indexes] = width_data2_temp-1
        indexes = np.where(y1_2 > (length_data2_temp-1))
        y1_2[indexes] = length_data2_temp-1
        data1 = np.zeros((y2dim, x2dim))
        data1 = data2_temp[y1_2, x1_2]
        data1[y_to_restore, x_to_restore] = data2_temp_0_0
        print('..done')
        return data1
    return y1_2, x1_2

