#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import time
import numpy as np
import requests
from os import path, remove
import gdal
from osgeo import osr
import plant


class PlantGrid():
    def __init__(self, *args, **kwargs):
        self._lat_arr = None
        self._lon_arr = None
        self._step_lat = None
        self._step_lon = None
        self._lat_size = None
        self._lon_size = None
        self._projection = None
        ret_dict = get_coordinates(*args, **kwargs)
        if not ret_dict:
            return
        for key, value in ret_dict.items():
            self.__dict__[f'_{key}'] = value

    # geotransform
    @property
    def geotransform(self):
        geotransform = get_geotransform(
            lat_arr=self._lat_arr,
            lon_arr=self._lon_arr,
            step_lat=self._step_lat,
            step_lon=self._step_lon,
            lat_size=self._lat_size,
            lon_size=self._lon_size)
        return geotransform

    # bbox
    @property
    def bbox(self):
        bbox = get_bbox(self._lat_arr, self._lon_arr)
        return bbox

    # edges
    @property
    def lat_arr(self):
        return self._lat_arr

    @property
    def lon_arr(self):
        return self._lon_arr

    # size
    @property
    def lat_size(self):
        return self._lat_size

    @property
    def lon_size(self):
        return self._lon_size

    # step
    @property
    def step_lat(self):
        return self._step_lat

    @property
    def step_lon(self):
        return self._step_lon

    # projection
    @property
    def projection(self):
        return self._projection

    @projection.setter
    def projection(self, new_projection):
        if (not self.projection or
                not new_projection or
                compare_projections(self.projection,
                                    new_projection)):
            self._projection = new_projection
            return
        new_geotransform = plant.update_geotransform_projection(
            self.projection,
            geotransform=self.geotransform,
            output_projection=new_projection)[0]
        ret_dict = plant.get_coordinates(
            geotransform=new_geotransform,
            lat_size=self._lat_size,
            lon_size=self._lon_size,
            projection=new_projection)
        for key, value in ret_dict.items():
            self.__dict__[f'_{key}'] = value

    def print_geoinformation(self):
        print_geoinformation(lat_arr=self.lat_arr,
                             lon_arr=self.lon_arr,
                             projection=self.projection,
                             step_lat=self.step_lat,
                             step_lon=self.step_lon,
                             lat_size=self.lat_size,
                             lon_size=self.lon_size)


def get_coordinate_string(lat=None, lon=None):
    '''
    generate lat/lon string
    '''
    if lat is not None:
        str_lat = 'S' if lat < 0 else 'N'
        str_lat += "%02d" % abs(lat,)
        if lon is None:
            return str_lat
    if lon is not None:
        str_lon = 'W' if lon < 0 else 'E'
        str_lon += "%03d" % abs(lon,)
        if lat is None:
            return str_lon
        else:
            return str_lat, str_lon
    return -1


def get_coordinates_string(bbox=None,
                           lat_arr=None,
                           lon_arr=None,
                           lat=None,
                           lon=None):
    '''
    generate coordinates string
    '''
    # one value for lat or lon:
    if lat is not None and lon is not None:
        str_lat, str_lon = get_coordinate_string(lat, lon)
        return str_lat+str_lon
    elif lat is not None:
        return get_coordinate_string(lat)
    elif lon is not None:
        return get_coordinate_string(lon)
    # two values (min/max) for lat or lon:
    if lat_arr is None:
        lat_arr = np.asarray(bbox[0:2], dtype=np.float)
    if lon_arr is None:
        lon_arr = np.asarray(bbox[2:4], dtype=np.float)
    if lon_arr:
        lon_arr_temp = np.copy(lon_arr)
    lon_arr_temp[0] -= 360 if lon_arr_temp[0] > 180 else 0
    lon_arr_temp[1] -= 360 if lon_arr_temp[1] > 180 else 0
    if lon_arr_temp[0] == lon_arr_temp[1]:  # wrapped at the same point
        lon_arr_temp = [-180.0, 180.0]
    str_lat_min, str_lon_min = get_coordinate_string(lat_arr[0],
                                                     lon_arr_temp[0])
    str_lat_max, str_lon_max = get_coordinate_string(lat_arr[1],
                                                     lon_arr_temp[1])

    return 'Lat_%s_%s_Lon_%s_%s' % (str_lat_min, str_lat_max,
                                    str_lon_min, str_lon_max)


def compare_geotransforms(geotransform_1, geotransform_2, error=0.001):
    ret_dict = get_coordinates(geotransform=geotransform_1)
    # lat_arr_1, lon_arr_1, step_lat_1,
    #  step_lon_1, lat_size_1, lon_size_1 = ret
    lat_arr_1 = ret_dict['lat_arr']
    lon_arr_1 = ret_dict['lon_arr']
    step_lat_1 = ret_dict['step_lat']
    step_lon_1 = ret_dict['step_lon']
    lat_size_1 = ret_dict['lat_size']
    lon_size_1 = ret_dict['lon_size']
    ret_dict = get_coordinates(geotransform=geotransform_2)
    lat_arr_2 = ret_dict['lat_arr']
    lon_arr_2 = ret_dict['lon_arr']
    step_lat_2 = ret_dict['step_lat']
    step_lon_2 = ret_dict['step_lon']
    lat_size_2 = ret_dict['lat_size']
    lon_size_2 = ret_dict['lon_size']

    for i in range(2):
        if abs(lat_arr_1[i]-lat_arr_2[i]) > error:
            return False
    for i in range(2):
        if abs(lon_arr_1[i]-lon_arr_2[i]) > error:
            return False
    if abs(step_lat_1-step_lat_2) > error:
        return False
    if abs(step_lon_1-step_lon_2) > error:
        return False
    if lat_size_1 != lat_size_2:
        return False
    if lon_size_1 != lon_size_2:
        return False
    return True


def merge_geotransforms(geotransform_1, geotransform_2):
    if geotransform_2 is None:
        return geotransform_1
    if geotransform_1 is None:
        return geotransform_2

    geotransform_new_length = max([len(geotransform_2), len(geotransform_1)])
    geotransform_new = [np.nan for i in range(geotransform_new_length)]
    for i in range(geotransform_new_length):
        if i < len(geotransform_1):
            geotransform_new[i] = geotransform_1[i]
        if plant.isnan(geotransform_new[i]):
            geotransform_new[i] = geotransform_2[i]
    # geotransform_new = list(geotransform_1)
    # for i in range(len(geotransform_new)):
    #     if plant.isnan(geotransform_new[i]):
    #         geotransform_new[i] = geotransform_2[i]

    if (len(geotransform_new) > 6 and
            plant.isvalid(geotransform_new[3]) and
            plant.isvalid(geotransform_new[5]) and
            plant.isvalid(geotransform_new[7])):
        lat_arr = [geotransform_new[7], geotransform_new[3]]
        lat_arr = [min(lat_arr), max(lat_arr)]
        geotransform_new[9] = round(float(lat_arr[1] -
                                          lat_arr[0]) /
                                    abs(geotransform_new[5])+1)

    if (len(geotransform_new) > 6 and
            plant.isvalid(geotransform_new[0]) and
            plant.isvalid(geotransform_new[1]) and
            plant.isvalid(geotransform_new[6])):
        geotransform_new[8] = round(float(geotransform_new[6] -
                                          geotransform_new[0]) /
                                    geotransform_new[1]+1)

    return geotransform_new


def geotransform_iscomplete(geotransform):
    if geotransform is None:
        return False
    return all([x is not None and np.isfinite(x) for x in geotransform[0:6]])


def get_point_topo_dir(input_data=None,
                       image_obj=None,
                       image=None,
                       width=None,
                       length=None,
                       geotransform=None,
                       projection=None,
                       lat=None,
                       lon=None,
                       topo_dir=None,
                       y_pos=None,
                       x_pos=None,
                       footprint_m=None,
                       verbose=False):
    if footprint_m is not None:
        raise NotImplementedError

    if lat is not None:
        lat = plant.read_image(lat).image
    if lon is not None:
        lon = plant.read_image(lon).image
    if x_pos is not None:
        x_pos = plant.read_image(x_pos).image
    if y_pos is not None:
        y_pos = plant.read_image(y_pos).image
    if topo_dir is None:
        kwargs = locals()
        return get_point(**kwargs)

    if image_obj is None:
        image_obj = plant.PlantImage(input_data,
                                     verbose=verbose)

    if lat is not None and lon is not None:
        ret_dict = get_lat_lon_pos_topo_dir(
            lat, lon, topo_dir, image_obj, verbose=verbose)
        if ret_dict is None:
            ret_dict = {}
            return ret_dict
    else:
        ret_dict = get_lat_lon_topo_dir(
            y_pos, x_pos, topo_dir, verbose=verbose)
        if ret_dict is None:
            ret_dict = {}
            return ret_dict
    x_pos_image = ret_dict['x_pos_image']
    y_pos_image = ret_dict['y_pos_image']
    if image_obj is not None:
        data_value = image_obj.image[y_pos_image, x_pos_image]
        ret_dict['data'] = data_value
    if verbose:
        print_get_point_results(ret_dict)
    return ret_dict


def print_get_point_results(ret_dict):
    x_pos = ret_dict.get('x_pos', None)
    y_pos = ret_dict.get('y_pos', None)
    lon = ret_dict.get('lon', None)
    lat = ret_dict.get('lat', None)
    x_pos_image = ret_dict.get('x_pos_image', None)
    y_pos_image = ret_dict.get('y_pos_image', None)
    lon_image = ret_dict.get('lon_image', None)
    lat_image = ret_dict.get('lat_image', None)
    data_value = ret_dict.get('data', None)
    flag_outside_image = ret_dict.get('flag_outside_image', None)

    flag_outside_image_array = np.asarray(flag_outside_image).ravel()
    lat_array = np.asarray(lat).ravel()
    lon_array = np.asarray(lon).ravel()
    x_pos_array = np.asarray(x_pos).ravel()
    y_pos_array = np.asarray(y_pos).ravel()
    lat_image_array = np.asarray(lat_image).ravel()
    lon_image_array = np.asarray(lon_image).ravel()
    x_pos_image_array = np.asarray(x_pos_image).ravel()
    y_pos_image_array = np.asarray(y_pos_image).ravel()
    data_value_array = np.asarray(data_value).ravel()
    for i in range(lat_image.size):
        if flag_outside_image_array[i]:
            msg_list = []
            if i < len(x_pos_array) and i < len(y_pos_array):
                msg_list.extend(['x=%.0f' % x_pos_array[i],
                                 'y=%.0f' % y_pos_array[i]])
            if i < len(lat_array) and i < len(lon_array):
                msg_list.extend(['lat=%.16f' % lat_array[i],
                                 'lon=%.16f' % lon_array[i]])
            if len(msg_list) > 0:
                print('WARNING coordinates (%s)'
                      ' outside image boundaries'
                      % ', '.join(msg_list))
        msg_list = []
        if i < len(x_pos_image_array) and i < len(y_pos_image_array):
            msg_list.extend(['x=%.0f' % x_pos_image_array[i],
                             'y=%.0f' % y_pos_image_array[i]])
        if (i < len(lat_array) and i < len(lon_array) and
                plant.isvalid(lat_array[i]) and
                plant.isvalid(lon_array[i]) and
                not flag_outside_image_array[i]):
            msg_list.extend(['lat=%.16f' % lat_array[i],
                             'lon=%.16f' % lon_array[i]])
        elif (i < len(lat_image_array) and i < len(lon_image_array) and
                flag_outside_image_array[i]):
            if plant.isvalid(lat_image_array[i]):
                msg_list.extend(['lat=%.16f' % lat_image_array[i]])
            if plant.isvalid(lon_image_array[i]):
                msg_list.extend(['lon=%.16f' % lon_image_array[i]])
        if i < len(data_value_array):
            print('value at (%s): %s'
                  % (', '.join(msg_list), str(data_value_array[i])))
        else:
            print('position: %s'
                  % (', '.join(msg_list)))


def get_lat_lon_topo_dir(y_pos, x_pos, topo_dir=None, image_obj=None,
                         verbose=False):
    ret = get_lat_lon_files_from_topo_dir(topo_dir)
    if ret is None:
        return
    file_lat, file_lon = ret
    lat_image = plant.read_matrix(file_lat)
    lon_image = plant.read_matrix(file_lon)
    flag_outside_image_list = []
    x_pos_image_list = []
    y_pos_image_list = []
    out_lat_list = []
    out_lon_list = []
    y_pos_vect = np.asarray(y_pos).ravel()
    x_pos_vect = np.asarray(x_pos).ravel()
    for y, x in zip(y_pos_vect, x_pos_vect):
        current_y = int(y)
        current_x = int(x)
        flag_outside_image = (current_y < 0 or
                              current_x < 0 or
                              (image_obj is not None and
                               (current_y > image_obj.image.shape[0] or
                                (current_x > image_obj.image.shape[1]))))
        flag_outside_image_list.append(flag_outside_image)
        x_pos_image_list.append(current_x)
        y_pos_image_list.append(current_y)
        if not flag_outside_image:
            out_lat_list.append(lat_image[current_y, current_x])
            out_lon_list.append(lon_image[current_y, current_x])
        else:
            out_lat_list.append(np.nan)
            out_lon_list.append(np.nan)

    ret_dict = _get_point_prepare_output(
        y_pos_image=y_pos_image_list, x_pos_image=x_pos_image_list,
        lat_image=out_lat_list, lon_image=out_lon_list,
        flag_outside_image=flag_outside_image_list,
        y_pos=y_pos_vect, x_pos=x_pos_vect,
        ref_array=y_pos)
    return ret_dict


def get_lat_lon_pos_topo_dir(lat, lon,
                             topo_dir=None,
                             image_obj=None,
                             verbose=False):
    ret = get_lat_lon_files_from_topo_dir(topo_dir)
    if ret is None:
        return
    file_lat, file_lon = ret
    MAX_DIST = 0.00083  # ~90m
    lat_image = plant.read_matrix(file_lat)
    lon_image = plant.read_matrix(file_lon)

    lat_vect = np.asarray(lat).ravel()
    lon_vect = np.asarray(lon).ravel()
    x_pos_image_list = []
    y_pos_image_list = []
    flag_outside_image_list = []
    out_lat_list = []
    out_lon_list = []
    for current_lat, current_lon in zip(lat_vect, lon_vect):
        dist_image = ((lat_image - current_lat)**2 +
                      (lon_image - current_lon)**2)
        index_ravel = np.argmin(dist_image)
        index = np.unravel_index(index_ravel,
                                 dist_image.shape)
        y_pos, x_pos = index
        out_lat_list.append(lat_image[y_pos, x_pos])
        out_lon_list.append(lon_image[y_pos, x_pos])
        flag_outside_image = dist_image[y_pos, x_pos] > MAX_DIST
        if flag_outside_image and verbose:
            lat_orig = current_lat
            lon_orig = current_lon
            current_lat = lat_image[y_pos, x_pos]
            current_lon = lon_image[y_pos, x_pos]
            print('Requested point: (lat=%f, lon=%f)'
                  % (lat_orig, lon_orig))
            print('Nearest point: (lat=%f, lon=%f)'
                  % (current_lat, current_lon))
            print('Distance to the nearest point [deg]: %f'
                  % (dist_image[y_pos, x_pos]))
        x_pos_image_list.append(x_pos)
        y_pos_image_list.append(y_pos)
        flag_outside_image_list.append(flag_outside_image)
    ret_dict = _get_point_prepare_output(
        lat=lat_vect, lon=lon_vect,
        y_pos_image=y_pos_image_list, x_pos_image=x_pos_image_list,
        lat_image=out_lat_list, lon_image=out_lon_list,
        flag_outside_image=flag_outside_image_list, ref=lat)
    return ret_dict


def _get_point_prepare_output(**kwargs):
    ref = kwargs.pop('ref', None)
    ret_dict = {}
    if ref is not None and np.asarray(ref).size == 1:
        for key, value in kwargs.items():
            ret_dict[key] = value[0]
        return ret_dict
    for key, value in kwargs.items():
        if ref is not None and isinstance(ref, np.ndarray):
            ret_dict[key] = np.asarray(value).reshape(ref.shape)
        else:
            ret_dict[key] = np.asarray(value)
    return ret_dict


def get_point(input_data=None,
              image_obj=None,
              image=None,
              width=None,
              length=None,
              geotransform=None,
              lat=None,
              lon=None,
              topo_dir=None,
              projection=None,
              y_pos=None,
              x_pos=None,
              footprint_m=None,
              verbose=False):
    if lat is not None:
        lat = plant.read_image(lat).image
    if lon is not None:
        lon = plant.read_image(lon).image
    if x_pos is not None:
        x_pos = plant.read_image(x_pos).image
    if y_pos is not None:
        y_pos = plant.read_image(y_pos).image
    if topo_dir is not None:
        kwargs = locals()
        return get_point_topo_dir(**kwargs)

    if image_obj is None:
        image_obj = plant.PlantImage(input_data,
                                     verbose=verbose,
                                     only_header=False)

    if geotransform is None:
        geotransform = image_obj.geotransform
    if not projection:
        projection = image_obj.projection

    if width is None:
        width = image_obj.width
    if length is None:
        length = image_obj.length

    if geotransform is not None:
        ret_dict = get_coordinates(geotransform=geotransform,
                                   projection=projection,
                                   flag_output_in_wgs84=True)
        if ret_dict is None:
            print('WARNING (plant_geo.get_point) input does not '
                  'have a valid geo-reference')
            return
        lat_arr = ret_dict['lat_arr']
        lon_arr = ret_dict['lon_arr']
        step_lat = ret_dict['step_lat']
        step_lon = ret_dict['step_lon']
        # lat_size = ret_dict['lat_size']
        # lon_size = ret_dict['lon_size']
        lat_beg = lat_arr[0]
        lon_beg = lon_arr[0]
        step_lat = abs(step_lat)
    elif y_pos is None or x_pos is None:
        print(f'ERROR georeference of "{image_obj}" not found')
        return
    else:
        lat_beg = np.nan
        lon_beg = np.nan
        step_lat = np.nan
        step_lon = np.nan

    if footprint_m is not None and geotransform is not None:
        footprint_deg = plant.m_to_deg(footprint_m)
        filter_size = [footprint_deg[0]/step_lat,
                       footprint_deg[1]/step_lon]
        # print('*** footprint_m: ', footprint_m)
        # print('*** footprint_deg: ', footprint_deg)
        # print('*** filter_size: ', filter_size)
        if (((float(filter_size[0])-float(filter_size[1])) /
             float(filter_size[0])) > 0.1):
            # if filter dimensions are similar
            image_obj = plant.filter_data(image_obj,
                                          mean=filter_size)
        else:
            image_obj = plant.filter_data(image_obj,
                                          trapezoid=filter_size,
                                          trapezoid_slope=1)
    elif footprint_m is not None:
        raise NotImplementedError

    lat_array = np.asarray(lat).ravel()
    lon_array = np.asarray(lon).ravel()
    x_pos_array = np.asarray(x_pos).ravel()
    y_pos_array = np.asarray(y_pos).ravel()

    x_pos_list = []
    y_pos_list = []
    lat_list = []
    lon_list = []
    x_pos_image_list = []
    y_pos_image_list = []
    lon_image_list = []
    lat_image_list = []
    flag_outside_image_list = []
    data_list = []
    for i in range(max([lat_array.size, y_pos_array.size])):
        current_lat = lat_array[i] if lat is not None else None
        current_lon = lon_array[i] if lon is not None else None
        current_y_pos = y_pos_array[i] if y_pos is not None else None
        current_x_pos = x_pos_array[i] if x_pos is not None else None

        ret_dict = _get_single_point(current_lat, current_lon,
                                     current_y_pos, current_x_pos,
                                     length, width, lat_beg, lon_beg,
                                     step_lat, step_lon, image_obj)
        x_pos_list.append(ret_dict.get('x_pos', None))
        y_pos_list.append(ret_dict.get('y_pos', None))
        lat_list.append(ret_dict.get('lat', None))
        lon_list.append(ret_dict.get('lon', None))
        x_pos_image_list.append(ret_dict.get('x_pos_image', None))
        y_pos_image_list.append(ret_dict.get('y_pos_image', None))
        lon_image_list.append(ret_dict.get('lon_image', None))
        lat_image_list.append(ret_dict.get('lat_image', None))
        flag_outside_image_list.append(ret_dict.get('flag_outside_image',
                                                    None))
        data_list.append(ret_dict.get('data', None))
    ret_dict = _get_point_prepare_output(
        y_pos=y_pos_list, x_pos=x_pos_list, lat=lat_list, lon=lon_list,
        lat_image=lat_image_list, lon_image=lon_image_list,
        y_pos_image=y_pos_image_list, x_pos_image=x_pos_image_list,
        flag_outside_image=flag_outside_image_list, data=data_list,
        ref_array=x_pos)
    if verbose:
        print_get_point_results(ret_dict)
    return ret_dict


def _get_single_point(lat, lon, y_pos, x_pos, length, width,
                      lat_beg, lon_beg, step_lat, step_lon,
                      image_obj):
    # print('*** locals: ', locals)
    if plant.isvalid(lat) and plant.isnan(y_pos):
        # print('*** lat: ', lat)
        # print('*** step_lat: ', step_lat)
        # print('*** lat_beg: ', lat_beg)
        y_pos = int(round((lat-lat_beg)/step_lat))
        y_pos = int(length-1-y_pos)
    if plant.isnan(lat) and plant.isvalid(y_pos):
        lat = (length-1-y_pos)*step_lat+lat_beg

    if plant.isvalid(lon) and plant.isnan(x_pos):
        x_pos = int(round((lon-lon_beg)/step_lon))
    if plant.isnan(lon) and plant.isvalid(x_pos):
        lon = x_pos*step_lon+lon_beg

    flag_valid_y = not (plant.isnan(y_pos) or
                        y_pos < 0 or
                        y_pos > length-1)
    flag_valid_x = not (plant.isnan(x_pos) or
                        x_pos < 0 or
                        x_pos > width-1)

    # force NaN (it could be None, inf, ..)
    x_pos = np.nan if plant.isnan(x_pos) else x_pos
    y_pos = np.nan if plant.isnan(y_pos) else y_pos
    flag_outside_image = not flag_valid_x or not flag_valid_y

    if plant.isvalid(y_pos) and not flag_valid_y:
        y_pos_image = int(min(y_pos, length-1))
        y_pos_image = int(max(y_pos_image, 0))
    else:
        y_pos_image = y_pos
    lat_image = None
    lon_image = None
    if (flag_valid_y and plant.isvalid(length) and
            plant.isvalid(y_pos_image) and
            plant.isvalid(step_lat) and
            plant.isvalid(lat_beg)):
        lat_image = (length-1-y_pos_image)*step_lat+lat_beg
    else:
        lat_image = y_pos*np.nan
    if plant.isvalid(x_pos) and not flag_valid_x:
        x_pos_image = int(min(x_pos, width-1))
        x_pos_image = int(max(x_pos_image, 0))
    else:
        x_pos_image = x_pos

    if (flag_valid_x and
            plant.isvalid(x_pos_image) and
            plant.isvalid(step_lon) and
            plant.isvalid(lon_beg)):
        lon_image = x_pos_image*step_lon+lon_beg
    else:
        lat_image = x_pos*np.nan

    '''
    flag_valid_y_2 = not (plant.isnan(y_pos_image) or
                          y_pos_image < 0 or
                          y_pos_image > length-1)
    flag_valid_x_2 = not (plant.isnan(x_pos_image) or
                          x_pos_image < 0 or
                          x_pos_image > width-1)

    # force NaN (it could be None, inf, ..)
    # x_pos = np.nan if plant.isnan(x_pos) else x_pos
    # y_pos = np.nan if plant.isnan(y_pos) else y_pos
    flag_outside_image_2 = not flag_valid_x_2 or not flag_valid_y_2

    flag_outside_image &= flag_outside_image_2
    '''
    ret_dict = {}
    ret_dict['x_pos'] = x_pos
    ret_dict['y_pos'] = y_pos
    ret_dict['lat'] = lat
    ret_dict['lon'] = lon
    ret_dict['x_pos_image'] = x_pos_image
    ret_dict['y_pos_image'] = y_pos_image
    ret_dict['lon_image'] = lon_image
    ret_dict['lat_image'] = lat_image
    ret_dict['flag_outside_image'] = flag_outside_image
    # print('*** ret_dict: ', ret_dict)
    # print('*** image.shape: ', image_obj.shape)
    if (plant.isnan(y_pos_image) or plant.isnan(x_pos_image) or
            image_obj is None):
        # if (flag_outside_image or
        #         image_obj is None):
        return ret_dict

    data_value = []
    plant_transform_obj = plant.PlantTransform(select_row = int(y_pos_image),
                                               select_col = int(x_pos_image))
    image_crop_obj = plant.PlantImage(image_obj,
                                      plant_transform_obj = plant_transform_obj)
    for b in range(image_obj.nbands):
        image = image_crop_obj.get_image(band=b)
        if image is None:
            print('ERROR reading %s band %d'
                  % (image_obj.filename, b))
        data_value.append(image[0, 0])
        # data_value.append(image[int(y_pos_image), int(x_pos_image)])
    ret_dict['data'] = data_value
    # print_get_point_results(ret_dict)
    return ret_dict


def update_geotransform_with_coord_transformation(
        geotransform,
        coord_transformation=None):
    if coord_transformation is None or geotransform is None:
        return geotransform

    # coord_transformation.SetAxisMappingStrategy(
    #     osr.OAMS_TRADITIONAL_GIS_ORDER)
    
    lon = geotransform[0]
    lat = geotransform[3]
    # if plant.isvalid(lat) and plant.isvalid(lon):
    if plant.isnan(lat) or plant.isnan(lon):
        return
    # print('*** lon, lat: ', lon, lat)
    new_lon, new_lat, _ = coord_transformation.TransformPoint(
        float(lon), float(lat))
    # print('*** new_lon, new_lat: ', new_lon, new_lat)
    geotransform[0] = new_lon
    geotransform[3] = new_lat

    step_lon = geotransform[1]
    step_lat = geotransform[5]
    if plant.isnan(step_lat) or plant.isnan(step_lon):
        return

    # if plant.isvalid(step_lat) and plant.isvalid(step_lon):
    # print('*** step_lon, step_lat: ', step_lon, step_lat)
    _, new_lat_temp, _ = \
        coord_transformation.TransformPoint(float(lon),
                                            float(lat+step_lat))
    new_step_lat = new_lat_temp - new_lat
    new_lon_temp, _, _ = \
        coord_transformation.TransformPoint(float(lon+step_lon),
                                            float(lat))
    new_step_lon = new_lon_temp - new_lon
    # print('*** new_step_lon, new_step_lat: ',
    #       new_step_lon, new_step_lat)




    # print('*** (2) new_step_lon, new_step_lat: ',
    #       new_step_lon, new_step_lat)

    geotransform[1] = new_step_lon
    geotransform[5] = new_step_lat

    if len(geotransform) > 6:
        lon = geotransform[6]
        lat = geotransform[7]
        new_lon, new_lat, _ = coord_transformation.TransformPoint(lon, lat, 0)
        # print('*** new_lon, new_lat (2): ', new_lon, new_lat)

        geotransform[6] = new_lon
        geotransform[7] = new_lat
    # return geotransform


def utm_to_wgs(north, east, zone):
    wgs84_coordinate_system = osr.SpatialReference()
    wgs84_coordinate_system.SetWellKnownGeogCS("WGS84")
    try:
        wgs84_coordinate_system.SetAxisMappingStrategy(
            osr.OAMS_TRADITIONAL_GIS_ORDER)
    except AttributeError:
        pass

    utm_coordinate_system = osr.SpatialReference()
    utm_coordinate_system.SetWellKnownGeogCS("WGS84")
    utm_coordinate_system.SetUTM(zone, True)
    try:
        utm_coordinate_system.SetAxisMappingStrategy(
            osr.OAMS_TRADITIONAL_GIS_ORDER)
    except AttributeError:
        pass

    transformation = osr.CoordinateTransformation(utm_coordinate_system,
                                                  wgs84_coordinate_system)
    lon, lat, _ = transformation.TransformPoint(float(east), float(north), 0)
    return (lat, lon)


def get_lat_lon_arr_from_gcp_list(gcp_list,
                                  lat_arr=None,
                                  lon_arr=None):
    if not gcp_list:
        return
    if lat_arr is None:
        lat_arr = [np.nan, np.nan]
    if lon_arr is None:
        lon_arr = [np.nan, np.nan]
    for gcp in gcp_list:
        # gcp_projection = None
        lon = gcp.GCPX
        lat = gcp.GCPY
        if plant.isnan(lat_arr[0]) or lat_arr[0] > lat:
            lat_arr[0] = lat
        if plant.isnan(lat_arr[1]) or lat_arr[1] < lat:
            lat_arr[1] = lat
        if plant.isnan(lon_arr[0]) or lon_arr[0] > lon:
            lon_arr[0] = lon
        if plant.isnan(lon_arr[1]) or lon_arr[1] < lon:
            lon_arr[1] = lon
    # GCPZ = gcp.GCPZ
    # GCPPixel = gcp.GCPPixel - xoff
    # GCPLine = gcp.GCPLine - yoff
    # Info = gcp.Info
    # Id = gcp.Id
    return lat_arr, lon_arr


def get_coordinates(geo_center=None,
                    geo_search_str=None,
                    bbox=None,

                    gcp_list=None,
                    gcp_projection=None,

                    step=None,
                    step_lat=None,
                    step_lon=None,
                    step_m=None,
                    step_m_lat=None,
                    step_m_lon=None,
                    bbox_topo=None,
                    bbox_file=None,
                    verbose=None,

                    # prefix=None,
                    projection=None,
                    default_step=None,
                    geotransform=None,
                    lon_size=None,
                    lat_size=None,
                    lat_arr=None,
                    lon_arr=None,
                    image=None,
                    plant_transform_obj=None,
                    flag_output_in_wgs84=False):

    kwargs = locals()

    output_geotransform = None if geotransform is None else geotransform[:]

    # Coordinates from PlantImage
    if image is not None:
        geotransform_temp = image.geotransform
        if lat_size is None:
            lat_size = image.length
        if lon_size is None:
            lon_size = image.width
        output_geotransform = merge_geotransforms(output_geotransform,
                                                  geotransform_temp)






    if gcp_list:
        ret = get_lat_lon_arr_from_gcp_list(gcp_list)
        lat_arr, lon_arr = ret
        geotransform_temp = get_geotransform(lat_arr=lat_arr,
                                             lon_arr=lon_arr)
        geotransform_temp, projection = update_geotransform_projection(
            gcp_projection,
            geotransform_temp,
            output_projection=projection)
        output_geotransform = merge_geotransforms(output_geotransform,
                                                  geotransform_temp)

    # current_projection = projection

    # coordinates from GEO header
    if (not geotransform_iscomplete(output_geotransform) and
            bbox_file is not None):
        geotransform_temp = None
        geo_list = [bbox_file] if isinstance(bbox_file, str) else bbox_file

        '''
        if isinstance(geo, str):
            if not plant.isfile(geo):
                print('ERROR file not found: '+geo)
                sys.exit(1)
            image_obj = plant.read_image(
                geo, only_header=True,
                plant_transform_obj=plant_transform_obj)

            # if this_projection is not None:
            #    current_projection = this_projection

            if image_obj is not None:
                geotransform_temp = image_obj.geotransform

            if geotransform_temp is not None:
                this_projection = image_obj.projection
                geotransform_temp, projection = update_geotransform_projection(
                    this_projection,
                    geotransform_temp,
                    output_projection=projection)
            # geotransform_temp = get_geotransform_from_header(geo)
            if verbose and geotransform_temp is not None:
                print('coordinates from reference GEO: %s'
                      % geo)
            if verbose and geotransform_temp is None:
                print('WARNING geotransform could not be read from ' +
                      geo)
        # list of files
        else:
        '''
        lat_arr_temp = []
        lon_arr_temp = []
        step_lat_temp = []
        step_lon_temp = []
        # lat_size_temp = []
        # lon_size_temp = []
        flag_print = True

        for current_geo in geo_list:
            if not plant.isfile(current_geo):
                continue
            # if not current_geo.endswith('.xml'):
            #    current_geo = current_geo+'.xml'
            image_obj = plant.read_image(
                current_geo,
                only_header=True,
                plant_transform_obj=plant_transform_obj)
            if image_obj is None:
                continue
            geotransform_temp = image_obj.geotransform
            projection_temp = image_obj.projection
            gcp_list_temp = image_obj.gcp_list
            gcp_projection_temp = image_obj.gcp_projection

            if geotransform_temp is None and gcp_list_temp:
                ret = get_lat_lon_arr_from_gcp_list(gcp_list_temp)
                lat_arr, lon_arr = ret
                geotransform_temp = get_geotransform(lat_arr=lat_arr,
                                                     lon_arr=lon_arr)
                projection_temp = gcp_projection_temp
            elif geotransform_temp is None:
                continue
            geotransform_temp, projection = update_geotransform_projection(
                projection_temp,
                geotransform_temp,
                output_projection=projection)
            if verbose and flag_print:
                print('coordinates from reference GEO')
                flag_print = False
            ret_dict = get_coordinates(geotransform=geotransform_temp,
                                       lat_size=image_obj.length,
                                       lon_size=image_obj.width)
            if ret_dict is None:
                continue

            lat_arr_temp.append(ret_dict['lat_arr'])
            lon_arr_temp.append(ret_dict['lon_arr'])
            if plant.isvalid(ret_dict['step_lat']):
                step_lat_temp.append(abs(ret_dict['step_lat']))
            if plant.isvalid(ret_dict['step_lon']):
                step_lon_temp.append(ret_dict['step_lon'])
            # lat_size_temp.append(image_obj.height)
            # lon_size_temp.append(image_obj.width)

            '''
            if len(geotransform_temp) > 6:
                lon_arr_temp += [geotransform_temp[0],
                                 geotransform_temp[6]]
                lat_arr_temp_2 = [geotransform_temp[3],
                                  geotransform_temp[7]]
                lon_size = geotransform_temp[8]
                lat_size = geotransform_temp[9]
            else:
                lon_arr_temp += [geotransform_temp[0],
                                 geotransform_temp[0] +
                                 (lon_size-1) *
                                 geotransform_temp[1]]
                lat_arr_temp_2 = [geotransform_temp[3],
                                  geotransform_temp[3] +
                                  (lat_size-1) *
                                  geotransform_temp[5]]
            step_lon_temp += [geotransform_temp[1]]
            if geotransform_temp[5] < 0:
                lat_arr_temp_2 = [lat_arr_temp_2[1],
                                  lat_arr_temp_2[0]]
            lat_arr_temp += lat_arr_temp_2
            if plant.isvalid(geotransform_temp[5]):
                step_lat_temp += [abs(geotransform_temp[5])]
            '''

        lat_arr_temp = np.asarray(lat_arr_temp).ravel()
        lon_arr_temp = np.asarray(lon_arr_temp).ravel()
        lat_arr = None
        lon_arr = None
        step_lat = None
        step_lon = None
        if len(lat_arr_temp) != 0:
            lat_arr = [min(lat_arr_temp), max(lat_arr_temp)]
        if len(lon_arr_temp) != 0:
            lon_arr = [min(lon_arr_temp), max(lon_arr_temp)]
        if len(step_lat_temp) != 0:
            step_lat = min(step_lat_temp)
        if len(step_lon_temp) != 0:
            step_lon = min(step_lon_temp)
        if lat_arr is not None and lon_arr is not None:
            geotransform_temp = get_geotransform(lat_arr=lat_arr,
                                                 lon_arr=lon_arr,
                                                 step_lat=step_lat,
                                                 step_lon=step_lon)
        output_geotransform = merge_geotransforms(output_geotransform,
                                                  geotransform_temp)

    # Coordinates from georeference data
    if (not geotransform_iscomplete(output_geotransform) and
            bbox_topo is not None):
        geotransform_temp = None
        if isinstance(bbox_topo, str):
            ret = get_lat_lon_boundaries_from_topo_dir(
                bbox_topo,
                plant_transform_obj=plant_transform_obj)
            # print('*** ret: ', ret)
            if verbose and ret is not None:
                print('coordinates from georeference files')
            if ret is not None:
                lat_arr, lon_arr, step_lat, step_lon = ret
                geotransform_temp = get_geotransform(lat_arr=lat_arr,
                                                     lon_arr=lon_arr,
                                                     step_lat=step_lat,
                                                     step_lon=step_lon)
        # list of files
        else:
            lat_arr_temp = []
            lon_arr_temp = []
            step_lat_temp = []
            step_lon_temp = []
            flag_print = True
            for current_dir in bbox_topo:
                ret = get_lat_lon_boundaries_from_topo_dir(
                    current_dir,
                    plant_transform_obj=plant_transform_obj)
                if ret is not None:
                    if verbose and flag_print:
                        print('coordinates from georeference files')
                        flag_print = False
                    curr_lat_arr, curr_lon_arr, curr_step_lat, \
                        curr_step_lon = ret
                    lat_arr_temp += curr_lat_arr
                    lon_arr_temp += curr_lon_arr
                    step_lon_temp += [abs(curr_step_lon)]
                    step_lat_temp += [abs(curr_step_lat)]
            if (len(lat_arr_temp) != 0 and len(lon_arr_temp) != 0):
                lat_arr = [min(lat_arr_temp), max(lat_arr_temp)]
                lon_arr = [min(lon_arr_temp), max(lon_arr_temp)]
                step_lat = min(step_lat_temp)
                step_lon = min(step_lon_temp)
            else:
                lat_arr = None
                lon_arr = None
        if lat_arr is not None and lon_arr is not None:
            geotransform_temp = get_geotransform(lat_arr=lat_arr,
                                                 lon_arr=lon_arr,
                                                 step_lat=step_lat,
                                                 step_lon=step_lon)
            output_geotransform = merge_geotransforms(output_geotransform,
                                                      geotransform_temp)

     #print('*** output_geotransform: ', output_geotransform)

    if flag_output_in_wgs84:
        output_geotransform, projection = \
            update_geotransform_projection(projection,
                                           output_geotransform)

    # print('*** output_geotransform (2): ', output_geotransform)

    if kwargs['projection'] is None:
       kwargs['projection'] = projection

    geotransform_precedence = get_geotransform_all(**kwargs)
    output_geotransform = merge_geotransforms(geotransform_precedence,
                                              output_geotransform)


    # coordinates from output_geotransform
    if output_geotransform is not None:
        if len(output_geotransform) > 6:
            lon_arr = [output_geotransform[0], output_geotransform[6]]
            lat_arr = [output_geotransform[3], output_geotransform[7]]
            # lon_size = output_geotransform[8]
            # lat_size = output_geotransform[9]
        else:
            if (lon_size is not None and
                    output_geotransform[0] is not None and
                    output_geotransform[1] is not None):
                lon_arr = [output_geotransform[0],
                           output_geotransform[0] +
                           (lon_size-1)*output_geotransform[1]]
            else:
                lon_arr = [output_geotransform[0], np.nan]
            if (lat_size is not None and
                    output_geotransform[3] is not None and
                    output_geotransform[5] is not None):
                lat_arr = [output_geotransform[3],
                           output_geotransform[3] +
                           (lat_size-1)*output_geotransform[5]]
            else:
                lat_arr = [output_geotransform[3], np.nan]








        step_lon = output_geotransform[1]
        if plant.isvalid(output_geotransform[5]):
            if output_geotransform[5] < 0:
                lat_arr = [lat_arr[1], lat_arr[0]]
            step_lat = abs(output_geotransform[5])


    # lat/lon step from default_step
    if (step_lon is None or not np.isfinite(step_lon) and
            default_step is not None):
        step_lon = default_step
    if (step_lat is None or not np.isfinite(step_lat) and
            default_step is not None):
        step_lat = default_step

    if step_lat is not None:
        if step_lat == 0:
            step_lat = None

    if step_lon is not None:
        if step_lon == 0:
            step_lon = None

    if lat_arr is not None:
        if (plant.isvalid(lat_arr[0]) and plant.isvalid(lat_arr[1]) and
                lat_arr[0] != lat_arr[1]):
            lat_arr = [min(lat_arr), max(lat_arr)]
            if plant.isvalid(step_lat):
                step_lat = abs(step_lat)
                try:
                    lat_size_temp = round(float(lat_arr[1]-lat_arr[0]) /
                                          step_lat+1)
                except:
                    lat_size_temp = None
                if plant.isnan(lat_size) or lat_size_temp is not None:
                    lat_size = lat_size_temp

    if lon_arr is not None:
        if (plant.isvalid(lon_arr[0]) and plant.isvalid(lon_arr[1]) and
                lon_arr[0] != lon_arr[1]):
            lon_arr = [min(lon_arr), max(lon_arr)]
            if plant.isvalid(step_lon):
                step_lon = abs(step_lon)
                try:
                    lon_size_temp = round(float(lon_arr[1]-lon_arr[0]) /
                                          step_lon+1)
                except:
                    lon_size_temp = None
                if plant.isnan(lon_size) or lon_size_temp is not None:
                    lon_size = lon_size_temp

    # lat_arr_temp = np.clip(lat_arr, -90, 90)
    # if lat_arr_temp != lat_arr:
    #    print('WARNING lat_arr updated from lat
    #    lat_arr = lat_arr_temp
    # lon_arr_temp = np.clip(lon_arr, -180, 360)
    if verbose:
        print_geoinformation(lat_arr=lat_arr,
                             lon_arr=lon_arr,
                             projection=projection,
                             # prefix=prefix,
                             step_lat=step_lat,
                             step_lon=step_lon,
                             lat_size=lat_size,
                             lon_size=lon_size)
    if (lat_arr is None and lon_arr is None and step_lat is None
            and step_lon is None and lat_size is None and lon_size is None):
        return
    # return lat_arr, lon_arr, step_lat, step_lon, lat_size, lon_size
    ret_dict = {}
    ret_dict['lat_arr'] = lat_arr
    ret_dict['lon_arr'] = lon_arr
    ret_dict['step_lat'] = step_lat
    ret_dict['step_lon'] = step_lon
    ret_dict['lat_size'] = lat_size
    ret_dict['lon_size'] = lon_size
    ret_dict['projection'] = projection
    return ret_dict


def get_geotransform_all(geo_center=None,
                         geo_search_str=None,
                         bbox=None,

                         gcp_list=None,
                         gcp_projection=None,

                         step=None,
                         step_lat=None,
                         step_lon=None,
                         step_m=None,
                         step_m_lat=None,
                         step_m_lon=None,
                         # bbox_topo=None,
                         # bbox_file=None,
                         verbose=None,

                         # prefix=None,
                         projection=None,
                         default_step=None,
                         # geotransform=None,
                         lon_size=None,
                         lat_size=None,
                         lat_arr=None,
                         lon_arr=None,
                         **kwargs):
    # image=None,
    # plant_transform_obj=None,
    # flag_output_in_wgs84=False):

    # Coordinates from bbox or step were provided
    if geo_center is not None:
        if lat_arr is None and geo_center is not None:
            lat_arr = np.asarray([geo_center[0]-geo_center[2]/2,
                                  geo_center[0]+geo_center[2]/2],
                                 dtype=np.float)
        if lon_arr is None and geo_center is not None:
            lon_arr = np.asarray([geo_center[1]-geo_center[3]/2,
                                  geo_center[1]+geo_center[3]/2],
                                 dtype=np.float)

        # geotransform_temp, projection = \
        #    update_geotransform_projection(
        #        this_projection,
        #        geotransform_temp,
        #        output_projection=projection)

    if geo_search_str is not None:
        ret_dict = get_lat_lon_search_str(geo_search_str[0],
                                          verbose=True)
        print('coordinates from geo_search: ', ret_dict)
        # print('*** ret_dict: ', ret_dict)
        lat = ret_dict['lat']
        lon = ret_dict['lon']
        lat_diff = float(geo_search_str[1])/2
        lon_diff = float(geo_search_str[2])/2
        lat_arr = [lat-lat_diff, lat+lat_diff]
        lon_arr = [lon-lon_diff, lon+lon_diff]

    if bbox is not None and verbose:
        print('coordinates from bbox')
    if lat_arr is None and bbox is not None:
        lat_arr = np.asarray(bbox[0:2], dtype=np.float)
    if lon_arr is None and bbox is not None:
        lon_arr = np.asarray(bbox[2:4], dtype=np.float)


    flag_projected = projection is not None and is_projected(projection)
    # print('*** projection: ', projection, flag_projected)

    if step_m_lat is None and step_m is not None:
        step_m_lat = step_m

    if step_lat is None and step is not None:
        step_lat = step

    if step_lat is None and step_m_lat is not None and flag_projected:
        step_lat = step_m_lat

    elif step_lat is None and step_m_lat is not None:
        step_lat = m_to_deg_lat(step_m_lat)

    if step_m_lon is None and step_m is not None:
        step_m_lon = step_m

    if step_lon is None and step is not None:
        step_lon = step

    if step_lon is None and step_m_lon is not None and flag_projected:
        step_lon = step_m_lon

    elif step_lon is None and step_m_lon is not None:
        if lat_arr is not None:
            lat = np.nanmean(lat_arr)
        else:
            lat = None
        step_lon = m_to_deg_lon(step_m_lon, lat=lat)

    if step_lat is None and default_step is not None:
        step_lat = default_step
    if step_lon is None and default_step is not None:
        step_lon = default_step

    geotransform_temp = get_geotransform(lat_arr=lat_arr,
                                         lon_arr=lon_arr,
                                         step_lat=step_lat,
                                         step_lon=step_lon)

    return geotransform_temp



def get_lat_lon_search_str(search_str, verbose=True):
    session = requests.Session()
    PARAMS = {'action': "query",
              'prop': "coordinates",
              'titles': search_str,
              'format': "json"}
    request = session.get(url=plant.WIKI_API, params=PARAMS)
    data = request.json()
    pages = data['query']['pages']
    ret_dict = {}
    for key, value in pages.items():
        if 'coordinates' in value:
            ret_dict['lat'] = value['coordinates'][0]['lat']
            ret_dict['lon'] = value['coordinates'][0]['lon']
            break
    else:
        if verbose:
            print(f'ERROR location not found: {search_str}')
        return
    return ret_dict


def get_projection_wkt(projection):
    srs = plant.get_srs_from_projection(projection)
    if srs is None:
        return srs
    projection = str(srs).replace('\n', '')
    projection = projection.replace('    ', ' ')
    projection = projection.strip()
    return projection


def get_projection_proj4(projection):
    srs = plant.get_srs_from_projection(projection)
    if srs is None:
        return
    projection = srs.ExportToProj4()
    projection = projection.strip()
    return projection


def get_srs_from_projection(projection,
                            flag_oams_traditional_gis_order=True):
    if not projection:
        return
    srs = osr.SpatialReference()
    if projection.upper() == 'WGS84':
        srs.SetWellKnownGeogCS(projection)
    else:
        srs.ImportFromProj4(projection)
    if not flag_oams_traditional_gis_order:
        return srs
    try:
        srs.SetAxisMappingStrategy(
            osr.OAMS_TRADITIONAL_GIS_ORDER)
    except AttributeError:
        pass
    return srs


def compare_projections(projection_1, projection_2):
    if ((projection_1 is None and projection_2 is not None) or
            (projection_1 is not None and projection_2 is None)):
        return False
    srs_1 = get_srs_from_projection(projection_1)
    if srs_1 is None:
        return
    srs_2 = get_srs_from_projection(projection_2)
    if srs_2 is None:
        return

    try:
        geogcs_1 = srs_1.GetAttrValue('geogcs')
    except TypeError:
        return True

    if geogcs_1 == 'unknown':
        return True

    try:
        geogcs_2 = srs_2.GetAttrValue('geogcs')
    except TypeError:
        return True

    if geogcs_2 == 'unknown':
        return True

    return srs_1.IsSame(srs_2)



def update_geotransform_projection(projection,
                                   geotransform,
                                   output_projection='WGS84'):
    if not output_projection:
        return geotransform, projection
    if not projection:
        return geotransform, output_projection
    # print('*** compare: ', projection, output_projection)

    if compare_projections(projection, output_projection) is not False:
        # print('*** exiting')
        return geotransform, projection

    current_coordinate_system = get_srs_from_projection(projection)
    if not current_coordinate_system:
        print('WARNING error transforming projection from %s to %s'
              % (projection, output_projection))
        return geotransform, projection

    output_coordinate_system = get_srs_from_projection(output_projection)
    if not output_coordinate_system:
        print('WARNING error transforming projection from %s to %s'
              % (projection, output_projection))
        return geotransform, projection

    coord_transformation = None
    try:
        coord_transformation = osr.CoordinateTransformation(
            current_coordinate_system,
            output_coordinate_system)
    except TypeError:
        print('WARNING error transforming projection from %s to %s (TypeError)'
              % (projection, output_projection))
        return geotransform, projection

    update_geotransform_with_coord_transformation(
        geotransform,
        coord_transformation=coord_transformation)
    return geotransform, output_coordinate_system.GetAttrValue('projcs')


def is_projected(projection):
    if not projection:
        return False
    projection_upper = projection.upper()
    flag_projected = ('UTM' in projection_upper or
                      'METER' in projection_upper)
    return flag_projected


def print_geoinformation(*args, **kwargs):
    kwargs_orig = kwargs.copy()
    prefix = kwargs.pop('prefix', '')
    projection = kwargs.get('projection', '')
    flag_print_all = kwargs.pop('flag_print_all', False)
    flag_print_step = kwargs.pop('flag_print_step', True)
    flag_output_in_wgs84 = kwargs.get('flag_output_in_wgs84', False)
    flag_projected = (is_projected(projection) and
                      not flag_output_in_wgs84)
    # flag_print_all = kwargs.pop('flag_print_all', '')
    kwargs['verbose'] = False
    ret_dict = get_coordinates(*args, **kwargs)
    if ret_dict is None:
        return

    lat_arr = ret_dict['lat_arr']
    lon_arr = ret_dict['lon_arr']
    step_lat = ret_dict['step_lat']
    step_lon = ret_dict['step_lon']
    lat_size = ret_dict['lat_size']
    lon_size = ret_dict['lon_size']
    # flag_lat_lon = projection is None or 'utm' not in projection.lower()
    if not flag_projected:
        northing_str = 'lat'
        easting_str = 'lon'
        unit_str = '[deg]'
    else:
        northing_str = 'northing'
        easting_str = 'easting'
        unit_str = '[m]'

    if projection and flag_print_all:
        print(prefix + 'projection:',
              projection.replace('],', '],\n'+prefix+' '*len('projection: ')),
              end='\r')

    # outer edges
    if flag_print_all or flag_print_step:
        if ((lat_arr is not None and
             plant.isvalid(lat_arr[0]) and
             plant.isvalid(lat_arr[1]) and
             plant.isvalid(step_lat)) or
            (lon_arr is not None and
             plant.isvalid(lon_arr[0]) and
             plant.isvalid(lon_arr[1]) and
             plant.isvalid(step_lon))):
            print(prefix + 'boundaries (outer edges):')
        if (lat_arr is not None and
                plant.isvalid(lat_arr[0]) and
                plant.isvalid(lat_arr[1]) and
                plant.isvalid(step_lat)):
            print(prefix + '%s: %.16f, %.16f'
                  % (northing_str,
                     lat_arr[0]-abs(step_lat),
                     lat_arr[1]+abs(step_lat)))
        if (lon_arr is not None and
                plant.isvalid(lon_arr[0]) and
                plant.isvalid(lon_arr[1]) and
                plant.isvalid(step_lon)):
            print(prefix + '%s: %.16f, %.16f'
                  % (easting_str,
                     lon_arr[0]-abs(step_lon),
                     lon_arr[1]+abs(step_lon)))

    # pixels center
    if flag_print_all or not flag_print_step:
        if ((lat_arr is not None and
             plant.isvalid(lat_arr[0]) and
             plant.isvalid(lat_arr[1])) or
            (lon_arr is not None and
             plant.isvalid(lon_arr[0]) and
             plant.isvalid(lon_arr[1]))):
            print(prefix + 'boundaries (pixel centers):')
        if (lat_arr is not None and
                plant.isvalid(lat_arr[0]) and
                plant.isvalid(lat_arr[1]) and
                lat_arr[0] != lat_arr[1]):
            print(prefix + '%s: %.16f, %.16f'
                  % (northing_str,
                     lat_arr[0], lat_arr[1]))
        elif (lat_arr is not None and
              plant.isvalid(lat_arr[0])):
            print(prefix + '%s: %.16f'
                  % (northing_str,
                     lat_arr[0]))
        if (lon_arr is not None and
                plant.isvalid(lon_arr[0]) and
                plant.isvalid(lon_arr[1]) and
                lon_arr[0] != lon_arr[1]):
            print(prefix + '%s: %.16f, %.16f'
                  % (easting_str,
                     lon_arr[0], lon_arr[1]))
        elif (lon_arr is not None and
              plant.isvalid(lon_arr[0])):
            print(prefix + '%s: %.16f'
                  % (easting_str, lon_arr[0]))

    if (lat_arr is not None and
            plant.isvalid(lat_arr[0]) and
            plant.isvalid(lat_arr[1]) and
            lat_arr[0] != lat_arr[1]):
        print(prefix + 'center %s: %.16f'
              % (northing_str, (lat_arr[0]+lat_arr[1])/2.0))
    if (lon_arr is not None and
            plant.isvalid(lon_arr[0]) and
            plant.isvalid(lon_arr[1]) and
            lon_arr[0] != lon_arr[1]):
        print(prefix + 'center %s: %.16f'
              % (easting_str, (lon_arr[0]+lon_arr[1])/2.0))

    if flag_print_step and step_lat is not None:
        if not flag_projected:
            approx_str = '(approx. %.1fm)' % deg_to_m_lat(step_lat)
        else:
            approx_str = ''
        print(prefix + '%s step: %.16f %s'
              % (northing_str, step_lat,
                 approx_str))
    if flag_print_step and step_lon is not None:
        if (not not flag_projected or
                lat_arr is None or
                plant.isnan(lat_arr[0]) or
                plant.isnan(lat_arr[1])):
            approx_str = ''
        else:
            mean_lat = (lat_arr[0]+lat_arr[1])/2.0
            approx_str = ('(approx. %.1fm at lat=%.0f)'
                          % (deg_to_m_lon(step_lon,
                                          lat=mean_lat),
                             mean_lat))
        print(prefix + '%s step: %.16f %s'
              % (easting_str, step_lon, approx_str))

    if flag_print_all:
        if (flag_print_step and step_lon is not None and
                step_lon is not None):
            print(prefix + 'PLAnT parameters: -b '
                  ' %.8f %.8f %.8f %.8f'
                  ' --step-lat %.8f  --step-lon %.8f'
                  % (lat_arr[0], lat_arr[1], lon_arr[0], lon_arr[1],
                     step_lat, step_lon))
        print(prefix + 'polygon (counterclockwise) (lon, lat): '
              '%.8f,%.8f,%.8f,%.8f,%.8f,%.8f,%.8f,%.8f'
              % (lon_arr[1], lat_arr[1], lon_arr[0], lat_arr[1],
                 lon_arr[0], lat_arr[0], lon_arr[1], lat_arr[0]))
    if lat_size is not None and step_lat is not None:
        if not not flag_projected:
            approx_str = ''
        else:
            approx_str = ('(approx. %.3fkm)'
                          % (1e-3*lat_size*deg_to_m_lat(step_lat)))
        print(prefix + f'%s size {unit_str}: %.16f %s'
              % (northing_str, lat_size*step_lat, approx_str))

    if lon_size is not None and step_lon is not None:
        if not not flag_projected:
            approx_str = ''
        else:
            mean_lat = (lat_arr[0]+lat_arr[1])/2.0
            approx_str = ('(approx. %.3fkm at lat=%.0f)'
                          % (1e-3*lon_size*deg_to_m_lon(step_lon,
                                                        lat=mean_lat),
                             mean_lat))
        print(prefix + f'%s size {unit_str}: %.16f %s'
              % (easting_str, lon_size*step_lon, approx_str))
    '''
    if flag_projected:
        kwargs_orig['flag_output_in_wgs84'] = True
        # kwargs_orig['flag_print_all'] = False
        print('')
        print_geoinformation(*args, **kwargs_orig)
        print('')
    '''


def m_to_deg(m_value, lat=0):
    # Earth's radius, sphere
    # if np.abs(lat) < 15:
    #    delta_lat = m_value/110574
    # elif 15 <= np.abs(lat) and np.abs(lat) <= 45:
    #    delta_lat = m_value/111132
    # else:
    delta_lat = 180*m_value/(np.pi*plant.EARTH_RADIUS)
    delta_lon = 180*m_value/(np.pi*plant.EARTH_RADIUS*np.cos(np.pi*lat/180))
    return delta_lat, delta_lon


def m_to_deg_lat(m_value):
    delta_lat = (180*m_value /
                 (np.pi*plant.EARTH_RADIUS))
    return delta_lat


def m_to_deg_lon(m_value, lat=None):
    if lat is None:
        lat = 0.0
    delta_lon = (180*m_value /
                 (np.pi*plant.EARTH_RADIUS*np.cos(np.pi*lat/180)))
    return delta_lon


def deg_to_m(deg_value, lat=0):
    # Earth's radius, sphere
    # if np.abs(lat) < 15:
    #     delta_lat = deg_value*110574
    # elif 15 <= np.abs(lat) and np.abs(lat) <= 45:
    #     delta_lat = deg_value*111132
    # else:
    delta_lat = deg_value*np.pi*plant.EARTH_RADIUS/180
    delta_lon = deg_value*(np.pi*plant.EARTH_RADIUS*np.cos(np.pi*lat/180))/180
    return delta_lat, delta_lon


def deg_to_m_lat(deg_value):
    delta_lat = deg_value*np.pi*plant.EARTH_RADIUS/180
    return delta_lat


def deg_to_m_lon(deg_value, lat=0):
    delta_lon = (deg_value*(np.pi*plant.EARTH_RADIUS *
                            np.cos(np.pi*lat/180))/180)
    return delta_lon


def test_overlap(image_obj=None,
                 geotransform=None,
                 gcp_list=None,
                 gcp_projection=None,
                 lat_size=None,
                 lon_size=None,
                 lat_arr=None,
                 lon_arr=None,
                 projection=None,
                 image_2_obj=None,
                 geotransform_2=None,
                 gcp_list_2=None,
                 gcp_projection_2=None,
                 lat_size_2=None,
                 lon_size_2=None,
                 lat_arr_2=None,
                 lon_arr_2=None,
                 projection_2=None):
    # print('debug fix this (test_overlap)')
    # return True
    # if valid_coordinates(geotransform=geotransform):

    image_obj = plant.PlantImage(image_obj,
                                 geotransform=geotransform,
                                 gcp_list=gcp_list,
                                 gcp_projection=gcp_projection,
                                 projection=projection)
    geotransform = image_obj.geotransform
    gcp_list = image_obj.gcp_list
    gcp_projection = image_obj.gcp_projection
    projection = image_obj.projection
    image_2_obj = plant.PlantImage(image_2_obj,
                                   geotransform=geotransform_2,
                                   gcp_list=gcp_list_2,
                                   gcp_projection=gcp_projection_2,
                                   projection=projection_2)
    geotransform_2 = image_2_obj.geotransform
    gcp_list_2 = image_2_obj.gcp_list
    gcp_projection_2 = image_2_obj.gcp_projection
    projection_2 = image_2_obj.projection
    # print('*** projection: ', projection)
    # print('*** geotransform: ', geotransform)
    # print('*** projection_2: ', projection_2)
    # print('*** geotransform_2: ', geotransform_2)
    if not projection and projection_2:
        projection = projection_2
    elif projection and not projection_2:
        projection_2 = projection
    if not gcp_projection and gcp_projection_2:
        gcp_projection = gcp_projection_2
    elif gcp_projection and not gcp_projection_2:
        gcp_projection_2 = gcp_projection
    ret_dict = get_coordinates(geotransform=geotransform,
                               lat_arr=lat_arr,
                               lon_arr=lon_arr,
                               gcp_list=gcp_list,
                               gcp_projection=gcp_projection,
                               lat_size=lat_size,
                               lon_size=lon_size,
                               projection=projection,
                               flag_output_in_wgs84=True)
    if ret_dict is None:
        return
    lat_arr_a = ret_dict['lat_arr']
    lon_arr_a = ret_dict['lon_arr']

    # print('*** image 1: ', lat_arr_a, lon_arr_a)
    # bbox_a = get_bbox(lat_arr_a, lon_arr_a)
    # debug(ret)
    # debug(geotransform_2)
    ret_dict = get_coordinates(geotransform=geotransform_2,
                               lat_arr=lat_arr_2,
                               lon_arr=lon_arr_2,
                               gcp_list=gcp_list_2,
                               gcp_projection=gcp_projection_2,
                               lat_size=lat_size_2,
                               lon_size=lon_size_2,
                               projection=projection_2,
                               flag_output_in_wgs84=True)
    
    if ret_dict is None:
        return
    lat_arr_b = ret_dict['lat_arr']
    lon_arr_b = ret_dict['lon_arr']
    # print('*** image 2: ', lat_arr_b, lon_arr_b)
    # debug(ret)
    # print('***********************')
    # print(lat_arr_a, lon_arr_a)
    # print(lat_arr_b, lon_arr_b)
    # print('***********************')

    if (lat_arr_a is None or
            lon_arr_a is None or
            lat_arr_b is None or
            lon_arr_b is None):
        return

    if (lat_arr_a[0] < lat_arr_b[0] and
            lat_arr_a[1] < lat_arr_b[0]):
        return False

    if (lat_arr_a[0] > lat_arr_b[1] and
            lat_arr_a[1] > lat_arr_b[1]):
        return False

    if (lon_arr_a[0] < lon_arr_b[0] and
            lon_arr_a[1] < lon_arr_b[0]):
        return False

    if (lon_arr_a[0] > lon_arr_b[1] and
            lon_arr_a[1] > lon_arr_b[1]):
        return False

    return True
    # bbox_b = get_bbox(lat_arr_b, lon_arr_b)

    # if bbox_b is None:
    #     return
    '''
    if test_point_inside_bbox(lat_arr_a[0], lon_arr_a[0],
                              bbox_b):
        return True
    if test_point_inside_bbox(lat_arr_a[0], lon_arr_a[1],
                              bbox_b):
        return True
    if test_point_inside_bbox(lat_arr_a[1], lon_arr_a[0],
                              bbox_b):
        return True
    if test_point_inside_bbox(lat_arr_a[1], lon_arr_a[1],
                              bbox_b):
        return True
    if test_point_inside_bbox(lat_arr_b[0], lon_arr_b[0],
                              bbox_a):
        return True
    if test_point_inside_bbox(lat_arr_b[0], lon_arr_b[1],
                              bbox_a):
        return True
    if test_point_inside_bbox(lat_arr_b[1], lon_arr_b[0],
                              bbox_a):
        return True
    if test_point_inside_bbox(lat_arr_b[1], lon_arr_b[1],
                              bbox_a):
        return True
    '''
    # return False


def test_point_inside_bbox(lat, lon, bbox):
    if (lat >= bbox[0] and lat <= bbox[1] and
            lon >= bbox[2] and lon <= bbox[3]):
        # debug(lat, lon, bbox, True)
        return True
    # debug(lat, lon, bbox, False)
    return False


def valid_coordinates(*args, **kwargs):
    # return True
    geotransform = kwargs.get('geotransform', None)
    if (geotransform is not None and
            geotransform[0:6] == [0.0, 1.0, 0.0, 0.0, 0.0, 1.0]):
        return False
    # return True
    # projection = kwargs.get('projection', None)
    # print('*** test valid')

    ret_dict = get_coordinates(*args, **kwargs, flag_output_in_wgs84=True)
    # print('*** ret_dict: ', ret_dict)
    if ret_dict is None:
        # print('*** exit 1')
        return False

    if not all([v is not None for k, v in ret_dict.items()
                if k != 'projection']):
        # print('*** exit 2')
        return False
    return True
    lat_arr = ret_dict['lat_arr']
    lon_arr = ret_dict['lon_arr']
    # print('*** valid_coordinates lat, lon:', lat_arr, lon_arr)
    # step_lat = ret_dict['step_lat']
    # step_lon = ret_dict['step_lon']
    # lat_size = ret_dict['lat_size']
    # lon_size = ret_dict['lon_size']

    # using SE reference (step_lat > 0)
    if lat_arr is not None:
        # lat_arr = [np.nanmin(lat_arr), np.nanmax(lat_arr)]
        if (any([x < -100 for x in lat_arr]) or
                any([x > 100 for x in lat_arr])):
            # print('*** exit 3')
            return False
    if lon_arr is not None:
        # lon_arr = [np.nanmin(lon_arr), np.nanmax(lon_arr)]
        if (any([x < -190 for x in lon_arr]) or
                any([x > 370 for x in lon_arr])):
            # print('*** exit 4')
            return False
    # print('*** exit 5')
    return True


def get_geotransform(bbox=None,
                     lat_arr=None,
                     lon_arr=None,
                     step_lat=None,
                     step_lon=None,
                     step=None,
                     lat_size=None,
                     lon_size=None,
                     geotransform=None,
                     keep_step_lat_signal=False):
    '''
    get gdal's geotransform vector
    '''
    lat_beg = None
    lat_end = None
    lon_beg = None
    lon_end = None
    if geotransform is not None:
        if step_lon is None:
            step_lon = geotransform[1]
        if step_lat is None:
            step_lat = geotransform[5]
        lat_beg = geotransform[3]
        lon_beg = geotransform[0]
        if len(geotransform) > 6:
            lat_end = geotransform[7]
            lon_end = geotransform[6]
            if lat_size is None:
                lat_size = geotransform[9]
            if lon_size is None:
                lon_size = geotransform[8]
        else:
            if (lat_size is not None and
                    step_lat is not None):
                lat_end = (geotransform[3] +
                           (lat_size-1)*step_lat)
            if (lon_size is not None and
                    step_lon is not None):
                lon_end = (geotransform[0] +
                           (lon_size-1)*step_lon)

    # step
    if (step is not None and
            (step_lat is None or
             step_lon is None)):
        step_temp = np.asarray(step)
        dim = np.sum(step_temp.shape)
        if dim == 0:  # int, float, etc
            step_temp = [step, step]
        elif dim == 1:  # array/list one element
            step_temp = [step[0], step[0]]
        else:
            step_temp = step
        if step_lat is None:
            step_lat = step_temp[0]
        if step_lon is None:
            step_lon = step_temp[1]

    # lat_beg
    if (plant.isnan(lat_beg) and lat_arr is not None and
            plant.isvalid(lat_arr[0])):
        lat_beg = lat_arr[0]
    if plant.isnan(lat_beg) and bbox is not None:
        lat_beg = bbox[0]
    if plant.isnan(lat_beg):
        lat_beg = np.nan
    else:
        lat_beg = float(lat_beg)

    # lat_end
    if (plant.isnan(lat_end) and lat_arr is not None and
            plant.isvalid(lat_arr[1])):
        lat_end = lat_arr[1]
    if plant.isnan(lat_end) and bbox is not None:
        lat_end = bbox[1]
    if plant.isnan(lat_end):
        lat_end = np.nan
    else:
        lat_end = float(lat_end)

    # lon_beg
    if (plant.isnan(lon_beg) and lon_arr is not None and
            plant.isvalid(lon_arr[0])):
        lon_beg = lon_arr[0]
    if plant.isnan(lon_beg) and bbox is not None:
        lon_beg = bbox[2]
    if plant.isnan(lon_beg):
        lon_beg = np.nan
    else:
        lon_beg = float(lon_beg)

    # lon_end
    if (plant.isnan(lon_end) and lon_arr is not None and
            plant.isvalid(lon_arr[1])):
        lon_end = lon_arr[1]
    if plant.isnan(lon_end) and bbox is not None:
        lon_end = bbox[3]

    if plant.isnan(lon_end):
        lon_end = np.nan
    else:
        lon_end = float(lon_end)

    '''
    try:
        lat_end = lat_arr[1]
    except:
        lat_end = np.nan

    try:
        lon_beg = lon_arr[0]
    except:
        lon_beg = np.nan
    try:
        lon_end = lon_arr[1]
    except:
        lon_end = np.nan

    if not step_lat:
        step_lat = np.nan
    if not step_lon:
        step_lon = np.nan
    '''
    # lat_size

    if lat_size is None:
        if (plant.isvalid(lat_beg) and
                plant.isvalid(lat_end) and
                plant.isvalid(step_lat)):
            lat_size = round(abs(float(lat_end-lat_beg)/step_lat)+1)
        else:
            lat_size = np.nan

    # step_lat
    if (plant.isnan(step_lat) and
            plant.isvalid(lat_end) and
            plant.isvalid(lat_beg) and
            plant.isvalid(lat_size)):
        step_lat = float(lat_end-lat_beg)/(lat_size-1)

    # lon_size
    if lon_size is None:
        if (plant.isvalid(lon_beg) and
                plant.isvalid(lon_end) and
                plant.isvalid(step_lon)):
            lon_size = round(float(lon_end-lon_beg)/step_lon+1)
        else:
            lon_size = np.nan

    # step_lon
    if (plant.isnan(step_lon) and
            plant.isvalid(lon_end) and
            plant.isvalid(lon_beg) and
            plant.isvalid(lon_size)):
        step_lon = float(lon_end-lon_beg)/(lon_size-1)

    # if no lat_beg, use lat end instead
    if plant.isnan(lat_beg):
        lat_temp = lat_beg
        lat_beg = lat_end
        lat_end = lat_temp
        if plant.isvalid(step_lat):
            step_lat = -step_lat

    # change lat signal
    if (plant.isvalid(lat_beg) and
            plant.isvalid(lat_end) and
            not keep_step_lat_signal):
        lat_arr = [lat_beg, lat_end]
        lat_beg = min(lat_arr)
        lat_end = max(lat_arr)
        if plant.isvalid(step_lat):
            step_lat = abs(step_lat)

    # print(lat_end, lon_end, lat_size, lon_size)
    # create geotransform
    if ((plant.isvalid(lat_end) and
            plant.isvalid(lon_end)) or  # and
            (plant.isvalid(lat_size) and
             plant.isvalid(lon_size))):
        geotransform = [lon_beg,
                        step_lon,
                        0,
                        lat_beg,
                        0,
                        step_lat,
                        lon_end,
                        lat_end,
                        lon_size,
                        lat_size]
    else:
        geotransform = [lon_beg,
                        step_lon,
                        0,
                        lat_beg,
                        0,
                        step_lat]

    return geotransform


def get_bbox(lat_arr, lon_arr, string=False):
    '''
    get bounding box
    '''

    if (lat_arr is None or
            lon_arr is None or
            len(lat_arr) != 2 or
            len(lon_arr) != 2):
        return
    bbox = np.asarray([lat_arr, lon_arr]).ravel()
    if all([plant.isnan(x) for x in bbox]):
        return
    if not string:
        return bbox
    else:
        bbox = [str(x) for x in bbox]
        return ' '.join(bbox)


def get_lat_lon_arr(bbox):
    lat_arr = np.asarray(bbox[0:2], dtype=np.float)
    lon_arr = np.asarray(bbox[2:4], dtype=np.float)
    return lat_arr, lon_arr


def geotransform_shift(geotransform_orig, lat_shift, lon_shift,
                       lat_size=None, lon_size=None):
    if geotransform_orig is None:
        return
    ret_dict = get_coordinates(geotransform=geotransform_orig,
                               lat_size=lat_size,
                               lon_size=lon_size)
    if ret_dict is None:
        return
    lat_arr = ret_dict['lat_arr']
    lon_arr = ret_dict['lon_arr']
    step_lat = ret_dict['step_lat']
    step_lon = ret_dict['step_lon']
    lat_size = ret_dict['lat_size']
    lon_size = ret_dict['lon_size']
    print('shifting geo-reference by (%.16f, %.16f)'
          % (lat_shift, lon_shift))
    lat_arr = [x + lat_shift for x in lat_arr]
    lon_arr = [x + lon_shift for x in lon_arr]
    geotransform = get_geotransform(lat_arr=lat_arr,
                                    lon_arr=lon_arr,
                                    step_lat=step_lat,
                                    step_lon=step_lon)
    return geotransform


def geotransform_edges_to_centers(geotransform_orig,
                                  lat_size=None,
                                  lon_size=None,
                                  keep_step_lat_signal=False):
    if geotransform_orig is None:
        return
    ret_dict = get_coordinates(geotransform=geotransform_orig,
                               lat_size=lat_size+1,
                               lon_size=lon_size+1)
    if ret_dict is None:
        return
    lat_arr = ret_dict['lat_arr']
    lon_arr = ret_dict['lon_arr']
    step_lat = ret_dict['step_lat']
    step_lon = ret_dict['step_lon']
    lat_size = ret_dict['lat_size']
    lon_size = ret_dict['lon_size']

    if lat_arr is None or lon_arr is None:
        return
    step_lat_orig = geotransform_orig[5]
    if step_lat_orig < 0 and keep_step_lat_signal:
        lat_arr = [max(lat_arr), min(lat_arr)]
        step_lat = step_lat_orig
    geotransform = get_geotransform(lat_arr=[lat_arr[0]+step_lat/2,
                                             lat_arr[1]-step_lat/2],
                                    lon_arr=[lon_arr[0]+step_lon/2,
                                             lon_arr[1]-step_lon/2],
                                    step_lat=step_lat,
                                    step_lon=step_lon,
                                    keep_step_lat_signal=keep_step_lat_signal)
    return geotransform


def geotransform_centers_to_edges(geotransform_orig,
                                  lat_size=None,
                                  lon_size=None,
                                  keep_step_lat_signal=False):
    if geotransform_orig is None:
        return

    ret_dict = get_coordinates(geotransform=geotransform_orig,
                               lat_size=lat_size,
                               lon_size=lon_size)

    if ret_dict is None:
        return
    lat_arr = ret_dict['lat_arr']
    lon_arr = ret_dict['lon_arr']
    step_lat = ret_dict['step_lat']
    step_lon = ret_dict['step_lon']
    lat_size = ret_dict['lat_size']
    lon_size = ret_dict['lon_size']
    if lat_arr is None or lon_arr is None:
        return
    step_lat_orig = geotransform_orig[5]
    if (step_lat_orig is not None and step_lat_orig < 0 and
            keep_step_lat_signal):
        lat_arr = [max(lat_arr), min(lat_arr)]
        step_lat = step_lat_orig
    if step_lat is None or step_lon is None:
        return
    geotransform = get_geotransform(lat_arr=[lat_arr[0]-step_lat/2,
                                             lat_arr[1]+step_lat/2],
                                    lon_arr=[lon_arr[0]-step_lon/2,
                                             lon_arr[1]+step_lon/2],
                                    step_lat=step_lat,
                                    step_lon=step_lon,
                                    keep_step_lat_signal=keep_step_lat_signal)
    return geotransform


def get_geotransform_from_header(filename_orig, verbose=False):
    '''
    get geotransform (gdal) from an ISCE .xml header
    '''
    if plant.test_other_drivers(filename_orig):
        return

    try:
        from iscesys.Parsers.FileParserFactory import createFileParser
        from isceobj.Util import key_of_same_content
    except ModuleNotFoundError:
        return

    geotransform = None

    filename = filename_orig
    if ((not path.isfile(filename_orig)) and
            plant.IMAGE_NAME_SEPARATOR in filename_orig):
        filename = filename.split(plant.IMAGE_NAME_SEPARATOR)[0]

    gdal.UseExceptions()
    try:
        current_dataset = gdal.Open(filename_orig, gdal.GA_ReadOnly)
        file_format = str(current_dataset.GetDriver().ShortName)
        if verbose:
            print('reading coordinates from: %s (GDAL:%s) '
                  % (filename_orig, file_format))
    except:
        current_dataset = None

    if (current_dataset is None and
            plant.isfile(filename+'.vrt')):
        try:
            current_dataset = gdal.Open(filename+'.vrt',
                                        gdal.GA_ReadOnly)
            file_format = str(current_dataset.GetDriver().ShortName)
            if verbose:
                print('reading coordinates from: %s (GDAL:%s) '
                      % (filename, file_format))
        except:
            current_dataset = None

    if current_dataset is not None:
        geotransform_edges = list(current_dataset.GetGeoTransform())
        lon_size = current_dataset.RasterXSize
        lat_size = current_dataset.RasterYSize
        geotransform = None
        if valid_coordinates(geotransform=geotransform_edges,
                             lat_size=lat_size,
                             lon_size=lon_size):
            geotransform = geotransform_edges_to_centers(
                geotransform_edges, lat_size=lat_size,
                lon_size=lon_size)  # [0:6]
        current_dataset = None
        gdal.ErrorReset()

    if (geotransform is None and
            plant.isfile(filename + '.xml') or
            filename.endswith('.xml')):
        if verbose:
            print('reading coordinates from: %s (ISCE) ')
        PA = createFileParser('xml')
        if filename.endswith('.xml'):
            dictNow, dictFact, dictMisc = PA.parse(filename)
        else:
            dictNow, dictFact, dictMisc = PA.parse(filename+'.xml')
        flag_outer_edges = False
        for i in range(1, 3):
            try:
                coordinate = key_of_same_content(
                    'Coordinate'+str(i), dictNow)[1]
                starting_value = float(key_of_same_content(
                    'startingValue', coordinate)[1])
                delta = float(key_of_same_content(
                    'delta', coordinate)[1])
                size = float(key_of_same_content(
                    'size', coordinate)[1])
            except:
                return
            # ending_value from header may be wrong
            ending_value_center = starting_value + delta*(size-1)
            ending_value_edges = starting_value + delta*(size)

            try:
                ending_value_header = float(key_of_same_content(
                    'endingValue', coordinate)[1])
            except:
                ending_value_header = float('nan')

            # print(ending_value_header)
            # WARNING: Hard-coding:
            # ending_value_header = float('nan')
            flag_force_pixels_center = True

            message = ''
            if plant.isvalid(ending_value_header):
                diff_center = abs(ending_value_center -
                                  ending_value_header)
                diff_edges = abs(ending_value_edges -
                                 ending_value_header)
                if (flag_force_pixels_center or
                        diff_center <= diff_edges):
                    ending_value = ending_value_center
                    message = ('WARNING (ISCE header) '
                               'considering pixels center convention..')
                else:
                    ending_value = ending_value_edges
                    flag_outer_edges = True
                    message = ('WARNING (ISCE header) '
                               'considering outer edges convention')
            else:
                # ending_value = ending_value_center
                ending_value = ending_value_header

            if i == 1:
                lon_arr = [starting_value, ending_value]
                lon_arr = [min(lon_arr), max(lon_arr)]
                step_lon = delta
                lon_size = size
            else:
                # ending_value = starting_value + delta*(size-1)
                lat_arr = [starting_value, ending_value]
                lat_arr = [min(lat_arr), max(lat_arr)]
                step_lat = abs(delta)
                lat_size = size
        geotransform = [lon_arr[0], step_lon, 0, lat_arr[1], 0,
                        -step_lat, lon_arr[1], lat_arr[0], lon_size,
                        lat_size]

        if valid_coordinates(geotransform=geotransform):
            if message and verbose:
                print(message)
            if flag_outer_edges:
                geotransform = geotransform_edges_to_centers(
                    geotransform, lat_size=lat_size,
                    lon_size=lon_size)  # [0:6]
        else:
            geotransform = None

    # geotransform += [geotransform[0]+geotransform[1]*(lon_size-1),
    #                     geotransform[3]+geotransform[5]*(lat_size-1),
    #                     lon_size, lat_size]
    #    return geotransform
# 
    # else:
    #     return
    return geotransform


def assign_georeferences(input_var,
                         topo_dir=None,
                         lat_file=None,
                         lon_file=None,
                         output_file=None,
                         plant_transform_obj=None,
                         suffix='temp',
                         # temporary_files=[],
                         flag_temporary=True,
                         force=True,
                         verbose=True):
    '''
    assign georeference to a slant-range product
    '''
    if isinstance(input_var, plant.PlantImage):
        image_obj = input_var
    else:
        image_obj = plant.PlantImage(input_var)
    if output_file is None:
        output_file = image_obj.filename
    width_data = image_obj.width
    length_data = image_obj.length

    if lat_file is None or lon_file is None:
        ret_dict = plant.get_topo_files(topo_dir)
        if lat_file is None and 'lat_file' in ret_dict.keys():
            lat_file = ret_dict['lat_file']
        if lon_file is None and 'lon_file' in ret_dict.keys():
            lon_file = ret_dict['lon_file']

    if not(plant.isfile(lat_file)):
        print('ERROR file not found: '
              f'lat.rdr, lat.vrt or lat.rdr.vrt in {topo_dir}')
        return
    if not(plant.isfile(lon_file)):
        print('ERROR file not found: '
              f'lon.rdr, lon.vrt or lon.rdr.vrt in {topo_dir}')
        return

    if verbose:
        print(f'Northing/latitude file: {lat_file}')
        print(f'Easting/longitude file: {lon_file}')

    flag_generate_lat_lon = False
    flag_generate_new_file = False
    new_file = None
    lat_file_obj = None
    lon_file_obj = None

    if not isinstance(lat_file, str):
        lat_file_obj = lat_file
        flag_generate_lat_lon = True

    if not isinstance(lon_file, str):
        lon_file_obj = lon_file
        flag_generate_lat_lon = True

    if (not flag_generate_lat_lon and
        (not plant.test_gdal_open(lat_file) or
            not plant.test_gdal_open(lon_file) or
            (plant_transform_obj is not None and
             plant_transform_obj.flag_apply_transformation()))):
        # if is_isce_image(lat_file) and is_isce_image(lon_file):
        flag_generate_lat_lon = True
    elif not flag_generate_lat_lon:
        ret_dict = plant.generate_vrt_with_geolocation_files(
            output_file, lat_file, lon_file,
            flag_temporary=flag_temporary)
        if ret_dict is None:
            flag_generate_lat_lon = True
            flag_generate_new_file = True
        else:
            new_file = ret_dict['data_file']
            plant.append_temporary_file(new_file)

    if flag_generate_lat_lon:
        print('updating northing/easting gelocation files...')
        if lat_file_obj is None:
            new_lat = path.basename(lat_file+'_geo_temp_lat_' +
                                    suffix).replace(':', '_')
            lat_file_obj = plant.read_image(
                lat_file,
                plant_transform_obj=plant_transform_obj,
                verbose=False)
        else:
            new_lat = path.basename('geo_temp_lat_' +
                                    suffix).replace(':', '_')
            
        if (lat_file_obj.length != length_data or
                lat_file_obj.width != width_data):
            print('ERROR input data and northing/lat geolocation array'
                  ' have different dimmensions: (%d, %d) and (%d, %d)'
                  % (length_data, width_data, lat_file_obj.length,
                     lat_file_obj.width))
            return
        if lon_file_obj is None:
            new_lon = path.basename(lon_file+'_geo_temp_lon_' +
                                    suffix).replace(':', '_')
            lon_file_obj = plant.read_image(
                lon_file,
                plant_transform_obj=plant_transform_obj,
                verbose=False)
        else:
            new_lon = path.basename('geo_temp_lon_' +
                                    suffix).replace(':', '_')
        if (lon_file_obj.length != length_data or
                lon_file_obj.width != width_data):
            print('ERROR input data and eating/lon geolocation array'
                  ' have different dimmensions: (%d, %d) and (%d, %d)'
                  % (length_data, width_data, lon_file_obj.length,
                     lon_file_obj.width))
            return
        plant.append_temporary_file(new_lat)
        plant.append_temporary_file(new_lon)
        plant.save_image(lat_file_obj, new_lat, force=force,
                         output_format=plant.DEFAULT_GDAL_FORMAT)
        plant.save_image(lon_file_obj, new_lon, force=force,
                         output_format=plant.DEFAULT_GDAL_FORMAT)
        if new_file is None and not flag_generate_new_file:
            ret_dict = plant.generate_vrt_with_geolocation_files(
                output_file, new_lat, new_lon,
                flag_temporary=flag_temporary)
            if ret_dict is not None:
                new_file = ret_dict['data_file']
        if new_file is None:
            new_file = path.basename(output_file+'_geo_temp_ag_' +
                                     suffix)
            plant.append_temporary_file(new_file)
            plant.copy_image(output_file,
                             new_file,
                             create_link=True,
                             force=force)
            ret_dict = plant.generate_vrt_with_geolocation_files(
                new_file, new_lat, new_lon,
                flag_temporary=flag_temporary)
            if ret_dict is not None:
                new_file = ret_dict['data_file']
    if new_file and not new_file.endswith('.vrt'):
        new_file += '.vrt'
    return new_file


def get_lat_lon_files_from_topo_dir(bbox_topo):
    topo_dict = plant.get_topo_files(bbox_topo)
    if topo_dict is None:
        print('ERROR invalid geolocation directory (topo_dir): %s'
              % bbox_topo)
        return
    file_lat = topo_dict['lat_file']
    if not(plant.isfile(file_lat)):
        print('ERROR northing/latitude file not found in %s'
              % bbox_topo)
        return
    file_lon = topo_dict['lon_file']
    if not(plant.isfile(file_lon)):
        print('ERROR easting/longitude file not found in %s'
              % bbox_topo)
        return
    return file_lat, file_lon


def get_lat_lon_boundaries_from_topo_dir(
        bbox_topo,
        plant_transform_obj=None):
    '''
    get lat/lon boundaries from geolocation files
    '''
    # if not path.isdir(bbox_topo):
    #     return
    ret = get_lat_lon_files_from_topo_dir(bbox_topo)
    if ret is None:
        return
    file_lat, file_lon = ret
    ret = plant.get_min_max_gdal(file_lat, plant_transform_obj)
    min_lat, max_lat = ret
    # print('*** lat ', file_lat, min_lat, max_lat)
    ret = plant.get_min_max_gdal(file_lon, plant_transform_obj)
    min_lon, max_lon = ret
    # print('*** lon ', file_lon, min_lon, max_lon)
    lat_image_obj = plant.read_image(
        file_lat,
        plant_transform_obj=plant_transform_obj)
    lon_image_obj = plant.read_image(
        file_lon,
        plant_transform_obj=plant_transform_obj)
    width = lat_image_obj.width
    length = lat_image_obj.length
    lat_data = lat_image_obj.image
    lon_data = lon_image_obj.image

    # Fix this:
    angle_1 = np.arctan2(lat_data[0, 0] - lat_data[-1, 0],
                         lon_data[0, 0] - lon_data[-1, 0])
    angle_2 = np.arctan2(lat_data[0, -1] - lat_data[-1, -1],
                         lon_data[0, -1] - lon_data[-1, -1])
    angle = (angle_1 + angle_2) / 2.0
    lat_size = abs(length*np.sin(angle))+abs(width*np.cos(angle))
    lon_size = abs(length*np.cos(angle))+abs(width*np.sin(angle))
    # step_lat = min(float(max_lat - min_lat) / float((width + length)/2.0),
    #                float(max_lon - min_lon) / float((width + length)/2.0))
    step_lat = float(max_lat - min_lat)/lat_size
    step_lon = float(max_lon - min_lon)/lon_size
    step_lon = (step_lat + step_lon)/2.0
    step_lat = step_lon
    lat_arr = [min_lat, max_lat]
    lon_arr = [min_lon, max_lon]
    return lat_arr, lon_arr, step_lat, step_lon


def coregister_geo(input_file,
                   reference_file,
                   output_file=None,
                   # flag_return=False,
                   in_null=None,
                   out_null=None,
                   verbose=False,
                   force=None):
    if verbose:
        print('co-registering %s to  %s geometry..'
              % (input_file, reference_file))
    if output_file is None:
        # flag_return = True
        time_stamp = str(int(time.time()))
        # time_stamp = time.strftime("%Y_%m_%d_%H:%M:%S",
        #                            time.localtime(time.time()))
        output_file = 'temp_coregister_geo_%s' % (time_stamp)
        flag_temporary = True
    else:
        flag_temporary = False
    command = ('plant_mosaic.py %s -g %s -o %s '
               % (input_file,
                  reference_file,
                  output_file))
    if in_null is not None:
        command += (' --in-null %s' % (str(in_null)))
    if out_null is not None:
        command += (' --out-null %s' % (str(out_null)))
    if force is not None or plant.plant_config.flag_all:
        if force or plant.plant_config.flag_all:
            command += ' -f'
    if plant.plant_config.flag_never:
        command += ' -N'
    image_obj = plant.execute(command)
    # if flag_return:
    #     if plant.isfile(output_file):
    #         image = read_image(output_file)
    #     else:
    #         return
    if flag_temporary:
        if plant.isfile(output_file):
            remove(output_file)
        if plant.isfile(output_file+'.vrt'):
            remove(output_file+'.vrt')
        if plant.isfile(output_file+'.xml'):
            remove(output_file+'.xml')
        if plant.isfile(output_file+'.hdr'):
            remove(output_file+'.hdr')
    # if flag_return:
    return image_obj


def register_geo(*args, **kwargs):
    input_file = args[0]
    output_file = kwargs.pop('output_file', None)
    projection = kwargs.get('projection', None)
    # flag_return = kwargs.pop('flag_return', None)
    in_null = kwargs.pop('in_null', None)
    out_null = kwargs.pop('out_null', None)
    verbose = kwargs.pop('verbose', False)
    force = kwargs.pop('force', None)
    if verbose:
        print('registering %s parameters:  %s..'
              % (input_file, str(kwargs)))
    ret_dict = get_coordinates(**kwargs)
    if ret_dict is None:
        print('ERROR geo-coordinates could not be determined')
        return
    lat_arr = ret_dict['lat_arr']
    lon_arr = ret_dict['lon_arr']
    step_lat = ret_dict['step_lat']
    step_lon = ret_dict['step_lon']
    # lat_size = ret_dict['lat_size']
    # lon_size = ret_dict['lon_size']
    '''
    geotransform = get_geotransform(lat_arr=lat_arr,
                                    lon_arr=lon_arr,
                                    step_lat=step_lat,
                                    step_lon=step_lon,
                                    lat_size=lat_size,
                                    lon_size=lon_size)
    geotransform_centers = geotransform_edges_to_centers(
        geotransform,
        lat_size=lat_size,
        lon_size=lon_size) # [0:6]
    '''
    bbox = get_bbox(lat_arr, lon_arr)
    coord_str = ('-b %.16f %.16f %.16f %.16f'
                 % (bbox[0], bbox[1], bbox[2], bbox[3]))
    if projection:
        coord_str += ' --projection %s' % projection

    step_str = (' --step-lat %.16f --step-lon %.16f'
                % (step_lat, step_lon))

    if output_file is None:
        # flag_return = True
        time_stamp = str(int(time.time()))
        # time_stamp = time.strftime("%Y_%m_%d_%H:%M:%S",
        #                            time.localtime(time.time()))
        output_file = 'temp_coregister_geo_%s' % (time_stamp)
        flag_temporary = True
    else:
        flag_temporary = False

    command = ('plant_mosaic.py %s %s %s -o %s '
               % (input_file,
                  coord_str,
                  step_str,
                  output_file))
    if in_null is not None:
        command += (' --in-null %s' % (str(in_null)))
    if out_null is not None:
        command += (' --out-null %s' % (str(out_null)))
    if force is not None or plant.plant_config.flag_all:
        if force or plant.plant_config.flag_all:
            command += ' -f'
    if plant.plant_config.flag_never:
        command += ' -N'

    image_obj = plant.execute(command)
    # if flag_return:
    #    if plant.isfile(output_file):
    #        image = read_image(output_file)
    #    else:
    #        return
    if flag_temporary:
        if plant.isfile(output_file):
            remove(output_file)
        if plant.isfile(output_file+'.vrt'):
            remove(output_file+'.vrt')
        if plant.isfile(output_file+'.xml'):
            remove(output_file+'.xml')
        if plant.isfile(output_file+'.hdr'):
            remove(output_file+'.hdr')
    # if flag_return:
    return image_obj
