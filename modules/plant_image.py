
#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# 23280666007378
import sys
import time
from glob import glob as glob_glob
import xml.etree.ElementTree as ET
import numpy as np
from collections.abc import Sequence
import pandas as pd
import os
import gdal
from osgeo import ogr  # , osr
import importlib
import numbers
import mimetypes
from skimage.draw import ellipse
import matplotlib.image as mpimg
from matplotlib import colors as mcolors
from zipfile import ZipFile
import plant
import h5py

# TODO:
# update read_csv_data_lat_lon with pandas
FLAG_VECTOR_SAVE_LAT_LON_BANDS = True


def get_info(input_var,
             name=None,
             fig=False,
             # db10=False,
             # db20=False,
             verbose=True,
             approx_stats=False,
             n_points=None,
             band=None,
             input_key=None,
             sampling_step=None,
             sampling_step_x=None,
             sampling_step_y=None,
             sampling_step_z=None,
             input_format=None,
             # input_use_ctable=None,

             plant_transform_obj=None,
             in_null=None,

             topo_dir=None,
             only_geoinformation=False,
             show_geoinformation=True,

             show_all=None,

             data_stats=False,
             data_min=False,
             data_mean=False,
             # data_median=False,
             data_stddev=False,
             data_max=False,
             # data_border=False,
             force=None):

    '''
    print matrices and vector parameters for debugging
    '''
    kwargs = locals()

    flag_stats = (data_stats or
                  data_min or
                  data_mean or
                  # data_median or
                  data_stddev or
                  data_max or
                  # data_border or
                  n_points or
                  fig)

    # if not data_min and not data_mean and not data_max:
    #    data_min = True
    #    data_mean = True
    #    data_max = True
    image_obj = None
    image = None
    if show_all is None and flag_stats:
        show_all = True

    if isinstance(input_var, str):
        # filename = search_image(input_var)
        # plant.debug(filename)
        # if not filename:
        #     print(f'ERROR opening {input_var}')
        # filename = filename[0]
        image_obj = read_image(input_var,
                               verbose=verbose,
                               band=band,
                               # only_header=not(flag_stats),
                               input_key=input_key,
                               sampling_step=sampling_step,
                               sampling_step_x=sampling_step_x,
                               sampling_step_y=sampling_step_y,
                               sampling_step_z=sampling_step_z,
                               input_format=input_format,
                               # input_use_ctable=input_use_ctable,
                               plant_transform_obj=plant_transform_obj,
                               force=force)
        # if not flag_stats and verbose:
    elif isinstance(input_var, plant.PlantImage):
        image_obj = input_var
    else:
        image_obj = plant.PlantImage(input_var)

    if image_obj is None:
        return

    image_str = str(image_obj)
    if verbose:
        print(f'## image: {image_str}')
    file_format = None
    dtype = None
    width = None
    length = None
    depth = None
    if verbose and name:
        print('## %s' % name)
    if only_geoinformation and not isinstance(image,
                                              plant.PlantImage):
        print('WARNING (info) geoinformation not available')
        return
    # elif image_obj is not None:
    # name = image_obj.name
    with plant.PlantIndent():
        name = image_obj.name
        header_file = image_obj.header_file
        file_format = image_obj.file_format
        dtype = image_obj.dtype
        length = image_obj.length
        width = image_obj.width
        depth = image_obj.depth
        nbands = image_obj.nbands
        scheme = image_obj.scheme
        metadata = image_obj.metadata
        # description = image_obj.description
        gcp_list = image_obj.gcp_list
        gcp_projection = image_obj.gcp_projection
        n_elements = image_obj.n_elements()

        if topo_dir is not None:
            print('geographic information (geolocation arrays):')
            with plant.PlantIndent():
                plant.print_geoinformation(
                    bbox_topo=topo_dir,
                    lat_size=length,
                    lon_size=width,
                    plant_transform_obj=plant_transform_obj,
                    # prefix='    ',
                    flag_print_step=False,
                    verbose=False)

        if (show_geoinformation and only_geoinformation and
                not verbose):
            return

        if (show_geoinformation and only_geoinformation and
                image_obj.geotransform is None):
            print('WARNING geoinformation not available')
            return

        if (show_geoinformation and only_geoinformation and
                verbose):
            print('geographic information:')
            with plant.PlantIndent():
                plant.print_geoinformation(
                    geotransform=image_obj.geotransform,
                    plant_transform_obj=plant_transform_obj,
                    # prefix='    ',
                    projection=image_obj.projection,
                    flag_print_all=show_all)
            return

        '''
        n_elements = 1
        if width is not None:
            n_elements *= width
        if length is not None:
            n_elements *= length
        if depth is not None:
            n_elements *= depth
        '''

        if verbose:
            # if name is not None:
            #     print(f'    name: {name}')
            if name is not None and name != image_str:
                print(f'name: {name}')
            if header_file is not None:
                print(f'header file: {header_file}')
            # print('image attributes:')
            print('structure:')
            with plant.PlantIndent():
                print('number of bands: %d' % nbands)
                if file_format is not None:
                    print(f'file format: {file_format}')
                if scheme is not None:
                    print(f'scheme: {scheme}')
                if length is not None:
                    print(f'length: {length}')
                if width is not None:
                    print(f'width: {width}')
                if depth is not None and depth > 1:
                    print(f'depth: {depth}')
                if n_elements is not None:
                    print(f'number of elements: {n_elements}')
            if gcp_list is not None and len(gcp_list) > 0:
                print(f'GCP number of elements: {len(gcp_list)}')
                if gcp_projection is not None:
                    print(f'GCP projection: {gcp_projection}')
            if (metadata is not None and
                    (show_all or 'HDF5' not in file_format.upper())):
                print('metadata:')
                with plant.PlantIndent():
                    for i, md_element in enumerate(metadata.items()):
                        key, value = md_element
                        print(f'{key}: {value}')
            # if description is not None:
            #     print(f'description: {description}')
        if show_geoinformation and verbose:
            print('geographic information:')
            with plant.PlantIndent():
                plant.print_geoinformation(
                    geotransform=image_obj.geotransform,
                    plant_transform_obj=plant_transform_obj,
                    # prefix='    ',
                    projection=image_obj.projection,
                    flag_print_all=show_all)

        #  and (db10 or db20)

        flag_gdal = (not data_stddev and
                     not (plant_transform_obj is not None and
                          plant_transform_obj.flag_apply_transformation()) and
                     test_gdal_open(image_obj.filename))
        #             and  image_obj.nbands == 1)

        kwargs['n_elements'] = n_elements
        kwargs['flag_gdal'] = flag_gdal
        # kwargs.pop('show_all', None)
        kwargs.pop('input_var', None)
        kwargs.pop('show_geoinformation', None)
        kwargs.pop('only_geoinformation', None)
        # kwargs.pop('plant_transform_obj', None)
        kwargs.pop('input_format', None)
        kwargs.pop('sampling_step_x', None)
        kwargs.pop('sampling_step_y', None)
        kwargs.pop('sampling_step_z', None)
        kwargs.pop('sampling_step', None)
        kwargs.pop('band', None)
        kwargs.pop('name', None)
        kwargs.pop('topo_dir', None)

        if band is None:
            band_indexes = list(range(nbands))
        else:
            band_indexes = plant.get_int_list(band)
        if len(band_indexes) == 0:
            return
        if not show_all and not flag_stats:
            band_obj = image_obj.get_band(band=0)
            dtype = plant.get_dtype_name(band_obj.dtype)
            if (dtype is not None and
                    all([dtype == plant.get_dtype_name(
                        image_obj.get_band(band=b).dtype)
                         for b in range(image_obj.nbands)])):
                print(f'data type: {dtype}')
            return
        '''
        if nbands == 1:
            # band = band_indexes[0]
            band = 0
            kwargs.pop('input_key', None)
            return info_band(image_obj.get_band(band=band),
                             **kwargs)
        '''
        ret_var = [[] for x in range(nbands)]
        for b, current_band in enumerate(band_indexes):
            band_obj = image_obj.get_band(band=b)
            name = band_obj.name
            if verbose and nbands != 1 and name:
                print(f'## band {current_band}: "{name}"')
            elif verbose and nbands != 1:
                print(f'## band {current_band}')
            kwargs.pop('input_key', None)
            ret_var[b] = info_band(band_obj, **kwargs)
        return ret_var
        # return info_band(None, **kwargs)


def info_band(input_var,
              fig=False,
              # db10=False,
              # db20=False,
              verbose=True,
              approx_stats=False,
              n_points=None,

              in_null=None,
              plant_transform_obj=None,

              data_stats=False,
              data_min=False,
              data_mean=False,
              # data_median=False,
              data_stddev=False,
              data_max=False,
              n_elements=None,
              show_all=False,
              flag_gdal=False,
              force=None):

    flag_stats = (data_stats or
                  data_min or
                  data_mean or
                  # data_median or
                  data_stddev or
                  data_max or
                  # n_points or
                  fig)
    kwargs = locals()

    if isinstance(input_var, plant.PlantBand):
        band_obj = input_var
    else:
        band_obj = plant.PlantBand(input_var)

    image = None
    # name = band_obj.name
    image_loaded = band_obj.image_loaded
    if (image_loaded or n_points is not None or
            (flag_stats and not flag_gdal)):
        image = band_obj.image
        image_loaded = band_obj.image_loaded
    dtype = plant.get_dtype_name(band_obj.dtype)
    type_size = plant.get_dtype_size(band_obj.dtype)
    if n_elements is not None and type_size is not None:
        data_size = n_elements*type_size
    else:
        data_size = None

    with plant.PlantIndent():
        if verbose:
            # if name:
            #     print('name: %s' % name)
            if show_all and image_loaded:
                n_valid = np.sum(plant.isvalid(image, in_null))
                if n_elements == 0:
                    n_valid_percent = 0
                else:
                    n_valid_percent = 100*float(n_valid)/float(n_elements)
                print('# valid elements: %d (%d%%)'
                      % (n_valid,
                         n_valid_percent))
            if dtype is not None:
                print('data type: %s' % dtype)
            if type_size is not None:
                print('data type size: %dB' % type_size)
            if in_null is not None:
                print('null: %s' % in_null)
            if data_size is not None:
                print('data size: %s'
                      % plant.get_file_size_string(data_size))
            if band_obj.ctable is not None:
                print('has color table: %s'
                      % str(band_obj.ctable is not None))
            else:
                print('no ctable')

        if n_points is not None:
            if (verbose and
                    n_points <= n_elements):
                print('elements:', image)
            elif verbose and n_points is not None:
                print('first '+str(n_points)+' elements:',
                      (image.ravel())[0: n_points])
                print('last '+str(n_points)+' elements:',
                      (image.ravel())[-1-n_points: -1])
            if not flag_stats:
                return image.ravel()
        elif not flag_stats:
            return

        # plant.debug(flag_gdal)
        ret_dict = None
        if flag_gdal:
            # print('*** calling get_stats_gdal')
            try:
                ret_dict = plant.get_stats_gdal(
                    band_obj.parent_orig.filename,
                    verbose=False,
                    band=band_obj.parent_orig_band_orig_index,
                    plant_transform_obj=plant_transform_obj,
                    approx_stats=approx_stats)
            except RuntimeError:
                image = band_obj.image
                image_loaded = band_obj.image_loaded
        if ret_dict is None:
            # if image is None:
            #    image = band_obj.image
            if image is None:
                print('ERROR reading input data')
                return
            kwargs.pop('flag_gdal')
            kwargs.pop('input_var')
            kwargs.pop('n_elements')
            kwargs.pop('fig')
            kwargs.pop('force')
            kwargs.pop('show_all')
            kwargs.pop('n_points')
            kwargs.pop('plant_transform_obj')
            ret_dict = get_stats(image, **kwargs)

        minimum = None
        mean = None
        # median = None
        maximum = None
        stddev = None
        if 'minimum' in ret_dict.keys():
            minimum = ret_dict['minimum']
        if 'maximum' in ret_dict.keys():
            maximum = ret_dict['maximum']
        if 'mean' in ret_dict.keys():
            mean = ret_dict['mean']
        if 'stdDev' in ret_dict.keys():
            stddev = ret_dict['stdDev']
        # if 'median' in ret_dict.keys():
        #    median = ret_dict['median']

        if fig:
            import matplotlib.pyplot as plt
            plt.figure()
            plt.imshow(image, aspect='auto')
            plt.grid(True)
            plt.colorbar()
            mngr = plt.get_current_fig_manager()
            mngr.window.setGeometry(0,
                                    0,
                                    plant.DEFAULT_PLOT_SIZE_X,
                                    plant.DEFAULT_PLOT_SIZE_Y)
            plt.show()

        if verbose and approx_stats:
            str_star = '*'
        else:
            str_star = ''

        '''
        if db10:
            minimum = 10*np.log10(minimum) if minimum != 0 else -np.inf
            mean = 10*np.log10(mean)
            # if median is not None:
            #    median = 10*np.log10(median)
            maximum = 10*np.log10(maximum)

        if db20:
            minimum = 20*np.log10(minimum) if minimum != 0 else -np.inf
            mean = 20*np.log10(mean)
            # if median is not None:
            #     median = 20*np.log10(median)
            maximum = 20*np.log10(maximum)
        '''

        if verbose:
            text = ''
            if ((data_stats or data_min) and minimum is not None):
                text += ('min%s=%.16f  '
                         % (str_star, minimum))
            # elif data_stats or data_min:
            #     text += ('min%s=%s  '
            #              % (str_star, str(minimum)))
            if ((data_stats or data_max) and maximum is not None):
                text += ('max%s=%.16f  '
                         % (str_star, maximum))
            # elif data_stats or data_max:
            #     text += ('max%s=%s  '
            #              % (str_star, str(maximum)))
            if ((data_stats or data_mean) and mean is not None):
                text += ('mean%s=%.16f  '
                         % (str_star, mean))
            # elif data_stats or data_mean:
            #     text += ('mean%s=%s  '
            #              % (str_star, str(mean)))
            if ((data_stats or data_stddev) and stddev is not None):
                text += ('stddev%s=%.16f  '
                         % (str_star, stddev))
            # elif data_stats or data_stddev:
            #     text += ('stddev%s=%s  '
            #              % (str_star, str(stddev)))
            # if ((data_stats or data_median) and median is not None):
            #    text += ('median%s=%.16f  '
            #             % (str_star, median))
            # elif data_stats or data_median:
            #     text += ('median%s=%s  '
            #              % (str_star, str(median)))
            print(text)
        if approx_stats and verbose:
            print('    * approx.')
        # if flag_gdal and verbose:
        #    print('stats from gdalinfo')
        if data_stats:
            return [mean, stddev, minimum, maximum]
        if data_mean:
            return mean
        # if data_median:
        #    return median
        if data_stddev:
            return stddev
        if data_min:
            return minimum
        if data_max:
            return maximum


def get_stats(image,
              approx_stats=True,
              flag_stats=True,
              in_null=None,

              data_stats=False,
              data_min=False,
              data_mean=False,
              # data_median=False,
              data_stddev=False,
              data_max=False,

              # db10=False,
              # db20=False,

              verbose=True):

    if approx_stats and flag_stats:
        image = sample_image(image,
                             fast=approx_stats,
                             verbose=False)

    minimum = None
    maximum = None
    ret_dict = {}
    data = np.asarray(0, dtype=np.float)
    ndata = np.asarray(0, dtype=np.uint)
    for i in range(image.shape[0]):
        if len(image.shape) > 1:
            data_line = image[i, :]
        else:
            data_line = image
        indexes = np.where(plant.isvalid(data_line, in_null))
        data_line = data_line[indexes]
        if len(data_line) < 1:
            continue
        if 'complex' in plant.get_dtype_name(image.dtype):
            data_line = np.absolute(data_line)
        if data_stats or data_min:
            minimum_temp = np.min(data_line)
            if minimum is None:
                minimum = minimum_temp
            elif minimum_temp < minimum:
                minimum = minimum_temp
        if data_stats or data_max:
            maximum_temp = np.max(data_line)
            if maximum is None:
                maximum = maximum_temp
            elif maximum_temp > maximum:
                maximum = maximum_temp
        if data_stats or data_mean:
            data += np.sum(data_line)
            ndata += np.uint((data_line).size)
    # if data_stats or data_median:
    #     output_median = np.nanmedian(image)
    if data_stats or data_stddev:
        # if db10:
        #    image = 10*np.log10(image)
        # if db20:
        #    image = 20*np.log10(image)
        output_stddev = np.nanstd(image)

    if data_stats or (ndata != 0 and data_mean):
        output_mean = data/np.float(ndata)
    elif verbose and (data_stats or (ndata == 0 and data_mean)):
        print('no valid elements found')
        return
    if data_stats or data_min:
        ret_dict['minimum'] = minimum
    if data_stats or data_mean:
        ret_dict['mean'] = output_mean
    # if data_stats or data_median:
    #    ret_dict['median'] = output_median
    if data_stats or data_max:
        ret_dict['maximum'] = maximum
    if data_stats or data_stddev:
        ret_dict['stdDev'] = output_stddev
    return ret_dict


def new_image_obj_like(ref_image_obj):
    image_obj_copy = plant.PlantImage()
    for key in ref_image_obj.__dict__.keys():
        if key == '_band_list':
            continue
        image_obj_copy.__dict__.update({key: ref_image_obj.__dict__[key]})
    image_obj_copy._nbands = 0
    image_obj_copy._band_orig = None
    return image_obj_copy


def render_vrt(input_file,
               output_file=None,
               force_writting=False,
               edges_outer=None,
               edges_center=None,
               force=None,
               flag_temporary=False,
               verbose=False,
               context=None):
    '''
    renders GDAL .vrt header for an input file
    '''
    if not output_file:
        if not flag_temporary:
            output_file = input_file
        else:
            timestamp = str(int(time.time()))
            output_file = os.path.basename(input_file)+'_temp_'+timestamp
        if not output_file.endswith('.vrt'):
            output_file += '.vrt'
        if flag_temporary:
            plant.append_temporary_file(output_file)
    if plant.isfile(output_file) and not force_writting:
        output_file = os.path.basename(input_file)
        if not output_file.endswith('.vrt'):
            output_file += '.vrt'
    if plant.isfile(output_file) and not force_writting:
        # print(f'*** exiting here because {output_file} already exists')
        return output_file
    # if not plant.test_gdal_open(input_file):
    #     print('*** render_vrt_isce')

    if context is None:
        image_obj = read_image(input_file,
                               edges_outer=edges_outer,
                               edges_center=edges_center,
                               only_header=True)
    else:
        image_obj = context.read_image(input_file,
                                       edges_outer=edges_outer,
                                       edges_center=edges_center,
                                       only_header=True)
    save_image(image_obj,
               output_file,
               # output_reference_file=input_file,
               force=force,
               output_format='VRT',
               verbose=verbose)
    return output_file
    '''
    except:
        pass

    render_vrt_isce(input_file,
                    output_file=output_file,
                    force_writting=force_writting,
                    edges_outer=edges_outer,
                    edges_center=edges_center)
    return output_file
    '''


# from Image.py (ISCE)
def render_vrt_isce(input_file,
                    output_file=None,
                    force_writting=False,
                    edges_outer=None,
                    edges_center=None):
    '''
    renders GDAL .vrt header for an input file
    '''
    if not output_file:
        output_file = input_file
        if not output_file.endswith('.vrt'):
            output_file += '.vrt'
    if plant.isfile(output_file) and not force_writting:
        return output_file
    image_obj = read_image(input_file,
                           edges_outer=edges_outer,
                           edges_center=edges_center,
                           only_header=True)

    # orderMap = {'L' : 'LSB',
    #             'B' : 'MSB'}
    geotransform = image_obj.geotransform
    root = ET.Element('VRTDataset')
    root.attrib['rasterXSize'] = str(image_obj.width)
    root.attrib['rasterYSize'] = str(image_obj.length)
    if geotransform is not None:
        if plant.valid_coordinates(geotransform=geotransform,
                                   lat_size=image_obj.length,
                                   lon_size=image_obj.width):
            ET.SubElement(root, 'SRS').text = "EPSG:4326"
            geotransform_edges = \
                plant.geotransform_centers_to_edges(geotransform,
                                                    lat_size=image_obj.length,
                                                    lon_size=image_obj.width)
            geotransform_edges[5] = -geotransform_edges[5]
            geotransform_edges[3] = geotransform_edges[7]
            geotransform_edges = geotransform_edges[:6]
            geotransform_edges = [str(i) for i in geotransform_edges]
            ET.SubElement(root, 'GeoTransform').text = \
                ', '.join(geotransform_edges)
            if image_obj.projection is not None:
                ET.SubElement(root, 'Projection').text = image_obj.projection
    nbytes = plant.get_dtype_size(image_obj.dataType)
    for band in range(image_obj.bands):
        broot = ET.Element('VRTRasterBand')
        broot.attrib['dataType'] = plant.get_vrt_dtype(image_obj.dataType)
        broot.attrib['band'] = str(band + 1)
        broot.attrib['subClass'] = "VRTRawRasterBand"
        elem_parent = ET.SubElement(broot, 'SimpleSource')
        elem = ET.SubElement(elem_parent, 'SourceFilename')
        elem.attrib['relativeToVRT'] = "1"

        elem.text = image_obj.getFilename()
        if image_obj.scheme.upper() == 'BIL':
            ET.SubElement(broot, 'ImageOffset').text = \
                str(band * image_obj.width * nbytes)
            ET.SubElement(broot, 'PixelOffset').text = str(nbytes)
            ET.SubElement(broot, 'LineOffset').text = \
                str(image_obj.bands * image_obj.width * nbytes)
        elif image_obj.scheme.upper() == 'BIP':
            ET.SubElement(broot, 'ImageOffset').text = str(band * nbytes)
            ET.SubElement(broot, 'PixelOffset').text = \
                str(image_obj.bands * nbytes)
            ET.SubElement(broot, 'LineOffset').text = \
                str(image_obj.bands * image_obj.width * nbytes)
        elif image_obj.scheme.upper() == 'BSQ':
            ET.SubElement(broot, 'ImageOffset').text = \
                str(band * image_obj.width * image_obj.length)
            ET.SubElement(broot, 'PixelOffset').text = str(nbytes)
            ET.SubElement(broot, 'LineOffset').text = \
                str(image_obj.width * nbytes)
        root.append(broot)
    plant.indent_XML(root)
    tree = ET.ElementTree(root)
    tree.write(output_file, encoding='unicode')
    return output_file


# based on(isce2gis.py (ISCE)
def generate_vrt_with_geolocation_files(infile,
                                        lat_file,
                                        lon_file,
                                        flag_temporary=False,
                                        verbose=True):
    
    # print('*** locals (generate)', locals())
    # img = read_image(infile)
    # width = img.getWidth()
    # length = img.getLength()
    # print('*** generate_vrt_with_geolocation_files: ',
    # lat_file, lon_file)
    lat_dict = plant.parse_filename(lat_file)
    lat_filename = lat_dict['filename']
    lat_band = lat_dict['band']
    if lat_band is None:
        lat_band = 0
    lat_band += 1

    lon_dict = plant.parse_filename(lon_file)
    lon_filename = lon_dict['filename']
    lon_band = lon_dict['band']
    if lon_band is None:
        lon_band = 0
    lon_band += 1

    '''
    if (not lat_filename.endswith('.vrt') and
            plant.isfile(lat_filename+'.vrt')):
        lat_filename = lat_filename+'.vrt'
    elif not lat_filename.endswith('.vrt'):
        lat_filename = render_vrt(lat_filename,
                                  flag_temporary=flag_temporary)
    if (lon_filename != lat_filename and
            not lon_filename.endswith('.vrt') and
            plant.isfile(lon_filename+'.vrt')):
        lon_filename = lon_filename+'.vrt'
    elif (lon_filename != lat_filename and
          not lon_filename.endswith('.vrt')):
        lon_filename = render_vrt(lon_filename,
                                  flag_temporary=flag_temporary)
    '''
    infile_vrt = render_vrt(infile,
                            flag_temporary=flag_temporary)

    tree = ET.parse(infile_vrt)
    root = tree.getroot()

    update_metadata = True
    # remove existing pair {domain: GEOLOCATION} metadata
    for metadata in root.findall('metadata'):
        if len(metadata) == 0:
            root.remove(metadata)
            continue
        meta_dict = metadata.attrib
        flag_found = False
        for key, value in meta_dict.items():
            if key == 'domain' and value == 'GEOLOCATION':
                flag_found = True
        x_dataset = None
        y_dataset = None
        x_band = None
        y_band = None
        for mdi in metadata.findall('mdi'):  # .text
            if mdi.attrib.get('key', '') == 'X_DATASET':
                x_dataset = mdi.text
            if mdi.attrib.get('key', '') == 'Y_DATASET':
                y_dataset = mdi.text
            if mdi.attrib.get('key', '') == 'X_BAND':
                x_band = int(mdi.text)
            if mdi.attrib.get('key', '') == 'Y_BAND':
                y_band = int(mdi.text)
        same_lat_file = (os.path.abspath(lat_filename) == y_dataset and
                         lat_band == y_band)
        same_lon_file = (os.path.abspath(lon_filename) == x_dataset and
                         lon_band == x_band)
        update_metadata = (not same_lat_file or not same_lon_file)
        if flag_found and update_metadata:
            root.remove(metadata)

    ret_dict = {}
    ret_dict['lat_file'] = lat_filename
    ret_dict['lon_file'] = lon_filename
    ret_dict['data_file'] = infile_vrt
    if not update_metadata:
        return ret_dict

    meta = ET.SubElement(root, 'metadata')
    meta.attrib['domain'] = "GEOLOCATION"
    meta.tail = '\n'
    meta.text = '\n    '
    rdict = {'Y_DATASET': os.path.abspath(lat_filename),
             'X_DATASET': os.path.abspath(lon_filename),
             'X_BAND': f"{lon_band}",
             'Y_BAND': f"{lat_band}",
             'PIXEL_OFFSET': "0",
             'LINE_OFFSET': "0",
             'LINE_STEP': "1",
             'PIXEL_STEP': "1"}
    for key, val in rdict.items():
        data = ET.SubElement(meta, 'mdi')
        data.text = val
        data.attrib['key'] = key
        data.tail = '\n    '
    data.tail = '\n'
    # print('*** tree: ', tree)
    try:
        tree.write(infile_vrt)
        if plant.isfile(infile_vrt) and verbose:
            print(f'## file saved: {infile_vrt}')
        return ret_dict
    except:
        pass


def save_vector(data_vect,
                output_file,
                lat_vect=None,
                lon_vect=None,
                lat_arr=None,
                lon_arr=None,
                step_lat=None,
                step_lon=None,
                footprint=None,
                footprint_array=None,
                str_data='data',
                save_ndata=False,
                save_as_text=False,
                save_as_vector=False,
                save_as_raster=False,
                save_as_raster_gdal=False,
                loreys_height=False,
                sum_samples=False,
                id_vect=None,
                str_id='id',
                lat_text='lat',
                lon_text='lon',
                nbands=None,
                geotransform=None,
                output_scheme='',
                descr='',
                cmap=None,
                cmap_crop_min=None,
                cmap_crop_max=None,
                cmap_min=None,
                cmap_max=None,
                background_color=None,
                in_null=None,
                out_null=None,
                output_dtype=None,
                output_format=None,
                projection=None,

                verbose=True,
                force=None):

    output_data = None
    if (geotransform is not None or
        (lat_arr is not None and
         lon_arr is not None and
         step_lat is not None and
         step_lon is not None)):
        ret_dict = plant.get_coordinates(geotransform=geotransform,
                                         lat_arr=lat_arr,
                                         lon_arr=lon_arr,
                                         step_lat=step_lat,
                                         step_lon=step_lon)
        if ret_dict is not None:
            lat_arr = ret_dict['lat_arr']
            lon_arr = ret_dict['lon_arr']
            step_lat = ret_dict['step_lat']
            step_lon = ret_dict['step_lon']
            # lat_size = ret_dict['lat_size']
            # lon_size = ret_dict['lon_size']
        if lat_arr is None:
            flag_lat_arr_from_lat_vect = True
        else:
            flag_lat_arr_from_lat_vect = (plant.isnan(lat_arr[0]) or
                                          plant.isnan(lat_arr[1]))
        if (flag_lat_arr_from_lat_vect and
                lat_vect is not None and
                lon_vect is not None):
            lat_arr = [np.min(lat_vect), np.max(lat_vect)]
        if lon_arr is None:
            flag_lon_arr_from_lon_vect = True
        else:
            flag_lon_arr_from_lon_vect = (plant.isnan(lon_arr[0]) or
                                          plant.isnan(lon_arr[1]))
        if flag_lon_arr_from_lon_vect:
            lon_arr = [np.min(lon_vect), np.max(lon_vect)]
        if step_lat is None and footprint is not None:
            if plant.isvalid(footprint) and footprint != 0:
                step_lat = footprint/2
        if step_lon is None and footprint is not None:
            if plant.isvalid(footprint) and footprint != 0:
                step_lon = footprint/2
        geotransform = plant.get_geotransform(lat_arr=lat_arr,
                                              lon_arr=lon_arr,
                                              step_lat=step_lat,
                                              step_lon=step_lon)

    # data_vect = np.asarray(data_vect)
    # lat_vect = np.asarray(lat_vect)
    # lon_vect = np.asarray(lon_vect)

    #    print('# valid points total: '+str(len(data_vect)))
    #    print('mean %s: %f'
    #          % (str_data, np.nanmean(data_vect)))

    image_obj = plant.PlantImage(data_vect,
                                 nbands=nbands)

    if output_file.upper().startswith('MEM:'):
        output_key = 'OUTPUT:'+output_file[4:]
        # print('*** image_obj.filename_orig 2: ', image_obj.filename_orig)
        # print('*** image_obj.filename 2: ', image_obj.filename)
        plant.plant_config.variables[output_key] = image_obj
        return

    if not os.path.isdir(plant.dirname(output_file)):
        try:
            os.makedirs(plant.dirname(output_file))
        except FileExistsError:
            pass
        #   os.makedirs(os.path.dirname(output_file))

    data_vect = image_obj.image_list
    nbands = len(data_vect)

    if (isinstance(str_data, list) and
            len(str_data) == len(data_vect)):
        str_data_list = str_data
    elif (isinstance(str_data, str) and
            len(data_vect) == 1):
        str_data_list = [str_data]
    else:
        str_data_list = [str_data+'_band_%d' % b
                         for b in range(len(data_vect))]

    if ((save_as_raster or save_as_raster_gdal) and
            (lat_vect is None or lon_vect is None)):
        print('ERROR lat or lon vector were not '
              'provided to generate output image')
        return
    elif save_as_raster:
        if verbose:
            print('save vector mode: raster (PLAnT)')
        ret = rasterize_with_footprint(data_vect,
                                       lat_vect,
                                       lon_vect,
                                       lat_arr,
                                       lon_arr,
                                       step_lat,
                                       step_lon,
                                       nbands=nbands,
                                       footprint=footprint,
                                       footprint_array=footprint_array,
                                       out_null=out_null,
                                       loreys_height=loreys_height,
                                       sum_samples=sum_samples,
                                       verbose=verbose)
        data_list, ndata, geotransform = ret
        if save_ndata:
            # save density of valid points
            save_image(ndata,
                       output_file+'_ndata',
                       geotransform=geotransform,
                       output_scheme=output_scheme,
                       descr=descr,
                       cmap=cmap,
                       cmap_crop_min=cmap_crop_min,
                       cmap_crop_max=cmap_crop_max,
                       cmap_min=cmap_min,
                       cmap_max=cmap_max,
                       background_color=background_color,
                       in_null=in_null,
                       out_null=out_null,
                       output_dtype=output_dtype,
                       output_format=output_format,
                       projection=projection,
                       verbose=verbose,
                       force=force)
        # save data
        save_image(data_list,
                   output_file,
                   output_format=output_format,
                   force=force,
                   verbose=verbose,
                   # dtype=dtype,
                   output_dtype=output_dtype,
                   geotransform=geotransform)
    elif save_as_raster_gdal:
        if verbose:
            print('save vector mode: raster (gdal_grid)')
        rasterize_with_footprint_gdal(data_vect,
                                      lat_vect,
                                      lon_vect,
                                      output_file,
                                      lat_arr,
                                      lon_arr,
                                      step_lat,
                                      step_lon,
                                      footprint,
                                      str_data=str_data_list,
                                      lat_text=lat_text,
                                      lon_text=lon_text,
                                      output_format=output_format,
                                      output_dtype=output_dtype,
                                      out_null=out_null,
                                      verbose=verbose,
                                      force=force)
    elif (save_as_text or
          ((output_file.endswith('.csv') or
            output_file.endswith('.txt')) and
           not save_as_raster_gdal and not
           save_as_vector)):
        # if verbose:
        #     print('save vector mode: text')
        args_vects = []
        args_labels = []
        if (lat_vect is not None):
            args_vects += [lat_vect]
            args_labels += [lat_text]
        if (lon_vect is not None):
            args_vects += [lon_vect]
            args_labels += [lon_text]
        if (id_vect is not None):
            args_vects += [id_vect]
            args_labels += [str_id]
        args_vects += data_vect
        args_labels += str_data_list
        args = args_vects + args_labels + [output_file]
        save_text(*args,
                  verbose=verbose,
                  force=force)
    else:
        # if verbose:
        # print('save vector mode: extension default or binary')

        kwargs = {}
        kwargs['output_scheme'] = output_scheme
        kwargs['descr'] = descr
        kwargs['cmap'] = cmap
        kwargs['cmap_crop_min'] = cmap_crop_min
        kwargs['cmap_crop_max'] = cmap_crop_max
        kwargs['cmap_min'] = cmap_min
        kwargs['cmap_max'] = cmap_max
        kwargs['background_color'] = background_color
        kwargs['in_null'] = in_null
        kwargs['out_null'] = out_null
        kwargs['output_dtype'] = output_dtype
        kwargs['output_format'] = output_format
        kwargs['projection'] = projection
        kwargs['verbose'] = verbose
        kwargs['force'] = force
        # geotransform=geotransform,
        new_image_obj = plant.PlantImage(data_vect)
        # new_image_obj.realize_changes()
        if FLAG_VECTOR_SAVE_LAT_LON_BANDS:
            if lat_vect is not None:
                new_image_obj.add_band(lat_vect,
                                       name=plant.Y_BAND_NAME)
                nbands += 1
            if lon_vect is not None:
                new_image_obj.add_band(lon_vect,
                                       name=plant.X_BAND_NAME)
                nbands += 1
            kwargs['nbands'] = nbands
            save_image(new_image_obj, output_file, **kwargs)
        else:
            if lat_vect is not None:
                lat_vect_obj = plant.PlantImage(lat_vect)
                lat_vect_obj.get_band().name = 'lat/north'
                lat_file = os.path.join(os.path.dirname(output_file),
                                        'lat.rdr')
                save_image(lat_vect_obj, lat_file, **kwargs)
            if lon_vect is not None:
                lon_vect_obj = plant.PlantImage(lon_vect)
                lon_vect_obj.get_band().name = 'lon/east'
                lon_file = os.path.join(os.path.dirname(output_file),
                                        'lon.rdr')
                save_image(lon_vect_obj, lon_file, **kwargs)
            kwargs['nbands'] = nbands
            save_image(new_image_obj, output_file, **kwargs)
        return new_image_obj
    return output_data


def get_output_format(output_file, output_format=None):

    if output_format is not None and 'tif' in output_format.lower():
        output_format = 'GTiff'
        if output_format in plant.OUTPUT_FORMAT_MAP.keys():
            output_format = plant.OUTPUT_FORMAT_MAP[output_format]
        return output_format

    if output_format is not None and 'ANN' not in output_format:
        if output_format in plant.OUTPUT_FORMAT_MAP.keys():
            output_format = plant.OUTPUT_FORMAT_MAP[output_format]
        return output_format
    if not output_file:
        return
    filename, extension = os.path.splitext(output_file)
    # extension = output_file.split('.')[-1]
    extension = extension.lower()
    if extension and extension.startswith('.'):
        extension = extension[1:]
    if (extension == 'tif' or extension == 'tiff'):
        output_format = 'GTiff'
    elif (extension == 'bin'):
        output_format = 'ENVI'  # 'ISCE'
    elif (extension == 'txt' or extension == 'csv'):
        output_format = 'CSV'
    elif extension.upper() in plant.FIG_DRIVERS:
        output_format = extension.upper()
    elif (extension == 'kml'):
        output_format = 'KML'
    elif (extension == 'kmz'):
        output_format = 'KMZ'
    elif (extension == 'npy'):
        output_format = 'NUMPY'
    elif (extension == 'vrt'):
        output_format = 'VRT'
    elif (extension == 'htm' or extension == 'html'):
        output_format = 'HTML'
    else:
        output_format = plant.DEFAULT_OUTPUT_FORMAT
    if output_format in plant.OUTPUT_FORMAT_MAP.keys():
        output_format = plant.OUTPUT_FORMAT_MAP[output_format]
    return output_format


def rasterize_with_footprint_gdal(data_vect,
                                  lat_vect,
                                  lon_vect,
                                  output_file,
                                  lat_arr=None,
                                  lon_arr=None,
                                  step_lat=None,
                                  step_lon=None,
                                  footprint=None,
                                  max_points=10,
                                  out_null='nan',
                                  output_format=None,
                                  output_dtype=None,
                                  str_data='data',
                                  lat_text='lat',
                                  lon_text='lon',
                                  nbands=None,
                                  csv_file=None,
                                  verbose=True,
                                  force=None):

    update_image = plant.overwrite_file_check(output_file,
                                              force=force)
    if not update_image:
        print('operation cancelled.')
        sys.exit(0)

    timestamp = str(int(time.time()))

    image_obj = plant.PlantImage(data_vect,
                                 nbands=nbands)
    data_vect = image_obj.image_list
    nbands = len(data_vect)

    if (isinstance(str_data, list) and
            len(str_data) == nbands):
        str_data_list = str_data
    elif (isinstance(str_data, str) and
            nbands == 1):
        str_data_list = [str_data]
    else:
        str_data_list = [str_data+'_band_%d' % b
                         for b in range(len(data_vect))]

    if csv_file is not None and not plant.isfile(csv_file):
        print('ERROR file not found: %s'
              % csv_file)
        return
    elif csv_file is None:
        csv_file = output_file+'_temp_'+timestamp+'.csv'
        args_vects = []
        args_labels = []
        if (lat_vect is not None):
            args_vects += [lat_vect]
            args_labels += [lat_text]
        if (lon_vect is not None):
            args_vects += [lon_vect]
            args_labels += [lon_text]
        # if (id_vect is not None):
        #    args_vects += [id_vect]
        #    args_labels += [str_id]

        args_vects += data_vect
        args_labels += str_data_list
        args = args_vects + args_labels + [csv_file]
        save_text(*args,
                  verbose=verbose,
                  force=force)

        if not plant.isfile(csv_file):
            print('ERROR file could not be generated: ' +
                  csv_file)
            return
        plant.append_temporary_file(csv_file)

    vrt_file = output_file+'_temp_'+timestamp+'.vrt'
    root = ET.Element('OGRVRTDataSource')

    for b in range(nbands):
        elem = ET.SubElement(root, 'OGRVRTLayer')
        elem.attrib['name'] = 'l'+str_data_list[b]
        ET.SubElement(elem, 'SrcDataSource').text = 'CSV:'+csv_file
        ET.SubElement(elem, 'GeometryType').text = 'wkbPoint'
        # ET.SubElement(elem, 'SrcLayer').text = csv_file.replace('.csv',
        #                                                         '')
        # ET.SubElement(elem, 'LayerSRS').text = 'EPSG:4919'
        ET.SubElement(elem, 'SrcLayer').text = \
            os.path.basename(csv_file).replace('.csv', '')
        ET.SubElement(elem, 'LayerSRS').text = 'WGS84'
        elem_sub = ET.SubElement(elem, 'GeometryField')
        elem_sub.attrib['separator'] = ','
        elem_sub.attrib['encoding'] = 'PointFromColumns'
        elem_sub.attrib['x'] = lon_text
        elem_sub.attrib['y'] = lat_text
        elem_sub.attrib['z'] = str_data_list[b]

    plant.indent_XML(root)
    tree = ET.ElementTree(root)
    # tree.text = "\n    "
    # tree.tail = "\n      "
    tree.write(vrt_file, encoding='unicode')
    plant.append_temporary_file(vrt_file)

    if verbose:
        print('file saved (temp): %s (VRT)' % (vrt_file))

    # format
    if output_format is None:
        output_format = get_output_format(output_file,
                                          output_format=output_format)
    if output_format is None:
        output_format = 'GTiff'

    # outputType
    if output_dtype is not None:
        outputType = plant.get_gdal_dtype_from_np(output_dtype)
    else:
        outputType = plant.get_gdal_dtype_from_np(np.float32)

    # algorithm
    if out_null is None:
        out_null = 'nan'
    algorithm = f'invdistnn:nodata={out_null}'
    if footprint is not None:
        if plant.isvalid(footprint) and footprint != 0:
            footprint_radius = footprint/2
            algorithm += (':radius=%.15f'
                          % (footprint_radius))
    if max_points is not None:
        algorithm += ':max_points=%d' % max_points

    # outputBounds
    if (lat_arr is not None and
            lon_arr is not None and
            plant.isvalid(step_lat) and
            plant.isvalid(step_lon)):
        lat_size = round(float(lat_arr[1]-lat_arr[0])/step_lat+1)
        lon_size = round(float(lon_arr[1]-lon_arr[0])/step_lon+1)
        outputBounds = [lon_arr[0]-step_lon/2,
                        lat_arr[0]-step_lat/2,
                        lon_arr[1]+step_lon/2,
                        lat_arr[1]+step_lat/2]
    else:
        outputBounds = None
        lon_size = None
        lat_size = None
    # command_suffix += ' -outsize %d %d ' % (lon_size, lat_size)

    for b in range(nbands):
        if nbands == 1:
            output_file_temp = ('%s_temp_out_%s.tif'
                                % (output_file, timestamp))
        else:
            output_file_temp = ('%s_temp_out_band_%d_%s.tif'
                                % (output_file, b, timestamp))
        plant.append_temporary_file(output_file_temp)


        
        ret_obj = plant.Grid(output_file_temp, vrt_file,
                             flag_return=True,
                             format=output_format,
                             outputType=outputType,
                             width=lon_size,
                             height=lat_size,
                             outputBounds=outputBounds,
                             algorithm=algorithm,

                             layers=f'l{str_data_list[b]}')
        # if not plant.isfile(output_file_temp):
        #     print('ERROR generating file: '+output_file_temp)
        #     return
        if b == 0:
            image_obj = ret_obj
            image = image_obj.image
        else:
            image = ret_obj.image
        image = image[::-1, :]
        image_obj.set_image(image, band=b)

    save_image(image_obj,
               output_file,
               # out_null=out_null,
               output_format=output_format,
               output_dtype=output_dtype,
               verbose=verbose,
               force=force)
    # if not keep_temporary and plant.isfile(output_file_temp):
    #     remove(output_file_temp)
    # elif verbose:
    plant.plant_config.output_files.append(output_file)
    if verbose:
        print('## file saved: %s (GDAL_GRID)' % output_file)


def get_images_from_list(input_files,
                         parameters_list=None,
                         invalid_list=None,
                         plant_transform_obj=None,
                         flag_exit_if_error=True,
                         flag_no_messages=False,
                         input_format=None,
                         verbose=True,
                         **kwargs):
    input_images = []
    if parameters_list is not None:
        new_parameters_list = []
    for i, current_file in enumerate(input_files):
        current_file_orig = current_file
        if (test_valid_number_sequence(current_file) or
                test_gdal_open(current_file)):
            input_images.append(current_file)
            if parameters_list is not None:
                new_parameters_list.append(parameters_list[i])
            continue
        elif plant.IMAGE_NAME_SEPARATOR in current_file:
            # print('*** before: ', current_file_orig)
            ret_dict = parse_filename(current_file_orig)
            # print('*** after: ', current_file_orig)
            current_file = ret_dict['filename']
            if 'filename_position' in ret_dict.keys():
                filename_position = ret_dict['filename_position']
            else:
                filename_position = 0
            if 'driver' in ret_dict.keys() and ret_dict['driver'] == 'MEM':
                image_obj = plant.plant_config.variables[current_file]
                if image_obj is not None:
                    input_images.append(current_file_orig)
                    if parameters_list is not None:
                        new_parameters_list.append(parameters_list[i])
                elif invalid_list is not None:
                    plant.handle_exception(
                        'opening file %s' % (current_file_orig),
                        flag_exit_if_error=flag_exit_if_error,
                        flag_no_messages=flag_no_messages,
                        verbose=verbose)
                    invalid_list.append(current_file_orig)
                continue
                # return image_obj
            current_file_splitted = \
                current_file_orig.split(plant.IMAGE_NAME_SEPARATOR)
        else:
            current_file_splitted = None
        '''
            if len(current_file_splitted) > 2:
                current_file = current_file_splitted[1]
            else:
                current_file = current_file_splitted[0]
        '''
        filelist_expanded = plant.glob(current_file)
        if len(filelist_expanded) == 0:
            plant.handle_exception('file not found %s'
                                   % (current_file_orig),
                                   flag_exit_if_error=flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            if invalid_list is not None:
                invalid_list.append(current_file_orig)
        for f in filelist_expanded:
            if f in input_images:
                continue
            if current_file_splitted is not None:
                current_file_splitted[filename_position] = f
            if plant.IMAGE_NAME_SEPARATOR in current_file_orig:
                '''
                if len(current_file_splitted) > 2:
                    current_file_splitted[1] = f
                else:
                    current_file_splitted[0] = f
                f = plant.IMAGE_NAME_SEPARATOR.join(current_file_splitted)
                '''
                f = get_filename_from_dict(ret_dict, filename=f)
            kwargs['only_header'] = True
            kwargs['verbose'] = False
            kwargs['plant_transform_obj'] = plant_transform_obj
            if input_format is not None:
                kwargs['input_format'] = input_format
            kwargs['flag_exit_if_error'] = False
            image_obj = read_image(f, **kwargs)
            if image_obj is not None:
                if current_file_splitted is not None:
                    input_images.append(plant.IMAGE_NAME_SEPARATOR.join(
                        current_file_splitted))
                else:
                    input_images.append(f)
                if parameters_list is not None:
                    new_parameters_list.append(parameters_list[i])
            elif invalid_list is not None:
                plant.handle_exception(
                    'opening file %s' % (current_file_orig),
                    flag_exit_if_error=flag_exit_if_error,
                    flag_no_messages=flag_no_messages,
                    verbose=verbose)
                invalid_list.append(f)
    if parameters_list is None:
        return input_images
    return(input_images, new_parameters_list)


def get_files_from_list(self,
                        input_files,
                        parameters_list=None,
                        verbose=False,
                        flag_no_messages=False,
                        invalid_list=None):
    file_list = []
    if parameters_list is not None:
        new_parameters_list = []
    for i, current_file in enumerate(input_files):
        current_file_orig = current_file
        if (test_valid_number_sequence(current_file) or
                test_gdal_open(current_file)):
            file_list.append(current_file)
            if parameters_list is not None:
                new_parameters_list.append(parameters_list[i])
            continue
        elif plant.IMAGE_NAME_SEPARATOR in current_file:
            ret_dict = parse_filename(current_file_orig)
            current_file = ret_dict['filename']
            if 'driver' in ret_dict.keys() and ret_dict['driver'] == 'MEM':
                image_obj = plant.plant_config.variables[current_file]
                if image_obj is not None:
                    file_list.append(current_file_orig)
                    if parameters_list is not None:
                        new_parameters_list.append(parameters_list[i])
                elif invalid_list is not None:
                    if not flag_no_messages:
                        print('WARNING error reading file.'
                              ' Ignoring: %s' % (current_file_orig))
                    invalid_list.append(current_file_orig)
                continue
        filelist_expanded = plant.glob(current_file)
        if len(filelist_expanded) == 0 and not flag_no_messages:
            print('WARNING file not found.'
                  ' Ignoring: %s' % (current_file_orig))
        if len(filelist_expanded) == 0 and invalid_list is not None:
            invalid_list.append(current_file_orig)
        for f in filelist_expanded:
            if f in file_list:
                continue
            if plant.IMAGE_NAME_SEPARATOR in current_file_orig:
                f = get_filename_from_dict(ret_dict, filename=f)
            image_obj = self.read_image(f,
                                        only_header=True,
                                        verbose=verbose,
                                        flag_no_messages=flag_no_messages,
                                        flag_exit_if_error=False)
            if (image_obj is not None or
                    (image_obj is None and os.path.isfile)):
                file_list.append(f)
                if parameters_list is not None:
                    new_parameters_list.append(parameters_list[i])
            elif invalid_list is not None and not flag_no_messages:
                print('WARNING error reading file. '
                      'Ignoring: %s' % (current_file_orig))
                invalid_list.append(f)
    if parameters_list is None:
        return file_list
    else:
        return(file_list, new_parameters_list)


def create_config_txt(txt_file,
                      input_file='',
                      polar_type='full',
                      polar_case='monostatic',
                      width=None,
                      length=None,
                      force=None,
                      verbose=True):
    '''
    create config.txt for non-ISCE images
    '''
    ret = plant.overwrite_file_check(txt_file,
                                     force=force)
    if not ret and verbose:
        print('WARNING file not updated: ' + txt_file,
              1)
    else:
        with open(txt_file, 'w') as f:
            if width is None or length is None:
                image_obj = read_image(input_file,
                                       only_header=True)
                length = image_obj.length
                width = image_obj.width
            f.write('Nrow\n')
            f.write('%d\n' % (length))
            f.write('---------\n')
            f.write('Ncol\n')
            f.write('%d\n' % (width))
            f.write('---------\n')
            f.write('PolarCase\n')
            f.write(polar_case+'\n')
            f.write('---------\n')
            f.write('PolarType\n')
            f.write(polar_type+'\n')


def get_info_from_config_txt(filename,
                             verbose=False,
                             flag_no_messages=False,
                             flag_exit_if_error=True):
    '''
    get information from file using config.txt
    '''
    directory = os.path.dirname(filename)
    if not directory:
        directory = '.'
    config_file = os.path.join(directory, 'config.txt')
    nbands_orig = 1
    if not plant.isfile(config_file):
        # print('Warning file not found: '+config_file)
        return
    with open(config_file, 'r') as f:
        config_from_file = f.read()
        config_from_file = config_from_file.upper()
        config_from_file = config_from_file.split('\n')
        try:
            ncol_index = config_from_file.index('NCOL')
            width_orig = int(config_from_file[ncol_index+1])
        except:
            width_orig = None
        try:
            nrow_index = config_from_file.index('NROW')
            length_orig = int(config_from_file[nrow_index+1])
        except:
            length_orig = None
        if width_orig is not None and length_orig is not None:
            try:
                file_size = os.stat(os.path.realpath(filename)).st_size
            except FileNotFoundError:
                plant.handle_exception(f'file not found: {filename}'
                                       f' (config.txt)',
                                       flag_exit_if_error,
                                       flag_no_messages=flag_no_messages,
                                       verbose=verbose)
                return
            type_size = file_size // (width_orig*length_orig)
            if type_size == 1:
                dtype = np.byte
            elif type_size == 2:
                dtype = np.int16
            elif type_size == 8:
                dtype = np.complex64
            elif type_size == 16:
                dtype = np.complex128
            else:
                dtype = np.float32
        else:
            dtype = None
        ext = 'bin'
        image_obj = plant.PlantImage(filename=filename,
                                     width_orig=width_orig,
                                     length_orig=length_orig,
                                     dtype=dtype,
                                     nbands_orig=nbands_orig,
                                     file_format=ext.upper())
        return image_obj


def get_envi_header(filename):
    if not filename:
        return
    if filename.endswith('.hdr'):
        return filename
    basename = os.path.basename(filename)
    if '.' not in basename:
        return(filename+'.hdr')
    new_filename = os.path.join(os.path.dirname(filename),
                                '.'.join(basename.split('.')[0:-1])+'.hdr')
    return new_filename


def get_info_from_envi_header(metadata, filename=None):
    """
    determines  image name, width, image type and data type from ENVI
    header
    """
    if not metadata and not filename:
        return
    if not metadata:
        metadata = filename
    image_hdr = get_envi_header(metadata)
    ret = plant.read_parameters_text_file(image_hdr)
    if ret is None:
        return
    filename = filename if filename else metadata
    filename = filename.replace('.hdr', '')
    scheme = ret['interleave']
    width = int(ret['samples'])
    length = int(ret['lines'])
    nbands = len(ret['bands'])
    dtype = int(ret['data type'])
    switcher = {
        1: '-b1',
        2: '-i2',
        3: '-i4',
        4: '-r4',
        5: '-r8',
        6: '-c8',
        9: '-c16',
        12: '-i2',  # unsigned
        13: '-i4',  # unsigned
        14: '-i8',
        15: '-i8',  # unsigned
    }
    dtype = switcher.get(dtype, '-r4')
    image_obj = plant.PlantImage(filename=filename,
                                 width=width,
                                 length=length,
                                 dtype=dtype,
                                 scheme=scheme,
                                 file_format='ENVI',
                                 nbands=nbands)
    return image_obj


def get_envi_boundaries(hdr):
    '''
    get boundaries form the ENVI header
    '''
    with open(hdr) as f:
        envi_header = f.read()
    lines = envi_header.split('\n')
    for current_line in lines:
        try:
            parameter, value = current_line.split('=')
            if parameter.strip() == 'lat_min':
                lat_min = float(value)
            if parameter.strip() == 'lat_max':
                lat_max = float(value)
            if parameter.strip() == 'lon_min':
                lon_min = float(value)
            if parameter.strip() == 'lon_max':
                lon_max = float(value)
        except:
            pass
    return lat_min, lat_max, lon_min, lon_max


def is_isce_image(filename):
    '''
    test if file is an ISCE image
    '''
    if (not plant.isfile(filename + '.xml') and
            not filename.endswith('.xml')):
        return False

    image_obj = read_image(filename,
                           # only_header=False,
                           only_header=True,
                           input_format='ISCE',
                           verbose=False,
                           flag_exit_if_error=False)

    # this should be improved
    return(image_obj is not None)


def test_for_gdal_convention_error(filename):
    '''
    '''
    # if is_isce_image(filename):
    #     return False
    # image_obj = read_image(filename, only_header=True)
    # image_format = image_obj.file_format
    return False
    # if ('TIF' not in image_format.upper()):
    #     # and 'ENVI' not in image_format.upper()):
    #     return False
    # geotransform = image_obj.geotransform

    # or 'ENVI' in image_format.upper()
    # return(geotransform[5] > 0)
    # return True


def read_matrix(*args, **kwargs):
    '''
    read matrix from file
    '''
    filename = kwargs.get('filename_orig', None)
    copy = kwargs.pop('copy', None)
    if isinstance(filename, plant.PlantImage):
        image_obj = filename
    else:
        image_obj = read_image(*args, **kwargs)
    if image_obj is None:
        return

    if copy:
        return np.copy(image_obj.image)
    return image_obj.image


def sample_image(input_name,
                 sampling_step=None,
                 sampling_step_x=None,
                 sampling_step_y=None,
                 sampling_step_z=None,
                 fast=None,
                 only_header=None,
                 verbose=True,
                 band=None):
    if input_name is None:
        return
    kwargs = locals()
    geotransform = None
    if isinstance(input_name, str):
        image_obj = read_image(search_image(input_name)[0],
                               only_header=only_header,
                               band=band)
    elif isinstance(input_name, plant.PlantImage):
        image_obj = input_name
    else:
        image_obj = None
        depth, length, width = plant.get_image_dimensions(input_name)
    if image_obj is not None:
        # image_loaded = image_obj.image_loaded
        if image_obj.nbands > 1 and band is None:
            kwargs.pop('band', None)
            kwargs.pop('input_name', None)
            print(f'resampling image {image_obj.filename_orig}... ')
            with plant.PlantIndent():
                image_obj = sample_image(image_obj,
                                         band=0,
                                         **kwargs)
                for b in range(1, image_obj.nbands):
                    image = image_obj.get_image(band=b)
                    current_image = sample_image(image, **kwargs)
                    image_obj.set_image(current_image, band=b)
                return image_obj
        image = image_obj.get_image(band=band)
        width = image_obj.width
        length = image_obj.length
        depth = image_obj.depth
        # dtype = image_obj.dtype
        geotransform = image_obj.geotransform
    else:
        image = input_name
    image_loaded = image is not None

    if (sampling_step is None and
            sampling_step_x is None and
            sampling_step_y is None and
            sampling_step_z is None and
            fast):
        n_elements = 1
        if width is not None:
            n_elements *= width
        if length is not None:
            n_elements *= length
        if depth is not None:
            n_elements *= depth
        dtype = plant.get_dtype_name(image.dtype)
        type_size = plant.get_dtype_size(dtype)
        data_size = n_elements*type_size
        sampling_step = max([1, data_size//1e7])

    if sampling_step is None:
        sampling_step = 1
    # sampling_step = plant.demux_input(sampling_step, 3)
    if (sampling_step_x is None and
            isinstance(sampling_step, numbers.Number)):
        sampling_step_x = sampling_step
    elif (sampling_step_x is None and
          hasattr(sampling_step, '__getitem__')
          and len(sampling_step) >= 1):
        sampling_step_x = sampling_step[0]
    elif sampling_step_x is None:
        sampling_step_x = 1

    if (sampling_step_y is None and
            isinstance(sampling_step, numbers.Number)):
        sampling_step_y = sampling_step
    elif (sampling_step_y is None and
          hasattr(sampling_step, '__getitem__')
          and len(sampling_step) >= 2):
        sampling_step_y = sampling_step[1]
    elif sampling_step_y is None:
        sampling_step_y = 1

    if (sampling_step_z is None and
            isinstance(sampling_step, numbers.Number)):
        sampling_step_z = sampling_step
    elif (sampling_step_z is None and
          hasattr(sampling_step, '__getitem__')
          and len(sampling_step) >= 3):
        sampling_step_z = sampling_step[2]
    elif sampling_step_z is None:
        sampling_step_z = 1

    sampling_step_x = int(sampling_step_x)
    sampling_step_y = int(sampling_step_y)
    sampling_step_z = int(sampling_step_z)

    if (sampling_step_x == 1 and
            sampling_step_y == 1 and
            sampling_step_z == 1):
        return input_name

    if image_loaded and depth <= 1:
        # from PIL import Image
        # start_time = time.time()
        # new_image = Image.fromarray(image).thumbnail
        # ((image.shape[0]/sampling_step_x,
        #             image.shape[1]/sampling_step_y))
        # print('time: '+str(time.time()-start_time))
        new_image = image[::sampling_step_y,
                          ::sampling_step_x]
        # new_length = new_image.shape[0]
        # new_width = new_image.shape[1]
        # new_depth = 1
    elif image_loaded:
        new_image = image[::sampling_step_y,
                          ::sampling_step_x,
                          ::sampling_step_z]
        # new_length = new_image.shape[0]
        # new_width = new_image.shape[1]
        # new_depth = new_image.shape[2]
    else:
        new_width = int(np.ceil(float(width) /
                                sampling_step_x))
        new_length = int(np.ceil(float(length) /
                                 sampling_step_y))
        new_depth = int(np.ceil(float(depth) /
                                sampling_step_z))
    if image_loaded:
        new_depth, new_length, new_width = \
            plant.get_image_dimensions(new_image)

    if geotransform is not None:
        geotransform_new = geotransform[:]
        geotransform_new[1] = geotransform_new[1]*sampling_step_x
        geotransform_new[5] = geotransform_new[5]*sampling_step_y
        image_obj.set_geotransform(geotransform_new,
                                   realize_changes=False)

    '''

    if image_loaded and depth<=1:
        new_image = np.zeros((new_length, new_width),
                             dtype=dtype)
        for i in range(int(new_length)):
            line = image[i*sampling_step_y,
                         0::sampling_step_x]
            new_image[i, :] = line
    elif image_loaded:
        new_image = np.zeros((new_length,
                              new_width,
                              new_depth),
                             dtype=dtype)
        for i in range(int(new_length)):
            for j in range(int(new_width)):
                line = image[i*sampling_step_y,
                             j*sampling_step_x,
                             0::sampling_step_z]
                new_image[i, j, :] = line

    if geotransform is not None:
        geotransform_new = geotransform[:]
        geotransform_new[1] = geotransform_new[1]*(width/new_width)
        geotransform_new[5] = geotransform_new[5]*(length/new_length)
        image_obj.geotransform = geotransform_new
    '''
    if verbose and depth <= 1:
        print('new shape: (%d, %d)'
              % (new_length, new_width))
    elif verbose:
        print('new shape: (%d, %d, %d)'
              % (new_length, new_width, new_depth))
    if image_obj is not None:
        if image_loaded:
            image_obj.set_image(new_image, realize_changes=False)
        image_obj._length = new_length
        image_obj._width = new_width
        image_obj._depth = new_depth
        return image_obj
    # image_obj.dtype = dtype
    return new_image


def test_valid_number_sequence(filename):
    # if not isinstance(filename, str):
    #     return
    if (isinstance(filename, numbers.Number) or
            isinstance(filename, np.ndarray) or
            isinstance(filename, slice)):
        return True
    if isinstance(filename, list) or isinstance(filename, tuple):
        for element in filename:
            if not test_valid_number_sequence(element):
                return False
        return True
    filename_splitted = filename.split(plant.IMAGE_NAME_SEPARATOR)
    if (len(filename_splitted) >= 1 and
            # plant.isfile(filename_splitted[0])):
            os.path.isfile(filename_splitted[0])):
        return False
    elif (len(filename_splitted) > 2 and
            # plant.isfile(filename_splitted[1])):
            os.path.isfile(filename_splitted[1])):
        return False
    # returnall([len(np.fromstring(x, sep=plant.ELEMENT_SEPARATOR))
    #             for x in filename_splitted])
    filename_mult = filename.replace(plant.LINE_MULTIPLIER,
                                     plant.LINE_SEPARATOR)
    filename_splitted = filename_mult.split(plant.LINE_SEPARATOR)
    for line_str in filename_splitted:
        line_splitted = line_str.split(plant.IMAGE_NAME_SEPARATOR)
        for element in line_splitted:
            element_mult = element.replace(plant.ELEMENT_MULTIPLIER,
                                           plant.ELEMENT_SEPARATOR)
            element_splitted = element_mult.split(plant.ELEMENT_SEPARATOR)
            for number in element_splitted:
                if not plant.isnumeric(number):
                    return False
    return True


def test_gdal_open(filename, geo=False, parameters_dict=None):
    gdal.UseExceptions()
    gdal.ErrorReset()

    if parameters_dict is not None:
        parameters_dict['filename'] = filename
    if filename is None:
        return False

    if not test_valid_number_sequence(filename):
        ret_dict = parse_filename(filename)
        if (ret_dict is not None and 'driver' in ret_dict.keys() and
                'HDF5' == ret_dict['driver'].upper()):
            return False
        if (ret_dict is not None and 'filename' in ret_dict.keys() and
            'driver' in ret_dict.keys() and
                'NETCDF' not in ret_dict['driver']):
            try:
                _ = h5py.File(ret_dict['filename'], 'r')
                # flag_test_hdf5 = True
                # input_format = 'HDF5'
                return False
            except:
                # except (OSError, ValueError):
                pass

    dataset = None
    gdal.UseExceptions()
    gdal.ErrorReset()
    try:
        dataset = gdal.Open(filename)
    except:
        pass
    if (dataset is None and
            plant.IMAGE_NAME_SEPARATOR not in filename):
        # print('*** 1')
        dataset = None
        gdal.ErrorReset()
        return False
    if dataset is None:
        # print('*** 2')
        filename_splitted = filename.split(plant.IMAGE_NAME_SEPARATOR)
        filename_to_test = plant.IMAGE_NAME_SEPARATOR.join(
            filename_splitted[:-1])
        complement = filename_splitted[-1]
        test_parameters_dict = {}
        ret = test_gdal_open(filename_to_test, geo=geo,
                             parameters_dict=test_parameters_dict)
        if not ret or parameters_dict is None:
            dataset = None
            gdal.ErrorReset()
            return ret
        parameters_dict['filename'] = filename_to_test
        if test_parameters_dict['has_sub_datasets']:
            parameters_dict['key'] = complement
        else:
            parameters_dict['band'] = complement
        return ret
    if parameters_dict is not None:
        # print('*** 3')
        sub_datasets = dataset.GetSubDatasets()
        parameters_dict['has_sub_datasets'] = bool(sub_datasets)
    # print('*** 4')
    dataset = None
    gdal.ErrorReset()
    if geo:
        if not plant.valid_coordinates(bbox_file=filename):
            return False
    return True


def test_other_drivers(input_file, parameters_dict=None):
    input_file_splitted = input_file.split(':')
    # print(len(input_file_splitted) == 1)
    # print(mimetypes.guess_type((input_file_splitted[0]))[0])
    # print(input_file_splitted[0].upper())
    # print(plant.AVAILABLE_DRIVERS)
    if (len(input_file_splitted) == 1 and
         ' ' in input_file_splitted[0]):
        return test_other_drivers('UTIL:'+input_file,
                                  parameters_dict=parameters_dict)
        
    if (len(input_file_splitted) == 1 and
            mimetypes.guess_type((input_file_splitted[0]))[0] ==
            'text/plain'):
        return test_other_drivers('TEXT:'+input_file,
                                  parameters_dict=parameters_dict)

    if len(input_file_splitted) <= 1:
        return False
    driver = input_file_splitted[0].upper()
    flag_driver_found = driver in plant.AVAILABLE_DRIVERS
    if (not flag_driver_found and
            len(input_file_splitted) == 2 and
            driver not in plant.TEXT_DRIVERS):
        if (mimetypes.guess_type((input_file_splitted[0]))[0] ==
                'text/plain'):
            return test_other_drivers('CSV:'+input_file,
                                      parameters_dict=parameters_dict)

    if flag_driver_found and parameters_dict is not None:
        filename = input_file_splitted[1]
        filename_position = 1
        filename = filename.replace('"', '')
        filename = filename.replace("'", '')
        width = None
        length = None
        depth = None
        dtype = None
        key = None
        header_file = None
        if (driver.upper() == 'BIN'):
            dtype = 'float32'
            if len(input_file_splitted) >= 3:
                width = int(input_file_splitted[2])
            if (len(input_file_splitted) >= 4 and
                    plant.isnumeric(input_file_splitted[3])):
                length = int(input_file_splitted[3])
            if (len(input_file_splitted) >= 5 and
                    plant.isnumeric(input_file_splitted[4])):
                depth = int(input_file_splitted[4])
            last_element = input_file_splitted[
                len(input_file_splitted)-1]
            if not plant.isnumeric(last_element):
                try:
                    dtype = plant.get_dtype_name(
                        plant.get_np_dtype(last_element))
                    if dtype == 'NoneType':
                        dtype = None
                except:
                    dtype = None
        elif ('SENTINEL' in driver.upper()):
            header_file = input_file_splitted[2]
            if len(input_file_splitted) > 3:
                key = input_file_splitted[3]
            '''
        elif ('NISAR' in driver.upper()):
            # header = filename
            key = input_file_splitted[2].strip()
            if key.upper() == 'B' or key == '2':
                key = 'B'
            elif key.upper() == 'A' or key == '1':
                key = 'A'
            '''
        elif len(input_file_splitted) > 2:
            key = input_file_splitted[2]
            key = key.replace('//', '')
        parameters_dict['driver'] = driver
        # if os.path.isfile(filename):
        if len(glob_glob(filename)) > 0:
            parameters_dict['filename'] = glob_glob(filename)[0]
        else:
            parameters_dict['filename'] = filename
        parameters_dict['filename_position'] = filename_position
        parameters_dict['width'] = width
        parameters_dict['length'] = length
        parameters_dict['depth'] = depth
        parameters_dict['dtype'] = dtype
        parameters_dict['key'] = key
        if plant.isnumeric(key):
            parameters_dict['band'] = key
        parameters_dict['header_file'] = header_file

    plant_module = None
    if not flag_driver_found:
        module_name = driver.replace('.py', '').lower()
        if not module_name.startswith('plant_'):
            module_name = 'plant_' + module_name
        try:
            _ = importlib.import_module('plant.app.' +
                                        module_name)
            plant_module = module_name
            flag_driver_found = True
            driver = module_name
        except ImportError:
            pass
        if (plant_module is not None and
                parameters_dict is not None):
            parameters_dict['plant_module'] = plant_module
            filename = plant.IMAGE_NAME_SEPARATOR.join(
                input_file_splitted[1:])
            parameters_dict['filename'] = filename
    # print('*** flag_driver_found: ', flag_driver_found)
    # print('*** parameters_dict: ', parameters_dict)
    return flag_driver_found


def search_image(filename_orig, invalid_list=None):
    if filename_orig is None:
        return False
    # print('*** filename_orig:', filename_orig)
    if (isinstance(filename_orig, plant.PlantImage) or
            test_valid_number_sequence(filename_orig) or
            test_gdal_open(filename_orig) or
            test_other_drivers(filename_orig)):
        return [filename_orig]

    if plant.IMAGE_NAME_SEPARATOR in filename_orig:
        ret_dict = parse_filename(filename_orig)
        filename = ret_dict['filename']
        filename_list = []
        glob_list = plant.glob(filename)
        if len(glob_list) == 0:
            if invalid_list is not None:
                invalid_list.append(filename_orig)
            return []
        for f in glob_list:
            filename = get_filename_from_dict(ret_dict, filename=f)
            filename_list += [filename]
            '''
            filename_list += \
                [plant.IMAGE_NAME_SEPARATOR.join([f]+filename_splitted[1:])]
            '''
        return filename_list
    elif plant.isfile(filename_orig):
        # return [filename_orig]
        return plant.glob(filename_orig)
    elif invalid_list is not None:
        invalid_list.append(filename_orig)
    return []


def read_image_kml(kml_file,
                   band=None,
                   verbose=False,
                   only_header=True,
                   flag_no_messages=False,
                   flag_exit_if_error=True):
    try:
        kml = ET.parse(kml_file)
    except:
        plant.handle_exception('file could not be opened: %s'
                               ' (kml)'
                               % kml_file,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    icon_list = []
    for element in kml.getroot().iter():
        for lat_lon_box_element in element.getchildren():
            if 'LatLonBox' in lat_lon_box_element.tag:
                icon = None
                for other_child in element.getchildren():
                    if 'Icon' in other_child.tag:
                        icon = other_child.getchildren()[0].text
                if icon is not None:
                    north = None
                    south = None
                    east = None
                    west = None
                    for coord in lat_lon_box_element.getchildren():
                        if 'north' in coord.tag:
                            north = coord.text
                        if 'south' in coord.tag:
                            south = coord.text
                        if 'east' in coord.tag:
                            east = coord.text
                        if 'west' in coord.tag:
                            west = coord.text
                    try:
                        icon_list.append([icon,
                                          [float(south),
                                           float(north),
                                           float(east),
                                           float(west)]])
                    except:
                        pass
    image_obj = None

    band_count = 0
    image_obj = None
    for i, icon in enumerate(icon_list):
        current_image_obj = read_image(icon[0],
                                       verbose=verbose,
                                       only_header=(only_header or
                                                    band is not None))
        if current_image_obj is None:
            continue
        if image_obj is not None:
            if (image_obj.width != current_image_obj.width or
                    image_obj.length != current_image_obj.length):
                if not flag_no_messages:
                    print('WARNING ignoring %s because image '
                          'dimensions diffe from %s'
                          % (icon[0], icon_list[0][0]))
                continue
        if (band is not None and
                band < band_count + current_image_obj.nbands):
            image_obj = read_image(icon[0],
                                   verbose=verbose,
                                   band=band-band_count,
                                   only_header=only_header)
            geotransform = get_geotransform_kml(icon[1],
                                                image_obj.length,
                                                image_obj.width)
            image_obj.set_geotransform(geotransform,
                                       realize_changes=False)
            break
        if band is None and i == 0:
            image_obj = current_image_obj
            geotransform = get_geotransform_kml(icon[1],
                                                image_obj.length,
                                                image_obj.width)
            image_obj.set_geotransform(geotransform,
                                       realize_changes=False)
        elif band is None:
            for b in range(current_image_obj.bands):
                image_obj.set_band(current_image_obj.get_band(band=b),
                                   band=band_count+b)
        band_count += current_image_obj.nbands

    '''
    band_count = 0
    if band is None:
        band = 0
    elif band >= len(icon_list):
        if flag_exit_if_error:
            plant.handle_exception('file %s has only %d band(s)'
                             % (kml_file,
                                len(icon_list)),
                             flag_exit_if_error,
                             verbose=verbose)
            return
    i = icon_list[band]
    if verbose:
        print('selected KML icon: %s' % (i))
    if image_obj is None:
        image_obj = read_image(i[0],
                               verbose=verbose)
        band_count += 3
    '''
    # if band is not None:
    #    image_obj.set_image(image_obj.get_image(band=band),
    #                        band=0)
    #    image_obj.nbands = 1

    return image_obj


def get_geotransform_kml(bbox_outer, length, width):
    step_lat = ((bbox_outer[1]-bbox_outer[0])/width)
    step_lon = ((bbox_outer[3]-bbox_outer[2])/length)
    bbox = [bbox_outer[0]-step_lat/2,
            bbox_outer[1]+step_lat/2,
            bbox_outer[2]-step_lon/2,
            bbox_outer[3]+step_lon/2]
    geotransform = plant.get_geotransform(bbox=bbox,
                                          lat_size=length,
                                          lon_size=width)
    return geotransform


def parse_filename(filename_orig):
    band = None
    key = None
    ret_dict = {}
    # if os.path.isfile(filename_orig):
    #     ret_dict['filename'] = glob_glob(filename_orig)[0]
    # else:
    #     ret_dict['filename'] = filename_orig
    ret_dict['band'] = band
    ret_dict['key'] = key
    ret_dict['filename_position'] = 0

    if filename_orig.startswith('MEM:'):
        ret_dict['filename'] = filename_orig
        return ret_dict
    elif len(glob_glob(filename_orig)) > 0:
        ret_dict['filename'] = glob_glob(filename_orig)[0]
        return ret_dict
    filename_splitted = filename_orig.split(plant.IMAGE_NAME_SEPARATOR)
    # if (filename_splitted[0] == 'MEM' and
    #        filename_splitted[1] in plant.plant_config.variables.keys()):
    #    return ret_dict

    if filename_splitted[0].upper() in plant.AVAILABLE_DRIVERS:
        parameters_dict = {}
        test_other_drivers(filename_orig,
                           parameters_dict=parameters_dict)
        return parameters_dict
    # driver = ret_dict['driver']
    # print('*** filename: ', filename)
    # print('*** driver: ', driver)
    # if (driver == 'MEM' and
    #        filename in plant.plant_config.variables.keys()):
    #    return [filename_orig]

    filename_position = 1 if len(filename_splitted) > 2 else 0

    filename = filename_splitted[filename_position]
    if len(filename_splitted) > 1:
        complement = filename_splitted[filename_position+1]
        if plant.isnumeric(complement) and band is None:
            band = int(complement)
        elif not plant.isnumeric(complement) and key is None:
            key = complement
    ret_dict = {}
    ret_dict['filename_position'] = filename_position
    # if os.path.isfile(filename):
    if len(glob_glob(filename)) > 0:
        ret_dict['filename'] = glob_glob(filename)[0]
    else:
        ret_dict['filename'] = filename
    ret_dict['band'] = band
    ret_dict['key'] = key
    return ret_dict


def get_filename_from_dict(input_dict, filename=None, band=None, key=None):
    if not filename:
        filename = input_dict.get('filename', None)
    if band is None:
        band = input_dict.get('band', None)
    if band is not None:
        filename += f':{band}'
    if key is None:
        key = input_dict.get('key', None)
    if key is not None:
        filename += f'{key}'
    # print('*** .', filename)
    return filename


def _read_image_unlock(image_obj):
    if image_obj is None:
        return
    image_obj.unlock_realize_changes()


def read_image(filename_orig,
               band=None,
               verbose=True,
               input_format=None,
               only_header=True,
               read_only=True,
               edges_outer=None,
               edges_center=None,
               plant_transform_obj=None,
               ref_image_obj=None,
               scheme=None,

               geotransform=None,
               projection=None,
               lat_size=None,
               lon_size=None,

               input_width=None,
               input_length=None,
               input_depth=None,
               input_dtype=None,

               input_key=None,
               header_file=None,
               in_null=None,
               # out_null=None,
               sampling_step=None,
               sampling_step_x=None,
               sampling_step_y=None,
               force=None,
               flag_no_messages=False,
               flag_exit_if_error=True,
               **kwargs_orig):
    '''
    read image and parameters and return them as PlantImage object
    '''
    # print('*** sampling_step:', sampling_step)
    # print('*** sampling_step_x:', sampling_step_x)
    # print('*** sampling_step_y:', sampling_step_y)
    # print('*** input_format (0): ', input_format)
    # print('*** filename_orig ', filename_orig, filename_orig.__class__)
    # print('*** aaa input_format: ', input_format)
    # print('*** bbb input_format: ', input_format)
    if input_format is not None:
        input_format = input_format.strip().upper()
    if isinstance(filename_orig, plant.PlantImage):
        # print('*** PlantImage obj: ', id(plant.PlantImage))
        image_obj = filename_orig
        _read_image_unlock(image_obj)
        return image_obj
    if (isinstance(filename_orig, np.ndarray) or
            isinstance(filename_orig, numbers.Number) or
            isinstance(filename_orig, list) or
            isinstance(filename_orig, tuple) or
            isinstance(filename_orig, slice)):
        if (isinstance(filename_orig, numbers.Number)):
            filename_orig = np.asarray(filename_orig)
        elif (isinstance(filename_orig[0], str) and
              all([plant.isfile(f) for f in filename_orig])):
            # print('*** new list of files')
            kwargs = locals()
            kwargs.pop('filename_orig')
            image_obj = None
            current_band = None
            for f in filename_orig:
                # print('*** processing file: ', f)
                # print('*** processing band: ', current_band)
                if image_obj is None:
                    image_obj = plant.read_image(f, **kwargs)
                    current_band = image_obj.nbands
                    continue
                current_image_obj = plant.read_image(f, **kwargs)
                for b in range(current_image_obj.nbands):
                    image_obj.set_image(current_image_obj.get_image(
                        band=b), band=current_band)
                    current_band += 1
            _read_image_unlock(image_obj)
            return image_obj
        image_obj = plant.PlantImage(filename_orig,
                                     file_format='ARRAY',
                                     ref_image_obj=ref_image_obj,
                                     plant_transform_obj=plant_transform_obj)
        _read_image_unlock(image_obj)
        return image_obj
    if test_valid_number_sequence(filename_orig):
        parameters_dict = {}
    else:
        parameters_dict = plant.parse_filename(filename_orig)
    # parameters_dict = {}
    flag_test_gdal_open = test_gdal_open(filename_orig,
                                         parameters_dict=parameters_dict)
    kwargs = locals()
    if flag_no_messages or input_format:
        image_obj = _read_image(**kwargs)
        if (ref_image_obj is not None and
                id(image_obj) != id(ref_image_obj)):
            image_obj = image_obj.copy(ref_image_obj=ref_image_obj)
        '''
        if (ref_image_obj is not None and
                id(image_obj) != id(ref_image_obj)):
            for key, value in image_obj.__dict__:
                ref_image_obj.__dict__[key] = value
            if band is None:
                band_list = range(ref_image_obj.nbands)
            else:
                band_list = [band]
            for b in band_list:
                band_obj = ref_image_obj.get_band(band=b)
                band_obj.parent_list.append(ref_image_obj)
        '''
        # print('*** ref_image_obj: ', ref_image_obj)
        _read_image_unlock(image_obj)
        return image_obj
    gdal.UseExceptions()
    gdal.ErrorReset()
    # print('*** input_format: ', input_format)
    try:
        ret_dict = plant.parse_filename(filename_orig)
        current_dataset = gdal.Open(ret_dict['filename'])
    except:
        current_dataset = None
    '''
    if current_dataset is not None:
        sub_datasets = current_dataset.GetSubDatasets()
        flag_test_hdf5 = bool(sub_datasets)
        current_dataset = None
    else:
        flag_test_hdf5 = False

    print('*** flag_test_hdf5´: ', flag_test_hdf5)
    '''
    if input_format is None and 'driver' in parameters_dict.keys():
        input_format = parameters_dict['driver']
    if input_format is None and isinstance(filename_orig, str):
        filename = parameters_dict['filename']
        # print('*** filename: ', filename)
        try:
            hdf5_obj = h5py.File(filename_orig, 'r')
            # flag_test_hdf5 = True
            input_format = 'HDF5'
        except OSError:
            pass
            # flag_test_hdf5 = False
    # print('*** input_format: ', input_format)
    # else:
    #    flag_test_hdf5 = 'HDF5' in input_format
    gdal.ErrorReset()
    kwargs['flag_no_messages'] = (input_format is None or
                                  'HDF5' not in input_format)
    kwargs['input_format'] = input_format
    image_obj = _read_image(**kwargs)
    if image_obj is None:
        plant.handle_exception(f'opening file {filename_orig}',
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return
    # image_obj = image_obj**2
    if (ref_image_obj is not None and
            id(image_obj) != id(ref_image_obj)):
        image_obj = image_obj.copy(ref_image_obj=ref_image_obj)
    _read_image_unlock(image_obj)
    return image_obj


def _read_image(filename_orig,
                band=None,
                verbose=True,
                input_format=None,
                only_header=True,
                read_only=True,
                edges_outer=None,
                edges_center=None,
                plant_transform_obj=None,
                ref_image_obj=None,
                scheme=None,
                input_width=None,
                input_length=None,
                input_depth=None,
                input_dtype=None,
                input_key=None,
                header_file=None,
                in_null=None,
                # out_null=None,

                geotransform=None,
                projection=None,
                lat_size=None,
                lon_size=None,

                sampling_step=None,
                sampling_step_x=None,
                sampling_step_y=None,
                force=None,
                flag_no_messages=False,
                flag_exit_if_error=True,

                parameters_dict=None,
                flag_test_gdal_open=None,
                **kwargs_orig):
    # print('*** filename_orig: ', filename_orig)
    image_obj_kwargs = {}
    image_obj_kwargs['realize_changes_locked'] = True
    if ref_image_obj is not None:
        image_obj_kwargs['ref_image_obj'] = ref_image_obj

    # print('*** ref_image_obj: ', ref_image_obj)
    out_null = None
    if flag_no_messages:
        verbose = False
    # print('*** (read_image): ', filename_orig)
    # print('*** (read_image) band: ', band)

    # print('*** transform (read_image): ', plant_transform_obj)
    # plant.debug(input_format)

    # if not isinstance(filename_orig, str):
    #     filename_orig = str(filename_orig)
    if not filename_orig:
        plant.handle_exception('invalid input',
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return
    '''
    if plant_transform_obj is not None:
        plant_transform_obj.offset_x = None
        plant_transform_obj.offset_y = None
        plant_transform_obj.width = None
        plant_transform_obj.length = None
    '''

    kwargs = locals()
    kwargs['image_obj_kwargs'] = image_obj_kwargs
    # plant.debug(input_key)
    # print(filename_orig, input_format)
    args = []
    args.append(kwargs.pop('filename_orig'))
    # args.append(kwargs.pop('filename'))
    # kwargs.pop('width')
    # kwargs.pop('input_key')
    width_orig = input_width
    length_orig = input_length
    depth_orig = input_depth

    if ((sampling_step is not None and
         sampling_step != 1) or
        (sampling_step_x is not None and
         sampling_step_x != 1) or
        (sampling_step_y is not None and
         sampling_step_y != 1)):
        kwargs.pop('sampling_step')
        kwargs.pop('sampling_step_x')
        kwargs.pop('sampling_step_y')
        # kwargs.pop('scheme')
        image_obj = read_image(*args, **kwargs)
        # print(image_obj.image)
        if image_obj is None:
            plant.handle_exception('invalid input %s'
                                   % filename_orig,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return

        if verbose and image_obj.depth <= 1:
            print('original shape: (%d, %d)'
                  % (image_obj.length,
                     image_obj.width))
        elif verbose:
            print('original shape: (%d, %d, %d)'
                  % (image_obj.length,
                     image_obj.width,
                     image_obj.depth))
        with plant.PlantIndent():
            image_obj = sample_image(image_obj,
                                     only_header=only_header,
                                     sampling_step=sampling_step,
                                     sampling_step_x=sampling_step_x,
                                     sampling_step_y=sampling_step_y,
                                     verbose=verbose)
        if image_obj is None:
            plant.handle_exception('invalid input %s'
                                   % filename_orig,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return

        # if only_header:
        #     return image_obj

        # image_obj = plant.apply_null(image_obj,
        #                       in_null=in_null,
        #                       out_null=out_null)
        # image_obj = plant.apply_mask(image_obj,
        #                       plant_transform_obj=plant_transform_obj,
        #                       verbose=verbose,
        #                       force=force)
        return image_obj
    else:
        kwargs.pop('sampling_step')
        kwargs.pop('sampling_step_x')
        kwargs.pop('sampling_step_y')

    # header_file = None
    # length_orig = None
    # width_orig = None
    # depth_orig = None
    image = None
    # print('*** input_dtype: ', input_dtype)
    dtype = None if input_dtype is None else input_dtype
    # width_orig = None
    image_obj = None

    if test_valid_number_sequence(filename_orig):
        image_obj = read_image_array(filename_orig,
                                     verbose=verbose,
                                     plant_transform_obj=plant_transform_obj,
                                     in_null=None,
                                     out_null=None,
                                     force=None,
                                     only_header=only_header,
                                     flag_no_messages=False,
                                     flag_exit_if_error=False)
        return image_obj

    '''
    parameters_dict = {}
    flag_test_gdal_open = test_gdal_open(filename_orig,
                                         parameters_dict=parameters_dict)
    '''
    if flag_test_gdal_open:
        filename = parameters_dict['filename']
        if band is None and 'band' in parameters_dict.keys():
            band = parameters_dict['band']
        if input_key is None and 'key' in parameters_dict.keys():
            input_key = parameters_dict['key']
    elif test_other_drivers(filename_orig,
                            parameters_dict=parameters_dict):
        # plant.debug('other drivers ok')
        filename = parameters_dict['filename']
        if header_file is None and 'header_file' in parameters_dict.keys():
            header_file = parameters_dict['header_file']
        if input_key is None and 'key' in parameters_dict.keys():
            input_key = parameters_dict['key']
        if input_dtype is None and 'dtype' in parameters_dict.keys():
            dtype = parameters_dict['dtype']
        else:
            dtype = input_dtype
        if width_orig is None and 'width' in parameters_dict.keys():
            width_orig = parameters_dict['width']
        if length_orig is None and 'length' in parameters_dict.keys():
            length_orig = parameters_dict['length']
        if depth_orig is None and 'depth' in parameters_dict.keys():
            depth_orig = parameters_dict['depth']
        if input_format is None and 'driver' in parameters_dict.keys():
            input_format = parameters_dict['driver']
        if input_format is not None and input_format == 'MEM':
            try:
                image_obj = plant.plant_config.variables[filename]
            except KeyError:
                # first attemp: globals()
                image_list = [x for x in globals().values()
                              if isinstance(x, plant.PlantImage) and
                              x.get_filename() == filename]

                # second attemp: garbage collector
                if len(image_list) == 0:
                    import gc
                    image_list = [x for x in gc.get_objects()
                                  if isinstance(x, plant.PlantImage) and
                                  x.get_filename() == filename]
                if len(image_list) == 0:
                    image_obj = None
                else:
                    image_obj = image_list[0]
            # print('*** image_obj: ', image_obj, image_obj.__class__)
            if image_obj is None:
                plant.handle_exception(f'opening memory file {filename_orig}',
                                       flag_exit_if_error,
                                       flag_no_messages=flag_no_messages,
                                       verbose=verbose)
                return
            # if only_header:
            #     image_obj = image_obj.copy()
            # else:
            if not isinstance(image_obj, plant.PlantImage):
                out_image_obj = plant.PlantImage(image_obj)
            elif band is None:
                out_image_obj = image_obj.deep_copy(
                    ref_image_obj=ref_image_obj)
            else:
                out_image_obj = image_obj.soft_copy(
                    ref_image_obj=ref_image_obj)
                out_image_obj.nbands = 0
                band_list = band if hasattr(band, '__getitem__') else [band]
                for i, b in enumerate(band_list):
                    out_image_obj.set_band(image_obj.get_band(band=b).copy(),
                                           band=i)
            if (not out_image_obj.image_loaded and
                    out_image_obj.plant_transform_obj is not None and
                    plant_transform_obj is not None and
                    out_image_obj.plant_transform_obj is not
                    plant_transform_obj):
                out_image_obj.realize_changes()
            # print('*** WARNING two different PlantTransforms')
            # print(f'*** opening image from memory: {filename}'
            #       f' (new id: {id(image_obj)})')
            # print('*** width (1) ', image_obj.width_orig)
            # print('*** length (1) ', image_obj.length_orig)
            # print('*** shape (1) ', image_obj.shape)
            # print('*** nbands (1) ', image_obj.nbands)

            # print('*** image_obj.filename: ', image_obj.filename)
            # print('*** image_obj.filename_orig: ', image_obj.filename_orig)
            if not out_image_obj.filename_orig:
                out_image_obj.filename_orig = filename_orig
            if not out_image_obj.filename:
                out_image_obj.filename = filename_orig
            if not out_image_obj.file_format:
                out_image_obj.file_format = 'MEM'

            input_format = 'MEM'

            # out_image_obj =
            prepare_image(out_image_obj,
                          filename,
                          flag_exit_if_error=flag_exit_if_error,
                          plant_transform_obj=plant_transform_obj,
                          in_null=in_null,
                          out_null=out_null,
                          input_format=input_format,
                          verbose=verbose,
                          only_header=only_header,
                          flag_no_messages=flag_no_messages,
                          force=force)

            # print('*** width (2) ', out_image_obj.width_orig)
            # print('*** length (2) ', out_image_obj.length_orig)
            # print('*** shape (2) ', out_image_obj.shape)
            # print('*** nbands (2) ', out_image_obj.nbands)
            return out_image_obj

        if ('plant_module' in parameters_dict.keys() and
                parameters_dict['plant_module'] is not None):
            plant_module = parameters_dict['plant_module']
            if filename_orig in plant.plant_config.cache_dict.keys():
                image_obj = plant.plant_config.cache_dict[filename_orig]
            else:
                module_obj = plant.ModuleWrapper(plant_module)
                # flag_mute = plant.plant_config.logger_obj.flag_mute
                # plant.plant_config.logger_obj.flag_mute = True
                # print('*** filename: ', filename)
                image_obj = module_obj(filename, verbose=True)
                # plant.plant_config.logger_obj.flag_mute = flag_mute
                image_obj.filename_orig = ''
                filename = ''
                image_obj.filename = filename
                input_format = 'MEM'
                plant.plant_config.cache_dict[filename_orig] = image_obj
                image_obj.file_format = input_format

            # image_obj =
            prepare_image(image_obj,
                          filename,
                          flag_exit_if_error=flag_exit_if_error,
                          plant_transform_obj=plant_transform_obj,
                          in_null=in_null,
                          out_null=out_null,
                          input_format=input_format,
                          verbose=verbose,
                          only_header=only_header,
                          flag_no_messages=flag_no_messages,
                          force=force)

            return image_obj
    elif plant.IMAGE_NAME_SEPARATOR in filename_orig:
        ret_dict = parse_filename(filename_orig)
        filename = ret_dict['filename']
        if band is None and 'band' in ret_dict.keys():
            band = ret_dict['band']
        if input_key is None and 'key' in ret_dict.keys():
            input_key = ret_dict['key']
    elif not plant.isfile(filename_orig):
        filename_temp = plant.glob(filename_orig)
        if not filename_temp or len(filename_temp) == 0:
            plant.handle_exception('file not found: '+filename_orig,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return
        filename = filename_temp[0]
        if os.path.isdir(filename):
            plant.handle_exception('input is a directory: '+filename,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return
    else:
        filename = os.path.expanduser(filename_orig)
        filename_list = plant.glob(filename)
        if len(filename_list) == 0:
            plant.handle_exception(f'file not found: {filename}',
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return
        filename = filename_list[0]
    args.append(filename)

    kwargs['band'] = band
    kwargs['input_key'] = input_key
    kwargs['input_width'] = input_width

    if not input_format:
        input_format = ''

    if ((not(input_format) and filename.endswith('.npy')) or
            input_format == 'NUMPY'):
        # image = np.load(filename)
        image = np.fromfile(filename)
        image_obj = plant.PlantImage(filename=filename,
                                     image=image,
                                     # dtype=dtype,
                                     # width=width_orig,
                                     # length=length_orig,
                                     # depth=depth_orig,
                                     file_format='NUMPY',
                                     # ref_image_obj=ref_image_obj,
                                     plant_transform_obj=plant_transform_obj,
                                     **image_obj_kwargs)
        # image_obj =
        prepare_image(image_obj,
                      filename,
                      flag_exit_if_error=flag_exit_if_error,
                      plant_transform_obj=plant_transform_obj,
                      in_null=in_null,
                      out_null=out_null,
                      input_format=input_format,
                      verbose=verbose,
                      only_header=only_header,
                      flag_no_messages=flag_no_messages,
                      force=force)
        return image_obj

    header_dict = None
    if header_file is None:
        header_dict = plant.parse_uavsar_filename(filename)
        header_file = plant.get_annotation_file(
            header_dict, dirname=os.path.dirname(filename))
        if isinstance(header_file, list) and len(header_file) > 0:
            header_file = header_file[0]

    # ANNOTATION FILE
    if (not input_format or
            'ANN' in input_format.upper() or
            (header_file and header_file.endswith('.ann'))):
        if (len(plant.glob(os.path.join(plant.dirname(filename),
                                        '*ann'))) != 0):
            kwargs['header_file'] = header_file
            kwargs['header_dict'] = header_dict
            image_obj = _read_image_ann(*args, **kwargs)
            kwargs.pop('header_dict')
            if image_obj is not None:
                return image_obj
            elif input_format == 'ANN':
                plant.handle_exception(f'reading file {filename_orig} (ANN)',
                                       flag_exit_if_error,
                                       flag_no_messages=flag_no_messages,
                                       verbose=verbose)
                return
        elif input_format == 'ANN':
            plant.handle_exception('annotation file not found',
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return

    # 'ISCE3'
    if input_format in ['HDF5', 'NISAR']:
        image_obj = _read_hdf5(*args, **kwargs)
        if image_obj is not None:
            return image_obj


    # GDAL
    if ((not input_format and not scheme) or
            (input_format and
             (input_format not in plant.AVAILABLE_DRIVERS or
              input_format in ['HDF5', 'NETCDF', 'SENTINEL']))):
        kwargs.pop('flag_exit_if_error')
        image_obj = _read_image_gdal(*args, **kwargs)
        kwargs['flag_exit_if_error'] = flag_exit_if_error

        # Sentinel
        if 'SENTINEL' in input_format and input_key:
            image_obj = _read_image_sentinel(image_obj, filename,
                                             header_file, input_key,
                                             verbose)

        if image_obj is not None:
            return image_obj
        # print('*** input_format: ', input_format)
        if input_format == 'GDAL' or 'HDF5' in input_format:
            plant.handle_exception('file could not be opened: ' +
                                   filename+' (GDAL)',
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return

    # HDF5 (moved to GDAL driver)
    '''
    if ('HDF5' in input_format or
            not input_format or
            (header_file and header_file.endswith('.h5'))):
        image_obj = _read_image_hdf5()
    '''

    # ISCE
    if not scheme and filename.endswith('.xml'):
        try:
            image_obj = get_info_from_xml(filename)
        except KeyError:
            pass
    elif not scheme and plant.isfile(filename + '.xml'):
        try:
            image_obj = get_info_from_xml(filename + '.xml')
        except KeyError:
            pass
    if image_obj is not None:
        scheme = image_obj.scheme
    if not scheme:
        scheme = 'BSQ'
    if (input_format == 'ISCE' or
            not input_format or
            input_format == 'ENVI'):
        flag_exit_if_error_isce = (kwargs.pop('flag_exit_if_error', True) and
                                   input_format == 'ISCE')
        kwargs['flag_exit_if_error'] = flag_exit_if_error_isce
        kwargs['image_obj'] = image_obj
        image_obj = _read_image_bin(*args, **kwargs)
        if image_obj is None and (input_format == 'ISCE' or
                                  input_format == 'ENVI'):
            plant.handle_exception(
                f'file could not be opened: {filename}'
                f' using format: {input_format}',
                flag_exit_if_error,
                flag_no_messages=flag_no_messages,
                verbose=verbose)
            return
        elif image_obj is not None:
            return image_obj

    # SRF
    flag_open_srf = (not input_format or
                     input_format == 'SRF')
    if flag_open_srf:
        try:
            header = np.fromfile(filename, dtype=np.uint32, count=8).tolist()
            if len(header) == 0:
                header = [0]
            if header[0] != plant.SRF_MAGIC_NUMBER:
                flag_open_srf = False
        except:
            flag_open_srf = False

    if input_format == 'SRF' and not flag_open_srf:
        print(f'ERROR opening {filename_orig} using SRF format')
        return

    if flag_open_srf:
        if verbose and not only_header:
            print('opening: ' + filename + ' (SRF)')
        width_orig = header[1]
        length_orig = header[2]
        depth_orig = 1
        dtype = plant.get_np_dtype_from_srf(header[5])
        dtype_size = header[3] / 8
        n_elements = header[4]
        nbands_orig = max([1, int(n_elements /
                                  (width_orig*length_orig*8))])
        if dtype is None:
            print('SRF header: ', header)
            print(f'    width: {width_orig}')
            print(f'    length: {length_orig}')
            print(f'    data type: {dtype}')
            print(f'    data type size: {dtype_size}')
            print(f'    n_elements: {n_elements}')
            print(f'    nbands: {nbands_orig}')
            plant.handle_exception('file could not be opened %s (SRF)'
                                   % filename,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return
        '''
        if not only_header:
            image = np.fromfile(filename, dtype=dtype,
                                count=width_orig*length_orig)
            image = image.reshape((length_orig,
                                   width_orig))
            image = plant.insert_nan(image,
                                     np.where(image == -9999),
                                     out_null=np.nan)
        '''
        if in_null is None:
            in_null = -9999.0
        # kwargs['band'] = list(range(nbands_orig))
        image_obj = plant.PlantImage(filename=filename,
                                     # image=image,
                                     filename_orig=filename_orig,
                                     dtype=dtype,
                                     width=width_orig,
                                     length=length_orig,
                                     depth=depth_orig,
                                     null=in_null,
                                     nbands=nbands_orig,
                                     file_format='SRF',
                                     # ref_image_obj=ref_image_obj,
                                     plant_transform_obj=plant_transform_obj,
                                     scheme=scheme,
                                     **image_obj_kwargs)

        if not only_header:
            kwargs['image_obj'] = image_obj
            # kwargs['input_width'] = width_orig
            # kwargs['input_length'] = length_orig
            # kwargs['input_depth'] = depth_orig
            # kwargs['scheme'] = 'BSQ'
            kwargs['offset'] = 32
            image_obj = _read_image_bin(*args, **kwargs)
        image_obj = plant.PlantImage(input_data=image_obj,
                                     filename=filename,
                                     filename_orig=filename_orig,
                                     # ref_image_obj=ref_image_obj,
                                     plant_transform_obj=plant_transform_obj,
                                     file_format='SRF',
                                     scheme=scheme,
                                     **image_obj_kwargs)
        if not only_header:
            return image_obj

    elif (filename.endswith('.csv') or filename.endswith('.txt') or
          input_format in plant.TEXT_DRIVERS):
        image_obj = read_image_csv(filename,
                                   filename_orig,
                                   plant_transform_obj,
                                   band=band,
                                   key=input_key,
                                   in_null=in_null,
                                   out_null=out_null,
                                   only_header=only_header,
                                   input_format=input_format,
                                   flag_exit_if_error=flag_exit_if_error,
                                   force=force,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
        return image_obj

    elif (filename.endswith('.bin') or
          input_format == 'BIN' or
          (filename.endswith('.dat') and
           plant.isfile(filename.replace('.dat', '.Hdr')))):

        if band is not None:
            band_suffix = f', band: {band}'
        else:
            band_suffix = ''
        if ((width_orig is not None or length_orig is not None) and
                dtype is not None):
            image_obj = None
            dtype_size = plant.get_dtype_size(dtype)
            file_size = int(os.stat(filename).st_size)
            if plant.isvalid(file_size) and plant.isvalid(dtype_size):
                n_elements = file_size/dtype_size
                if length_orig is None and width_orig is not None:
                    length_orig = int(n_elements/width_orig)
                elif width_orig is None and length_orig is not None:
                    width_orig = int(n_elements/length_orig)
                elif width_orig is not None and length_orig is not None:
                    depth_orig = int(n_elements/(width_orig *
                                                 length_orig))
                else:
                    width_orig = int(n_elements)
                    length_orig = 1
            else:
                n_elements = np.nan
                length_orig = np.nan
            if depth_orig is None:
                depth_orig = 1
            # print('*** length_orig: ', length_orig)
            # print('*** width_orig: ', width_orig)
            # print('*** depth_orig: ', depth_orig)
            # print('*** dtype: ', dtype)
            filename_orig=(f'BIN:{filename}:{width_orig}:'
                           f'{length_orig}:{depth_orig}:'
                           f'{dtype}')
            band_orig = None if band is None else [band]
            image_obj = plant.PlantImage(
                input_data=image_obj,
                filename=filename,
                filename_orig=filename_orig,
                nbands_orig=1,
                band_orig=band_orig,
                width_orig=width_orig,
                length_orig=length_orig,
                depth=depth_orig,
                dtype=dtype,
                # ref_image_obj=ref_image_obj,
                plant_transform_obj=plant_transform_obj,
                file_format='BIN',
                scheme=scheme,
                **image_obj_kwargs)
            if verbose and not only_header:
                print('opening: %s (binary mmap%s)'
                      % (filename, band_suffix))
            if verbose:
                if plant.isvalid(dtype_size):
                    print('    data type size: %d' % (dtype_size))
                if plant.isvalid(file_size):
                    print('    file size: %d' % (file_size))
                if plant.isvalid(n_elements):
                    print('    n_elements: %d' % (n_elements))
                print('    width: %d' % (width_orig))
                if plant.isvalid(length_orig):
                    print('    length: %d' % (length_orig))
        elif filename.endswith('dat'):
            image_obj = read_NOHRSC(filename,
                                    verbose=verbose)
        elif plant.isfile(os.path.join(os.path.dirname(filename),
                                       'config.txt')):
            if verbose and not only_header:
                print('opening: %s (config.txt mmap%s)'
                      % (filename, band_suffix))
            image_obj = get_info_from_config_txt(
                filename,
                flag_exit_if_error=flag_exit_if_error,
                flag_no_messages=flag_no_messages,
                verbose=verbose)
        elif input_format:
            if verbose and not only_header:
                print('opening: %s (binary%s)'
                      % (filename, band_suffix))
            kwargs['only_header'] = True
            kwargs['plant_transform_obj'] = None
            kwargs['input_format'] = None
            image_obj = read_image(filename, **kwargs)
            kwargs['only_header'] = only_header
            kwargs['input_format'] = input_format
            kwargs['plant_transform_obj'] = plant_transform_obj

        if image_obj is None:
            plant.handle_exception('file could not be opened %s'
                                   % filename,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return

        # if not only_header:
        kwargs['image_obj'] = image_obj
        # print('*** here: ', dtype)
        # print('*** here: ', image_obj.dtype)
        image_obj = _read_image_bin(*args, **kwargs)
        # print('*** here: ', image_obj.dtype)

        image_obj = plant.PlantImage(input_data=image_obj,
                                     filename=filename,
                                     filename_orig=filename_orig,
                                     # ref_image_obj=ref_image_obj,
                                     plant_transform_obj=plant_transform_obj,
                                     file_format='BIN',
                                     scheme=scheme,
                                     **image_obj_kwargs)
        if not only_header:
            return image_obj

    # KML
    elif (input_format == 'KML' or
          filename.endswith('.kml')):
        image_obj = read_image_kml(filename,
                                   verbose=verbose,
                                   flag_no_messages=flag_no_messages,
                                   flag_exit_if_error=flag_exit_if_error,
                                   only_header=only_header,
                                   band=band)
        if image_obj is None:
            return
        width_orig = image_obj.width
        length_orig = image_obj.length
        depth_orig = image_obj.depth

    elif (filename.endswith('.shp') or
          input_format == 'shapefile'):
        FLAG_OGR_READ_ONLY = 0
        FLAG_OGR_WRITEABLE = 1
        driver = ogr.GetDriverByName('ESRI Shapefile')
        data_source = driver.Open(filename, FLAG_OGR_READ_ONLY)
        if data_source is None:
            plant.handle_exception('file could not be opened %s'
                                   % filename,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return
        if width_orig is None and lon_size is not None:
            width_orig = lon_size
        if length_orig is None and lat_size is not None:
            length_orig = lat_size

        kwargs = {}
        kwargs['filename'] = filename
        kwargs['filename_orig'] = filename_orig
        kwargs['plant_transform_obj'] = plant_transform_obj
        kwargs['scheme'] = scheme
        kwargs['band'] = band

        kwargs['verbose'] = verbose
        kwargs['only_header'] = only_header
        kwargs['flag_no_messages'] = flag_no_messages
        kwargs['flag_exit_if_error'] = flag_exit_if_error




        if length_orig is not None and length_orig == 0:
            raise

        if geotransform is None:
            image_obj = _read_shapefile_as_vector(data_source,
                                                  **kwargs)
        '''
        print('*** width_orig: ', image_obj.width_orig)
        print('*** length_orig: ', image_obj.length_orig)
        print('*** width_orig: ', image_obj.depth_orig)
        print('*** width: ', image_obj.width)
        print('*** length: ', image_obj.length)
        print('*** width: ', image_obj.depth)
        '''
        if image_obj is None or image_obj.image is None:
            image_obj = _read_shapefile_as_image(data_source,
                                                 geotransform=geotransform,
                                                 projection=projection,
                                                 width=width_orig,
                                                 length=length_orig,
                                                 out_null=out_null,
                                                 **kwargs)

    # image_obj =
    prepare_image(image_obj,
                  filename,
                  flag_exit_if_error=flag_exit_if_error,
                  plant_transform_obj=plant_transform_obj,
                  in_null=in_null,
                  out_null=out_null,
                  input_format=input_format,
                  verbose=verbose,
                  only_header=only_header,
                  flag_no_messages=flag_no_messages,
                  force=force)
    return image_obj


def _read_shapefile_as_image(data_source,
                             filename,
                             filename_orig,
                             plant_transform_obj,
                             scheme,
                             # ref_image_obj=None,
                             geotransform=None,
                             projection=None,
                             out_null=None,
                             length=None,
                             width=None,
                             band=None,
                             only_header=True,
                             flag_no_messages=False,
                             flag_exit_if_error=False,
                             verbose=True,
                             image_obj_kwargs={}):
    # determine x_min, x_max, y_min, y_max, step_lat and step_lon
    SHAPEFILE_DEFAULT_WIDTH_LENGTH = 4096
    if geotransform:
        geotransform_edges = \
            plant.geotransform_centers_to_edges(geotransform,
                                                lat_size=length,
                                                lon_size=width)
        step_lon = geotransform_edges[1]
        step_lat = geotransform_edges[5]
        if len(geotransform_edges) > 6:
            geotransform_edges[5] = -geotransform_edges[5]
            geotransform_edges[3] = geotransform_edges[7]
            geotransform_edges = geotransform_edges[:6]
        if width is None or length is None:
            x_min, x_max, y_min, y_max = _shapefile_get_extent(
                data_source)
    else:
        x_min, x_max, y_min, y_max = _shapefile_get_extent(
            data_source)
        if width:
            step_lon = ((x_max - x_min) / width)
        else:
            step_lon = ((x_max - x_min) /
                        SHAPEFILE_DEFAULT_WIDTH_LENGTH)
        if length:
            step_lat = ((y_max - y_min) / length)
        else:
            step_lat = ((y_max - y_min) /
                        SHAPEFILE_DEFAULT_WIDTH_LENGTH)
        if length is None and width is None:
            step_lon = min([step_lon, step_lat])
            step_lat = step_lon
    if not width:
        width = int((x_max - x_min) / step_lon)
    else:
        x_min = geotransform_edges[0]
        x_max = x_min+width*step_lon
    if not length:
        length = int((y_max - y_min) / step_lat)
    else:
        y_max = geotransform_edges[3]
        y_min = y_max-length*step_lat

    image_list = []
    target_ds = \
        gdal.GetDriverByName('MEM').Create('', width, length,
                                           gdal.GDT_Byte)
    geotransform_edges = (x_min, step_lon, 0, y_max, 0, -step_lat)
    if projection is not None:
        target_ds.SetProjection(projection)
    target_ds.SetGeoTransform(geotransform_edges)
    n_layers = data_source.GetLayerCount()


    if band is None:
        band_orig = None
        # nbands = 2 + zbands
    elif isinstance(band, Sequence) or isinstance(band, np.ndarray):
        band_orig = band
        # nbands = len(band)
    else:
        band_orig = [band]
        # nbands = 1
    
    for b in range(n_layers):
        if band_orig is not None and b not in band_orig:
            continue

        layer = data_source.GetLayer(b)
        if projection is None:
            projection = str(layer.GetSpatialRef())
        band = target_ds.GetRasterBand(1)
        if out_null is not None:
            band.SetNoDataValue(out_null)
        else:
            band.SetNoDataValue(0)
        gdal.RasterizeLayer(target_ds, [1], layer, burn_values=[1])
        image = band.ReadAsArray()
        image_list.append(image)
        continue
    geotransform = \
        plant.geotransform_edges_to_centers(
            geotransform_edges,
            lat_size=length,
            lon_size=width)

    image_obj = plant.PlantImage(
        input_data=image_list,
        band_orig=band_orig,
        geotransform=geotransform,
        filename=filename,
        filename_orig=filename_orig,
        # ref_image_obj=ref_image_obj,
        plant_transform_obj=plant_transform_obj,
        projection=projection,
        file_format='SHP',
        scheme=scheme,
        **image_obj_kwargs)
    return image_obj


def _shapefile_get_extent(data_source):
    n_layers = data_source.GetLayerCount()
    for b in range(n_layers):
        layer = data_source.GetLayer(b)
        if b == 0:
            x_min, x_max, y_min, y_max = layer.GetExtent()
        else:
            x_min_temp, x_max_temp, y_min_temp, y_max_temp = \
                layer.GetExtent()
            x_min = min([x_min, x_min_temp])
            y_min = min([y_min, y_min_temp])
            x_max = max([x_max, x_max_temp])
            y_max = max([y_max, y_max_temp])
    return x_min, x_max, y_min, y_max


def _read_shapefile_as_vector(data_source,
                              filename,
                              filename_orig,
                              scheme,
                              plant_transform_obj=None,
                              band=None,
                              only_header=True,
                              flag_no_messages=False,
                              flag_exit_if_error=False,
                              verbose=True,
                              image_obj_kwargs={}):
    image_x_list = []
    image_y_list = []
    image_z_dict = {}
    max_len = None
    image = None
    image_length_list = []
    for layer in data_source:
        field_list = []
        layer_definition = layer.GetLayerDefn()
        for i in range(layer_definition.GetFieldCount()):
            fieldName = \
                layer_definition.GetFieldDefn(i).GetName()
            field_list.append(fieldName)
        for i, feature in enumerate(layer):
            '''
            if only_header:
                width_feature = 1
                image_length_list.append(width_feature)
                if max_len is None or width_feature > max_len:
                    max_len = width_feature
            else:
            '''
            geom = feature.GetGeometryRef()
            points = geom.GetPoints()
            if points is None:
                continue
            # width_feature = len(points)
            image = np.asarray(points)
            if image.size == 0:
                continue
            if not only_header:
                image_x = image[:, 0]
                image_y = image[:, 1]
                image_x_list.append(image_x)
                image_y_list.append(image_y)
            else:
                image_length_list.append(image.shape[0])
            if max_len is None or image.shape[0] > max_len:
                max_len = image.shape[0]
            remove_list = []
            for j, field in enumerate(field_list):
                image_z = feature.GetField(field)
                if isinstance(image_z, str):
                    remove_list.append(field)
                    continue
                if only_header:
                    continue
                if field not in image_z_dict.keys():
                    image_z_dict[field] = []
                if hasattr(image_z, '__getitem__'):
                    image_z_dict[field].append(image_z)
                else:
                    image_z_dict[field].append([image_z])
            for field in remove_list:
                field_list.remove(field)
    zbands = len(field_list)
    if band is None:
        band_orig = None
        nbands = 2 + zbands
    elif isinstance(band, Sequence) or isinstance(band, np.ndarray):
        band_orig = band
        nbands = len(band)
    else:
        band_orig = [band]
        nbands = 1

    depth_orig = 1
    width = max_len

    if only_header:
        length_orig = len(image_length_list)
        if length_orig == 0:
            return
        image = None
    else:
        length_orig = len(image_x_list)
        if length_orig == 0:
            return
        image_x = np.full((length_orig, width), np.nan)
        image_y = np.full((length_orig, width), np.nan)
        for i in range(length_orig):
            line_len = len(image_x_list[i])
            image_x[i, :line_len] = image_x_list[i]
            image_y[i, :line_len] = image_y_list[i]

    if band is None:
        if not only_header:
            image = [image_x, image_y]
        image_name_list = [plant.X_BAND_NAME,
                           plant.Y_BAND_NAME]
    else:
        band_indexes = plant.get_int_list(band)
        if not only_header:
            image = []
        image_name_list = []
        for b in band_indexes:
            if b == 0:
                if not only_header:
                    image.append(image_x)
                image_name_list.append(plant.X_BAND_NAME)
            elif b == 1:
                if not only_header:
                    image.append(image_y)
                image_name_list.append(plant.Y_BAND_NAME)

    for j, field in enumerate(field_list):
        if band_orig is not None and j+2 not in band_orig:
            continue
        image_name_list.append(field)
        if only_header:
            continue
        image_z_list = image_z_dict[field]
        # if len(image_z_list) == 0:
        #     continue
        image_z = np.full((length_orig, width), np.nan)
        # print('*** image_z_list: ', len(image_z_list))
        # for i in range(np.min([length_orig, len(image_z_list)])):
        for i in range(length_orig):
            line_len = len(image_z_list[i])
            image_z[i, :line_len] = image_z_list[i]
        image.append(image_z)

    if not only_header and (image is None or len(image) == 0):
        return

    image_obj = plant.PlantImage(
        input_data=image,
        # geotransform=geotransform,
        band_orig=band_orig,
        filename=filename,
        filename_orig=filename_orig,
        # ref_image_obj=ref_image_obj,
        width=width,
        length=length_orig,
        depth=depth_orig,
        nbands=nbands,
        plant_transform_obj=plant_transform_obj,
        file_format='SHP',
        scheme=scheme,
        **image_obj_kwargs)
    for b, name in enumerate(image_name_list):
        image_obj.get_band(band=b).name = name
    return image_obj


def _read_image_sentinel(image_obj, filename, header_file, input_key,
                         verbose):
    if verbose:
        message = f'opening: {image_obj} (SENTINEL)'
        if header_file:
            message += f' header: {header_file}'
        if input_key:
            message += f' radiometry: {input_key}'
        print(message)
    if header_file is None:
        header_basename = plant.replace_extension(
            os.path.basename(filename), '.xml')
        header_basename = 'calibration-'+header_basename
        header_file_test = os.path.join(
            plant.dirname(filename),
            '../annotation/calibration/',
            header_basename)
        if plant.isfile(header_file_test):
            header_file = header_file_test
            if verbose:
                print('INFO SENTINEL calibration header'
                      f' found at {header_file}')

    if not header_file:
        return image_obj

    flag_beta = input_key and 'BETA' in input_key.upper()
    flag_sigma = input_key and 'SIGMA' in input_key.upper()
    flag_gamma = (input_key and ('GAMMA' in input_key.upper() or
                                 'GAMA' in input_key.upper()))
    if flag_beta:
        calibration_name = 'beta-naught'
    elif flag_sigma:
        calibration_name = 'sigma-naught'
    elif flag_gamma:
        calibration_name = 'gamma-naught'
    # image_obj.plant_transform_obj.function_dict[
    #     'radiometric_correction'] = calibration_name
    # print('*** sentinel: ', filename, header_file, input_key)
    # print('*** iamge is None: ', image_obj is None)
    # print('*** iamge is loaded: ', image_obj.image_loaded)
    #     if (image_obj is None or not image_obj.image_loaded):
    if image_obj is None:
        return
    print(f'calibration header file: {header_file}')
    rcs_lut = plant.sentinel_get_calibration_vectors(
        header_file,
        flag_beta=flag_beta,
        flag_sigma=flag_sigma,
        flag_gamma=flag_gamma)
    # print('*** rcs_lut', rcs_lut)
    if not rcs_lut:
        return
    print('applying radiometric correction:'
          f' {calibration_name}..')
    rcs_lut_array = None
    for b in range(image_obj.nbands):
        # print('*** processing band: ', b)
        image = np.asarray(image_obj.get_image(band=b),
                           np.float32)

        if rcs_lut_array is None:
            y_ind = np.arange(image.shape[0])
            x_ind = np.arange(image.shape[1])
            rcs_lut_array = rcs_lut(y_ind, x_ind)
        # image = rcs_lut_array
        image = np.asarray(image, dtype=float)/rcs_lut_array
        image *= image
        ind = np.where(image > 10)
        image[ind] = np.nan
        image_obj.set_image(image, band=b,
                            realize_changes=False)
    # print('*** done')
    return image_obj


def read_slice(filename_orig, default_stop=None):
    try:
        input_slice = plant.str_to_slice(filename_orig)
    except AttributeError:
        input_slice = None
    # print('*** input_slice:',
    #       input_slice, input_slice.__class__)
    if (input_slice is not None and
            input_slice.start is None and
            input_slice.stop is not None and
            input_slice.step is None):
        input_slice = None
    if input_slice is not None:
        ret = plant.expand_slice(input_slice,
                                 default_stop=default_stop)
        if ret is not None:
            return [list(ret)]
        # else:
        #     ret
        #     raise NotImplementedError
        return
    # else:
    rows = plant.read_image(
        filename_orig,
        plant_transform_obj=None,
        verbose=False).image.tolist()
    return rows


def read_image_array(filename_orig,
                     verbose=True,
                     plant_transform_obj=None,
                     # ref_image_obj=None,
                     in_null=None,
                     out_null=None,
                     force=None,
                     only_header=True,
                     flag_no_messages=False,
                     flag_exit_if_error=False,
                     image_obj_kwargs={}):
    if verbose:
        print('input array: %s' % filename_orig)
    if (isinstance(filename_orig, numbers.Number) or
            isinstance(filename_orig, np.ndarray) or
            isinstance(filename_orig, list) or
            isinstance(filename_orig, tuple)):
        image = np.asarray(filename_orig)
        filename_orig = str(filename_orig)
    else:
        # filename_orig_mult =
        # filename_orig.replace(plant.LINE_MULTIPLIER, 'm')
        line_splitted = filename_orig.split(plant.LINE_SEPARATOR)
        line_list = []
        for line_str in line_splitted:
            line_multiplier, line_str = _get_multiplier_array(
                line_str, plant.LINE_MULTIPLIER,
                plant.LINE_SEPARATOR)
            element_list = []
            line_str_splitted = line_str.split(plant.ELEMENT_SEPARATOR)
            for element_str in line_str_splitted:
                element_multiplier, element_str = _get_multiplier_array(
                    element_str, plant.ELEMENT_MULTIPLIER,
                    plant.ELEMENT_SEPARATOR)
                if plant.IMAGE_NAME_SEPARATOR in element_str:
                    element_str_splitted = element_str.split(
                        plant.IMAGE_NAME_SEPARATOR)
                    parameters = [plant.str_to_number(x)
                                  for x in element_str_splitted]
                    if parameters[0] == parameters[1]:
                        parameters_int = [int(x)
                                          for x in element_str_splitted[2:]]
                        element = np.zeros((tuple(parameters_int)),
                                           dtype=np.float32)
                        element += parameters[0]
                    else:
                        element = np.arange(*parameters)
                else:
                    element = np.asarray(np.fromstring(
                        element_str,
                        sep=plant.ELEMENT_SEPARATOR))
                    element = element.reshape(element.size)
                this_element = element.tolist()*element_multiplier
                if not isinstance(this_element, Sequence):
                    this_element = [this_element]
                element_list.extend(this_element)
                # print('*** element_list: ', element_list)
            line_list.extend([element_list]*line_multiplier)
        # print('*** line_list: ', line_list)
        image = plant.shape_image(line_list)
        # print('*** image: ', image)
    depth_orig, length_orig, width_orig = plant.get_image_dimensions(image)
    dtype = image.dtype
    image_obj = plant.PlantImage(input_data=image,
                                 width=width_orig,
                                 length=length_orig,
                                 depth=depth_orig,
                                 filename=filename_orig,
                                 file_format='ARRAY',
                                 # ref_image_obj=ref_image_obj,
                                 plant_transform_obj=plant_transform_obj,
                                 dtype=dtype,
                                 **image_obj_kwargs)
    if plant_transform_obj is not None:
        # image_obj =
        plant.apply_crop(image_obj,
                         plant_transform_obj=plant_transform_obj,
                         verbose=False)
    # if only_header:
    #     return image_obj
    # image_obj.set_image(image)
    # image_obj =
    plant.apply_null(image_obj,
                     in_null=in_null,
                     out_null=out_null)
    # image_obj =
    plant.apply_mask(image_obj,
                     plant_transform_obj=plant_transform_obj,
                     verbose=verbose,
                     force=force)
    return image_obj


def _get_multiplier_array(input_str, multiplier_symbol,
                          multiplier_separator):
    if multiplier_symbol not in input_str:
        return 1, input_str
    line_mult_splitted = input_str.split(multiplier_symbol)
    if len(line_mult_splitted) > 2:
        print(f'ERROR multiplier {multiplier_symbol}'
              ' can only be used once within between each'
              f' separator {multiplier_separator}')
        return
    if plant.isnumeric(line_mult_splitted[0]):
        multiplier = int(line_mult_splitted[0])
        input_str = line_mult_splitted[1]
    elif plant.isnumeric(line_mult_splitted[1]):
        input_str = line_mult_splitted[0]
        multiplier = int(line_mult_splitted[1])
    else:
        print('ERROR line multiplier can only be used with'
              ' constants')
        return
    return multiplier, input_str


def prepare_image(image_obj,
                  filename=None,
                  flag_exit_if_error=True,
                  plant_transform_obj=None,
                  in_null=None,
                  out_null=None,
                  input_format=None,
                  verbose=True,
                  only_header=True,
                  flag_no_crop=False,
                  flag_no_messages=False,
                  force=None):
    if image_obj is not None and filename is None:
        filename = image_obj.filename
    if image_obj is not None and plant_transform_obj is None:
        plant_transform_obj = image_obj.plant_transform_obj
    if image_obj is None and input_format:
        plant.handle_exception('file could not be opened: %s with '
                               'format %s'
                               % (filename, input_format),
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return
    elif image_obj is None:
        plant.handle_exception('file could not be opened: %s'
                               % (filename),
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return
    '''
    if (plant_transform_obj is not None and
            image_obj._applied_transform_obj_list and
            id(plant_transform_obj) in image_obj._applied_transform_obj_list):
        return image_obj
    '''

    # plant.handle_exception('file could not be opened: %s'
    # % filename,
    #                     flag_exit_if_error,
    #                     flag_no_messages=flag_no_messages,
    #                     verbose=verbose)
    #    return

    if not flag_no_crop:
        plant.apply_crop(image_obj,
                         plant_transform_obj=plant_transform_obj,
                         verbose=verbose)

    if image_obj is None:
        plant.handle_exception('no valid data after cropping '
                               'file: %s' % filename,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
    # if not image_obj.image_loaded:
    #     return image_obj
    # if only_header:
    #    return image_obj
    # image_obj =
    plant.apply_null(image_obj, in_null=in_null, out_null=out_null)
    # image_obj =
    plant.apply_mask(image_obj,
                     plant_transform_obj=plant_transform_obj,
                     verbose=verbose,
                     force=force)
    return image_obj

    # .las
    # if filename.endswith('.las'):
    #    from laspy.file import File
    #    las_file = File(filename, mode = "r")
    #    # return lat_file.points
    #    x_dimension = las_file.X
    #    scale = las_file.header.scale[0]
    #    offset = las_file.header.offset[0]
    #    return x_dimension*scale + offset

    # RAT
    # else:
    #     width = header[1]
    #     length = header[2]
    #     dtype = plant.get_srf_dtype(header[5])
    #     header_2 = np.fromfile(filename, dtype=np.byte, count=80)
    #     image = np.fromfile(filename, dtype=dtype, count=width*length)
    #     image[np.where(image == 0)] = np.nan
    #     return({'image': image, 'width': width, 'length': length,
    #             'dtype': dtype,
    #             'geotransform': geotransform,
    #             'scheme': scheme, 'image_2': None})
    #     # a=lonarr(8)
    #     # a(0)=2
    #     # a(1)=tam(1)
    #     # a(2)=tam(2)
    #     # a(3)=6
    #     # a(4)=101
    #     # a(6)=536871028
    #     # a(7)=1
    #     # ;print,a
    #     #
    #     # b=bytarr(80)
    #     # b=[ 117, 110, 107, 110, 111, 119, 110,  32 , 99 ,111, 110
    # ,116 ,101 ,110 ,116  ,32 , 32  ,32  ,32  ,32 , 32  ,32 , 32 , 32
    # ,32 , 32,  32  ,32 , 32 , 32  ,32  ,32,  32 , 32 , 32 , 32 , 32,  32,$
    #   32,  32 , 32  ,32,  32 , 32,  32  ,32 , 32,  32,  32,  32 , 32
    # ,  32 , 32 , 32 , 32 , 32 , 32 , 32  ,32,  32  ,32,  32 , 32  ,32
    # ,  32  ,32 , 32 , 32 , 32,  32,  32 , 32  ,32,  32,  32,  32,$
    #   32 , 32  ,32,  32]

# def read_csv_data_new(current_file,
#                       first_line


def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.decode('utf-8')


def read_image_csv(filename,
                   filename_orig,
                   plant_transform_obj,
                   # ref_image_obj=None,
                   band=None,
                   key=None,
                   delimiter=None,
                   in_null=None,
                   out_null=None,
                   input_format='CSV',
                   only_header=False,
                   force=None,
                   flag_exit_if_error=True,
                   flag_no_messages=False,
                   verbose=True,
                   image_obj_kwargs={}):
    kwargs = locals()
    # import pandas as pd
    if verbose and not only_header and verbose:
        print('opening: ' + filename + ' (CSV pandas)')

    offset_x = None
    offset_y = None
    width = None
    length = None
    if plant_transform_obj is not None:
        offset_x, offset_y, width, length = plant_transform_obj.crop_window
        if offset_x is None and offset_y is not None:
            offset_x = offset_y
        if width is None and length is not None:
            width = length
    if key is None:
        key_list = None
    else:
        key_list = key.split(',')
        if all([plant.isnumeric(k) for k in key_list]):
            try:
                key_list = [int(k) for k in key_list]
            except ValueError:
                pass
    if delimiter is None:
        delimiter = ','
    skiprows = list(range(offset_x)) if offset_x else None
    header = 'infer' if key_list is None else 0
    # plant.debug(key_list)
    # plant.debug(skiprows)
    # plant.debug(header)
    # plant.debug(width)
    try:
        df = pd.read_csv(filename,
                         sep=delimiter,
                         usecols=key_list,
                         skiprows=skiprows,
                         header=header,
                         # verbose=False,
                         # index_col=band,
                         # parse_dates=True,
                         # infer_datetime_format=True,
                         nrows=width)
    # except pd.errors.ParserError pandas.errors.EmptyDataError:
    except:
        plant.handle_exception('file could not be opened: %s'
                               ' (pandas csv)'
                               % filename,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return
    width, nbands = df.shape
    length_orig = 1
    depth_orig = 1
    '''
    if only_header:
        image_obj = plant.PlantImage(filename=filename_orig,
                               image=None,
                               # width=width_orig,
                               width=width,
                               length=length_orig,
                               depth=depth_orig,
                               nbands=nbands,
                               input_key=key,
                               dtype='str',
                               ref_image_obj=ref_image_obj,
                               plant_transform_obj=plant_transform_obj,
                               file_format=input_format,
                               geotransform=None)
        return image_obj
    '''
    bands_count = 0
    for b, column in enumerate(df.columns):
        if band is not None and b != band:
            continue
        if only_header:
            image = None
        elif isinstance(df[column][0], str):
            from dateutil import parser
            flag_date = True
            try:
                date_array = [parser.parse(d) for d in df[column]]
            except ValueError:
                flag_date = False
            if flag_date:
                image = np.asarray(date_array).reshape(length_orig, width)
            else:
                image = np.asarray(df[column],
                                   dtype=str).reshape(length_orig, width)
        else:
            image = np.asarray(df[column]).reshape(length_orig, width)
        # image = np.asarray(image_list[b],
        #                   dtype=dtype).reshape(length_orig, width)
        # print(f'*** band: {b}')
        # print(f'*** name: {df[column][1]}')
        if bands_count == 0:
            image_obj = plant.PlantImage(
                filename=filename_orig,
                image=image,
                width=width,
                length=length_orig,
                depth=depth_orig,
                input_key=key,
                # dtype='CSV',
                nbands=nbands,
                # ref_image_obj=ref_image_obj,
                plant_transform_obj=plant_transform_obj,
                file_format=input_format,
                geotransform=None,
                **image_obj_kwargs)
        else:
            image_obj.set_image(image, band=bands_count,
                                realize_changes=False)
        # print('*** name set as: ', column)
        image_obj.get_band(band=bands_count).set_name(column)
        bands_count += 1
        # print(f'... image: {id(image_obj)}')
        # print(f'... band {b}: {id(image_obj.get_band(band=b))}')

    prepare_image(image_obj,
                  filename,
                  flag_exit_if_error=flag_exit_if_error,
                  plant_transform_obj=plant_transform_obj,
                  in_null=in_null,
                  out_null=out_null,
                  input_format=input_format,
                  flag_no_crop=True,
                  verbose=verbose,
                  only_header=only_header,
                  flag_no_messages=flag_no_messages,
                  force=force)

    return image_obj


def read_image_csv_python(filename,
                          filename_orig,
                          plant_transform_obj,
                          # ref_image_obj=None,
                          band=None,
                          key=None,
                          delimiter=None,
                          input_format='CSV',
                          in_null=None,
                          out_null=None,
                          only_header=False,
                          force=None,
                          flag_exit_if_error=True,
                          flag_no_messages=False,
                          verbose=True,
                          image_obj_kwargs={}):
    kwargs = locals()
    # args = [kwargs.pop('filename_orig')]
    if delimiter is None:
        delimiter_list = [',', '\t', ' ']
        for delimiter in delimiter_list:
            kwargs['delimiter'] = delimiter
            ret = read_image_csv(**kwargs)
            if ret is not None:
                return ret
        plant.handle_exception('file could not be opened: %s (csv)'
                               % filename,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    import csv
    if verbose and not only_header:
        print('opening: ' + filename + ' (CSV)')

    offset_x = None
    offset_y = None
    width = None
    length = None
    if plant_transform_obj is not None:
        offset_x, offset_y, width, length = plant_transform_obj.crop_window
        if offset_y is None and offset_x is not None:
            offset_y = offset_x
        if width is None and length is not None:
            width = length

    if key is None:
        key_list = []
    else:
        key_list = key.split(',')

    with open(filename, 'rb') as csvfile:
        # dialect = csv.Sniffer().sniff(csvfile.read(1024).decode('utf-8'),
        #                              delimiters=' ')
        # print(dialect)
        # csvfile.seek(0)
        # csv_reader = csv.DictReader(csvfile,
        #                             dialect)
        dtype = np.float32
        image_list = []
        csvfile.seek(0)
        if offset_x is None:
            offset_x = 0
        csv_lines = (row for row in utf_8_encoder(csvfile)
                     if not row.startswith('#'))
        csv_reader = csv.DictReader(csv_lines,
                                    delimiter=delimiter,
                                    quotechar='|')
        if csv_reader is None:
            plant.handle_exception('file could not be opened: %s (csv)'
                                   % filename,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return
        flag_first_value_as_key = False
        for i, line in enumerate(csv_reader):

            # pos is the array position (i+header if numeric)
            pos = i + int(flag_first_value_as_key)
            if i != 0 and pos < offset_x:
                continue
            if (width is not None and pos > width+offset_x):
                break
            if i == 0:
                key_list_file = list(line.keys())
                flag_user_key_list_is_numeric = all([plant.isnumeric(k)
                                                     for k in key_list])
                new_key_list = []

                if len(key_list) == 0:
                    flag_first_value_as_key = all([plant.isnumeric(x)
                                                   for x in key_list_file])
                    new_key_list = key_list_file
                else:
                    new_key_list = [x for x in key_list_file
                                    for k in key_list
                                    if k.strip() == x.strip()]

                    # if list doesn't match column headers
                    if len(new_key_list) == 0:
                        if flag_user_key_list_is_numeric:
                            try:
                                new_key_list = [key_list_file[int(k)]
                                                for k in key_list]
                                flag_first_value_as_key = True
                            except IndexError:
                                pass
                if len(new_key_list) == 0:
                    print('ERROR key(s) %s not found in %s'
                          % (str(key_list), filename))
                    return
                key_list = new_key_list
                if (flag_first_value_as_key and
                        (offset_x is None or offset_x == 0)):
                    image_list = [[] for j in key_list]
                    for j, current_key in enumerate(key_list):
                        image_list[j].append(np.float(current_key))
            pos = i + int(flag_first_value_as_key)

            if pos < offset_x:
                continue
            if len(image_list) == 0:
                image_list = [[] for j in key_list]
            for j, current_key in enumerate(key_list):
                value = line[current_key]
                if plant.isnumeric(value):
                    value = np.float(value)
                elif not value:
                    value = np.float(np.nan)
                else:
                    value = str(value)
                    # print(value)
                    dtype = None
                # if (offset_x is None or
                #        (i + int(flag_first_value_as_key)) > offset_x):
                image_list[j].append(value)
        # print(only_header, image)
        # width_orig = len(csv_lines)-1+int(flag_first_value_as_key)
        # if width is None:
        width = len(image_list[0])
        # print('width: ', width)
        # print('length: ', length)
        length_orig = 1
        depth_orig = 1
        if only_header:
            image_obj = plant.PlantImage(
                filename=filename_orig,
                image=None,
                # width=width_orig,
                width=width,
                length=length_orig,
                depth=depth_orig,
                nbands=len(image_list),
                input_key=key,
                # dtype=dtype,
                # ref_image_obj=ref_image_obj,
                plant_transform_obj=plant_transform_obj,
                file_format=input_format,
                geotransform=None,
                **image_obj_kwargs)
            return image_obj
        for b in range(len(image_list)):
            image = np.asarray(image_list[b],
                               dtype=dtype).reshape(length_orig, width)
            if b == 0:
                image_obj = plant.PlantImage(
                    filename=filename_orig,
                    image=image,
                    # width=width_orig,
                    width=width,
                    length=length_orig,
                    depth=depth_orig,
                    input_key=key,
                    # dtype=dtype,
                    # ref_image_obj=ref_image_obj,
                    plant_transform_obj=plant_transform_obj,
                    file_format=input_format,
                    geotransform=None,
                    **image_obj_kwargs)
            else:
                image_obj.set_image(image, band=b)

        prepare_image(image_obj,
                      filename,
                      flag_exit_if_error=flag_exit_if_error,
                      plant_transform_obj=plant_transform_obj,
                      in_null=in_null,
                      out_null=out_null,
                      flag_no_crop=True,
                      input_format=input_format,
                      verbose=verbose,
                      only_header=only_header,
                      flag_no_messages=flag_no_messages,
                      force=force)

        # image_obj = plant.apply_null(image_obj,
        #                       in_null=in_null,
        #                       out_null=out_null)
        # image_obj = plant.apply_mask(image_obj,
        #                       plant_transform_obj=plant_transform_obj,
        #                       verbose=verbose,
        #                       force=force)
        return image_obj
    return


def read_csv_data_lat_lon(filename,
                          verbose=True,
                          lat_column=None,
                          lon_column=None,
                          data_column_list=None,
                          lat_text='lat',
                          lon_text='lon',
                          data_text_list=None,
                          separator=',',
                          n_lines=None,
                          lat_arr=None,
                          lon_arr=None,
                          first_line=None,
                          first_header_column=0,
                          flag_no_messages=False,
                          flag_exit_if_error=True,
                          trim_whitespaces=False):

    if flag_no_messages:
        verbose = False

    if not plant.isfile(filename):
        plant.handle_exception(f'file {filename} not found or invalid',
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    if ((data_text_list is None or len(data_text_list) == 0) and
            (data_column_list is None or len(data_column_list) == 0)):
        plant.handle_exception('please specify CSV data text or'
                               ' CSV data column number',
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        # print('ERROR please specify CSV data text or '
        #      'CSV data column number')
        return

    lat_vect = []
    lon_vect = []
    # lat_column = None
    # lon_column = None
    # data_column = None
    if data_column_list is None and data_text_list is not None:
        data_column_list = [None for i in range(len(data_text_list))]
    # data_vect = [] # [[]] * len(data_column_list)
    data_vect = [[] for i in range(len(data_column_list))]
    # for i in range(len(data_column_list)):
    #    data_vect.append([])
    # print('data column list: ', data_column_list)
    # print('data text list: ', data_text_list)
    # print('data: ', data)

    if verbose:
        print('opening: %s (CSV)' % (filename))
        print('separator: "%s"' % (separator))

    with open(filename, 'r', encoding="ISO-8859-1") as f:
        lines = f.readlines()
        if n_lines is not None:
            lines = lines[0:n_lines+1]
        else:
            lines = lines[0:]

        if first_line is not None:
            lines = lines[first_line:]

        line = lines[0].replace('\n', '')
        if trim_whitespaces:
            line = ' '.join(line.split())

        line_splitted = line.split(separator)
        line_splitted = line_splitted[first_header_column:]

        # finding key positions
        if (lat_column is None or
                lon_column is None or
                any([plant.isnan(x) for x in data_column_list])):
            for j, element in enumerate(line_splitted):
                element = element.strip()
                if (lat_text == element):
                    lat_column = j
                    if verbose:
                        print('latitude in column: '+str(j) +
                              '   label: '+element)
                    continue
                elif (lon_text == element):
                    if verbose:
                        print('longitude in column: '+str(j) +
                              '   label: '+element)
                    lon_column = j
                    continue
                elif (element in data_text_list):
                    index = data_text_list.index(element)
                    if verbose:
                        print('data %s in column: %d'
                              % (element, j))
                        print('   label: '+element)
                    data_column_list[index] = j
                    continue
        for i, data_column in enumerate(data_column_list):
            if data_column is None:
                if data_text_list[i]:
                    print('WARNING data column not found: %s in %s'
                          % (data_text_list[i],
                             str(line_splitted)))
                else:
                    print('WARNING data column not found')
                print('CSV separator: "%s"' % (separator))
                return

        # appending data (line by line)
        lines = lines[1:]
        for i, line in enumerate(lines):
            line = line.replace('\n', '')
            if trim_whitespaces:
                line = ' '.join(line.split())
            line_splitted = line.split(separator)
            data = []
            for j in range(2+len(data_column_list)):
                if (j == 0 and
                        lat_column is not None and
                        lat_text is not None):
                    current_column = lat_column
                    current_text = lat_text
                elif (j == 1 and
                        lon_column is not None and
                        lon_text is not None):
                    current_column = lon_column
                    current_text = lon_text
                elif j < 2:
                    continue
                else:
                    current_column = data_column_list[j-2]
                    current_text = data_text_list[j-2]

                output_value = None
                try:
                    output_value = float(line_splitted[current_column])
                except KeyboardInterrupt:
                    raise plant.PlantExceptionKeyboardInterrupt
                except IndexError:
                    print('WARNING error reading %s '
                          ' (column: %d) from CSV '
                          'file line: %d'
                          % (current_text, current_column, i))
                    continue
                except:
                    print('WARNING error reading %s '
                          ' (column: %d) from CSV '
                          'file line: %d'
                          % (current_text, current_column, i))
                    continue

                if j == 0:
                    lat = output_value
                    if (lat_arr is not None and
                        (lat < lat_arr[0] or
                         lat > lat_arr[1])):
                        break
                elif j == 1:
                    lon = output_value
                    if lon > 180:
                        lon -= 360
                    if (lon_arr is not None and
                        (lon < lon_arr[0] or
                         lon > lon_arr[1])):
                        break
                else:
                    data.append(output_value)

            if (lat_arr is not None and
                (lat < lat_arr[0] or
                 lat > lat_arr[1])):
                continue
            if (lon_arr is not None and
                (lon < lon_arr[0] or
                 lon > lon_arr[1])):
                continue

            if (lat_column is not None and
                    lat_text is not None):
                lat_vect.append(lat)
            if (lon_column is not None and
                    lon_text is not None):
                lon_vect.append(lon)

            for j in range(len(data_column_list)):
                (data_vect[j]).append(data[j])
    # print('*** data_vect: ', data_vect)
    # print('*** lat_vect: ', lat_vect)
    # print('*** lon_vect: ', lon_vect)
    return(data_vect, lat_vect, lon_vect)


def save_image_figure(*args, **kwargs):
    force = kwargs.get('force', None)
    output_format = kwargs.pop('output_format', 'PNG')
    output_file = args[-1]
    args = args[0:-1]
    n_images = len(args)
    update_image = plant.overwrite_file_check(output_file,
                                              force=force)
    verbose = kwargs.get('verbose', True)
    if not update_image:
        print('WARNING file not updated: %s'
              % (output_file))
        return
    if n_images > 4:
        print('WARNING output has more than four '
              'channels. Only the first four channels '
              'will be saved')
    image = plant.image_to_figure(*args, **kwargs)
    # print('*** image.shape: ', image.shape)
    # print('*** ', len(image.shape))
    # print('*** ', image.shape[2])
    # if len(image.shape) == 3 and image.shape[2] == 4:
    #     from PIL import Image
    #     # image = [image[:, :, i] for i in range(4)]
    #     im = Image.fromarray(image.astype('uint8'))
    #     image = im.convert('RGB')
    #     print('*** new code!!!')
    #     print('*** mode: ', image.mode)
    #     for i in range(3):
    #         image[:, :, i] = image[:, :, i]*image[:, :, 3]
    #     image = image[:, :, 0:3]
    # print('*** new image.shape: ', image.shape)
    # mpimg.imsave(output_file,
    #              rgb_im,
    #              format=output_format)
    # if img.mode == 'RGBA':
    #     img = img.convert('RGB')
    mpimg.imsave(output_file,
                 image,
                 format=output_format.lower())
    plant.plant_config.output_files.append(output_file)
    if verbose and plant.isfile(output_file):
        print('## file saved: %s (%s)'
              % (output_file,
                 output_format))


def save_image_kml(*args, **kwargs):

    # to prevent opening the display:
    import matplotlib as mpl
    mpl.use('Agg')

    ret = None
    verbose = kwargs.get('verbose', True)
    output_format = kwargs.get('output_format', 'KML')
    if verbose and output_format:
        print(f'## saving image with format: {output_format}')

    with plant.PlantIndent():
        ret = _save_image_kml(*args, **kwargs)
    return ret

def _save_image_kml(*args, **kwargs):
    '''
    create kml file
    '''
    # from plant_ui import save_cmap, save_ctable, \
    #     image_obj_ctable_expand
    # image = kwargs.get('image', None)
    image_obj = kwargs.get('image_obj', None)
    image = kwargs.get('image', None)
    output_file = kwargs.get('output_file', None)
    nbands = kwargs.get('nbands', None)
    cmap = kwargs.get('cmap', plant.DEFAULT_CMAP)
    cmap_crop_min = kwargs.get('cmap_crop_min', None)
    cmap_crop_max = kwargs.get('cmap_crop_max', None)

    cmap_min = kwargs.pop('cmap_min', None)
    cmap_max = kwargs.pop('cmap_max', None)
    background_color = kwargs.pop('background_color', None)
    flag_cmap_already_expanded = kwargs.get('flag_cmap_already_expanded',
                                            False)







    
    ctable = kwargs.get('ctable', None)
    flag_scale_data = kwargs.get('flag_scale_data', None)

    # cmap, ticklabels = ctable_to_cmap(ctable, data=data)

    output_format = kwargs.get('output_format', 'KML')
    extension = '.'+output_format.lower()
    percentile = kwargs.get('percentile', None)

    # flag_percentile_not_fixed = percentile is None
    # if percentile is None:
    # percentile = 95

    verbose = kwargs.get('verbose', True)
    force = kwargs.get('force', None)
    data_output_file = kwargs.get('data_output_file',
                                  output_file.replace(extension, ''))
    geotransform = kwargs.get('geotransform')
    if geotransform is None:
        print('ERROR geo-reference not found')
        return
    data_output_format = 'PNG'
    file_list = []

    if output_format.upper() == 'KMZ' and output_file.endswith('.kmz'):
        kml_file = output_file.replace('.kmz', '_'+str(time.time())+'.kml')
    elif output_format.upper() == 'KMZ':
        kml_file = output_file + '_'+str(time.time())+'.kml'
    elif output_file.endswith('.kml'):
        kml_file = output_file
    else:
        kml_file = output_file + '.kml'

    # geotransform=geotransform_edges,
    with open(kml_file, 'w') as fp:

        if image_obj is not None:
            cbar_file = 'cmap_'+str(time.time())+'.png'
            # if ctable is not None:
            ret = plant.image_obj_ctable_expand(image_obj,
                                                ctable=ctable)
            image_obj = ret['image_obj']
            ctable = ret['ctable']
            if ctable is not None:
                cmap = None
            length = image_obj.length
            width = image_obj.width
            projection = image_obj.projection
            nbands = image_obj.nbands
        else:
            # length = image.shape[0]
            # width = image.shape[1]
            depth, length, width = plant.get_image_dimensions(image)
            projection = None
            nbands = 1
        # print('*** nbands: ', nbands)
        if nbands == 1 and cmap.upper() == 'RGB':
            # print('WARNING invalid cmap (RGB) for single band input.')
            cmap = 'gray'

        output_projection = 'WGS84'
        flag_fix_projection = not plant.compare_projections(
            projection, output_projection)

        outputSRS = plant.get_projection_proj4(output_projection)
        if flag_fix_projection:
            temp_file_in = plant.get_temporary_file(suffix='_in',
                                                    ext='bin',
                                                    append=True)
            temp_file_out = plant.get_temporary_file(suffix='_out',
                                                     ext='bin')
            plant.save_image(image_obj, temp_file_in,
                             output_format='ENVI',
                             force=True)
            plant.Warp(temp_file_out,
                       temp_file_in,
                       # options=options,
                       dstSRS=outputSRS,
                       flag_return=False,
                       srcNodata='nan',
                       format='ENVI')
            plant.append_temporary_file(temp_file_out)

            # the wrapper above should return the right image_obj

            image_obj = plant.read_image(temp_file_out)
            projection = image_obj.projection
            geotransform = image_obj.geotransform
            depth, length, width = plant.get_image_dimensions(image_obj)
            # print('new projection: ', projection)
            # print('new geotransform: ', geotransform)
            # print('new depth: ', depth)
            # print('new length: ', length)
            # print('new width: ', width)


        ret_dict = plant.get_coordinates(geotransform=geotransform,
                                         # flag_output_in_wgs84=True,
                                         projection=projection,
                                         lat_size=length,
                                         lon_size=width)
        if ret_dict is None:
            return
        lat_arr = ret_dict['lat_arr']
        lon_arr = ret_dict['lon_arr']



        # print('lat_arr: ', lat_arr)
        # print('lon_arr: ', lon_arr)

        # step_lat = ret_dict['step_lat']
        # step_lon = ret_dict['step_lon']
        # lat_size = ret_dict['lat_size']
        # lon_size = ret_dict['lon_size']
        if output_format.upper() == 'KMZ':
            tile_size = plant.KMZ_TILE_SIZE
        else:
            tile_size = plant.KML_TILE_SIZE
        n_tiles_x = np.ceil(float(width)/tile_size)
        n_tiles_y = np.ceil(float(length)/tile_size)
        n_tiles = int(n_tiles_x*n_tiles_y)
        if n_tiles > 1 and verbose:
            print(f'total number of tiles: {n_tiles}')
        lat_tile_range = (tile_size *
                          (max(lat_arr)-min(lat_arr)) / length)
        lon_tile_range = (tile_size *
                          (max(lon_arr)-min(lon_arr)) / width)

        mean_lon = (lon_arr[0]+lon_arr[1])/2
        mean_lat = (lat_arr[0]+lat_arr[1])/2
        name = os.path.basename(output_file).replace(extension, '')
        fp.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        fp.write('<kml xmlns="http://www.opengis.net/kml/2.2">\n')
        fp.write('  <Document>\n')

        # mean_lat_str = 'S' if mean_lat < 0 else 'N'
        # mean_lat_str += abs(mean_lat)
        # mean_lon_str = 'W' if mean_lon < 0 else 'E'
        # mean_lon_str += abs(mean_lon)

        # fp.write('\t<Placemark id="mountainpin1">\n')
        fp.write('\t <LookAt>\n')
        fp.write(f'\t\t <longitude>{mean_lon}</longitude>\n')
        fp.write(f'\t\t <latitude>{mean_lat}</latitude>\n')
        fp.write('\t\t <altitude>100000</altitude>\n')
        # fp.write('\t\t <range>500</range>\n')
        fp.write('\t\t <tilt>0</tilt>\n')
        fp.write('\t\t <heading>0</heading>\n')
        fp.write('\t\t <altitudeMode>relativeToGround</altitudeMode>\n')
        # fp.write('\t\t <altitudeMode>clampToGround</altitudeMode>\n')
        fp.write('\t </LookAt>\n')

        '''
        fp.write('\t<Placemark id="normalPlacemark">\n')
        fp.write('\t\t<name>%s</name>\n' % name)
        fp.write('\t\t<description>%s</description>\n' % name)
        fp.write('\t\t<visibility>0</visibility>\n')
        fp.write('\t\t<Point>\n')
        fp.write('\t\t<coordinates>%f,%f,0</coordinates>\n'
                 % (mean_lon, mean_lat))
        fp.write('\t\t</Point>\n')
        fp.write('\t</Placemark>\n')
        '''
        
        fp.write('\t<Region>\n')
        fp.write('\t\t<LatLonAltBox>\n')
        fp.write('\t\t<north>%f</north>\n' % lat_arr[1])
        fp.write('\t\t<south>%f</south>\n' % lat_arr[0])
        fp.write('\t\t<east>%f</east>\n' % lon_arr[1])
        fp.write('\t\t<west>%f</west>\n' % lon_arr[0])
        fp.write('\t\t</LatLonAltBox>\n')
        # fp.write('\t\t<Lod>\n')
        # fp.write('\t\t<minLodPixels>128</minLodPixels>\n')
        # fp.write('\t\t<maxLodPixels>-1</maxLodPixels>\n')
        # fp.write('\t\t</Lod>\n')
        fp.write('\t</Region>\n')

        cmap_min_band_list = []
        cmap_max_band_list = []
        cbar_file_list = []
        flag_one_cmap = (flag_cmap_already_expanded or
                         (cmap is not None and
                          flag_scale_data is False and
                          not hasattr(cmap_min, '__getitem__') and
                          not hasattr(cmap_max, '__getitem__')))
        # print('*** flag_one_cmap: ', flag_one_cmap)
        if ctable is None and not flag_one_cmap:
            for k in range(min([nbands, 3])):
                # print('band: ', k, image_obj)
                cbar_prefix = data_output_file+f'_colorbar_{k}'
                if output_format.upper() != 'KMZ':
                    cbar_file = cbar_prefix+'.png'
                else:
                    cbar_file = (cbar_prefix+'_' +
                                 str(time.time())+'.png')
                # cbar_file = 'cmap_temp.png'
                if image_obj is not None:
                    current_image = image_obj.get_image(band=k)
                if current_image is not None:
                    image = current_image
                if 'complex' in plant.get_dtype_name(
                        image.dtype).lower():
                    image = np.absolute(image)
                # if image is None:
                #     continue
                # if image is None:
                #     continue
                cmap_min_band = None
                cmap_max_band = None
                percentile_temp = percentile
                if percentile_temp is None:
                    percentile_temp = plant.DEFAULT_MIN_MAX_PERCENTILE
                # if ctable is None:
                while percentile_temp <= 100:
                    if (cmap_min is None or
                            cmap_max is None or
                            not hasattr(cmap_min, '__getitem__') or
                            not hasattr(cmap_max, '__getitem__')):
                        ret = plant.get_min_max_percentile(image,
                                                           percentile_temp,
                                                           cmap_min,
                                                           cmap_max,
                                                           band=k)
                        cmap_min_band, cmap_max_band = ret
                    if hasattr(cmap_min, '__getitem__'):
                        cmap_min_band = plant.get_element_from_arg(cmap_min,
                                                                   index=k)
                    if hasattr(cmap_max, '__getitem__'):
                        cmap_max_band = plant.get_element_from_arg(cmap_max,
                                                                   index=k)
                        
                    # print('*** percentile_temp: ',
                    #      percentile_temp, cmap_min_band, cmap_max_band)
                    
                    if (cmap_min_band != cmap_max_band or
                        (cmap_min_band == cmap_max_band and
                            percentile is not None)):
                        break
                    percentile_temp += 1

                if (cmap_min_band is None or
                        cmap_max_band is None):
                    print('ERROR determining min and max for percentile:'
                          f' {percentile}')
                cmap_min_band = plant.round_to_sig_figs(cmap_min_band, 3)
                cmap_max_band = plant.round_to_sig_figs(cmap_max_band, 3)
                if verbose:
                    print('band: %d' % k)
                    print('    color map: ', cmap)
                    if percentile is not None:
                        print('    percentile: ', percentile)
                    print('    color map min: ', cmap_min_band)
                    print('    color map max: ', cmap_max_band)
                    print('    background color: ', background_color)
                # if (cmap is not None and
                #        cmap_min_band is not None and
                #        cmap_max_band is not None):
                if (cmap is not None and
                    (isinstance(cmap, mcolors.LinearSegmentedColormap) or
                     cmap.upper() != 'RGB')):
                    # if ctable is not None:
                    #    plant.save_ctable(ctable, cbar_file)
                    # else:
                    # if cmap is not None:
                    # if cmap.upper() != 'RGB':
                    if output_format.upper() == 'KMZ':
                        plant.append_temporary_file(cbar_file)
                    plant.save_cmap(cmap,
                                    cbar_file,
                                    cmap_min_band,
                                    cmap_max_band,
                                    verbose=verbose)
                    '''
                    if plant.isfile(cbar_file) and verbose:
                        print(f'file saved: {cbar_file}')
                    '''
                    file_list.append(cbar_file)
                    cbar_file_list.append(cbar_file)
                # if ctable is None:
                if flag_scale_data is not False:
                    image = plant.scale_data(image, cmap_min_band,
                                             cmap_max_band, 1.0)
                # cmap_min = plant.round_to_sig_figs(cmap_min_band, 3)
                # cmap_max = plant.round_to_sig_figs(cmap_max_band, 3)
                cmap_min_band_list.append(cmap_min_band)
                cmap_max_band_list.append(cmap_max_band)
                image_obj.set_image(image, band=k)
        else:
            if output_format.upper() == 'KMZ':
                plant.append_temporary_file(cbar_file)
            if flag_one_cmap:

                if (cmap_min is None or
                        cmap_max is None):
                    if image is None and image_obj is not None:
                        image = image_obj.image
                    if percentile is None:
                        percentile = plant.DEFAULT_MIN_MAX_PERCENTILE
                    ret = plant.get_min_max_percentile(image,
                                                       percentile,
                                                       cmap_min,
                                                       cmap_max)
                    cmap_min, cmap_max = ret

                cmap_min = plant.round_to_sig_figs(cmap_min, 3)
                cmap_max = plant.round_to_sig_figs(cmap_max, 3)
                plant.save_cmap(cmap,
                                cbar_file,
                                cmap_min,
                                cmap_max,
                                vmin=cmap_crop_min,
                                vmax=cmap_crop_max,
                                verbose=verbose)
            else:
                plant.save_ctable(ctable,
                                  cbar_file,
                                  verbose=verbose)
            '''
            if plant.isfile(cbar_file) and verbose:
                print(f'file saved: {cbar_file}')
            '''
            file_list.append(cbar_file)
            add_legend_to_kml(fp, cbar_file)
            # cbar_file_list = [cbar_file]

        # print('*** len(cbar_file_list): ', cbar_file_list)
        # print('*** flag_one_cmap: ', flag_one_cmap)
        # print('*** cmap: ', cmap)
        # print('*** nbands: ', nbands)

        if (ctable is None and  not flag_one_cmap and
            (not flag_cmap_already_expanded and
             cmap is not None and
             (isinstance(cmap, mcolors.LinearSegmentedColormap) or
              cmap.upper() != 'RGB')) or nbands == 1):
            valid_tiles_list = [False for t in range(n_tiles)]
            for band_count in range(min([nbands, 3])):
                name = os.path.basename(output_file).replace(
                    extension, ' (band: %d)' % (band_count+1))
                fp.write('\t<Folder>\n')
                fp.write('\t\t<name>%s</name>\n' % name)
                if band_count == 0 and nbands == 1:
                    visibility = 1
                else:
                    visibility = 0
                fp.write('\t\t<visibility>%d</visibility>\n'
                         % (visibility))
                fp.write('\t\t<open>0</open>\n')
                if nbands > 1:
                    image = image_obj.getImage(band=band_count)
                    band_suffix = '_band_'+str(band_count)
                    current_data_output_file = (data_output_file +
                                                band_suffix)
                else:
                    current_data_output_file = data_output_file
                valid_tile_count = 0

                if nbands > 1:
                    name = f'Legend (band: {band_count+1})'
                if not flag_one_cmap:
                    add_legend_to_kml(fp, cbar_file_list[band_count],
                                      name=name, visibility=visibility)
                # elif flag_one_cmap and band_count==0:
                #    add_legend_to_kml(fp, cbar_file,
                #                      name=name,
                #                      visibility=visibility)

                for tile_count in range(n_tiles):
                    if n_tiles != 1:
                        if verbose:
                            print(f'processing tile {tile_count+1}/'
                                  f'{n_tiles}...')
                        i = int(tile_count/n_tiles_x)
                        j = np.mod(tile_count, n_tiles_x)
                        ret = get_tile(i,
                                       j,
                                       lat_arr,
                                       lon_arr,
                                       lat_tile_range,
                                       lon_tile_range,
                                       length,
                                       width,
                                       tile_size)
                        tile_suffix = '_tile_'+str(valid_tile_count)
                        bbox, start_y, end_y, start_x, end_x = ret
                        image_tile = image[start_y:end_y, start_x:end_x]
                        current_data_output_file_tile = \
                            (current_data_output_file +
                             tile_suffix)
                        # if output_format.upper() == 'KMZ':
                        #     current_data_output_file_tile += str(time.time())
                        name = os.path.basename(output_file)
                        if nbands == 1:
                            name = name.replace(extension,
                                                '(tile: %d)' % (tile_count))
                        else:
                            name = name.replace(extension,
                                                ' (band: %d, tile: %d)'
                                                % (band_count,
                                                   tile_count))
                        name = os.path.basename(name)
                    else:
                        current_data_output_file_tile = \
                            current_data_output_file
                        bbox = plant.get_bbox(lat_arr, lon_arr)
                        image_tile = image
                        name = os.path.basename(output_file)
                        if nbands == 1:
                            name = name.replace(extension, '')
                        else:
                            name = name.replace(extension,
                                                ' (band: %d)' % (band_count))
                    if output_format.upper() == 'KMZ':
                        current_data_output_file_tile += '_'+str(time.time())
                    current_data_output_file_tile += '.png'
                    if np.all(plant.isnan(image_tile)):
                        continue
                    valid_tiles_list[tile_count] = True
                    valid_tile_count += 1
                    if output_format.upper() == 'KMZ':
                        plant.append_temporary_file(
                            current_data_output_file_tile)
                    save_image_figure(image_tile,
                                      current_data_output_file_tile,
                                      output_format=data_output_format,
                                      # percentile=percentile,
                                      cmap_min=0,
                                      cmap_max=1,
                                      cmap=cmap,
                                      flag_scale_data=False,
                                      background_color=background_color,
                                      # verbose=verbose,
                                      verbose=False,
                                      # prefix='    ',
                                      force=force)
                    if not plant.isfile(current_data_output_file_tile):
                        print('ERROR saving file'
                              f' {current_data_output_file_tile}')
                    elif verbose:
                        print('## file saved:'
                              f' {current_data_output_file_tile}')
                    file_list.append(current_data_output_file_tile)
                    plant.append_temporary_file(current_data_output_file_tile)
                    add_layer_to_kml(fp,
                                     name,
                                     os.path.basename(
                                         current_data_output_file_tile),
                                     bbox,
                                     visibility=visibility)

                fp.write('\t</Folder>\n')
        else:
            valid_tiles_list = [True for t in range(n_tiles)]

        if nbands > 1:
            name = os.path.basename(output_file.replace(extension, ' (RGB)'))
            fp.write('\t<Folder>\n')
            fp.write('\t\t<name>%s</name>\n' % name)
            visibility = 1
            fp.write('\t\t<visibility>%d</visibility>\n'
                     % (visibility))
            fp.write('\t\t<open>0</open>\n')
            valid_tile_count = 0

            if ctable is None and not flag_one_cmap:
                cbar_file_list = []
                for i, cmap in enumerate(['Reds', 'Greens', 'Blues']):
                    cbar_prefix = data_output_file+f'_colorbar_{cmap}'
                    if output_format.upper() != 'KMZ':
                        cbar_file = cbar_prefix+'.png'
                    else:
                        cbar_file = (cbar_prefix+'_' +
                                     str(time.time())+'.png')
                    if (i > len(cmap_min_band_list)-1 or
                            i > len(cmap_max_band_list)-1):
                        break
                    plant.append_temporary_file(cbar_file)
                    plant.save_cmap(cmap,
                                    cbar_file,
                                    cmap_min_band_list[i],
                                    cmap_max_band_list[i],
                                    verbose=verbose)
                    '''
                    if plant.isfile(cbar_file) and verbose:
                        print(f'file saved: {cbar_file}')
                    '''
                    cbar_file_list.append(cbar_file)
                cbar_prefix = data_output_file+'_colorbar_rgb'
                if output_format.upper() != 'KMZ':
                    cbar_file = cbar_prefix+'.png'
                else:
                    cbar_file = (cbar_prefix+'_' +
                                 str(time.time())+'.png')
                image = plant.concatenate_y_filelist(cbar_file_list)
                if output_format.upper() == 'KMZ':
                    plant.append_temporary_file(cbar_file)
                plant.save_image(image,
                                 cbar_file,
                                 verbose=verbose)
                '''
                if plant.isfile(cbar_file) and verbose:
                    print(f'file saved: {cbar_file}')
                '''
                file_list.append(cbar_file)
                plant.append_temporary_file(cbar_file)
            name = f'Legend (RGB)'
            add_legend_to_kml(fp, cbar_file, name=name,
                              visibility=True)

            for tile_count in range(n_tiles):
                if not valid_tiles_list[tile_count]:
                    continue
                if n_tiles != 1:
                    i = int(tile_count/n_tiles_x)
                    j = np.mod(tile_count, n_tiles_x)
                    ret = get_tile(i,
                                   j,
                                   lat_arr,
                                   lon_arr,
                                   lat_tile_range,
                                   lon_tile_range,
                                   length,
                                   width,
                                   tile_size)
                    bbox, start_y, end_y, start_x, end_x = ret
                    tile_suffix = '_tile_'+str(valid_tile_count)
                    data_output_file_tile = (data_output_file +
                                             tile_suffix)
                    name = output_file.replace(extension,
                                               ' (RGB tile %d)'
                                               % (tile_count))
                else:
                    data_output_file_tile = data_output_file
                    bbox = plant.get_bbox(lat_arr, lon_arr)
                    name = output_file.replace(extension,
                                               ' (RGB)')
                args = []
                for k in range(min([nbands, 3])):
                    image = image_obj.get_image(band=k)
                    if n_tiles != 1:
                        image = image[start_y:end_y, start_x:end_x]
                    args.append(image)
                valid_tile_count += 1
                if output_format.upper() == 'KMZ':
                    data_output_file_tile += '_'+str(time.time())
                data_output_file_tile += '.png'
                args.append(data_output_file_tile)
                if output_format.upper() == 'KMZ':
                    plant.append_temporary_file(
                        data_output_file_tile)
                save_image_figure(*args,
                                  output_format=data_output_format,
                                  # percentile=percentile,
                                  cmap_min=0,
                                  cmap_max=1,
                                  flag_scale_data=False,
                                  background_color=background_color,
                                  verbose=False,
                                  # prefix='    ',
                                  force=force)
                if not plant.isfile(data_output_file_tile):
                    print('ERROR saving file'
                          f' {current_data_output_file_tile}')
                elif verbose:
                    print('## file saved:'
                          f' {data_output_file_tile}')
                file_list.append(data_output_file_tile)
                plant.append_temporary_file(data_output_file_tile)
                add_layer_to_kml(fp,
                                 name,
                                 data_output_file_tile,
                                 bbox,
                                 visibility=True)
            fp.write('\t</Folder>\n')
        fp.write('  </Document>\n')
        fp.write('</kml>\n')
    if not plant.isfile(kml_file):
        return
    if output_format.upper() == 'KMZ':
        plant.append_temporary_file(kml_file)
    file_list.append(kml_file)
    if verbose:
        print('## file saved: %s (KML)' % kml_file)
    if output_format.upper() != 'KMZ':
        plant.plant_config.output_files.append(kml_file)
    else:
        if verbose:
            print('compressing files into %s...'
                  % (output_file))
        # if plant.isfile(output_file):
        #    remove(output_file)
        with ZipFile(output_file, 'w') as myzip:
            for filename in file_list:
                myzip.write(filename,
                            os.path.basename(filename))
            # for filename in file_list:
                # print('removing: ' + filename +'...')
                # remove(filename)
        plant.plant_config.output_files.append(output_file)
        if plant.isfile(output_file) and verbose:
            print('## file saved: %s (KMZ)' % output_file)


def get_tile(i,
             j,
             lat_arr,
             lon_arr,
             lat_tile_range,
             lon_tile_range,
             length,
             width,
             tile_size):
    start_lat = max(lat_arr)-i*lat_tile_range
    end_lat = max(lat_arr)-(i+1)*lat_tile_range
    end_lat = max([end_lat, min(lat_arr)])
    start_lon = min(lon_arr)+j*lon_tile_range
    end_lon = min(lon_arr)+(j+1)*lon_tile_range
    end_lon = min([end_lon, max(lon_arr)])
    bbox = plant.get_bbox([end_lat, start_lat],
                          [start_lon, end_lon])
    start_y = int(i*tile_size)
    end_y = int((i+1)*tile_size)
    end_y = min([end_y, length])
    start_x = int(j*tile_size)
    end_x = int((j+1)*tile_size)
    end_x = min([end_x, width])
    return(bbox, start_y, end_y, start_x, end_x)


def add_legend_to_kml(fp, icon, name='Legend', visibility=1):
    fp.write('\t\t<ScreenOverlay>')
    fp.write('\t\t\t<name>%s</name>' % name)
    fp.write('\t\t\t<visibility>%d</visibility>' % visibility)
    fp.write('\t\t\t<Icon>')
    fp.write('\t\t\t<href>%s</href>' % os.path.basename(icon))
    fp.write('\t\t\t</Icon>')
    fp.write('\t\t\t<overlayXY x="0" xunits="fraction" '
             'y="0.0" yunits="fraction" />')
    fp.write('\t\t\t<screenXY x="0" xunits="fraction" '
             'y="0.02" yunits="fraction" />')
    fp.write('\t\t\t<rotationXY x="0" xunits="fraction" '
             'y="0" yunits="fraction" />')
    image_obj = plant.read_image(icon, verbose=False)
    # 300, 50
    if image_obj is None:
        print(f'ERROR creating {icon}')
        return
    width = image_obj.width/2
    length = image_obj.length/2
    fp.write(f'\t\t\t<size x="{width}" xunits="pixels"'
             f' y="{length}" yunits="pixels" />')
    fp.write('\t\t</ScreenOverlay>')


def add_layer_to_kml(fp, name, icon, bbox, visibility=1):
    fp.write('\t\t<GroundOverlay>\n')
    fp.write('\t\t\t<name>%s</name>\n' % name)
    fp.write('\t\t\t<visibility>%d</visibility>\n' % visibility)
    fp.write('\t\t\t<color>ffffffff</color>\n')
    fp.write('\t\t\t<drawOrder>1</drawOrder>\n')
    fp.write('\t\t\t<Icon>\n')
    fp.write('\t\t\t\t<href>%s</href>\n' % os.path.basename(icon))
    fp.write('\t\t\t</Icon>\n')
    fp.write('\t\t\t<LatLonBox>\n')
    fp.write('\t\t\t\t<north>%f</north>\n' % bbox[1])
    fp.write('\t\t\t\t<south>%f</south>\n' % bbox[0])
    fp.write('\t\t\t\t<east>%f</east>\n' % bbox[2])
    fp.write('\t\t\t\t<west>%f</west>\n' % bbox[3])
    fp.write('\t\t\t</LatLonBox>\n')
    fp.write('\t\t</GroundOverlay>\n')

# def add_network_link_to_kml(fp, name, icon, bbox):
#     fp.write('\t\t<NetworkLink>\n')
#     fp.write('\t\t\t<name>%s</name>\n' %(name))
#     fp.write('\t\t\t<Region>\n')
#     fp.write('\t\t\t<Lod>\n')
#     fp.write('\t\t\t<minLodPixels>200</minLodPixels>\n')
#     fp.write('\t\t\t<maxLodPixels>-1</maxLodPixels>\n')
#     fp.write('\t\t\t</Lod>\n')
#     fp.write('\t\t\t<LatLonAltBox>\n')
#     fp.write('\t\t\t\t<north>%f</north>\n' % bbox[1])
#     fp.write('\t\t\t\t<south>%f</south>\n' % bbox[0])
#     fp.write('\t\t\t\t<east>%f</east>\n' % bbox[2])
#     fp.write('\t\t\t\t<west>%f</west>\n' % bbox[3])
#     fp.write('\t\t\t</LatLonAltBox>\n')
#     fp.write('\t\t\t</Region>\n')
#     fp.write('\t\t\t<Link>\n')
#     fp.write('\t\t\t<href>%s</href>\n' %(icon))
#     fp.write('\t\t\t<viewRefreshMode>onRegion</viewRefreshMode>\n')
#     fp.write('\t\t\t</Link>\n')
#     fp.write('\t\t</NetworkLink>\n')


def save_image(input_data,
               output_file,
               # band=None,
               verbose=True,
               save_header_only=False,
               force=None,
               geotransform=None,
               projection=None,
               nbands=None,
               lat_size=None,
               lon_size=None,
               # dtype=None,
               flag_scale_data=None,
               output_scheme='',
               descr='',

               folium_map=None,
               percentile=None,
               cmap=None,
               cmap_crop_min=None,
               cmap_crop_max=None,
               cmap_min=None,
               cmap_max=None,
               flag_cmap_already_expanded=False,
               ctable=None,
               background_color=None,
               in_null=None,
               out_null=None,
               output_dtype=None,
               output_format=None,
               output_projection=None,
               # output_reference_file=None,
               flag_temporary=False):
    '''
    save image to file following ISCE standards or GDAL:
    output_format = GDAL Raster Formats (e.g. GTiff, ENVI)
    '''
    # print('*** save_image')
    # from plant_ui import image_obj_ctable_expand
    # if dtype is None and output_dtype is not None:
    #     output_dtype = output_dtype
    if not output_file:
        print('ERROR output file is empty or invalid')
        return

    if isinstance(input_data, plant.PlantImage):
        filename = input_data.filename
        file_format = input_data.file_format
    else:
        filename = ''
        file_format = None
    if (output_format is None and
            filename and output_file and
            filename.split('.')[-1] == output_file.split('.')[-1]):
        output_format = get_output_format(output_file, file_format)
        # elif output_format is None:
    else:
        output_format = get_output_format(output_file,
                                          output_format=output_format)

    image_obj = plant.PlantImage(
        input_data,
        # only_header=True,
        update=output_format.upper() != 'VRT',
        # filename=output_file,
        nbands=nbands,
        # geotransform=geotransform,
        # projection=projection,
        scheme=output_scheme,
        ctable=ctable)

    # check for geo-inputs' inconsistency
    if ((lat_size is not None and
         lat_size != image_obj.length) or
        (lon_size is not None and
         lon_size != image_obj.width)):
        geotransform = None
        projection = None
    else:
        if (image_obj.geotransform is None and
                geotransform is not None):
            image_obj.geotransform = geotransform
        if (image_obj.projection is None and
                projection is not None):
            image_obj.projection = projection

    # plant.imshow(image_obj, title='ok')
    # plant_transform_obj=plant_transform_obj,
    # dtype=dtype,
    # file_format=output_format,
    # print('************************************')
    # print('(save_image) nbands: ', image_obj.nbands)
    # print('(save_image) ctble: ', image_obj.band.ctable)
    # print('output_file: ', output_file)
    # print('************************************')

    if image_obj is None:
        print('ERROR saving %s. Invalid data' % output_file)
        return

    # print('*** output_format: ', output_format)
    if (image_obj.depth is not None and
            image_obj.depth > 1 and
            (not output_format or
             (output_format != 'ISCE' and
              output_format not in plant.FIG_DRIVERS))):
        print('WARNING output image is 3D. '
              'Forcing format to ISCE.')
        output_format = 'ISCE_NP'
    # print('*** output_format 2: ', output_format)
    # print(image_obj.type_size)

    # plant.debug('2', image_obj.band_orig)
    if (in_null is not None or
            out_null is not None):
        # plant.debug(in_null)
        # plant.debug(out_null)
        # image_obj =
        # print('*** image: ', image_obj.image)
        plant.apply_null(image_obj,
                         in_null=in_null,
                         out_null=out_null,
                         force_update=True)
        # print('*** image: ', image_obj.image, '(after)')

    if output_file.upper().startswith('MEM:'):
        # output_key = 'OUTPUT:'+output_file[4:]
        output_key = output_file[4:]
        # print('*** image_obj.filename_orig 1: ', image_obj.filename_orig)
        # print('*** image_obj.filename 1: ', image_obj.filename)
        # print('*** realizing changes (2)...')
        image_obj.realize_changes()
        image_obj.filename = output_file
        image_obj.filename_orig = output_file
        '''
        print('*** id: ', id(image_obj))
        print('*** __str__: ', image_obj)
        print('*** image: ', image_obj.image)
        print('*** output_key: ', output_key)
        '''
        plant.plant_config.variables[output_key] = image_obj
        return

    # plant.debug('3', image_obj.band_orig)
    image = image_obj.image if output_format != 'VRT' else None

    nbands = image_obj.nbands
    geotransform = image_obj.geotransform
    metadata = image_obj.metadata
    # description = image_obj.description
    gcp_list = image_obj.gcp_list
    gcp_projection = image_obj.gcp_projection
    projection = image_obj.projection
    ctable = image_obj.ctable
    # dtype = image_obj.dtype

    if (cmap_min is None and cmap_max is None and
            image_obj.file_format in plant.FIG_DRIVERS):
        cmap_min = 0
        cmap_max = 255

    # same extension (keep file_format)
    # print(image_obj.filename.split('.')[-1], output_file.split('.')[-1])

    # output_format = ''
    scheme = image_obj.scheme
    if scheme is None:
        scheme = 'BSQ'
    projection = image_obj.projection
    flag_figure = output_format.upper() in plant.FIG_DRIVERS
    flag_html = output_format.upper() == 'HTML'
    flag_kml_kmz = (output_format.upper() == 'KML' or
                    output_format.upper() == 'KMZ')
    flag_csv = output_format.upper() == 'CSV'
    flag_isce = (output_format.upper() == 'ISCE_NP' or
                 (output_format.upper() == 'ISCE' and
                  (save_header_only or
                   (scheme and scheme.upper() != 'BSQ'))))
    flag_srf = output_format.upper() == 'SRF'
    flag_numpy = output_format.upper() == 'NUMPY'
    '''
    if (not flag_isce and scheme and
            scheme.upper() != 'BSQ' and
            output_format == 'ENVI'):
        print('WARNING unsupported file format %s'
              ' and scheme %s. Changing file format to ISCE.'
              % (output_format, scheme))
        output_format = 'ISCE'
        flag_isce = True
    elif not flag_isce and scheme and scheme.upper() != 'BSQ':
        print('WARNING unsupported file format %s'
              ' and scheme %s. Changing scheme to BSQ.'
              % (output_format, scheme))
        scheme = 'BSQ'
    '''
    if image is not None:
        flag_isce = flag_isce or len(image.shape) == 1
        for b in range(image_obj.nbands):
            band = image_obj.get_band(band=b)
            dtype = band.dtype
            if output_dtype is not None:
                band_dtype = output_dtype
            else:
                band_dtype = dtype
            flag_force_output_dtype = False
            if (output_dtype is not None and
                    (dtype is None or output_dtype != dtype)):
                flag_force_output_dtype = True
            elif (dtype is not None and
                  dtype == 'bool'):
                flag_force_output_dtype = True
                band_dtype = 'byte'
            if (ctable is not None and
                    'int' not in plant.get_dtype_name(dtype) and
                    'short' not in plant.get_dtype_name(dtype) and
                    'long' not in plant.get_dtype_name(dtype) and
                    'byte' not in plant.get_dtype_name(dtype) and
                    not flag_figure and
                    not flag_html and
                    not flag_kml_kmz):
                dtype_size = plant.get_dtype_size(dtype)*8
                print(f'WARNING forcing output to uint{dtype_size}'
                      ' because image has color table (GDAL)..')
                # band_dtype = np.uint16
                band_dtype = np.__dict__[f'uint{dtype_size}']
                flag_force_output_dtype = True
            if flag_force_output_dtype:
                band.dtype = band_dtype
                # image = np.asarray(image, dtype=output_dtype)
                # dtype = output_dtype

    # print('*** plant.dirname:', plant.dirname(output_file))
    # print('*** os.path.dirname:', os.path.dirname(output_file))
    
    if not os.path.isdir(plant.dirname(output_file)):
        try:
            os.makedirs(plant.dirname(output_file))
        except FileExistsError:
            pass

    ret = plant.overwrite_file_check(output_file,
                                     force=force)
    if not ret:
        return
        
    update_image = not save_header_only

    if os.path.isfile(output_file) and update_image:
        os.remove(output_file)

    if flag_numpy:
        _save_numpy(image, output_file, verbose,
                    flag_temporary)
        return

    if flag_srf:
        _save_srf(image, image_obj, output_file, verbose,
                  flag_temporary)
        return

    flag_save_isce_header = (output_format.upper() == 'ISCE' or
                             output_format.upper() == 'ENVI')
    if flag_isce:
        ret = _save_isce(image, image_obj, output_file,
                         nbands, output_dtype, scheme, geotransform,
                         update_image, verbose, force,
                         flag_temporary)
        if ret is None:
            return
        flag_save_isce_header = False
        if scheme and scheme.upper() != 'BSQ':
            print(f'WARNING changing scheme from {scheme.upper()}'
                  ' to BSQ and file format'
                  f' from {output_format.upper()} to ENVI')
            scheme = 'BSQ'
            output_format = 'ENVI'
        if output_format.upper() == 'ISCE_NP':
            output_format = 'ISCE'

    '''
    if not update_image:
        print('WARNING not able to only update header '
              'of %s with output format %s'
              % (output_file, output_format))
        return
    '''

    geotransform_edges = None
    if geotransform:
        if (output_projection is not None and
                'WGS84' in output_projection.upper()):
            geotransform_temp, projection = \
                plant.update_geotransform_projection(
                    projection,
                    geotransform,
                    output_projection=output_projection.upper())
        # GDAL's convention (outer edges and negative lat. step):
        geotransform_edges = \
            plant.geotransform_centers_to_edges(geotransform,
                                                lat_size=image_obj.shape[0],
                                                lon_size=image_obj.shape[1])
        if geotransform_edges is not None and  len(geotransform_edges) > 6:
            geotransform_edges[5] = -geotransform_edges[5]
            geotransform_edges[3] = geotransform_edges[7]
            geotransform_edges = geotransform_edges[:6]

    # if output_format.upper() == 'JSON':
    #    print('ERROR not implemented yet')
    #    return
    # if output_format.upper() == 'LAS':
    #    print('ERROR not implemented yet')
    #    return

    # percentile = 95

    cmap = plant.DEFAULT_CMAP if cmap is None else cmap

    if flag_figure or flag_html:
        nbands = image_obj.nbands
        if ctable is not None:
            ret_dict = plant.image_obj_ctable_expand(image_obj,
                                                     ctable=ctable)
            image_obj = ret_dict['image_obj']
            cmap_min = None
            cmap_max = None
            flag_scale_data = False
            nbands = image_obj.nbands

        elif (nbands > 3 and image_obj.file_format is not None and
            image_obj.file_format.upper() not in plant.FIG_DRIVERS):
            print('WARNING input is 4 bands or more. Only the'
                  ' first three bands will be saved to'
                  ' the output.')
            nbands = 3

        if nbands == 1:
            args = [image]
        else:
            args = []
            for i in range(nbands):
                args.append(image_obj.get_image(band=i))
        if flag_figure:
            args.append(output_file)
            # output_format_fig = output_format
            save_image_figure(*args,
                              output_format=output_format,
                              percentile=percentile,
                              flag_scale_data=flag_scale_data,
                              background_color=background_color,
                              cmap=cmap,
                              cmap_min=cmap_min,
                              cmap_max=cmap_max,
                              verbose=verbose,
                              force=force)
        elif flag_html:
            import folium
            from folium import raster_layers
            geotransform_edges = plant.geotransform_centers_to_edges(
                geotransform,
                lat_size=image_obj.length,
                lon_size=image_obj.width)
            ret_dict = plant.get_coordinates(geotransform=geotransform_edges,
                                             lat_size=image_obj.length,
                                             lon_size=image_obj.width)
            lat_min, lat_max = ret_dict['lat_arr']
            lon_min, lon_max = ret_dict['lon_arr']
            '''
            step_lat = ret_dict['step_lat']
            step_lon = ret_dict['step_lon']
            step_lon = geotransform[1]
            step_lat = geotransform[5]
            lon_arr = [geotransform[0],
                       geotransform[0]+step_lon*image_obj.width]
            lat_arr = [geotransform[3]+step_lat*image_obj.length,
                       geotransform[3]]
            step_lat = np.abs(step_lat)
            step_lon = np.abs(step_lon)
            lon_min = np.max([np.min(lon_arr)-step_lon/2, -180.0])
            lon_max = np.min([np.max(lon_arr)+step_lon/2, 180.0])
            lat_min = np.max([np.min(lat_arr)-step_lat/2, -90.0])
            lat_max = np.min([np.max(lat_arr)+step_lat/2, 90.0])
            '''
            if folium_map is None:
                folium_map = folium.Map(location=[(lat_min+lat_max)/2,
                                                  (lon_min+lon_max)/2],
                                        # zoom_start=7,
                                        tiles=None)
            folium_map = plant.folium_add_layer(folium_map,
                                                flag_google_maps=True)
            folium_map.fit_bounds([[lat_min, lon_min],
                                   [lat_max, lon_max]])
            image = plant.image_to_figure(*args,
                                          cmap=cmap,
                                          cmap_min=cmap_min,
                                          cmap_max=cmap_max,
                                          # temp_file,
                                          verbose=False)
            # print('*** folium_map (1)', np.nanmean(image))
            raster_layers.ImageOverlay(
                # image=data,
                image=image,
                # image=temp_file,
                bounds=[[lat_min, lon_min], [lat_max, lon_max]]
            ).add_to(folium_map)
            plant.save_folium(folium_map,
                              output_file,
                              verbose=verbose,
                              force=force)
        return

    if flag_kml_kmz:
        # image=image,
        save_image_kml(image_obj=image_obj,
                       nbands=nbands,
                       ctable=ctable,
                       cmap=cmap,
                       cmap_crop_min=cmap_crop_min,
                       cmap_crop_max=cmap_crop_max,
                       cmap_min=cmap_min,
                       cmap_max=cmap_max,
                       flag_scale_data=flag_scale_data,
                       background_color=background_color,
                       percentile=percentile,
                       verbose=verbose,
                       geotransform=geotransform_edges,
                       output_file=output_file,
                       output_format=output_format,
                       force=force)
        return

    if flag_csv:
        image = image.ravel()
        ind = np.where(np.zeros(image.shape)+1)
        if len(image.shape) == 1:
            # data_x = ind[0]
            save_text(image.ravel(),
                      'data',
                      output_file,
                      force=force)
            return
        else:
            data_x = ind[1]
            data_y = ind[0]
            save_text(data_x, data_y, image.ravel(),
                      'x', 'y', 'data',
                      output_file,
                      force=force)
            return

    if save_header_only:
        output_file_orig = output_file
        output_file = get_temporary_file()
        verbose_orig = verbose
        verbose = False

    ret = _save_gdal(image, image_obj, output_file,
                     # output_reference_file,
                     output_format, nbands, output_dtype, ctable,
                     geotransform, geotransform_edges, metadata,
                     # description,
                     projection, gcp_list, gcp_projection,
                     save_header_only, flag_save_isce_header,
                     verbose, flag_temporary)
    if ret is None and save_header_only:
        verbose = verbose_orig
        file_list = plant.glob(output_file+'*')
        for current_file in file_list:
            # plant.append_temporary_file(current_file)
            plant.append_temporary_file(current_file)
            new_file = current_file.replace(output_file,
                                            output_file_orig)
            if current_file == new_file:
                continue
            update = plant.overwrite_file_check(new_file,
                                                force=force)
            if not update:
                continue
            os.rename(current_file, new_file)
            if plant.isfile(new_file):
                print(f'## file saved: {new_file} (GDAL)')
        current_file = get_envi_header(output_file)
        # plant.append_temporary_file(current_file)
        plant.append_temporary_file(current_file)
        if plant.isfile(current_file):
            update = plant.overwrite_file_check(new_file,
                                                force=force)
            if update:
                new_file = get_envi_header(output_file_orig)
                os.rename(current_file, new_file)
                if plant.isfile(new_file):
                    print(f'## file saved: {new_file} (GDAL)')
    if ret is None:
        return

    ret = _save_isce(image, image_obj, output_file,
                     nbands, output_dtype, scheme, geotransform,
                     update_image, verbose, force,
                     flag_temporary)
    '''
    if ret and not output_file.endswith('.vrt'):
        ''
        image_obj.filename_orig = output_file
        image_obj.filename = output_file
        output_file += '.vrt'
        output_format = 'VRT'
        ''
        new_file = output_file+'.vrt'
        update = plant.overwrite_file_check(new_file, force=force)
        if update:
            plant.BuildVRT(new_file, output_file,
                           image_obj.geotransform)
        ''
        ret = _save_gdal(image, image_obj, output_file,
                         # output_reference_file,
                         output_format, nbands, dtype, ctable,
                         geotransform, geotransform_edges, projection,
                         gcp_list, gcp_projection, save_header_only,
        flag_save_isce_header, verbose)
        ''
    '''


def get_image_dimensions(image):
    shape = image.shape
    width = shape[-1] if len(shape) >= 1 else 1
    length = shape[-2] if len(shape) >= 2 else 1
    depth = shape[-3] if len(shape) >= 3 else 1
    return depth, length, width


def _save_srf(image, image_obj, output_file, verbose,
              flag_temporary):
    image = plant.apply_srf_dtype(image)
    nbands = image_obj.nbands
    depth, length, width = get_image_dimensions(image)
    dtype = plant.get_dtype_name(image)
    dtype_srf = plant.get_srf_dtype(dtype)
    dtype_size = plant.get_dtype_size(dtype)
    header = np.zeros((8), dtype=np.uint32)
    header[0] = plant.SRF_MAGIC_NUMBER
    header[1] = width
    header[2] = length
    header[3] = dtype_size*8
    header[4] = width*length*nbands*8
    header[5] = dtype_srf
    header[6] = 0  # map type
    header[7] = 0  # map length
    with open(output_file, "wb") as of:
        # of.write(byte_array_header)
        header.tofile(of)
        for b in range(image_obj.nbands):
            if b != 0:
                image = image_obj.get_image(band=b)
                image = plant.apply_srf_dtype(image)
            # byte_array_image = bytearray(image.ravel())
            # of.write(byte_array_image)
            image.tofile(of)
    if not plant.isfile(output_file):
        print(f'ERROR saving image to {output_file} (SRF)')
        return
    if flag_temporary:
        plant.append_temporary_file(output_file)
        return
    plant.plant_config.output_files.append(output_file)
    if verbose:
        print(f'## file saved: {output_file} (SRF)')


def _save_numpy(image, output_file, verbose,
                flag_temporary):
    # np.save(output_file, image)
    image.tofile(output_file)
    if not plant.isfile(output_file):
        print(f'ERROR saving image to {output_file} (NUMPY)')
        return
    if flag_temporary:
        plant.append_temporary_file(output_file)
        return
    plant.plant_config.output_files.append(output_file)
    if verbose:
        print(f'## file saved: {output_file} (NUMPY)')
    # image.tofile(output_file)
    # if plant.isfile(output_file):
    #     print(f'## file saved: {output_file}')


def _save_isce(image, image_obj, output_file,
               nbands, dtype, scheme,
               geotransform, update_image, verbose, force,
               flag_temporary):
    # print('*** save_image (ISCE)')
    if dtype is None:
        dtype = plant.get_dtype_name(image)
    depth, length, width = get_image_dimensions(image)
    # image = image.reshape(width, 1)
    # if os.path.isfile(output_file+'.xml'):
    #     os.remove(output_file+'.xml')
    # print('*** ', nbands)
    if update_image and nbands == 1:
        image.tofile(output_file)
    elif update_image:
        data = np.zeros((length*width*depth*nbands),
                        dtype=dtype)
        if scheme.upper() == 'BIP':
            data[0::nbands] = image.ravel()
            for i in range(1, nbands):
                current_image = image_obj.getImage(band=i)
                data[i::nbands] = current_image.ravel()
        elif scheme.upper() == 'BIL':
            for i in range(0, length):
                offset = i*width*nbands
                data[offset:offset+width] = image[i, :]
            for b in range(1, nbands):
                current_image = image_obj.getImage(band=b)
                for i in range(0, length):
                    offset = i*width*nbands+b*width
                    data[offset:offset+width] = current_image[i, :]
        elif scheme.upper() == 'BSQ':
            data[0:length*width*depth] = image.ravel()
            for i in range(1, nbands):
                current_image = image_obj.getImage(band=i)
                data[i*length*width*depth:
                     (i+1)*length*width*depth] = \
                    current_image.ravel()
        else:
            print('ERROR cannot save ISCE image'
                  ' with scheme %s'
                  % (scheme))
            return
        data.tofile(output_file)
    if flag_temporary:
        plant.append_temporary_file(output_file)
    else:
        plant.plant_config.output_files.append(output_file)
        if (verbose and update_image and
                plant.isfile(output_file) and nbands == 1):
            print('## file saved: %s (ISCE)' % output_file)
        elif verbose and update_image and plant.isfile(output_file):
            print('## file saved: %s (ISCE: %s)' % (output_file, scheme))
        elif verbose and update_image:
            print('ERROR saving file: ' + output_file)
            return

    header_files = [output_file+ext for ext in ['.xml', '.vrt']]
    if not update_image:
        for header_file in header_files:
            if not plant.isfile(header_file):
                continue
            update_header = plant.overwrite_file_check(header_file,
                                                       force=force)
            if not update_header:
                print('WARNING operation cancelled')
                return
    if create_isce_header(output_file,
                          data=image,
                          scheme=scheme,
                          nbands=nbands,
                          geotransform=geotransform):
        if not update_image:
            for header_file in header_files:
                if flag_temporary:
                    plant.append_temporary_file(header_file)
                    continue
                plant.plant_config.output_files.append(
                    header_file)
                if plant.isfile(header_file) and verbose:
                    print('## file saved: %s' % header_file)
        '''
        if scheme and scheme.upper() != 'BSQ':
            envi_hdr = get_envi_header(output_file)
            os.remove(envi_hdr)
        '''
        return
    print('WARNING there was an error saving the ISCE header')
    # ERROR saving ISCE header
    return True


def get_temporary_file(basefile=None, dirname=None, append=False,
                       suffix=None, ext=None):
    timestamp = str(float(time.time()))+str(np.random.randint(1000))
    temp_file = 'temp_' + timestamp
    if basefile is not None:
        ret_dict = plant.parse_filename(basefile)
        filename = ret_dict['filename']
        temp_file = os.path.basename(filename)+'_'+temp_file
    if suffix is not None:
        temp_file += suffix
    if dirname is not None:
        temp_file = os.path.join(dirname,
                                 os.path.basename(temp_file))
    if ext is not None:
        if not ext.startswith('.'):
            ext = '.'+ext
        temp_file += ext
    if append:
        append_temporary_file(temp_file)
    return temp_file

def append_output_file(output_file):
    plant.plant_config.output_files.append(output_file)


def append_temporary_file(temp_file):
    plant.plant_config.temporary_files.append(str(temp_file))


def _save_gdal(image, image_obj, output_file,  # output_reference_file,
               output_format, nbands, dtype, ctable, geotransform,
               geotransform_edges, metadata,
               # description,
               projection, gcp_list, gcp_projection, save_header_only,
               flag_save_isce_header, verbose, flag_temporary):
    # print('*** save_image (GDAL): ', image)

    kwargs = locals()
    if dtype is None:
        dtype = plant.get_dtype_name(image)
    gdal_dtype = plant.get_gdal_dtype_from_np(dtype)
    if (gdal_dtype == gdal.GDT_Unknown and
            'int' in plant.get_dtype_name(dtype).lower()):
        print('WARNING invalid data type %s (GDAL).'
              ' Converting output to Int32..' % dtype)
        dtype = np.int32
        gdal_dtype = plant.get_gdal_dtype_from_np(dtype)
    elif gdal_dtype == gdal.GDT_Unknown:
        print('WARNING invalid data type %s (GDAL).'
              ' Converting output to Float32..' % dtype)
        dtype = np.float32
        gdal_dtype = plant.get_gdal_dtype_from_np(dtype)
    gdal.ErrorReset()

    dataset = None
    if metadata is None:
        metadata = {}
    if plant.plant_config.command_line is not None:
        metadata['command'] = f"{plant.plant_config.command_line}"
    metadata['path'] = os.getcwd()
    metadata['platform'] = sys.platform
    # print('*** metadata: ', metadata)
    # image is None or
    if (output_format is not None and output_format.upper() == 'VRT'):
        # dataset = gdal.Open(image_obj.filename)
        # print('WARNING output format is VRT. Changes may be lost')
        # if output_reference_file is None:
        #     output_reference_file = image_obj.filename
        # if image_obj.nbands != 1:
        # print('*** image_obj.nbands: ', image_obj.nbands)
        src_win = None
        file_list = []
        band_list = []
        for b in range(image_obj.nbands):
            band = image_obj.get_band(band=b)
            # file_list.append(band.parent_orig.filename)
            file_list.append(band.parent_orig.filename_orig)
            band_list.append(band.parent_orig_band_orig_index+1)
            if (src_win is None and
                    band.parent_orig.width != band.parent_orig.width_orig or
                    band.parent_orig.length != band.parent_orig.length_orig):
                # src_win = None
                # else:
                # x0 = image_obj.plant_transform_obj.offset_x
                # y0 = image_obj.plant_transform_obj.offset_y
                # width = image_obj.width
                # length = image_obj.length
                # src_win = [x0, y0, width, length]
                src_win = band.parent_orig.crop_window

        n_files = len(list(set(file_list)))
        if n_files == 1:
            ref_file = list(set(file_list))[0]
            plant.Translate(output_file, ref_file, verbose=False,
                            srcWin=src_win, bandList=band_list,
                            metadataOptions=metadata,
                            format='VRT')
        elif src_win is None:
            args = [output_file]
            args.append(file_list)
            plant.BuildVRT(*args,
                           bandList=band_list, verbose=False,
                           separate=True)
        else:
            print('WARNING the current implementation of GDAL'
                  ' Translate and BuildVRT (v.3.0.0) do not allow'
                  ' for cropping and multi-input files'
                  ' simultaneously.'
                  ' Creating a temporary file to perform both'
                  ' operations...')
            temp_file = get_temporary_file(append=False)+'.vrt'
            args = [temp_file]
            args.append(file_list)
            plant.BuildVRT(*args, bandList=band_list, verbose=False,
                           separate=True)
            ref_file = temp_file
            plant.Translate(output_file, ref_file, verbose=False,
                            metadataOptions=metadata,
                            srcWin=src_win, format='VRT')
            # plant.append_temporary_file(temp_file)
        # src_win = ' '.join([str(x) for x in src_win])
        '''
        plant.execute(f'gdal_translate {output_reference_file}'
                      f' {output_file} {srcwin_str}')
        '''

        if flag_temporary:
            plant.append_temporary_file(output_file)
            return

        plant.plant_config.output_files.append(
            output_file)
        if plant.isfile(output_file) and verbose:
            print('## file saved: %s (GDAL:VRT)' % output_file)
        # del dataset
        # gdal.ErrorReset()
        return

        '''
        print('*** output_reference_file: ', output_reference_file)
        try:
            dataset = gdal.Open(output_reference_file)
        except:
            dataset = None
        if dataset is None:
            driver = gdal.GetDriverByName(output_format)
            dataset = driver.Create(output_file,
                                    xsize=image_obj.width,
                                    ysize=image_obj.length,
                                    bands=image_obj.nbands_orig,
                                    eType=gdal_dtype)
        '''
    else:
        if output_format.upper() != 'ISCE':
            driver = gdal.GetDriverByName(output_format)
        else:
            driver = gdal.GetDriverByName('ENVI')
        if driver is None:
            del dataset
            gdal.ErrorReset()
            print('ERROR saving file %s. '
                  'Output format not recognized: %s'
                  % (output_file, output_format))
            return
        try:
            dataset = driver.Create(output_file,
                                    image_obj.width,
                                    image_obj.length,
                                    nbands,
                                    gdal_dtype)
        except RuntimeError:
            error_message = plant.get_error_message()
            del dataset
            gdal.ErrorReset()
            # if 'Image file is too small' not in error_message:
            message = ('saving file %s with format %s. %s'
                       % (output_file, output_format, error_message))
            if (output_format.upper() == 'ENVI' or
                    output_format.upper() == 'ISCE'):
                print(f'WARNING error {message}')
                return True
            print(f'ERROR {message}')
            return
            # dataset = None
        '''
        if dataset is None:
            driver.Deregister()
            del driver
            print(f'WARNING there was a problem saving'
                  f' the output file with format'
                  f' {output_format}. Updating output'
                  f' format to GTiff.')
            output_format = 'PNG'
            kwargs['output_format'] = output_format
            return _save_gdal(**kwargs)
        '''

    if dataset is None:
        print('ERROR saving file: %s' % output_file)
        return

    if geotransform_edges is not None:
        dataset.SetGeoTransform(geotransform_edges)
        # print('*** saving geotransform (edges): ', geotransform_edges)

    dataset.SetMetadata(metadata)

    # if description is not None:
    #     dataset.SetDescription(description)

    '''
        command = 'PLAnT '+plant.version+' - '
        command += plant.plant_config.command_line
        dataset.SetDescription(command)
    '''

    # print('*** saving geotransform: ', geotransform)
    # print('*** saving projection: ', projection)
    if (not projection and
            plant.valid_coordinates(geotransform=geotransform,
                                    projection='WGS84')):
        projection = plant.PROJECTION_REF_WKT
        print('WARNING (save_image) projection not found, '
              'considering: %s' % (projection))
    if projection:
        # print(f'*** (gdal_save) ds.SetProjection: {projection}')
        try:
            dataset.SetProjection(projection)
            # print(f'*** (gdal_save) projection saved: ',
            #       dataset.GetProjection())
        except RuntimeError:
            print(f'WARNING invalid projection: {projection}')
    if gcp_list is not None:
        dataset.SetGCPs(gcp_list, gcp_projection)
    # if gcp_projection:
    #         dataset.SetGCPProjection()

    '''
    if image is None or output_format.upper() == 'VRT':
        gdal.Translate(output_file,
                       dataset,
                       format='VRT')
        del dataset
        gdal.ErrorReset()
        return
    '''
    # dataset.GetRasterBand(1).WriteArray(image)
    # if ctable is not None:
    #    print('ds saving ctable: %s' % ctable)
    #    dataset.SetColorTable(ctable)

    for i in range(nbands):
        current_image = image_obj.getImage(band=i)
        current_band = dataset.GetRasterBand(i+1)
        # current_band = np.asarray(current_band, dtype=dtype)

        # print(current_image.shape)
        if ctable is not None:
            #  if verbose:
            #     print('including ctable: %s' % ctable)
            # if isinstance(ctable, dict):
            #    print('from dict')
            #    current_band.SetColorTable(new_ctable)
            # else:
            # print('***:', plant.get_dtype_name(current_image))
            current_band.SetColorTable(ctable)
            # for key in range(6):
            #    print('geting: ', ctable.GetColorEntry(int(key)))
            # debug(current_band.GetColorTable())
        name = image_obj.get_band(band=i).name
        # if nbands > 1 and verbose and name:
        if verbose and name:
            print(f'saving band {i}: {name}')
            # if nbands > 1 and verbose:
        elif nbands > 1 and verbose:
            print(f'saving band: {i}')
        if name:
            # current_band.SetBandName(name)
            current_band.SetDescription(name)

        if not save_header_only:
            try:
                current_band.WriteArray(current_image)
            except ValueError:
                error_message = plant.get_error_message()
                print('ERROR saving band with dimensions ',
                      f'{current_image.shape}: {error_message}')
            current_band.FlushCache()
        current_band = None  # dereference band to avoid "gotcha"

    if not save_header_only:
        dataset.FlushCache()
    del dataset
    gdal.ErrorReset()
    if plant.isfile(output_file+'.aux.xml'):
        os.remove(output_file+'.aux.xml')

    plant.plant_config.output_files.append(output_file)
    if verbose and plant.isfile(output_file):
        print('## file saved: %s (GDAL:%s)'
              % (output_file, output_format))
    if flag_save_isce_header:
        create_isce_header(output_file,
                           data=image,
                           nbands=nbands,
                           dtype=dtype,
                           image_obj=image_obj,
                           geotransform=geotransform)
    if plant.isfile(output_file+'.aux.xml'):
        plant.plant_config.temporary_files.append(output_file+'.aux.xml')
    # GDAL OGR
    # shpdriver = gdal.ogr.GetDriverByName(output_format)
    # if plant.isfile(output_file):
    #     os.remove(output_file)
    # point = ogr.Geometry(ogr.wkbPoint)
    #
    # # if os.path.exists(outputBufferfn):
    # #    shpdriver.DeleteDataSource(outputBufferfn)
    # outputBufferds = shpdriver.CreateDataSource(output_file)
    # bufferlyr = outputBufferds.CreateLayer(output_file,
    #                                        geom_type=gdal.ogr.wkbPolygon)
    # featureDefn = bufferlyr.GetLayerDefn()
    # # for feature in inputlyr:
    # # bufferDist = 10
    # # ingeom = image.GetGeometryRef()
    # # geomBuffer = ingeom.Buffer(bufferDist)
    # outFeature = gdal.ogr.Feature(featureDefn)
    # # outFeature.SetGeometry(geomBuffer)
    # bufferlyr.CreateFeature(outFeature)


def rename_image(input_file,
                 output_file,
                 verbose=True,
                 input_format=None,
                 force=None,
                 in_null=np.nan,
                 out_null=np.nan,
                 output_format=None,
                 output_dtype=None):

    image_obj = read_image(input_file,
                           # only_header=True,
                           input_format=input_format)
    output_dirname = os.path.dirname(output_file)
    if output_dirname and not os.path.isdir(output_dirname):
        try:
            os.makedirs(output_dirname)
        except FileExistsError:
            pass
    if verbose:
        print('renaming '+input_file+' to '+output_file+' ..')
    if os.path.isfile(output_file):
        if plant.overwrite_file_check(output_file,
                                      force=force):
            if not os.path.samefile(input_file, output_file):
                os.remove(output_file)
            for ext in ['.vrt', '.xml', '.aux.xml', None]:
                if ext is not None:
                    input_file_ext = input_file + ext
                    output_file_ext = output_file + ext
                else:
                    input_file_ext = get_envi_header(input_file)
                    output_file_ext = get_envi_header(output_file)
                if (os.path.isfile(input_file_ext) and
                        os.path.isfile(output_file_ext) and
                    not os.path.samefile(input_file_ext,
                                         output_file_ext)):
                    os.remove(output_file_ext)
    plant.apply_null(image_obj,
                     in_null=in_null,
                     out_null=out_null)
    kwargs = {}
    if output_format is not None:
        kwargs['output_format'] = output_format
    if output_dtype is not None:
        kwargs['output_dtype'] = output_dtype

    save_image(image_obj,
               output_file,
               verbose=verbose,
               force=True,
               **kwargs)
    if not os.path.isfile(output_file):
        print('ERROR there was an error saving file:'
              f' {output_file}')
        return
    if not os.path.samefile(input_file, output_file):
        os.remove(input_file)
    for ext in ['.vrt', '.xml', '.aux.xml', None]:
        if ext is not None:
            input_file_ext = input_file + ext
            output_file_ext = output_file + ext
        else:
            input_file_ext = get_envi_header(input_file)
            output_file_ext = get_envi_header(output_file)
        if (os.path.isfile(input_file_ext) and
                os.path.isfile(output_file_ext) and
            not os.path.samefile(input_file_ext,
                                 output_file_ext)):
            os.remove(input_file_ext)
        elif os.path.isfile(input_file_ext):
            os.rename(input_file_ext, output_file_ext)


def copy_image(input_file,
               output_file,
               verbose=True,
               create_link=False,
               input_format=None,
               force=None,
               in_null=np.nan,
               out_null=np.nan,
               output_dtype=None,
               output_format=None):
    image_obj = read_image(input_file,
                           # only_header=True,
                           input_format=input_format)
    if create_link:
        if verbose:
            print('linking '+input_file+' to '+output_file+' ..')
    else:
        if verbose:
            print('copying '+input_file+' to '+output_file+' ..')
    if plant.isfile(output_file):
        if plant.overwrite_file_check(output_file,
                                      force=force):
            os.remove(output_file)
            if plant.isfile(output_file+'.vrt'):
                os.remove(output_file+'.vrt')
            if plant.isfile(output_file+'.xml'):
                os.remove(output_file+'.xml')
            envi_hdr = get_envi_header(output_file)
            if plant.isfile(envi_hdr):
                os.remove(envi_hdr)

    if create_link:
        os.symlink(os.path.abspath(input_file),
                   os.path.abspath(output_file))

        # os.symlink(input_file, output_file)
        #  envi_hdr = get_envi_header(input_file)
        # if plant.isfile(input_file):
        #     output_envi_hdr = get_envi_header(output_file)
        #     os.symlink(envi_hdr, output_envi_hdr)
        create_isce_header(output_file,
                           nbands=1,
                           length=image_obj.length,
                           width=image_obj.width,
                           dtype=image_obj.dtype,
                           scheme=image_obj.scheme,
                           descr='',
                           geotransform=image_obj.geotransform)

    # elif (in_null == out_null or
    #        (np.isnan(in_null) and np.isnan(out_null))):
    #    copyfile(input_file, output_file)
    #    create_isce_header(output_file, 1, image_obj.length, image_obj.width,
    #                  image_obj.dtype, image_obj.scheme, descr='',
    #                  geotransform=image_obj.geotransform)
    else:
        # image_obj = read_image(input_file,
        #                        input_format=input_format)
        # image_obj =
        plant.apply_null(image_obj,
                         in_null=in_null,
                         out_null=out_null)
        kwargs = {}
        if output_format is not None:
            kwargs['output_format'] = output_format
        if output_dtype is not None:
            kwargs['output_dtype'] = output_dtype
        save_image(image_obj,
                   output_file,
                   verbose=verbose,
                   force=force,
                   **kwargs)


def fix_vrt(file_list,
            force=None):
    # if isinstance(file_list, str):
    #    file_list = search_image(file_list)
    for current_file in file_list:
        save_image(read_image(current_file,
                              verbose=False),
                   current_file,
                   only_header=True,
                   verbose=False,
                   force=force)


def read_large(input_name, name=None,  # db10=False, db20=False,
               function=''):
    # ret = read_image(input_name, only_header=True)
    # print(ret)
    test_var = np.memmap(search_image(input_name)[0], mode="r")
    total_size = test_var.shape[0]
    max_chunk_size = 2**24
    print('total size: '+str(total_size))
    print('max chunk size: '+str(max_chunk_size))
    nchunks = 1
    while (float(total_size)/nchunks > max_chunk_size or
           total_size % nchunks != 0):
        nchunks += 1
    chunk_size = total_size/nchunks
    print('new shape: ' + str(nchunks) + ' x ' + str(chunk_size))
    test_var = np.reshape(test_var, (nchunks, chunk_size))
    # print(test_var.shape)
    if not function:
        function = get_info
    for i in range(test_var.shape[0]):
        function(test_var)
        # , db10=db10, db20=db20)


def save_text(*args, **kwargs):
    '''
    save data as DSV (e.g. CSV, TSV, etc.)
    '''
    force = kwargs.pop('force', None)
    verbose = kwargs.pop('verbose', True)
    delimiter = kwargs.pop('delimiter', ',')
    str_list = []
    vector_list = []
    for count, arg in enumerate(args):
        if isinstance(arg, str):
            str_list.append(arg)
        elif isinstance(arg, dict):
            for key in arg:
                str_list.append(key)
                vector_list.append(arg[key])
        elif isinstance(arg, np.ndarray):
            vector_list.append(arg.ravel())
        else:
            vector_list.append(arg)
    output_file = str_list[-1]
    str_list = str_list[:-1]

    if (len(str_list) != len(vector_list)
            and len(str_list) != 0):
        print('vector list size: ', len(vector_list))
        print('string list size: ', len(str_list))
        print('ERROR vector and string list sizes do not '
              'match')
        return

    if plant.isfile(output_file):
        ret = plant.overwrite_file_check(output_file,
                                         force=force)
        if not ret and verbose:
            print('WARNING file not updated: ' + output_file,
                  1)
            return

    if not os.path.isdir(plant.dirname(output_file)):
        try:
            os.makedirs(plant.dirname(output_file))
        except FileExistsError:
            pass
    with open(output_file, 'w') as f:
        # for i in n_parameters:
        #    f.write('Column '+str(i+1)+': 'text[i]++' \n')
        #    f.write('\n')
        if len(str_list) > 1:
            line_str = ('%s' % (str_list[0]))
            for n in range(1, len(str_list)):
                line_str += ('%s%s' % (delimiter,
                                       str_list[n]))
            line_str += '\n'
            f.write(line_str)

        if isinstance(vector_list[0], np.ndarray):
            n_lines = vector_list[0].size
        else:
            n_lines = len(vector_list[0])
        for p in range(n_lines):
            if isinstance((vector_list[0])[p], int):
                line_str = ('%d' % float((vector_list[0])[p]))
            elif plant.isnumeric((vector_list[0])[p]):
                line_str = ('%.16f' % float((vector_list[0])[p]))
            elif isinstance((vector_list[0])[p], str):
                line_str = ('"%s"' % (vector_list[0])[p])
            else:
                line_str = ('%s' % (vector_list[0])[p])
            for n in range(1, len(vector_list)):
                try:
                    value = (vector_list[n])[p]
                except:
                    continue
                if isinstance(value, int):
                    line_str += ('%s%.16f' % (delimiter, value))
                elif plant.isnumeric(value):
                    line_str += ('%s%.16f' % (delimiter, value))
                elif isinstance(value, str):
                    line_str += ('%s"%s"' % (delimiter, value))
                else:
                    line_str += ('%s%s' % (delimiter, value))
            line_str += '\n'
            f.write(line_str)
        plant.plant_config.output_files.append(
            output_file)
        if verbose:
            print('## file saved: %s (TEXT)' % output_file)


def get_footprint_grid(footprint, step_lat, step_lon):
    footprint_lat = footprint // step_lat
    footprint_lon = footprint // step_lon
    if (footprint_lat == 1
            and footprint_lon == 1):
        fp_x = np.asarray([0], dtype=np.int)
        fp_y = np.asarray([0], dtype=np.int)
    elif (footprint_lat == 1
            and footprint_lon == 2):
        fp_x = np.asarray([0, 1], dtype=np.int)
        fp_y = np.asarray([0, 0],
                          dtype=np.int)
    elif (footprint_lat == 2
            and footprint_lon == 1):
        fp_x = np.asarray([0, 0], dtype=np.int)
        fp_y = np.asarray([0, 1],
                          dtype=np.int)
    elif (footprint_lat == 2
            and footprint_lon == 2):
        fp_x = np.asarray([0, 0, 0, 1, -1], dtype=np.int)
        fp_y = np.asarray([0, 1, -1, 0, 0], dtype=np.int)
    else:
        fp_y, fp_x = ellipse(2*footprint_lat,
                             2*footprint_lon,
                             footprint_lat/2,
                             footprint_lon/2)
        fp_x = np.asarray(fp_x - np.mean(fp_x),
                          dtype=np.int)
        fp_y = np.asarray(fp_y - np.mean(fp_y),
                          dtype=np.int)
    return fp_x, fp_y


def rasterize_with_footprint(data_vect,
                             lat_vect,
                             lon_vect,
                             lat_arr,
                             lon_arr,
                             step_lat,
                             step_lon,
                             footprint=None,
                             footprint_array=None,
                             nbands=None,
                             out_null=0,
                             verbose=True,
                             loreys_height=False,
                             sum_samples=False,
                             E=None):
    # from plant_ui import print_progress

    # data_vect = plant.shape_image(data_vect)
    lat_vect = np.asarray(lat_vect)
    lon_vect = np.asarray(lon_vect)
    image_obj = plant.PlantImage(data_vect,
                                 nbands=nbands)
    data_vect = image_obj.image_list
    data_vect = [x.ravel() for x in data_vect]
    width = data_vect[0].size

    if width != lat_vect.size:
        print('ERROR dimensions do not match '
              '(data size=%d, lat vect width=%d)'
              % (width, lat_vect.size))
        return

    if width != lon_vect.size:
        print('ERROR dimensions do not match '
              '(data size=%d, lon vect width=%d)'
              % (width, lon_vect.size))
        return

    nbands = len(data_vect)
    if step_lat is None or step_lon is None:
        print('ERROR please set the step size')
    lat_size = int(round(float(lat_arr[1]-lat_arr[0])/step_lat+1))
    lon_size = int(round(float(lon_arr[1]-lon_arr[0])/step_lon+1))
    data = [np.zeros((lat_size, lon_size)) for b in range(nbands)]
    ndata = np.zeros((lat_size, lon_size))

    if loreys_height:
        weight_sum = np.zeros((lat_size, lon_size))
    # n_points_outside = 0
    # footprint = footprint/2
    min_step = min([step_lat, step_lon])

    footprint_type = 0
    if footprint is not None:
        if min_step < footprint/2:
            fp_x, fp_y = get_footprint_grid(footprint/2, step_lat, step_lon)
            footprint_type = 1
    if footprint_array is not None:
        footprint_type = 2

    # if verbose:
    #    progress_last = -1
    weight = 1.0
    for i in range(0, width):
        plant.print_progress(i, width, verbose=verbose)
        lat_ind = int(round((lat_vect[i]-lat_arr[0])/step_lat))
        lon_ind = int(round((lon_vect[i]-lon_arr[0])/step_lon))

        if lat_ind >= lat_size or lon_ind >= lat_size:
            # n_points_outside += 1
            continue
        if lat_ind < 0 or lon_ind < 0:
            # n_points_outside += 1
            continue

        if footprint_type == 2:
            small_footprint = min_step >= footprint_array[i]/2
        else:
            small_footprint = footprint_type == 0

        if not small_footprint:
            if footprint_type == 2:
                fp_x, fp_y = get_footprint_grid(footprint_array[i]/2,
                                                step_lat,
                                                step_lon)
            lat_footprint_ind = lat_ind + fp_y
            lon_footprint_ind = lon_ind + fp_x
            valid = np.where(np.logical_and(
                    np.logical_and((lat_footprint_ind >= 0),
                                   (lon_footprint_ind >= 0)),
                    np.logical_and(
                        (lat_footprint_ind <= (lat_size-1)),
                        (lon_footprint_ind <= (lon_size-1)))))
            lat_footprint_ind = np.asarray(lat_footprint_ind[valid],
                                           dtype=np.int)
            lon_footprint_ind = np.asarray(lon_footprint_ind[valid],
                                           dtype=np.int)
            ndata[lat_footprint_ind, lon_footprint_ind] += 1

        else:
            ndata[lat_ind, lon_ind] += 1
        for b in range(nbands):
            if not small_footprint:
                if loreys_height:
                    # since diam**2=r**2/4 proportional to pi*(r**2):
                    weight = plant.tree_diameter_from_height(data_vect[b][i],
                                                             E=E)**2
                    weight_sum[lat_footprint_ind, lon_footprint_ind] += weight
                data[b][lat_footprint_ind, lon_footprint_ind] += \
                    weight*data_vect[b][i]
            else:
                if loreys_height:
                    # since diam**2=r**2/4 proportional to pi*(r**2):
                    weight = plant.tree_diameter_from_height(data_vect[b][i],
                                                             E=E)**2
                    weight_sum[lat_ind, lon_ind] += weight
                data[b][lat_ind, lon_ind] += weight*data_vect[b][i]
    plant.print_progress(width, width, verbose=verbose)

    if verbose:
        print('100')
    if loreys_height:
        ind = np.where(weight_sum != 0)
        for b in range(nbands):
            data[b][ind] = data[b][ind]/weight_sum[ind]

    if not sum_samples:
        ind = np.where(ndata != 0)
        for b in range(nbands):
            data[b][ind] = data[b][ind]/ndata[ind]

    ind = np.where(ndata == 0)
    # convention: higher latitudes at the top of the array
    for b in range(nbands):
        data[b][ind] = out_null
        data[b] = data[b][::-1, :]
    ndata = ndata[::-1, :]
    geotransform = plant.get_geotransform(lat_arr=lat_arr,
                                          lon_arr=lon_arr,
                                          step_lat=step_lat,
                                          step_lon=step_lon)
    return data, ndata, geotransform


def get_info_from_xml(xmlFile, filename=None):
    '''
    extracts width, length and dtype from an ISCE XML file.
    '''
    if not plant.isfile(xmlFile):
        return

    try:
        from iscesys.Parsers.FileParserFactory import createFileParser
        from isceobj.Util import key_of_same_content
    except ModuleNotFoundError:
        # print('WARNING ISCE is not installed. Error reading ISCE header')
        return

    PA = createFileParser('xml')
    try:
        dictNow, dictFact, dictMisc = PA.parse(xmlFile)
    except:
        return
    width = key_of_same_content('width', dictNow)[1]
    length = key_of_same_content('length', dictNow)[1]

    try:
        dtype = key_of_same_content('data_type', dictNow)[1]
    except:
        # print('WARNING data_type not present in ISCE header')
        return
    nbands = key_of_same_content('number_bands', dictNow)[1]
    scheme = key_of_same_content('scheme', dictNow)[1]
    numpyType = plant.get_np_dtype(dtype)

    if filename is None:
        filename = xmlFile.replace('.xml', '')

    image_obj = plant.PlantImage(filename=filename,
                                 width=width,
                                 length=length,
                                 dtype=numpyType,
                                 nbands=nbands,
                                 scheme=scheme)
    return image_obj


'''
def get_info_from_rsc(self, imagersc, filename=None):
    """
    determines image name, width, image type and data type from input rsc
    """
    from iscesys.Parsers.FileParserFactory import createFileParser
    if filename is None:
        filename = imagersc.replace('.rsc', '')
    try:
        PA = createFileParser('rsc')
        dictOut = PA.parse(imagersc)
        # dictOut has a top node that is just a name
        dictNow = dictOut[list(dictOut.keys())[0]]
        if 'WIDTH' in dictNow:
            width = int(dictNow.WIDTH)
        try:
            if 'LAT_REF1' in dictNow:
                # extract the geo info
                self._width.append(float(width))
                self._startLon.append(float(dictNow['X_FIRST']))
                self._deltaLon.append(float(dictNow['X_STEP']))
                self._length.append(float(dictNow['FILE_LENGTH']))
                self._startLat.append(float(dictNow['Y_FIRST']))
                self._deltaLat.append(float(dictNow['Y_STEP']))
                self._names.append(filename)
        except:
            pass  # not a geo file
    except:
        print("Error. Cannot extract width from input file.")
        raise Exception
    # assume imagersc = 'name.ext.rsc'
    try:
        ext = imagersc.split('.')[-2]
    except:
        print("Error. Cannot extract extension from input file.")
        raise Exception
    found = False
    for k, v in self._ext.items():
        if ext in v:
            found = True
            break
    if not found:
        print("Error. Invalid image extension", ext, ".")
        self.printExtensions()
        raise Exception
    extNow = self.getRscExt(ext)
    dtype = self._mapDataType['rsc'][extNow]
    image_obj = plant.PlantImage(filename=filename,
                           file_format=ext,
                           width=width,
                           dtype=dtype)
    return image_obj
'''

def _read_image_hdf5(hdf5_obj, image_obj,
                     # offset_x, width,
                     # offset_y, length,
                     only_header=False,
                     plant_transform_obj=None, 
                     flag_no_messages=False,
                     flag_exit_if_error=True,
                     out_null=None,
                     verbose=True,
                     force=False):
    # depth_orig, length_orig, width_orig = \
    #     plant.get_image_dimensions(hdf5_obj)
    # dtype = hdf5_obj.dtype
    # image_obj.set_depth_orig(1)
    # image_obj.set_nbands_orig(nbands_orig)
    # image_obj.set_length_orig(length_orig)
    # image_obj.set_width_orig(width_orig)
    # print('*** reading HDF5 image: ', image_obj.filename_orig)
    # print('*** only_header: ', only_header)
    try:
        dtype = hdf5_obj.dtype
    except TypeError:
        dtype = None

    if plant_transform_obj is not None:
        image_obj._applied_transform_obj_crop_list.append(
            plant_transform_obj.get_crop_dict())
        
    # if not only_header:
    if dtype is None:
        if verbose and not flag_no_messages:
            print('WARNING there was an error reading data'
                  f' type of {image_obj.filename_orig}.'
                  ' Casting sub-dataset to complex64')
        dtype = np.complex64
        sink = hdf5_obj.astype(np.complex64)
    else:
        sink = plant.DummySink()

    with sink:
        # print('*** nbands: ', image_obj.nbands)
        for b in range(image_obj.nbands):

            # print('*** setting band: ', b, ' of ', image_obj.band_orig)
            if not only_header and len(hdf5_obj.shape) == 0:
                image = np.asarray(hdf5_obj[()])
            elif not only_header and len(hdf5_obj.shape) == 1:
                image = hdf5_obj[image_obj.offset_x:
                                 image_obj.offset_x+image_obj.width]
            elif not only_header and len(hdf5_obj.shape) == 2:
                image = hdf5_obj[image_obj.offset_y:
                                 image_obj.offset_y+image_obj.length,
                                 image_obj.offset_x:
                                 image_obj.offset_x+image_obj.width]
            elif not only_header and len(hdf5_obj.shape) == 3:
                '''
                image = hdf5_obj[image_obj.offset_y:
                                 image_obj.offset_y+image_obj.length,
                                 image_obj.offset_x:
                                 image_obj.offset_x+image_obj.width,
                                 image_obj.band_orig[b]]
                '''
                image = hdf5_obj[image_obj.band_orig[b],
                                 image_obj.offset_y:
                                 image_obj.offset_y+image_obj.length,
                                 image_obj.offset_x:
                                 image_obj.offset_x+image_obj.width]

            elif only_header:
                image = None
                if dtype is None:
                    dtype = np.complex64
            else:
                raise NotImplementedError

            if (image is not None and
                dtype is not None and
                (dtype == 'object' or
                 '|S' in str(dtype))):
                image = np.array(image, dtype=str)

        band_obj = plant.PlantBand(image, dtype=dtype)
        if plant_transform_obj is not None and only_header:
            band_obj._applied_transform_obj_crop_header_list = \
                [plant_transform_obj.get_crop_dict()]
        elif plant_transform_obj is not None:
            band_obj._applied_transform_obj_crop_image_list = \
                [plant_transform_obj.get_crop_dict()]
        image_obj.set_band(band_obj, band=b, realize_changes=False)
        image_obj.set_null(out_null, realize_changes=False)




        

    # nbands = 1 
    # 1.7976931348623155e+308
    # ind = np.isnan(image)
    # image[ind] = np.nan
    # image = plant.insert_nan(image,
    #                          np.where(np.isnan(image)),
    #                          out_null=in_null)
    # image_obj.nbands_orig = 1
    # image_obj.bands_orig = 1
    # image_obj.band_orig = [0]
    # if only_header:
    #    return image_obj
    # image_obj = plant.apply_mask(image_obj,
    #                             plant_transform_obj=plant_transform_obj,
    #                             verbose=verbose,
    #                             force=force)
    # image_obj = plant.apply_null(image_obj,
    #                             in_null=in_null,
    #                             out_null=out_null)
    # image_obj =
    plant.apply_mask(image_obj,
                     plant_transform_obj=plant_transform_obj,
                     verbose=verbose,
                     force=force)
    image_obj = prepare_image(image_obj,
                              # filename,
                              flag_exit_if_error=flag_exit_if_error,
                              plant_transform_obj=plant_transform_obj,
                              # in_null=in_null,
                              out_null=out_null,
                              # input_format=input_format,
                              verbose=verbose,
                              only_header=only_header,
                              flag_no_messages=flag_no_messages,
                              force=force)
    return image_obj


def read_parameters_annotation_file(filename,
                                    eq_symbol='='):
    '''
    read parameters from annotation file (.ann)
    '''
    if not plant.isfile(filename):
        return
    parameters = None
    with open(filename, 'r', encoding='ISO-8859-1') as f:
        lines = f.readlines()
        parameters = {}
        for i, current_line in enumerate(lines):
            try:
                parameter, value = current_line.split(eq_symbol)
                value = value.replace('"', '')
                if ';' in value:
                    value = value[:value.index(';')]
                parameter = parameter.strip()
                if parameter[-1] == ')':
                    for i in range(len(parameter)):
                        if parameter[len(parameter)-1-i] == '(':
                            parameter = parameter[:len(parameter)-1-i]
                            parameter = parameter.strip()
                            break
                value = value.strip()
                if plant.isnumeric(value):
                    value = float(value)
                parameters[parameter] = value
            except:
                pass
    return parameters


def read_NOHRSC(filename,
                verbose=False):
    header_file = filename.replace('.dat', '.Hdr')
    if not plant.isfile(header_file):
        return
    if verbose:
        print('opening: %s (NOHRSC)'
              % filename)
    para_dict = read_parameters_annotation_file(header_file,
                                                eq_symbol=':')

    # para_dict['Horizontal precision']
    # para_dict['Elevation above datum']
    # para_dict['Vertical datum']
    # para_dict['Vertical precision']
    # para_dict['Benchmark column']
    # para_dict['Benchmark row']
    # para_dict['Benchmark x-axis coordinate']
    # para_dict['Benchmark y-axis coordinate']
    # x_offset = int(para_dict['X-axis offset'])
    # y_offset = int(para_dict['Y-axis offset'])
    dtype = para_dict['Data type']  # .replace('"', '')
    # dtype = plant.get_np_dtype(dtype)
    type_size = int(para_dict['Data bytes per pixel'])
    # print(dtype, type_size)
    if 'int' in dtype:
        dtype = 'uint'+str(type_size*8)
    elif 'float' in dtype:
        dtype = 'float'+str(type_size*8)
    elif 'complex' in dtype:
        dtype = 'complex'+str(type_size*8)
    elif 'byte' in dtype:
        dtype = 'byte'+str(type_size*8)
    else:
        dtype = dtype

    description = para_dict['Description']
    if verbose:
        print('    description: '+description)

    data_units = para_dict['Data units']
    if verbose:
        print('    data unit: '+data_units)
    width = int(para_dict['Number of columns'])
    length = int(para_dict['Number of rows'])
    # projection = para_dict['Horizontal datum']
    dummy_value = para_dict['No data value']

    lon_beg = para_dict['Minimum x-axis coordinate']
    lon_end = para_dict['Maximum x-axis coordinate']
    lon_arr = [lon_beg, lon_end]
    lat_beg = para_dict['Minimum y-axis coordinate']
    lat_end = para_dict['Maximum y-axis coordinate']
    lat_arr = [lat_beg, lat_end]
    step_lon = para_dict['X-axis resolution']
    step_lat = para_dict['Y-axis resolution']
    geotransform = plant.get_geotransform(lat_arr=lat_arr,
                                          lon_arr=lon_arr,
                                          step_lat=step_lat,
                                          step_lon=step_lon)

    image_obj = plant.PlantImage(
        filename=filename,
        width=width,
        length=length,
        dtype=dtype,
        description=description,
        # plant_transform_obj=plant_transform_obj,
        # projection=projection,
        null=dummy_value,
        geotransform=geotransform)

    return image_obj


def _read_image_ann(filename_orig,
                    filename,
                    band=None,
                    verbose=True,
                    input_format=None,
                    only_header=True,
                    read_only=True,
                    edges_outer=None,
                    edges_center=None,
                    header_file=None,
                    header_dict=None,
                    # ref_image_obj=None,
                    plant_transform_obj=None,
                    metadata=None,
                    in_null=None,
                    out_null=None,
                    input_key=None,
                    force=None,
                    flag_no_messages=False,
                    flag_exit_if_error=True,
                    image_obj_kwargs={},
                    **kwargs_orig):
    kwargs = locals()
    args = []
    args.append(kwargs.pop('filename_orig'))
    args.append(kwargs.pop('filename'))
    ext = filename.split('.')[-1]
    error_message = ('data dimensions of %s '
                     'could not be determined'
                     % filename)
    if not((input_format is not None and
            'ANN' in input_format) or
           ext in plant.EXT_READ_FROM_ANN_FILE):
        return
    if header_dict is None:
        header_dict = plant.parse_uavsar_filename(filename)
    if header_file is None:
        header_file = plant.get_annotation_file(
            header_dict, dirname=os.path.dirname(filename))

    if header_file is None:
        header_file_search_string = \
            os.path.join(os.path.dirname(filename), '*ann')
        header_file_list = plant.glob(header_file_search_string)
        if len(header_file_list) == 0:
            if filename.endswith('.dat'):
                return
            error_message = f'annotation file not found for {filename}'
            plant.handle_exception(error_message,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            # sys.exit(1)
            return

    if header_file is None:
        header_file = header_file_list[0]
    elif isinstance(header_file, list):
        header_file = header_file[0]

    if header_dict is None:
        header_dict = plant.parse_uavsar_filename(header_file)

    if metadata is None:
        try:
            metadata = read_parameters_annotation_file(header_file)
        except:
            pass

    if not metadata:
        plant.handle_exception(error_message,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    if ext and ext.lower() == 'llh' or ext.lower() == 'lkv':
        nbands_orig = 3
        scheme = 'BIP'
    else:
        nbands_orig = 1
        scheme = 'BSQ'

    length_orig = None
    width_orig = None
    flag_geo = ext in plant.EXT_READ_FROM_ANN_FILE_GEO
    file_size = os.stat(filename).st_size
    complement_list = ['_pwr', '_mag', '', '_phase']
    suffix = ''
    if 'segment_number' in header_dict.keys():
        suffix += header_dict['segment_number'][1:]
    if 'downsample_factor' in header_dict.keys():
        if suffix:
            suffix += '_'
        suffix += header_dict['downsample_factor']
    if suffix:
        suffix = '_'+suffix
        for complement in reversed(complement_list[:]):
            complement_list.insert(0, suffix+complement)

    if ext.lower() == 'sch':
        try:
            length_orig = int(metadata['SCH Number of Along Track Lines'])
            width_orig = int(metadata['SCH Number of Cross Track Samples'])
        except KeyError:
            pass
    elif flag_geo:
        try:
            length_orig = int(metadata['GRD Latitude Lines'])
            width_orig = int(metadata['GRD Longitude Samples'])
        except KeyError:
            pass

    if width_orig is None:
        for complement in complement_list:
            try:
                length_orig = int(metadata[ext+complement +
                                           '.set_rows'])
                width_orig = int(metadata[ext+complement +
                                          '.set_cols'])
                break
            except KeyError:
                pass

    if width_orig is None and not flag_geo:
        for complement in complement_list:
            ext_effective = 'slt'
            try:
                length_orig = int(metadata[ext_effective+complement +
                                           '.set_rows'])
                width_orig = int(metadata[ext_effective+complement +
                                          '.set_cols'])
                break
            except KeyError:
                pass

    if width_orig is None:
        file_list = plant.glob(os.path.join(plant.dirname(filename), '*'))
        similar_file = None
        if ext == 'inc':
            similar_file_ext_list = ['hgt']
        elif ext.endswith('grd'):
            similar_file_ext_list = ['grd']
        else:
            similar_file_ext_list = ['slc']
        for similar_file_ext in similar_file_ext_list:
            for complement in complement_list:
                try:
                    length_orig = int(metadata[similar_file_ext +
                                               complement +
                                               '.set_rows'])
                    width_orig = int(metadata[similar_file_ext +
                                              complement +
                                              '.set_cols'])
                    break
                except KeyError:
                    pass
    if width_orig is None:
        plant.handle_exception(error_message,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return
        # sys.exit(1)

    type_size = file_size/(length_orig*width_orig*nbands_orig)

    if type_size == 1:
        dtype_str = 'byte'
    elif type_size == 2:
        dtype_str = 'int16'
    elif type_size == 4:
        dtype_str = 'float32'
    elif type_size == 8:
        dtype_str = 'complex64'
    elif type_size == 16:
        dtype_str = 'complex128'
    else:
        plant.handle_exception(error_message,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        # sys.exit(1)
        return

    geotransform = None
    lat_arr = None
    lon_arr = None
    # if ext.lower() == 'grd':
    if flag_geo:
        try:
            lat = float(metadata['GRD Starting Latitude'])
            lon = float(metadata['GRD Starting Longitude'])
            step_lat = float(metadata['GRD Latitude Spacing'])
            step_lon = float(metadata['GRD Longitude Spacing'])
            lat_arr = [lat, lat+step_lat*(length_orig-1)]
            lon_arr = [lon, lon+step_lon*(width_orig-1)]
        except:
            pass

    if ((lat_arr is None or lon_arr is None) and
            flag_geo):
        for complement in complement_list:
            try:
                lat = float(metadata['grd'+complement +
                                     '.row_addr'])
                lon = float(metadata['grd'+complement +
                                     '.col_addr'])
                step_lat = float(metadata['grd'+complement +
                                          '.row_mult'])
                step_lon = float(metadata['grd'+complement +
                                          '.col_mult'])
                lat_arr = [lat, lat+step_lat*(length_orig-1)]
                lon_arr = [lon, lon+step_lon*(width_orig-1)]
            except:
                pass

    if ((lat_arr is None or lon_arr is None) and
            ext in plant.EXT_READ_FROM_ANN_FILE_GEO):
        for complement in complement_list:
            try:
                lat = float(metadata[ext+complement +
                                     '.row_addr'])
                lon = float(metadata[ext+complement +
                                     '.col_addr'])
                step_lat = float(metadata[ext+complement +
                                          '.row_mult'])
                step_lon = float(metadata[ext+complement +
                                          '.col_mult'])
                lat_arr = [lat, lat+step_lat*(length_orig-1)]
                lon_arr = [lon, lon+step_lon*(width_orig-1)]
            except:
                pass

    '''
    if not only_header:
        print('WARNING header of %s not found. '
              'Considering %s and '
              'dimensions from %s.'
              % (filename, dtype_str, header_file))
    '''
    # width = width if width is not None else width_orig
    # length = length if length is not None else length_orig

    if (lat_arr is not None and lon_arr is not None):
        geotransform = plant.get_geotransform(lat_arr=lat_arr,
                                              lon_arr=lon_arr,
                                              step_lat=step_lat,
                                              step_lon=step_lon,
                                              keep_step_lat_signal=True)
        if step_lat < 0:
            geotransform = plant.get_geotransform(
                geotransform=geotransform,
                lon_size=width_orig,
                lat_size=length_orig)
        geotransform = plant.crop_geotransform(
            geotransform,
            plant_transform_obj=plant_transform_obj,
            # offset_x=offset_x,
            # offset_y=offset_y,
            # new_lon_size=width,
            # new_lat_size=length,
            lon_size=width_orig,
            lat_size=length_orig)

    # if only_header:
    #    image_obj = plant.PlantImage(filename=filename,
    #                           # image=image,
    #                           plant_transform_obj=plant_transform_obj,
    #                           width=width,
    #                           length=length,
    #                           width_orig=width_orig,
    #                           length_orig=length_orig,
    #                           dtype=plant.get_np_dtype(dtype_str),
    #                           file_format='BIN',
    #                           geotransform=geotransform,
    #                           # scheme=scheme,
    #                           nbands=1)
    #    return image_obj
    # input_data=image_obj,

    if band is None:
        band_orig = None
        nbands = nbands_orig
    elif isinstance(band, Sequence) or isinstance(band, np.ndarray):
        band_orig = band
        nbands = len(band)
    else:
        band_orig = [band]
        nbands = 1
    image_obj = plant.PlantImage(
        filename=filename,
        filename_orig=filename_orig,
        header_file=header_file,
        # image=image,
        # band=band,
        nbands=nbands,
        nbands_orig=nbands_orig,
        width=width_orig,
        length=length_orig,
        input_key=input_key,
        band_orig=band_orig,
        # depth=depth_orig,
        dtype=dtype_str,
        plant_transform_obj=plant_transform_obj,
        file_format='BIN',
        # geotransform=None,
        scheme=scheme)
    kwargs['image_obj'] = image_obj
    kwargs['band'] = band
    kwargs['verbose'] = False
    # kwargs.pop('input_format')
    kwargs.pop('header_dict')
    kwargs.pop('header_file')
    if verbose and not only_header and band is None:
        print(f'opening: {filename} (annotation file)')
    elif verbose and not only_header:
        print(f'opening: {filename} (annotation file, band: {band})')

    image_obj = _read_image_bin(*args, **kwargs)
    # args[0] = 'BIN:'+filename+':'+str(width_orig)+':'+dtype_str
    # plant_transform_obj.crop_window = crop_window
    # plant_transform_obj.offset_x = offset_x
    # plant_transform_obj.offset_y = offset_y
    # plant_transform_obj.width = width
    # plant_transform_obj.length = length
    # kwargs['plant_transform_obj'] = plant_transform_obj
    # kwargs['offset_x'] = offset_x
    # kwargs['offset_y'] = offset_y
    # kwargs['width'] = width
    # kwargs['length'] = length
    # kwargs['input_format'] = 'BIN'
    # kwargs['scheme'] = 'BIP'
    # image_obj = read_image(*args, **kwargs)
    # if not only_header:
    #    for current_band in range(image_obj.nbands):
    #        image = image_obj.get_image(band=current_band)
    #        image = image[::-1, :]
    #        image_obj.set_image(image, band=current_band)
    # image_obj.set_metadata(metadata)
    if (image_obj is not None and plant_transform_obj is not None and
            only_header):


        image_obj.get_band(band=0)._applied_transform_obj_crop_header_list = \
                [plant_transform_obj.get_crop_dict()]


        # image_obj._applied_transform_obj_crop_header_list.append(
        #     plant_transform_obj.get_crop_dict())



        
    elif image_obj is not None and plant_transform_obj is not None:


        image_obj.get_band(band=0)._applied_transform_obj_crop_image_list = \
                [plant_transform_obj.get_crop_dict()]
        
        # image_obj._applied_transform_obj_crop_image_list.append(
        #     plant_transform_obj.get_crop_dict())




        
    if image_obj is not None and geotransform is not None:
        image_obj.set_geotransform(geotransform,
                                   realize_changes=False)

    if (plant.isnan(in_null) and
            ((filename.endswith('.hgt') or
              filename.endswith('.hgt.'+ext)) or
             (filename.endswith('.inc') or
              filename.endswith('.inc.'+ext)))):
        in_null = -10000
    elif (plant.isnan(in_null) and
            ((filename.endswith('.slope') or
              filename.endswith('.slope.'+ext)))):
        in_null = -10000 - 1j*10000
    elif (plant.isnan(in_null) and
          (filename.endswith('.grd') or
           filename.endswith('.grd.'+ext) or
           filename.endswith('.cor') or
           filename.endswith('.cor.'+ext) or
           filename.endswith('.rtc') or
           filename.endswith('.rtc.'+ext) or
           filename.endswith('.rtcgrd') or
           filename.endswith('.rtcgrd.'+ext) or
           filename.endswith('.mlc') or
           filename.endswith('.mlc.'+ext) or
           filename.endswith('.slc') or
           filename.endswith('.slc.'+ext) or
           filename.endswith('.pwr') or
           filename.endswith('.pwr.'+ext))):
        in_null = 0
    image_obj.file_format = 'ANN'
    # image_obj =
    plant.apply_null(image_obj,
                     in_null=in_null,
                     out_null=out_null)
    return image_obj

def get_annotation_file(input_dict, ext=None, dirname=None):
    if input_dict is None:
        return
    ext = '.ann' if ext is None else f'*{ext}'.replace('..', '.')
    ext = ext.lower()
    site_name = input_dict.get('site_name', '*')
    line_id = input_dict.get('line_id', '*')
    flight_id = input_dict.get('flight_id', '*')
    data_take_number = input_dict.get('data_take_number', '*')
    acquisition_date = input_dict.get('acquisition_date', '*')
    stack_number = input_dict.get('stack_number', '*')
    # stack_version = input_dict.get('stack_version', '*')
    # {stack_version}
    baseline_corrected = input_dict.get('baseline_corrected', False)
    baseline_uncorrected = input_dict.get('baseline_uncorrected', False)
    baseline_corrected_str = ''
    if baseline_corrected:
        baseline_corrected_str = '_BC'
    elif baseline_uncorrected:
        baseline_corrected_str = '_BU'

    band = input_dict.get('band', '*')
    steering = input_dict.get('steering', '*')
    polarization = input_dict.get('polarization', '*')
    downsample_factor = input_dict.get('downsample_factor', None)
    segment_number = input_dict.get('segment_number', None)
    list_ext = []
    if not 'dop' in ext and segment_number:
        list_ext += [segment_number]
    if not 'dop' in ext and downsample_factor:
        list_ext += [downsample_factor]
    if len(list_ext) == 0:
        extended_ext = ext
    else:
        extended_ext = '_'+'_'.join(list_ext)+ext
    if 'slc' in ext:
        annotation_file = (f'{site_name}_{line_id}_{flight_id}'
                           f'_{data_take_number}_{acquisition_date}'
                           f'_{band}{steering}{polarization}'
                           f'_{stack_number}'
                           # f'_{stack_version}'
                           f'{baseline_corrected_str}{extended_ext}')
    elif 'llh' in ext or 'lkv' in ext:
        annotation_file = (f'{site_name}_{line_id}_{stack_number}'
                           f'{baseline_corrected_str}{extended_ext}')
    elif 'dop' in ext:
        annotation_file = (f'{site_name}_{line_id}_{stack_number}'
                           f'{baseline_corrected_str}{extended_ext}')
    else:
        annotation_file = (f'{site_name}_{line_id}_{flight_id}'
                           f'_{data_take_number}_{acquisition_date}'
                           f'_{band}{steering}*_{stack_number}'
                           f'{baseline_corrected_str}{extended_ext}')
    # print('*** extended_ext: ', extended_ext)
    # print('*** annotation_file: ', annotation_file)
    annotation_file = '*'.join([x for x in annotation_file.split('*') if x])
    if dirname is not None:
        annotation_file = os.path.join(dirname, annotation_file)
    annotation_file_list = plant.glob(annotation_file)
    # print('*** list: ', annotation_file_list)
    if len(annotation_file_list) == 0:
        return
    if len(annotation_file_list) == 1:
        return annotation_file_list[0]
    return annotation_file_list



def _read_image_gdal(filename_orig,
                     filename,
                     **kwargs):
    kwargs['filename'] = filename
    kwargs['filename_orig'] = filename_orig

    # print('*** read_image_gdal (orig): ', filename_orig) 
    # kwargs = locals()
    current_dataset = None
    gdal.UseExceptions()
    gdal.ErrorReset()
    try:
        current_dataset = gdal.Open(filename)
    except KeyboardInterrupt:
        current_dataset = None
        gdal.ErrorReset()
        raise plant.PlantExceptionKeyboardInterrupt
    except:
        current_dataset = None
        gdal.ErrorReset()
        return

    image_obj = read_image_from_gdal_dataset(
        current_dataset,
        **kwargs)
    return image_obj


def _hdf5_get_sub_datasets(h5py_obj, sub_datasets_keys, sub_datasets,
                           prefix):
    # if isinstance(sub_datasets_keys, str):
    # if len(sub_datasets_keys) == 0:
    # sub_datasets.append(sub_datasets_keys)
    #     return
    # while len(sub_datasets_keys) != 0:
    # print(h5py_obj.name)
    for s in sub_datasets_keys:
        # s = sub_datasets_keys.pop()
        # print('*** opening key: ', s)
        # if 'keys' not in h5py_obj[s].__dir__() and
        # isinstance(h5py_obj[s], str):
        #     sub_datasets.append(sub_datasets_keys)
        # el
        if 'keys' not in h5py_obj[s].__dir__():
            # print('*** dtype: ', h5py_obj[s].dtype)
            shape = [str(x) for x in h5py_obj[s].shape]
            name = ''
            # name += ' /'+h5py_obj[s].name
            try:
                name += '('+str(h5py_obj[s].dtype)+')'
            except TypeError:
                pass
            if len(shape) > 0:
                if name:
                    name += ' '
                name += f'{len(shape)}-D array ['+'x'.join(shape)+']'
            if len(shape) == 0:
                name += f': {h5py_obj[s][()]}'
            sub_datasets.append((prefix+h5py_obj.name + '/' + s,
                                 name))
            continue
        sub_datasets_keys = list(h5py_obj[s].keys())
        # print('*** opening key 1: ', sub_datasets_keys)
        sub_datasets_keys = [s+'/'+k for k in sub_datasets_keys]
        # print('*** opening key 2: ', sub_datasets_keys)
        _hdf5_get_sub_datasets(h5py_obj, sub_datasets_keys, sub_datasets,
                               prefix)


def get_nisar_subdatasets(hdf5_file, input_key,
                          band=None):

    # if isinstance(hdf5_file, h5py._hl.files.File)
    if isinstance(hdf5_file, str):
        hdf5_obj = h5py.File(hdf5_file, 'r')
    else:
        hdf5_obj = hdf5_file

    for product in plant.NISAR_PRODUCT_LIST:
        current_key = (os.path.join('//', 'science', 'LSAR', product,
                                    f'frequency{input_key}',
                                    'listOfPolarizations'))
        if current_key not in hdf5_obj:
            continue
        list_of_polarizations = hdf5_obj[current_key]
        break
    else:
        # del hdf5_obj
        return

    if band is None:
        nbands = len(list_of_polarizations)
        band_indexes = list(range(nbands))
    else:
        band_indexes = plant.get_int_list(band)
        nbands = len(band_indexes)
    list_of_polarizations = list_of_polarizations[band_indexes]
    subdatasets_dict = {}
    for b, pol_encoded in enumerate(list_of_polarizations):
        pol = pol_encoded.decode().upper()
        current_key = (f'//science/LSAR/{product}/'
                       f'frequency{input_key}/{pol}')
        if current_key not in hdf5_obj:
            current_key = (f'//science/LSAR/{product}/'
                           f'frequency{input_key}/{pol}{pol}')
        subdatasets_dict[pol] = current_key
    # del hdf5_obj
    return subdatasets_dict



def _read_hdf5(filename_orig,
               filename,
               input_format=None,
               only_header=False,
               # edges_center=None,
               # edges_outer=None,
               input_key=None,
               band=None,
               # filename=None,
               # filename_orig=None,
               # ref_image_obj=None,
               plant_transform_obj=None,
               # in_null=None,
               out_null=None,
               force=False,
               verbose=True,
               flag_no_messages=False,
               flag_exit_if_error=False,
               image_obj_kwargs={},
               **kwargs_orig):
    # import h5py
    # filename_splitted = filename.split(':')
    kwargs = locals()
    length_orig = 1
    width_orig = 1
    # filename = filename_splitted[-2]
    # filename = filename.replace('"', '')
    # filename = filename.replace("'", '')
    # key = filename_splitted[-1].replace('//', '')
    ret_dict = plant.parse_filename(filename_orig)
    filename = ret_dict['filename']
    # print('*** here')
    if input_key is None:
        input_key = ret_dict['key']
    flag_single_subdataset = input_key is not None
    hdf5_obj = None
    if flag_single_subdataset:
        if verbose:
            print(f'opening: {filename} (HDF5: {input_key})')
        hdf5_file = h5py.File(filename, 'r')
        if input_format == 'NISAR' and input_key.upper() in ['A', 'B']:
            with plant.PlantIndent():
                subdatasets_dict = get_nisar_subdatasets(hdf5_file,
                                                         input_key,
                                                         band)
                del hdf5_file
                if subdatasets_dict is None:
                    print('ERROR NISAR product not found:'
                          f' {input_key} in {filename}')
                    return

                for b, pol in enumerate(subdatasets_dict.keys()):
                    current_key = subdatasets_dict[pol]

                    # input_key = current_key
                    if 'GCOV' in current_key:
                        driver = 'NETCDF'
                    else:
                        driver = 'HDF5'
                    kwargs['input_format'] = driver
                    kwargs['input_key'] = current_key
                    # kwargs['verbose'] = False
                    kwargs['filename_orig'] = (driver + ':' +
                                               filename + ':' +
                                               current_key)

                    if driver == 'NETCDF':
                        image_obj = read_image(**kwargs)
                    else:
                        image_obj = _read_hdf5(**kwargs)
                    # out_image_obj.set_filename_orig(
                    #     )
                    if image_obj is None:
                        return

                    if b == 0:
                        out_image_obj = image_obj.soft_copy()
                        # out_image_obj.geotransform = image_obj.geotransform
                        # out_image_obj.projection = image_obj.projection
                    # band_obj._applied_transform_obj_crop_header_list = \
                    #     [plant_transform_obj.get_crop_dict()]
                    # print('*** image_obj id: ', id(image_obj))
                    out_image_obj.set_band(image_obj.band, band=b,
                                           realize_changes=False)
                    out_image_obj.set_name(pol, band=b)

                    # image_obj.nbands = len(list_of_polarizations)
                out_image_obj.nbands_orig = len(subdatasets_dict)
                out_image_obj._band_orig = np.arange(
                    out_image_obj._nbands_orig)
                out_image_obj.file_format = 'NISAR'
                out_image_obj.input_key = input_key
                return out_image_obj
        nbands_orig = 1
        try:
            hdf5_obj = hdf5_file[input_key]
        except KeyError:
            print(f'ERROR sub-dataset {input_key} not found'
                  f' in {filename}')
            return
        flag_single_subdataset = not isinstance(hdf5_obj, h5py.Group)

    if not flag_single_subdataset:
        nbands_orig = 0
        sub_datasets = []
        if hdf5_obj is None:
            hdf5_obj = h5py.File(filename, 'r')
        sub_datasets_keys = list(hdf5_obj.keys())
        hdf5_prefix = f'HDF5:{filename}:'
        _hdf5_get_sub_datasets(hdf5_obj, sub_datasets_keys, sub_datasets,
                               prefix=hdf5_prefix)
        # print('*** sub_datasets_keys: ', sub_datasets_keys)
        # print('*** sub_datasets: ', sub_datasets)
        # sub_datasets = list(h5py.File(filename, 'r').iterkeys())
        # if not flag_exit_if_error:
        #    error_message = 'indeterminate HDF5 subset'
        # else:
        error_message = ('the input has multiple sub-datasets.'
                         ' Please, select one sub-dataset'
                         ' from the list above.')
        error_description = ''
        if not flag_no_messages:
            # print('INFO available sub-datasets:')
            for i, sub_dataset in enumerate(sub_datasets):
                for j, element in enumerate(sub_dataset):
                    # prefix = f'{i} ' if j == 0 else ' '*4
                    prefix = '' if j == 0 else ' '*4
                    error_description += f'{prefix}{element}\n'
                    # print(f'{prefix}{element}')
        plant.handle_exception(error_message,
                               flag_exit_if_error,
                               error_description=error_description,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    if len(hdf5_obj.shape) == 1:
        width_orig = hdf5_obj.shape[0]
    elif len(hdf5_obj.shape) == 2:
        length_orig = hdf5_obj.shape[0]
        width_orig = hdf5_obj.shape[1]
    elif len(hdf5_obj.shape) == 3:
        # length_orig = hdf5_obj.shape[0]
        # width_orig = hdf5_obj.shape[1]
        # nbands_orig = hdf5_obj.shape[2]
        length_orig = hdf5_obj.shape[1]
        width_orig = hdf5_obj.shape[2]
        nbands_orig = hdf5_obj.shape[0]
    elif len(hdf5_obj.shape) != 0:
        raise NotImplementedError

    # print('*** length_orig =', length_orig)
    # print('*** width_orig =', width_orig)
    # print('*** nbands_orig =', nbands_orig)
    # if len(sub_datasets) == 0:
    if band is None:
        nbands = nbands_orig
        band_indexes = list(range(nbands))

    else:
        band_indexes = plant.get_int_list(band)
        nbands = len(band_indexes)
    '''
    if band_indexes:
        # debug(band_indexes)
        current_band = dataset.GetRasterBand(band_indexes[0] + 1)
    else:
        # debug(1)
        current_band = dataset.GetRasterBand(1)
    '''
    # if current_band is None and sub_datasets and not input_key:
    # if sub_datasets and
    # if not input_key:
    if plant_transform_obj is not None:
        plant_transform_obj.update_crop_window(length_orig=length_orig,
                                               width_orig=width_orig)
        # geotransform=geotransform,
        # projection=projection,
        crop_window = plant_transform_obj.crop_window
        offset_x, offset_y, width, length = crop_window
    else:
        offset_x = 0
        offset_y = 0
        width = width_orig
        length = length_orig

    image_obj = plant.PlantImage(
        filename=filename,
        filename_orig=filename_orig,
        width=width,
        length=length,
        width_orig=width_orig,
        length_orig=length_orig,
        offset_x=offset_x,
        offset_y=offset_y,
        # metadata=metadata,
        nbands=nbands,
        nbands_orig=nbands_orig,
        band_orig=band_indexes,
        input_key=input_key,
        # null=null,
        # projection=projection,
        file_format=input_format,
        plant_transform_obj=plant_transform_obj,
        # geotransform=geotransform,
        # gcp_list=gcp_list,
        # gcp_projection=gcp_projection,
        **image_obj_kwargs)
    for b in range(image_obj.nbands):
        band = image_obj.get_band(band=b)
    image_obj = _read_image_hdf5(hdf5_obj,
                                 image_obj,
                                 only_header=only_header,
                                 plant_transform_obj=plant_transform_obj,
                                 out_null=out_null,
                                 verbose=verbose,
                                 force=force)
    return image_obj

def read_image_from_gdal_dataset(
        dataset,
        input_format=None,
        only_header=False,
        edges_center=None,
        edges_outer=None,
        input_key=None,
        band=None,
        filename=None,
        filename_orig=None,
        # ref_image_obj=None,
        plant_transform_obj=None,
        in_null=None,
        out_null=None,
        force=False,
        verbose=True,
        flag_no_messages=False,
        flag_exit_if_error=False,
        image_obj_kwargs={},
        **kwargs_orig):

    # print('*** flag_exit_if_error: ', flag_exit_if_error)
    # print('*** read_image_gdal (orig): ', filename_orig) 

    if input_format is None:
        input_format = 'GDAL'

    file_format = str(dataset.GetDriver().ShortName)
    sub_datasets = dataset.GetSubDatasets()
    if 'HDF5' in file_format.upper():
        # import h5py
        # filename_splitted = filename.split(':')
        length_orig = 1
        width_orig = 1
        # filename = filename_splitted[-2]
        # filename = filename.replace('"', '')
        # filename = filename.replace("'", '')
        # key = filename_splitted[-1].replace('//', '')
        ret_dict = plant.parse_filename(filename_orig)
        # print('*** ret_dict: ', ret_dict)
        filename = ret_dict['filename']
        # print('*** here')
        if input_key is None:
            input_key = ret_dict['key']
        if input_key is not None:
            if verbose:
                print(f'opening: {filename} (HDF5: {input_key})')
            nbands_orig = 1
            hdf5_obj = h5py.File(filename, 'r')[input_key]
            # print('*** here.shape: ', hdf5_obj.shape)
            # depth_orig = 1
            if len(hdf5_obj.shape) == 1:
                width_orig = hdf5_obj.shape[0]
            elif len(hdf5_obj.shape) == 2:
                length_orig = hdf5_obj.shape[0]
                width_orig = hdf5_obj.shape[1]
            elif len(hdf5_obj.shape) == 3:
                # length_orig = hdf5_obj.shape[0]
                # width_orig = hdf5_obj.shape[1]
                # nbands_orig = hdf5_obj.shape[2]
                length_orig = hdf5_obj.shape[1]
                width_orig = hdf5_obj.shape[2]
                nbands_orig = hdf5_obj.shape[0]
            elif len(hdf5_obj.shape) != 0:
                raise NotImplementedError
        else:
            nbands_orig = 0
            sub_datasets = []
            h5py_obj = h5py.File(filename, 'r')
            sub_datasets_keys = list(h5py_obj.keys())
            hdf5_prefix = f'HDF5:{filename}:'
            _hdf5_get_sub_datasets(h5py_obj, sub_datasets_keys, sub_datasets,
                                   prefix=hdf5_prefix)
            # print('*** sub_datasets_keys: ', sub_datasets_keys)
            # print('*** sub_datasets: ', sub_datasets)
            # sub_datasets = list(h5py.File(filename, 'r').iterkeys())
        # print('*** length_orig =', length_orig)
        # print('*** width_orig =', width_orig)
        # print('*** nbands_orig =', nbands_orig)
        # if len(sub_datasets) == 0:
    else:
        try:
            width_orig = dataset.RasterXSize
        except:
            width_orig = None
        if (width_orig is None and
                input_format == 'GDAL'):
            dataset = None
            gdal.ErrorReset()
            plant.handle_exception('opening file %s (GDAL)'
                                   % filename,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return
        elif width_orig is None:
            return
        length_orig = dataset.RasterYSize
        nbands_orig = dataset.RasterCount

    if band is None:
        nbands = nbands_orig
        band_indexes = list(range(nbands))

    else:
        band_indexes = plant.get_int_list(band)
        nbands = len(band_indexes)
    '''
    print('*** band: ', band)
    print('*** nbands: ', nbands)
    print('*** nbands_orig: ', nbands_orig)
    print('*** band_indexes: ', band_indexes)
    if nbands == 1:
        raise
    '''
    metadata = dataset.GetMetadata()
    # print('*** in metadata: ', metadata)
    # description = dataset.GetDescription()
    projection = dataset.GetProjectionRef()

    if 'LOCAL_CS["Arbitrary"' in projection:
        projection = None

    geotransform_edges = list(dataset.GetGeoTransform())
    gcp_list = dataset.GetGCPs()
    try:
        gcp_projection = dataset.GetGCPProjection()
    except RuntimeError:
        # PROJ: proj_create_from_database: ellipsoid not found
        gcp_projection = None

    '''
    print('key: ', input_key)
    print('filename: ', filename)
    print('filename: ', filename_orig)
    print('geotransform: ', geotransform_edges)
    '''

    # flag_geotransform_from_isce = False
    # file_format == 'ISCE' or

    if (plant.valid_coordinates(geotransform=geotransform_edges,
                                lat_size=length_orig,
                                lon_size=width_orig,
                                projection=projection)):
        # if 'ENVI' in file_format.upper():
        if edges_center:
            # print('WARNING geotransform from GDAL header (center)')
            # if verbose:
            #     print('WARNING considering boundary refs in'
            #           ' the center of the pixel..')
            geotransform = geotransform_edges
        else:
            # print('WARNING geotransform from GDAL header (edges to center)')
            geotransform = \
                plant.geotransform_edges_to_centers(geotransform_edges,
                                                    lat_size=length_orig,
                                                    lon_size=width_orig,
                                                    keep_step_lat_signal=True)

        # print('geotransform, projection: ', geotransform, projection)

    # [0:6]
        # geotransform += [geotransform[0]+geotransform[1]*(width-1),
        #                 geotransform[3]+geotransform[5]*(length_orig-1),
        #                 width, length_orig]
    elif ((plant.isfile(filename + '.xml') or
           filename.endswith('.xml'))):
        # raise
        geotransform_temp = plant.get_geotransform_from_header(filename)
        if plant.valid_coordinates(geotransform=geotransform_temp,
                                   lat_size=length_orig,
                                   lon_size=width_orig,
                                   projection=projection):
            if edges_outer:
                # print('WARNING geotransform from ISCE header
                # (edges to centers)')
                geotransform = plant.geotransform_edges_to_centers(
                    geotransform_temp,
                    lat_size=length_orig,
                    lon_size=width_orig)
            else:
                # print('WARNING geotransform from ISCE header (center)')
                geotransform = geotransform_temp
            # flag_geotransform_from_isce = True
            # if plant.isfile(filename + '.xml') and verbose:
            #    print('WARNING using GDAL but georeference from '
            #          'ISCE header: %s' %(filename+'.xml'))
            # elif verbose:
            #    print('WARNING using GDAL but georeference from '
            #           'ISCE header: %s' %(filename))
        else:
            geotransform = None
    else:
        geotransform = None

    if plant_transform_obj is None:
        plant_transform_obj = plant.PlantTransform(verbose=verbose)

    if geotransform is not None and plant_transform_obj is not None:
        plant_transform_obj.update_crop_window(geotransform=geotransform,
                                               projection=projection,
                                               length_orig=length_orig,
                                               width_orig=width_orig)

    crop_window = plant_transform_obj.crop_window
    offset_x, offset_y, width, length = crop_window
    read_as_array_kwargs = {}
    if offset_x is not None:
        read_as_array_kwargs['xoff'] = int(offset_x)
    if offset_y is not None:
        read_as_array_kwargs['yoff'] = int(offset_y)
    if width is not None:
        read_as_array_kwargs['win_xsize'] = int(width)
    if length is not None:
        read_as_array_kwargs['win_ysize'] = int(length)

    flag_transpose = False
    flag_invert = False
    # print('*** metadata: ', metadata)
    if (geotransform is None and metadata and
            ('Grid_GridHeader' in metadata or
             'Grids_G1_G1_GridHeader' in metadata)):
        if 'Grid_GridHeader' in metadata:
            grid_header = metadata['Grid_GridHeader']
        else:
            grid_header = metadata['Grids_G1_G1_GridHeader']
        # print(grid_header.__class__)
        grid_header = grid_header.replace('\n', '')
        grid_header_splitted = grid_header.split(';')
        grid_header_dict = {}
        for element in grid_header_splitted:
            element_splitted = element.split('=')
            if len(element_splitted) != 2:
                continue
            if plant.isnumeric(element_splitted[1]):
                value = float(element_splitted[1])
            else:
                value = element_splitted[1]
            grid_header_dict[element_splitted[0]] = value

        # Registration=CENTER;
        # print('*** grid_header_dict: ', grid_header_dict)
        step_lat = grid_header_dict['LatitudeResolution']
        step_lon = grid_header_dict['LongitudeResolution']
        lat_arr = [grid_header_dict['SouthBoundingCoordinate'],
                   grid_header_dict['NorthBoundingCoordinate']]
        lon_arr = [grid_header_dict['WestBoundingCoordinate'],
                   grid_header_dict['EastBoundingCoordinate']]
        # print('*** lat_arr: ', lat_arr)
        # print('*** lon_arr: ', lon_arr)
        # print('*** step_lat: ', step_lat)
        # print('*** step_lon: ', step_lon)
        length_temp = (lat_arr[1]-lat_arr[0])/step_lat
        width_temp = (lon_arr[1]-lon_arr[0])/step_lon
        if length_temp == width_orig and width_temp == length_orig:
            flag_transpose = True
            length_orig = width_temp
            width_orig = length_temp
            if not only_header:
                print('WARNING dimensions seem to be inverted.'
                      ' Transposing image..')
        geotransform = plant.get_geotransform(lat_arr=lat_arr,
                                              lon_arr=lon_arr,
                                              step_lat=step_lat,
                                              step_lon=step_lon)
        # lat_size=length_orig,
        # lon_size=width_orig)
        if 'CENTER' not in grid_header_dict['Registration'].upper():
            geotransform = plant.geotransform_edges_to_centers(
                geotransform,
                lat_size=length_orig,
                lon_size=width_orig)

        flag_invert = 'NORTH' in grid_header_dict['Origin'].upper()
        # flag_set_transformation_function = True
        # image_obj.plant_transform_obj.function_dict[
        #     'radiometric_correction'] = calibration_name

        '''
        image = image[::-1, :]
        print(grid_header_dict)
        '''

    # plant.debug(band_indexes)
    # plant.debug(dataset)
    # plant.debug(dataset.RasterCount)
    if ('HDF5' not in file_format.upper() and
            band_indexes and
            np.max(band_indexes) + 1 > dataset.RasterCount):

        gdal.ErrorReset()
        plant.handle_exception('invalid band: %d '
                               '(number of available bands: %d)'
                               % (np.max(band_indexes),
                                  dataset.RasterCount),
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        dataset = None
        gdal.ErrorReset()
        # debug('exit 6')
        return

    if band_indexes:
        # debug(band_indexes)
        current_band = dataset.GetRasterBand(band_indexes[0] + 1)
    else:
        # debug(1)
        current_band = dataset.GetRasterBand(1)

    # plant.debug(file_format)

    # if current_band is None and file_format == 'HDF5':
    # print('*******************')
    # print('*** current_band: ', current_band)
    # print('*** sub_datasets: ', sub_datasets)
    # print('*** input_key: ', input_key)

    if current_band is None and sub_datasets and not input_key:
        # if not flag_exit_if_error:
        #    error_message = 'indeterminate HDF5 subset'
        # else:
        error_message = ('indeterminate HDF5 subset. Please,'
                         ' select a sub-dataset: \n')
        if not flag_no_messages:
            # print('INFO available sub-datasets:')
            for i, sub_dataset in enumerate(sub_datasets):
                for j, element in enumerate(sub_dataset):
                    # prefix = f'{i} ' if j == 0 else ' '*4
                    prefix = '' if j == 0 else ' '*4
                    error_message += f'{prefix}{element}\n'
                    # print(f'{prefix}{element}')
        plant.handle_exception(error_message,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    if current_band is None and not input_key:
        dataset = None
        gdal.ErrorReset()
        if band_indexes:
            band_str = ' (band: %d)' % band_indexes[0]
        else:
            band_str = ''
        plant.handle_exception(f'file could not be opened: {filename}'
                               f' {band_str}',
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        # debug('exit 7')
        return

    # band_descr = current_band.GetDescription()
    # if not band_descr:
    #    band_descr = ''
    # else:
    #    band_descr = ' "'+band_descr+'"'

    # min = current_band.GetMinimum()
    # max = current_band.GetMaximum()
    # scale = current_band.GetScale()
    # unit_type = current_band.GetUnitType()

    # dtype = plant.get_np_dtype_from_gdal(current_band.DataType)

    length = length if length is not None else length_orig
    width = width if width is not None else width_orig

    '''
    if file_format.upper() == 'ENVI':
        file_nbands = dataset.RasterCount
        n_elements = length*width
        file_size = os.stat(filename).st_size
        type_size = round(file_size / (n_elements*file_nbands))
        if (plant.get_dtype_size(dtype) == 4 and type_size == 8):
            print('WARNING data type and file size do not match. '
                  'This may cause future problems.')
            # if verbose:
            #    print('WARNING data type and file size do not match. '
            #          'Forcing data type from float32 to '
            #          'complex32 and changing driver from ENVI to ISCE')
            # dtype = np.complex64
    '''
    null = in_null if only_header else out_null
    # print('*** filename: ', filename)
    # print('*** filename_orig: ', filename_orig)
    # print('*** key: ', input_key)
    image_obj = plant.PlantImage(
        filename=filename,
        filename_orig=filename_orig,
        width=width,
        length=length,
        width_orig=width_orig,
        length_orig=length_orig,
        offset_x=offset_x,
        offset_y=offset_y,
        metadata=metadata,
        # description=description,
        nbands=nbands,
        nbands_orig=nbands_orig,
        band_orig=band_indexes,
        input_key=input_key,
        null=null,
        # dtype=dtype,
        projection=projection,
        file_format=file_format,
        # ref_image_obj=ref_image_obj,
        plant_transform_obj=plant_transform_obj,
        geotransform=geotransform,
        gcp_list=gcp_list,
        gcp_projection=gcp_projection,
        **image_obj_kwargs)

    '''
    if flag_set_transformation_function:
        image_obj.plant_transform_obj.function_dict[
            'custom_geotransformation'] = True
    '''

    # scheme=scheme

    # There seems to be a bug in GDAL when length=1
    # if image_obj.length == 1:
    #    kwargs['input_format']='ISCE'
    #    return(read_image(*args,
    #                      **kwargs))
    # GDAL does not correctly working with HDF5 and vectors
    # if only_header and not 'HDF5:' in filename.upper():

    if geotransform is not None:
        if (offset_x or
                offset_y or
                width is not None or
                length is not None):

            geotransform = plant.crop_geotransform(geotransform,
                                                   offset_x=offset_x,
                                                   offset_y=offset_y,
                                                   new_lon_size=width,
                                                   new_lat_size=length,
                                                   lon_size=width_orig,
                                                   lat_size=length_orig)
        geotransform = plant.get_geotransform(geotransform=geotransform,
                                              lat_size=length,
                                              lon_size=width)
        image_obj.set_geotransform(geotransform,
                                   realize_changes=False)

    # if only_header and not file_format == 'HDF5Image':
    #     return image_obj
    # due to bug in GDAL for HDF5 and vectors:
    # if file_format == 'HDF5Image':
    if 'HDF5' in file_format:
        image_obj = _read_image_hdf5(hdf5_obj,
                                     image_obj,
                                     only_header=only_header,
                                     plant_transform_obj=plant_transform_obj,
                                     out_null=out_null,
                                     verbose=verbose,
                                     force=force)
        return image_obj

    if verbose and not only_header:
        if band is None or nbands_orig == 1:
            print('opening: %s (GDAL: %s) '
                  % (filename, file_format))
        else:
            print('opening: %s (GDAL: %s, band(s): %s) '
                  % (filename, file_format, band))
    '''
    else:
        # ReadRaster(buf_line_space, buf_pixel_space)
        image = current_band.ReadAsArray(**read_as_array_kwargs)
        if image is None and len(read_as_array_kwargs) > 0:
            plant.handle_exception('file %s could not be opened'
                             ' using GDAL driver and parameters %s'
                             % (filename,
                                str(read_as_array_kwargs)),
                             flag_exit_if_error,
                             verbose=verbose)
            return
        # image_obj.offset_x = offset_x
        # image_obj.offset_y = offset_y
    if image is None:
        plant.handle_exception('file could not be opened: %s (GDAL)'
                         % filename,
                         flag_exit_if_error,
                         verbose=verbose)
        return
    '''

    if plant_transform_obj is not None:
        image_obj._applied_transform_obj_crop_list.append(
            plant_transform_obj.get_crop_dict())

    #  band_count = 0
    for b in range(nbands):
        # debug(nbands)
        if b != 0:
            current_band = dataset.GetRasterBand(band_indexes[b]+1)
            if current_band is None:
                current_band = None
                dataset = None
                gdal.ErrorReset()
                plant.handle_exception('band '+str(b) +
                                       ' not found in '+filename,
                                       flag_exit_if_error,
                                       flag_no_messages=flag_no_messages,
                                       verbose=verbose)
                # debug('exit 8')
                return

        dtype = plant.get_np_dtype_from_gdal(current_band.DataType)
        # print('* only header', only_header)
        if not only_header:
            # plant.debug(f'reading band {b} with args:'
            #             f' {read_as_array_kwargs}')
            image = current_band.ReadAsArray(**read_as_array_kwargs)
            # plant.debug('..done')
            # plant.debug(np.nanmean(image))

            if image is None and len(read_as_array_kwargs) > 0:
                current_band = None
                dataset = None
                gdal.ErrorReset()
                plant.handle_exception('file %s could not be opened'
                                       ' using GDAL driver and parameters %s'
                                       % (filename,
                                          str(read_as_array_kwargs)),
                                       flag_exit_if_error,
                                       flag_no_messages=flag_no_messages,
                                       verbose=verbose)
                # debug('exit 9')
                return

        no_data = current_band.GetNoDataValue()
        # print('*** no_data: ', no_data)
        band_null = no_data if plant.isnan(in_null) else in_null
        if not only_header:
            # image =
            plant.apply_null(image,
                             in_null=band_null,
                             out_null=out_null)
            if flag_transpose:
                image = np.transpose(image)
                image = image[::-1, :]
            if flag_invert:
                image = image[::-1, :]
        else:
            image = None
        # if input_use_ctable:
        ctable = current_band.GetColorTable()
        null = in_null if only_header else out_null
        description = current_band.GetDescription()
        if nbands == 1:
            flag_default_description = (
                isinstance(description, str) and
                description.startswith('Band ') and
                plant.isnumeric(description.replace('Band ', '')))
            if flag_default_description:
                description = None
        # print('*** image: ', image.__class__)
        band_obj = plant.PlantBand(image,
                                   dtype=dtype,
                                   null=null,
                                   name=description,
                                   # metadata=metadata,
                                   ctable=ctable)

        if plant_transform_obj is not None and only_header:
            band_obj._applied_transform_obj_crop_header_list = \
                [plant_transform_obj.get_crop_dict()]
        elif plant_transform_obj is not None:
            band_obj._applied_transform_obj_crop_image_list = \
                [plant_transform_obj.get_crop_dict()]
        # else:
        #     ctable = None
        image_obj.set_band(band_obj, band=b, realize_changes=False)
        # band_count += 1
    current_band = None
    dataset = None
    gdal.ErrorReset()
    # if only_header:
    #    # debug('exit 10')
    #    return image_obj
    if not only_header:
        image_obj.set_null(out_null, realize_changes=False)
    # image_obj = plant.apply_null(image_obj,
    #                             in_null=in_null,
    #                             out_null=out_null)
    # image_obj =
    plant.apply_mask(image_obj,
                     plant_transform_obj=plant_transform_obj,
                     verbose=verbose,
                     force=force)

    # debug('exit 11')
    return image_obj


def _read_image_bin(filename_orig,
                    filename,
                    band=None,
                    verbose=True,
                    only_header=True,
                    read_only=True,
                    edges_outer=None,
                    edges_center=None,
                    image_obj=None,
                    header_file=None,
                    input_format=None,
                    scheme=None,
                    # ref_image_obj=None,
                    plant_transform_obj=None,
                    offset=None,
                    in_null=None,
                    out_null=None,
                    force=None,
                    flag_no_messages=False,
                    flag_exit_if_error=True,
                    image_obj_kwargs={},
                    **kwargs_orig):
    # print('*** transform (isce): ', plant_transform_obj)
    kwargs = locals()
    args = []
    args.append(kwargs.pop('filename_orig'))
    args.append(kwargs.pop('filename'))

    # image = None
    geotransform = None
    dtype = None
    # header_file = None
    if header_file is None and filename.endswith('.xml'):
        header_file = filename
    elif header_file is None and plant.isfile(filename + '.xml'):
        header_file = filename + '.xml'
    if image_obj is None and header_file is not None:
        try:
            image_obj = get_info_from_xml(header_file)
        except:
            header_file = None
    # print('*** image_obj is None: ', image_obj is None)
    # print('*** header_file: ', header_file)
    if image_obj is None:
        kwargs['only_header'] = True
        kwargs['flag_no_messages'] = True
        kwargs.pop('image_obj')
        image_obj = _read_image_gdal(*args, **kwargs)
        # image_obj.header_file = header_file
    # print('*** image_obj is None: (after)', image_obj is None)
    if image_obj is None and header_file is None:
        if filename.endswith('.xml'):
            header_file = filename
        else:
            header_file = filename + '.xml'
        plant.handle_exception('invalid ISCE header: ' +
                               header_file,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        # sys.exit(1)
    if image_obj is None:
        plant.handle_exception('reading file: '+filename+' (ISCE)',
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        # sys.exit(1)
        return
    filelist = plant.glob(filename)
    if len(filelist) == 0:
        return
    filename = filelist[0]

    width_orig = image_obj.width_orig
    length_orig = image_obj.length_orig
    depth_orig = image_obj.depth_orig
    # print('*** ', width_orig, length_orig, depth_orig)
    dtype = image_obj.dtype
    if not scheme:
        scheme = image_obj.scheme
    if scheme is None:
        scheme = 'BSQ'
    nbands_orig = image_obj.nbands_orig

    nbands = nbands_orig
    band_indexes = plant.get_int_list(band)
    if offset is None:
        offset = 0
    if band_indexes:
        if np.max(band_indexes) > nbands_orig-1:
            plant.handle_exception('band %d not found in %s'
                                   ' with %d bands'
                                   % (np.max(band_indexes), filename,
                                      nbands_orig),
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            # sys.exit(1)
            return
        nbands = len(band_indexes)
        # band_indexes = band
    # else:
    #    nbands = current_dataset.RasterCount
    #        band_indexes = None
    #    else:
    geotransform_temp = plant.get_geotransform_from_header(filename)
    if plant.valid_coordinates(geotransform=geotransform_temp,
                               lat_size=length_orig,
                               lon_size=width_orig):
        if edges_outer:
            geotransform = plant.geotransform_edges_to_centers(
                geotransform_temp,
                lat_size=length_orig,
                lon_size=width_orig)
        else:
            geotransform = geotransform_temp
    else:
        geotransform = None

    if geotransform is not None and plant_transform_obj is not None:
        plant_transform_obj.update_crop_window(geotransform=geotransform,
                                               length_orig=length_orig,
                                               width_orig=width_orig)

    image_obj = plant.PlantImage(
        image_obj,
        filename=filename,
        filename_orig=filename_orig,
        header_file=header_file,
        # image=image,

        width_orig=width_orig,
        length_orig=length_orig,
        depth_orig=depth_orig,
        dtype=dtype,

        file_format='ISCE',
        # ref_image_obj=ref_image_obj,
        plant_transform_obj=plant_transform_obj,
        geotransform=geotransform,
        nbands_orig=nbands_orig,
        band_orig=band_indexes,
        # input_key=input_key,
        nbands=nbands,
        scheme=scheme,
        **image_obj_kwargs)

    if only_header:
        # image_obj =
        plant.apply_crop(image_obj,
                         plant_transform_obj=plant_transform_obj,
                         verbose=verbose)
        return image_obj

    if band_indexes is None:
        band_indexes = np.arange(nbands)
    # elif not isinstance(band, list):
    #     band = [band]
    flag_error = False
    if header_file is not None and depth_orig <= 1:
        try:
            for i, current_band in enumerate(band_indexes):
                from isceobj.Util.ImageUtil import ImageLib as IML
                # if i == 0:
                current_image = IML.mmapFromISCE(filename,
                                                 IML.createLogger(
                                                     0)).bands[current_band]
                if verbose:
                    str_message = ('opening: ' + filename +
                                   ' (ISCE:mmap)')
                    if nbands_orig > 1:
                        str_message += (' (band=' +
                                        str(int(current_band))+')')
                    print(str_message)
                if not read_only:
                    current_image = np.copy(current_image)
                image_obj.set_image(current_image, band=band_indexes[i],
                                    realize_changes=False)
        except:
            flag_error = True

    if flag_error or header_file is None or depth_orig >= 1:
        dtype_np = plant.get_np_dtype(dtype)
        image = np.memmap(filename,
                          dtype=dtype_np,
                          mode='r',
                          offset=offset,
                          shape=(length_orig * depth_orig *
                                 width_orig * nbands_orig))
        # image = np.fromfile(filename, dtype=dtype)

        str_message = f'opening: {filename} (ISCE numpy)'
        # if depth_orig == 1:
        #     shape = (length_orig, width_orig)
        # else:
        shape = (depth_orig, length_orig, width_orig)
        if nbands_orig == 1:
            if verbose:
                print(str_message)
            image = np.reshape(image, shape)
            image_obj.set_image(image, realize_changes=False)
        else:
            if verbose and len(band_indexes) == 1:
                str_message += (' (band=' +
                                str(int(band_indexes[0]))+')')
            nbands_ref = depth_orig * nbands_orig
            for band_index, current_band in enumerate(band_indexes):
                current_image = np.zeros(shape, dtype=dtype)
                for depth in range(depth_orig):
                    band_location = current_band*depth_orig+depth
                    if scheme.upper() == 'BIP':
                        image_temp = \
                            image[band_location::
                                  nbands_ref].reshape(shape[1:])
                        current_image[depth, :, :] = image_temp
                    elif scheme.upper() == 'BIL':
                        for i in range(length_orig):
                            band_offset = (i*width_orig*nbands_ref +
                                           band_location*width_orig)
                            current_image[depth, i, :] = \
                                image[band_offset:band_offset+width_orig]
                    elif scheme.upper() == 'BSQ':
                        current_image[depth, :, :] = \
                            image[(width_orig*length_orig)*band_location:
                                  (width_orig*length_orig) *
                                  (band_location+1)].reshape(length_orig,
                                                             width_orig)
                    else:
                        plant.handle_exception(
                            'read_image(): not implemented '
                            'for scheme: '+scheme,
                            flag_exit_if_error,
                            flag_no_messages=flag_no_messages,
                            verbose=verbose)
                        # sys.exit(1)
                        return
                if depth == 1:
                    current_image = np.reshape(current_image,
                                               (length_orig, width_orig))
                else:
                    current_image = np.reshape(current_image,
                                               (depth_orig, length_orig,
                                                width_orig))
                # from plant_ui import imshow
                # imshow(current_image)
                image_obj.set_image(current_image, band=band_index,
                                    realize_changes=False)

    '''
    # image_obj =
    plant.apply_crop(image_obj,
                     plant_transform_obj=plant_transform_obj,
                     verbose=verbose)
    # image_obj =
    plant.apply_null(image_obj,
                     in_null=in_null,
                     out_null=out_null)
    # image_obj =
    plant.apply_mask(image_obj,
                     plant_transform_obj=plant_transform_obj,
                     verbose=verbose,
                     force=force)
    '''
    prepare_image(image_obj,
                  # filename,
                  flag_exit_if_error=flag_exit_if_error,
                  plant_transform_obj=plant_transform_obj,
                  in_null=in_null,
                  out_null=out_null,
                  input_format=input_format,
                  verbose=verbose,
                  only_header=only_header,
                  flag_no_messages=flag_no_messages,
                  force=force)
    return image_obj


# based on ImageLib.renderEnviHDR
def create_isce_header(input_file,
                       nbands=1,
                       length=None,
                       width=None,
                       dtype=None,
                       scheme=None,
                       bbox=None,
                       step=None,
                       descr=None,
                       data=None,
                       verbose=False,
                       geotransform=None,
                       projection=None,
                       image_obj=None):
    '''
    renders an ISCE XML with the right information.
    '''
    try:
        from isceobj import createImage
    except ImportError:
        error_message = plant.get_error_message()
        '''
        print('WARNING isceobj.createImage could'
              ' not be imported, skipping ISCE header'
              ' of: %s. %s' % (input_file,
                               error_message))
        '''
        return
    try:
        img = createImage()
    except ImportError:
        # error_message = plant.get_error_message()
        '''
        print('WARNING error executing createImage(),'
              ' skipping ISCE header creation of: %s.'
              # ' ISCE error message: %s'
              % input_file)
        '''
        # , error_message
        return
    except:
        error_message = plant.get_error_message()
        print('WARNING %s' % error_message)
        return

    '''
    if (input_file and input_file.endswith('.xml') and
            plant.isfile(input_file.replace('.xml',
                                               '')):
        img.filename = input_file.replace('.xml', '')
    elif input_file:
    '''
    if image_obj is None:
        image_obj = data
    image_obj = plant.PlantImage(
        image_obj,
        # filename=img.filename,
        nbands=nbands,
        length=length,
        width=width,
        geotransform=geotransform,
        # description=descr,
        scheme=scheme,
        dtype=dtype,
        file_format='ISCE',
        # plant_transform_obj=plant_transform_obj,
        projection=projection)

    if input_file and not input_file.endswith('xml'):
        img.filename = input_file
    else:
        img.filename = image_obj.filename

    # geo-information:
    if (image_obj.geotransform is not None and
            (bbox is None or
             step is None)):
        # geotransform_edges=plant.geotransform_centers_to_edges(geotransform)
        ret_dict = plant.get_coordinates(
            geotransform=image_obj.geotransform,
            lon_size=image_obj.width,
            lat_size=image_obj.length)
        if ret_dict is not None:
            lat_arr = ret_dict['lat_arr']
            lon_arr = ret_dict['lon_arr']
            step_lat = ret_dict['step_lat']
            step_lon = ret_dict['step_lon']
            # lat_size = ret_dict['lat_size']
            # lon_size = ret_dict['lon_size']
            if plant.isvalid(step_lat) and plant.isvalid(step_lon):
                step = [step_lat, step_lon]
            if (lat_arr is not None and
                    lon_arr is not None and
                    plant.isvalid(lat_arr[0]) and
                    plant.isvalid(lat_arr[1]) and
                    plant.isvalid(lon_arr[0]) and
                    plant.isvalid(lon_arr[1])):
                bbox = plant.get_bbox(lat_arr, lon_arr)
    img.width = image_obj.width
    img.length = image_obj.length
    if bbox is not None:
        if step is None:
            print('WARNING bbox coordinates were provided but '
                  'no pixel size information was found')
            return
        else:
            step_temp = np.asarray(step)
            dim = np.sum(step_temp.shape)
            if dim == 0:  # int, float, etc
                step_temp = [step, step]
            elif dim == 1:  # array/list one element
                step_temp = [step[0], step[0]]
            else:
                step_temp = step
            if bbox[1] < bbox[0]:
                img.setDeltaLatitude(abs(step_temp[0]))
            else:
                img.setDeltaLatitude(-abs(step_temp[0]))
            img.setDeltaLongitude(step_temp[1])
        img.coord2.coordStart = bbox[1]  # +abs(step_temp[0])/2
        img.coord2.coordEnd = bbox[0]  # -abs(step_temp[0])/2
        img.setXmin(bbox[2])  # -step_temp[1]/2)
        img.setXmax(bbox[3])  # +step_temp[1]/2)
    else:
        img.coord2.coordEnd = np.nan
        img.setXmax(np.nan)

    if descr is not None:
        img.addDescription(descr)
    n_elements = (image_obj.width *
                  image_obj.length *
                  image_obj.depth)

    filename = os.path.realpath(img.filename)
    if isheader(filename):
        return

    if image_obj.dtype is None:
        print('WARNING could not determine data type of: ' +
              input_file +
              ' considering it as: '+str(dtype))
    else:
        img.dataType = plant.get_isce_dtype(image_obj.dtype)

    if image_obj.scheme is not None:
        img.scheme = image_obj.scheme
    else:
        img.scheme = 'BSQ'
    img.bands = image_obj.nbands

    if plant.isfile(filename):
        file_size = os.stat(filename).st_size
        observed_type_size = round(file_size /
                                   (n_elements*image_obj.nbands))
        type_size = plant.get_dtype_size(img.dataType)
        if (file_size % n_elements) != 0:
            print('WARNING dimensions seems to be wrong. ')
            verbose = True
    else:
        type_size = None

    if (type_size is not None and
            type_size != observed_type_size):
        print('WARNING %s data type seems to be wrong'
              % (img.filename))
        print('file size: %d' % (file_size))
        print('number of pixels: %d' % (n_elements))
        print('expected type size: %d' % (type_size))
        print('observed type size: %d' % (observed_type_size))
        print('bands: %d' % (image_obj.nbands))
        verbose = True
        # raise

    if verbose:
        print('filename: %s' % (img.filename))
        print('width: %d' % (img.width))
        print('length: %d' % (img.length))
        print('depth: %d' % (image_obj.depth))
        print('number of elements: %d' % (n_elements))
        if plant.isfile(img.filename):
            print('file size: %d' % (file_size))
            print('file size/n_elements: %f' % (file_size/n_elements))
        print('dtype: %s' % (img.dataType))

        print('dtype size: %s' % (observed_type_size))

        print('scheme: %s' % (img.scheme))
        print('bands: %s' % (img.bands))
        if geotransform or bbox:
            print('lat beg (upper left): ', img.coord2.coordStart)
            print('lat end (lower left): ', img.coord2.coordEnd)
            print('lat step: ', img.getDeltaLatitude())
            print('lon beg: ', img.getXmin())
            print('lon end: ', img.getXmax())
            print('lon step: ', img.getDeltaLongitude())
    try:
        img.renderHdr()
    except:
        error_message = plant.get_error_message()
        if dtype == 'LONG':
            print('WARNING ISCE header could '
                  'not be created. Please, try another '
                  'output data type. %s'
                  % error_message)
        else:
            print('WARNING ISCE header could '
                  'not be created. %s'
                  % error_message)
    return True


def isheader(filename):
    ext_list = ['.txt', '.vrt', '.xml']
    for ext in ext_list:
        if filename.endswith(ext):
            return True
    return False


def crop_valid_box_image_obj(input_name, null=None,
                             verbose=True, flag_all=False):
    flag_return_array = False
    if isinstance(input_name, str):
        image_obj = plant.read_image(input_name)
    elif isinstance(input_name, plant.PlantImage):
        image_obj = input_name
    else:
        flag_return_array = True
        image_obj = plant.PlantImage(input_name)
        # image_obj = None
        # input_name = plant.shape_image(input_name)
        # depth, length, width = get_image_dimensions(input_name)

    min_y = image_obj.length
    max_y = 0
    min_x = image_obj.width
    max_x = 0
    for current_band in range(image_obj.nbands):
        image_1 = image_obj.getImage(band=current_band)
        for i in range(min_y):
            if np.sum(plant.isvalid(image_1[i, :],
                      null=null)) != 0:
                min_y = i
                break
        for j in range(min_x):
            if np.sum(plant.isvalid(image_1[:, j],
                      null=null)) != 0:
                min_x = j
                break
        for i in range(image_obj.length - max_y):
            if np.sum(plant.isvalid(image_1[image_obj.length-i-1,
                                            :],
                      null=null)) != 0:
                max_y = image_obj.length-i-1
                break
        for j in range(image_obj.width - max_x):
            if np.sum(plant.isvalid(image_1[:, image_obj.width -
                                            j-1],
                      null=null)) != 0:
                max_x = image_obj.width-j-1
                break
    if min_y > max_y:
        min_y = np.nan
        max_y = np.nan
    if min_x > max_x:
        min_x = np.nan
        max_x = np.nan

    if verbose and plant.isvalid(min_y) and plant.isvalid(min_x):
        print('Start point (x, y): (%d, %d)' % (min_x, min_y))
    if verbose and plant.isvalid(max_y) and plant.isvalid(max_x):
        print('End point (x, y): (%d, %d)' % (max_x, max_y))

    if (plant.isnan(min_y) and plant.isnan(min_x) and
            plant.isnan(max_y) and plant.isnan(max_x)):
        return

    out_image_obj = image_obj.copy()
    for current_band in range(image_obj.nbands):
        image_1 = image_obj.getImage(band=current_band)
        image_1 = image_1[min_y:max_y+1, min_x:max_x+1]
        out_image_obj.set_image(image_1, band=current_band)
    if flag_return_array:
        image = out_image_obj.image
    else:
        image = out_image_obj
    if not flag_all:
        return image
    ret_dict = {}
    ret_dict['image'] = image
    ret_dict['min_x'] = min_x
    ret_dict['min_y'] = min_y
    ret_dict['max_x'] = max_x
    ret_dict['max_y'] = max_y
    return ret_dict


def crop_fig(filename, force=True, verbose=True):
    image_obj = plant.read_image(filename)
    image_obj = crop_valid_box_image_obj(image_obj,
                                         null=255,
                                         verbose=verbose)
    plant.save_image(image_obj, filename, force=force,
                     verbose=verbose)
