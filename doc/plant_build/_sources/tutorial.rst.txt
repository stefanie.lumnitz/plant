Tutorial
=================================

Introduction
------------
Polarimetric Interferometric Lab and Analysis Tool

PLANT is a collection of software tools developed at the Jet Propulsion Laboratory to support processing and analysis of Synthetic Aperture Radar (SAR) data for ecosystem and land-cover/land-use change science and applications. PLANT inherits code components from the Interferometric Scientific Computing Environment (ISCE) to generate high-resolution, polarimetric-interferometric SLC stacks from Level-0 or Level-1 data for a variety of airborne and spaceborne sensors. The goal is to provide the ecosystem and land-cover/land-use change communities with rigorous and efficient tools to perform multi-temporal, polarimetric and tomographic analyses in order to generate calibrated, geocoded and mosaicked Level-2 and Level-3 products (e.g., maps of above-ground biomass or forest disturbance).

M. Lavalle, G. H. X. Shiroma, P. Agram, E. Gurrola, G. F. Sacco, and P. Rosen, “PLANT: Polarimetric-interferometric lab and analysis tools for ecosystem and land-cover science and applications,” in 2016 IEEE International Geoscience and Remote Sensing Symposium (IGARSS), July 2016, pp. 5354–5357.
